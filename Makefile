interprete = @python3 manage.py

server:
	$(interprete) runserver 0.0.0.0:8001

restart:
	@sudo systemctl restart gunicorn
	@sudo systemctl restart nginx
watch:
	@sudo tail -f /var/log/syslog