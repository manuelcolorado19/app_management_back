from django.db import models
from OperationManagement.settings import MERCADOLIBRE_USER_ID as user_ml 


	# rut = models.CharField(null=True, blank=True, max_length=30)
	# name = models.CharField(max_length=150, blank=True)
# Create your models here.

class Period(models.Model):

    name = models.CharField(null = False, blank = False, max_length = 50)
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    time_measure = models.CharField(null=False, blank = False, max_length=10)

    class Meta: 
        db_table = 'period'

class ResponseTime(models.Model):

     response_time = models.IntegerField(null=False)
     sales_percent_increase = models.IntegerField(null=True)
     create_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
     search_date = models.DateField(null=False)
     period = models.ForeignKey(Period, on_delete=models.SET_NULL, null=True, blank=False, related_name='period_response_time')
    
     class Meta:
        db_table = 'response_time'


