from rest_framework import serializers
from .models import Period, ResponseTime

class ResponseTimeSerializer(serializers.ModelSerializer):
    class Meta: 
        model = ResponseTime
        fields = '__all__'

class PeriodSerializer(serializers.ModelSerializer):
    period_response_time = ResponseTimeSerializer(read_only=True, many=True )
    class Meta: 
        model = Period
        fields = '__all__'