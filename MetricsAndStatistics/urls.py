from django.conf.urls import url, include 
from rest_framework.routers import DefaultRouter
from .views import PeriodViewSet

router = DefaultRouter()
router.register(r'period', PeriodViewSet, base_name='period')
urlpatterns = [
    url(r'^', include(router.urls))

]