from datetime import datetime, timedelta
from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Period, ResponseTime
from .serializer import PeriodSerializer
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.response import Response
import itertools 
from operator import itemgetter 

class PeriodViewSet(ModelViewSet):
    queryset = Period.objects.all()
    serializer_class = PeriodSerializer
    permission_classes = (AllowAny,)
    http_method_names = ["get"]

    def list(self, request):
        reference_day = datetime.now() - timedelta(days=15)
        responses_time = ResponseTime.objects.filter(search_date__gte=reference_day).exclude(period__name='total').values("response_time", "sales_percent_increase", "search_date", "period__name").order_by('period__name')
        group_order = []
        group_order_period = []
        data_table = []
        data_chart = []
        sorted_order = sorted(responses_time, key=itemgetter('search_date'))
        for key, group in itertools.groupby(sorted_order, key=lambda x:x["search_date"] ):
            group_order.append(list(group))
        for item in group_order:
            data_table.append({
                "date_search":item[0]['search_date'],
                "p1":item[1]['response_time'],
                "p2":item[0]['response_time'],
                "p3":item[2]['response_time'],
                "e1":item[1]['sales_percent_increase'],
                "e2":item[0]['sales_percent_increase'],
                "e3":item[2]['sales_percent_increase'],
            })
        responses_time_period = ResponseTime.objects.values("response_time", "sales_percent_increase", "search_date","period__name")
       
        for each in responses_time_period: 
            group_order_period.append(each)
        third = []
        fourth =[]
        fifth = []
        group_order_period = sorted(group_order_period, key=itemgetter('search_date'))
        for item in group_order_period:
            if item["period__name"] == 'weekdays_extra_hours':
                third.append({"name":item["search_date"], "value": item["response_time"]})
            if item["period__name"] == 'weekdays_working_hours':
                fourth.append({"name":item["search_date"], "value": item["response_time"]})
            if item["period__name"] == 'weekend':
                fifth.append({"name":item["search_date"], "value": item["response_time"]})
        data_chart.append({
            "name": "Lunes a Viernes 18:00 - 00:00",
            "series":third
        })
        data_chart.append({
            "name": "Lunes a Viernes 09:00 - 18:00",
            "series":fourth
        })
        data_chart.append({
            "name": "Fin de semana",
            "series":fifth
        }) 
        return Response(data={"data_table":data_table, "data_chart": data_chart}, status=status.HTTP_200_OK)
