"""OperationManagement URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from Scraper.views import ScraperView, MergeLabelsFalabella
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from rest_framework.routers import DefaultRouter
from users.views import UserViewSet, ForgotViewSet, RecoverPasswordViewSet
from products.views import ProductViewSet, ExportDataMercadolibreXLSViewSet, ExportDataPcfactoryXLSXViewSet, ExportOrdersXLSViewSet, DownloadXLSXMercadolibre, SendOrdersEmail, IndicatorsViewSet, MLCDataViewSet, CategoryRanking, SellerRanking
from sales.views import SalesXLSViewSet, RetailXLSExampleViewSet, MassiveRetailDataViewSet, AutomaticRetailSalesXLSViewSet, GenerateBillOpenFactura, AutomaticRetailSalesRipleyXLSViewSet, StockXLSXViewSet, AutomaticRetailSalesLinioXLSViewSet, ExportXLSSKUViewSet, ShippingDocument, UpdateShippingSubStatus, Sales_ReportCashViewSet, GrouponUpload, ChannelSaleReport, FalabellaUpload, ShippingGuide, ParisUpload, BillParisGenerate, ExportParisEmmitedOrdersXLSViewSet, AutomaticRetailSalesMercadoshopXLSViewSet, ChargeSKUList, ChargeSKUPack, CancelBillOpenFactura, DeleteSKU, ExportPublicationsXLSViewSet, ExportPublicationsRipleyXLSViewSet, ExportPublicationsLinioXLSViewSet, LastReportSales
from purchases.views import MassivePurchaseData, PurchasesXLSViewSet, PurchaseXLSXExampleViewSet
from prices.views import Price_ProductXLSViewSet, UploadPricesFalabella
from notifications.views import SendAutomaticMessage, VideoProceduresUploadView, QuestionsFileXlsx
from activities.views import GenerateReportSupervisor
from stock.views import InventoryReport, TotalizationInventory
from warehouses.views import InitialStockWarehouse
from retailPayment.views import RetailPaymentSalesReport, BankReportXLSX, RetailPaymentsChecked,RetailPaymentXLSX, SalesPaymentXLSX, PaymentForecastReportXLSX
from .routers import DefaultRouter
from products.urls import router as products_router
from users.urls import router as users_router
from sales.urls import router as sales_router
from purchases.urls import router as purchases_router
from prices.urls import router as prices_router
from notifications.urls import router as notifications_router
from returns.urls import router as returns_router
from activities.urls import router as activity_router
from stock.urls import router as stock_router
from warehouses.urls import router as warehouse_router
from retailPayment.urls import router as retail_payment_router
from MetricsAndStatistics.urls import router as metrics_statistics_router
from django.conf.urls.static import static
from OperationManagement import settings

router = DefaultRouter()
# router.register(r'users', UserViewSet, base_name='users')
# router.register(r'products', ProductViewSet, base_name='products')
router.extend(users_router)
router.extend(products_router)
router.extend(sales_router)
router.extend(purchases_router)
router.extend(prices_router)
router.extend(notifications_router)
router.extend(returns_router)
router.extend(activity_router)
router.extend(stock_router)
router.extend(warehouse_router)
router.extend(retail_payment_router)
router.extend(metrics_statistics_router)



urlpatterns = [
    url(r'^', include(router.urls)),
    path('admin/', admin.site.urls),
    path('forgot/password/', ForgotViewSet.as_view()),
    path('change/password/', RecoverPasswordViewSet.as_view()),
    path('scrapper/', ScraperView.as_view()),
    url(r'^login/', obtain_jwt_token),
    url(r'^refresh/', refresh_jwt_token),
    url(r'^export/ml/xls/$', ExportDataMercadolibreXLSViewSet.as_view(), name='export_data_mercadolibre_xls'),
    url(r'^export/ml/xls/generate$', DownloadXLSXMercadolibre.as_view(), name='export_data_mercadolibre_xls_generate'),
    # url(r'^export/pcfactory/xls/$', ExportDataPcfactoryXLSViewSet.as_view(), name='export_data_pcfactory_xls'),
    url(r'^export/pcfactory/xlsx/$', ExportDataPcfactoryXLSXViewSet.as_view(), name='export_data_pcfactory_xlsx'),
    url(r'^export/orders/xlsx$', ExportOrdersXLSViewSet.as_view(), name='export_data_orders_xlsx'),
    url(r'^export/publications/xlsx$', ExportPublicationsXLSViewSet.as_view(), name='export_publications_ml_xlsx'),
    url(r'^export/publications-ripley/xlsx$', ExportPublicationsRipleyXLSViewSet.as_view(), name='export_publications_ripley_xlsx'),
    url(r'^export/publications-linio/xlsx$', ExportPublicationsLinioXLSViewSet.as_view(), name='export_publications_linio_xlsx'),
    url(r'^send/orders/xlsx$', SendOrdersEmail.as_view(), name='send_orders_xlsx'),
    url(r'^export/sales/xlsx$', SalesXLSViewSet.as_view(), name='export_sales_xlsx'),
    url(r'^export/sales/retail/example$', RetailXLSExampleViewSet.as_view(), name='export_retail_example_xlsx'),
    url(r'^export/prices/xlsx$', Price_ProductXLSViewSet.as_view(), name='export_prices_xlsx'),
    url(r'^export/sku/xlsx$', ExportXLSSKUViewSet.as_view(), name='export_sku_xlsx'),
    url(r'^export/inventory/xlsx$', InventoryReport.as_view(), name='export_inventory_xlsx'),
    url(r'^massive/retail/data$', MassiveRetailDataViewSet.as_view(), name='massive-retail-data'),
    url(r'^massive/purchase/data$', MassivePurchaseData.as_view(), name='massive-purchase-data'),
    url(r'^export/purchase/xlsx/example$', PurchaseXLSXExampleViewSet.as_view(), name='export_purchase_example_xlsx'),
    url(r'^export/purchases/(?P<pk>\d+)/$', PurchasesXLSViewSet.as_view(), name='export_purchase_xlsx'),
    url(r'^automatic/retail/sales/mercadolibre$', AutomaticRetailSalesXLSViewSet.as_view(), name='automatic_retail_sales_mercadolibre'),
    url(r'^automatic/retail/sales/ripley$', AutomaticRetailSalesRipleyXLSViewSet.as_view(), name='automatic_retail_sales_ripley'),
    url(r'^automatic/retail/sales/linio$', AutomaticRetailSalesLinioXLSViewSet.as_view(), name='automatic_retail_sales_linio'),
    url(r'^automatic/retail/sales/mercadoshop$', AutomaticRetailSalesMercadoshopXLSViewSet.as_view(), name='automatic_retail_sales_linio'),
    url(r'^generate-bill/$', GenerateBillOpenFactura.as_view(), name='generate-bill'),
    url(r'^generate/shipping-document/$', ShippingDocument.as_view(), name='generate-shipping-document'),
    url(r'^update/shipping-sub-status$', UpdateShippingSubStatus.as_view(), name='update-shipping-sub-status'),
    url(r'^sales/report/cash$', Sales_ReportCashViewSet.as_view(), name='sales-report-cash'),
    url(r'^sales/report/channel$', ChannelSaleReport.as_view(), name='sales-report-channel'),
    url(r'^upload/(?P<filename>[^/]+)$', VideoProceduresUploadView.as_view(), name='file_upload'),
    url(r'^load/groupon$', GrouponUpload.as_view(), name='upload-groupon'),
    url(r'^load/falabella$', FalabellaUpload.as_view(), name='upload-falabella'),
    url(r'^load/paris$', ParisUpload.as_view(), name='upload-paris'),
    url(r'^sku_charge/$', ChargeSKUList.as_view(), name='upload-sku'),
    url(r'^sku_charge_pack/$', ChargeSKUPack.as_view(), name='upload-sku-pack'),
    url(r'^shipping/guide$', ShippingGuide.as_view(), name='shipping-guide'),
    url(r'^generate/bill$', BillParisGenerate.as_view(), name='bill-generate'),
    url(r'^export/emmited$', ExportParisEmmitedOrdersXLSViewSet.as_view(), name='export-emmited'),
    # url(r'^print/bill/(?P<pk>\d+)$', PrintBill.as_view(), name='print-bill'),
    url(r'^send/message/$', SendAutomaticMessage.as_view(), name='automatic_message'),
    url(r'^generate-report/$', GenerateReportSupervisor.as_view(), name='generate-report'),
    url(r'^merge-labels/$', MergeLabelsFalabella.as_view(), name='merge-labels'),
    url(r'^price_product_falabella/$', UploadPricesFalabella.as_view(), name='upload-prices-falabella'),
    url(r'^indicators/$', IndicatorsViewSet.as_view(), name='indicators-nocturnal-process'),
    url(r'^mlc-data/$', MLCDataViewSet.as_view(), name='mlc-data-chart'),
    url(r'^load-warehouse/$', InitialStockWarehouse.as_view(), name='load-warehouse'),
    url(r'^cancel-bill/$', CancelBillOpenFactura.as_view(), name='cancel-bill'),
    url(r'^category-ranking/$', CategoryRanking.as_view(), name='category-ranking'),
    url(r'^seller-ranking/$', SellerRanking.as_view(), name='seller-ranking'),
    url(r'^retail-sales-report/$', RetailPaymentSalesReport.as_view(), name='retail-sales-report'),
    url(r'^bank-xlsx-report/$', BankReportXLSX.as_view(), name='bank-xlsx-report'),
    url(r'^payment-xlsx-report/$', RetailPaymentXLSX.as_view(), name='payment-xlsx-report'),
    url(r'^sales-xlsx-report/$', SalesPaymentXLSX.as_view(), name='sales-xlsx-report'),
    url(r'^remove-sku/$', DeleteSKU.as_view(), name='remove-sku'),
    url(r'^last-report-sale/$', LastReportSales.as_view(), name='last-report'),
    url(r'^retail-payment-checked/$', RetailPaymentsChecked.as_view(), name='payment-checked'),
    url(r'^question-file/$', QuestionsFileXlsx.as_view(), name='payment-checked'),
    url(r'^export/totalization/xlsx$', TotalizationInventory.as_view(), name='export_totalization_xlsx'),
    url(r'^payment-forecast$', PaymentForecastReportXLSX.as_view(), name='payment_forecast'),
    #url(r'^intention-robot/$', IntentionRobotViewSet.as_view(), name='intention-robot'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
