from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from bs4 import BeautifulSoup
from rest_framework.permissions import AllowAny
import requests
import mechanicalsoup
from os import listdir
from os.path import isfile, join
from pdf2image import convert_from_path
from os import listdir
from os.path import isfile, join
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
import shutil, os
from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from django.http import FileResponse
import base64, io
from PyPDF2 import PdfFileWriter, PdfFileReader
from reportlab.lib.pagesizes import letter
from reportlab.lib.pagesizes import letter, landscape

class ScraperView(APIView):
    def get(self, request):
        page_link = 'https://articulo.mercadolibre.cl/MLC-460192973-silla-mecedora-bebeglo-rosado-_JM#reco_item_pos=3&reco_backend=l3-l7-v2p-ngrams-unified-model&reco_backend_type=function&reco_client=navigation_trend_homes&reco_id=42f8cca3-9787-498c-b33f-a2e724f33c9e&c_id=/home/navigation-trends-recommendations/element&c_element_order=4'
        page_response = requests.get(page_link, timeout=5)
        page_content = BeautifulSoup(page_response.content, "html.parser")
        title_page = page_content.title.text
        if 'Mercado Libre' in title_page:
            content = page_content.find_all('span',attrs={"class":"price-tag-fraction"})
            price = float(content[0].text.replace('.','').replace(',','.'))
        if 'Ripley' in title_page:
            content = page_content.find_all('span',attrs={"class":"product-price"})
            price = float(content[1].text[1:].replace('.','').replace(',','.'))
        if 'yapo.cl' in title_page:
            content = page_content.find('div', attrs={"class": "price"})
            for t in content.text.split():
                try:
                    price = float(t.replace('.','').replace(',','.'))
                except ValueError:
                    pass

        # get search from ml, yapo, ripley

        # # Connect to duckduckgo
        # browser = mechanicalsoup.StatefulBrowser()
        # browser.open("https://duckduckgo.com/") https://www.yapo.cl/, https://www.mercadolibre.cl/, https://simple.ripley.cl/cyber
        #
        # # Fill-in the search form
        # browser.select_form('#search_form_homepage') //ripley autocomplete-input, ml .nav-search
        # browser["q"] = "MechanicalSoup" //ripley js-search-bar, ml as_word,
        # browser.submit_selected()
        #
        # # Display the results
        # for link in browser.get_current_page().select('a.result__a'):
        #     print(link.text, '->', link.attrs['href']) //a.item__info-title para ml, catalog-product catalog-item para Ripley
        #  redirect-to-url para yapo.cl

        return Response(data={'precios': float(price)}, status=status.HTTP_200_OK)


class MergeLabelsFalabella(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        # print(request.data)
        for each_file, value_file in request.data.items():
            myfile = value_file
            fs = FileSystemStorage(location="Scraper/labels/") #defaults to   MEDIA_ROOT  
            filename = fs.save(myfile.name, myfile)


        path_labels = "Scraper/labels"
        onlyfiles = [f for f in listdir(path_labels) if isfile(join(path_labels, f))]
        double_label = {}


        count = []
        for label in onlyfiles:
            split_name = label[label.find("(")+1:label.find(")")].split("-")[0]
            if split_name.isdigit():
                count.append(label)
            # for label2 in onlyfiles:
            #     if split_name in label2:
            # if len(count) > 1:
            #     double_label[split_name] = count

        for merge in count:
        	label_numbers = int(merge[merge.find("(")+1:merge.find(")")].split("-")[0])
        	for y in range(label_numbers):
        		packet = io.BytesIO()
        		can = canvas.Canvas(packet, pagesize=letter)
        		can.drawString(10, 570, str(y + 1) + " de " + str(label_numbers))
        		can.save()
        		packet.seek(0)
        		new_pdf = PdfFileReader(packet)
        		file_existin = open(path_labels +"/"+ merge, "rb")
        		existing_pdf = PdfFileReader(file_existin)
        		output = PdfFileWriter()
        		page = existing_pdf.getPage(0)
        		page.mergePage(new_pdf.getPage(0))
        		output.addPage(page)
        		outputStream = open(path_labels +"/merge " + str(merge.replace(".pdf", "") + str(y + 1)) + ".pdf", "wb")
        		output.write(outputStream)
        		outputStream.close()
        		file_existin.close()
        	os.remove(path_labels +"/"+ merge)

        # for r,value in double_label.items():
        #     for each in range(0, len(value)):
        #         packet = io.BytesIO()
        #         can = canvas.Canvas(packet, pagesize=letter)
        #         can.drawString(10, 570, str(each + 1) + " de " + str(len(value)))
        #         can.save()
        #         packet.seek(0)
        #         new_pdf = PdfFileReader(packet)
        #         file_existin = open(path_labels +"/"+ value[each], "rb")
        #         existing_pdf = PdfFileReader(file_existin)
        #         output = PdfFileWriter()
        #         page = existing_pdf.getPage(0)
        #         page.mergePage(new_pdf.getPage(0))
        #         output.addPage(page)
        #         outputStream = open(path_labels +"/merge " + str(r + str(each)) + ".pdf", "wb")
        #         output.write(outputStream)
        #         outputStream.close()
        #         file_existin.close()
        #         os.remove(path_labels +"/"+ value[each])

        # print(lkjhgfghjk)

        # path_labels = "Scraper/labels"
        onlyfiles = [f for f in listdir(path_labels) if isfile(join(path_labels, f))]
        i = 1
        for label in onlyfiles:
            pages = convert_from_path(path_labels +"/"+ label, 500)
            for page in pages:
                page.save('Scraper/images/out' + str(i) +'.jpg', 'JPEG')
            i += 1



        path_labels = "Scraper/images"
        onlyfiles = [f for f in listdir(path_labels) if isfile(join(path_labels, f))]
        w, h = A4
        path_labels = path_labels +"/" 
        buff = io.BytesIO()
        c = canvas.Canvas(buff, pagesize=landscape(letter))
        x_constant = 260
        width_ = 730
        height_ = 600
        last = len(onlyfiles) % 3
        for i in range(0, (len(onlyfiles) - last), 3):
            c.drawImage(path_labels + onlyfiles[i], 0, -5, width=width_, height=height_)
            c.drawImage(path_labels + onlyfiles[i + 1], x_constant, -5, width=width_, height=height_)
            c.drawImage(path_labels + onlyfiles[i + 2], x_constant * 2, -5, width=width_, height=height_)
            # c.drawImage(path_labels + onlyfiles[i + 3], 10, h - 900, width=500, height=500)
            # c.drawImage(path_labels + onlyfiles[i + 4], 210, h - 900, width=500, height=500)
            # c.drawImage(path_labels + onlyfiles[i + 5], 410, h - 900, width=500, height=500)
            c.showPage()
        if last > 0:
            #print("aqui")
            x_value = 0
            for i in range(1, last + 1):
                # print(i)
                # h_value = h - 100
                if x_value > 410:
                    x_value = 10
                # if i > 3:
                #     h_value = h - 900
                c.drawImage(path_labels + onlyfiles[(len(onlyfiles) - last + i) - 1], x_value, -5, width=width_, height=height_)
                x_value += 280
        c.save()

        folder = 'Scraper/images'
        for the_file in os.listdir(folder):
            file_path = os.path.join(folder, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                #elif os.path.isdir(file_path): shutil.rmtree(file_path)
            except Exception as e:
                print(e)

        folder = 'Scraper/labels'
        for the_file in os.listdir(folder):
            file_path = os.path.join(folder, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                #elif os.path.isdir(file_path): shutil.rmtree(file_path)
            except Exception as e:
                print(e)
        # with open("Etiquetas falabella.pdf", "rb") as pdf_file:
        #     encoded_string = base64.b64encode(pdf_file.read())

        # # report_encoded = base64.b64encode(buff.getvalue())
        response = HttpResponse(buff.getvalue(), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="' + "hola.pdf" + '"'
        return response
        # return Response(data={"labels": ["Etiquetas generadas de forma exitosa"],}, status=status.HTTP_200_OK)
