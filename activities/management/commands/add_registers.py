import requests
import datetime
from notifications.models import Token
from activities.models import Activity, ReportSupervisor, Report_Activity
from MetricsAndStatistics.models import Period
from MetricsAndStatistics.models import ResponseTime
from OperationManagement.settings import MERCADOLIBRE_USER_ID as user_ml
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    args = ''
    help = 'Get answer response time for questions from ML API'

    def handle(self, *args, **options):
        ResponseTime.objects.create( response_time = 500 ,sales_percent_increase = 10 , search_date ="2019-07-23", period_id= 1)
        ResponseTime.objects.create( response_time = 180 ,sales_percent_increase = 12 , search_date ="2019-07-23", period_id= 4)
        ResponseTime.objects.create( response_time = 300 ,sales_percent_increase = 18 , search_date ="2019-07-23", period_id= 3)
        ResponseTime.objects.create( response_time = 280 ,sales_percent_increase = 10 , search_date ="2019-07-23", period_id= 5)
        ResponseTime.objects.create( response_time = 220 ,sales_percent_increase = 10 , search_date ="2019-07-24", period_id= 1)
        ResponseTime.objects.create( response_time = 800 ,sales_percent_increase = 12 , search_date ="2019-07-24", period_id= 4)
        ResponseTime.objects.create( response_time = 189 ,sales_percent_increase = 10 , search_date ="2019-07-24", period_id= 3)
        ResponseTime.objects.create( response_time = 200 ,sales_percent_increase = 10 , search_date ="2019-07-24", period_id= 5)
        ResponseTime.objects.create( response_time = 420 ,sales_percent_increase = 15 , search_date ="2019-07-25", period_id= 1)
        ResponseTime.objects.create( response_time = 208 ,sales_percent_increase = 10 , search_date ="2019-07-25", period_id= 4)
        ResponseTime.objects.create( response_time = 380 ,sales_percent_increase = 10 , search_date ="2019-07-25", period_id= 3)
        ResponseTime.objects.create( response_time = 200 ,sales_percent_increase = 10 , search_date ="2019-07-25", period_id= 5)
        ResponseTime.objects.create( response_time = 255 ,sales_percent_increase = 7 , search_date ="2019-07-26", period_id= 1)
        ResponseTime.objects.create( response_time = 200 ,sales_percent_increase = 19 , search_date ="2019-07-26", period_id= 4)
        ResponseTime.objects.create( response_time = 520 ,sales_percent_increase = 10 , search_date ="2019-07-26", period_id= 3)
        ResponseTime.objects.create( response_time = 400 ,sales_percent_increase = 15 , search_date ="2019-07-26", period_id= 5)
        ResponseTime.objects.create( response_time = 200 ,sales_percent_increase = 10 , search_date ="2019-07-27", period_id= 1)
        ResponseTime.objects.create( response_time = 500 ,sales_percent_increase = 10 , search_date ="2019-07-27", period_id= 4)
        ResponseTime.objects.create( response_time = 200 ,sales_percent_increase = 10 , search_date ="2019-07-27", period_id= 3)
        ResponseTime.objects.create( response_time = 420 ,sales_percent_increase = 10 , search_date ="2019-07-27", period_id= 5)
        ResponseTime.objects.create( response_time = 230 ,sales_percent_increase = 11 , search_date ="2019-07-28", period_id= 1)
        ResponseTime.objects.create( response_time = 200 ,sales_percent_increase = 10 , search_date ="2019-07-28", period_id= 4)
        ResponseTime.objects.create( response_time = 300 ,sales_percent_increase = 10 , search_date ="2019-07-28", period_id= 3)
        ResponseTime.objects.create( response_time = 200 ,sales_percent_increase = 9 , search_date ="2019-07-28", period_id= 5)
        ResponseTime.objects.create( response_time = 280 ,sales_percent_increase = 10 , search_date ="2019-07-29", period_id= 1)
        ResponseTime.objects.create( response_time = 300 ,sales_percent_increase = 15 , search_date ="2019-07-29", period_id= 4)
        ResponseTime.objects.create( response_time = 200 ,sales_percent_increase = 10 , search_date ="2019-07-29", period_id= 3)
        ResponseTime.objects.create( response_time = 210 ,sales_percent_increase = 10 , search_date ="2019-07-29", period_id= 5)