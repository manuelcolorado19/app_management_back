from activities.models import Activity, ReportSupervisor, Report_Activity
from django.core.management.base import BaseCommand, CommandError
activities = Activity.objects.all()
class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		
		report = ReportSupervisor.objects.create()
		for i in activities:
			report_activity = Report_Activity.objects.create(
				activity=i,
				report_supervisor=report,
				count=0,
				work_time=0,
			)