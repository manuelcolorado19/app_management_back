import requests 
import datetime
from notifications.models import Token
from activities.models import Activity, ReportSupervisor, Report_Activity
from MetricsAndStatistics.models import Period
from MetricsAndStatistics.models import ResponseTime
from OperationManagement.settings import MERCADOLIBRE_USER_ID as user_ml
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
	args = ''
	help = 'Get answer response time for questions from ML API'

	def handle(self, *args, **options):
		updated_token = Token.objects.get(id=1).token_ml 
		URL = "https://api.mercadolibre.com/users/"+ user_ml +"/questions/response_time"
		PARAMS = {'access_token':updated_token}
		r = requests.get(url=URL,params=PARAMS)
		data = r.json()
		# g = 0 
		print(data)
		s_p_i = 0
		for item, key in data.items():
			for row in Period.objects.all():
				if row.name == item and item != 'user_id':
					foreign = row.id 
					if "sales_percent_increase" in key: 
						s_p_i = key["sales_percent_increase"]
					w = ResponseTime.objects.create(response_time=key["response_time"], sales_percent_increase=s_p_i, search_date=datetime.datetime.now().date(), period_id=foreign)
				# g+=1
			s_p_i = 0	
		
		 
				
			
