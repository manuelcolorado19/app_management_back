from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sales.models import Product_Sale, Color, Product_Code, Pack, Pack_Products, Product_SKU_Ripley
from django.db.models import Q
from activities.models import Activity
from categories.models import Channel
wb = load_workbook('activities/files/Tareas Supervisor 25-02-2019.xlsx')
ws = wb['Hoja5']
count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		channel = Channel.objects.filter(name=row[2].value)[0] if Channel.objects.filter(name=row[2].value).count() > 0 else None
		user_responsable = '' if row[5].value == None else row[5].value
		detail = '' if row[4].value == None else row[4].value
		x_base = 0 if row[9].value == None else row[9].value
		y_variable = 0 if row[10].value == None else row[10].value
		activity = Activity.objects.create(
			hour=str(row[1].value)[:5],
			channel=channel,
			description=row[3].value,
			detail=row[4].value,
			user_responsable=user_responsable,
			x_base=x_base,
			y_variable=y_variable,
		)