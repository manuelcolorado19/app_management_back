from django.db import models
from django.contrib.auth import get_user_model
from categories.models import Channel
User = get_user_model()

class ReportSupervisor(models.Model):
	user_generated = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='user_generated_report')
	file_name = models.CharField(max_length=60, blank=True, null=True)
	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	class Meta:
		db_table = 'report_supervisor'

class Activity(models.Model):
	hour = models.CharField(max_length=6, null=True, blank=True)
	channel = models.ForeignKey(Channel, on_delete=models.SET_NULL, blank=True, null=True, related_name='channel_activity')
	description = models.CharField(max_length=170)
	detail = models.CharField(max_length=250, blank=True, null=True)
	user_responsable = models.CharField(max_length=30, blank=True, null=True)
	x_base = models.IntegerField(null=True)
	y_variable = models.IntegerField(null=True)
	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	class Meta:
		db_table = 'activity'

class Report_Activity(models.Model):
	activity = models.ForeignKey(Activity, on_delete=models.SET_NULL, blank=True, null=True, related_name='activity_report')
	report_supervisor = models.ForeignKey(ReportSupervisor, on_delete=models.SET_NULL, blank=True, null=True, related_name='report_activity')
	status = models.BooleanField(default=False)
	count = models.IntegerField(null=True)
	identified = models.IntegerField(null=True)
	work_time = models.IntegerField(null=True)
	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	class Meta:
		db_table = 'report_activity'
