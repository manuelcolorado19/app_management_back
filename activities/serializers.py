from .models import ReportSupervisor, Activity, Report_Activity
from rest_framework import serializers
from sales.serializers import ChannelName


class ActivitySerializer(serializers.ModelSerializer):
	channel_name = ChannelName(read_only=True, source='*')
	status = serializers.BooleanField(read_only=True, default=False)
	count = serializers.IntegerField(read_only=True, default=0)
	identified = serializers.IntegerField(read_only=True, default=0)
	class Meta:
		fields = '__all__'
		model = Activity
class DateSupervision(serializers.CharField):
	def to_representation(self, value):
		return value.date_created.date()

class ActivityQuantity(serializers.IntegerField):
	def to_representation(self, value):
		return value.report_activity.all().count()

class CompletedQuantity(serializers.IntegerField):
	def to_representation(self, value):
		return value.report_activity.filter(status=True).count()
		
class UncompletedQuantity(serializers.IntegerField):
	def to_representation(self, value):
		return value.report_activity.filter(status=False).count()

class ReportSupervisorSerializer(serializers.ModelSerializer):
	date_supervition = DateSupervision(read_only=True, source='*')
	activity_quantity = ActivityQuantity(read_only=True, source='*')
	completed = CompletedQuantity(read_only=True, source='*')
	uncompleted = UncompletedQuantity(read_only=True, source='*')
	class Meta:
		fields = '__all__'
		model = ReportSupervisor

class HourSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.activity.hour

class DescriptionSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.activity.description

class DetailSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.activity.detail

class UserResponsableSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.activity.user_responsable
class X_BaseSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.activity.x_base
class Y_VariableSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.activity.y_variable
class StatusActivity(serializers.CharField):
	def to_representation(self, value):
		if value.status:
			return 'yes'
		return 'no'

class Report_ActivitySerializer(serializers.ModelSerializer):
	hour = HourSerializer(read_only=True, source='*')
	channel_name = ChannelName(read_only=True, source='activity')
	description = DescriptionSerializer(read_only=True, source='*')
	detail = DetailSerializer(read_only=True, source='*')
	user_responsable = UserResponsableSerializer(read_only=True, source='*')
	x_base = X_BaseSerializer(read_only=True, source='*')
	y_variable = Y_VariableSerializer(read_only=True, source='*')
	status_activity = StatusActivity(read_only=True, source='*')
	class Meta:
		fields = '__all__'
		model = Report_Activity