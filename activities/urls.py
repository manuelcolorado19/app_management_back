from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import ActivityViewSet, ReportSupervisorViewSet, Report_ActivityViewSet

router = DefaultRouter()
router.register(r'activity', ActivityViewSet, base_name='activity')
router.register(r'report-supervisor', ReportSupervisorViewSet, base_name='report-supervisor')
router.register(r'report-activity', Report_ActivityViewSet, base_name='report-activity')

urlpatterns = [
	url(r'^', include(router.urls)),
	
]
