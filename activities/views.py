from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from .models import Activity, ReportSupervisor, Report_Activity
from django.http import HttpResponse
from categories.models import Channel
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import ActivitySerializer, ReportSupervisorSerializer, Report_ActivitySerializer
import datetime
from django.db import transaction
import xlwt
import xlsxwriter
import io
from datetime import date, timedelta
from django.db.models import F

today = datetime.datetime.now().date()

class ActivityViewSet(ModelViewSet):
	queryset = Activity.objects.all()
	serializer_class = ActivitySerializer
	permission_classes = (IsAuthenticated,)
	http_method_names  = ['get', 'post']

class ReportSupervisorViewSet(ModelViewSet):
	queryset = ReportSupervisor.objects.all()
	serializer_class = ReportSupervisorSerializer
	permission_classes = (IsAuthenticated,)
	http_method_names  = ['get', 'post']

class Report_ActivityViewSet(ModelViewSet):
	queryset = Report_Activity.objects.filter(date_created__gte=today).order_by('activity__hour')
	serializer_class = Report_ActivitySerializer
	pagination_class = None
	permission_classes = (IsAuthenticated,)
	http_method_names  = ['get', 'post']

	def get_queryset(self):
		today = datetime.datetime.now().date()
		return self.queryset.filter(date_created__date=today).order_by('activity__hour')

	@transaction.atomic
	def create(self, request):
		report = request.data['report_id']
		# print(request.data)
		for each in request.data['activities']:
			if each.get('id', None) is not None:
				status_act = True if each['status_activity'] == 'yes' else False
				count = int(each['count']) if each['count'] is not None else None
				report_activity = Report_Activity.objects.filter(id=each['id'])
				if count is not None and report_activity[0].activity.x_base is not None and report_activity[0].activity.y_variable is not None:
					work_time = (report_activity[0].activity.x_base + (report_activity[0].activity.y_variable * count))
				else:
					work_time = None
				report_activity.update(
					status=status_act,
					count=count,
					work_time=work_time,
				)
			else:
				if each['channel_name'] == '-':
					channel = None
				else:
					channel = Channel.objects.filter(name=each['channel_name'].title())[0]
				new_activity = Activity.objects.create(
					hour=each['hour'],
					channel=channel,
					description=each['description'],
					detail=each['detail'],
					user_responsable=each['user_responsable'],
					x_base=each['x_base'],
					y_variable=each['y_variable'],
				)
				status_act = True if each['status_activity'] == 'yes' else False
				count = int(each['count']) if each['count'] is not None else None
				if count is not None and new_activity.x_base is not None and new_activity.y_variable is not None:
					work_time = (new_activity.x_base + (new_activity.y_variable * count))
				else:
					work_time = None
				new_report_activity = Report_Activity.objects.create(
					activity=new_activity,
					report_supervisor=ReportSupervisor.objects.get(id=report),
					status=status_act,
					count=count,
					work_time=work_time,
				)
		return Response(data={"activity": ['Actividades actualizadas de forma exitosa']}, status=status.HTTP_200_OK)


class GenerateReportSupervisor(APIView):
	def get(self, request):
		# print(request.data)
		# Se crea el reporte
		today_date = datetime.datetime.now().date()
		output = io.BytesIO()
		file_name = 'Informe supervision.xlsx'
		wb =  xlsxwriter.Workbook(output)
		for sheet in range(3):
			if sheet == 0:
				sheet_name = 'Completas'
			# 	# header = 'Registro de actividades completas ' + str(today.time())
			if sheet == 1:
				sheet_name = 'Conteo' 
			# 	# header = 'Ordenes con Etiquetas ' + str(today.time())
			if sheet == 2:
				sheet_name = 'Horas trabajadas'
				# header = 'Ordenes con Etiquetas ' + str(today.time())
			ws = wb.add_worksheet(sheet_name)
			activities = Activity.objects.all().order_by('hour')
			columns = [{"name": 'Actividad', "width":80, "rotate": False}, {"name": 'Responsable', "width":15, "rotate": False} ]
			row_num = 0
			min_date = ReportSupervisor.objects.earliest('date_created').date_created.date()
			delta = today_date - min_date
			dates = 0
			cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
			cell_format_fields_90.set_center_across()
			cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
			cell_format_fields.set_center_across()
			cell_format_data = wb.add_format({'font_size': 9.5})
			for i in range(delta.days + 1):
				# columns.append(min_date + timedelta(i))
				columns.append({"name": (min_date + timedelta(i)), "width": 10, "rotate": True})
				dates += 1

			for col_num in range(len(columns)):
				if columns[col_num]['rotate']:
					ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
					ws.set_column(col_num, col_num, columns[col_num]['width'])
				else:
					ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
					ws.set_column(col_num, col_num, columns[col_num]['width'])
			row_num += 1
			for activity in activities:
				ws.write(row_num, 0, activity.description, cell_format_data)
				ws.write(row_num, 1, activity.user_responsable, cell_format_data)
				row_num += 1
			row_num = 0
			if sheet == 0:
				for row in activities:
					row_num += 1
					each_col = 2
					for each in row.activity_report.annotate(data=F('status')).values('data').order_by('date_created'):
						if each['data']:
							status = "Completa"
						else:
							status = "Incompleta" 
						ws.write(row_num, each_col, status, cell_format_data)
						each_col += 1
			if sheet == 1:
				for row in activities:
					row_num += 1
					each_col = 2
					for each in row.activity_report.annotate(data=F('count')).values('data').order_by('date_created'):
						ws.write(row_num, each_col, each['data'], cell_format_data)
						each_col += 1
			if sheet == 2:
				for row in activities:
					row_num += 1
					each_col = 2
					for each in row.activity_report.annotate(data=F('work_time')).values('data').order_by('date_created'):
						ws.write(row_num, each_col, str(each['data']) + " minutos", cell_format_data)
						each_col += 1

		# Se genera el Archivo PDF de supervisión
		wb.close()
		output.seek(0)
		filename = 'Informe supervision.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		# print("--- %s seconds ---" % (time.time() - start_time))
		return response

		# return Response(data={"report": ['Reporte generado exitosamente']}, status=status.HTTP_200_OK)
