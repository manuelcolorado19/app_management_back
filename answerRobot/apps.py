from django.apps import AppConfig


class AnswerrobotConfig(AppConfig):
    name = 'answerRobot'
