from openpyxl import load_workbook
from answerRobot.models import Word, Intention, IntentionWords, Response
from notifications.models import PublicationDataMercadolibre, Question

wb = load_workbook('answerRobot/files/Lista Preguntas Mercadolibre conteo.xlsx')
ws = wb['Conteo Palabras']


count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		word = Word.objects.filter(text_word=row[0].value.lower())
		if word:
			continue
		else:
			Word.objects.create(
				text_word=row[0].value.lower(),
				category_word='key_word',
			)

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		intention = Intention.objects.filter(text_intention=row[2].value.lower())
		if intention:
			continue
		else:
			Intention.objects.create(
				text_intention=row[2].value.lower(),
			)

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		intention_word = IntentionWords.objects.filter(word__text_word=row[0].value.lower(),intention__text_intention=row[2].value.lower())
		if intention_word:
			continue
		else:
			word = Word.objects.get(text_word=row[0].value.lower())
			intention = Intention.objects.get(text_intention=row[2].value.lower())
			IntentionWords.objects.create(
				word=word,
				intention=intention
			)

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		intention = Intention.objects.get(text_intention=row[2].value.lower())
		if Response.objects.filter(
				intention=intention
			):
			continue
		else:
			if row[4].value == 0:
				negative_answer = None
			else:
				negative_answer = row[4].value.lower()
			# print(negative_answer)
			response = Response.objects.create(
				intention=intention,
				positive_answer=row[3].value.lower(),
				negative_answer=negative_answer,
			)