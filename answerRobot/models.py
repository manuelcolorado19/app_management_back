from django.db import models
from notifications.models import PublicationDataMercadolibre, Question

class Word(models.Model):
	CATEGORY_WORD = (
		('key_word', 'key_word'),
		('complementary_word', 'complementary_word'),
		('other_word', 'other_word'),
	)

	text_word = models.CharField(max_length=30, null=True, blank=True)
	category_word = models.CharField(max_length=100, choices=CATEGORY_WORD)
	date_created = models.DateTimeField(auto_now_add=True)
	publication_asociated = models.ManyToManyField(PublicationDataMercadolibre)

	class Meta:
		db_table = 'robot_words'

class Synonyms(models.Model):
	text_synonym = models.CharField(max_length=30, null=True, blank=True)
	word = models.ForeignKey(Word, null=True, blank=True, related_name='synonym_word', on_delete=models.SET_NULL)
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'robot_synonyms'

class WrongWords(models.Model):
	text_wrong = models.CharField(max_length=30, null=True, blank=True)
	word = models.ForeignKey(Word, null=True, blank=True, related_name='wrong_word', on_delete=models.SET_NULL)
	synonym = models.ForeignKey(Synonyms, null=True, blank=True, related_name='synonym_word', on_delete=models.SET_NULL)
	date_created = models.DateTimeField(auto_now_add=True)
	class Meta:
		db_table = 'robot_wrong_words'

class Intention(models.Model):
	text_intention = models.CharField(max_length=100, null=True, blank=True)
	date_created = models.DateTimeField(auto_now_add=True)
	
	class Meta:
		db_table = 'robot_intention'

class Response(models.Model):
	intention = models.OneToOneField(Intention, null=True, blank=True, related_name='intention_response', on_delete=models.SET_NULL)
	positive_answer = models.CharField(max_length=200, null=True, blank=True)
	negative_answer = models.CharField(max_length=200, null=True, blank=True)
	condition_text = models.TextField(null=True)
	date_created = models.DateTimeField(auto_now_add=True)
	class Meta:
		db_table = 'robot_response'

class Aceptance(models.Model):
	response = models.ForeignKey(Response, null=True, blank=True, related_name='response_aceptance', on_delete=models.SET_NULL)
	percentage_aceptance = models.IntegerField(null=True)
	question_asociated = models.OneToOneField(Question, null=True, blank=True, on_delete=models.SET_NULL, related_name='question_aceptance')
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'robot_aceptance'

class IntentionWords(models.Model):
	word = models.ForeignKey(Word, null=True, blank=True, related_name='intention_word', on_delete=models.SET_NULL)
	intention = models.ForeignKey(Intention, null=True, blank=True, related_name='word_intention', on_delete=models.SET_NULL)
	date_created = models.DateTimeField(auto_now_add=True) 
	class Meta:
		db_table = 'robot_intention_words'

