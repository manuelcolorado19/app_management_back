import nltk
import numpy as np
import random
import string
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from openpyxl import load_workbook
import re
from notifications.models import PublicationDataMercadolibre
data_questions = ""
wb = load_workbook('answerRobot/files/Lista Preguntas Mercadolibre.xlsx')
ws = wb['Preguntas']
count = 0
# print(len(ws.rows))
for row in ws.rows:
	count += 1
	if count > 1:
		data_questions += row[2].value + "\n"
# print(len(data_questions))

f=open('answerRobot/files/chatbot2.txt','r',errors = 'ignore')
raw=data_questions
raw=raw.lower()
sent_tokens = nltk.sent_tokenize(raw)
word_tokens = nltk.word_tokenize(raw)
lemmer = nltk.stem.WordNetLemmatizer()
def LemTokens(tokens):
	return [lemmer.lemmatize(token) for token in tokens]
remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

def LemNormalize(text):
	return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up","hey",)
GREETING_RESPONSES = ["hi", "hey", "*nods*", "hi there", "hello", "I am glad! You are talking to me"]
def greeting(sentence):
 
    for word in sentence.split():
        if word.lower() in GREETING_INPUTS:
            return random.choice(GREETING_RESPONSES)
def response(user_response):
	robo_response=''
	sent_tokens.append(user_response)
	TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english')
	tfidf = TfidfVec.fit_transform(sent_tokens)
	vals = cosine_similarity(tfidf[-1], tfidf)
	print(vals.argsort()[0])
	idx=vals.argsort()[0][-2]
	flat = vals.flatten()
	flat.sort()
	req_tfidf = flat[-2]
	print(flat)
	if(req_tfidf==0):
		robo_response=robo_response+"I am sorry! I don't understand you"
		return robo_response
	else:
		count = 0
		for row in ws.rows:
			if count > 1:
				if count == idx + 1:
					sku_data = PublicationDataMercadolibre.objects.filter(mlc=row[0].value)
					if sku_data:
						sku = sku_data[0].custom_field
					number = re.findall(r'\d+', user_response)
					if number:
						number = int(number[0])
						if number > int(row[3].value):
							return robo_response + "Lo siento no tenemos esa cantidad de stock Atte. Cluboferta"
					robo_response = robo_response + row[7].value + " Atte. Cluboferta"
					return robo_response
			count += 1
		# robo_response = robo_response+sent_tokens[idx]
		# return robo_response

# flag=True
# print("ROBO: My name is Robo. I will answer your queries about Chatbots. If you want to exit, type Bye!")

# while(flag==True):
# 	user_response = input()
# 	user_response=user_response.lower()
# 	if(user_response!='bye'):
# 		if(user_response=='thanks' or user_response=='thank you' ):
# 			flag=False
# 			print("ROBO: You are welcome..")
# 		else:
# 			if(greeting(user_response)!=None):
# 				print("ROBO: "+greeting(user_response))
# 			else:
# 				print("ROBO: ",end="")
# 				print(response(user_response))
# 				sent_tokens.remove(user_response)
# 	else:
# 		flag=False
# 		print("ROBO: Bye! take care..")

import xlsxwriter
wb2 =  xlsxwriter.Workbook(",")
ws3 = wb.add_worksheet("Palabras")
count = 0
key_words = []
wb = load_workbook('answerRobot/files/Lista Preguntas Mercadolibre conteo.xlsx')
ws = wb['Intenciones']
ws2 = wb['Conteo Palabras']
for row in ws2.rows:
	count += 1
	if count > 1:
		if not row[0].value.lower() in key_words:
			key_words.append(row[0].value.lower())
count = 0
row_num = 1
for row2 in ws.rows:
	count += 1
	row_num += 1
	if count > 1:
		key_question = ""
		for word in key_words:
			if word in row[2].value:
				key_question += str(word) + ","

		ws3.write(row_num, 0, str(row[2].value))
		ws3.write(row_num, 1, str(key_question))
wb2.close()




