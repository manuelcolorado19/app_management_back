from openpyxl import load_workbook
from notifications.models import Token, PublicationDataMercadolibre
from sales.models import Product_Sale
from operator import itemgetter
from nltk import sent_tokenize
from answerRobot.models import Word, Intention, Response, IntentionWords
from sales.models import Color, Product_Sale
import re
import string
import nltk
import numpy as np
import random
from sklearn.feature_extraction.text import TfidfVectorizer
import requests
from sklearn.metrics.pairwise import cosine_similarity
import xlsxwriter

token = Token.objects.get(id=1).token_ml


def clean_text_round1(text):
    '''Make text lowercase, remove text in square brackets, remove punctuation and remove words containing numbers.'''
    text = text.lower()
    text = re.sub('\[.*?¿\]\%', ' ', text)
    text = re.sub('[%s]' % re.escape(string.punctuation), ' ', text)
    text = re.sub('\w*\d\w*', '', text)
    return text

key_words = Word.objects.filter(category_word='key_word').values_list('text_word', flat=True)

preguntas = [""]
count = 0

# print("Ingresa el dato: ")
# user_response = input()
# user_response=user_response.lower()


def generate_response(question, mlc):
	mlc = mlc
	api_data = requests.get("https://api.mercadolibre.com/items/" + mlc + "?access_token=" + token).json()
	formater_data = []
	question_example = question.lower()
	all_sentences = sent_tokenize(question_example)
	for r in all_sentences:
		if len(r) < 10:
			continue
		punctuation = []
		others = []
		keys = []
		numbers = []
		words_string = r.split()
		for each_word in words_string:
			if each_word.isdigit():
				numbers.append(int(each_word))
		words_string2 = "".join((char if char.isalpha() else " ") for char in r).split()
			
		for word in words_string2:
			if word.lower() in key_words and (not word.lower() in keys):
				keys.append(word)
			else:
				others.append(word)

		formater_data.append({"question": r, "key_words": keys, "number":numbers})
	# print(formater_data)
	response_completed = ""
	for each in formater_data:
		if each['key_words']:
			# a = response(each)
			a = None
			if a is not None:
				response_completed += a + ". "
			else:
				# Ir al otro metodo para crear respuesta para sugerir al humano
				intentions = Intention.objects.filter(word_intention__word__text_word__in=each['key_words']).distinct()
				intention_selected = None
				to_select = []
				for i in intentions:
					for key in each['key_words']:
						count_word_intention = IntentionWords.objects.filter(word__text_word=key, intention=i)
						if count_word_intention.count() == len(each['key_words']):
							intention_selected = i
						else:
							percentage_precition = (count_word_intention.count() * 100)/len(each['key_words'])
							to_select.append({"intention": i, "key": [key,], "percentage_precition": percentage_precition})
					if intention_selected is not None:
						break
					else:
						# Anadir las combinaciones entre palabras y filtrar por intenciones para añadir porcentajes
						for w in each['key_words']:
							for y in each['key_words']:
								new_key = list(set([w,y]))
								if len(new_key) == 1:
									continue
								# print(new_key)
								count_word_intention = IntentionWords.objects.filter(word__text_word__in=new_key, intention=i)
								percentage_precition = (count_word_intention.count() * 100)/len(each['key_words'])
								to_select.append({"intention": i, "key": new_key, "percentage_precition": percentage_precition})
				to_select_remove_duplicates = sorted([i for n, i in enumerate(to_select) if i not in to_select[n + 1:]], key=itemgetter('percentage_precition'), reverse=True)
				if intention_selected == None:
					if to_select_remove_duplicates[0]['percentage_precition'] == 100:
						intetions_selected = [to_select_remove_duplicates[0]['intention']]
					else:
						intetions_selected = []
						for intention in range(len(to_select_remove_duplicates) - 1):
							if to_select_remove_duplicates[intention]['percentage_precition'] >= (100/len(each['key_words'])):
								if to_select_remove_duplicates[intention]['intention'] in intetions_selected:
									continue
								else:
									intetions_selected.append(to_select_remove_duplicates[intention]['intention'])
				else:
					intetions_selected = [intention_selected]

				# Concatenación y condicion de respuesta basado en petición de API
				response_string = ''
				for selected in intetions_selected:
					if selected.intention_response.negative_answer == None and (not selected.id in [6,7]):
						response_string += selected.intention_response.positive_answer + " "
					else:
						# Llamar a API y decidir respuesta
						if selected.id == 2:
							# Stock disponible Integrar llamado de variantes de colores si aplica.
							color = Color.objects.filter(description__in=[i.upper() for i in each['key_words']])
							# print(color)
							if color:
								# Busqueda en api por stock en colores
								for variation in api_data['variations']:
									if variation['attribute_combinations'][0]['value_name'][5:].upper() == color[0].description:
										if variation['available_quantity'] > 0:
											response_string += selected.intention_response.positive_answer + " "
											break
										else:
											response_string += selected.intention_response.negative_answer + " "
											break

							else:
								if api_data['available_quantity'] > 0:
									response_string += selected.intention_response.positive_answer + " "
								else:
									response_string += selected.intention_response.negative_answer + " "
						elif selected.id == 5:
							# Envío gratis
							if api_data['shipping']['free_shipping']:
								response_string += selected.intention_response.positive_answer + " "
							else:
								response_string += selected.intention_response.negative_answer + " "
						elif selected.id == 6:
							# Medidas del producto
							publication_data = PublicationDataMercadolibre.objects.filter(mlc=mlc, custom_field__isnull=False)
							if publication_data:
								product_data = Product_Sale.objects.filter(sku=publication_data[0].custom_field[:5])
								if product_data:
									response_string += selected.intention_response.positive_answer.replace("X", str(product_data[0].height)).replace("Y", str(product_data[0].width)).replace("Z", str(product_data[0].length)).replace("W", str(product_data[0].weight))
								else:
									response_string += "no tengo datos del producto preguntado."
							else:
								response_string += "no tengo datos del producto preguntado."
						pass

				response_completed += response_string + " "
		else:
			# Ir al humano directamente
			return "pregunta sin palabras claves"
	return response_completed + "Saludos."

# print(formater_data)
wb2 =  xlsxwriter.Workbook("Respuesta Mercadolibre.xlsx")
ws2 = wb2.add_worksheet("Respuestas")
wb = load_workbook('answerRobot/files/Lista Preguntas Mercadolibre conteo.xlsx')
ws = wb['Intenciones']
count = 0
row_num = 0
# print(ws.rows)
for row in ws.rows:
	count += 1
	if count > 1:
		print(count)
		generate_response_ = generate_response(row[2].value, row[0].value)
		ws2.write(row_num, 0, generate_response_)
		row_num += 1

wb2.close()


        
# f.close()
# f2=open("answerRobot/files/Preguntas Mercadolibre.txt","r", errors = 'ignore')
# raw=f2.read()
# raw=raw.lower()
# f2.close()
# sent_tokens = nltk.sent_tokenize(raw)
# word_tokens = nltk.word_tokenize(raw)
# lemmer = nltk.stem.WordNetLemmatizer()
# def LemTokens(tokens):
# 	return [lemmer.lemmatize(token) for token in tokens]
# remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

# def LemNormalize(text):
# 	return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

# def response(user_response):
# 	robo_response=''
# 	sent_tokens.append(user_response['question'])
# 	TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english')
# 	tfidf = TfidfVec.fit_transform(sent_tokens)
# 	vals = cosine_similarity(tfidf[-1], tfidf)
# 	idx=vals.argsort()[0][-2]
# 	flat = vals.flatten()
# 	flat.sort()
# 	req_tfidf = flat[-2]
# 	question_selected = None
# 	for i in range(len(vals.argsort()[0]) - 2, 0, -1):
# 		apearance = 0
# 		for key in user_response['key_words']:
# 			if key in sent_tokens[i]:
# 				apearance += 1
# 		if apearance == len(user_response['key_words']):
# 			question_selected = i
# 			break
# 	if question_selected is not None:
# 		answer_posible = None
# 		for each_selected in sent_tokens[question_selected].splitlines():
# 			apearance = 0
# 			for key in user_response['key_words']:
# 				if key in each_selected:
# 					apearance += 1
# 			if apearance == len(user_response['key_words']):
# 				answer_posible = each_selected
# 				break
# 		count = 0
# 		response_posible = None
# 		for row in ws.rows:
# 			count += 1
# 			if count > 1:
# 				if answer_posible == row[2].value.lower() + ".":
# 					response_posible = row[8].value
# 					break 
# 					# print(row[8].value)
# 		if response_posible is not None:
# 			return robo_response + response_posible
# 		else:
# 			return None
# 	else:
# 		return None

# print(response_completed + "Saludos.")
