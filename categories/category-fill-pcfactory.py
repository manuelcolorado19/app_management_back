from categories.models import SubCategories, Category, Channel
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from bs4 import SoupStrainer
from datetime import datetime as dt
import datetime as date_time
channel = Channel.objects.get(name="Pcfactory")

page_response = requests.get("https://www.pcfactory.cl/", timeout=10)
content_categories = SoupStrainer(id='drop-down')
page_content = BeautifulSoup(page_response.content, "html.parser", parse_only=content_categories)
content_all_categories = page_content.find('ul', attrs={"id": "menu-principal"})

for li in content_all_categories.find_all('li', attrs={"data-menu-path": "Inicio"}):
	category_name = " ".join(li.find('h3', attrs={"class": "menu"}).text.split())
	code = li.find_all('div', attrs={"class": "sub-menu"})[0]['data-menu-categoria']
	new_category = Category.objects.create(
		name=category_name,
		channel=channel,
		code=code,
		total_items=0
	)
	print("New Category")
	sub_menu =  li.find('div', attrs={"class": "sub-menu"})
	for ul in sub_menu.find_all('ul', attrs={"class": "columna-1"}):
		for li2 in ul.find_all('li', recursive=False):
			if(li2.find('li', attrs={'class':"subCategoria"})):
				# name_sub_category = li2.find('h2').find('a', attrs={"class": "categoria"}).text
				# code_sub_category = li2['data-menu-categoria']
				# url_sub_category = li2.find('h2').find_all('a', attrs={"class": "categoria"})[0]['href']
				# new_sub_category = SubCategories.objects.create(
				#     name=name_sub_category,
				#     code=code_sub_category,
				#     category=new_category,
				#     total_items=0,
				#     url_sub_category= "https://www.pcfactory.cl" + url_sub_category
				# )
				# print("New Subcategory")

				for li3 in li2.find_all('li'):
					name_sub_category = li3.find('a', attrs={"class": "categoria"}).text.replace('-', '').lstrip()
					code_sub_category = li3['data-menu-categoria']
					url_sub_category = li3.find_all('a', attrs={"class": "categoria"})[0]['href']
					new_sub_category = SubCategories.objects.create(
						name=name_sub_category,
						code=code_sub_category,
						category=new_category,
						total_items=0,
				    	url_sub_category= "https://www.pcfactory.cl" + url_sub_category
					)
					print("New Subcategory")
			else:
				name_sub_category = li2.find('a', attrs={"class": "categoria"}).text
				code_sub_category = li2['data-menu-categoria']
				url_sub_category = li2.find('h2').find_all('a', attrs={"class": "categoria"})[0]['href']
				new_sub_category = SubCategories.objects.create(
					name=name_sub_category,
					code=code_sub_category,
					category=new_category,
					total_items=0,
			    	url_sub_category= "https://www.pcfactory.cl" + url_sub_category
				)
				print("New Subcategory")

				