import requests
from categories.models import Channel, Category, SubCategories
categories = requests.get("https://api.mercadolibre.com/sites/MLC/categories", timeout=10).json()
channel = Channel.objects.get(name="Mercadolibre")
for category in  categories:
    category_and_subcategories = requests.get("https://api.mercadolibre.com/categories/" + category['id']).json()
    new_category = Category.objects.create(
        name=category_and_subcategories['name'],
        channel=channel,
        code=category_and_subcategories['id'],
        total_items=category_and_subcategories['total_items_in_this_category']
    )
    for sub_category in category_and_subcategories['children_categories']:
        new_sub_category = SubCategories.objects.create(
            name=sub_category['name'],
            code=sub_category['id'],
            category = new_category,
            total_items=sub_category['total_items_in_this_category'],
            url_sub_category='https://api.mercadolibre.com/sites/MLC/search?category=' + sub_category['id']
        )
print("Categorys created")
