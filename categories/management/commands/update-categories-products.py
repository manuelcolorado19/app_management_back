from django.core.management.base import BaseCommand, CommandError
from products.models import Product
import os
from time import time
import argparse
import asyncio
import sys
from aiohttp import ClientSession
import json
from datetime import datetime as dt
from sellers.models import Seller
from products.models import Product
from categories.models import Channel
from datetime import timedelta
import time as ti_me
from notifications.models import Token
from django.db.models import Q
all_products = Product.objects.filter(channel__name="Mercadolibre").filter(Q(last_category__isnull=True) | Q(seller_id_ml__isnull=True)).values_list('code', flat=True).distinct()[:3000]
token = Token.objects.get(id=1).token_ml
start_time = time()
sellers_bulk = []


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		print(len(all_products))
		if all_products:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()
		# print(len(repeated_remove))
async def run(session, number_of_requests, concurrent_limit):
	base_url = "https://api.mercadolibre.com/items/{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		url = base_url.format(i)
		async with session.get(url) as response:
			response = await response.read()
			sem.release()
			responses.append(response)
			data_json = json.loads(response)
			# Se añade el seller a la base
			print(data_json['category_id'])
			Product.objects.filter(code=i).update(last_category=data_json['category_id'], seller_id_ml=data_json['seller_id'])
			
			return response

	for i in all_products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	# print("total_sellers_append: {}".format(len(sellers_bulk)))
	print("--- %s seconds ---" % (time() - start_time))

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return