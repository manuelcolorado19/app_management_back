from django.db import models

class Channel(models.Model):
    name = models.CharField(max_length=50)
    web_page = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'channel'

class Category(models.Model):
    name = models.CharField(max_length=150)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    code = models.CharField(max_length=25)
    total_items = models.IntegerField()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'category'

class SubCategories(models.Model):
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=25)
    total_items = models.IntegerField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    url_sub_category = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'sub-categories'
