from notifications.models import CustomMessageProduct
from sales.models import Product_Sale, Pack

products = Product_Sale.objects.all()
packs = Pack.objects.all()

for product in products:
	if CustomMessageProduct.objects.filter(product__sku=product.sku):
		continue
	else:
		CustomMessageProduct.objects.create(
			message='',
			product=product,
		)

for pack in packs:
	if CustomMessageProduct.objects.filter(pack__sku_pack=pack.sku_pack):
		continue
	else:
		CustomMessageProduct.objects.create(
			message='',
			pack=pack,
		)

print("Messages created")