from notifications.models import Token, PublicationDataMercadolibre
import requests
from sellers.models import Seller
token = Token.objects.get(id=1).token_ml
url = "https://api.mercadolibre.com/sites/MLC/search?seller_id=216244489&access_token=" + token

data = requests.get(url).json()
if data['paging']['total'] <= 50:
	rows = data["results"]
else:
	rows = []
	count = 0
	iteration_end = int(data['paging']['total']/50)
	module = int(data['paging']['total']%50)
	for i in range(iteration_end + 1):
		print(i)
		new_rows = requests.get(url + "&offset=" + str(i*50)).json()['results']
		rows = rows + new_rows
	rows = rows + requests.get(url + "&offset="+str(((iteration_end*50) + module))).json()['results'] 
all_data = []
for row in rows:
	if PublicationDataMercadolibre.objects.filter(mlc=row['id']):
		continue
	else:
		new_data = PublicationDataMercadolibre.objects.create(
			title=row['title'],
			link_publication=row['permalink'],
			img_first=row['thumbnail'],
			seller=Seller.objects.get(id_ml=row['seller']['id']),
			mlc=row['id'],
		)
# token = Token.objects.get(id=1).token_ml
url = "https://api.mercadolibre.com/items/"
all_publications = PublicationDataMercadolibre.objects.filter(custom_field__isnull=True)
not_sku = []
for i in all_publications:
	data = requests.get(url + i.mlc + "?access_token=" + token).json()
	PublicationDataMercadolibre.objects.filter(id=i.id).update(custom_field=data.get('seller_custom_field', None))
	if not data['variations']:
		not_sku.append(i.mlc)
	print("sku added")
print("finish")
# print(len(all_data))
