from notifications.models import Token, PublicationDataMercadolibre
import requests
from sellers.models import Seller
token = Token.objects.get(id=1).token_ml
url = "https://api.mercadolibre.com/items/"
all_publications = PublicationDataMercadolibre.objects.filter(custom_field__isnull=True)
not_sku = []
for i in all_publications:
	data = requests.get(url + i.mlc + "?access_token=" + token).json()
	PublicationDataMercadolibre.objects.filter(id=i.id).update(custom_field=data['seller_custom_field'])
	if not data['variations']:
		not_sku.append(i.mlc)
	print("sku added")
print("finish")
