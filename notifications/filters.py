from .models import CustomMessageProduct, Question
import django_filters

class CustomMessageProductFilter(django_filters.FilterSet):
    pack = django_filters.BooleanFilter(field_name='pack', lookup_expr='isnull', exclude=True)
    class Meta:

        model = CustomMessageProduct
        fields = '__all__'

class QuestionFilter(django_filters.FilterSet):
    suggested_answer = django_filters.BooleanFilter(field_name='suggested_answer', lookup_expr='isnull', exclude=True)
    answer_question__is_robot = django_filters.BooleanFilter(field_name='answer_question__is_robot', exclude=False)
    class Meta:

        model = Question
        fields = ('suggested_answer', 'is_answer', 'answer_question__is_robot', 'status_question')
