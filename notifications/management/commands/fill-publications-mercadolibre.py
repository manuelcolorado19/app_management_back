from django.core.management.base import BaseCommand, CommandError
from notifications.models import Token, PublicationDataMercadolibre
import requests
from sellers.models import Seller
class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		url = "https://api.mercadolibre.com/items/"
		token = Token.objects.get(id=1).token_ml
		url = "https://api.mercadolibre.com/sites/MLC/search?seller_id=216244489&access_token=" + token

		data = requests.get(url).json()
		if data['paging']['total'] <= 50:
			rows = data["results"]
		else:
			rows = []
			count = 0
			iteration_end = int(data['paging']['total']/50)
			module = int(data['paging']['total']%50)
			for i in range(iteration_end + 1):
				# print(i)
				new_rows = requests.get(url + "&offset=" + str(i*50)).json()['results']
				rows = rows + new_rows
			rows = rows + requests.get(url + "&offset="+str(((iteration_end*50) + module))).json()['results'] 
		all_data = []
		for row in rows:
			if PublicationDataMercadolibre.objects.filter(mlc=row['id']):
				#new_img = requests.get(url + row['id'] + "?access_token=" + token).json().get('pictures', None)
				#if new_img is not None and len(new_img) > 0:
				#	img_publication = new_img[0]['url']
				#	PublicationDataMercadolibre.objects.filter(mlc=row['id']).update(img_first=img_publication)
				continue
			else:
				new_img = requests.get(url + row['id'] + "?access_token=" + token).json().get('pictures', None)
				if new_img is not None and len(new_img) > 0:
					img_publication = new_img[0]['url']
				# continue
				new_data = PublicationDataMercadolibre.objects.create(
					title=row['title'],
					link_publication=row['permalink'],
					img_first=img_publication,
					seller=Seller.objects.get(id_ml=row['seller']['id']),
					mlc=row['id'],
				)
		# print("finish 1")
		
		all_publications = PublicationDataMercadolibre.objects.filter(custom_field__isnull=True)
		not_sku = []
		for i in all_publications:
			data = requests.get(url + i.mlc + "?access_token=" + token).json()
			PublicationDataMercadolibre.objects.filter(id=i.id).update(custom_field=data.get('seller_custom_field', None))
			#if not data['variations']:
			#	not_sku.append(i.mlc)
			# print("sku added")
		print("finish 2")
