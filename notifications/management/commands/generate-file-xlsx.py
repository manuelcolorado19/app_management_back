import argparse
import asyncio
import sys
from django.core.management.base import BaseCommand, CommandError
# import resource
from aiohttp import ClientSession
from functools import partial
from time import time
from bs4 import BeautifulSoup, SoupStrainer
import json
import requests
from multiprocessing.dummy import Pool as ThreadPool
from operator import itemgetter
from notifications.models import Token
import xlsxwriter
wb =  xlsxwriter.Workbook("Lista Preguntas Mercadolibre.xlsx")
ws = wb.add_worksheet("Preguntas")

token = Token.objects.get(id=1).token_ml
link_append = []
products = []
with_problem = []
start_time = time()
data_question = []
with open("LinksQuestions.txt") as f:
	content = f.readlines()
	for i in content:
		link_append.append(i.replace("\n", ""))

async def run(session, number_of_requests, concurrent_limit):
	base_url = "{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		url = base_url.format(i) + "&access_token=" + token
		async with session.get(url) as response:
			response = await response.read()
			sem.release()
			responses.append(response)
			data_json = json.loads(response)
			# print(len(data_json['results']))
			# try:
			for each in data_json['questions']:
				if each.get('answer', None) is not None:
					answer = each['answer']['text']
					date_created = each['answer']['date_created']
				else:
					answer = ''
					date_created = ''
				data_question.append({
						"MLC":each['item_id'],
						"question": each['text'],
						"date_question": each['date_created'],
						"answer": answer,
						"date_answer": date_created,
					})
			# except Exception as e:
				# with_problem.append(i)
			# print("request made")
			return response

	for i in link_append:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)

	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	print("total_data: {}".format(len(data_question)))
	
	print("--- %s seconds ---" % (time() - start_time))

	return responses

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		# print("empezo")
		loop = asyncio.get_event_loop()
		loop.run_until_complete(main(10000, 10000))
		loop.close()
		ws.write(0, 0, "MLC")
		ws.write(0, 1, "Título")
		ws.write(0, 2, "Pregunta")
		ws.write(0, 3, "Fecha De pregunta")
		ws.write(0, 4, "Respuesta")
		ws.write(0, 5, "Fecha de Respuesta")
		row_num = 1
		for e in data_question:
			ws.write(row_num, 0, str(e['MLC']))
			ws.write(row_num, 2, str(e['question']))
			ws.write(row_num, 3, str(e['date_question']))
			ws.write(row_num, 4, str(e['answer']))
			ws.write(row_num, 5, str(e['date_answer']))
			row_num +=1
		wb.close()
		

		# print()