from notifications.models import Token
import requests
import json
import argparse
from aiohttp import ClientSession
import asyncio
import sys
from OperationManagement.settings import CLIENT_ID, CLIENT_SECRET
from time import time
from django.core.management.base import BaseCommand, CommandError
token = Token.objects.get(id=1).token_ml
url_mlc = "https://api.mercadolibre.com/sites/MLC/search?seller_id=216244489&access_token=" + token
links = []
start_time = time()
data = requests.get(url_mlc).json()
if data['paging']['total'] <= 50:
	rows = data["results"]
else:
	rows = []
	count = 0
	iteration_end = int(data['paging']['total']/50)
	module = int(data['paging']['total']%50)
	for i in range(iteration_end + 1):
		new_rows = requests.get(url_mlc + "&offset=" + str(i*50)).json()['results']
		rows = rows + new_rows
	rows = rows + requests.get(url_mlc + "&offset="+str(((iteration_end*50) + module))).json()['results']

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		
		print(len(rows))
		loop = asyncio.get_event_loop()
		loop.run_until_complete(main(10000, 10000))
		loop.close()

async def run(session, number_of_requests, concurrent_limit):
	base_url = "https://api.mercadolibre.com/questions/search?item={}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		url = base_url.format(i) + "&access_token=" + token
		async with session.get(url) as response:
			response = await response.read()
			sem.release()
			responses.append(response)
			data_json = json.loads(response)
			# print(data_json['total'])
			if data_json['total'] <= 50:
			# print(len(data_json['results']))
				links.append(base_url.format(i))
			else:
				iteration_end = int(data_json['total']/50)
				module = int(data_json['total']%50)
				for y in range(iteration_end + 1):
					links.append(base_url.format(i) + "&offset=" + str(y*50))
			# print("request made")
			return response

	for i in rows:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i['id']))
		task.add_done_callback(tasks.remove)
		tasks.append(task)

	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	print("total_links: {}".format(len(links)))
	f= open("LinksQuestions.txt","w+")
	for j in links:
		f.write("%s\r" % (j))
	f.close()
	print("--- %s seconds ---" % (time() - start_time))

	return responses

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return

