from notifications.models import Token
import requests
import json
from OperationManagement.settings import CLIENT_ID, CLIENT_SECRET
from django.core.management.base import BaseCommand, CommandError

url_auth = "https://api.mercadolibre.com/oauth/token"

data_for_token = {
	"grant_type":"client_credentials",
	"client_id":CLIENT_ID,
	"client_secret":CLIENT_SECRET
}

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		new_token = requests.post(url_auth, data=json.dumps(data_for_token)).json()
		token = new_token['access_token']
		query_token = Token.objects.filter(id=1)
		if query_token:
			query_token.update(token_ml=token)
		else:
			new_register = Token.objects.create(
				token_ml=token
			)
		# print("Token generate or reload")
