from django.core.management.base import BaseCommand, CommandError
from sales.models import Sale
import datetime
from OperationManagement.settings import API_KEY_RP, API_KEY_LINIO, USER_ID
import requests
import urllib
from hashlib import sha256
from hmac import HMAC
from notifications.models import Token
from datetime import timedelta

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def status_shipping(self, x):
		return {
			'paid': "Pagado",
			'cancelled': "Cancelado",
			'confirmed': "Confirmado",
			'payment_in_process': "Pago en proceso",
			'payment_required': "Pago requerido",
			'invalid': "Invalido",
		}[x]
	def status_ripley(self, x):
		return {
			'RECEIVED': "Recibido",
			'CLOSED': "Cerrado",
			'SHIPPED': "Enviado",
			'SHIPPING': "Enviando",
			'WAITING_DEBIT_PAYMENT': "Pago de débito en espera",
			'WAITING_ACCEPTANCE': "Esperando aceptación",
			'STAGING': "En Andamiaje",
			'CANCELED': "Cancelada",
		}[x]

	def status_linio(self, x):
		if type(x) == list:
			status = x[0]
		else:
			status = x
		return {
	        'pending': "Pendiente",
	        'canceled': "Cancelado",
	        'ready_to_ship': "Listo para Enviar",
	        'delivered': "Entregado",
	        'returned': "Devuelto",
	        'shipped': "Enviado",
	        'failed': "Fallido",
			'return_shipped_by_customer': "Devolución enviada por comprador",
			'return_waiting_for_approval': "Devolución esperando por aprobación",
	    }[status]

	def update_mercadolibre(self, sales):
		token = Token.objects.get(id=1).token_ml
		url_request = "https://api.mercadolibre.com/orders/"
		access_token = "?access_token="+token
		for i in sales:
			data_sale = requests.get(url_request + str(i.order_channel) + access_token).json()
			status_channel = self.status_shipping(data_sale['status'])
			if i.channel_status != status_channel:
				# print(status_channel)
				q = Sale.objects.filter(id=i.id).update(channel_status=status_channel)

	def update_ripley(self, sales):
		url_request = "https://ripley-prod.mirakl.net/api/orders"
		headers = {'Accept': 'application/json', 'Authorization': API_KEY_RP}
		date_from = (sales[0].date_sale + timedelta(days=-1)).strftime("%Y-%m-%dT00:%M:%SZ")
		date_to = (sales[sales.count() - 1].date_sale + timedelta(days=1)).strftime("%Y-%m-%dT00:%M:%SZ")
		url_rp = url_request + "?start_date="+ date_from +"&end_date="+ date_to + "&paginate=false"
		sales_ripley = requests.get(url_rp, headers=headers).json()['orders']
		print(len(sales_ripley))
		for i in sales:
			sale_selected = None
			for each in sales_ripley:
				if str(i.order_channel) == each['commercial_id']:
					# print(i.order_channel)
					sale_selected = each
					break
			if sale_selected is not None:
				status_sale = self.status_ripley(sale_selected['order_state'])
				# if status_sale != i.shipping_status:
					# print(status_sale)
				q = Sale.objects.filter(id=i.id).update(channel_status=status_sale)

	def update_linio(self, sales):
		url_linio = "https://sellercenter-api.linio.cl?"
		parameters = {
		  'UserID': USER_ID,
		  'Version': '1.0',
		  'Action': 'GetOrders',
		  'Format':'JSON',
		  'Timestamp':datetime.datetime.now().isoformat(),
		  'CreatedAfter': (sales[0].date_sale + timedelta(days=-1)).isoformat(),
		  #'CreatedAfter': date_from.isoformat(),
		  'CreatedBefore': (sales[sales.count() - 1].date_sale + timedelta(days=1)).isoformat()
		  #'CreatedBefore': date_to.isoformat()
		}
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))
		parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))

		url_request = url_linio + concatenated
		data = requests.get(url_request).json()["SuccessResponse"]["Body"]["Orders"]["Order"]
		if type(data) != list:
			data = [data]
		print(len(data))
		for i in sales:
			sale_selected = None
			for each in data:
				if str(i.order_channel) == each['OrderNumber']:
					sale_selected = each
					break
			if sale_selected is not None:
				status_sale = self.status_linio(sale_selected['Statuses']['Status'])
				# print(status_sale)
				# if status_sale != i.shipping_status:
				q = Sale.objects.filter(id=i.id).update(channel_status=status_sale)


	def handle(self, *args, **options):
		retail_api_sales = Sale.objects.filter(channel_id__in=[1,3,5])

		self.update_mercadolibre(retail_api_sales.filter(channel_id=1,))
		self.update_ripley(retail_api_sales.filter(channel_id=3).order_by('date_sale'))
		self.update_linio(retail_api_sales.filter(channel_id=5))