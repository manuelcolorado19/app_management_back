import argparse
import asyncio
import sys
from django.core.management.base import BaseCommand, CommandError
# import resource
from aiohttp import ClientSession
from functools import partial
from time import time
from bs4 import BeautifulSoup, SoupStrainer
import json
import requests
from multiprocessing.dummy import Pool as ThreadPool
from operator import itemgetter
from notifications.models import Token
from openpyxl import load_workbook

# import xlsxwriter
wb = load_workbook('Lista Preguntas Mercadolibre.xlsx')
ws = wb['Preguntas']

token = Token.objects.get(id=1).token_ml
count = 0
mlc_data = []
for row in ws.rows:
	count += 1
	if count > 1:
		mlc_data.append({"MLC": str(row[0].value), "row_number": count})
start_time = time()
title_data = []
# data_question = []

async def run(session, number_of_requests, concurrent_limit):
	base_url = "https://api.mercadolibre.com/items/{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		url = base_url.format(i['MLC']) + "?access_token=" + token
		async with session.get(url) as response:
			response = await response.read()
			sem.release()
			responses.append(response)
			data_json = json.loads(response)
			# print(data_json)
			title_data.append({"title": data_json['title'], "row": i['row_number'] })
			# print(len(data_json['results']))
			# try:
			# for each in data_json['questions']:
			# 	if each.get('answer', None) is not None:
			# 		answer = each['answer']['text']
			# 		date_created = each['answer']['date_created']
			# 	else:
			# 		answer = ''
			# 		date_created = ''
			# 	data_question.append({
			# 			"MLC":each['item_id'],
			# 			"question": each['text'],
			# 			"date_question": each['date_created'],
			# 			"answer": answer,
			# 			"date_answer": date_created,
			# 		})
			# except Exception as e:
				# with_problem.append(i)
			# print("request made")
			return response

	for i in mlc_data:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)

	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	print("total_data: {}".format(len(title_data)))
	f= open("Titulos de Publicaciones.txt","w+")
	for j in title_data:
		f.write("%s \t %s\r" % (j['title'], j['row']))
	f.close()
	
	print("--- %s seconds ---" % (time() - start_time))

	return responses

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		loop = asyncio.get_event_loop()
		loop.run_until_complete(main(10000, 10000))
		loop.close()