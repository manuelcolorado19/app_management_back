from django.db import models
from sales.models import Product_Sale, Pack
from categories.models import Channel
from django.contrib.auth import get_user_model
from sellers.models import Seller
from django.contrib.postgres.fields import ArrayField
User = get_user_model()
class Token(models.Model):
	token_ml = models.CharField(max_length=200, null=True, blank=True)
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'token'

class AutomaticMessage(models.Model):
	MESSAGE_TYPE = (
		('to_be_agreed', 'to_be_agreed'),
		('with_label', 'with_label')
	)
	message = models.TextField(max_length=10000, null=True, blank=True)
	message_type = models.CharField(choices=MESSAGE_TYPE, default='', blank=True, null=True, max_length=20)

	class Meta:
		db_table = "automatic_message"

class EmailReceiver(models.Model):
	email = models.CharField(max_length=40, null=True, blank=True)

	class Meta:
		db_table = "sales_orders_email"

class CustomMessageProduct(models.Model):
	message = models.TextField(max_length=2500, null=True, blank=True)
	product = models.OneToOneField(Product_Sale, on_delete=models.SET_NULL, blank=True, null=True)
	pack = models.ForeignKey(Pack, on_delete=models.SET_NULL, blank=True, null=True)

	class Meta:
		db_table = 'custom_message_product'

class Video(models.Model):
	name=models.CharField(max_length=500, null=True)
	description=models.CharField(max_length=500, null=True)
	videofile=models.FileField(upload_to='videos/', null=True, verbose_name="")

	def __str__(self):
		return self.name + ": " + str(self.videofile)

class PublicationDataMercadolibre(models.Model):
	title = models.CharField(max_length=250, null=True, blank=True)
	link_publication = models.URLField(null=True, blank=True)
	img_first = models.URLField(null=True, blank=True)
	seller = models.ForeignKey(Seller, on_delete=models.SET_NULL, blank=True, null=True, related_name='publication_seller')
	mlc = models.CharField(max_length=15, null=True, blank=True)
	custom_field = models.CharField(max_length=10, null=True, blank=True)

	class Meta:
		db_table = 'publications_mercadolibre'


class Question(models.Model):
	STATUS_QUESTION = (
		('pending', 'pending'),
		('answered', 'answered'),
		('delete_by_mercadolibre', 'delete_by_mercadolibre'),
	)

	text = models.TextField()
	is_answer = models.BooleanField(default=False)
	status_question = models.CharField(choices=STATUS_QUESTION, max_length=30, null=True)
	mercadolibre_id_question = models.BigIntegerField(null=True)
	user_id_ml = models.BigIntegerField(null=True)
	user_nickname_ml = models.CharField(max_length=40, null=True)
	suggested_answer = models.TextField(null=True, blank=True)
	key_words = ArrayField(models.CharField(max_length=30), blank=True, null=True)
	publication_data = models.ForeignKey(PublicationDataMercadolibre, on_delete=models.SET_NULL, blank=True, null=True, related_name='publication_data_question')
	channel = models.ForeignKey(Channel, on_delete=models.SET_NULL, null=True, blank=True, related_name='question_channel')
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'questions'

class Answer(models.Model):
	question = models.OneToOneField(Question, on_delete=models.CASCADE, related_name='answer_question')
	answer = models.TextField()
	is_robot = models.BooleanField(default=False)
	mercadolibre_id_answer = models.BigIntegerField(null=True)
	user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='user_answer')
	date_created_mercadolibre = models.DateTimeField(null=True)
	date_answer = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'answer'

class SpecialEvent(models.Model):
	name=models.CharField(max_length=200)
	channel=models.ForeignKey(Channel, on_delete=models.SET_NULL, blank=True, null=True)
	date_created=models.DateTimeField(null=True)