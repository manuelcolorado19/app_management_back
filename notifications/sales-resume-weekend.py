from sales.models import Sale
import datetime
from datetime import timedelta
import xlsxwriter
import itertools, os
from operator import itemgetter
from django.core.mail.message import EmailMessage
today = datetime.datetime.now()
# today = datetime.datetime(2019,4,8)
week_end_start = (today + timedelta(days=-2)).replace(hour=15, minute=00)
sales = Sale.objects.filter(date_created__gte=week_end_start, order_channel__isnull=False).order_by('id')
file_name = os.path.expanduser("~/Documents/Resumen Ventas/Resumen Ventas Fin de Semana " + str(today.date()) + ".xlsx") 
wb =  xlsxwriter.Workbook(file_name)
ws = wb.add_worksheet(str(today.date()))
ws.write(0, 0, str("Id"))
ws.write(0, 1, str("Id Venta"))
ws.write(0, 2, str("Fecha Venta"))
ws.write(0, 3, str("Hora Venta"))
ws.write(0, 4, str("Canal"))
ws.write(0, 5, str("Total"))
row_num = 1

for i in sales:
	ws.write(row_num, 0, str(i.id))
	ws.write(row_num, 1, i.order_channel)
	ws.write(row_num, 2, str(i.date_sale))
	ws.write(row_num, 3, str(i.hour_sale_channel))
	ws.write(row_num, 4, str(i.channel.name))
	ws.write(row_num, 5, i.total_sale)
	row_num += 1

ws2 = wb.add_worksheet("Resumen Canales")
row_num = 0
ws2.write(row_num, 0, str("Canal"))
ws2.write(row_num, 1, str("Total de Ventas"))
sales = sales.values('total_sale', 'channel__name', 'channel__id')
sorted_order = sorted(sales, key=itemgetter('channel__id',))
group_order = []
for key, group in itertools.groupby(sorted_order, key=lambda x:x['channel__id']):
	group_order.append(list(group))
row_num = 1
for each in group_order:
	ws2.write(row_num, 0, str(each[0]['channel__name']))
	ws2.write(row_num, 1, len(each))
	row_num += 1
wb.close()
# output.seek(0)
email = EmailMessage()
email.subject = "Reporte de ventas Fin de Semana"
email.from_email = "mejiasabelito@gmail.com"
email.to = [ "mejiasabelito@gmail.com", ]
email.attach_file(file_name)
email.send()
# Enviar por correo electrónico