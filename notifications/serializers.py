from rest_framework import serializers
from .models import AutomaticMessage, EmailReceiver, CustomMessageProduct, Video, Question, PublicationDataMercadolibre, Answer, Token
import requests
import json
from datetime import timedelta
from sales.models import Sale, Product_Sale, Pack
from django.db.models import F, CharField, Value
from django.db.models.functions import Concat, Cast
from answerRobot.models import Response, Intention, Word

class AutomaticMessageSerializer(serializers.ModelSerializer):
	class Meta:
		fields = '__all__'
		model = AutomaticMessage

class EmailReceiverSerializer(serializers.ModelSerializer):
	class Meta:
		fields = '__all__'
		model = EmailReceiver

class SKUSerializer(serializers.IntegerField):
	def to_representation(self, value):
		if value.product is not None:
			return value.product.sku
		return value.pack.sku_pack

class DescriptionSerializer(serializers.CharField):
	def to_representation(self, value):
		if value.product is not None:
			return value.product.description
		return value.pack.description


class CustomMessageProductSerializer(serializers.ModelSerializer):
	sku = SKUSerializer(source='*', read_only=True)
	description = DescriptionSerializer(source='*', read_only=True)
	class Meta:
		fields = '__all__'
		model = CustomMessageProduct

class VideoSerializer(serializers.ModelSerializer):
	
	class Meta:
		fields = ('videofile','name', 'id', 'description')
		model = Video
		extra_kwargs = {
			"name": {
				"read_only": True
			},
			"description": {
				"read_only": True
			}
		}

	def create(self, validated_data):
		# print(validated_data)
		validated_data['name'] = validated_data['videofile'].name.split(".")[0]
		
		return Video.objects.create(**validated_data)

	def update(self, instance, validated_data):
		instance.videofile = validated_data.get('videofile', instance.videofile)
		instance.save()
		return instance

class NicknameSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.seller.nickname
class TechnicalData(serializers.DictField):
	def to_representation(self, value):
		if value.custom_field is not None:
			data = Product_Sale.objects.filter(sku=value.custom_field[:5]).annotate(height2=Concat(F('height'), Value(' cm'), output_field=CharField()), length2=Concat(F('length'), Value(' cm'), output_field=CharField()), width2=Concat(F('width'), Value(' cm'), output_field=CharField()), weight2=Concat(F('weight'), Value(' kg'), output_field=CharField())).values('height2', 'length2', 'weight2', 'width2')
			if data:
				return data[0]
			else:
				data = Pack.objects.filter(sku_pack=value.custom_field[:5]).annotate(height2=Concat(F('height'), Value(' cm'), output_field=CharField()), length2=Concat(F('length'), Value(' cm'), output_field=CharField()), width2=Concat(F('width'), Value(' cm'), output_field=CharField()), weight2=Concat(F('weight'), Value(' kg'), output_field=CharField())).values('height2', 'length2', 'weight2', 'width2')
				if data:
					return data[0]
				else:
					return None
		else:
			return None
class PublicationMercadolibreSerializer(serializers.ModelSerializer):
	seller_nickname = NicknameSerializer(read_only=True, source='*')
	#technical_data = TechnicalData(read_only=True, source='*')
	class Meta:
		fields = '__all__'
		model = PublicationDataMercadolibre

class UserAnswer(serializers.CharField):
	def to_representation(self, value):
		return value.user.first_name + " " + value.user.last_name 

class UserBuy(serializers.CharField):
	def to_representation(self, value):
		if value.question.publication_data is not None:
			sale = Sale.objects.filter(mlc=value.question.publication_data.mlc, user_id_ml=value.question.user_id_ml).count()
			if sale > 0:
				return True
		return False

class Incidence(serializers.CharField):
	def to_representation(self, value):
		question_72hours = value.date_created + timedelta(hours=72)
		if value.is_answer and value.answer_question is not None:
			if value.publication_data is not None:
				sale = Sale.objects.filter(mlc=value.publication_data.mlc, user_id_ml=value.user_id_ml,
				date_created__gt=value.answer_question.date_answer)
				if sale:
					return "Despues"
		else:
			if value.publication_data is not None:
				filer_question = Sale.objects.filter(mlc=value.publication_data.mlc, user_id_ml=value.user_id_ml, date_created__gt=value.date_created, date_created__lt=question_72hours)
				if filer_question:
					return "Antes"
				else:
					return "-"
		return "-"
class DiferenceQuestion(serializers.CharField):
	def to_representation(self, value):
		diference = value.date_answer - value.question.date_created
		seconds = diference.seconds
		minutes = (diference.days * 24 * 60) + (diference.seconds/60)
		#print(minutes)
		if minutes > 1:
			return str(int(minutes)) + " minutos"
		return str(int(minutes)) + " minuto"
class AnswerSerializer(serializers.ModelSerializer):
	mercadolibre_id_question = serializers.IntegerField(write_only=True)
	user_answer = UserAnswer(read_only=True, source='*')
	user_buy = UserBuy(read_only=True, source='*')
	diference_question_answer = DiferenceQuestion(read_only=True, source='*')
	class Meta:
		fields = '__all__'
		model = Answer
		
	def create(self, validated_data):
		# Se hace petición a api de mercadolibre y luego se crea respuesta basado en si fue exitosa o no

		token = Token.objects.get(id=1).token_ml
		url = "https://api.mercadolibre.com/answers?access_token=" + token
		data_answer = {
			"question_id":validated_data['mercadolibre_id_question'],
			"text":validated_data['answer']
		}
		# print(data_answer)
		# return Answer.objects.get(id=1)
		data = requests.post(url, data=json.dumps(data_answer))
		# print(data.json())
		if data.status_code == 200:
			validated_data['question'].is_answer = True
			validated_data['question'].status_question = "answered"
			validated_data['question'].save()
			# if validated_data['question'].suggested_answer == validated_data['answer']:
			# 	is_robot = True
			# else:
			# 	is_robot = False
			answer = Answer.objects.create(
				question=validated_data['question'],
				answer=validated_data['answer'],
				mercadolibre_id_answer=data.json()['id'],
				user=self.context['request'].user,
			)
			return answer
		raise serializers.ValidationError({"answer": ["Disculpe hubo un error al enviar la respuesta"]})


class IntentionsQuestionSerializer(serializers.ListField):
	def to_representation(self, value):
		if value.key_words is not None:
			return Intention.objects.filter(word_intention__word__text_word__in=value.key_words).distinct().values_list('text_intention', flat=True)
		return []

class AllIntentionsSerializer(serializers.ListField):
	def to_representation(self, value):
		return Intention.objects.all().values_list('text_intention', flat=True)

class QuestionSerializer(serializers.ModelSerializer):
	publication_data = PublicationMercadolibreSerializer(read_only=True)
	answer_question = AnswerSerializer(read_only=True)
	incidence = Incidence(read_only=True, source='*')
	intentions_question = IntentionsQuestionSerializer(read_only=True, source='*')
	all_intentions=AllIntentionsSerializer(read_only=True, source='*')
	class Meta:
		fields = '__all__'
		model = Question

