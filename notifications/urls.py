from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import AutomaticMessageViewSet, EmailReceiverViewSet, CustomMessageProductViewSet, VideoViewSet, QuestionViewSet, AnswerViewSet

router = DefaultRouter()
router.register(r'automatic_message', AutomaticMessageViewSet, base_name='automatic_message')
router.register(r'email_receiver', EmailReceiverViewSet, base_name='email_receiver')
router.register(r'custom_message_product', CustomMessageProductViewSet, base_name='custom_message_product')
router.register(r'video', VideoViewSet, base_name='video')
router.register(r'question', QuestionViewSet, base_name='question')
router.register(r'answer', AnswerViewSet, base_name='answer')

urlpatterns = [
	url(r'^', include(router.urls)),
	
]