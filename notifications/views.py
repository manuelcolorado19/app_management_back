from django.shortcuts import render
from rest_framework.views import APIView
from openpyxl import load_workbook
import requests
import json
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from OperationManagement.settings import CLIENT_ID, CLIENT_SECRET
from sales.models import Sale, Sale_Product, Pack, Pack_Products, Product_Sale, Product_Code, Color, Commission
from django.contrib.auth import get_user_model
from categories.models import Channel
from django.db.models import F, Case, When, CharField, Value, IntegerField, DurationField, Q, Sum, Value
import datetime
from django.db import transaction
from datetime import timedelta
from rest_framework.throttling import AnonRateThrottle
from random import randint
import time
from .serializers import AutomaticMessageSerializer, EmailReceiverSerializer, CustomMessageProductSerializer, VideoSerializer, QuestionSerializer, AnswerSerializer
from .models import AutomaticMessage, EmailReceiver, CustomMessageProduct, Video, Token, Question, Answer, PublicationDataMercadolibre
from rest_framework.viewsets import ModelViewSet
from .filters import CustomMessageProductFilter, QuestionFilter
from rest_framework import filters as search_filter
from django_filters import rest_framework as filters
from sales.views import CustomPagination
from rest_framework.parsers import FileUploadParser
from rest_framework.pagination import PageNumberPagination
from django.core.mail import send_mail
from operator import itemgetter
from nltk import sent_tokenize
from answerRobot.models import Word, Intention, IntentionWords
from answerRobot.models import Response as response_robot
import re, itertools
from operator import itemgetter
import string
import nltk
import numpy as np
import random
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import xlsxwriter, io
from django.db.models.functions.text import Substr
from django.http import HttpResponse

#key_words = Word.objects.filter(category_word='key_word').values_list('text_word', flat=True)
#f2=open("answerRobot/files/Preguntas Mercadolibre.txt","r", errors = 'ignore')
#raw=f2.read()
#raw=raw.lower()
#f2.close()
# print(raw)
#sent_tokens = nltk.sent_tokenize(raw)
#word_tokens = nltk.word_tokenize(raw)
#lemmer = nltk.stem.WordNetLemmatizer()
#wb = load_workbook('answerRobot/files/Lista Preguntas Mercadolibre conteo.xlsx')
#ws = wb['Intenciones']
class AnonOnePerSecondThrottle(AnonRateThrottle):
	def parse_rate(self, rate):
		"""
		Given the request rate string, return a two tuple of:
		<allowed number of requests>, <period of time in seconds>

		So we always return a rate for 10 request per 10 minutes.

		Args:
			string: rate to be parsed, which we ignore.

		Returns:
			tuple:  <allowed number of requests>, <period of time in seconds>
		"""
		return (1, 3)

User = get_user_model()

def nth_repl(s, sub, repl, nth):
	find = s.find(sub)
	# if find is not p1 we have found at least one match for the substring
	i = find != -1
	# loop util we find the nth or we find no match
	while find != -1 and i != nth:
		# find + 1 means we start at the last match start index + 1
		find = s.find(sub, find + 1)
		i += 1
	# if i  is equal to nth we found nth matches so replace
	if i == nth:
		return s[:find]+repl+s[find + len(sub):]
	return s

class SendAutomaticMessage(APIView):
	permission_classes = (AllowAny,)
	#throttle_classes = (AnonOnePerSecondThrottle,)
	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl","abel.mejias@asiamerica.cl", "mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code_data  = Product_Code.objects.filter(code_seven_digits=each)[0]
				message_text = product_code_data.product.description + " Variante: " + product_code_data.color.description + "(" + str(each) + ")"
				#message_append.append(message_text)
			else:
				product_sale_data = Product_Sale.objects.filter(sku=each)[0]
				message_text = product_sale_data.description + "(" + str(each) + ")"
			message_append.append(message_text)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def status(self, x):
		return {
			'paid': "Pagado",
			'cancelled': "Cancelado",
			'confirmed': "Confirmado",
			'payment_in_process': "Pago en proceso",
			'payment_required': "Pago requerido",
			'invalid': "Invalido",
		}[x]

	def status_shipping(self, x):
		return {
			'delivered': "Entregado",
			'shipped': "Enviado",
			'pending': "Pendiente",
			'ready_to_ship': "Listo para enviar",
			'handling': "En Manejo",
			'not_delivered': "No entregado",
			'not_verified': "No verificado",
			'cancelled': "Cancelado",
			'closed': "Cerrado",
			'to_be_agreed':None,
			'error': "Error",
			'active': "Activo",
			'not_specified': "No especificado",
			'stale_ready_to_ship': "Estancado en listo para enviar",
			'stale_shipped': "Estancado en enviado",
			'to_be_agreed': "Por acordar",
		#'cost_exceeded': "Costo Excesivo",
		#'printed': "Impresa"
		}[x]

	def roundup(self, x):
		return int(math.ceil(x / 100.0)) * 100

	def calculate_commission(self, sale_product):
		token = Token.objects.get(id=1).token_ml
		sale_date_data = sale_product.sale.date_sale
		sale_date_plus = sale_product.sale.date_sale + timedelta(days=1)
		url_data = "https://api.mercadolibre.com/orders/search?seller=216244489&access_token="+ token +"&order.date_created.from="+sale_date_data.strftime("%Y-%m-%dT00:00:00.000-00:00")+"&order.date_created.to="+sale_date_plus.strftime("%Y-%m-%dT23:59:00.000-00:00")
		data_orders = requests.get(url_data).json()
		if data_orders.get('results', None) is not None:
			commission_api = None
			data_orders = data_orders['results']
			for i in data_orders:
				if i['id'] == sale_product.sale.order_channel:
					commission_api = i['order_items'][0]['sale_fee']
					unit_price_api = i['order_items'][0]['unit_price']
					quantity_api = i['order_items'][0]['quantity']
					break
		else:
			return 0
		return commission_api * quantity_api

	def create_sale(self, order_data, token):
		sale_data = Sale.objects.filter(order_channel=order_data['id'], channel_id=1)
		if sale_data.count() > 0:
			sale = Sale.objects.get(id=sale_data[0].id)
			if order_data['shipping']['status'] != 'to_be_agreed':
				sub_status_notification = order_data['shipping']['substatus']
				if sub_status_notification == 'cost_exceeded':
					sale.shipping_type = 'por acordar'
					sale.shipping_type_sub_status = 'pendiente'
				sale.save()
			if sale.tracking_number == None and order_data['shipping'].get('id', None) is not None:
				shipping_tracking = requests.get("https://api.mercadolibre.com/shipments/" + str(order_data['shipping']['id']) + "?access_token=" +token).json()['tracking_number']
				if shipping_tracking is not None and shipping_tracking.isdigit():
					sale.tracking_number = int(shipping_tracking)
					sale.save()
			pass
		else:
			if not order_data['order_items'][0]['item']['variation_attributes']:
				if order_data['shipping']['status'] != 'to_be_agreed':
					shipping_tracking = requests.get("https://api.mercadolibre.com/shipments/" + str(order_data['shipping']['id']) + "?access_token=" +token).json()['tracking_number']
					if shipping_tracking is not None:
						shipping_tracking = int(shipping_tracking)
					shipping_type="con etiqueta"
					shipping_status=self.status_shipping(order_data['shipping']['status'])
					shipping_type_sub_status=''
				else:
					shipping_type="por acordar"
					shipping_type_sub_status='pendiente'
					shipping_tracking=None
					shipping_status=None
				date_sale = datetime.datetime.strptime(nth_repl(order_data['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.%f%z")
				sale = Sale.objects.create(
					order_channel=order_data['id'],
					total_sale=0,
					user=User.objects.get(username='api'),
					channel=Channel.objects.get(name='Mercadolibre'),
					document_type='sdt',
					payment_type='transferencia retail',
					sender_responsable="comprador" if order_data['shipping']['status'] == "to_be_agreed" else "vendedor",
					status='pendiente',
					channel_status=self.status(order_data['status']),
					comments=order_data['order_items'][0]['item']['title'] +" (Venta total de "+ str(order_data['total_amount']) + ")",
					date_sale=date_sale,
					tracking_number=shipping_tracking,
					shipping_type=shipping_type,
					shipping_status=shipping_status,
					shipping_type_sub_status=shipping_type_sub_status,
					mlc=order_data['order_items'][0]['item']['id'],
					user_id_ml=order_data['buyer']['id'],
					last_user_update=User.objects.get(username='api'),
					hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute)))),
					product_quantity_api=order_data['order_items'][0]['quantity'],
				)
				if shipping_type == 'con etiqueta':
					unit_price = (order_data['shipping']['shipping_option']['cost'] - order_data['shipping']['shipping_option']['list_cost']) * (-1)
					sale_product = Sale_Product.objects.create(
						product=Product_Sale.objects.get(sku=1),
						sale=sale,
						quantity=1,
						unit_price=unit_price,
						total=(unit_price*1),
					)
					sale.total_sale = sale_product.total
					sale.save()
			else:
				if order_data['order_items'][0]['item']['variation_attributes']:
					if "(" in order_data['order_items'][0]['item']['variation_attributes'][0]['value_name'] and ")" in order_data['order_items'][0]['item']['variation_attributes'][0]['value_name']:
						if order_data['order_items'][0]['item']['seller_sku'] is not None:
							sku_mercadolibre = order_data['order_items'][0]['item']['seller_sku']
						else:
							sku_mercadolibre = order_data['order_items'][0]['item']['seller_custom_field']
					else:
						sku_mercadolibre = order_data['order_items'][0]['item']['variation_attributes'][0]['value_name'][:7]
				else:
					sku_mercadolibre = None
				if sku_mercadolibre[:5].isdigit():
					is_pack = Pack.objects.filter(sku_pack=sku_mercadolibre[:5])
				else:
					is_pack = Pack.objects.filter(sku_pack=order_data['order_items'][0]['item']['seller_sku'][:5])
				# print(is_pack)
				# print(sku_mercadolibre)
				if is_pack:
					pack = is_pack[0]
					products = Pack_Products.objects.filter(pack=is_pack[0]).annotate(
						sku_product=F('product_pack__sku'), 
						quantity_product=F('quantity') * Value(order_data['order_items'][0]['quantity'], output_field=IntegerField()), 
						unit_price_product=(Value(order_data['order_items'][0]['unit_price'], output_field=IntegerField()) * F('percentage_price') / Value(100, output_field=IntegerField())) / (F('quantity'))).values('sku_product', 'quantity_product', 'unit_price_product').order_by('-sku_product')
					if products.count() == 1 and len(order_data['order_items'][0]['item']['variation_attributes']) > 0:
						sku_7_pack = str(products[0]['sku_product']) + sku_mercadolibre[5:7]
					else:
						sku_7_pack = None
					if sku_7_pack == None:
						pass
					elif sku_7_pack.isdigit():
						pass
					else:
						sku_7_pack = str(products[0]['sku_product']) + sku_mercadolibre[:2]
					# print(sku_7_pack)
					# return Response(data={"todo malo": ['ksjsjsjksjs']})
				else:
					pack = None
					try:
						products = Product_Sale.objects.filter(sku=sku_mercadolibre[:5]).annotate(sku_product=F('sku'), quantity_product=Value(order_data['order_items'][0]['quantity'], output_field=IntegerField()), unit_price_product=Value(order_data['order_items'][0]['unit_price'], output_field=IntegerField())).values('sku_product', 'quantity_product', 'unit_price_product')
					except ValueError as e:
						products = Product_Sale.objects.filter(sku=order_data['order_items'][0]['item']['seller_sku'][:5]).annotate(sku_product=F('sku'), quantity_product=Value(order_data['order_items'][0]['quantity'], output_field=IntegerField()), unit_price_product=Value(order_data['order_items'][0]['unit_price'], output_field=IntegerField())).values('sku_product', 'quantity_product', 'unit_price_product')
						sku_mercadolibre = order_data['order_items'][0]['item']['seller_sku']
				# print(products)
				
				date_sale = datetime.datetime.strptime(nth_repl(order_data['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.%f%z") + timedelta(hours=1)
				if order_data['shipping']['status'] != 'to_be_agreed':
					shipping_tracking = requests.get("https://api.mercadolibre.com/shipments/" + str(order_data['shipping']['id']) + "?access_token=" +token).json()['tracking_number']
					shipping_type="con etiqueta"
					shipping_status=self.status_shipping(order_data['shipping']['status'])
					shipping_type_sub_status=''
					if shipping_tracking is not None:
						shipping_tracking = int(shipping_tracking)
				else:
					shipping_type="por acordar"
					shipping_tracking=None
					shipping_status=None
					shipping_type_sub_status='pendiente'
				comment = order_data['order_items'][0]['item']['title']
				if order_data['order_items'][0]['item']['variation_attributes']:
					comment += (" " + order_data['order_items'][0]['item']['variation_attributes'][0]['value_name'])
				if sku_mercadolibre.endswith("99"):
					comment += " REACONDICIONADA"
				sale = Sale.objects.create(
					order_channel=order_data['id'],
					total_sale=0,
					user=User.objects.get(username='api'),
					channel=Channel.objects.get(name='Mercadolibre'),
					document_type='sdt',
					payment_type='transferencia retail',
					sender_responsable="comprador" if order_data['shipping']['status'] == "to_be_agreed" else "vendedor",
					status='pendiente',
					channel_status=self.status(order_data['status']),
					comments=comment,
					date_sale=date_sale,
					tracking_number=shipping_tracking,
					shipping_type=shipping_type,
					shipping_status=shipping_status,
					mlc=order_data['order_items'][0]['item']['id'],
					user_id_ml=order_data['buyer']['id'],
					last_user_update=User.objects.get(username='api'),
					shipping_type_sub_status=shipping_type_sub_status,
					hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute)))),
					product_quantity_api=order_data['order_items'][0]['quantity'],
				)
				total = 0
				total_sale_no_shipping = order_data['total_amount']
				total_products = 0
				products_stock = []
				# if order_data['order_items'][0]['unit_price'] < 19990:
				# 	aditional_cost = 100 * order_data['order_items'][0]['quantity']
				# else:
				# 	aditional_cost = 0
				# commision_calculate = (order_data['order_items'][0]['unit_price'] * 0.13) + aditional_cost
				count_variation = 0
				for product in products:
					if (len(sku_mercadolibre) == 7) and pack == None:
						product_code = Product_Code.objects.get(code_seven_digits=sku_mercadolibre)
					else:
						codes = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits',6,6)).filter(product__sku=product['sku_product'], code_seven__gte=50).values('code_seven_digits')
						if codes.count() == 1:
							product_code = Product_Code.objects.get(code_seven_digits=codes[0]['code_seven_digits'])
						else:
							if is_pack and (sku_7_pack is not None):
								product_code = Product_Code.objects.filter(code_seven_digits=sku_7_pack)
								if product_code:
									product_code = product_code[0]
								else:
									product_code = None
							else:
								# tratamiento para packs con mas de 1 producto y mas de 1 variante en ambos productos o en al menos 1 de ellos
								try:
									value_variation = order_data['order_items'][0]['item']['variation_attributes'][0]['value_name']
									last_2_code_seven = value_variation[value_variation.find("(")+1:value_variation.find(")")].split("-")[0]

									code_seven = str(product['sku_product']) + last_2_code_seven[count_variation:count_variation + 2]
									product_code = Product_Code.objects.filter(code_seven_digits=code_seven)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								except Exception as e:
									product_code = None
					count_variation += 2
					if (product['unit_price_product'] * product['quantity_product']) > total_sale_no_shipping:
						unit_price = total_sale_no_shipping/(product['quantity_product'])
					else:
						if not (product['unit_price_product']/1.0).is_integer():
							unit_price = self.roundup(product['unit_price_product'])
							total_sale_no_shipping -= (unit_price * product['quantity_product'])
						else:
							unit_price = product['unit_price_product']
					sale_product = Sale_Product.objects.create(
						product=Product_Sale.objects.get(sku=product['sku_product']),
						product_code=product_code,
						sale=sale,
						quantity=product['quantity_product'],
						unit_price=unit_price,
						total=product['quantity_product'] * unit_price,
						comment_unit=order_data['order_items'][0]['item']['title'],
						from_pack=pack,
					)
					# comissions_api = self.calculate_commission(sale_product)
					# percentage_product_commission = comissions_api * (((sale_product.total * 100) / (order_data['order_items'][0]['quantity'] * order_data['order_items'][0]['unit_price']))/100)
					# percentage_product_commission_calculate = commision_calculate * (((sale_product.total * 100) / (order_data['order_items'][0]['quantity'] * order_data['order_items'][0]['unit_price']))/100)

					# if order_data['shipping']['status'] != 'to_be_agreed':
					# 	shipping_complete_price = (order_data['shipping']['shipping_option']['cost'] - order_data['shipping']['shipping_option']['list_cost']) * (-1)
					# 	unit_shipping_price = shipping_complete_price * (((sale_product.total * 100) / (order_data['order_items'][0]['quantity'] * order_data['order_items'][0]['unit_price']))/100)
					# else:
					# 	unit_shipping_price = 0

					# commission = Commission.objects.create(
					# 	sale_product=sale_product,
					# 	api_commission=percentage_product_commission,
					# 	calculate_commission=percentage_product_commission_calculate,
					# 	shipping_price_assigned=unit_shipping_price,
					# )
					quantity_stock = Product_Sale.objects.get(sku=product['sku_product']).stock - (product['quantity_product'])
					Product_Sale.objects.filter(sku=product['sku_product']).update(stock=quantity_stock)
					if product_code is not None:
						quantity_stock_color = product_code.quantity - (product['quantity_product'])
						Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
						if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not (product_code.code_seven_digits in products_stock)):
							products_stock.append(product_code.code_seven_digits)
					else:
						if quantity_stock <= 5 and quantity_stock >= 0 and (not (product['sku_product'] in products_stock)):
							products_stock.append(product['sku_product'])
					total += product['quantity_product'] * unit_price
					total_products += product['quantity_product']

				if Sale_Product.objects.filter(sale=sale).exclude(product__sku=1):
					if total != order_data['total_amount']:
						product_to_split = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1)[0]
						product_split = Sale_Product.objects.get(id=product_to_split.id)
						diference_totals = order_data['total_amount'] - total
						if product_split.quantity == 1:
							product_split.unit_price = product_split.unit_price + diference_totals
							product_split.save()
							product_split.total = product_split.unit_price * product_split.quantity
							product_split.save()
						else:
							product_split.quantity = product_split.quantity - 1
							product_split.total = product_split.total - product_split.unit_price
							product_split.save()
							sale_product = Sale_Product.objects.create(
								product=Product_Sale.objects.get(sku=product_split.product.sku),
								product_code=product_split.product_code,
								sale=sale,
								quantity=1,
								unit_price=product_split.unit_price + diference_totals,
								total=(product_split.unit_price + diference_totals),
							)
							# commission = Commission.objects.create(
							# 	sale_product=sale_product,
							# 	api_commission=0,
							# 	calculate_commission=self.calculate_commission(sale_product)[1],
							# )
						total = order_data['total_amount']

				if order_data['shipping']['status'] == 'to_be_agreed':
					shipping_price = 0
				else:
					shipping_price = (order_data['shipping']['shipping_option']['cost'] - order_data['shipping']['shipping_option']['list_cost']) * (-1)
				sale_product = Sale_Product.objects.create(
					product=Product_Sale.objects.get(sku=1),
					sale=sale,
					quantity=1,
					unit_price=shipping_price,
					total=shipping_price,
					# from_pack=pack,
				)
				total += shipping_price
				sale.total_sale = total
				sale.declared_total = sale.sale_of_product.exclude(product__sku=1).aggregate(sum_totals=Sum('total'))['sum_totals']
				sale.total_number_of_product = total_products
				sale.save()
				if products_stock:
					self.alert_stock(products_stock)
	def clean_text_round1(self, text):
		'''Make text lowercase, remove text in square brackets, remove punctuation and remove words containing numbers.'''
		text = text.lower()
		text = re.sub('\[.*?¿\]\%', ' ', text)
		text = re.sub('[%s]' % re.escape(string.punctuation), ' ', text)
		text = re.sub('\w*\d\w*', '', text)
		return text

	def LemTokens(self, tokens):
		return [lemmer.lemmatize(token) for token in tokens]
	# remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

	def LemNormalize(self, text):
		remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)
		return self.LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

	def response(self, user_response):
		robo_response=''
		sent_tokens.append(user_response['question'])
		TfidfVec = TfidfVectorizer(tokenizer=self.LemNormalize, stop_words='english')
		tfidf = TfidfVec.fit_transform(sent_tokens)
		vals = cosine_similarity(tfidf[-1], tfidf)
		idx=vals.argsort()[0][-2]
		flat = vals.flatten()
		flat.sort()
		req_tfidf = flat[-2]
		question_selected = None
		for i in range(len(vals.argsort()[0]) - 2, 0, -1):
			apearance = 0
			for key in user_response['key_words']:
				if key in sent_tokens[i]:
					apearance += 1
			if apearance == len(user_response['key_words']):
				question_selected = i
				break
		if question_selected is not None:
			answer_posible = None
			for each_selected in sent_tokens[question_selected].splitlines():
				apearance = 0
				for key in user_response['key_words']:
					if key in each_selected:
						apearance += 1
				if apearance == len(user_response['key_words']):
					answer_posible = each_selected
					break
			count = 0
			response_posible = None
			for row in ws.rows:
				count += 1
				if count > 1:
					if answer_posible == row[2].value.lower() + ".":
						response_posible = row[8].value
						break 
						# print(row[8].value)
			if response_posible is not None:
				return robo_response + response_posible
			else:
				return None
		else:
			return None

	def suggested_answer(self, question_data):
		token = Token.objects.get(id=1).token_ml
		mlc = question_data['item_id']
		api_data = requests.get("https://api.mercadolibre.com/items/" + mlc + "?access_token=" + token).json()
		formater_data = []
		question_example = question_data['text'].lower()
		all_sentences = sent_tokenize(question_example)
		for r in all_sentences:
			if len(r) < 10:
				continue
			punctuation = []
			others = []
			keys = []
			numbers = []
			words_string = r.split()
			for each_word in words_string:
				if each_word.isdigit():
					numbers.append(int(each_word))
			words_string2 = "".join((char if char.isalpha() else " ") for char in r).split()
				
			for word in words_string2:
				if word.lower() in key_words and (not word.lower() in keys):
					keys.append(word)
				else:
					others.append(word)

			formater_data.append({"question": r, "key_words": keys, "number":numbers})

		response_completed = ""
		# print(formater_data)
		for each in formater_data:
			if each['key_words']:
				a = self.response(each)
				if a is not None:
					response_completed += a + ". "
				else:
					# Ir al otro metodo para crear respuesta para sugerir al humano
					intentions = Intention.objects.filter(word_intention__word__text_word__in=each['key_words']).distinct()
					intention_selected = None
					to_select = []
					for i in intentions:
						for key in each['key_words']:
							count_word_intention = IntentionWords.objects.filter(word__text_word=key, intention=i)
							if count_word_intention.count() == len(each['key_words']):
								intention_selected = i
							else:
								percentage_precition = (count_word_intention.count() * 100)/len(each['key_words'])
								to_select.append({"intention": i, "key": [key,], "percentage_precition": percentage_precition})
						if intention_selected is not None:
							break
						else:
							# Anadir las combinaciones entre palabras y filtrar por intenciones para añadir porcentajes
							for w in each['key_words']:
								for y in each['key_words']:
									new_key = list(set([w,y]))
									if len(new_key) == 1:
										continue
									# print(new_key)
									count_word_intention = IntentionWords.objects.filter(word__text_word__in=new_key, intention=i)
									percentage_precition = (count_word_intention.count() * 100)/len(each['key_words'])
									to_select.append({"intention": i, "key": new_key, "percentage_precition": percentage_precition})
					to_select_remove_duplicates = sorted([i for n, i in enumerate(to_select) if i not in to_select[n + 1:]], key=itemgetter('percentage_precition'), reverse=True)
					if intention_selected == None:
						if to_select_remove_duplicates[0]['percentage_precition'] == 100:
							intetions_selected = [to_select_remove_duplicates[0]['intention']]
						else:
							intetions_selected = []
							for intention in range(len(to_select_remove_duplicates) - 1):
								if to_select_remove_duplicates[intention]['percentage_precition'] >= (100/len(each['key_words'])):
									if to_select_remove_duplicates[intention]['intention'] in intetions_selected:
										continue
									else:
										intetions_selected.append(to_select_remove_duplicates[intention]['intention'])
					else:
						intetions_selected = [intention_selected]

					# Concatenación y condicion de respuesta basado en petición de API
					response_string = ''
					for selected in intetions_selected:
						if selected.intention_response.negative_answer == None and (not selected.id in [6,7]):
							response_string += selected.intention_response.positive_answer + " "
						else:
							# Llamar a API y decidir respuesta
							if selected.id == 2:
								# Stock disponible Integrar llamado de variantes de colores si aplica.
								color = Color.objects.filter(description__in=[i.upper() for i in each['key_words']])
								# print(color)
								if color:
									# Busqueda en api por stock en colores
									for variation in api_data['variations']:
										if variation['attribute_combinations'][0]['value_name'][5:].upper() == color[0].description:
											if variation['available_quantity'] > 0:
												response_string += selected.intention_response.positive_answer + " "
												break
											else:
												response_string += selected.intention_response.negative_answer + " "
												break

								else:
									if api_data['available_quantity'] > 0:
										response_string += selected.intention_response.positive_answer + " "
									else:
										response_string += selected.intention_response.negative_answer + " "
							elif selected.id == 5:
								# Envío gratis
								if api_data['shipping']['free_shipping']:
									response_string += selected.intention_response.positive_answer + " "
								else:
									response_string += selected.intention_response.negative_answer + " "
							elif selected.id == 6:
								# Medidas del producto
								publication_data = PublicationDataMercadolibre.objects.filter(mlc=mlc, custom_field__isnull=False)
								if publication_data:
									product_data = Product_Sale.objects.get(sku=publication_data[0].custom_field[:5])
									response_string += selected.intention_response.positive_answer.replace("X", str(product_data.height)).replace("Y", str(product_data.width)).replace("Z", str(product_data.length)).replace("W", str(product_data.weight))
								else:
									response_string += "no tengo datos del producto preguntado."
							pass

					response_completed += response_string + " "
			else:
				# Ir al humano directamente
				response_completed += ""
				# print("pregunta sin palabras claves")
		if response_completed != "" and response_completed != " ":
			# print("Aqui")
			all_keys = []
			for data in formater_data:
				all_keys += data['key_words']
			return response_completed + "Saludos.".capitalize(), all_keys
		else:
			return None, []
	
	def post(self, request):
		data = request.data
		url_ml = "https://api.mercadolibre.com"
		token = Token.objects.get(id=1).token_ml
		if data['topic'] == 'orders':
			order_data = requests.get(url_ml + data['resource'] + "?access_token=" + token, timeout=20).json()
			#if order_data['shipping']['status'] == 'to_be_agreed':
			#	print(order_data)
			if order_data.get('seller', None) is not None:
				pass
			else:
				url_auth = "https://api.mercadolibre.com/oauth/token"
				data_for_token = {
					"grant_type":"client_credentials",
					"client_id":CLIENT_ID,
					"client_secret":CLIENT_SECRET
				}
				new_token = requests.post(url_auth, data=json.dumps(data_for_token)).json()
				token = new_token['access_token']
				query_token = Token.objects.filter(id=1).update(token_ml=token)
				order_data = requests.get(url_ml + data['resource'] + "?access_token=" + token, timeout=20).json()
			message = {
				"from": {"user_id": order_data['seller']['id']},
				"to": [
					{
						"user_id": order_data['buyer']['id'],
						"resource": "orders",
						"resource_id": order_data['id'],
						"site_id": "MLC"
					}
				],
				"subject": "Respuesta Venta",
				"text": {}
			}
			if order_data['status'] == 'paid' and order_data['shipping']['status'] == 'ready_to_ship' and (not order_data['feedback']['purchase']) and (not order_data['feedback']['sale']):
				#pass
				message_data = AutomaticMessage.objects.get(id=1).message
				message['text']['plain'] = message_data
					
			if order_data['status'] == 'paid' and order_data['shipping']['status'] == 'to_be_agreed' and (not order_data['feedback']['purchase']) and (not order_data['feedback']['sale']):
				# print("por acordar")
				message_data = AutomaticMessage.objects.get(id=2).message
				message['text']['plain'] = message_data
			time.sleep(randint(3, 25))
			self.create_sale(order_data, token)
			if Sale.objects.filter(order_channel=order_data['id'], channel_id=1).count() > 0:
				sale = Sale.objects.filter(order_channel=order_data['id'], channel_id=1)
				new_status = self.status_shipping(order_data['shipping']['status'])
				if new_status != sale[0].shipping_status:
					sale.update(shipping_status=new_status)
					if new_status == 'Cancelado':
						sale.update(status='cancelado')

			if not message['text']:
				return Response(data={"message": "Sin mensaje para enviar"}, status=status.HTTP_204_NO_CONTENT)

			messages_quantity = requests.get(url_ml + '/messages/orders/' + str(order_data['id']) + "?access_token=" + token, timeout=10).json()
			#print(messages_quantity)

			if messages_quantity["paging"]["total"] == 0:
				#time.sleep(randint(3, 12))
				data_message = json.dumps(message, ensure_ascii=False).encode("utf-8")
				message_send = requests.post(url_ml + '/messages' + "?access_token=" + token, data=data_message)
				# print(message_send.json())
				#custom_message = CustomMessageProduct.objects.filter(Q(product__sku=order_data['order_items'][0]['item']['seller_custom_field']) | Q(pack__sku_pack=order_data['order_items'][0]['item']['seller_custom_field']))
				#if order_data['order_items'][0]['item']['seller_custom_field'] is not None:
				#if custom_message and order_data['order_items'][0]['item']['seller_custom_field'] is not None:
				#	if custom_message[0].message != "" and len(order_data['order_items'][0]['item']['seller_custom_field']) == 5:
				#		message['text']['plain'] = custom_message[0].message
				#		data_message = json.dumps(message, ensure_ascii=False).encode("utf-8")
				#		message_send = requests.post(url_ml + '/messages' + "?access_token=" + token, data=data_message)
			
				return Response(data={"message": "Mensaje enviado satisfactoriamente"}, status=status.HTTP_201_CREATED)
			
			return Response(data={"message": "Mensaje enviado satisfactoriamente"}, status=status.HTTP_200_OK)
		elif data['topic'] == 'questions':
			question_data = requests.get(url_ml + data['resource'] + "?access_token=" + token, timeout=10).json()
			if question_data.get('id', None) is not None:
				# Pregunta sin problema
				print(question_data['id'])
				question_filter = Question.objects.filter(mercadolibre_id_question=question_data['id'])
				if question_filter:
					if question_data['answer'] is not None and question_filter[0].is_answer == False:
						question_get = Question.objects.get(mercadolibre_id_question=question_data['id'])
						answer = Answer.objects.create(
							question=question_get,
							answer=question_data['answer']['text'],
							user=User.objects.get(username='api'),
							#date_created_mercadolibre=datetime.datetime.strptime(nth_repl(question_data['date_created']['answer']['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.%f%z") + timedelta(hours=1)
						)
						question_get.is_answer = True
						question_get.status_question = "answered"
						question_get.save()
						return Response(data={"answer": ["Respuesta guardada"]}, status=status.HTTP_200_OK)
					else:
						return Response(data={"question": ["Pregunta ya guardada"]}, status=status.HTTP_200_OK)
				else:
					user_data = requests.get(url_ml + "/users/" + str(question_data['from']['id']) + "?access_token=" + token).json()
					#suggested_, key_words_ = self.suggested_answer(question_data)
					question = Question.objects.create(
						text=question_data['text'],
						mercadolibre_id_question=question_data['id'],
						user_id_ml=user_data['id'],
						user_nickname_ml=user_data['nickname'],
						status_question="pending",
						#suggested_answer=suggested_,
						#key_words=key_words_,
						publication_data=PublicationDataMercadolibre.objects.get(mlc=question_data["item_id"]),
						channel=Channel.objects.get(name='Mercadolibre'),
					)
					if question_data['answer'] is not None:
						answer = Answer.objects.create(
							question=question,
							answer=question_data['answer']['text'],
							user=User.objects.get(username='api'),
							#date_created_mercadolibre=datetime.datetime.strptime(nth_repl(question_data['date_created']['answer']['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.%f%z") + timedelta(hours=1)
						)
						question.is_answer = True
						question.status_question = "answered"
						question.save()

					return Response(data={"question":["Pregunta creada exitosamente"]})
			else:
				# Tratamiento para preguntas con problema
				id_mercadolibre_question = data['resource'].replace("/questions/","")
				question_filter = Question.objects.filter(mercadolibre_id_question=int(id_mercadolibre_question))
				if question_filter:
					question_filter.update(status_question="delete_by_mercadolibre")
				else:
					pass
				return Response(data={"question": ['Pregunta procesada de forma correcta']})
			# Notificaciones de preguntas
		else:
			return Response(data={"message": "Se enviara mensaje solo a ventas o preguntas"}, status=status.HTTP_202_ACCEPTED)

	def put(self, request, pk=None):
		token = Token.objects.get(id=1).token_ml
		question_data = requests.get("https://api.mercadolibre.com/questions/6191657439?access_token=" + token, timeout=10).json()
		a = self.suggested_answer(question_data)
		# print(a)
		return Response({"hola":a})


class AutomaticMessageViewSet(ModelViewSet):
	queryset = AutomaticMessage.objects.all().order_by('id')
	serializer_class = AutomaticMessageSerializer
	permission_classes = (IsAuthenticated,)
	http_methods_name = ['get', 'put',]

class EmailReceiverViewSet(ModelViewSet):
	queryset = EmailReceiver.objects.all()
	serializer_class = EmailReceiverSerializer
	permission_classes = (IsAuthenticated,)
	http_methods_name = ['get', 'post', 'delete']

class CustomMessageProductViewSet(ModelViewSet):
	queryset = CustomMessageProduct.objects.all().exclude(pack__pack_ripley__isnull=False)
	serializer_class = CustomMessageProductSerializer
	permission_classes = (IsAuthenticated,)
	http_methods_name = ['get', 'put',]
	pagination_class = CustomPagination
	filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter,)
	filter_class = CustomMessageProductFilter
	filter_fields = ('pack', )
	search_fields  = ('^product__sku', 'product__description', '^pack__sku_pack', 'pack__description')
class VideoUploadParser(FileUploadParser):
	media_type = 'video/*'

class VideoProceduresUploadView(APIView):
	parser_classes = (FileUploadParser,)

	def put(self, request, filename, format=None):
		file_obj = request.FILES['file']
		print(type(file_obj))
		video = Video.objects.create(name=filename, videofile=file_obj)
		# do some stuff with uploaded file
		return Response(status=204)

class VideoViewSet(ModelViewSet):
	queryset = Video.objects.all()
	serializer_class = VideoSerializer
	permission_classes = (AllowAny,)
	http_methods_name = ['get', 'post', 'put']

class UnanswerPagination(PageNumberPagination):
	page_size = 10
	page_size_query_param = 'page_size'
	max_page_size = 10
	def get_paginated_response(self, data):
		unanswer = Question.objects.filter(status_question="pending").count()
		return Response({
		   'next': self.get_next_link(),
		   'unanswer':unanswer,
		   'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'results': data,
		})

class QuestionViewSet(ModelViewSet):
	queryset = Question.objects.all().order_by('-id')
	serializer_class = QuestionSerializer
	pagination_class = UnanswerPagination
	permission_classes = (IsAuthenticated,)
	http_methods_name = ['get', 'post', 'put']
	filter_backends = (filters.DjangoFilterBackend,)
	filter_class = QuestionFilter
	filter_fields = ('suggested_answer', 'is_answer', 'status_question')

class QuestionsFileXlsx(APIView):
	permission_classes = (AllowAny,)
	def get(self, request):
		output = io.BytesIO()
		# file_name = 'Base de Preguntas y respuestas Mercadolibre.xlsx'
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("Hoja1")
		columns = [
			{"name": 'Fecha Pregunta', "width":12, "rotate": False}, 
			{"name": 'Hora Pregunta', "width":12, "rotate": False}, 
			{"name": 'Periodo', "width":12, "rotate": False}, 
			{"name": 'Publicación', "width":22, "rotate": False}, 
			{"name": 'Pregunta', "width":27, "rotate": False}, 
			{"name": 'Seller', "width":12, "rotate": False}, 
			{"name": 'Nickname', "width":12, "rotate": False}, 
			{"name": 'Respondida por:', "width":12, "rotate": False},
			{"name": 'Respuesta', "width":27, "rotate": False},
			{"name": 'Fecha Respuesta', "width":12, "rotate": False},
			{"name": 'Hora Respuesta', "width":12, "rotate": False},
			{"name": 'Tiempo de Respuesta', "width":12, "rotate": False}, 
			{"name": 'Compro', "width":12, "rotate": False}, 
			{"name": 'Incidencia', "width":12, "rotate": False}, 
		]

		row_num = 0
		cell_format_fields_90 = wb.add_format({'font_size': 9, 'border': 1})
		cell_format_fields_90.set_text_wrap()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_fields.set_text_wrap()
		cell_format_fields.set_align('vcenter')
		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])

		queryset = Question.objects.filter(status_question='answered', publication_data__isnull=False, answer_question__isnull=False).order_by('-id')
		serializer = QuestionSerializer(queryset, many=True)
		rows = serializer.data

		for i in rows:
			row_num += 1
			question = datetime.datetime.strptime(i['date_created'], "%Y-%m-%dT%H:%M:%S.%f")
			answer = datetime.datetime.strptime(i['answer_question']['date_answer'], "%Y-%m-%dT%H:%M:%S.%f")
			compro = "Si" if i['answer_question']['user_buy'] else "No"
			if question.weekday() < 5:
				if question.hour < 18 and question.hour > 8:
					period = 'L - V (09-18)'
				elif question.hour > 0 and question.hour < 9:
					period = 'L - V (00-08)'
				else:
					period = 'L - V (18-00)'
			else:
				period = "S - D"
			ws.write(row_num, 0, str(question.date()), cell_format_fields_90)
			ws.write(row_num, 1, str(question.time())[:8], cell_format_fields_90)
			ws.write(row_num, 2, period, cell_format_fields_90)
			ws.write(row_num, 3, str(i['publication_data']['title']), cell_format_fields_90)
			ws.write(row_num, 4, str(i['text']), cell_format_fields_90)
			ws.write(row_num, 5, str(i['publication_data']['seller_nickname']), cell_format_fields_90)
			ws.write(row_num, 6, str(i['user_nickname_ml']), cell_format_fields_90)
			ws.write(row_num, 7, str(i['answer_question']['user_answer']), cell_format_fields_90)
			ws.write(row_num, 8, str(i['answer_question']['answer']), cell_format_fields_90)
			ws.write(row_num, 9, str(answer.date()), cell_format_fields_90)
			ws.write(row_num, 10, str(answer.time())[:8], cell_format_fields_90)
			ws.write(row_num, 11, str(i['answer_question']['diference_question_answer']), cell_format_fields_90)
			ws.write(row_num, 12, str(compro), cell_format_fields_90)
			ws.write(row_num, 13, str(i['incidence']), cell_format_fields_90)
		
		ws.autofilter(0, 0, 0, len(columns) - 1)

		ws2 = wb.add_worksheet("Resumen Preguntas")

		blue_cell_format_fields = wb.add_format({'bold': True, 'bg_color': '#538ed5', 'border': 1, 'font_size': 10})
		blue_cell_format_fields.set_center_across()
		blue_cell_format_fields.set_text_wrap()
		blue_cell_format_fields.set_align('vcenter')

		gray_cell_format_fields = wb.add_format({'bold': True, 'bg_color': '#d9d9d9', 'border': 1, 'font_size': 10})
		gray_cell_format_fields.set_center_across()
		gray_cell_format_fields.set_text_wrap()
		gray_cell_format_fields.set_align('vcenter')

		normal_cell_format_fields = wb.add_format({'bold': True, 'border': 1, 'font_size': 10})
		normal_cell_format_fields.set_center_across()
		normal_cell_format_fields.set_text_wrap()
		normal_cell_format_fields.set_align('vcenter')

		columns = [
			{"name": 'Promedio Últimos 14 días', "width":12, "rotate": False, "col_num": 0, "keyword": "col1"}, 
			{"name": 'Tiempo Respuesta Todos', "width":12, "rotate": False, "col_num": 1,  "keyword": "col2"}, 
		]
		date_for_filter = (datetime.datetime.today() - timedelta(days=14)).date()
		questions = Question.objects.filter(status_question='answered', answer_question__isnull=False, date_created__date__gte=date_for_filter).annotate(answer_time=(F('answer_question__date_answer') - F('date_created')), period_question=Case(When(date_created__week_day__in=[1, 7], then=Value('S - D')),  When(Q(date_created__hour__lt=18) & Q(date_created__hour__gt=8), then=Value('L - V (09-18)')), When(Q(date_created__hour__gt=0) & Q(date_created__hour__lt=9), then=Value('L - V (00-08)')),default=Value('L - V (18-00)'), output_field=CharField())).values('answer_time', 'period_question', 'answer_question__user__first_name', 'answer_question__user__username', 'date_created__date')
		new_col_num = 2
		for y in questions.distinct('answer_question__user__username'):
			columns.append({"name": "Tiempo de Respuesta " + y['answer_question__user__first_name'], "width":12, "rotate": False, "col_num": new_col_num, "keyword": y['answer_question__user__username']})
			new_col_num += 1

		row_num = 0
		for col_num in range(len(columns)):
			ws2.write(row_num, columns[col_num]['col_num'], str(columns[col_num]['name']), cell_format_fields)
			ws2.set_column(col_num, columns[col_num]['col_num'], columns[col_num]['width'])
		rows_text = [
			{"period": "Lunes a Viernes \n09:00 - 18:00 (P1)", "col_num":1, "keyword": 'L - V (09-18)'},
			{"period": "Lunes a Viernes \n18:00 - 00:00 (P2)", "col_num":2, "keyword": 'L - V (18-00)'},
			{"period": "Lunes a Viernes \n00:00 - 08:00 (P3)", "col_num":3, "keyword": 'L - V (00-08)'},
			{"period": "Fin de semana (P4)", "col_num":4, "keyword": 'S - D'},
		]
		for col_num in range(len(rows_text)):
			ws2.write(rows_text[col_num]['col_num'], 0, str(rows_text[col_num]['period']), blue_cell_format_fields)
		sorted_order = sorted(questions, key=itemgetter('answer_question__user__username', 'period_question'))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:(x['answer_question__user__username'], x['period_question'])):
			group_order.append(list(group))
		data_resume = []
		for each in group_order:
			dict_resumen = {"period":each[0]['period_question'], "username": each[0]['answer_question__user__username'], "result": int(sum([i['answer_time'].seconds / 60 for i in each]) / len(each))}
			data_resume.append(dict_resumen)

		sorted_order2 = sorted(questions, key=itemgetter('period_question'))
		group_order2 = []
		for key, group in itertools.groupby(sorted_order2, key=lambda x:x['period_question']):
			group_order2.append(list(group))
		for each in group_order2:
			dict_resumen = {"period":each[0]['period_question'], "username": "col2", "result": int(sum([i['answer_time'].seconds / 60 for i in each]) / len(each))}
			data_resume.append(dict_resumen)

		for row in rows_text:
			for user in columns[0:len(columns)]:
				for y in data_resume:
					if row['keyword'] == y['period'] and user['keyword'] == y['username']:
						ws2.write(row['col_num'], user['col_num'], y['result'], cell_format_fields_90)
					else:
						continue
		ws3 = wb.add_worksheet("Tiempos días")

		columns2 = [
			{"name": 'Periodo', "width":12, "rotate": False, "col_num": 0, "keyword": "col1"}, 
			{"name": 'Día', "width":12, "rotate": False, "col_num": 1,  "keyword": "col3"}, 
			{"name": 'N° de Preguntas', "width":12, "rotate": False, "col_num": 2,  "keyword": "col3"}, 
			{"name": 'Tiempo Respuesta Todos', "width":12, "rotate": False, "col_num": 3,  "keyword": "col2"}, 
		]
		new_col_num = 4
		for y in questions.distinct('answer_question__user__username'):
			columns2.append({"name": "Tiempo de Respuesta " + y['answer_question__user__first_name'], "width":12, "rotate": False, "col_num": new_col_num, "keyword": y['answer_question__user__username']})
			new_col_num += 1
		row_num = 0
		for col_num in range(len(columns2)):
			ws3.write(row_num, columns2[col_num]['col_num'], str(columns2[col_num]['name']), cell_format_fields)
			ws3.set_column(col_num, columns2[col_num]['col_num'], columns2[col_num]['width'])

		rows_text2 = [
			{"period": "Lunes a Viernes \n09:00 - 18:00 (P1)", "col_num":1, "keyword": 'L - V (09-18)',"cell_format_data":normal_cell_format_fields ,"pivote":1 ,"dates": []},
			{"period": "Lunes a Viernes \n18:00 - 00:00 (P2)", "col_num":2, "keyword": 'L - V (18-00)', "cell_format_data": gray_cell_format_fields,"pivote":1,"dates": []},
			{"period": "Lunes a Viernes \n00:00 - 08:00 (P3)", "col_num":3, "keyword": 'L - V (00-08)', "cell_format_data":normal_cell_format_fields ,"pivote":1,"dates": []},
			{"period": "Fin de semana (P4)", "col_num":4, "keyword": 'S - D', "cell_format_data": gray_cell_format_fields,"pivote":1 ,"dates": []},
		]
		sorted_order3 = sorted(questions.order_by('date_created__date'), key=itemgetter('date_created__date', 'period_question'))
		group_order3 = []
		for key, group in itertools.groupby(sorted_order3, key=lambda x:(x['date_created__date'], x['period_question'])):
			group_order3.append(list(group))
		key_row = 1
		for u in rows_text2:
			key_row = key_row
			for k in group_order3:
				if u['keyword'] == k[0]['period_question']:
					u['dates'].append({"date":k[0]['date_created__date'], "pivote":key_row})
					key_row += 1
		row_start = 1
	
		for e in range(len(rows_text2)):
			for q in rows_text2[e]['dates']:
				ws3.write(row_start, 1, str(q['date']), rows_text2[e]['cell_format_data'])
				row_start += 1

		sorted_order4 = sorted(questions.order_by('date_created__date'), key=itemgetter('date_created__date', 'period_question', 'answer_question__user__username'))
		group_order4 = []
		for key, group in itertools.groupby(sorted_order4, key=lambda x:(x['date_created__date'], x['period_question'], x['answer_question__user__username'])):
			group_order4.append(list(group))
		data_resume2 = []
		for n in group_order4:
			dict_resumen = {"period":n[0]['period_question'], "username": n[0]['answer_question__user__username'], "result": int(sum([i['answer_time'].seconds / 60 for i in n]) / len(n)), "date_created": n[0]['date_created__date']}
			data_resume2.append(dict_resumen)

		sorted_order5 = sorted(questions.order_by('date_created__date'), key=itemgetter('period_question', 'date_created__date'))
		group_order5 = []
		for key, group in itertools.groupby(sorted_order5, key=lambda x:(x['period_question'], x['date_created__date'])):
			group_order5.append(list(group))
		for each in group_order5:
			dict_resumen = {"period":each[0]['period_question'], "username": "col2", "result": int(sum([i['answer_time'].seconds / 60 for i in each]) / len(each)), "date_created": each[0]['date_created__date'], "count_questions":len(each)}
			data_resume2.append(dict_resumen)
		row_start = 1
		format0 = wb.add_format({'bg_color':'white','border':1})
		for row in rows_text2:
			ws3.merge_range(row_start, 0, row['dates'][len(row['dates']) - 1]['pivote'], 0, str(row['period']), row['cell_format_data'])
			row_start = row['dates'][len(row['dates']) - 1]['pivote'] + 1
			for date in row['dates']:
				for user in columns2[0:len(columns2)]:
					for y in data_resume2:
						if row['keyword'] == y['period'] and user['keyword'] == y['username'] and date['date'] == y['date_created']:
							ws3.write(date['pivote'], user['col_num'], y['result'], cell_format_fields_90)
							if y['username'] == 'col2':
								ws3.write(date['pivote'], 2,y['count_questions'], cell_format_fields_90)
						else:
							continue
		last_row_ws3 =  rows_text2[len(rows_text2) - 1]['dates'][len(rows_text2[len(rows_text2) - 1]['dates']) - 1]['pivote']
		last_colum_ws3 =  columns2[len(columns2) - 1]['col_num']
		last_row_ws2 = rows_text[len(rows_text) - 1]['col_num']
		last_colum_ws2 = columns[len(columns) - 1]['col_num']
		ws3.conditional_format(0,0,last_row_ws3,last_colum_ws3, {'type':'blanks', 'format': format0})
		ws2.conditional_format(0,0,last_row_ws2,last_colum_ws2, {'type':'blanks', 'format': format0})

		wb.close()
		output.seek(0)
		filename = 'Informe de Preguntas.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response



class AnswerViewSet(ModelViewSet):
	queryset = Answer.objects.all()
	serializer_class = AnswerSerializer
	permission_classes = (IsAuthenticated,)
	http_methods_name = ['get', 'post', 'put']

class IntentionRobotViewSet(APIView):

	@transaction.atomic
	def post(self, request):
		# print(request.data)
		for word in request.data['selected_words']:
			if Word.objects.filter(text_word=word):
				continue
			else:
				# print(word + " palabra nueva")
				new_word = Word.objects.create(
					text_word=word,
					category_word='key_word',
				)
				if request.data['intention_type'] == 'generic':
					# Se asocia con todas las publicaciones
					# print(word + " generica")
					for publication in PublicationDataMercadolibre.objects.all():
						new_word.publication_asociated.add(publication)

				else:
					# Se asocia con la de la pregunta unicamente
					# print(word + " exclusiva sku")
					new_word.publication_asociated.add(PublicationDataMercadolibre.objects.get(publication_data_question__id=request.data['question_id']))
		for intention in request.data['intentions_question']:
			if Intention.objects.filter(text_intention=intention):
				continue
			else:
				# print(intention + " y respuesta")
				new_intention = Intention.objects.create(
					text_intention=intention,
				)
				new_response = response_robot.objects.create(
					intention=new_intention,
					positive_answer=request.data['positive_answer'],
					negative_answer=request.data['negative_answer'],
					condition_text=request.data['condition'],
				)
		for each in request.data['selected_words']:
			for intent in request.data['intentions_question']:
				if IntentionWords.objects.filter(word__text_word=each, intention__text_intention=intent):
					continue
				else:
					word = Word.objects.get(text_word=each)
					intention = Intention.objects.get(text_intention=intent)
					IntentionWords.objects.create(word=word, intention=intention)
		return Response(data={"hola": ['soy un robot']}, status=status.HTTP_200_OK)
