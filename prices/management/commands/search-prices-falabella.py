import re
from mechanize import Browser
from bs4 import BeautifulSoup as BS
import json
from sales.models import Product_SKU_Ripley
from prices.models import Price_Product
import datetime
from django.core.management.base import BaseCommand, CommandError
import time
import xlsxwriter
class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		sku_falabella = Product_SKU_Ripley.objects.filter(channel_id=8, code_seven_digits__isnull=False)
		wb = xlsxwriter.Workbook("Categorías Falabella con Código.xlsx")
		ws = wb.add_worksheet("Categorias")
		# print(sku_falabella.count())
		row_num = 0
		for each in sku_falabella:
			print(each.sku_ripley)
			br = Browser()
			br.set_handle_robots(False)
			br.addheaders = [('User-agent', 'Firefox')]
			try:
				br.open("https://www.falabella.com/falabella-cl/", timeout=3)
				for form in br.forms():
					if form.attrs.get('id', None) == 'searchForm':
						br.form = form
						break
				br.form['Ntt'] = each.sku_ripley
				br.submit()
				soup = BS(br.response().read(), 'lxml')
				bread_crumb = soup.findAll('b', {"class": "fb-masthead__breadcrumb__links"})
				span_ = bread_crumb[0].findAll('span', {"itemprop":'title'})
				links_ = bread_crumb[0].findAll('a', {"class":'fb-masthead__breadcrumb__link'}, href=True)
				last_category_id = links_[len(links_) - 1]['href']
				last_category_id = last_category_id.replace("/falabella-cl/category/","").split("/")[0]
				# print(last_category_id)
				categories_tree = ""
				for i in span_:
					categories_tree += i.text
				categories_tree = categories_tree.replace(u'\xa0', u' ').replace("Home /  ", "")
				ws.write(row_num, 0, each.sku_ripley)
				ws.write(row_num, 1, categories_tree)
				ws.write(row_num, 2, last_category_id)
				ws.write(row_num, 3, each.code_seven_digits)
				row_num += 1
			except Exception as e:
				continue
		wb.close()

			# data = soup.find_all('script')
			# script_selected = ""
			# for i in data:
			# 	if "var fbra_browseMainProductConfig" in i.text:
			# 		script_selected = i
			# 		break

			# if script_selected != "":
			# 	json_text = re.search(r'^\s*var fbra_browseMainProductConfig\s*=\s*({.*?})\s*;\s*$', script_selected.string, flags=re.DOTALL | re.MULTILINE).group(1)
			# 	data = json.loads(json_text)["state"]["product"]["prices"]

			# 	if len(data) == 2:
			# 		lower_price = int(data[0]['originalPrice'].replace(".",""))
			# 		# print("1")
			# 	elif len(data) == 3:
			# 		lower_price = int(data[0]['originalPrice'].replace(".",""))
			# 		middle_price = int(data[1]['originalPrice'].replace(".",""))
			# 		# print("2")
			# 	else:
			# 		print(data)

			# 	# print(data)
			# 	# offer_price = data[0]['originalPrice']
			# 	# page_price = int(offer_price.replace(".",""))
			# 	# print("Precio: " + str(page_price))
			# time.sleep(1.5)