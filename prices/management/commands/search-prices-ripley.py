import re
from mechanize import Browser
from bs4 import BeautifulSoup as BS
import json
from sales.models import Product_SKU_Ripley
from prices.models import Price_Product
import datetime
from django.core.management.base import BaseCommand, CommandError
import time
from OperationManagement.settings import API_KEY_RP
import requests
from prices.models import RipleyPublications, RipleyPriceVariation

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		url_ml = "https://ripley-prod.mirakl.net/api/offers?max=100"
		headers = {'Accept': 'application/json', 'Authorization': API_KEY_RP}
		data = requests.get(url_ml, headers=headers).json()
		if data['total_count'] <= 100:
			rows = data["offers"]
		else:
			rows = []
			count = 0
			iteration_end = int(data['total_count']/100)
			module = int(data['total_count']%100)
			for i in range(iteration_end + 1):
				new_rows = requests.get(url_ml + "&offset=" + str(i*100), headers=headers).json()['offers']
				rows = rows + new_rows
			rows = rows + requests.get(url_ml + "&offset="+str(((iteration_end*100) + module)), headers=headers).json()['offers']
		all_publications = []

		for each in rows:
			publication = RipleyPublications.objects.filter(sku_ripley=each['product_sku'])
			if publication:
				publication_used = publication[0]
			else:
				# Se crea nueva publicacion
				publication_used = RipleyPublications.objects.create(
					sku_ripley=each['product_sku'],
					title=each['product_title'],
					sku_seller=each['shop_sku'],
					category_ripley=each['category_label'],
					is_active=each['active'],
				)

			br = Browser()
			br.set_handle_robots(False)
			br.addheaders = [('User-agent', 'Firefox')]
			try:
				br.open("https://simple.ripley.cl/" + each['product_sku'].split("-")[0].lower())
				soup = BS(br.response().read(), 'lxml')
				e = soup.findAll('section', class_="product-info")
				prices = []
				if len(e) > 0:
					for i in e[0].find_all('li'):
						if "$" in i.find_all('span')[1].text:
							prices.append(int(i.find_all('span')[1].text.replace("$", "").replace(".", "")))
					prices.sort() 
					# print(prices)
					if len(prices) == 1:
						lowest_price = prices[0]
						middle_price = None
						higher_price = None
					elif len(prices) == 2:
						lowest_price = prices[0]
						middle_price = prices[1]
						higher_price = None
					elif len(prices) == 3:
						lowest_price = prices[0]
						middle_price = prices[1]
						higher_price = prices[2]
					else:
						print(prices)
						pass
						# Notificacion de nuevo precio

					# Se actualiza el precio para el SKU de Ripley
				else:
					lowest_price = None
					middle_price = None
					higher_price = None
					# print("Sin stock")
			except Exception as e:
				lowest_price = None
				middle_price = None
				higher_price = None
			# print(each['product_sku'])
			# print(lowest_price)

			ripley_variation = RipleyPriceVariation(
				ripley_publication=publication_used,
				lower_price=lowest_price,
				middle_price=middle_price,
				higher_price=higher_price,
				stock=each['quantity']
			)
			all_publications.append(ripley_variation)
		RipleyPriceVariation.objects.bulk_create(all_publications)
		print("Precios almacenados")


