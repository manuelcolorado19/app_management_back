from django.db import models
from sales.models import Product_Sale, Pack

class Price_Product(models.Model):
	normal_price = models.IntegerField(null=True)
	wholesaler_price = models.IntegerField(null=True)
	product_sale = models.OneToOneField(Product_Sale, null=True, blank=True, related_name='price_product_sale', on_delete=models.SET_NULL)
	pack = models.OneToOneField(Pack, null=True, blank=True, related_name='price_pack', on_delete=models.SET_NULL)
	falabella_price = models.IntegerField(null=True)
	mercadolibre_price = models.IntegerField(null=True)
	falabella_offer_price=models.IntegerField(null=True)
	date_start=models.DateField(null=True, blank=True)
	date_end=models.DateField(null=True, blank=True)

	class Meta:
		db_table = 'price_product'

class RipleyPublications(models.Model):
	sku_ripley = models.CharField(max_length=30)
	title = models.CharField(max_length=200)
	sku_seller = models.CharField(max_length=10)
	category_ripley = models.CharField(max_length=40)
	is_active = models.BooleanField(default=False)

	class Meta:
		db_table = 'ripley_publications_prices'

class RipleyPriceVariation(models.Model):
	ripley_publication = models.ForeignKey(RipleyPublications, on_delete=models.CASCADE, related_name='ripley_publication_variation')
	lower_price = models.IntegerField(null=True)
	middle_price = models.IntegerField(null=True)
	higher_price = models.IntegerField(null=True)
	stock = models.IntegerField(null=True)
	date_created = models.DateTimeField(auto_now_add=True)
	class Meta:
		db_table = 'ripley_publications_variations'