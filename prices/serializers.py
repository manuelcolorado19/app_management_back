from rest_framework import serializers
from .models import Price_Product

class SKUSerializer(serializers.IntegerField):
	def to_representation(self, value):
		return value.product_sale.sku

class DescriptionSerializer(serializers.IntegerField):
	def to_representation(self, value):
		return value.product_sale.description

class CategorySerializer(serializers.IntegerField):
	def to_representation(self, value):
		return value.product_sale.category_product_sale.name

class Price_ProductSerializer(serializers.ModelSerializer):
	sku = SKUSerializer(read_only=True, source='*')
	description = DescriptionSerializer(read_only=True, source='*')
	category_name = CategorySerializer(read_only=True, source='*')
	price_yapo = serializers.IntegerField(source='normal_price')
	class Meta:
		model = Price_Product
		fields = ('id','sku', 'description', 'category_name', 'price_yapo', 'wholesaler_price', 'mercadolibre_price', 'normal_price')