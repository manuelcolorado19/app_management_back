from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import Price_ProductViewSet

router = DefaultRouter()
router.register(r'price_product', Price_ProductViewSet, base_name='price_product')

urlpatterns = [
	url(r'^', include(router.urls)),
	
]
