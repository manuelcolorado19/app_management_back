from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from .models import Price_Product
from django.http import HttpResponse
from .serializers import Price_ProductSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated
import xlwt
import xlsxwriter
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import date, timedelta
from django.db import transaction
from sales.models import Product_Sale, Pack
import io
from django.db.models import Case, When, F
import datetime

class Price_ProductViewSet(ModelViewSet):
	queryset = Price_Product.objects.all()
	serializer_class = Price_ProductSerializer
	permission_classes = (IsAuthenticated,)
	http_method_names  = ['get', 'put', 'post']

	@transaction.atomic
	def create(self, request):
		for row in request.data:
			if Product_Sale.objects.filter(sku=row['sku']).count() == 0 and Pack.objects.filter(sku_pack=row['sku']).count() == 0:
				return Response(data={"sku": ["El número de sku " + str(row['sku']) + " no existe"]}, status=status.HTTP_400_BAD_REQUEST)
			if Price_Product.objects.filter(product_sale__sku=row['sku']).count() == 0 and Price_Product.objects.filter(pack__sku_pack=row['sku']).count() == 0:
				return Response(data={"sku": ["No existe registro de precio para el sku " + str(row['sku'])]}, status=status.HTTP_400_BAD_REQUEST)
		for row in request.data:
			if Product_Sale.objects.filter(sku=row['sku']):
				price_product = Price_Product.objects.get(product_sale__sku=row['sku'])
			else:
				price_product = Price_Product.objects.get(pack__sku_pack=row['sku'])
			price_product.normal_price = row['normal_price']
			price_product.wholesaler_price = row['wholesaler_price']
			price_product.presential_price = row['presential_price']
			price_product.mercadolibre_price = row['mercadolibre_price']
			price_product.save()

		return Response(data={'data': ['Datos cargados exitosamente.']}, status=status.HTTP_200_OK)

class Price_ProductXLSViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		today = date.today()
		output = io.BytesIO()
		file_name = 'Informe de precios ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		rows = Price_Product.objects.annotate(description=Case(When(product_sale=None, then=F('pack__description')), default=F('product_sale__description'))).values('description', 'normal_price', 'wholesaler_price', 'presential_price', 'mercadolibre_price')

		columns = [{"name": 'Producto o Pack', "width":40, "rotate": True}, {"name": 'Precio Normal(Presencial o Yapo)', "width": 15, "rotate": True}, {"name": 'Precio Mayorista', "width": 15, "rotate": True}, {"name": 'Precio Mercadolibre', "width": 15, "rotate": True}, {"name": 'Precio Falabella', "width": 15, "rotate": True}]

		number_columns = {"description": 0, "normal_price": 1, "wholesaler_price": 2, "presential_price": 3, "mercadolibre_price": 4}
		row_num = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
		for row in rows:
			row_num += 1

			for col_num in number_columns:
				if row[col_num] is not None:
					if isinstance(row[col_num], int) or isinstance(row[col_num], float):
						ws.write_number(row_num, number_columns[col_num], row[col_num], cell_format_data)
					else:
						ws.write(row_num, number_columns[col_num], str(row[col_num]), cell_format_data)
				else:
					ws.write(row_num, number_columns[col_num], "-", cell_format_data)
		ws.autofilter(0, 0, 0, len(columns) - 1)
		wb.close()
		output.seek(0)
		filename = 'Informe de precios Asiamerica.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response


class UploadPricesFalabella(APIView):
	def post(self, request):
		# print(request.data)
		for each in request.data:
			if Price_Product.objects.filter(product_sale__sku=str(each['sku'])[:5]):
				continue
			elif Price_Product.objects.filter(pack__sku_pack=str(each['sku'])[:5]):
				continue
			else:
				return Response(data={"product": ['Disculpe el sku ' + str(each['sku'][:5]) + ' no se encuentra en los registros']}, status=status.HTTP_400_BAD_REQUEST)
		for each in request.data:
			if Price_Product.objects.filter(product_sale__sku=str(each['sku'])[:5]):
				price = Price_Product.objects.filter(product_sale__sku=str(each['sku'])[:5])
			elif Price_Product.objects.filter(pack__sku_pack=str(each['sku'])[:5]):
				price = Price_Product.objects.filter(pack__sku_pack=str(each['sku'])[:5])
			else:
				price = None
			if price is not None:
				start_date = datetime.datetime.strptime(each['start_date'], "%d-%m-%Y")
				end_date = datetime.datetime.strptime(each['end_date'], "%d-%m-%Y")
				price.update(
					date_start=start_date,
					date_end=end_date,
					falabella_price=each['normal_price'],
					falabella_offer_price=each['offert_price'],
				)
		return Response(data={"prices": ['Precios cargados de forma exitosa']}, status=status.HTTP_200_OK)