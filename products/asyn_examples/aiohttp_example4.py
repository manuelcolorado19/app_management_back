import asyncio
import aiohttp
from bs4 import SoupStrainer, BeautifulSoup
from products.models import Product



@asyncio.coroutine
def get(*args, **kwargs):
    response = yield from aiohttp.request('GET', *args, **kwargs)
    return (yield from response.read_and_close(decode=True))




def first_magnet(page):
    soup = bs4.BeautifulSoup(page)
    # a = soup.find('a', title='Download this torrent using magnet')
    return "Page charged"


@asyncio.coroutine
def print_magnet(query):
    url = '{}'.format(query)
    page = yield from get(url, compress=True)
    magnet = first_magnet(page)
    print('{}'.format(query))

products = Product.objects.values_list('url_product', flat=True)[:50]
loop = asyncio.get_event_loop()
f = asyncio.wait([print_magnet(d) for d in products])
loop.run_until_complete(f)