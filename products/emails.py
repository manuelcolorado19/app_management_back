from django.template.loader import render_to_string
from django.core.mail import send_mail

def send_email_notification(self, to_email, channel):
	subject = 'hello'
	from_email = 'asiamerica@test.cl'
	to = "mejiasabelito@gmail.com"
	msg_html = render_to_string('market-analysis.html',
		{
			"name": "Abel Mejias",
			"url": "http://localhost:8000/export/xls/",
			"channel": "Mercadolibre"
		}
	)
	send_mail(
	   'Notificación de archivo',
	   subject,
	   from_email,
	   [to, ],
	   html_message=msg_html,
	)