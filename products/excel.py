from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Product, ProductVariation, ProductBranchOffice, BranchOffice
from .serializers import ProductSerializer
from rest_framework.permissions import AllowAny
import xlwt
import xlsxwriter
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import date, timedelta
from django.http import HttpResponse
from django.db.models import Prefetch, Sum
import datetime
import io
from django.http import StreamingHttpResponse


def retrieve_excel_ml(request):
	today = date.today()
	file_name = 'Análisis de Mercado Mercadolibre' + str(today)
	response = HttpResponse(content_type='application/ms-excel')
	response['Content-Disposition'] = 'attachment; filename=' + file_name + ".xlsx"
	wb = xlwt.Workbook(encoding='utf-8', style_compression=0)
	ws = wb.add_sheet(str(today), cell_overwrite_ok=True)
	alignment = xlwt.Alignment()
	alignment.horz = xlwt.Alignment.HORZ_CENTER
	style = xlwt.easyxf('font: bold on; align: rotation 90; pattern: pattern solid, fore_colour yellow; borders: top_color black, bottom_color black, right_color black, left_color black, left thin, right thin, top thin, bottom thin;')
	row_num = 0
	font_style = xlwt.XFStyle()
	font_style.font.bold = True
	font_style.alignment = alignment
	rows = Product.objects.filter(sub_category__category__channel__name='Mercadolibre').prefetch_related('product_variation').annotate(sale_total=Sum('product_variation__diference_day_before')).order_by('-sale_total')

	columns = [{"name": 'Código', "width":3700, "rotate": False}, {"name": 'Titulo', "width": 10000, "rotate": False}, {"name": 'Precio', "width": 2000, "rotate": False}, {"name": 'Cantidad Vendida', "width": 1700, "rotate": True}, {"name": 'Id Vendedor', "width": 2800, "rotate": False}, {"name": 'Cantidad disponible', "width": 1700, "rotate": True}, {"name": 'Url producto', "width": 4000, "rotate": False}, {"name": 'Precio original', "width": 2200, "rotate": True}, {"name": 'Fecha de culminación', "width": 3000, "rotate": True}, {"name": 'Promedio de Ranking', "width": 1100, "rotate": True}, {"name": 'Ventas Probables', "width": 1100, "rotate": True}]

	min_date = ProductVariation.objects.earliest('date_search').date_search
	delta = today - min_date
	dates = 0
	for i in range(delta.days + 1):
		# columns.append(min_date + timedelta(i))
		columns.append({"name": min_date + timedelta(i), "width": 1500, "rotate": True})
		dates += 1

	for col_num in range(len(columns)):
		ws.col(col_num).width = columns[col_num]['width']
		if columns[col_num]['rotate']:
			ws.write(row_num,col_num, str(columns[col_num]['name']), style=style)
		else:
			ws.write(row_num,col_num, str(columns[col_num]['name']), style=xlwt.easyxf('font: bold on; pattern: pattern solid, fore_colour yellow; borders: top_color black, bottom_color black, right_color black, left_color black, left thin, right thin, top thin, bottom thin;'))


	font_style = xlwt.XFStyle()
	number_columns = {"code": 0, "name": 1, "price": 2, "sold_quantity": 3, "seller_id": 4, "available_quantity": 5, "url_product": 6, "original_price": 7, "stop_time": 8, "rating": 9, "probably_sales": 10}
	for row in rows:
		row_num += 1
		for col_num in number_columns:
			if col_num == 'probably_sales':
				if row.sale_total:
					probably_sales = int(row.sale_total / dates)
				else:
					probably_sales = 0
				ws.write(row_num, number_columns[col_num], probably_sales, font_style)
			else:
				if col_num == 'stop_time':
					ws.write(row_num, number_columns[col_num], str(row.__dict__[col_num]), font_style)
				else:
					ws.write(row_num, number_columns[col_num], row.__dict__[col_num], font_style)
		for col_num in range(11, len(columns)):
			for sale in row._prefetched_objects_cache['product_variation']:
				if str(columns[col_num]['name']) == str(sale.date_search):
					ws.write(row_num, col_num, int(sale.sold_quantity), font_style)
	# ws.autofilter('A1:V1')
	# wb.save('products/files/Analisis Mercadolibre.xlsx')
	wb.save(response)
	return response

def retrieve_excel_pcfactory(request):
	today = date.today()
	output = io.BytesIO()
	file_name = 'Análisis Pc Factory' + str(today)
	wb =  xlsxwriter.Workbook(output)
	ws = wb.add_worksheet(str(today))
	rows = Product.objects.filter(sub_category__category__channel__name='Pcfactory').prefetch_related(Prefetch('product_branch_office', queryset=ProductBranchOffice.objects.order_by('id', 'office__order'))).order_by('id')[:60]
	columns = [{"name": 'Código', "width": 6.5, "rotate": False}, {"name": 'Titulo', "width": 40, "rotate": False}, {"name": 'Precio Efectivo', "width": 7, "rotate": True}, {"name": 'Cantidad disponible', "width": 5, "rotate": True}, {"name": 'Url producto', "width": 30, "rotate": False}, {"name": 'Precio original', "width": 7, "rotate": True}, {"name": 'Precio referencia', "width": 7, "rotate": True}]
	
	row_num = 0
	min_date = ProductBranchOffice.objects.earliest('date_search').date_search
	branch_office = BranchOffice.objects.values('name').order_by('order')
	delta = today - min_date
	date_quantity = 0
	counter = 0
	new_counter = 0
	offices_position = {}
	cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90})
	cell_format_fields_90.set_center_across()
	cell_format_fields = wb.add_format({'bold': True})
	cell_format_fields.set_center_across()
	cell_format_dates = wb.add_format({'bold': True,'bg_color': 'yellow', 'border': 1})
	cell_format_dates.set_center_across()
	cell_format_offices = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'gray', 'border': 1})
	cell_format_offices.set_center_across()
	for i in range(delta.days + 1):
		date_quantity += 1
		columns.append({"name": min_date + timedelta(i), "width": 4000, "rotate": False})
	for col_num in range(len(columns)):
		if col_num > 6:
			counter += 1
			if counter > 1 and counter < 3:
				real_col_num = col_num
				col_num = col_num + 30
				before_col_num = col_num
				new_counter += 1
			elif counter > 2:
				new_counter += 1
				real_col_num = col_num
				col_num = col_num + 30 * new_counter
				before_col_num = col_num
			else:
				pass
			if col_num > 30:
				for r in range(col_num, col_num + 31):
					offices_position[str(r)] = {"date_search": columns[real_col_num]['name']}
				ws.merge_range(0, col_num, 0, col_num + 30, str(columns[real_col_num]['name']), cell_format_dates)
			else:
				for r in range(col_num, col_num + 31):
					offices_position[str(r)] = {"date_search": columns[col_num]['name']}
				ws.merge_range(0, col_num, 0, col_num + 30, str(columns[col_num]['name']), cell_format_dates)
		else:
			if columns[col_num]['rotate']:
				ws.merge_range(0, col_num, 1, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.merge_range(0, col_num, 1, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
	row_num += 1
	real_col_num = 7
	
	for each_date in range(date_quantity):
		for col_num in range(branch_office.count()):
			ws.write(row_num, real_col_num, str(branch_office[col_num]['name']), cell_format_offices)
			ws.set_column(real_col_num, real_col_num, 3)
			if offices_position[str(real_col_num)]:
				offices_position[str(real_col_num)]["name_office"] = branch_office[col_num]['name']
			real_col_num += 1
	number_columns = {"code": 0, "name": 1, "price": 2, "available_quantity": 3, "url_product": 4, "original_price": 5, "referencial_price": 6}
	ws.set_column(1, 1, 40)
	for row in rows:
		row_num += 1
		for col_num in number_columns:
			ws.write(row_num, number_columns[col_num], row.__dict__[col_num])
		col_num = 7
		for s in range(7, (((delta.days + 1) * 31) + 7)):
			if str(s) in offices_position.keys():
				variation = ProductBranchOffice.objects.filter(office__name=offices_position[str(s)]['name_office'], date_search=offices_position[str(s)]['date_search'], product__name=row.name)
				if variation:
					ws.write(row_num, s, int(variation[0].branch_stock))
				else:
					ws.write(row_num, s, "")
			else:
				ws.write(row_num, s, "")
	# wb.save('products/files/Analisis Pcfactory con otro Formato.xlsx')
	ws.autofilter('A1:G1')
	wb.close()
	output.seek(0)
	filename = 'Análisis de Mercado Pc Factory.xlsx'
	response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
	response['Content-Disposition'] = 'attachment; filename=%s' % filename
	return response