import requests
from bs4 import BeautifulSoup, SoupStrainer
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from products.models import BranchOffice

url_offices = 'https://www.pcfactory.cl/producto/tiendas'
page_response = requests.get(url_offices, timeout=10)
content_offices = SoupStrainer('div', {'class': 'lp-cuerpo'})
page_content = BeautifulSoup(page_response.content, "html.parser", parse_only=content_offices)
content_all_offices = page_content.find('div', attrs={"class": "tiendasContenido"})
count = 0

for div in content_all_offices.find_all('div', attrs={"data-class": "TiendasSingle"}):
	name = div.find('button', attrs={"class": 'tiendas_accordion'}).find('span').text
	all_tr = div.find('table', attrs={"class": "datos_table"}).find_all('tr')
	span_address = all_tr[0].find_all('span')
	address = span_address[0].text + " " +span_address[1].text + span_address[2].text
	phone = all_tr[2].find('h4').text
	if "Santiago" in address:
		santiago = True
	else:
		santiago=False
	count += 1
	b = BranchOffice.objects.create(
		name=name,
		address=address,
		phone=phone,
		santiago=santiago,
		order=count
	)

	print("Office created")
