from time import time
from functools import partial
from openpyxl import load_workbook
from products.models import Product, ProductVariation
from django.db.models import Q
from categories.models import Channel
from datetime import date, timedelta
import datetime

min_date = datetime.datetime(2018,3,24)
today = datetime.datetime(2018,3,26)
start_time = time()

delta = today - min_date
dates = []
for i in range(delta.days + 1):
	dates.append(min_date + timedelta(i))

mlc_append = []
mlc_exists = []
for each in dates:
	month = ("0" + str(each.date().month)) if each.date().month < 10 else str(each.date().month) 
	day = ("0" + str(each.date().day)) if each.date().day < 10 else str(each.date().day) 
	filename = str(each.year) + month + day + ".xlsx"
	wb = load_workbook('products/files/' + filename, read_only=True)
	# wb = load_workbook('products/files/20180324.xlsx')
	print(filename)
	ws = wb['Hoja1']
	count = 0
	for row in ws.rows:
		if count == 0:
			for cell in range(len(row) - 1):
				name_cell = row[cell].value.lower()
				if name_cell == 'price':
					price = cell
				if name_cell == 'stock' or name_cell == 'available_quantity':
					stock = cell
				if name_cell == 'sold' or name_cell == 'sold_quantity':
					sold = cell
				if name_cell == 'item' or name_cell == 'id':
					item = cell
		else:
			if row[item].value in mlc_append:
				count += 1
				continue
			else:
				if Product.objects.filter(code=row[item].value, channel__id=1):
					# Cargar la data de los productvariation
					# if ProductVariation.objects.filter(date_search=each, product__code=row[item].value, product__channel__id=1):
					# 	count += 1
					# 	continue

					# else:
					# 	pass
					# 	# variation = ProductVariation(
					# 	# 	product_id=id_product,
			  #  #              sold_quantity=int(row[sold].value),
			  #  #              available_quantity=int(row[stock].value),
			  #  #              price=int(row[price].value),
			  #  #              date_search=each
					# 	# )
					if row[item].value in mlc_exists:
						pass
					else:
						mlc_exists.append(row[item].value)
					count += 1
					continue
				else:
					# Cargar a lista de productos faltantes para buscar datos con la api y luego ir al condicional de arriba
					if row[item].value is not None:
						mlc_append.append(row[item].value)
				# print(str(row[item].value) + " " + str(row[price].value) + " " + str(row[stock].value) + " " + str(row[sold].value))
		count += 1
	# print(len(mlc_append))
print(str(len(mlc_append)) + " mlc no existentes en la base")
print(str(len(mlc_exists)) + " mlc existentes en la base")
f= open("MLCNuevosJuan.txt","a+")
for j in mlc_append:
	f.write("%s\r" % (j))
f.close()
print("--- %s seconds ---" % (time() - start_time))

# print(len(mlc_append))