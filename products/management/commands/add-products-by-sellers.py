from django.core.management.base import BaseCommand, CommandError
from products.models import Product
import os
from time import time
import argparse
import asyncio
import sys
from aiohttp import ClientSession
import json
from datetime import datetime as dt
from sellers.models import Seller
from products.models import Product
from categories.models import Channel
from datetime import timedelta
import time as ti_me
channel=Channel.objects.get(id=1)
all_products = Product.objects.filter(channel__name="Mercadolibre").values_list('code', flat=True).distinct()
mlc_append = []
start_time = time()
initial_time = ti_me.strftime("%H:%M:%S")
products_bulk = []
with_exception = []
with open("BusquedaSellers" +".txt") as f:
	content = f.readlines()[:]
	for i in content:
		mlc_append.append(i.replace("\n", ""))
	repeated_remove = list(set(mlc_append) - set(all_products))[:1000]

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		print(len(repeated_remove))
		if repeated_remove:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()
		# print(len(repeated_remove))
async def run(session, number_of_requests, concurrent_limit):
	base_url = "https://api.mercadolibre.com/items/{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		url = base_url.format(i)
		async with session.get(url) as response:
			response = await response.read()
			sem.release()
			responses.append(response)
			data_json = json.loads(response)
			# print(data_json['title'])
			# Se añade el producto a la base
			seller = Seller.objects.filter(id_ml=data_json['seller_id'])
			if seller:
				seller = seller[0]
			else:
				seller = None
			try:
				product = Product(
					code=data_json['id'],
					name=data_json['title'],
					price=data_json.get('price', 0),
					sold_quantity=data_json.get('sold_quantity', 0),
					# sub_category=category,
					available_quantity=data_json.get('available_quantity', 0),
					url_product=data_json.get('permalink', ""),
					original_price=data_json['original_price'],
					stop_time=dt.strptime(data_json['stop_time'], "%Y-%m-%dT%H:%M:%S.%fZ") + timedelta(days=-7300),
					# rating=rating,
					seller=seller,
					seller_id_ml=data_json['seller_id'],
					from_seller_list=True,
					channel=channel,
				)
				products_bulk.append(product)
			except Exception as e:
				with_exception.append(i)
			return response

	for i in repeated_remove:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	print("total_products_append: {}".format(len(products_bulk)))
	Product.objects.bulk_create(products_bulk)
	print("--- %s seconds ---" % (time() - start_time))
	with open('LogProcess.txt', "a+") as f:
		f.write(str(dt.now().date())+ "\t" +"Etapa 2 Busqueda por lista de sellers" + "\t" + initial_time + "\t" + ti_me.strftime("%H:%M:%S") + "\t" + str(len(products_bulk)) + "\n")
		f.close()


async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return

