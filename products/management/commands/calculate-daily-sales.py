from products.models import Product
import os
from time import time
import argparse
import asyncio
import sys
# import resource
from aiohttp import ClientSession
import json
from datetime import datetime as dt
from sellers.models import Seller
from products.models import Product, ProductVariation
from categories.models import Channel
from django.core.management.base import BaseCommand, CommandError
from datetime import timedelta
from django.db.models import Prefetch, Sum, F, Func, Aggregate, FloatField, Case, When, Avg, IntegerField, Count, Q
all_products = Product.objects.filter(channel__id=1).order_by('id')[5000:10000]
start_time = time()

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		# print(len(all_products))
		if all_products:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()

async def run(session, number_of_requests, concurrent_limit):
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		# print(i.id)
		data_variations = i.product_variation.all().values('sold_quantity', 'projected_sale', 'is_projected', 'id', 'price').order_by('date_search')
		for each in range(1, data_variations.count()):
			# print(each)
			if data_variations[each - 1]:
				yesterday = data_variations[each - 1]['projected_sale'] if data_variations[each - 1]['is_projected'] else data_variations[each - 1]['sold_quantity']
				today = data_variations[each]['projected_sale'] if data_variations[each]['is_projected'] else data_variations[each]['sold_quantity']
				if today is not None and yesterday is not None:
					diference = today - yesterday
					if diference < 0:
						diference = None
					elif diference > 200:
						diference = None
					else:
						diference = diference
				else:
					diference = None
				if diference is not None and data_variations[each]['price'] is not None:
					daily_reward = diference * data_variations[each]['price']
				else:
					daily_reward = None
				ProductVariation.objects.filter(id=data_variations[each]['id']).update(diference_day_before=diference,daily_reward=daily_reward,)
				# print(str(diference) + " venta calculada")
		print("Venta calculada")

	for i in all_products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	print("total_products_append: {}".format(len(all_products)))
	print("5000")
	print("--- %s seconds ---" % (time() - start_time))

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return