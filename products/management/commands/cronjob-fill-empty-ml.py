from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.core.management.base import BaseCommand, CommandError
from products.models import Product, ProductVariation
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from django.utils import timezone
from datetime import date, timedelta
from django.contrib.postgres.aggregates.general import ArrayAgg
import math
from time import time
import argparse
import asyncio
import sys
# import resource
from aiohttp import ClientSession
start_time = time()
products = Product.objects.filter(channel__id=1,).order_by('id')[45000:50000]


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		# print(len(products))
		if products:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()

async def run(session, number_of_requests, concurrent_limit):
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(data):
		all_variations = data.product_variation.all().order_by('date_search')
		initial = all_variations.first()
		last = all_variations.last()
		range_days = (last.date_search - initial.date_search).days

		for i in range(1, range_days) :
			days_subtraction = (all_variations[i].date_search - initial.date_search).days
			if days_subtraction < 2 and days_subtraction > 0:
				initial = all_variations[i]
			else:
				initial_sold = initial.sold_quantity if initial.sold_quantity is not None else initial.projected_sale
				# if initial.sold_quantity is not None:
				# 	initial_sold = initial.sold_quantity
				# else:
				# 	initial_sold = initial.projected_sale
				if initial_sold == None:
					initial = ProductVariation.objects.create(
						product=data,
						price=initial.price,
						projected_sale=None,
						is_reused=True,
						date_search=(initial.date_search + timedelta(days=1))
					)
				else:
					# continue
					last = all_variations[i]
					if last.sold_quantity == None:
						# projected = None
						initial = ProductVariation.objects.create(
							product=data,
							price=initial.price,
							projected_sale=None,
							is_reused=True,
							date_search=(initial.date_search + timedelta(days=1))
						)
					else:
						a = math.ceil((last.sold_quantity - initial_sold) / (days_subtraction))
						projected = initial_sold + a
						if projected < initial_sold:
							initial = ProductVariation.objects.create(
								product=data,
								price=initial.price,
								projected_sale=None,
								is_reused=True,
								date_search=(initial.date_search + timedelta(days=1))
							)
							# continue
						else:
							initial = ProductVariation.objects.create(
								product=data,
								price=initial.price,
								projected_sale=projected,
								is_projected=True,
								date_search=(initial.date_search + timedelta(days=1))
							)
				all_variations = ProductVariation.objects.filter(product=data).order_by('date_search')
				# all_variations = data.product_variation.all().order_by('date_search')
		print("Variation Fill Empty Created")

	for i in products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(products)))
	print("45000")
	# print("total_sellers_append: {}".format(len(sellers_bulk)))
	print("--- %s seconds ---" % (time() - start_time))

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return

# class Command(BaseCommand):
#     args = ''
#     help = 'Export data to remote server'

#     def handle(self, *args, **options):
#     	start_time = time.time()
#     	data = []
#     	for product in products:
#     		data.append(product)
#     	pool = ThreadPool(20)
#     	results = pool.map(fill_empty_record_not_working_dates,data)
#     	pool.close()
#     	pool.join()
#     	self.send_email_notification()
#     	print("--- %s seconds ---" % (time.time() - start_time))

#     def send_email_notification(self):
#     	subject = 'hello'
#     	from_email = 'asiamerica@test.cl'
#     	to = "mejiasabelito@gmail.com"
#     	msg_html = render_to_string('market-analysis.html',
#     		{
#     			"name": "Abel Mejias",
#     			"url": "http://localhost:8000/export/xls/",
#     			"channel": "Mercadolibre Llenar vacios"
#     		}
#     	)
#     	send_mail(
# 		   'Notificación de archivo',
# 		   subject,
# 		   from_email,
# 		   [to, ],
# 		   html_message=msg_html,
# 		)

# def fill_empty_record_not_working_dates(data):
# 	today = date.today()
# 	min_date = ProductVariation.objects.earliest('date_search').date_search
# 	delta = today - min_date
# 	dates = []
# 	for i in range(delta.days):
# 		dates.append(min_date + timedelta(i))

# 	for eachDate in range(len(dates)):
# 		if dates[eachDate] in data.product_variation_dates: 
# 			last_day_record = dates[eachDate]
# 			continue
# 		else:
# 			if len(data.product_variation_dates) == 0:
# 				last_day_record = today
# 			# Este día no posee registro de actividad
# 			days_no_data = 1
# 			new_array = [dates[i] for i in range(eachDate, len(dates))]
# 			new_array.append(today)
# 			next_day = new_array[len(new_array) - 1]
# 			# print(new_array)
# 			for each_date_again in range(len(new_array)):
# 				if new_array[each_date_again] in data.product_variation_dates:
# 					next_day = new_array[each_date_again]
# 					break
# 				else:
# 					days_no_data += 1
# 					continue
# 			# print(last_day_record)
# 			# print(next_day)
# 			if not 'last_day_record' in locals():
# 				last_day_record = new_array[0]
# 			before_sold_quantity = ProductVariation.objects.filter(product=data, date_search=last_day_record)
# 			if before_sold_quantity:
# 				before_sold_quantity = before_sold_quantity[0].sold_quantity
# 			else:
# 				before_sold_quantity = 0
# 			after_sold_quantity = ProductVariation.objects.filter(product=data, date_search=next_day)
# 			if after_sold_quantity:
# 				after_sold_quantity = after_sold_quantity[0].sold_quantity
# 			else:
# 				after_sold_quantity = before_sold_quantity
# 			# print(str(after_sold_quantity.sold_quantity - before_sold_quantity.sold_quantity) + " con divisor " + str(days_no_data))
# 			average = round((after_sold_quantity - before_sold_quantity) / (days_no_data))
# 			# print(str(before_sold_quantity.sold_quantity) +"para el día " +str(before_sold_quantity.date_search))
# 			# print(str(after_sold_quantity.sold_quantity) +"para el día " +str(after_sold_quantity.date_search))
# 			# print(str(average) + " Para el producto " + data.name + " codigo " + data.code + " con fecha ")
# 			diference_day_before = before_sold_quantity + average - before_sold_quantity
# 			if diference_day_before < 0:
# 				diference_day_before = 0
			
# 			product_variation = ProductVariation.objects.create(
# 				product=data,
# 				sold_quantity=before_sold_quantity + average,
# 				available_quantity=data.available_quantity - average,
# 			  	diference_day_before=diference_day_before,
# 			  	date_search=dates[eachDate]
# 			)
# 			# print(product_variation.__dict__)
# 			last_day_record = dates[eachDate]

# def fill_empty_record_not_working_dates(data):
# 	all_variations = data.product_variation.all().order_by('date_search')
# 	initial = all_variations.first()
# 	last = all_variations.last()
# 	range_days = (last.date_search - initial.date_search).days

# 	for i in range(1, range_days) :
# 		days_subtraction = (all_variations[i].date_search - initial.date_search).days
# 		if  days_subtraction < 2 and days_subtraction > 0:
# 			initial = all_variations[i]
# 		else:
# 			if initial.sold_quantity is not None:
# 				initial_sold = initial.sold_quantity
# 			else:
# 				initial_sold = initial.projected_sale
# 			last = all_variations[i]
# 			a = math.ceil((last.sold_quantity - initial_sold) / (days_subtraction))
# 			projected = initial_sold + a
# 			print(str(projected) + " proyectada")
# 			# print(initial.date_search + timedelta(days=1))
# 			initial = ProductVariation.objects.create(
# 				product=data,
# 				price=initial.price,
# 				projected_sale=projected,
# 				is_projected=True,
# 				date_search=(initial.date_search + timedelta(days=1))
# 			)
# 			all_variations = ProductVariation.objects.filter(product=data).order_by('date_search')
# 			# all_variations = data.product_variation.all().order_by('date_search')

	
# 	print("Variation Fill Empty Created")

