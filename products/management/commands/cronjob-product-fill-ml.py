from categories.models import SubCategories
from products.models import Product
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from bs4 import SoupStrainer
from datetime import datetime as dt
import datetime as date_time
from sellers.models import Seller

from django.core.management.base import BaseCommand, CommandError
categories = SubCategories.objects.filter(category__channel__name='Mercadolibre')
# products_bulk = []
class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def handle(self, *args, **options):
    	start_time = time.time()
    	for category in categories:
    		# products_bulk = []
    		for offset in range(0, 1000, 50):
    			url_products_list = category.url_sub_category
    			try:
	    			request_made = requests.get(url_products_list + "&offset=" + str(offset), timeout=15).json()
	    			if request_made['paging']['offset'] >= request_made['paging']['primary_results']:
	    				list_products = request_made['results']
	    				data = []
	    				for product in list_products:
	    					data.append(product)
	    				pool = ThreadPool(15)
	    				results = pool.map(partial(get_price_from_url, category=category),data)
	    				pool.close()
	    				pool.join()
	    				break
	    			else:
	    				list_products = request_made['results']
	    				data = []
	    				for product in list_products:
	    					data.append(product)
	    				pool = ThreadPool(15)
	    				results = pool.map(partial(get_price_from_url, category=category),data)
	    				pool.close()
	    				pool.join()
	    		except requests.exceptions.Timeout:
	    			continue
    		# Product.objects.bulk_create(products_bulk)
    	print("--- %s seconds ---" % (time.time() - start_time))

def get_price_from_url(data, category):
	try:
		page_response = requests.get(data['permalink'], timeout=15)
		content_product = SoupStrainer(id='short-desc')
		page_content = BeautifulSoup(page_response.content, "html.parser", parse_only=content_product)
		content = page_content.find('div', attrs={"class": "item-conditions"})
		quantity_content = page_content.find('span', attrs={"class": "dropdown-quantity-available"})
		sold_quantity = 0
		if content is not None:
			content_split = content.text.split()
			for t in content_split:
				try:
					sold_quantity = int(t.replace('.','').replace(',','.'))
				except ValueError:
					pass
		else:
			sold_quantity = 0

		if quantity_content is not None:
			quantity_split = quantity_content.text.replace('(', '').replace(')', '').split()
			for a in quantity_split:
				try:
					quantity = int(a.replace('.','').replace(',','.'))
				except ValueError:
					pass
		else:
			quantity = 0

		if data['reviews']:
			rating = data['reviews']['rating_average']
		else:
			rating = None
		if Product.objects.filter(code=data['id'], url_product=data['permalink']):
			print("Product already created")
		else:
			seller = Seller.objects.filter(id=data['seller']['id'])
			if not seller:
				seller=None
			else:
				seller = seller[0]
			product = Product.objects.create(
				code=data['id'],
				name=data['title'],
				price=data['price'],
				sold_quantity=sold_quantity,
				sub_category=category,
			  	available_quantity=quantity,
			  	url_product=data['permalink'],
			  	original_price=data['original_price'],
			  	stop_time=dt.strptime(data['stop_time'], "%Y-%m-%dT%H:%M:%S.%fZ") + date_time.timedelta(days=-7300),
			  	rating=rating,
			  	seller=seller
				)
			# products_bulk.append(product)
			print("Product created")
	except requests.exceptions.Timeout:
		pass