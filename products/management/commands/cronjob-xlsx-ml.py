from django.core.management.base import BaseCommand, CommandError
import xlwt
import xlsxwriter
from django.db.models import Prefetch, Sum, F, Func, Aggregate, FloatField, Case, When, Avg, IntegerField, Count, Q
from datetime import date, timedelta
from products.models import Product, ProductVariation, ProductBranchOffice, BranchOffice
import datetime
import io
import time
import os, base64

datetime_7 = (datetime.datetime.today() - timedelta(days=7)).date()
datetime_15 = (datetime.datetime.today() - timedelta(days=15)).date()
datetime_30 = (datetime.datetime.today() - timedelta(days=30)).date()
datetime_60 = (datetime.datetime.today() - timedelta(days=60)).date()
datetime_90 = (datetime.datetime.today() - timedelta(days=90)).date()
today = date.today()
file_name = 'Analisis Mercadolibre ' + str(today) + " segundo.xlsx"
class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def handle(self, *args, **options):
        start_time = time.time()
        wb = xlsxwriter.Workbook(os.path.expanduser('~/Documents/Mercadolibre/' + file_name), {'constant_memory': True, 'strings_to_urls': False})
        ws = wb.add_worksheet(str(today))
        ws.freeze_panes(1, 0)
        rows = Product.objects.filter(
            sub_category__category__channel__name='Mercadolibre', from_seller_list=False
            ).prefetch_related('product_variation').annotate(
                sale_total=Sum('product_variation__diference_day_before'), 
                category_2=F('sub_category__name'), 
                category_1=F('sub_category__category__name'), 
                nickname=F('seller__nickname'), 
                last_7=Sum(
                    Case(
                        When(product_variation__date_search=datetime_7,
                            then=F('sold_quantity') - F('product_variation__sold_quantity')
                            ),
                        default=0,
                        output_field=IntegerField()
                )), 
                last_15=Sum(
                    Case(
                        When(product_variation__date_search=datetime_15,
                            then=F('sold_quantity') - F('product_variation__sold_quantity')
                            ),
                        default=0,
                        output_field=IntegerField()
                )),
                last_30=Sum(
                    Case(
                        When(product_variation__date_search=datetime_30,
                            then=F('sold_quantity') - F('product_variation__sold_quantity')
                            ),
                        default=0,
                        output_field=IntegerField()
                )),
                last_60=Sum(
                    Case(
                        When(product_variation__date_search=datetime_60,
                            then=F('sold_quantity') - F('product_variation__sold_quantity')
                            ),
                        default=0,
                        output_field=IntegerField()
                )),
                last_90=Sum(
                    Case(
                        When(product_variation__date_search=datetime_90,
                            then=F('sold_quantity') - F('product_variation__sold_quantity')
                            ),
                        default=0,
                        output_field=IntegerField()
                )),
                trend=F('sub_category'), 
                median=F('sub_category')
            )
        count = rows.count()
        columns = [{"name": 'Codigo', "width":12, "rotate": False}, {"name": 'Titulo', "width": 40, "rotate": False}, {"name": 'Categoria 1', "width": 14, "rotate": False}, {"name": 'Categoria 2', "width": 14, "rotate": False}, {"name": 'Ultima Categoría', "width": 14, "rotate": False},{"name": 'Precio', "width": 7.4, "rotate": True}, {"name": 'Cantidad Vendida', "width": 7, "rotate": True}, {"name": 'Id Vendedor', "width": 10, "rotate": True},{"name": 'Nickname', "width": 10, "rotate": True}, {"name": 'Cantidad disponible', "width": 5.5, "rotate": True}, {"name": 'Url producto', "width": 30, "rotate": False}, {"name": 'Fecha Publicada', "width": 10, "rotate": True}, {"name": 'Ventas Probables', "width": 5, "rotate": True}, {"name": 'Ultimos 7', "width": 5, "rotate": True}, {"name": 'Ultimos 15', "width": 5, "rotate": True}, {"name": 'Ultimos 30', "width": 5, "rotate": True}, {"name": 'Ultimos 60', "width": 5, "rotate": True}, {"name": 'Ultimos 90', "width": 5, "rotate": True}, {"name": 'Moda', "width": 8, "rotate": False}, {"name": 'Mediana', "width": 10, "rotate": False}]
        row_num = 0
        min_date = ProductVariation.objects.earliest('date_search').date_search
        delta = today - min_date
        dates = 0
        cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
        cell_format_fields_90.set_center_across()
        cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
        cell_format_fields.set_center_across()
        cell_format_data = wb.add_format({'font_size': 9.5})
        for i in range(delta.days + 1):
            columns.append({"name": min_date + timedelta(i), "width": 5.5, "rotate": True})
            dates += 1
        for col_num in range(len(columns)):
            if columns[col_num]['rotate']:
                ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
                ws.set_column(col_num, col_num, columns[col_num]['width'])
            else:
                ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
                ws.set_column(col_num, col_num, columns[col_num]['width'])
        number_columns = {"code": 0, "name": 1, "category_1": 2, "category_2": 3, "last_category": 4,"price": 5, "sold_quantity": 6, "seller_id": 7, "nickname": 8,"available_quantity": 9, "url_product": 10, "stop_time": 11, "probably_sales": 12, "last_7": 13, "last_15": 14, "last_30": 15, "last_60": 16, "last_90": 17, "trend": 18, "median": 19}
        for i in range(0, count, chunk_size):
            print("Hi")
            rows = Product.objects.filter(
                sub_category__category__channel__name='Mercadolibre', from_seller_list=False
                ).prefetch_related('product_variation').annotate(
                    sale_total=Sum('product_variation__diference_day_before'), 
                    category_2=F('sub_category__name'), 
                    category_1=F('sub_category__category__name'), 
                    nickname=F('seller__nickname'), 
                    last_7=Sum(
                        Case(
                            When(product_variation__date_search=datetime_7,
                                then=F('sold_quantity') - F('product_variation__sold_quantity')
                                ),
                            default=0,
                            output_field=IntegerField()
                    )), 
                    last_15=Sum(
                        Case(
                            When(product_variation__date_search=datetime_15,
                                then=F('sold_quantity') - F('product_variation__sold_quantity')
                                ),
                            default=0,
                            output_field=IntegerField()
                    )),
                    last_30=Sum(
                        Case(
                            When(product_variation__date_search=datetime_30,
                                then=F('sold_quantity') - F('product_variation__sold_quantity')
                                ),
                            default=0,
                            output_field=IntegerField()
                    )),
                    last_60=Sum(
                        Case(
                            When(product_variation__date_search=datetime_60,
                                then=F('sold_quantity') - F('product_variation__sold_quantity')
                                ),
                            default=0,
                            output_field=IntegerField()
                    )),
                    last_90=Sum(
                        Case(
                            When(product_variation__date_search=datetime_90,
                                then=F('sold_quantity') - F('product_variation__sold_quantity')
                                ),
                            default=0,
                            output_field=IntegerField()
                    )),
                    trend=F('sub_category'), 
                    median=F('sub_category')
                )[i:i+chunk_size]
            for row in rows:
		#print("l")
                row_num += 1
                row.__dict__['url_product'] = str(row.__dict__['url_product'])
                for col_num in number_columns:
                    if col_num == 'probably_sales':
                        if row.sale_total:
                            probably_sales = int(row.sale_total / dates)
                        else:
                            probably_sales = 0
                        ws.write(row_num, number_columns[col_num], probably_sales, cell_format_data)
                    else:
                        if col_num == 'stop_time':
                            ws.write(row_num, number_columns[col_num], str(row.__dict__[col_num]), cell_format_data)
                        else:
                            ws.write(row_num, number_columns[col_num], row.__dict__[col_num], cell_format_data)
                for col_num in range(20, len(columns)):
                    for sale in row._prefetched_objects_cache['product_variation']:
                        if str(columns[col_num]['name']) == str(sale.date_search):
                            ws.write(row_num, col_num, int(sale.sold_quantity), cell_format_data)
        ws.autofilter(0, 0, 0, len(columns) - 1)
        wb.close()
        print("--- %s seconds ---" % (time.time() - start_time))
