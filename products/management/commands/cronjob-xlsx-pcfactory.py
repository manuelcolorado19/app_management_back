from django.core.management.base import BaseCommand, CommandError
import xlwt
import xlsxwriter
from django.db.models import Prefetch, Sum, F, Func, Aggregate, FloatField, Case, When, Avg, IntegerField, Count, Q
from datetime import date, timedelta
from products.models import Product, ProductVariation, ProductBranchOffice, BranchOffice
import datetime
import io
import time
import os, base64
today = date.today()
from multiprocessing.dummy import Pool as ThreadPool
import multiprocessing

file_name = 'Analisis Pc Factory ' + str(today) + ".xlsx"
wb = xlsxwriter.Workbook(os.path.expanduser('~/Documents/PcFactory/' + file_name), {'constant_memory': True, 'strings_to_urls': False})
ws = wb.add_worksheet(str(today))
#file_name = 'Analisis Pc Factory ' + str(today) + "2.xlsx"
class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

    def handle(self, *args, **options):
    	start_time = time.time()
    	today = date.today()
    	# file_name = 'Análisis de Mercado Pc Factory' + str(today)
    	wb = xlsxwriter.Workbook(os.path.expanduser('~/Documents/PcFactory/' + file_name), {'constant_memory': True, 'strings_to_urls': False})
    	ws = wb.add_worksheet(str(today))
    	# ws.freeze_panes(1, 0)
    	# ws.freeze_panes('A2')
    	ws.freeze_panes(2, 7)
    	# rows = Product.objects.filter(sub_category__category__channel__name='Pcfactory').prefetch_related(Prefetch('product_branch_office', queryset=ProductBranchOffice.objects.order_by('id', 'office__order'))).order_by('id')[:1000]
    	query = Product.objects.filter(sub_category__category__channel__name='Pcfactory').prefetch_related(Prefetch('product_branch_office', queryset=ProductBranchOffice.objects.order_by('id')))[:200]
    	print("Here")
    	rows = [{'code': parent.code, "name": parent.name, "price": parent.price, "available_quantity": parent.available_quantity, "url_product": parent.url_product, "original_price": parent.original_price, "referencial_price": parent.referencial_price, 'children': [{'name_office': child.office.name, 'date_search':child.date_search, 'branch_stock': child.branch_stock} for child in parent.product_branch_office.all()]} for parent in query]
    	print("Now here")
    	columns = [{"name": 'Código',  "width": 6.5, "rotate": False}, {"name": 'Titulo', "width": 40, "rotate": False}, {"name": 'Precio Efectivo', "width": 7, "rotate": True}, {"name": 'Cantidad disponible', "width": 5, "rotate": True}, {"name": 'Url producto', "width": 30, "rotate": False}, {"name": 'Precio original', "width": 7, "rotate": True}, {"name": 'Precio referencia', "width": 7, "rotate": True}]
    	row_num = 0
    	min_date = ProductBranchOffice.objects.earliest('date_search').date_search
	#print(min_date)
    	branch_office = BranchOffice.objects.values('name').order_by('order')
    	delta = today - min_date
    	date_quantity = 0
    	counter = 0
    	new_counter = 0
    	offices_position = {}
    	cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'font_size': 10})
    	cell_format_fields_90.set_center_across()
    	cell_format_fields = wb.add_format({'bold': True, 'font_size': 10})
    	cell_format_fields.set_center_across()
    	cell_format_dates = wb.add_format({'bold': True,'bg_color': 'yellow', 'border': 1, 'font_size': 10})
    	cell_format_dates.set_center_across()
    	cell_format_offices = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'gray', 'border': 1, 'font_size': 9})
    	cell_format_offices.set_center_across()
    	for i in range(delta.days + 1):
    		date_quantity += 1
    		columns.append({"name": min_date + timedelta(i), "width": 4000, "rotate": False})
    	# print(columns)
    	for col_num in range(len(columns)):
    		if col_num > 6:
    			counter += 1
    			if counter > 1 and counter < 3:
    				real_col_num = col_num
    				col_num = col_num + 30
    				before_col_num = col_num
    				new_counter += 1
    			elif counter > 2:
    				new_counter += 1
    				real_col_num = col_num
    				col_num = col_num + 30 * new_counter
    				before_col_num = col_num
    			else:
    				pass
    			if col_num > 30:
    				print(str(columns[real_col_num]['name']))
    				for r in range(col_num, col_num + 31):
    					offices_position[str(r)] = {"date_search": columns[real_col_num]['name']}
    				ws.merge_range(0, col_num, 0, col_num + 30, str(columns[real_col_num]['name']), cell_format_dates)
    			else:
    				for r in range(col_num, col_num + 31):
    					offices_position[str(r)] = {"date_search": columns[col_num]['name']}
    				ws.merge_range(0, col_num, 0, col_num + 30, str(columns[col_num]['name']), cell_format_dates)
    		else:
    			# print("hola")
    			if columns[col_num]['rotate']:
    				# print(str(col_num))
    				ws.merge_range(0, col_num, 1, col_num, str(columns[col_num]['name']), cell_format_fields_90)
    				ws.set_column(col_num, col_num, columns[col_num]['width'])
    			else:
    				print(columns[col_num]['name'])

    				ws.merge_range(0, col_num, 1, col_num, str(columns[col_num]['name']), cell_format_fields)
    				ws.set_column(col_num, col_num, columns[col_num]['width'])
    	row_num += 1
    	real_col_num = 7
    	for each_date in range(date_quantity):
    		for col_num in range(branch_office.count()):
    			ws.write(row_num, real_col_num, str(branch_office[col_num]['name']), cell_format_offices)
    			ws.set_column(real_col_num, real_col_num, 3)
    			if offices_position[str(real_col_num)]:
    				offices_position[str(real_col_num)]["name_office"] = branch_office[col_num]['name']
    			real_col_num += 1
    	number_columns = {"code": 0, "name": 1, "price": 2, "available_quantity": 3, "url_product": 4, "original_price": 5, "referencial_price": 6}
    	ws.set_column(1, 1, 40)
    	cell_format_data = wb.add_format({'font_size': 9.5})
    	for row in rows:
    		print(row_num)
    		row_num += 1
    		for col_num in number_columns:
    			ws.write(row_num, number_columns[col_num], row[col_num], cell_format_data)
    		col_num = 7
    		for child in row['children']:
    			for s in range(7, (((delta.days + 1) * 31) + 7)):
    				if child['name_office'] == offices_position[str(s)]['name_office'] and child['date_search'] == offices_position[str(s)]['date_search']:
    					ws.write(row_num, s, int(child['branch_stock']), cell_format_data)
    	ws.autofilter('A2:ZZ2')
    	wb.close()
    	print("--- %s seconds ---" % (time.time() - start_time))
		# return response
