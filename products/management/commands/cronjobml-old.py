from django.core.management.base import BaseCommand, CommandError
from products.models import Product, ProductVariation
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
from time import time
from functools import partial
from django.template.loader import render_to_string
from django.core.mail import send_mail
from bs4 import SoupStrainer
from django.utils import timezone
from datetime import timedelta
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
# import requests
import time as ti_me
from datetime import datetime as dt
initial_time = ti_me.strftime("%H:%M:%S")
new_data = []
products = []
start_time = time()
today_date = timezone.now()
content_product = SoupStrainer(id='short-desc')
variations = ProductVariation.objects.filter(date_search=today_date).values_list('product__id', flat=True)
all_products = Product.objects.filter(channel__id=1, url_product__isnull=False).exclude(id__in=variations)[:200]

list_variations = []
class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

    def handle(self, *args, **options):
    	start_time = time()
    	data = []
    	for product in products:
    		data.append(product)
    	pool = ThreadPool(20)
    	results = pool.map(save_quantity_and_available_from_url, data)
    	pool.close()
    	pool.join()
    	ProductVariation.objects.bulk_create(list_variations)
    	f= open("Complete load Mercadolibre.txt","a+")
    	f.write("Complete date: %s\r" % (str(today_date)))
    	f.close()
    	#self.send_email_notification()
    	print("--- %s seconds ---" % (time() - start_time))

	def send_email_notification(self):
		subject = 'hello'
		from_email = 'asiamerica@test.cl'
		to = "mejiasabelito@gmail.com"
		msg_html = render_to_string('market-analysis.html',
			{
				"name": "Abel Mejias",
				"url": "http://localhost:8000/export/xls/",
				"channel": "Mercadolibre"
			}
		)
		send_mail(
		   'Notificación de archivo',
		   subject,
		   from_email,
		   [to, ],
		   html_message=msg_html,
		)

def save_quantity_and_available_from_url(data):
	# try:
	page_response = requests.get(data.url_product, timeout=15).content
	content_product = SoupStrainer(id='short-desc')
	page_content = BeautifulSoup(page_response, "lxml", parse_only=content_product)

	# page_content = BeautifulSoup(response.decode('utf-8'), 'lxml')
	content = page_content.find('div', attrs={"class": "item-conditions"})
	quantity_content = page_content.find('span', attrs={"class": "dropdown-quantity-available"})
	price_content = page_content.find('span', attrs={"class": "price-tag-fraction"})
	sold_quantity = 0

	if content is not None:
		content_split = content.text.split()
		for t in content_split:
			try:
				sold_quantity = int(t.replace('.','').replace(',','.'))
			except ValueError:
				pass
	else:
		sold_quantity = 0
	if quantity_content is not None:
		quantity_split = quantity_content.text.replace('(', '').replace(')', '').split()
		for a in quantity_split:
			try:
				quantity = int(a.replace('.','').replace(',','.'))
			except ValueError:
				pass
	else:
		quantity = 1
	if price_content is not None:
		price_split = price_content.text.replace('(', '').replace(')', '').split()
		for m in price_split:
			try:
				price = int(m.replace('.','').replace(',','.'))
			except ValueError:
				pass
	else:
		price = 0
	list_variations.append(
		ProductVariation(
			product_id=data.id,
			sold_quantity=sold_quantity,
			available_quantity=quantity,
			price=price,
			# diference_day_before=diference_day_before
		)
	)

	# return response
	# except Exception as e:
	# # print(e)
	# 	pass

# def save_quantity_and_available_from_url(data):
# 	if not ProductVariation.objects.filter(product=data, date_search=today_date):
# 		page_response = s.get(data.url_product, timeout=15).content
# 		content_product = SoupStrainer(id='short-desc')
# 		page_content = BeautifulSoup(page_response, "html.parser", parse_only=content_product)
# 		content = page_content.find('div', attrs={"class": "item-conditions"})
# 		quantity_content = page_content.find('span', attrs={"class": "dropdown-quantity-available"})
# 		price_content = page_content.find('span', attrs={"class": "price-tag-fraction"})
# 		sold_quantity = 0
# 		if content is not None:
# 			content_split = content.text.split()
# 			for t in content_split:
# 				try:
# 					sold_quantity = int(t.replace('.','').replace(',','.'))
# 				except ValueError:
# 					pass
# 		else:
# 			sold_quantity = 0
# 		if quantity_content is not None:
# 			quantity_split = quantity_content.text.replace('(', '').replace(')', '').split()
# 			for a in quantity_split:
# 				try:
# 					quantity = int(a.replace('.','').replace(',','.'))
# 				except ValueError:
# 					pass
# 		else:
# 			quantity = 0
# 		if price_content is not None:
# 			price_split = price_content.text.replace('(', '').replace(')', '').split()
# 			for m in price_split:
# 				try:
# 					price = int(m.replace('.','').replace(',','.'))
# 				except ValueError:
# 					pass
# 		else:
# 			price = 0

# 		day_before = ProductVariation.objects.filter(product=data, date_search=(today_date - date_before))
# 		if day_before:
# 			sold_day_before = day_before[0].sold_quantity
# 		else:
# 			sold_day_before = sold_quantity

# 			diference_day_before = sold_quantity - sold_day_before
# 			if diference_day_before < 0:
# 				diference_day_before = 0
# 			# product_variation = ProductVariation.objects.create(
# 			# 	product=data,
# 			# 	sold_quantity=sold_quantity,
# 			# 	available_quantity=quantity,
# 			#   	diference_day_before=diference_day_before
# 			# 	)
# 			# Second way
			
# 			list_variations.append(
# 				ProductVariation(
# 					product=data,
# 					sold_quantity=sold_quantity,
# 					available_quantity=quantity,
# 				  	diference_day_before=diference_day_before
# 				)
# 			)
# 			product_update = Product.objects.filter(id=data.id).update(sold_quantity=sold_quantity, available_quantity=quantity, price=price)
# 			# product_update.sold_quantity = sold_quantity
# 			# product_update.available_quantity = quantity
# 			# product_update.price = price
# 			# product_update.save()
# 			print("Variation Created")
# 	else:
# 		print("Variation already exists")
		
