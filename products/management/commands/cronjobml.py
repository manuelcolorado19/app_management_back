from django.core.management.base import BaseCommand, CommandError
from products.models import Product, ProductVariation
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
from time import time
from functools import partial
from django.template.loader import render_to_string
from django.core.mail import send_mail
from bs4 import SoupStrainer
from django.utils import timezone
from datetime import timedelta
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from django.contrib.postgres.aggregates.general import ArrayAgg
import asyncio
from aiohttp import ClientSession

today_date = timezone.now()
date_before = timedelta(1)
variations = ProductVariation.objects.filter(date_search=today_date).aggregate(id_products=ArrayAgg('product_id'))['id_products']
products = Product.objects.filter(sub_category__category__channel__name='Mercadolibre', from_seller_list=False).exclude(id__in=variations)[:1000]
list_responses = []
start_time = time()
content_product = SoupStrainer(id='short-desc')
list_variations = []
# print(products[0].__dict__)

class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def handle(self, *args, **options):
    	number = 10000
    	loop = asyncio.get_event_loop()
    	future = asyncio.ensure_future(run(number))
    	loop.run_until_complete(future)
    	# ProductVariation.objects.bulk_create(list_variations)
    	print(str(len(list_variations)) + " variaciones creadas")
    	#if len(list_variations) == 0:
    		#self.send_email_notification()

    	print("--- %s seconds ---" % (time() - start_time))

    def send_email_notification(self):
    	subject = 'hello'
    	from_email = 'asiamerica@test.cl'
    	to = "mejiasabelito@gmail.com"
    	msg_html = render_to_string('market-analysis.html',
    		{
    			"name": "Abel Mejias",
    			"url": "http://localhost:8000/export/xls/",
    			"channel": "Mercadolibre"
    		}
    	)
    	send_mail(
		   'Notificación de archivo',
		   subject,
		   from_email,
		   [to, ],
		   html_message=msg_html,
		)

async def save_quantity_and_available_from_url(data, page_content):
    content = page_content.find('div', attrs={"class": "item-conditions"})
    quantity_content = page_content.find('span', attrs={"class": "dropdown-quantity-available"})
    price_content = page_content.find('span', attrs={"class": "price-tag-fraction"})
    last_category_content = page_content.findAll('a', attrs={"class": "breadcrumb"})
    # print()
    last_category = " ".join(last_category_content[len(last_category_content) - 1].text.split())
    sold_quantity = 0
    
    if content is not None:
        content_split = content.text.split()
        for t in content_split:
            try:
                sold_quantity = int(t.replace('.','').replace(',','.'))
            except ValueError:
                pass
    else:
        sold_quantity = 0
    if quantity_content is not None:
        quantity_split = quantity_content.text.replace('(', '').replace(')', '').split()
        for a in quantity_split:
            try:
                quantity = int(a.replace('.','').replace(',','.'))
            except ValueError:
                pass
    else:
        quantity = 0
    if price_content is not None:
        price_split = price_content.text.replace('(', '').replace(')', '').split()
        for m in price_split:
            try:
                price = int(m.replace('.','').replace(',','.'))
            except ValueError:
                pass
    else:
        price = 0

    day_before = ProductVariation.objects.filter(product=data, date_search=(today_date - date_before))
    if day_before:
        sold_day_before = day_before[0].sold_quantity
    else:
        sold_day_before = sold_quantity

    if not ProductVariation.objects.filter(product=data, date_search=today_date):
        diference_day_before = sold_quantity - sold_day_before
        if diference_day_before < 0:
            diference_day_before = 0
        # Second way
        
        list_variations.append(
            ProductVariation(
                product=data,
                sold_quantity=sold_quantity,
                available_quantity=quantity,
                diference_day_before=diference_day_before
            )
        )
        product_update = Product.objects.filter(id=data.id).update(
            sold_quantity=sold_quantity, 
            available_quantity=quantity, 
            price=price,
            last_category=last_category
        )

        # print("Variation append")
    else:
        # print("Variation already exists")
        pass

async def fetch(url, session, product):
    try:
        async with session.get(url, timeout=20) as response:
            text = await response.read()
            # page_content = BeautifulSoup(text.decode('utf-8'), content_product)
            # await save_quantity_and_available_from_url(product, page_content)
        return text
    except Exception as e:
        pass

async def bound_fetch(sem, url, session, product):
    async with sem:
        await fetch(url, session, product)


async def run(r):
    
    tasks = []
    # create instance of Semaphore
    sem = asyncio.Semaphore(1000)

    async with ClientSession() as session:
        for product in products:
            task = asyncio.ensure_future(bound_fetch(sem, product.url_product, session, product))
            tasks.append(task)

        responses = asyncio.gather(*tasks)
        await responses


