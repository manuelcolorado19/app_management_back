from functools import partial
from django.template.loader import render_to_string
from django.core.mail import send_mail
from categories.models import SubCategories
from products.models import Product, ProductBranchOffice, BranchOffice, PriceVariationPcFactory
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from bs4 import SoupStrainer
from datetime import datetime as dt
import datetime as date_time
from django.core.management.base import BaseCommand, CommandError

products = Product.objects.filter(sub_category__category__channel__name='Pcfactory')
list_branch_offices_bulk = []
new_product_price_variation = []
today = dt.now()
class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def handle(self, *args, **options):
    	start_time = time.time()
    	data = []
    	for product in products:
    		data.append(product)
    	pool = ThreadPool(20)
    	results = pool.map(save_quantity_and_available_from_url,data)
    	pool.close()
    	pool.join()
    	ProductBranchOffice.objects.bulk_create(list_branch_offices_bulk)
    	PriceVariationPcFactory.objects.bulk_create(new_product_price_variation)
    	self.send_email_notification()
    	f= open("Complete load PcFactory.txt","a+")
    	f.write("Complete date: %s\r" % (str(today)))
    	f.close()
    	print("--- %s seconds ---" % (time.time() - start_time))

    def send_email_notification(self):
    	subject = 'hello'
    	from_email = 'asiamerica@test.cl'
    	to = "mejiasabelito@gmail.com"
    	msg_html = render_to_string('market-analysis.html',
    		{
    			"name": "Abel Mejias",
    			"url": "http://localhost:8000/export/xls/",
    			"channel": "Pcfactory"
    		}
    	)
    	send_mail(
		   'Notificación de archivo',
		   subject,
		   from_email,
		   [to, ],
		   html_message=msg_html,
		)

def save_quantity_and_available_from_url(data):
	page_response = requests.get(data.url_product, timeout=15)
	content_product = SoupStrainer('div', {'class': 'ficha_cuerpo'})
	page_content = BeautifulSoup(page_response.content, "html.parser", parse_only=content_product)
	content = page_content.find('div', attrs={"class": "ficha_producto_right"})

	if content:
		price_content = content.find('div', attrs={"class": "ficha_precio_efectivo"}).find('h2')
		original_price_content = content.find('div', attrs={"class": "ficha_precio_normal"}).find('h2')
		referencial_price_content = content.find('div', attrs={"class": "ficha_precio_referencial"})
		# print("Este es "+data.name + "con el codigo "+data.code)
		if price_content is not None:
			for t in price_content.text.split():
				try:
					price = int(t.replace('.','').replace(',','.'))
				except ValueError:
					pass
		else:
			price = 0
		if original_price_content is not None:
			for a in original_price_content.text.replace('(', '').replace(')', '').split():
				try:
					original_price = int(a.replace('.','').replace(',','.'))
				except ValueError:
					pass
		else:
			original_price = 0

		if referencial_price_content is not None:
			referencial_price_content = referencial_price_content.find('h2')
			for b in referencial_price_content.text.replace('(', '').replace(')', '').split():
				try:
					referencial_price = int(b.replace('.','').replace(',','.'))
				except ValueError:
					pass
		else:
			referencial_price = 0

	
		data_ficha_producto_tiendas = content.find('div',attrs={"class": "ficha_producto_tiendas"})
		ul_data = data_ficha_producto_tiendas.select('ul')[1]
		count = 0
		contador = 0
		for li in ul_data.find_all('li'):
			contador += 1
			branch_office = li.select('a span')[0].text
			if contador == 8:
				continue
			unitys = int(li.select('a span')[1].text.replace('+', ''))
			count = count + unitys
			if branch_office[0] == ' ':
				branch_office = branch_office[3:]
			office = BranchOffice.objects.get(name=branch_office)
			new_product_branch = ProductBranchOffice(
				office=office,
				product=data,
				branch_stock=unitys
			)
			list_branch_offices_bulk.append(new_product_branch)
		try:
			product_update = Product.objects.get(id=data.id)
			product_update.available_quantity = count
			product_update.price = price
			product_update.original_price = original_price
			product_update.referencial_price = referencial_price
			product_update.save()
			price_variation = PriceVariationPcFactory(
				product=product_update,
				price=product_update.price
			)
			new_product_price_variation.append(price_variation)
			print("Variation created")
		except (Product.DoesNotExist,):
			pass
	else:
		print("Not found data")

