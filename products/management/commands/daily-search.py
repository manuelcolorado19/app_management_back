from django.core.management.base import BaseCommand, CommandError
import argparse
import asyncio
import sys
# import resource
from aiohttp import ClientSession
from functools import partial
from time import time
from bs4 import BeautifulSoup, SoupStrainer
import json
import requests
from multiprocessing.dummy import Pool as ThreadPool
from products.models import Product, ProductVariation
from django.utils import timezone
import time as ti_me
from datetime import datetime as dt
initial_time = ti_me.strftime("%H:%M:%S")
new_data = []
products = []
start_time = time()
content_product = SoupStrainer(id='short-desc')
today_date = timezone.now()
variations = ProductVariation.objects.filter(date_search=today_date).values_list('product__id', flat=True)
all_products = Product.objects.filter(channel__id=1, url_product__isnull=False,).exclude(id__in=variations).values('url_product', 'id')[:4000]
# all_products = Product.objects.filter(channel__id=1, url_product__isnull=False).values('url_product', 'id')[:4000]
list_variations = []


async def run(session, number_of_requests, concurrent_limit):
	base_url = "{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i, id_product):
		url = base_url.format(i)
		try:
			async with session.get(url) as response:
				response = await response.read()
				sem.release()
				responses.append(response)
				page_content = BeautifulSoup(response.decode('utf-8'), 'lxml', parse_only=content_product)
				content = page_content.find('div', attrs={"class": "item-conditions"})
				quantity_content = page_content.find('span', attrs={"class": "dropdown-quantity-available"})
				price_content = page_content.find('span', attrs={"class": "price-tag-fraction"})
				sold_quantity = 0
	
				if content is not None:
					content_split = content.text.split()
					for t in content_split:
						try:
							sold_quantity = int(t.replace('.','').replace(',','.'))
						except ValueError:
							pass
				else:
					sold_quantity = None
				if quantity_content is not None:
					quantity_split = quantity_content.text.replace('(', '').replace(')', '').split()
					for a in quantity_split:
						try:
							quantity = int(a.replace('.','').replace(',','.'))
						except ValueError:
							pass
				else:
					quantity = None
				if price_content is not None:
					price_split = price_content.text.replace('(', '').replace(')', '').split()
					for m in price_split:
						try:
							price = int(m.replace('.','').replace(',','.'))
						except ValueError:
							pass
				else:
					price = None
				list_variations.append(
		            ProductVariation(
		                product_id=id_product,
		                sold_quantity=sold_quantity,
		                available_quantity=quantity,
		                price=price,
		                # diference_day_before=diference_day_before
		            )
		        )
			
				return response
		except Exception as e:
			# print(e)
			pass

	for i in all_products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i['url_product'], i['id']))
		task.add_done_callback(tasks.remove)
		tasks.append(task)

	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	print("total_variations: {}".format(len(list_variations)))
	ProductVariation.objects.bulk_create(list_variations)
	# variations = ProductVariation.objects.filter(date_search=today_date).values_list('product__id', flat=True)
	# all_products = Product.objects.filter(channel__id=1, url_product__isnull=False).exclude(id__in=variations).values('id')
	# if not all_products:
	with open('LogProcess.txt', "a+") as f:
		f.write(str(dt.now().date())+ "\t" +"Etapa 3 Recolección de datos" + "\t" + initial_time + "\t" + ti_me.strftime("%H:%M:%S") + "\t" + str(len(list_variations)) + "\t" + str(len(variations)) + "\n")
		f.close()
	return responses


async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		if all_products:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()
		print("--- %s seconds ---" % (time() - start_time))
# > y = ProductVariation.objects.create(product_id=653623, sold_quantity=9, available_quantity=10, price=10000, date_search=datetime.date(2019,2,26))
# y = ProductVariation.objects.create(product_id=654538, sold_quantity=9, available_quantity=10, price=10000, date_search=datetime.date(2019,2,26))
# y = ProductVariation.objects.create(product_id=650269, sold_quantity=2, available_quantity=10, price=56790, date_search=datetime.date(2019,2,26))