import requests
import datetime
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail, EmailMessage
import base64
import os
from io import BytesIO

class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def add_arguments(self, parser):
        parser.add_argument('hour', nargs='+', type=int)

    def handle(self, *args, **options):
        today = datetime.datetime.today().date()
        # output2 = BytesIO()
        hour_notify = options['hour'][0]
        # print(hour_notify)
        date_from = (datetime.datetime.today() - datetime.timedelta(days=21)).replace(hour=hour_notify, minute=00).strftime("%Y-%m-%dT%H:%M:%S.000%z-00:00")
        date_to = datetime.datetime.today().replace(hour=hour_notify, minute=00).strftime("%Y-%m-%dT%H:%M:%S.000%z-00:00")
        dls = "http://68.183.113.138:8000/export/orders/xlsx?hour_from="+ date_from +"&hour_to="+date_to
        resp = requests.get(dls)
        file_name_path = 'products/files/Ordenes ' + str(today) + '.xlsx'
        output = open(file_name_path, 'wb')
        output.write(resp.content)
        output.close()
        self.send_email_notification(attachment_path=file_name_path)
        print("File downloaded and send")

    def send_email_notification(self, attachment_path, mimetype='application/octet-stream', fail_silently=False):
    	subject = 'Ordenes del día'
    	from_email = 'mejiasabelito@gmail.com'
    	to = ["juan.ureta.e@gmail.com", "ventas@asiamerica.cl", "manuel.ureta@asiamerica.cl", "lucia.diaz@asiamerica.cl", "abel.mejias@asiamerica.cl", "archivosluciad@gmail.com", "pablo.guerrero@asiamerica.cl"]
    	#to = ["pablo.guerrero@asiamerica.cl"]
    	message = "Ordenes del día"
    	email = EmailMessage(subject, message, to=to)
    	output2 = BytesIO()
    	file_name = os.path.basename(attachment_path)
    	data = open(attachment_path, 'rb').read()

    	encoded = base64.b64encode(data)
    	email.attach_file(attachment_path)
    	email.send(fail_silently=fail_silently)
