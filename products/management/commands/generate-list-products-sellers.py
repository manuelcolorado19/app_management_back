import argparse
import asyncio
import sys
from django.core.management.base import BaseCommand, CommandError
# import resource
from aiohttp import ClientSession
from functools import partial
from time import time
from bs4 import BeautifulSoup, SoupStrainer
import json
import requests
from multiprocessing.dummy import Pool as ThreadPool
from operator import itemgetter
from notifications.models import Token


token = Token.objects.get(id=1).token_ml
link_append = []
products = []
with_problem = []
start_time = time()

with open("Links.txt") as f:
	content = f.readlines()
	for i in content:
		link_append.append(i.replace("\n", ""))

async def run(session, number_of_requests, concurrent_limit):
	base_url = "{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		url = base_url.format(i) + "&access_token=" + token
		async with session.get(url) as response:
			response = await response.read()
			sem.release()
			responses.append(response)
			data_json = json.loads(response)
			# print(len(data_json['results']))
			try:
				for each in data_json['results']:
					if each['sold_quantity'] >= 5:
						products.append(each['id'])
			except Exception as e:
				with_problem.append(i)
			# print("request made")
			return response

	for i in link_append:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)

	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	# print(with_problem[0])
	f= open("BusquedaSellers.txt","w+")
	for j in products:
		f.write("%s\r" % (j))
	f.close()
	print("--- %s seconds ---" % (time() - start_time))

	return responses

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		# print("empezo")
		loop = asyncio.get_event_loop()
		loop.run_until_complete(main(10000, 10000))
		loop.close()