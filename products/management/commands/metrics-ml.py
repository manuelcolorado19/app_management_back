from products.models import Product
import os
from time import time
import argparse
import asyncio
import sys
import pandas as pd
# import resource
from django.core.management.base import BaseCommand, CommandError
from aiohttp import ClientSession
import json
from datetime import datetime as dt
from sellers.models import Seller
from products.models import Product, MetricsMercadolibre
from categories.models import Channel
from django.db.models.expressions import Window
from django.db.models.functions import RowNumber
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from datetime import timedelta
from django.db.models import Prefetch, Sum, F, Func, Aggregate, FloatField, Case, When, Avg, IntegerField, Count, Q, Value, CharField
all_products = list(Product.objects.filter(channel__id=1,).order_by('id').values_list('id', flat=True)[70000:80000])
in_metrics = list(MetricsMercadolibre.objects.all().values_list('product_id', flat=True))
sum_products = all_products + in_metrics
sum_products = set(sum_products)
all_products = Product.objects.filter(channel__id=1).exclude(id__in=sum_products).order_by('id')[0:10000]
from decimal import Decimal
import datetime
now = datetime.datetime.now().date()
print(now)
datetime_7 = (now - timedelta(days=7))
datetime_28 = (now - timedelta(days=28))
datetime_14 = (now - timedelta(days=14))
start_time = time()
# rank
class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		print(len(all_products))
		if all_products:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()

async def run(session, number_of_requests, concurrent_limit):
	# base_url = "https://api.mercadolibre.com/items/{}"
	tasks = []
	responses = []
	all_metrics = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		data_metrics = i.product_variation.aggregate(daily_sales_count=Count('id'), daily_sales_sum=Sum('diference_day_before'),daily_sales_average=Avg(Case(When(diference_day_before__isnull=True, then=0), default=F('diference_day_before'), output_field=IntegerField())), acumulated=Sum('daily_reward'), average_reward=Avg(Case(When(daily_reward__isnull=True, then=0), default=F('daily_reward'), output_field=IntegerField())),average_last_7=Sum(Case(When(Q(date_search__gt=datetime_7) & Q(diference_day_before__isnull=False), then=F('diference_day_before')), default=0, output_field=IntegerField())), average_last_28=Sum(Case(When(Q(date_search__gt=datetime_28) & Q(diference_day_before__isnull=False), then=F('diference_day_before')), default=0, output_field=IntegerField())), average_last_14=Sum(Case(When(Q(date_search__gt=datetime_14) & Q(diference_day_before__isnull=False), then=F('diference_day_before')), default=0, output_field=IntegerField())))
		count = i.product_variation.count()
		values = i.product_variation.values_list('diference_day_before', flat=True).order_by('diference_day_before')
		if count % 2 == 1:
			median = values[int(round(count/2))]
		else:
			median = sum(values[count/2-1:count/2+1])/Decimal(2.0)
		trend = i.product_variation.values('diference_day_before').annotate(conteo=Count('diference_day_before')).order_by('-conteo', '-diference_day_before')[:1]
		sellers_count = Product.objects.filter(seller_id_ml=i.seller_id_ml).count()
		category_order_by_reward = Product.objects.filter(last_category=i.last_category).annotate(reward_total=Sum('product_variation__daily_reward')).order_by('-reward_total')
		count_reward = 1
		for each in category_order_by_reward:
			if each.id == i.id:
				break
			count_reward += 1


		category_order_by_sold_quantity = Product.objects.filter(last_category=i.last_category).order_by('-sold_quantity')
		count_sold = 1
		for each in category_order_by_sold_quantity:
			if each.id == i.id:
				break
			count_sold += 1
		publications_days = (now - i.stop_time).days
		historic_sales = i.sold_quantity/publications_days
		average_last_7 = data_metrics['average_last_7']/7
		average_last_28 = data_metrics['average_last_28']/28
		average_last_14 = data_metrics['average_last_14']/14
		id_time_serie = []
		product_variation_data = i.product_variation.all()
		k = 1
		for p in product_variation_data.filter(diference_day_before__isnull=False):
			id_time_serie.append({"id":k})
			k += 1
		df2 = pd.DataFrame(id_time_serie)
		df = pd.DataFrame(list(product_variation_data.filter(diference_day_before__isnull=False).values('date_search', 'diference_day_before').order_by('date_search')))
		x = df2.iloc[:, :1].values
		y = df.iloc[:, 1].values
		# print(x)
		# print(y)
		x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 1/3, random_state =0)
		regressor = LinearRegression()
		try:
			regressor.fit(x_train, y_train)
			hope_30 = product_variation_data.count() + 30
			hope_60 = product_variation_data.count() + 60
			hope_90 = product_variation_data.count() + 90
			e = [[hope_30], [hope_60], [hope_90]]
			y_pred = regressor.predict(e)
		except ValueError as e:
			# print(i.id)
			y_pred = [None, None, None]

		sold_quantity_count = i.product_variation.filter(sold_quantity__isnull=False).count()
		# print(data_metrics)
		# print(str(average_last_7) + " promedio 7")
		# print(str(average_last_28) + " promedio 28")
		# print(str(average_last_14) + " promedio 14")
		# print(str(publications_days) + " días de publicacion")
		# print(str(historic_sales) + " ventas históricas")
		# print(str(median) + " Mediana")
		# print(str(trend[0]['diference_day_before']) + " Moda")
		# print(str(sellers_count) + " Publicaciones del seller en la base")
		# print(str(i.seller.number_of_items) + " Publicaciones del seller en la página")
		# print(str(count_reward) + " filtrado por categorias y Ordenados por recaudación " + str(i.id))
		# print(str(count_sold) + " filtrado por categorias y Ordenados por cant vendida " + str(i.id))
		# print(str(category_order_by_reward.count()) + " publicaciones por categoria " + str(i.id))
		# print(str(y_pred[0]) + " esperanza (predicción) proximos 30 dias")
		# print(str(y_pred[1]) + " esperanza (predicción) proximos 60 dias")
		# print(str(y_pred[2]) + " esperanza (predicción) proximos 90 dias")
		# print(str(sold_quantity_count) + " cantidad de datos reales (recoleccion nocturna)")
		rank = 0
		if data_metrics['daily_sales_count'] is not None and  data_metrics['daily_sales_count']>= 21:
			rank += 1
		if data_metrics['daily_sales_sum'] is not None and data_metrics['daily_sales_sum']>= 21:
			rank += 1
		if data_metrics['daily_sales_average'] is not None and  data_metrics['daily_sales_average']>= 1:
			rank += 1
		if publications_days >= 21:
			rank += 1
		if historic_sales >= 1:
			rank += 1
		if data_metrics['acumulated'] is not None and data_metrics['acumulated'] >= 200000:
			rank += 1
		if data_metrics['average_reward'] is not None  and data_metrics['average_reward'] >= 10000:
			rank += 1
		if average_last_28 >= 1:
			rank += 1
		if average_last_7 >= 1:
			rank += 1
		if average_last_14 >= 1:
			rank += 1
		# print(median)
		if median is not None and median >= 1:
			rank += 1
		if trend[0]['diference_day_before'] >= 1:
			rank += 1
		if count_reward <= 10:
			rank += 1
		if category_order_by_reward.count() >= 25:
			rank += 1
		if sellers_count >= 300:
			rank += 1
		if i.seller.number_of_items >= 300:
			rank += 1
		if y_pred[0] is not None and  y_pred[0]>= 1:
			rank += 1
		if y_pred[1] is not None and  y_pred[1]>= 1:
			rank += 1
		if y_pred[2] is not None and  y_pred[2]>= 1:
			rank += 1
		if sold_quantity_count >= 21:
			rank += 1
		metric_data = MetricsMercadolibre.objects.filter(product=i)
		if MetricsMercadolibre.objects.filter(product=i):
			# print("hola")
			MetricsMercadolibre.objects.filter(product=i).update(
				daily_sales_count=data_metrics['daily_sales_count'],
				daily_sales_sum=data_metrics['daily_sales_sum'],
				daily_sales_average=data_metrics['daily_sales_average'],
				publications_days=publications_days,
				historic_sales=historic_sales,
				acumulated=data_metrics['acumulated'],
				average_reward=data_metrics['average_reward'],
				average_last_28=average_last_28,
				average_last_7=average_last_7,
				average_last_14=average_last_14,
				mode=trend[0]['diference_day_before'],
				median=median,
				location_by_category=count_reward,
				competition_number=category_order_by_reward.count(),
				sellers_publications=sellers_count,
				hope_30=y_pred[0],
				hope_60=y_pred[1],
				hope_90=y_pred[2],
				rank=rank,
			)
		else:
			# print("creado")
			try:
				ml_metric = MetricsMercadolibre.objects.create(
					product=i,
					daily_sales_count=data_metrics['daily_sales_count'],
					daily_sales_sum=data_metrics['daily_sales_sum'],
					daily_sales_average=data_metrics['daily_sales_average'],
					publications_days=publications_days,
					historic_sales=historic_sales,
					acumulated=data_metrics['acumulated'],
					average_reward=data_metrics['average_reward'],
					average_last_28=average_last_28,
					average_last_7=average_last_7,
					average_last_14=average_last_14,
					mode=trend[0]['diference_day_before'],
					median=median,
					location_by_category=count_reward,
					competition_number=category_order_by_reward.count(),
					sellers_publications=sellers_count,
					hope_30=y_pred[0],
					hope_60=y_pred[1],
					hope_90=y_pred[2],
					rank=rank,
				)
			except Exception as e:
				print(e)
				pass
			

		# print("----------------------------------------------------------------------")
		print(str(rank) + " Ranking de la publicación")

	for i in all_products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	# print("total_responses: {}".format(len(responses)))
	print("total_products_append: {}".format(len(all_products)))
	print("--- %s seconds ---" % (time() - start_time))
	print("0")
	MetricsMercadolibre.objects.bulk_create(all_metrics)

	# print(with_exception)
	# Product.objects.bulk_create(products_bulk)

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return