# Generated by Django 2.1.2 on 2018-10-23 18:29

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0015_auto_20181023_1135'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productvariation',
            name='date_search',
            field=models.DateField(default=datetime.date(2018, 10, 23)),
        ),
    ]
