from django.db import models
from categories.models import SubCategories, Channel
from sellers.models import Seller
from datetime import date
from django.utils import timezone

class Product(models.Model):
	code = models.CharField(max_length=80)
	name = models.CharField(max_length=350)
	price = models.DecimalField(decimal_places=1, max_digits=20, null=True, blank=True)
	sold_quantity = models.IntegerField(default=0, null=True, blank=True)
	sub_category = models.ForeignKey(SubCategories, on_delete=models.SET_NULL, null=True, blank=True)
	seller = models.ForeignKey(Seller, on_delete=models.SET_NULL, null=True, blank=True, related_name='seller_product')
	seller_id_ml = models.IntegerField(null=True)
	available_quantity = models.IntegerField(null=True, blank=True, default=0)
	url_product = models.URLField(null=True, blank=True)
	original_price = models.DecimalField(decimal_places=1, max_digits=20, null=True, blank=True, default=0) 
	stop_time = models.DateField(null=True, blank=True)
	rating = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=4)
	referencial_price = models.DecimalField(null=True, blank=True, decimal_places=1, max_digits=20)
	from_seller_list = models.BooleanField(null=True, default=False)
	last_category = models.CharField(max_length=60, blank=True, null=True)
	last_category_description = models.CharField(max_length=60, blank=True, null=True)
	channel=models.ForeignKey(Channel, on_delete=models.SET_NULL, null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	# seller = models.ForeignKey(Seller, on_delete=models.SET_NULL, null=True, blank=True)

	class Meta:
		db_table = 'product'


class ProductVariation(models.Model):
	product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_variation')
	sold_quantity = models.IntegerField(null=True)
	diference_day_before = models.IntegerField(default=0, null=True)
	date_search = models.DateField(default=timezone.now)
	available_quantity = models.IntegerField(null=True, blank=True,)
	price = models.IntegerField(null=True)
	projected_sale = models.IntegerField(null=True)
	is_projected = models.BooleanField(null=True)
	is_reused = models.BooleanField(default=False)
	daily_reward = models.IntegerField(null=True)
	created_at = models.DateTimeField(auto_now_add=True, null=True)

	class Meta:
		db_table = 'products_variation'

class BranchOffice(models.Model):
	name = models.CharField(max_length=100, null=True, blank=True)
	address = models.CharField(max_length=150, null=True, blank=True)
	phone = models.CharField(max_length=20, null=True, blank=True)
	santiago = models.BooleanField(default=False)
	order = models.IntegerField(null=True, blank=True)

	class Meta:
		db_table = 'branch_office'

class ProductBranchOffice(models.Model):
	office = models.ForeignKey(BranchOffice, on_delete=models.CASCADE, related_name='product_office', null=True, blank=True)
	product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_branch_office', null=True, blank=True)
	branch_stock = models.IntegerField(null=True, blank=True)
	date_search = models.DateField(null=True, blank=True, default=timezone.now)
	created_at = models.DateTimeField(auto_now_add=True, null=True)

	class Meta:
		db_table = 'product_branch_office'

class PriceVariationPcFactory(models.Model):
	product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product_price_variation_pcfactory', null=True, blank=True)
	date_search = models.DateField(null=True, blank=True, default=timezone.now)
	price = models.IntegerField(null=True)
	created_at = models.DateTimeField(auto_now_add=True, null=True)

	class Meta:
		db_table = 'price_variation_pcfactory'

class MetricsMercadolibre(models.Model):
	product = models.OneToOneField(Product, on_delete=models.SET_NULL, null=True, blank=True, related_name='metrics_product')
	daily_sales_count = models.IntegerField(null=True)
	daily_sales_sum = models.IntegerField(null=True)
	daily_sales_average = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=8) 
	publications_days = models.IntegerField(null=True)
	historic_sales = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=8) 
	acumulated = models.IntegerField(null=True)
	average_reward = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=8) 
	average_last_28 = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=8) 
	average_last_7 = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=8) 
	average_last_14 = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=8) 
	mode = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=6) 
	median = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=8) 
	location_by_category = models.IntegerField(null=True)
	competition_number = models.IntegerField(null=True)
	sellers_publications = models.IntegerField(null=True)
	hope_30 = models.IntegerField(null=True)
	hope_60 = models.IntegerField(null=True)
	hope_90 = models.IntegerField(null=True)
	rank = models.DecimalField(null=True, blank=True, decimal_places=3, max_digits=5)
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	class Meta:
		db_table = 'metrics_mercadolibre'
