import pandas as pd
import datetime
from products.models import Product, ProductVariation, ProductBranchOffice, BranchOffice, MetricsMercadolibre
from django.db.models import F, Case, When, CharField, Value, IntegerField, DurationField, Q, Sum, Value, Count
import time
from django.db.models.functions import Concat, Cast
from dateutil.parser import parse
from collections import OrderedDict
from datetime import timedelta
import datetime
start_time = time.time()

df = pd.DataFrame(list(Product.objects.filter(sub_category__category__channel__name='Pcfactory',).prefetch_related(Prefetch('product_branch_office', queryset=ProductBranchOffice.objects.order_by('id'))).annotate(id_producto=F('id')).values('code','name', 'price', 'available_quantity', 'url_product', 'original_price', 'referencial_price', 'id_producto').order_by('id')[0:1000]))
values_list = Product.objects.filter(sub_category__category__channel__name='Pcfactory',).prefetch_related(Prefetch('product_branch_office', queryset=ProductBranchOffice.objects.order_by('id'))).annotate(id_producto=F('id')).values_list('id', flat=True).order_by('id')[0:1000]

df2 = pd.DataFrame(list(ProductBranchOffice.objects.filter(product__sub_category__category__channel__name='Pcfactory', product__id__in=values_list).annotate(id_producto=F('product__id')).values('branch_stock', 'date_search', 'office__name', 'product__id', 'id_producto').order_by('product__id', 'date_search', 'office__id',)))
print("here")




data_dict = {}
j = 0
for index2, row2 in df.iterrows():
	new_df = df.loc[df['id_producto'] == row2['id_producto']]
	print(j)
	i = 0
	for index, row in pd.merge(new_df, df2, on='id_producto').iterrows():
		
		if len(data_dict.get(str(i), [])) > 0:
			data_dict[str(i)].append(row['branch_stock'])
		else:
			data_dict[str(i)] = []
			data_dict[str(i)].append(row['branch_stock'])
		i += 1
		# array_sales.append(row['branch_stock'])
	j +=1
print("--- %s seconds ---" % (time.time() - start_time))
new_dataframe = pd.DataFrame.from_dict(data_dict, orient='index')
data_complete = df.merge(new_dataframe.T, left_index=True, right_index=True, how='inner')
data_complete.to_excel("Analisis Pc Factory 12-04-19.xlsx")

# df = pd.DataFrame(list(Product.objects.filter(sub_category__category__channel__name='Mercadolibre', from_seller_list=False).values('code','name', 'price', 'sold_quantity', 'url_product').order_by('-sold_quantity')))


# q = ProductVariation.objects.filter(product__channel__id=1, product__id=520605).annotate(id_producto=F('product__id'), sold_quantity2=Case(When(sold_quantity__isnull=False, then=F('sold_quantity')), When(projected_sale__isnull=False, then=F('projected_sale')), default=Value('0', output_field=CharField()), output_field=CharField())).distinct().values('sold_quantity2', 'date_search', 'available_quantity', 'product__id', 'id_producto').order_by('product__id', 'date_search',)


# df = pd.DataFrame(list(Product.objects.filter(channel__id=1).annotate(id_producto=F('id')).values('code','name', 'price', 'sold_quantity', 'url_product', 'id_producto').order_by('id'))[:1000])



# Generación de informe de recaudación y ventas
# now = datetime.date(2019,4,2)
# datetime_30 = (now - timedelta(days=30))
# values_list = MetricsMercadolibre.objects.filter(product__channel__id=1).annotate(recaudacion_ultimos30=Sum(Case(When(product__product_variation__date_search__gt=datetime_30, then=F('product__product_variation__daily_reward')), default=0, output_field=IntegerField()))).values_list('product_id', flat=True).order_by('-recaudacion_ultimos30')[:10000]

# df2 = pd.DataFrame(list(ProductVariation.objects.filter(product__channel__id=1, product__id__in=values_list).annotate(id_producto=F('product__id'), sold_quantity2=Case(When(sold_quantity__isnull=False, then=Cast('sold_quantity', CharField())), When(projected_sale__isnull=False, then=(Concat(Value("("), 'projected_sale', Value(")"), output_field=CharField()))), default=Value('9999999', output_field=CharField()), output_field=CharField())).distinct().values('sold_quantity2', 'date_search', 'available_quantity', 'product__id', 'id_producto').order_by('product__id', 'date_search',)))

# df2 = pd.DataFrame(list(ProductVariation.objects.filter(product__channel__id=1, product__id__in=values_list).annotate(id_producto=F('product__id'), sold_quantity2=Case(When(sold_quantity__isnull=False, then=Cast('diference_day_before', CharField())), When(projected_sale__isnull=False, then=(Concat(Value("("), 'diference_day_before', Value(")"), output_field=CharField()))), default=Value('9999999', output_field=CharField()), output_field=CharField())).distinct().values('sold_quantity2', 'date_search', 'available_quantity', 'product__id', 'id_producto').order_by('product__id', 'date_search',)))


# df2 = pd.DataFrame(list(ProductVariation.objects.filter(product__channel__id=1, product__id__in=values_list).annotate(id_producto=F('product__id'), sold_quantity2=Case(When(sold_quantity__isnull=False, then=Cast('daily_reward', CharField())), When(projected_sale__isnull=False, then=(Concat(Value("("), 'daily_reward', Value(")"), output_field=CharField()))), default=Value('9999999', output_field=CharField()), output_field=CharField())).distinct().values('sold_quantity2', 'date_search', 'available_quantity', 'product__id', 'id_producto').order_by('product__id', 'date_search',)))

# df2 = pd.DataFrame(list(ProductVariation.objects.filter(product__channel__id=1, product__id__in=values_list).annotate(id_producto=F('product__id'), sold_quantity2=Case(When(sold_quantity__isnull=False, then=Cast('price', CharField())), When(projected_sale__isnull=False, then=(Concat(Value("("), 'price', Value(")"), output_field=CharField()))), default=Value('9999999', output_field=CharField()), output_field=CharField())).distinct().values('sold_quantity2', 'date_search', 'available_quantity', 'product__id', 'id_producto').order_by('product__id', 'date_search',)))

# df = pd.DataFrame(list(Product.objects.filter(channel__id=1, id__in=values_list).annotate(id_producto=F('id'), recaudacion_ultimos30=Sum(Case(When(product_variation__date_search__gt=datetime_30, then=F('product_variation__daily_reward')), default=0, output_field=IntegerField()))).values('code','name', 'price','original_price', 'sold_quantity', 'url_product', 'id_producto', 'seller_id_ml', 'seller__nickname', 'last_category', 'last_category_description', 'metrics_product__rank', 'recaudacion_ultimos30').order_by('-recaudacion_ultimos30')))


# data_dict = {}
# j = 0
# for index2, row2 in df.iterrows():
# 	new_df = df.loc[df['id_producto'] == row2['id_producto']]
# 	print(j)
# 	i = 0
# 	if j > 1:
# 		max_key = max(data_dict, key= lambda x: len(set(data_dict[x])))
# 		max_len = len(data_dict[max_key])
# 		for key, value in data_dict.items():
# 			if len(value) == max_len:
# 				continue
# 			else:
# 				data_dict[key].append(None)

# 	for index, row in pd.merge(new_df, df2, on='id_producto').iterrows():
# 		if len(data_dict.get(str(row['date_search']), [])) > 0:
# 			if len(data_dict[str(row['date_search'])]) > j:
# 				continue
# 			else:
# 				if row['sold_quantity2'] == '9999999':
# 					data_dict[str(row['date_search'])].append(None)
# 				else:
# 					data_dict[str(row['date_search'])].append(row['sold_quantity2'])
# 		else:
# 			data_dict[str(row['date_search'])] = []
# 			if j > 0:
# 				for r in range(j):
# 					data_dict[str(row['date_search'])].append(None)
# 			data_dict[str(row['date_search'])].append(row['sold_quantity2'])
# 		i += 1
# 	j +=1


# new_dataframe = pd.DataFrame.from_dict(OrderedDict(sorted(data_dict.items(), key=lambda x: parse(x[0]))), orient='index')
# data_complete = df.merge(new_dataframe.T, left_index=True, right_index=True, how='inner')
# styled = (data_complete.style
#             .applymap(lambda v: 'color: %s' % 'red' if (type(v) == str and v.startswith("(")) else ''))
# styled.to_excel("Descargar Nocturna Precios diarios 02-04-19.xlsx")
# print("--- %s seconds ---" % (time.time() - start_time))



# Informe de metricas
now = datetime.date(2019,4,2)
datetime_30 = (now - timedelta(days=30))
print("generandooooo")
df_metrics = pd.DataFrame(list(MetricsMercadolibre.objects.filter(product__channel__id=1, ).annotate(id_producto=F('product__id'), MLC=F('product__code'), titulo=F('product__name'), precio=F('product__price'), precio_original=F('product__original_price'), cantidad_vendida=F('product__sold_quantity'), URL=F('product__url_product'), seller_id_ml=F('product__seller_id_ml'), nickname=F('product__seller__nickname'), ultima_categoria=F('product__last_category'), ultima_categoria_descripcion=F('product__last_category_description'), conteo_ventas=F('daily_sales_count'), suma_ventas=F('daily_sales_sum'), promedio_ventas=F('daily_sales_average'), dias_publicacion=F('publications_days'), ventas_historicas=F('historic_sales'), acumulado=F('acumulated'), promedio_recaudacion=F('average_reward'), promedio_ultimos_28=F('average_last_28'), promedio_ultimos_7=F('average_last_7'), promedio_ultimos_14=F('average_last_14'), moda=F('mode'), mediana=F('median'), localizacion_categoria=F('location_by_category'), numero_competidor=F('competition_number'), publicaciones_seller=F('sellers_publications'), esperanza_30=F('hope_30'), esperanza_60=F('hope_60'), esperanza_90=F('hope_90'), Rank=F('rank'), recaudacion_ultimos30=Sum(Case(When(product__product_variation__date_search__gt=datetime_30, then=F('product__product_variation__daily_reward')), default=0, output_field=IntegerField())) ).values( 'id_producto', 'MLC', 'titulo', 'precio', 'precio_original','cantidad_vendida', 'URL', 'seller_id_ml', 'nickname', 'ultima_categoria', 'ultima_categoria_descripcion', 'conteo_ventas', 'suma_ventas', 'promedio_ventas', 'dias_publicacion', 'ventas_historicas', 'acumulado', 'promedio_recaudacion', 'promedio_ultimos_28', 'promedio_ultimos_7', 'promedio_ultimos_14', 'moda', 'mediana', 'localizacion_categoria', 'numero_competidor', 'publicaciones_seller', 'esperanza_30', 'esperanza_60', 'esperanza_90', 'Rank', 'recaudacion_ultimos30').order_by('-Rank')))

writer = pd.ExcelWriter('Metricas 02-04-19.xlsx', engine ='xlsxwriter',options={'strings_to_urls': False}) 
df_metrics.to_excel(writer, sheet_name ='Sheet1')
writer.save()


# df_metrics.to_excel("Metricas todas.xlsx")
df = pd.DataFrame(list(Product.objects.filter(sub_category__category__channel__name='Pcfactory',).prefetch_related(Prefetch('product_branch_office', queryset=ProductBranchOffice.objects.order_by('id'))).annotate(id_producto=F('id')).values('code','name', 'price', 'available_quantity', 'url_product', 'original_price', 'referencial_price', 'id_producto').order_by('id')[:1500]))
# df = pd.DataFrame(list(BlogPost.objects.filter(date__gte=datetime.datetime(2012, 5, 1)).values()))

# # limit which fields
# df = pd.DataFrame(list(BlogPost.objects.all().values('author', 'date', 'slug')))
df2 = pd.DataFrame(list(ProductBranchOffice.objects.filter(product__sub_category__category__channel__name='Pcfactory').annotate(id_producto=F('product__id')).values('branch_stock', 'date_search', 'office__name', 'product__id', 'id_producto').order_by('product__id', 'date_search', 'office__id',)))

# x = df.loc[df['id_producto'] == 97218]
# print(df)
# print(df2)
# print(x)
# print(pd.merge(df, df2, on='id_producto'))
# array_sales = []



data_dict = {}
for index2, row2 in df.iterrows():
	new_df = df.loc[df['id_producto'] == row2['id_producto']]
	# print(new_df)
	i = 0
	for index, row in pd.merge(new_df, df2, on='id_producto').iterrows():
		# print(i)
		# df.loc[index,'d'] = np.random.randint(0, 10)
		# print(len(data_dict.get(str(index), [])))
		if len(data_dict.get(str(i), [])) > 0:
			# print("aqui")
			data_dict[str(i)].append(row['branch_stock'])
		else:
			data_dict[str(i)] = []
			data_dict[str(i)].append(row['branch_stock'])
		i += 1
		# array_sales.append(row['branch_stock'])
print("--- %s seconds ---" % (time.time() - start_time))
# print(data_dict)
new_dataframe = pd.DataFrame.from_dict(data_dict, orient='index')
# new_dataframe.T
# print(df.merge(new_dataframe, left_index=True, right_index=True, how='inner'))
data_complete = df.merge(new_dataframe.T, left_index=True, right_index=True, how='inner')
# print(new_dataframe.T)
data_complete.to_excel("Analisis Pc Factory 14-02-19.xlsx")

