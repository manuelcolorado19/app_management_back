from categories.models import SubCategories
from products.models import Product, ProductBranchOffice, BranchOffice, PriceVariationPcFactory
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from bs4 import SoupStrainer
from datetime import datetime as dt
import datetime as date_time
import unidecode

categories = SubCategories.objects.filter(category__channel__name = 'Pcfactory').order_by('id')
pc_factory_url = "https://www.pcfactory.cl"

def get_price_from_url(data, category):
	page_response = requests.get(pc_factory_url + data['url'], timeout=5)
	content_product = SoupStrainer('div', {'class': 'ficha_cuerpo'})
	page_content = BeautifulSoup(page_response.content, "html.parser", parse_only=content_product)
	content = page_content.find('div', attrs={"class": "ficha_producto_right"})
	if content is not None:
		code = content.find('span', attrs={"itemprop": "sku"}).text
		price_content = content.find('div', attrs={"class": "ficha_precio_efectivo"}).find('h2')
		original_price_content = content.find('div', attrs={"class": "ficha_precio_normal"}).find('h2')
		referencial_price_content = content.find('div', attrs={"class": "ficha_precio_referencial"})
		if price_content is not None:
			for t in price_content.text.split():
				try:
					price = int(t.replace('.','').replace(',','.'))
				except ValueError:
					pass
		else:
			price = 0
		if original_price_content is not None:
			for a in original_price_content.text.replace('(', '').replace(')', '').split():
				try:
					original_price = int(a.replace('.','').replace(',','.'))
				except ValueError:
					pass
		else:
			original_price = 0

		if referencial_price_content is not None:
			referencial_price_content = referencial_price_content.find('h2')
			for b in referencial_price_content.text.replace('(', '').replace(')', '').split():
				try:
					referencial_price = int(b.replace('.','').replace(',','.'))
				except ValueError:
					pass
		else:
			referencial_price = 0
			# de aqui
		if Product.objects.filter(code=code):
			print("Product already created")
		else:
			product = Product.objects.create(
				code=code,
				name=data['name'],
				price=price,
				sub_category=category,
			  	url_product=pc_factory_url + data['url'],
			  	original_price=original_price,
			  	referencial_price=referencial_price
				)
			# # Hay que tener las oficinas de pc factory agregadas
			# data_ficha_producto_tiendas = content.find('div',attrs={"class": "ficha_producto_tiendas"})
			# ul_data = data_ficha_producto_tiendas.select('ul')[1]
			# count = 0
			# for li in ul_data.find_all('li'):
			# 	branch_office = li.select('a span')[0].text
			# 	# if branch_office == 'Internet':
			# 	# 	continue
			# 	unitys = int(li.select('a span')[1].text.replace('+', ''))
			# 	count = count + unitys
			# 	if branch_office[0] == ' ':
			# 		branch_office = branch_office[3:]
			# 	# print(branch_office + "                           " + str(unitys))
			# 	office = BranchOffice.objects.get(name=branch_office)
			# 	new_product_branch = ProductBranchOffice.objects.create(
			# 		office=office,
			# 		product=product,
			# 		branch_stock=unitys
			# 	)
			# # print(data['name'] + " Referencial  " + str(referencial_price) + "  Original  "+ str(original_price))
			# product.available_quantity = count
			# product.save()
			# price_variation = PriceVariationPcFactory.objects.create(
			# 	product=product,
			# 	price=product.price
			# )
			print("Products created")
	else:
		print("Not found web page")

start_time = time.time()
for category in categories:
# url_products_lists = ["https://www.pcfactory.cl/tablets?categoria=488&papa=967", "https://www.pcfactory.cl/juegos-pc?categoria=32&papa=657"]
# for url_products_list in url_products_lists:
	print("Categoria actual: " + str(category.id))
	url_products_list = category.url_sub_category
	list_products = requests.get(url_products_list, timeout=5)

	pagination_content = SoupStrainer('div', {'class': 'paginador-num-pag'})
	page_content = BeautifulSoup(list_products.content, "html.parser", parse_only=pagination_content)
	# print(pagination_content)
	pagination_init = 1

	if page_content is not None:

		if category.category.name == 'Apple':
			pagination_end = page_content.find('a', attrs={"id": "pag_ter_btn_755"}).text
		elif category.name in ['Interfaz de Usuario', 'Sensores', 'Actuadores', 'Placas de Desarrollo', 'Accesorios Electrónica']:
			pagination_end = page_content.find_all('li')[5].find('a').text
		elif category.code == '922':
			pagination_end = 1
		else:
			pagination_end = page_content.find_all('li')[5].find('a').text
	else:
		pagination_end = 0

	if pagination_init == int(pagination_end):
		content_product = SoupStrainer('div', {"class":'wookmark-initialised'})
		page_content = BeautifulSoup(list_products.content, "html.parser", parse_only=content_product)

		if (category.category.name == 'Apple') or (category.category.code == '740'):
			data_id = SoupStrainer('div', attrs={"data-id":"/" + category.url_sub_category.rsplit('/', 1)[-1]})
			data_id_content = BeautifulSoup(list_products.content, "html.parser", parse_only=data_id)
			page_content = data_id_content.find('div', attrs={"class": "wookmark-initialised"})

		if page_content:
			products = page_content.find_all('div', attrs={"class": "wrap-caluga-matrix"})
		if products is not None:
			data = []
			for product in products:
				if product is not None:
					name_product = product.find('span', attrs={"class": "nombre"}).text
					url_product = product.find_all('a', attrs={"class": "noselect"})[0]['href']
					data.append({"name": name_product, "url": url_product})
			#Guardar los productos
			pool = ThreadPool(20)
			results = pool.map(partial(get_price_from_url, category=category),data)
			pool.close()
			pool.join()
		else:
			continue
		total_items = Product.objects.filter(sub_category__id=category.id).count()
		category_update = SubCategories.objects.get(id=category.id)
		category_update.total_items = total_items
		category_update.save()
	else:
		for i in range(pagination_init, int(pagination_end) + 1):
			url_products_list = category.url_sub_category + "&pagina=" + str(i)
			list_products = requests.get(url_products_list, timeout=5)
			content_product = SoupStrainer(id='calugas-productos')
			page_content = BeautifulSoup(list_products.content, "html.parser", parse_only=content_product)
			products = page_content.find_all('div', attrs={"class": "wrap-caluga-matrix"})
			if products is not None:
				data = []
				for product in products:
					name_product = product.find('span', attrs={"class": "nombre"}).text
					url_product = product.find_all('a', attrs={"class": "noselect"})[0]['href']
					data.append({"name": name_product, "url": url_product})
				#Guardar los productos
				pool = ThreadPool(20)
				results = pool.map(partial(get_price_from_url, category=category),data)
				pool.close()
				pool.join()
			else:
				continue
		total_items = Product.objects.filter(sub_category__id=category.id).count()
		category_update = SubCategories.objects.get(id=category.id)
		category_update.total_items = total_items
		category_update.save()

	# content_product = SoupStrainer(id='calugas-productos')
	# page_content = BeautifulSoup(list_products.content, "html.parser", parse_only=content_product)
	# products = page_content.find_all('div', attrs={"class": "wrap-caluga-matrix"})
	# if products:
	# 	data = []
	# 	for product in products:
	# 		name_product = product.find('span', attrs={"class": "nombre"}).text
	# 		url_product = product.find_all('a', attrs={"class": "noselect"})[0]['href']
	# 		data.append({"name": name_product, "url": url_product})
	# 	#Guardar los productos
	# 	pool = ThreadPool(10)
	# 	results = pool.map(partial(get_price_from_url, category=category),data)
	# 	pool.close()
	# 	pool.join()
	# else:
	# 	continue
print("--- %s seconds ---" % (time.time() - start_time))
