from products.models import Product, ProductBranchOffice, BranchOffice, PriceVariationPcFactory
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from django.utils import timezone
from datetime import date, timedelta
from django.contrib.postgres.aggregates.general import ArrayAgg

products = Product.objects.filter(sub_category__category__channel__name='Pcfactory').prefetch_related('product_branch_office').prefetch_related('product_price_variation_pcfactory').annotate(product_variation_dates=ArrayAgg('product_branch_office__date_search')).order_by('id')

def fill_empty_record_not_working_dates(data):
	# print(data.product_variation_dates)
	today = date.today()
	min_date = ProductBranchOffice.objects.earliest('date_search').date_search
	delta = today - min_date
	dates = []
	for i in range(delta.days + 1):
		dates.append(min_date + timedelta(i))
	for eachDate in range(len(dates)):
		if dates[eachDate] in data.product_variation_dates:
			last_day_record = dates[eachDate]
			continue
		else:
			if len(data.product_variation_dates) == 0:
				last_day_record = today
			# Este día no posee registro de actividad
			days_no_data = 1
			new_array = [dates[i] for i in range(eachDate, len(dates))]
			next_day = new_array[len(new_array) - 1]
			for each_date_again in range(len(new_array)):
				if new_array[each_date_again] in data.product_variation_dates:
					next_day = new_array[each_date_again]
					break
				else:
					days_no_data += 1
					continue
			if not 'last_day_record' in locals():
				after_sold_quantity = ProductBranchOffice.objects.filter(product=data, date_search=next_day)
				for one_day in range(len(dates) - 1):
					for after_sold in after_sold_quantity:
						product_branch_office = ProductBranchOffice.objects.create(
							office=after_sold.office,
							product=after_sold.product,
							branch_stock=after_sold.branch_stock,
							date_search=one_day,
						)

					price_variation_pcfactory = PriceVariationPcFactory.objects.create(
						product=after_sold_quantity[0].product,
						price=after_sold_quantity[0].product.price,
						date_search=one_day,
					)
				break
			else:
				before_sold_quantity = ProductBranchOffice.objects.filter(product=data, date_search=last_day_record)
				after_sold_quantity = ProductBranchOffice.objects.filter(product=data, date_search=next_day)
				#Recorrer cada branch_office de cada product
				for before_sold in before_sold_quantity:
					for after_sold in after_sold_quantity:
						if (before_sold.product.id == after_sold.product.id) and (before_sold.office.id == after_sold.office.id):
							average = round((after_sold.branch_stock - before_sold.branch_stock) / (days_no_data))
							
							product_branch_office = ProductBranchOffice.objects.create(
								office=before_sold.office,
								product=before_sold.product,
								branch_stock=before_sold.branch_stock + average,
								date_search=dates[eachDate],
							)

							price_variation_pcfactory = PriceVariationPcFactory.objects.create(
								product=before_sold.product,
								price=before_sold.product.price,
								date_search=dates[eachDate],
							)
				last_day_record = dates[eachDate]

						
			# product_variation = ProductVariation.objects.create(
			# 	product=data,
			# 	sold_quantity=before_sold_quantity.sold_quantity + average,
			# 	available_quantity=data.available_quantity - average,
			#   	diference_day_before=((before_sold_quantity.sold_quantity + average) - before_sold_quantity.sold_quantity),
			#   	date_search=dates[eachDate]
			# )
			# last_day_record = product_variation.date_search



	# product_variation = ProductVariation.objects.create(
	# 	product=data,
	# 	sold_quantity=sold_quantity,
	# 	available_quantity=quantity,
	#   	diference_day_before=(sold_day_before - sold_quantity)
	# 	)
	print("Variation Fill Empty Created")

start_time = time.time()
data = []
for product in products:
	data.append(product)
pool = ThreadPool(10)
results = pool.map(fill_empty_record_not_working_dates,data)
pool.close()
pool.join()
print("--- %s seconds ---" % (time.time() - start_time))