from products.models import Product, ProductVariation
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from django.utils import timezone
from datetime import date, timedelta
from django.contrib.postgres.aggregates.general import ArrayAgg
products = Product.objects.filter(sub_category__category__channel__name='Mercadolibre').prefetch_related('product_variation').annotate(product_variation_dates=ArrayAgg('product_variation__date_search')).order_by('id')


def fill_empty_record_not_working_dates(data):
	today = date.today()
	min_date = ProductVariation.objects.earliest('date_search').date_search
	delta = today - min_date
	dates = []
	for i in range(delta.days):
		dates.append(min_date + timedelta(i))

	for eachDate in range(len(dates)):
		if dates[eachDate] in data.product_variation_dates: 
			last_day_record = dates[eachDate]
			continue
		else:
			# Este día no posee registro de actividad
			# print(str(dates[eachDate]) + " este dia no lo tiene")
			days_no_data = 1
			new_array = [dates[i] for i in range(eachDate + 1, len(dates))]
			for each_date_again in range(len(new_array)):
				if new_array[each_date_again] in data.product_variation_dates:
					next_day = new_array[each_date_again]
					break
				else:
					days_no_data += 1
					continue
			before_sold_quantity = ProductVariation.objects.filter(product=data, date_search=last_day_record)[0]
			after_sold_quantity = ProductVariation.objects.filter(product=data, date_search=next_day)[0]
			average = int((after_sold_quantity.sold_quantity - before_sold_quantity.sold_quantity) / (days_no_data + 1))
			# print(str(before_sold_quantity.sold_quantity) +"para el día " +str(before_sold_quantity.date_search))
			# print(str(after_sold_quantity.sold_quantity) +"para el día " +str(after_sold_quantity.date_search))
			# print(average)
			product_variation = ProductVariation.objects.create(
				product=data,
				sold_quantity=before_sold_quantity.sold_quantity + average,
				available_quantity=data.available_quantity - average,
			  	diference_day_before=((before_sold_quantity.sold_quantity + average) - before_sold_quantity.sold_quantity),
			  	date_search=dates[eachDate]
			)
			# print(product_variation.__dict__)
			last_day_record = product_variation.date_search
			# break
	# print(last_day_record)
	# print(next_day)
	# print(days_no_data)
	

	# product_variation = ProductVariation.objects.create(
	# 	product=data,
	# 	sold_quantity=sold_quantity,
	# 	available_quantity=quantity,
	#   	diference_day_before=(sold_day_before - sold_quantity)
	# 	)
	print("Variation Fill Empty Created")

start_time = time.time()
data = []
for product in products:
	data.append(product)
pool = ThreadPool(10)
results = pool.map(fill_empty_record_not_working_dates,data)
pool.close()
pool.join()


print("--- %s seconds ---" % (time.time() - start_time))