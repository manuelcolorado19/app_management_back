from categories.models import SubCategories
from products.models import Product, ProductBranchOffice, BranchOffice, PriceVariationPcFactory
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from bs4 import SoupStrainer
from datetime import datetime as dt
import datetime as date_time

products = Product.objects.filter(sub_category__category__channel__name='Pcfactory')
pc_factory_url = "https://www.pcfactory.cl"

def save_quantity_and_available_from_url(data):
	page_response = requests.get(data.url_product, timeout=5)
	content_product = SoupStrainer('div', {'class': 'ficha_cuerpo'})
	page_content = BeautifulSoup(page_response.content, "html.parser", parse_only=content_product)
	content = page_content.find('div', attrs={"class": "ficha_producto_right"})
	
	data_ficha_producto_tiendas = content.find('div',attrs={"class": "ficha_producto_tiendas"})
	ul_data = data_ficha_producto_tiendas.select('ul')[1]
	count = 0
	contador = 0
	for li in ul_data.find_all('li'):
		contador += 1
		branch_office = li.select('a span')[0].text
		if contador == 8:
			continue
		unitys = int(li.select('a span')[1].text.replace('+', ''))
		count = count + unitys
		if branch_office[0] == ' ':
			branch_office = branch_office[3:]
		print(data['name'] +"    " +branch_office + "                           " + str(unitys))
	# 	office = BranchOffice.objects.get(name=branch_office)
	# 	new_product_branch = ProductBranchOffice.objects.create(
	# 		office=office,
	# 		product=data,
	# 		branch_stock=unitys
	# 	)
	# product_update = Product.objects.get(id=data.id)
	# product_update.available_quantity = count
	# product_update.save()
	# price_variation = PriceVariationPcFactory.objects.create(
	# 	product=product_update,
	# 	price=product_update.price
	# )
	# print("Variation created")

start_time = time.time()
data = []
for product in products:
	data.append(product)
pool = ThreadPool(10)
results = pool.map(save_quantity_and_available_from_url,data)
pool.close()
pool.join()

print("--- %s seconds ---" % (time.time() - start_time))

