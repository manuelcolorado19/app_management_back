from products.models import Product, ProductVariation
import requests
from bs4 import BeautifulSoup
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from django.template.loader import render_to_string
from django.core.mail import send_mail
from bs4 import SoupStrainer
from django.utils import timezone
from datetime import timedelta

products = Product.objects.filter(sub_category__category__channel__name='Mercadolibre')

def send_email_notification():
	subject = 'hello'
	from_email = 'asiamerica@test.cl'
	to = "mejiasabelito@gmail.com"
	msg_html = render_to_string(
	   'market-analysis.html',
	   {
	       "name": "Abel Mejias",
	       "url": "http://localhost:8000/export/xls/",
	   }
	)
	send_mail(
	   'Notificación de archivo',
	   subject,
	   from_email,
	   [to, ],
	   html_message=msg_html,
	)


def save_quantity_and_available_from_url(data):
	page_response = requests.get(data.url_product, timeout=5)
	content_product = SoupStrainer(id='short-desc')
	page_content = BeautifulSoup(page_response.content, "html.parser", parse_only=content_product)
	content = page_content.find('div', attrs={"class": "item-conditions"})
	quantity_content = page_content.find('span', attrs={"class": "dropdown-quantity-available"})
	sold_quantity = 0
	if content is not None:
		for t in content.text.split():
			try:
				sold_quantity = int(t.replace('.','').replace(',','.'))
			except ValueError:
				pass
	else:
		sold_quantity = 0
	if quantity_content is not None:
		for a in quantity_content.text.replace('(', '').replace(')', '').split():
			try:
				quantity = int(a.replace('.','').replace(',','.'))
			except ValueError:
				pass
	else:
		quantity = 0
	sold_day_before = ProductVariation.objects.filter(product=data, date_search=(timezone.now() - timedelta(1)))[0].sold_quantity
	product_variation = ProductVariation.objects.create(
		product=data,
		sold_quantity=sold_quantity,
		available_quantity=quantity,
	  	diference_day_before=(sold_quantity - sold_day_before)
		)
	product_update = Product.objects.get(id=data.id)
	product_update.sold_quantity = sold_quantity
	product_update.save()
	print("Variation Created")


start_time = time.time()
data = []
for product in products:
	data.append(product)
pool = ThreadPool(10)
results = pool.map(save_quantity_and_available_from_url,data)
pool.close()
pool.join()

#Send email to inform file is available
send_email_notification()

print("--- %s seconds ---" % (time.time() - start_time))
