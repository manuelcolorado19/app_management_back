import csv
from products.models import Product, ProductVariation
from sellers.models import Seller
from categories.models import SubCategories
list_variation = []
import datetime

data = {}
with open("products/recovery/Variacion 09-11.csv", "U",  encoding="utf8") as csv_file2:
    for row in csv.reader(csv_file2):
        data[row[0]] = row




with open('products/recovery/Productos 09-11-2018.csv',  encoding="utf8") as csv_file, open('products/recovery/Variacion 09-11.csv', "U",  encoding="utf8") as csv_file2:
    # csv_reader = csv.reader(csv_file, delimiter=',')
    csv1_reader = [x for x in csv.reader(csv_file, delimiter=',')]
    csv2_reader = [y for y in csv.reader(csv_file2, delimiter=',')]
    for row in csv1_reader:
        if "MLC" in row[1]:
            q_seller = Seller.objects.filter(id_ml=row[5])
            if not q_seller:
                seller = None
            else:
                seller = q_seller[0]
            if row[9] is not '':
                rating = float(row[9])
            else:
                rating = 0
            if row[12] is not '':
                referencial_price = int(float(row[12]))
            else:
                referencial_price = 0
            if row[8] is not '':
                original_price = int(float(row[8]))
            else:
                original_price = 0
            if row[3] is not '':
                price = int(float(row[3]))
            else:
                price = 0
            product = Product.objects.create(
                code =row[1],
                name=row[2],
                price=price,
                sold_quantity=row[4],
                seller=seller,
                sub_category=SubCategories.objects.get(id=row[6]),
                available_quantity=row[7],
                url_product=row[11],
                original_price=original_price,
                stop_time=datetime.datetime.strptime(row[10], "%Y-%m-%d"),
                rating=rating,
                referencial_price=referencial_price,
            )
            for row2 in csv2_reader:
                if row[0] == row2[4]:
                    date_search = datetime.datetime.strptime(row2[2], "%Y-%m-%d")
                    if row2[1] is not '':
                        sold_quantity = float(row2[1])
                    else:
                        sold_quantity = 0
                    if row2[5] is not '':
                        diference_day_before = float(row2[5])
                    else:
                        diference_day_before = 0
                    if row2[3] is not '':
                        available_quantity = float(row2[3])
                    else:
                        available_quantity = 0
                    variation = ProductVariation.objects.create(
                        sold_quantity=int(sold_quantity),
                        date_search=date_search,
                        product=product,
                        diference_day_before=int(diference_day_before),
                        available_quantity=int(available_quantity)

                    )
            # ProductVariation.objects.bulk_create(list_variation)
            print('Product and variations create')
        else:
            continue

print(len(list_variation))    