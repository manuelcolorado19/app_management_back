import argparse
import asyncio
import sys
# import resource
from aiohttp import ClientSession
from functools import partial
from time import time
from bs4 import BeautifulSoup, SoupStrainer
import json
import requests
from multiprocessing.dummy import Pool as ThreadPool
new_data = []
products = []
start_time = time()

all_data = []
def save_quantity_and_available_from_url(i):
	child_categories = requests.get("https://api.mercadolibre.com/categories/{}".format(i)).json()
	data = child_categories['children_categories']
	# print(len(data))
	for j in data:
		child_categories = requests.get("https://api.mercadolibre.com/categories/{}".format(j['id'])).json()
		for k in child_categories['children_categories']:
			child2_categories = requests.get("https://api.mercadolibre.com/categories/{}".format(k['id'])).json()
			for grand_child in child2_categories['children_categories']:
				all_data.append(grand_child['id']) 
			all_data.append(k['id'])
		all_data.append(j['id'])
	# print(len(all_data))

all_categories = requests.get("https://api.mercadolibre.com/sites/MLC/categories").json()

data = []
data_append = []

async def run(session, number_of_requests, concurrent_limit):
    base_url = "https://api.mercadolibre.com/sites/MLC/search?category={}"
    second_childs = []
    tasks = []
    responses = []
    sem = asyncio.Semaphore(concurrent_limit)

    async def fetch(i):
        url = base_url.format(i)
        async with session.get(url) as response:
            response = await response.read()
            # page_content = BeautifulSoup(response.decode('utf-8'), 'lxml')
            sem.release()
            responses.append(response)
            data_json = json.loads(response)
           
            for each in data_json['results']:
            	if each['sold_quantity'] >= 5:
            		products.append(each['id'])
            # print("request made")
            return response

    for i in all_data:
        await sem.acquire()
        task = asyncio.ensure_future(fetch(i))
        task.add_done_callback(tasks.remove)
        tasks.append(task)

    await asyncio.wait(tasks)
    print("total_responses: {}".format(len(responses)))
    # print("Segundos hijos: {}".format(total))
    total_products = list(set(products))
    print("Total de productos con 50: {}".format(len(total_products)))
    # print("Total de productos con 50: {}".format(len(products)))
    f= open("Busqueda.txt","w+")
    for j in total_products:
    	f.write("%s\r" % (j))
    f.close()
    print("--- %s seconds ---" % (time() - start_time))

    return responses

async def main(number_of_requests, concurrent_limit):
    async with ClientSession() as session:
        responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
        return

if __name__ == '__main__':
    """
    run: python client.py -n 1000 -c 100
    """
    for i in all_categories:
    	data_append.append(i['id'])
    	all_data.append(i['id'])

    pool = ThreadPool(20)
    results = pool.map(save_quantity_and_available_from_url, data_append)
    print(len(all_data))
    parser = argparse.ArgumentParser()
    parser.add_argument('-n')
    parser.add_argument('-c')
    args = parser.parse_args()
    number_of_requests = int(args.n) if args.n else 10000
    concurrent_limit = int(args.c) if args.c else 1000
    print("number_of_requests: {}, concurrent_limit: {}".format(number_of_requests, concurrent_limit))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(number_of_requests, concurrent_limit))
    loop.close()



	

