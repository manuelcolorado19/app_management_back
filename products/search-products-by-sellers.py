import argparse
import asyncio
import sys
# import resource
from aiohttp import ClientSession
from functools import partial
from time import time
from bs4 import BeautifulSoup, SoupStrainer
import json
import requests
from multiprocessing.dummy import Pool as ThreadPool
from operator import itemgetter
from openpyxl import load_workbook
wb = load_workbook('sellers/Sellers.xlsm')
ws = wb['Hoja1']
# all_sellers = []
sellers_ids = []
count_sellers = 0
count = 0
for row in ws.rows:
    count += 1
    if count > 1:
        sellers_ids.append(str(row[0].value))
start_time = time()

async def run(session, number_of_requests, concurrent_limit):
    base_url = "https://api.mercadolibre.com/sites/MLC/search?seller_id={}"
    second_childs = []
    tasks = []
    responses = []
    count_sellers = []
    sem = asyncio.Semaphore(concurrent_limit)

    async def fetch(i):
        url = base_url.format(i)
        async with session.get(url) as response:
            response = await response.read()
            sem.release()
            responses.append(response)
            data_json = json.loads(response)
            if data_json['paging']['total'] > 0 and data_json['paging']['total'] < 10001:
                count_sellers.append({"id":i,"total":data_json['paging']['total']})
            # print("request made")
            return response

    for i in sellers_ids:
        await sem.acquire()
        task = asyncio.ensure_future(fetch(i))
        task.add_done_callback(tasks.remove)
        tasks.append(task)

    await asyncio.wait(tasks)
    print("total_responses: {}".format(len(responses)))
    total = 0
    # for j in count_sellers:
    #     total += j
    # newlist = sorted(count_sellers, key=itemgetter('total'), reverse=True)
    # print(json.dumps(newlist))
    # print(len(newlist))
    urls = []
    for seller_total in count_sellers:
        if seller_total['total'] < 50:
            urls.append("https://api.mercadolibre.com/sites/MLC/search?seller_id={}".format(seller_total['id']))
        else:
            iteration_end = int(seller_total['total']/50)
            module = int(seller_total['total']%50)
            for i in range(iteration_end + 1):
                urls.append("https://api.mercadolibre.com/sites/MLC/search?seller_id={}&offset={}".format(seller_total['id'],str(i*50)))
            # urls.append("https://api.mercadolibre.com/sites/MLC/search?seller_id={}&offset={}".format(seller_total['id'],str(((iteration_end*50) + module))))

    f= open("Links.txt","w+")
    for j in urls:
        f.write("%s\r" % (j))
    f.close()
    print("--- %s seconds ---" % (time() - start_time))
    return responses

async def main(number_of_requests, concurrent_limit):
    async with ClientSession() as session:
        responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
        return

if __name__ == '__main__':
    """
    run: python client.py -n 1000 -c 100
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-n')
    parser.add_argument('-c')
    args = parser.parse_args()
    number_of_requests = int(args.n) if args.n else 10000
    concurrent_limit = int(args.c) if args.c else 1000
    print("number_of_requests: {}, concurrent_limit: {}".format(number_of_requests, concurrent_limit))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(number_of_requests, concurrent_limit))
    loop.close()