from rest_framework import serializers
from .models import Product, ProductVariation, MetricsMercadolibre
import datetime
from django.db.models import Max, Min, F, Case, When, Sum, IntegerField, Value
from datetime import date, timedelta
from sellers.models import Seller

class ProductVariationSerializer(serializers.ModelSerializer):
	class Meta:
		model = ProductVariation
		fields = '__all__'

class VariationSerializer(serializers.ListField):
	def to_representation(self, value):
		return ProductVariation.objects.filter(product__id=value.id).values('sold_quantity', 'available_quantity', 'date_search')


class ProductSerializer(serializers.ModelSerializer):
	variations = VariationSerializer(read_only=True, source='*')
	# product_variation = ProductVariationSerializer(read_only=True)
	class Meta:
		model = Product
		depth = 1
		fields = '__all__'

class SellerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Seller
		fields = ('nickname',)

class MetricsMercadolibreSerializer(serializers.ModelSerializer):
	class Meta:
		model = MetricsMercadolibre
		fields = ('rank',)

class AditionalDataSerializer(serializers.DictField):
	def to_representation(self, value):
		now = datetime.datetime.now().date()
		datetime_7 = (now - timedelta(days=7))
		datetime_28 = (now - timedelta(days=28))
		publication_data = dict(value.product_variation.aggregate(min_price=Min('price'), max_price=Max('price'), sales_last_7=Sum(Case(When(date_search__gt=datetime_7, then=F('diference_day_before')), default=0, output_field=IntegerField())), sales_last_28=Sum(Case(When(date_search__gt=datetime_28, then=F('diference_day_before')), default=0, output_field=IntegerField())), recaudacion_ultimos7=Sum(Case(When(date_search__gt=datetime_7, then=F('daily_reward')), default=0, output_field=IntegerField())), recaudacion_ultimos28=Sum(Case(When(date_search__gt=datetime_28, then=F('daily_reward')), When(daily_reward__isnull=True, then=Value(0, output_field=IntegerField())), default=0, output_field=IntegerField()))))
		
		return publication_data

class KeyWordSearchSerializer(serializers.ModelSerializer):
	aditional_data = AditionalDataSerializer(read_only=True, source='*')
	seller = SellerSerializer(read_only=True)
	metrics_product = MetricsMercadolibreSerializer(read_only=True)
	class Meta:
		model = Product
		exclude = ('from_seller_list',)
