from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import ProductViewSet, KeyWordSearchViewSet

router = DefaultRouter()
router.register(r'products', ProductViewSet, base_name='products')
router.register(r'search-publications', KeyWordSearchViewSet, base_name='search-publications')

urlpatterns = [
	url(r'^', include(router.urls)),
	
]
