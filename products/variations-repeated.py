from products.models import Product, ProductVariation
from time import time
import argparse
from aiohttp import ClientSession
import asyncio
from datetime import date, timedelta
from django.db.models import Count
import sys
products = Product.objects.filter(channel__id=1).order_by('id')[70000:80000]
# repeated = []
# for product in products:
# 	variations = product.product_variation.all()
# 	for i in range(1, variations.count() - 1):
# 		if variations[i-1].date_search == variations[i].date_search:
# 			repeated.append(variations[i].id)
# 			break
start_time = time()
# min_date = ProductVariation.objects.earliest('date_search').date_search
# today_date = ProductVariation.objects.latest('date_search').date_search
# delta = today_date - min_date

# dates_array = [(min_date + timedelta(i)) for i in range(delta.days + 1)]
# print(dates_array)

async def run(session, number_of_requests, concurrent_limit):
	base_url = "https://api.mercadolibre.com/items/{}"
	tasks = []
	responses = []
	repeated = []

	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		# url = base_url.format(i)
		# print("hi")
		variations = i.product_variation.values('date_search').annotate(Count('id')).order_by().filter(id__count__gt=1)
		print(variations)
		for t in variations:
			for y in range(t['id__count'] - 1):
				ProductVariation.objects.filter(product__id=i.id, date_search=t['date_search']).first().delete()
				# break
		

	for i in products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	# print("total_repeated: {}".format(len(repeated)))
	# e = ProductVariation.objects.filter(id__in=repeated).delete()
	# print(e)
	# date_id_repeated = []
	# for repeat in repeated:
	# 	for date1 in dates_array:
	# 		if ProductVariation.objects.filter(product__id=repeat, date_search=date1).count() > 1:
	# 			date_id_repeated.append({"id": repeated, "date": date1})
	# print(len(date_id_repeated))


async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return

# print(len(all_products))
# print(len(repeated_remove))
loop = asyncio.get_event_loop()
loop.run_until_complete(main(10000, 10000))
loop.close()
print("--- %s seconds ---" % (time() - start_time))
