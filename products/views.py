from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Product, ProductVariation, ProductBranchOffice, BranchOffice, MetricsMercadolibre
from .serializers import ProductSerializer, KeyWordSearchSerializer
from rest_framework.permissions import AllowAny
import xlwt
import xlsxwriter
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import date, timedelta
from django.http import HttpResponse
from django.db.models import Prefetch, Sum, F, Func, Aggregate, FloatField, Case, When, Avg, IntegerField, Count, Q, CharField, Value, Max, Min
from django.db.models.functions import Concat, Cast
import datetime
import io
import time
from django.http import StreamingHttpResponse
from .excel import retrieve_excel_ml, retrieve_excel_pcfactory
import requests
from notifications.models import Token
import os, base64
from rest_framework import status
import requests
from django.core.mail import send_mail, EmailMessage
from io import BytesIO
from rest_framework import filters as search_filter
from rest_framework.pagination import PageNumberPagination
from sellers.models import Seller, SellerVariation
from random import sample
# from sales.models import Product_sale, Product_Code, Pack
datetime_7 = (datetime.datetime.today() - timedelta(days=7)).date()
datetime_15 = (datetime.datetime.today() - timedelta(days=15)).date()
datetime_30 = (datetime.datetime.today() - timedelta(days=30)).date()
datetime_60 = (datetime.datetime.today() - timedelta(days=60)).date()
datetime_90 = (datetime.datetime.today() - timedelta(days=90)).date()

def nth_repl(s, sub, repl, nth):
    find = s.find(sub)
    # if find is not p1 we have found at least one match for the substring
    i = find != -1
    # loop util we find the nth or we find no match
    while find != -1 and i != nth:
        # find + 1 means we start at the last match start index + 1
        find = s.find(sub, find + 1)
        i += 1
    # if i  is equal to nth we found nth matches so replace
    if i == nth:
        return s[:find]+repl+s[find + len(sub):]
    return s

class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = ProductSerializer

class ExportDataMercadolibreXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def get(self, request):
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Análisis Mercadolibre' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		ws.freeze_panes(1, 0)
		rows = Product.objects.filter(
			sub_category__category__channel__name='Mercadolibre', from_seller_list=False
			).prefetch_related('product_variation').annotate(
				sale_total=Sum('product_variation__diference_day_before'), 
				category_2=F('sub_category__name'), 
				category_1=F('sub_category__category__name'), 
				nickname=F('seller__nickname'), 
				last_7=Sum(
					Case(
						When(product_variation__date_search=datetime_7,
							then=F('sold_quantity') - F('product_variation__sold_quantity')
							),
						default=0,
			            output_field=IntegerField()
            	)), 
				last_15=Sum(
					Case(
						When(product_variation__date_search=datetime_15,
							then=F('sold_quantity') - F('product_variation__sold_quantity')
							),
						default=0,
			            output_field=IntegerField()
            	)),
				last_30=Sum(
					Case(
						When(product_variation__date_search=datetime_30,
							then=F('sold_quantity') - F('product_variation__sold_quantity')
							),
						default=0,
			            output_field=IntegerField()
            	)),
				last_60=Sum(
					Case(
						When(product_variation__date_search=datetime_60,
							then=F('sold_quantity') - F('product_variation__sold_quantity')
							),
						default=0,
			            output_field=IntegerField()
            	)),
				last_90=Sum(
					Case(
						When(product_variation__date_search=datetime_90,
							then=F('sold_quantity') - F('product_variation__sold_quantity')
							),
						default=0,
			            output_field=IntegerField()
            	)),
				trend=F('sub_category'), 
				median=F('sub_category')
			).order_by('-sale_total')
		# print(rows.count())

		columns = [{"name": 'Código', "width":12, "rotate": False}, {"name": 'Titulo', "width": 40, "rotate": False}, {"name": 'Categoria 1', "width": 14, "rotate": False}, {"name": 'Categoria 2', "width": 14, "rotate": False}, {"name": 'Precio', "width": 7.4, "rotate": True}, {"name": 'Cantidad Vendida', "width": 7, "rotate": True}, {"name": 'Id Vendedor', "width": 10, "rotate": True},{"name": 'Nickname', "width": 10, "rotate": True}, {"name": 'Cantidad disponible', "width": 5.5, "rotate": True}, {"name": 'Url producto', "width": 30, "rotate": False}, {"name": 'Fecha Publicada', "width": 10, "rotate": True}, {"name": 'Ventas Probables', "width": 5, "rotate": True}, {"name": 'Últimos 7', "width": 5, "rotate": True}, {"name": 'Últimos 15', "width": 5, "rotate": True}, {"name": 'Últimos 30', "width": 5, "rotate": True}, {"name": 'Últimos 60', "width": 5, "rotate": True}, {"name": 'Últimos 90', "width": 5, "rotate": True}, {"name": 'Moda', "width": 8, "rotate": False}, {"name": 'Mediana', "width": 10, "rotate": False}]

		row_num = 0
		min_date = ProductVariation.objects.earliest('date_search').date_search
		delta = today - min_date
		dates = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		for i in range(delta.days + 1):
			# columns.append(min_date + timedelta(i))
			columns.append({"name": min_date + timedelta(i), "width": 5.5, "rotate": True})
			dates += 1

		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])

		number_columns = {"code": 0, "name": 1, "category_1": 2, "category_2": 3, "price": 4, "sold_quantity": 5, "seller_id": 6, "nickname": 7,"available_quantity": 8, "url_product": 9, "stop_time": 10, "probably_sales": 11, "last_7": 12, "last_15": 13, "last_30": 14, "last_60": 15, "last_90": 16, "trend": 17, "median": 18}
		for row in rows:
			row_num += 1
			for col_num in number_columns:
				if col_num == 'probably_sales':
					if row.sale_total:
						probably_sales = int(row.sale_total / dates)
					else:
						probably_sales = 0
					ws.write(row_num, number_columns[col_num], probably_sales, cell_format_data)
				else:
					if col_num == 'stop_time':
						ws.write(row_num, number_columns[col_num], str(row.__dict__[col_num]), cell_format_data)
					else:
						ws.write(row_num, number_columns[col_num], row.__dict__[col_num], cell_format_data)
			for col_num in range(19, len(columns)):
				for sale in row._prefetched_objects_cache['product_variation']:
					if str(columns[col_num]['name']) == str(sale.date_search):
						ws.write(row_num, col_num, int(sale.sold_quantity), cell_format_data)
		
		ws.autofilter(0, 0, 0, len(columns) - 1)
		wb.close()
		output.seek(0)
		filename = 'Análisis Mercadolibre.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		print("--- %s seconds ---" % (time.time() - start_time))
		return response

class DownloadXLSXMercadolibre(APIView):
	permission_classes = (AllowAny,)
	def get(self, request):
		date_file = self.request.query_params.get('date_file', None)
		if date_file is not None:
			file_name = 'Analisis Mercadolibre ' + date_file +'.xlsx'
		else:
			today = date.today()
			file_name = 'Analisis Mercadolibre ' + str(today) +'.xlsx'
		file_path = os.path.join(os.path.dirname(os.path.realpath(__name__)), file_name)
		response = HttpResponse(open(file_path, 'rb').read())
		response['Content-Type'] = 'mimetype/submimetype'
		response['Content-Disposition'] = 'attachment; filename=' + file_name
		return response

class ExportDataPcfactoryXLSXViewSet(APIView):
	permission_classes = (AllowAny,)


	def get(self, request):
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Análisis de Mercado Pc Factory' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		# ws.freeze_panes(1, 0)
		# ws.freeze_panes('A2')
		ws.freeze_panes(2, 7)
		# rows = Product.objects.filter(sub_category__category__channel__name='Pcfactory').prefetch_related(Prefetch('product_branch_office', queryset=ProductBranchOffice.objects.order_by('id', 'office__order'))).order_by('id')[:1000]
		query = Product.objects.filter(sub_category__category__channel__name='Pcfactory').prefetch_related(Prefetch('product_branch_office', queryset=ProductBranchOffice.objects.order_by('id')))

		rows = [{'code': parent.code, "name": parent.name, "price": parent.price, "available_quantity": parent.available_quantity, "url_product": parent.url_product, "original_price": parent.original_price, "referencial_price": parent.referencial_price, 'children': [{'name_office': child.office.name, 'date_search':child.date_search, 'branch_stock': child.branch_stock} for child in parent.product_branch_office.all()]} for parent in query]


		columns = [{"name": 'Código',  "width": 6.5, "rotate": False}, {"name": 'Titulo', "width": 40, "rotate": False}, {"name": 'Precio Efectivo', "width": 7, "rotate": True}, {"name": 'Cantidad disponible', "width": 5, "rotate": True}, {"name": 'Url producto', "width": 30, "rotate": False}, {"name": 'Precio original', "width": 7, "rotate": True}, {"name": 'Precio referencia', "width": 7, "rotate": True}]
		
		row_num = 0
		min_date = ProductBranchOffice.objects.earliest('date_search').date_search
		branch_office = BranchOffice.objects.values('name').order_by('order')
		delta = today - min_date
		date_quantity = 0
		counter = 0
		new_counter = 0
		offices_position = {}
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_dates = wb.add_format({'bold': True,'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_dates.set_center_across()
		cell_format_offices = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'gray', 'border': 1, 'font_size': 9})
		cell_format_offices.set_center_across()
		for i in range(delta.days + 1):
			date_quantity += 1
			columns.append({"name": min_date + timedelta(i), "width": 4000, "rotate": False})
		for col_num in range(len(columns)):
			if col_num > 6:
				counter += 1
				if counter > 1 and counter < 3:
					real_col_num = col_num
					col_num = col_num + 30
					before_col_num = col_num
					new_counter += 1
				elif counter > 2:
					new_counter += 1
					real_col_num = col_num
					col_num = col_num + 30 * new_counter
					before_col_num = col_num
				else:
					pass
				if col_num > 30:
					for r in range(col_num, col_num + 31):
						offices_position[str(r)] = {"date_search": columns[real_col_num]['name']}
					ws.merge_range(0, col_num, 0, col_num + 30, str(columns[real_col_num]['name']), cell_format_dates)
				else:
					for r in range(col_num, col_num + 31):
						offices_position[str(r)] = {"date_search": columns[col_num]['name']}
					ws.merge_range(0, col_num, 0, col_num + 30, str(columns[col_num]['name']), cell_format_dates)
			else:
				if columns[col_num]['rotate']:
					ws.merge_range(0, col_num, 1, col_num, str(columns[col_num]['name']), cell_format_fields_90)
					ws.set_column(col_num, col_num, columns[col_num]['width'])
				else:
					ws.merge_range(0, col_num, 1, col_num, str(columns[col_num]['name']), cell_format_fields)
					ws.set_column(col_num, col_num, columns[col_num]['width'])
		row_num += 1
		real_col_num = 7
		
		for each_date in range(date_quantity):
			for col_num in range(branch_office.count()):
				ws.write(row_num, real_col_num, str(branch_office[col_num]['name']), cell_format_offices)
				ws.set_column(real_col_num, real_col_num, 3)
				if offices_position[str(real_col_num)]:
					offices_position[str(real_col_num)]["name_office"] = branch_office[col_num]['name']
				real_col_num += 1



		number_columns = {"code": 0, "name": 1, "price": 2, "available_quantity": 3, "url_product": 4, "original_price": 5, "referencial_price": 6}


		ws.set_column(1, 1, 40)


		cell_format_data = wb.add_format({'font_size': 9.5})
		for row in rows:
			row_num += 1
			for col_num in number_columns:
				ws.write(row_num, number_columns[col_num], row[col_num], cell_format_data)
			col_num = 7
			for child in row['children']:
				for s in range(7, (((delta.days + 1) * 31) + 7)):
					if child['name_office'] == offices_position[str(s)]['name_office'] and child['date_search'] == offices_position[str(s)]['date_search']:
						ws.write(row_num, s, int(child['branch_stock']), cell_format_data)
		ws.autofilter('A2:ZZ2')
		wb.close()
		output.seek(0)
		filename = 'Análisis de Mercado Pc Factory.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		print("--- %s seconds ---" % (time.time() - start_time))
		return response


class ExportOrdersXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def f(self, x):
	    return {
	        'pending': "Pendiente",
	        'handling': "En manejo",
	        'ready_to_ship': "Listo para envío",
	        'shipped': "Enviado",
	        'delivered': "Entregado",
	        'not_delivered': "No entregado",
	        'cancelled': "Cancelado",
	    }[x]

	def g(self, x):
	    return {
	        'ready_to_print': "Listo para imprimir",
	        'printed': "Impresa",
	    }[x]

	def get(self, request):
		date_from = self.request.query_params.get('hour_from', (datetime.datetime.today() - datetime.timedelta(days=21)).replace(hour=9, minute=00).strftime("%Y-%m-%dT%H:%M:%S.000%z-00:00"))
		date_to = self.request.query_params.get('hour_to', datetime.datetime.today().replace(hour=23, minute=00).strftime("%Y-%m-%dT%H:%M:%S.000%z-00:00"))
		today = datetime.datetime.today()
		output = io.BytesIO()
		file_name = 'Ordenes de compra'
		wb =  xlsxwriter.Workbook(output)
		for sheet in range(2):
			if sheet == 1:
				sheet_name = 'Envíos gratis ' + str(today.date())
				header = 'Ordenes con Etiquetas ' + str(today.time())
			else:
				sheet_name = 'Envíos por acordar ' + str(today.date())
				header = 'Ordenes sin Etiquetas ' + str(today.time())
			ws = wb.add_worksheet(sheet_name)
			ws.set_header(header, {'font_size': 18, 'bold': True})
			ws.repeat_rows(0)
			ws.set_margins(0.196, 0.196, 0.55, 0.55)
			ws.set_landscape()
			url_ml = "https://api.mercadolibre.com/orders/"
			token = Token.objects.get(id=1).token_ml
			cell_format_fields = wb.add_format({'bold': True, 'font_size': 10, 'bg_color': 'gray', 'border': 1})
			cell_format_fields.set_center_across()

			cell_format_data = wb.add_format({'font_size': 8, 'border': 1})
			cell_format_data.set_text_wrap()
			cell_format_data.set_align('vcenter')
			cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'font_size': 10, 'bg_color': 'gray', 'border': 1})
			cell_format_fields_90.set_center_across()

			cell_format_text_wrap = wb.add_format({'font_size': 8, 'border': 1})
			cell_format_text_wrap.set_text_wrap()
			cell_format_text_wrap.set_align('top')
			if sheet == 1:
				url_request = url_ml + "search?seller=216244489"+ "&access_token="+ token +"&order.date_created.from="+ date_from +"&order.date_created.to="+ date_to + "&shipping.status=ready_to_ship&shipping.substatus=ready_to_print,printed&sort=date_desc"
				

				columns = [{"name": 'N° de Venta', "width": 11, "rotate": True}, {"name": 'Fecha compra', "width": 8, "rotate": True}, {"name": 'N° de Seguimiento', "width": 9, "rotate": True}, {"name": 'Estado', "width": 9, "rotate": True}, {"name": 'Estado etiqueta', "width": 5, "rotate": True}, {"name": 'Comprador(a)', "width": 7, "rotate": True},{"name": 'SKU', "width": 4, "rotate": True},{"name": 'Producto', "width": 12, "rotate": True}, {"name": 'Cantidad', "width": 3, "rotate": True},  {"name": 'Precio', "width": 7, "rotate": True},  {"name": 'Total', "width": 6, "rotate": True}, {"name": 'Envío', "width": 4, "rotate": True}, {"name": 'Comisión', "width": 7, "rotate": True}, {"name": 'Ganancia', "width": 6, "rotate": True}, {"name": 'Ultimos mensajes', "width": 17, "rotate": True}, {"name": 'Notas', "width": 13, "rotate": True}]
			else:
				url_request = url_ml + "search?seller=216244489"+ "&access_token="+ token +"&order.date_created.from="+ date_from +"&order.date_created.to="+ date_to + "&shipping.status=to_be_agreed&feedback.status=pending&sort=date_desc"

				columns = [{"name": 'N° de Venta', "width": 8, "rotate": True}, {"name": 'Fecha compra', "width": 8, "rotate": True}, {"name": 'Comprador(a)', "width": 9, "rotate": True},{"name": 'SKU', "width": 4, "rotate": True},{"name": 'Producto', "width": 18, "rotate": True}, {"name": 'Cantidad', "width": 3, "rotate": True},  {"name": 'Precio', "width": 7, "rotate": True},  {"name": 'Total', "width": 6, "rotate": True}, {"name": 'Envío', "width": 6, "rotate": True}, {"name": 'Comisión', "width": 6, "rotate": True}, {"name": 'Ganancia', "width": 8, "rotate": True}, {"name": 'Ultimos mensajes', "width": 24, "rotate": True}, {"name": 'Notas', "width": 17, "rotate": True}]


			data = requests.get(url_request).json()
			if data['paging']['total'] <= 50:
				rows = data["results"]
			else:
				rows = []
				count = 0
				iteration_end = int(data['paging']['total']/50)
				module = int(data['paging']['total']%50)
				for i in range(iteration_end + 1):
					new_rows = requests.get(url_request + "&offset=" + str(i*50)).json()['results']
					rows = rows + new_rows
				rows = rows + requests.get(url_request + "&offset="+str(((iteration_end*50) + module))).json()['results']
			row_num = 0
			for col_num in range(len(columns)):
				if columns[col_num]['rotate']:
					ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				else:
					ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			row_num += 1
			for row in rows:
				cell_format_data = wb.add_format({'font_size': 8, 'border': 1})
				cell_format_data.set_text_wrap()
				cell_format_data.set_align('vcenter')
				cell_format_text_wrap = wb.add_format({'font_size': 8, 'border': 1})
				cell_format_text_wrap.set_text_wrap()
				cell_format_text_wrap.set_align('top')
				messages = requests.get("https://api.mercadolibre.com/messages/orders/" + str(row['id']) + "?access_token=" +token)
				#print(row['id'])
				if messages.status_code != 500:
					messages = messages.json()['results'][:3]
				else:
					messages = []
				notas = requests.get("https://api.mercadolibre.com/orders/" + str(row['id']) + "/notes?access_token=" +token).json()
				if notas:
					notas = notas[0]['results'][:3]
				size_messages = len(messages)
				counter = 0
				if sheet == 1:
					if size_messages < 2:
						if size_messages == 1:
							counter = 1
						size_messages += 1
						comision = row['order_items'][0].get('sale_fee', 0) * row['order_items'][0]['quantity']
						total = requests.get("https://api.mercadolibre.com/orders/" + str(row['id']) + "?access_token=" +token).json()['total_amount_with_shipping']
						date_order = datetime.datetime.strptime(nth_repl(row['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z")
						shipping_data = requests.get("https://api.mercadolibre.com/shipments/" + str(row['shipping']['id']) + "?access_token=" +token).json()
						shipping_cost1 = shipping_data['shipping_option'].get('cost', 0)
						shipping_cost2 = shipping_data['shipping_option'].get('list_cost', 0)
						ganancia = (total - ((shipping_cost1 - shipping_cost2)* (-1)) - comision)
						# print(str(row['id']))
						# print(shipping_data['shipping_option'].get('cost', 0))
						# print(shipping_data['shipping_option'].get('list_cost', 0))
						if (float(ganancia/total)) < 0.70:
							cell_format_data.set_bg_color('red')
							cell_format_text_wrap.set_bg_color('red')
						costo_envio = row['shipping']['cost']
						ws.write(row_num, 0, row['id'], cell_format_data)
						ws.write(row_num, 1, (str(date_order.date()) + " " +str(date_order.time())), cell_format_data)
						ws.write(row_num, 2, shipping_data['tracking_number'], cell_format_data)
						ws.write(row_num, 3, self.f(row['shipping']['status']), cell_format_data)
						ws.write(row_num, 4, self.g(row['shipping']['substatus']), cell_format_data)
						ws.write(row_num, 5, str(row['buyer']['first_name'] + " " + row['buyer']['last_name']), cell_format_data)
						ws.write(row_num, 6, str(row['order_items'][0]['item']['seller_custom_field']), cell_format_data)
						ws.write(row_num, 7, str(row['order_items'][0]['item']['title']), cell_format_data)
						ws.write(row_num, 8, row['order_items'][0]['quantity'], cell_format_data)
						ws.write(row_num, 9, row['order_items'][0]['unit_price'], cell_format_data)
						ws.write(row_num, 10, total, cell_format_data)
						ws.write(row_num, 11, ((shipping_cost1 - shipping_cost2)* (-1)), cell_format_data)
						ws.write(row_num, 13, ganancia,  cell_format_data)
							
						ws.write(row_num, 12, comision, cell_format_data)
						x = 0
						y = 0
						for i in messages:
							if len(i['text']['plain']) > 300:
								texto = i['text']['plain'][:300] + '...'
								if 'Muchas Gracias por su compra' in i['text']['plain']:
									texto = i['text']['plain'][:30] + '...'
							else:
								texto = i['text']['plain']
							ws.write(row_num + x, 14, texto, cell_format_text_wrap)
							if not notas:
								ws.write(row_num + x, 15, "", cell_format_text_wrap)
							x += 1

						for f in notas:
							ws.write(row_num + y, 15, f['note'], cell_format_text_wrap)
							y += 1
						if y < len(messages):
							for i in range(y, len(messages), 1):
								ws.write(row_num + i, 15, "", cell_format_text_wrap)
					else:
						comision = row['order_items'][0].get('sale_fee', 0) * row['order_items'][0]['quantity']
						total = requests.get("https://api.mercadolibre.com/orders/" + str(row['id']) + "?access_token=" +token).json()['total_amount_with_shipping']
						date_order = datetime.datetime.strptime(nth_repl(row['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z")
						shipping_data = requests.get("https://api.mercadolibre.com/shipments/" + str(row['shipping']['id']) + "?access_token=" +token).json()
						ganancia = total - (shipping_data['shipping_option'].get('cost', 0) - (shipping_data['shipping_option'].get('list_cost', 0)) * (-1)) - comision
						if (float(ganancia/total)) < 0.70:
							cell_format_data.set_bg_color('red')
							cell_format_text_wrap.set_bg_color('red')
						ws.merge_range(row_num, 0, (row_num + size_messages - 1), 0, row['id'], cell_format_data)
						ws.merge_range(row_num, 1, (row_num + size_messages - 1), 1, (str(date_order.date()) + " " +str(date_order.time())), cell_format_data)
						tracking_number = shipping_data['tracking_number']
						ws.merge_range(row_num, 2, (row_num + size_messages -1), 2, tracking_number, cell_format_data)
						costo_envio = ((shipping_data['shipping_option']['cost'] - shipping_data['shipping_option']['list_cost'] * (-1)))
						ws.merge_range(row_num, 3, (row_num + size_messages -1), 3, self.f(row['shipping']['status']), cell_format_data)
						ws.merge_range(row_num, 4, (row_num + size_messages -1), 4, self.g(row['shipping']['substatus']), cell_format_data)
						ws.merge_range(row_num, 5, (row_num + size_messages -1), 5, str(row['buyer']['first_name'] + " " + row['buyer']['last_name']), cell_format_data)
						ws.merge_range(row_num, 6, (row_num + size_messages -1), 6, str(row['order_items'][0]['item']['seller_custom_field']), cell_format_data)
						ws.merge_range(row_num, 7, (row_num + size_messages -1), 7, str(row['order_items'][0]['item']['title']), cell_format_data)
						ws.merge_range(row_num, 8, (row_num + size_messages -1), 8, row['order_items'][0]['quantity'], cell_format_data)
						ws.merge_range(row_num, 9, (row_num + size_messages -1), 9, row['order_items'][0]['unit_price'], cell_format_data)

						ws.merge_range(row_num, 10, (row_num + size_messages -1), 10, total, cell_format_data)
						ws.merge_range(row_num, 11, (row_num + size_messages -1), 11, ((shipping_data['shipping_option']['cost'] - shipping_data['shipping_option']['list_cost'] * (-1))), cell_format_data)
						ws.merge_range(row_num, 12, (row_num + size_messages -1), 12, comision, cell_format_data)
						ws.merge_range(row_num, 13, (row_num + size_messages -1), 13, ganancia, cell_format_data)
						x = 0
						y = 0
						# Mensajes
						for i in messages:
							if len(i['text']['plain']) > 300:
								texto = i['text']['plain'][:300] + '...'
								if 'Muchas Gracias por su compra' in i['text']['plain']:
									texto = i['text']['plain'][:30] + '...'
							else:
								texto = i['text']['plain']
							ws.write(row_num + x, 14, texto, cell_format_text_wrap)
							if not notas:
								ws.write(row_num + x, 15, "", cell_format_text_wrap)
							x += 1

						#Notas
						for f in notas:
							ws.write(row_num + y, 15, f['note'], cell_format_text_wrap)
							y += 1
						if y < len(messages):
							for i in range(y, len(messages), 1):
								ws.write(row_num + i, 15, "", cell_format_text_wrap)
					if counter == 1:
						size_messages = 1
					row_num = row_num + size_messages

				else:
					if size_messages < 2:
						if size_messages == 1:
							counter = 1
						size_messages += 1
						if row['payments'][0]['status'] == 'refunded':
							continue
						ws.write(row_num, 0, row['id'], cell_format_data)
						date_order = datetime.datetime.strptime(nth_repl(row['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z")
						ws.write(row_num, 1, (str(date_order.date()) + " " +str(date_order.time())), cell_format_data)
						# ws.write(row_num, 2, "Por acordar", cell_format_data)
						ws.write(row_num, 2, str(row['buyer']['first_name'] + " " + row['buyer']['last_name']), cell_format_data)
						ws.write(row_num, 3, str(row['order_items'][0]['item']['seller_custom_field']), cell_format_data)
						ws.write(row_num, 4, str(row['order_items'][0]['item']['title']), cell_format_data)
						ws.write(row_num, 5, row['order_items'][0]['quantity'], cell_format_data)
						ws.write(row_num, 6, row['order_items'][0]['unit_price'], cell_format_data)
						ws.write(row_num, 7,row['total_amount'], cell_format_data)
						ws.write(row_num, 8, 0, cell_format_data)
						comision = row['order_items'][0].get('sale_fee', 0)
						ws.write(row_num, 9, comision, cell_format_data)
						ws.write(row_num, 10, row['total_amount'] - comision, cell_format_data)
						costo_envio = 0
						x = 0
						y = 0
						for i in messages:
							if len(i['text']['plain']) > 300:
								texto = i['text']['plain'][:300] + '...'
								if 'Muchas Gracias por su compra' in i['text']['plain']:
									texto = i['text']['plain'][:30] + '...'
							else:
								texto = i['text']['plain']
							ws.write(row_num + x, 11, texto, cell_format_text_wrap)
							if not notas:
								ws.write(row_num + x, 12, "", cell_format_text_wrap)
							x += 1

						for f in notas:
							ws.write(row_num + y, 12, f['note'], cell_format_text_wrap)
							y += 1
						if y < len(messages):
							for i in range(y, len(messages), 1):
								ws.write(row_num + i, 12, "", cell_format_text_wrap)
					else:
						if row['payments'][0]['status'] == 'refunded':
							continue
						ws.merge_range(row_num, 0, (row_num + size_messages - 1), 0, row['id'], cell_format_data)
						date_order = datetime.datetime.strptime(nth_repl(row['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z")
						ws.merge_range(row_num, 1, (row_num + size_messages - 1), 1, (str(date_order.date()) + " " +str(date_order.time())), cell_format_data)
						# ws.merge_range(row_num, 2, (row_num + size_messages -1), 2, "Por acordar", cell_format_data)
						ws.merge_range(row_num, 2, (row_num + size_messages -1), 2, str(row['buyer']['first_name'] + " " + row['buyer']['last_name']), cell_format_data)
						ws.merge_range(row_num, 3, (row_num + size_messages -1), 3, str(row['order_items'][0]['item']['seller_custom_field']), cell_format_data)
						ws.merge_range(row_num, 4, (row_num + size_messages -1), 4, str(row['order_items'][0]['item']['title']), cell_format_data)
						ws.merge_range(row_num, 5, (row_num + size_messages -1), 5, row['order_items'][0]['quantity'], cell_format_data)
						ws.merge_range(row_num, 6, (row_num + size_messages -1), 6, row['order_items'][0]['unit_price'], cell_format_data)
						ws.merge_range(row_num, 7, (row_num + size_messages -1), 7,  row['total_amount'], cell_format_data)
						ws.merge_range(row_num, 8, (row_num + size_messages -1), 8, 0, cell_format_data)
						comision = row['order_items'][0].get('sale_fee', 0)
						ws.merge_range(row_num, 9, (row_num + size_messages -1), 9,  comision, cell_format_data)
						ws.merge_range(row_num, 10, (row_num + size_messages -1), 10, (row['total_amount'] - comision), cell_format_data)
						x = 0
						y = 0
						for i in messages:
							if len(i['text']['plain']) > 300:
								texto = i['text']['plain'][:300] + '...'
								if 'Muchas Gracias por su compra' in i['text']['plain']:
									texto = i['text']['plain'][:30] + '...'
							else:
								texto = i['text']['plain']
							ws.write(row_num + x, 11, texto, cell_format_text_wrap)
							if not notas:
								ws.write(row_num + x, 12, "", cell_format_text_wrap)
							x += 1

						for f in notas:
							ws.write(row_num + y, 12, f['note'], cell_format_text_wrap)
							y += 1
						if y < len(messages):
							for i in range(y, len(messages), 1):
								ws.write(row_num + i, 12, "", cell_format_text_wrap)
					if counter == 1:
						size_messages = 1
					row_num = row_num + size_messages
		wb.close()
		output.seek(0)
		filename = 'Ordenes de compra.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		# print("--- %s seconds ---" % (time.time() - start_time))
		return response

class SendOrdersEmail(APIView):
	def send_email_notification(self, attachment_path, mimetype='application/octet-stream', fail_silently=False, emails=[]):
		subject = 'Ordenes del día'
		from_email = 'mejiasabelito@gmail.com'
		to = emails
		message = "Ordenes del día"
		email = EmailMessage(subject, message, to=to)
		output2 = BytesIO()
		file_name = os.path.basename(attachment_path)
		data = open(attachment_path, 'rb').read()
		encoded = base64.b64encode(data)
		email.attach_file(attachment_path)
		email.send(fail_silently=fail_silently)

	def post(self, request):
		start_time = time.time()
		date_from = (datetime.datetime.today() - datetime.timedelta(days=21)).replace(hour=1, minute=00).strftime("%Y-%m-%dT%H:%M:%S.000%z-00:00")
		date_to = datetime.datetime.today().replace(hour=23, minute=00).strftime("%Y-%m-%dT%H:%M:%S.000%z-00:00")
		dls = "http://localhost:8000/export/orders/xlsx?hour_from="+ date_from +"&hour_to="+date_to
		resp = requests.get(dls)
		today = datetime.datetime.today().strftime("%Y-%m-%d %H %M %S")
		file_name_path = 'products/files/Ordenes ' + str(today) + '.xlsx'
		output = open(file_name_path, 'wb')
		output.write(resp.content)
		output.close()
		self.send_email_notification(emails=request.data, attachment_path=file_name_path)
		print("--- %s seconds ---" % (time.time() - start_time))
		return Response(data={"email": ['Correo enviado satisfactoriamente']}, status=status.HTTP_200_OK)



class IndicatorsViewSet(APIView):
	permission_classes = (AllowAny,)
	def get(self, request):
		today = datetime.datetime.now().date()
		datetime_7 = (today - timedelta(days=7))
		datetime_28 = (today - timedelta(days=28))
		# today = datetime.date(2019,4,2)
		new_publications = 500
		# data_count = ProductVariation.objects.filter(date_search=today).count()
		data_count = 1234311
		# all_times = ProductVariation.objects.filter(date_search=today).order_by('id')
		# download_time = int((all_times[data_count - 1].created_at -  all_times[0].created_at).total_seconds() / 60)
		download_time = 123
		# days_data = (ProductVariation.objects.latest('date_search').date_search - ProductVariation.objects.earliest('date_search').date_search).days
		days_data = 1231
		# best_publications_all = Product.objects.filter(channel__id=1,).annotate(title=F('name'), url_prefixe=Value('/charts', output_field=CharField()), url_key=Value('mlc', output_field=CharField()), url_value=F('code'), sales_last_7=Sum(Case(When(product_variation__date_search__gt=datetime_7, then=F('product_variation__diference_day_before')), default=0, output_field=IntegerField())), sales_last_28=Sum(Case(When(product_variation__date_search__gt=datetime_28, then=F('product_variation__diference_day_before')), default=0, output_field=IntegerField())), recaudacion_ultimos7=Sum(Case(When(product_variation__date_search__gt=datetime_7, then=F('product_variation__daily_reward')), default=0, output_field=IntegerField())), recaudacion_ultimos28=Sum(Case(When(product_variation__date_search__gt=datetime_28, then=F('product_variation__daily_reward')), default=0, output_field=IntegerField())))

		# best_last_7 = best_publications_all.order_by('-recaudacion_ultimos7').annotate(number=F('recaudacion_ultimos7')).values('title', 'number', 'url_prefixe', 'url_key', 'url_value')[:100]

		# best_last_28 = best_publications_all.order_by('-recaudacion_ultimos28').annotate(number=F('recaudacion_ultimos28')).values('title', 'number', 'url_prefixe', 'url_key', 'url_value')[:100]


		# best_publications = MetricsMercadolibre.objects.annotate(title=F('product__name'), number=F('rank'), url_prefixe=Value('/charts', output_field=CharField()), url_key=Value('mlc', output_field=CharField()), url_value=F('product__code')).values('title', 'number', 'product__code', 'url_prefixe', 'url_key', 'url_value').order_by('-rank')[:10]
		# best_sellers = Seller.objects.annotate(number=Sum('seller_variation__daily_sales'), title=F('nickname'), url_prefixe=Value('/tables', output_field=CharField()), url_key=Value('search', output_field=CharField()), url_value=F('nickname')).order_by('-number').values('title', 'number', 'url_prefixe', 'url_key', 'url_value')[:100]

		data_return = {
			"new_publications":new_publications,
			"data_count":data_count,
			"download_time":download_time,
			"days_data":days_data,
		}
		return Response(data=data_return, status=status.HTTP_200_OK)

class MLCDataViewSet(APIView):
	permission_classes = (AllowAny,)
	def get(self, request):
		now = datetime.datetime.now().date()
		datetime_7 = (now - timedelta(days=7))
		datetime_28 = (now - timedelta(days=28))
		mlc = self.request.query_params.get('MLC', None)
		publication_data = Product.objects.filter(code=mlc).values('name','seller__nickname', 'last_category_description', 'stop_time', 'url_product')
		if publication_data:
			publication_data = publication_data[0]
			publication_comparative = Product.objects.filter(channel__id=1, last_category_description=publication_data['last_category_description']).annotate(max_price=Max('product_variation__price'), min_price=Min('product_variation__price'), sales_last_7=Sum(Case(When(product_variation__date_search__gt=datetime_7, then=F('product_variation__diference_day_before')), default=0, output_field=IntegerField())), sales_last_28=Sum(Case(When(product_variation__date_search__gt=datetime_28, then=F('product_variation__diference_day_before')), default=0, output_field=IntegerField())), recaudacion_ultimos7=Sum(Case(When(product_variation__date_search__gt=datetime_7, then=F('product_variation__daily_reward')), default=0, output_field=IntegerField())), recaudacion_ultimos28=Sum(Case(When(product_variation__date_search__gt=datetime_28, then=F('product_variation__daily_reward')), default=0, output_field=IntegerField()))).order_by('-recaudacion_ultimos28').values('name', 'seller__nickname', 'original_price','max_price', 'min_price', 'metrics_product__rank', 'sales_last_7', 'sales_last_28', 'recaudacion_ultimos7', 'recaudacion_ultimos28', 'code')[:25]
			# print(publication_comparative)
			sold_quantity_data = {
				"data":[{
					"data": ProductVariation.objects.filter(product__code=mlc).annotate(sold_quantity2=Case(When(sold_quantity__isnull=False, then=F('sold_quantity')), When(projected_sale__isnull=False, then=F('projected_sale')), default=Value(0, output_field=IntegerField()), output_field=IntegerField())).order_by('date_search').values_list('sold_quantity2', flat=True),
					"label": "Cantidad Vendida"
				}]
			}
			stock_data = {
				"data":[{
					"data": ProductVariation.objects.filter(product__code=mlc).annotate(stock_data=Case(When(available_quantity__isnull=True, then=Value(0, output_field=IntegerField())), default=F('available_quantity'), output_field=IntegerField())).order_by('date_search').values_list('stock_data', flat=True),
					"label": "Stock"
				}]
			}
			diference_day_before_data = {
				"data":[{
					"data": ProductVariation.objects.filter(product__code=mlc).annotate(sales_data=Case(When(diference_day_before__isnull=True, then=Value(0, output_field=IntegerField())), default=F('diference_day_before'), output_field=IntegerField())).order_by('date_search').values_list('sales_data', flat=True),
					"label": "Ventas Diarias"
				}]
			}
			price_data = {
				"data":[{
					"data": ProductVariation.objects.filter(product__code=mlc).annotate(price_data=Case(When(price__isnull=True, then=Value(0, output_field=IntegerField())), default=F('price'), output_field=IntegerField())).order_by('date_search').values_list('price_data', flat=True),
					"label": "Variación de Precios"
				}]
			}
			seller_data = {
				"data":[{
					"data": SellerVariation.objects.filter(seller__seller_product__code=mlc).annotate(seller_data=Case(When(sales_last_4_month__isnull=True, then=Value(0, output_field=IntegerField())), default=F('sales_last_4_month'), output_field=IntegerField())).order_by('date_search').values_list('sales_last_4_month', flat=True),
					"label": "Variación de Ventas Seller"
				}]
			}
			seller_scale = SellerVariation.objects.filter(seller__seller_product__code=mlc).order_by('date_search').values_list('date_search', flat=True)
			dates_data = ProductVariation.objects.filter(product__code=mlc).order_by('date_search').values_list('date_search', flat=True)
			data_chart = {
				"sold_quantity_data":sold_quantity_data,
				"stock_data":stock_data,
				"diference_day_before_data":diference_day_before_data,
				"price_data":price_data,
				"dates_data":dates_data,
				"publication_data":publication_data,
				"publication_comparative":publication_comparative,
				"seller_data":seller_data,
				"seller_scale":seller_scale,
			}
			return Response(data=data_chart, status=status.HTTP_200_OK)
		else:
			return Response(data={"mlc": ['MLC no se encuentra registrado']}, status=status.HTTP_400_BAD_REQUEST)

class CustomPagination(PageNumberPagination):
	page_size = 25
	page_size_query_param = 'page_size'
	max_page_size = 25
	def get_paginated_response(self, data):
		today_date = datetime.datetime.now()
		return Response({
		   'next': self.get_next_link(),
		   'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'results': data,
			'today_date': today_date
		})
class KeyWordSearchViewSet(ModelViewSet):
	queryset = Product.objects.filter(channel__id=1)
	serializer_class = KeyWordSearchSerializer
	permission_classes = (AllowAny,)
	filter_backends = (search_filter.SearchFilter,)
	http_method_names  = ['get',]
	search_fields  = ('name', 'seller__nickname', 'last_category')
	pagination_class = CustomPagination

	def get_queryset(self):
		now = datetime.datetime.now().date()
		datetime_28 = (now - timedelta(days=28))
		return self.queryset.annotate(recaudacion_last28=Sum(Case(When(product_variation__date_search__gt=datetime_28, then=F('product_variation__daily_reward')), When(product_variation__daily_reward__isnull=True, then=Value(0, output_field=IntegerField())), default=0, output_field=IntegerField()))).filter(recaudacion_last28__isnull=False).order_by('-recaudacion_last28')

class MetricsFileXLSX(APIView):
	permission_classes = (AllowAny,)

	def get(self, request):
		today_date = datetime.datetime.now().date()
		file_name = "Metricas " + str(today_date) +"xlsx"
		file_path = os.path.join(os.path.dirname(os.path.realpath(__name__)), file_name)
		response = HttpResponse(open(file_path, 'rb').read())
		response['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		response['Content-Disposition'] = 'attachment; filename='+file_name
		return response

class CategoryRanking(APIView):
	permission_classes = (AllowAny,)

	def get(self, request):
		some_categories = Product.objects.all().distinct('last_category').values_list('last_category', flat=True)[:5]
		b = []
		now = datetime.datetime.now().date()
		datetime_7 = (now - timedelta(days=7))
		# datetime_28 = (now - timedelta(days=28))
		# print(some_categories)
		for i in some_categories:
			best_publications_categories = Product.objects.filter(last_category=i).annotate(title=F('name'), number=Sum(Case(When(product_variation__date_search__gt=datetime_7, then=F('product_variation__diference_day_before')), default=0, output_field=IntegerField())), number2=Sum(Case(When(product_variation__date_search__gt=datetime_7, then=F('product_variation__daily_reward')), default=0, output_field=IntegerField())), url_prefixe=Value('/charts', output_field=CharField()), url_key=Value('mlc', output_field=CharField()), url_value=F('code')).values('title', 'number', 'url_prefixe', 'url_key', 'url_value','last_category', 'last_category_description', 'number2')
			# print(best_publications)
			if best_publications_categories.count() >=  10:
				b.append({"category": best_publications_categories[0]['last_category_description'], "data_sales":best_publications_categories.order_by('last_category', '-number')[:10], "data_reward":best_publications_categories.order_by('last_category', '-number2')[:10],"mlc_category": best_publications_categories[0]['last_category']})
		return Response(data={"data_categories":b}, status=status.HTTP_200_OK)

class SellerRanking(APIView):
	permission_classes = (AllowAny,)

	def get(self, request):
		now = datetime.datetime.now().date()
		now2 = datetime.date(2019,4,1)
		datetime_7 = (now - timedelta(days=7))
		best_sellers_sales = Seller.objects.annotate(number=Sum('seller_variation__daily_sales'), title=F('nickname'), url_prefixe=Value('/tables', output_field=CharField()), url_key=Value('search', output_field=CharField()), url_value=F('nickname')).order_by('-number').values('number', 'url_prefixe', 'url_key', 'url_value', 'title')[:100]
		best_sellers_reward = Seller.objects.annotate(number=Sum(Case(When(seller_product__product_variation__date_search__gte=datetime_7, then=Case(When(seller_product__product_variation__daily_reward__isnull=False, then=F('seller_product__product_variation__daily_reward')), default=0, output_field=IntegerField())), default=0, output_field=IntegerField())), title=F('nickname'), url_prefixe=Value('/tables', output_field=CharField()), url_key=Value('search', output_field=CharField()), url_value=F('nickname'),).order_by('-number').values('number','url_prefixe', 'url_key', 'url_value', 'title')[:100]

		best_sellers_last_4 = SellerVariation.objects.filter(date_search__gte=now2, sales_last_4_month__isnull=False).annotate(number=F('sales_last_4_month'), title=F('seller__nickname'), url_prefixe=Value('/tables', output_field=CharField()), url_key=Value('search', output_field=CharField()), url_value=F('seller__nickname'),).order_by('-number').values('number','url_prefixe', 'url_key', 'url_value', 'title')[:150]
		return Response(data={
			"best_sales":best_sellers_sales, 
			"best_reward":best_sellers_reward, 
			"best_sellers_last_4":best_sellers_last_4}, status=status.HTTP_200_OK)




			




# class KeyWordSearchViewSet(APIView):
# 	permission_classes = (AllowAny,)
# 	def get(self, request):
# 		now = datetime.datetime.now().date()
# 		datetime_7 = (now - timedelta(days=7))
# 		datetime_28 = (now - timedelta(days=28))
# 		search = self.request.query_params.get('search', None)
# 		if search is not None and search != '':
# 			publication_data = Product.objects.filter(channel__id=1, name__icontains=search).annotate(max_price=Max('product_variation__price'), min_price=Min('product_variation__price'), sales_last_7=Sum(Case(When(product_variation__date_search__gt=datetime_7, then=F('product_variation__diference_day_before')), default=0, output_field=IntegerField())), sales_last_28=Sum(Case(When(product_variation__date_search__gt=datetime_28, then=F('product_variation__diference_day_before')), default=0, output_field=IntegerField())), recaudacion_ultimos7=Sum(Case(When(product_variation__date_search__gt=datetime_7, then=F('product_variation__daily_reward')), default=0, output_field=IntegerField())), recaudacion_ultimos28=Sum(Case(When(product_variation__date_search__gt=datetime_28, then=F('product_variation__daily_reward')), default=0, output_field=IntegerField()))).order_by('-recaudacion_ultimos28').values('name', 'seller__nickname', 'original_price','max_price', 'min_price', 'metrics_product__rank', 'sales_last_7', 'sales_last_28', 'recaudacion_ultimos7', 'recaudacion_ultimos28', 'code', 'last_category_description')[:25]
# 			if publication_data:
# 				return Response(data={"publications": publication_data}, status=status.HTTP_200_OK)
# 			return Response(data={"search": ['No se encontró coincidencia en su busqueda']}, status=status.HTTP_400_BAD_REQUEST)
# 		return Response(data={"publications": []}, status=status.HTTP_200_OK)
