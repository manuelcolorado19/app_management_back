# Generated by Django 2.1.2 on 2019-04-24 13:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('warehouses', '0003_auto_20190424_1232'),
        ('purchases', '0010_merge_20190408_1507'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchase',
            name='warehouse',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='warehouse_purchase', to='warehouses.WareHouse'),
        ),
    ]
