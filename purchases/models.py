from django.db import models
from django.contrib.auth import get_user_model
from sales.models import Product_Sale, Product_Code
from warehouses.models import WareHouse
User = get_user_model()
class Provider(models.Model):

	PROVIDER_TYPE = (
		('importador', 'importador'),
		('vendedor nacional', 'vendedor nacional'),
	)
	rut = models.CharField(null=True, blank=True, max_length=30)
	name = models.CharField(max_length=150, blank=True)
	provider_type = models.CharField(choices=PROVIDER_TYPE, max_length=30, default='vendedor nacional')
	address = models.CharField(max_length=120, blank=True, null=True)
	phone = models.CharField(max_length=15, blank=True, null=True)
	email = models.CharField(max_length=110, blank=True, null=True)

	class Meta:
		db_table = 'provider'

class Purchase(models.Model):

	DOCUMENT_TYPE = (
		('sdt', 'sdt'),
		('boleta', 'boleta'),
		('factura', 'factura'),
	)

	PURCHASE_TYPE = (
		('local', 'local'),
		('importación', 'importación'),
	)
	date_purchase = models.DateField()
	date_created = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='user_purchase')
	number_document = models.CharField(null=True, blank=True, max_length=30)
	provider = models.ForeignKey(Provider, on_delete=models.SET_NULL, null=True, blank=True, related_name="provider_purchase")
	total_purchase = models.DecimalField(decimal_places=1, max_digits=15, default=0)
	document_type = models.CharField(choices=DOCUMENT_TYPE, max_length=20, default='sdt')
	purchase_type = models.CharField(choices=PURCHASE_TYPE, max_length=15, default='local')
	warehouse = models.ForeignKey(WareHouse, on_delete=models.SET_NULL, null=True, blank=True, related_name='warehouse_purchase')

	class Meta:
		db_table = 'purchase'

class Purchase_Product(models.Model):
	purchase = models.ForeignKey(Purchase, on_delete=models.SET_NULL, null=True, blank=True, related_name="purchase_of_product")
	product_purchase = models.ForeignKey(Product_Sale, on_delete=models.SET_NULL, null=True, blank=True, related_name="product_of_purchase")
	product_purchase_code = models.ForeignKey(Product_Code, on_delete=models.SET_NULL, null=True, blank=True, related_name="product_of_purchase_code")
	quantity = models.IntegerField(default=0)
	seller_sku = models.IntegerField(null=True)
	unit_price = models.DecimalField(decimal_places=1, max_digits=15)
	average_price=models.DecimalField(decimal_places=1, max_digits=15, null=True)
	product_purchase_total = models.DecimalField(decimal_places=1, max_digits=15)
	credit_iva = models.DecimalField(decimal_places=2, max_digits=9)
	minimun_price_bill = models.IntegerField(null=True)

	class Meta:
		db_table = 'purchase_product'


# Create your models here.
