import requests
import xlsxwriter
from sales.models import Product_SKU_Ripley, Product_Code, Product_Sale, Pack, Pack_Products, CommissionChannel
from django.db.models.functions.text import Substr, Length
from categories.models import Channel
from django.db.models import F
from openpyxl import load_workbook
from notifications.models import Token
import pandas as pd
import datetime

wb = load_workbook('purchases/comisiones.xlsx')
ws = wb['Hoja1']

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		channel = Channel.objects.filter(name=row[3].value.lower().capitalize())
		if channel:
			channel = channel[0]
			product = Product_Sale.objects.filter(sku=str(row[0].value)[:5])
			if product:
				product = product[0]
				pack = None
			else:
				pack = Pack.objects.filter(sku_pack=str(row[0].value)[:5])
				if pack:
					pack = pack[0]
					product = None
				else:
					print("No existe " + str(row[0].value))
					continue
			if product is not None:
				print("producto")
				comission_channel = CommissionChannel.objects.create(
					product_sale=product,
					pack=None,
					channel=channel,
					percentage_commission=(row[2].value * 100),
					category_sku=row[1].value.lower(),
				)
			if pack is not None:
				comission_channel = CommissionChannel.objects.create(
					product_sale=None,
					pack=pack,
					channel=channel,
					percentage_commission=(row[2].value * 100),
					category_sku=row[1].value.lower(),
				)
				print("pack")
		# print(row[2].value * 100)

# wb =  xlsxwriter.Workbook("Base de Preguntas y respuestas Mercadolibre 26-04-19.xlsx")
# ws = wb.add_worksheet("Preguntas")
# ws.write(0, 0, str("Fecha Pregunta"))
# ws.write(0, 1, str("Hora Pregunta"))
# ws.write(0, 2, str("Publicación"))
# ws.write(0, 3, str("Pregunta"))
# ws.write(0, 4, str("Seller"))
# ws.write(0, 5, str("Nickname"))
# ws.write(0, 6, str("Respondida por:"))
# ws.write(0, 7, str("Respuesta"))
# ws.write(0, 8, str("Fecha Respuesta"))
# ws.write(0, 9, str("Hora Respuesta"))
# ws.write(0, 10, str("Tiempo de Respuesta"))
# ws.write(0, 11, str("Compro"))
# ws.write(0, 12, str("Incidencia"))
# row_num = 0
# url = "https://www.api.asiamerica.cl/question/?is_answer=2&suggested_answer=1&answer_question__is_robot=1"
# data = requests.get(url).json()
# if data['count'] <= 10:
# 	rows = data["results"]
# else:
# 	rows = []
# 	count = 0
# 	iteration_end = int(data['count']/10)
# 	module = int(data['count']%10)
# 	for i in range(1, iteration_end + 1):
# 		print(i)
# 		new_rows = requests.get(url + "&page=" + str(i)).json()['results']
# 		rows = rows + new_rows
# 	# rows = rows + requests.get(url + "&page="+str(((iteration_end) + module))).json()['results']

# for i in rows:
# 	row_num += 1
# 	question = datetime.datetime.strptime(i['date_created'][:16], "%Y-%m-%dT%H:%M")
# 	answer = datetime.datetime.strptime(i['answer_question']['date_answer'][:16], "%Y-%m-%dT%H:%M")
# 	compro = "Si" if i['answer_question']['user_buy'] else "No"
# 	ws.write(row_num, 0, str(question.date()))
# 	ws.write(row_num, 1, str(question.time()))
# 	ws.write(row_num, 2, str(i['publication_data']['title']))
# 	ws.write(row_num, 3, str(i['text']))
# 	ws.write(row_num, 4, str(i['publication_data']['seller_nickname']))
# 	ws.write(row_num, 5, str(i['user_nickname_ml']))
# 	ws.write(row_num, 6, str(i['answer_question']['user_answer']))
# 	ws.write(row_num, 7, str(i['answer_question']['answer']))
# 	ws.write(row_num, 8, str(answer.date()))
# 	ws.write(row_num, 9, str(answer.time()))
# 	ws.write(row_num, 10, str(i['answer_question']['diference_question_answer']))
# 	ws.write(row_num, 11, str(compro))
# 	ws.write(row_num, 12, str(i['incidence']))
# wb.close()