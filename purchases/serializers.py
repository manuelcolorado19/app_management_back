from rest_framework import serializers
from .models import Provider, Purchase, Purchase_Product
from django.db.models import F, Sum
from warehouses.models import WareHouse

class PurchasesWareHouse(serializers.IntegerField):
	def to_representation(self, value):
		purchases = Purchase_Product.objects.filter(purchase__warehouse=value.warehouse, product_purchase=value.product_sale, purchase__date_purchase__gte=value.date_count).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			return purchases['count_product']
		return 0

class PurchasesWareHouseCode(serializers.IntegerField):
	def to_representation(self, value):
		purchases = Purchase_Product.objects.filter(product_purchase_code=value.product_code, purchase__warehouse=value.stock_warehouse.warehouse).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			return purchases['count_product']
		return 0
class ProviderNameField(serializers.CharField):
	def to_representation(self, value):
		if value.provider is not None:
			return value.provider.name
		return '-'

class ProductsPurchase(serializers.ListField):
	def to_representation(self, value):
		
		# print(Purchase_Product.objects.filter(purchase=value).annotate(sku=F('product_purchase__sku'), code_seven_digits=F('product_purchase_code__product__description'), description=F('product_purchase__description'), model=F('product_purchase_code__color__description')).values(
		# 	'credit_iva',
		# 	'minimun_price_bill',
		# 	'product_purchase_total',
		# 	'quantity',
		# 	'seller_sku',
		# 	'unit_price',
		# 	'sku',
		# 	'code_seven_digits',
		# 	'description',
		# 	'model',
		# ).distinct())
		return Purchase_Product.objects.filter(purchase=value).annotate(sku=F('product_purchase__sku'), code_seven_digits=F('product_purchase_code__code_seven_digits'), description=F('product_purchase__description'), model=F('product_purchase_code__color__description')).values(
			'credit_iva',
			'minimun_price_bill',
			'product_purchase_total',
			'quantity',
			'seller_sku',
			'unit_price',
			'sku',
			'code_seven_digits',
			'description',
			'model',
		).order_by('sku')
class ProductQuantityField(serializers.IntegerField):
	def to_representation(self, value):
		return Purchase_Product.objects.filter(purchase=value).aggregate(product_quantity=Sum('quantity'))['product_quantity']

class PurchaseSerializer(serializers.ModelSerializer):
	product_quantity = ProductQuantityField(read_only=True, source='*')
	provider_name = ProviderNameField(read_only=True, source='*')
	products = ProductsPurchase(read_only=True, source='*')
	class Meta:
		model = Purchase
		fields = (
			'id',
			'date_purchase',
			'document_type',
			'number_document',
			'purchase_type',
			'total_purchase',
			'product_quantity',
			'provider_name',
			'products'
		)