

# from django.shortcuts import render
# from sales.serializers import Product_SaleSerializer, SaleSerializer, Sale_ProductSerializer, Product_SaleStockSerializer, Product_ChannelSerializer, RetailSalesSerializer, PackSerializer, Category_Product_SaleSerializer, SkuNewSerializer, Product_SaleSKUSerializer, SkuUpdateSerializer, Sales_ReportSerializer, Product_Code_CreateSerializer, Product_SKU_RipleySerializer, Product_SaleSerializer2, Product_SaleSerializer2
# from rest_framework.viewsets import ModelViewSet
# from sales.models import Product_Sale, Sale, Sale_Product, Product_Channel, Product_Code, Pack, Pack_Products, Category_Product_Sale, Product_SKU_Ripley, Sales_Report, Color
# from categories.models import Channel
# from rest_framework.permissions import AllowAny, IsAuthenticated
# from django_filters.rest_framework import DjangoFilterBackend
# from django_filters import rest_framework as filters
# import xlwt
# import xlsxwriter
# from rest_framework.views import APIView
# from django.contrib.postgres.aggregates.general import ArrayAgg
# import time
# from datetime import date, timedelta
# import io
# from django.http import HttpResponse
# from django.db.models import F, Case, When, CharField, Value, IntegerField, DurationField, Q, Sum, Value, Count
# from django.db.models.functions import Cast, Concat, Length
# from django.db.models.fields import DateField, FloatField
# from rest_framework.pagination import PageNumberPagination
# import datetime
# from rest_framework.response import Response
# from rest_framework import filters as search_filter
# from sales.permissions import SalesPermissions, Product_SalePermissions
# from rest_framework import status
# import os, base64
# from django.db import transaction
# import itertools
# from operator import itemgetter
# from notifications.models import Token, CustomMessageProduct
# import requests
# import json
# from OperationManagement.settings import API_KEY_OF, OPEN_FACTURA_URL, DATA_EMITTER, API_KEY_RP, API_KEY_LINIO, USER_ID, DATA_EMITTER_SHIPPING_GUIDE, DATA_RECEIVER, DATA_EMITTER_BILL, DATA_RECEIVER_BILL
# from wsgiref.util import FileWrapper
# from sales.filters import SalesFilter, SalesFilterByDate
import urllib
from hashlib import sha256
from io import BytesIO
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle, Spacer, Indenter
from reportlab.graphics.shapes import Line, Drawing
from reportlab.lib.units import inch, mm
from django.http import HttpResponse
from reportlab.pdfgen import canvas
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER, TA_RIGHT
from reportlab.platypus import ListFlowable, ListItem
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table
from hmac import HMAC
from django.contrib.auth import get_user_model
from PyPDF2 import PdfFileReader, PdfFileMerger
import PyPDF2
import urllib.request
import math
import pandas as pd
from django.db.models.functions import TruncMonth, TruncYear, TruncDay, TruncWeek, ExtractWeek, ExtractYear, ExtractMonth, ExtractWeekDay
import dateutil.relativedelta
from prices.models import Price_Product
from django.core.mail import send_mail

# sales_approved = [9710,9716,9721,9726,9732,9758,9747,9749,9750,9751]
# buff = BytesIO()
# today = datetime.datetime.today().strftime("%Y-%m-%d")


# doc = SimpleDocTemplate(buff, pagesize=letter, rightMargin=40, leftMargin=40, topMargin=60, bottomMargin=18,)
# clientes = []
# styles = getSampleStyleSheet()
# products_count_manifiesto = Sale.objects.filter(id__in=sales_approved).aggregate(product_quantity=Sum(Case(When(sale_of_product__product__sku=1, then=Value(0, output_field=IntegerField())), default=F('sale_of_product__quantity'), output_field=IntegerField())))
# header = Paragraph("Listado Productos a Buscar en Bodega (" + str(products_count_manifiesto['product_quantity']) +")", styles['Heading4'])
# style_products = getSampleStyleSheet()
# style_products2 = getSampleStyleSheet()
# styleDescription2 = style_products2["BodyText"]
# styleDescription2.fontSize = 10
# styleDescription2.leading = 10
# normal = style_products['Heading4']
# normal.alignment = TA_RIGHT
# normal.bottomMargin = 5
# products_receive = Paragraph("Productos Recibidos: _______________", normal)
# header_data = [
#     [header, products_receive],		    
# ]
# table_header = Table(header_data)
# last_manifiesto = Sales_Report.objects.latest('date_created')
# today = datetime.datetime.today().strftime("%Y-%m-%d %H:%M")
# number_manifiesto = Paragraph("Manifiesto #" + str(last_manifiesto.id + 1), styles['Heading2'])
# date_generate = Paragraph("Fecha: " +str(today), styles['Heading2'])
# clientes.append(number_manifiesto)
# clientes.append(date_generate)
# clientes.append(table_header)
# # clientes.append(products_receive)
# headings = ('SKU', 'Producto','Modelo', 'Cant.',"             ")
# a = Sale_Product.objects.filter(sale__id__in=sales_approved).exclude(product__sku=1).annotate(model=Case(When(product_code__isnull=False, then=F('product_code__color__description')), default=Value('por definir'), output_field=CharField())).values('product__description', 'quantity', 'product_code__color__description', 'product__sku', 'model')
# sorted_order = sorted(a, key=itemgetter('product__sku', 'product_code__color__description'))
# group_order = []
# for key, group in itertools.groupby(sorted_order, key=lambda x:(x['product__sku'], x['product_code__color__description']),):
#     group_order.append(list(group))
# data = []
# for row in group_order:
# 	quantity = 0
# 	for row2 in row:
# 		quantity += row2['quantity'] 
# 	data.append((row[0]['product__sku'], Paragraph(row[0]['product__description'], styleDescription2), row[0]['model'], quantity,"        "))
# allclientes = data
# t = Table([headings] + allclientes, colWidths=["15%","47%","19%","6%","13%"])
# t.setStyle(TableStyle(
# 	[
# 		('GRID', (0, 0), (4, -1), 1, colors.dodgerblue),
# 		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
# 		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
# 	]
# ))
# clientes.append(t)
# styleDescription = style_products["BodyText"]
# styleDescription.fontSize = 5
# styleDescription.leading = 5
# ventas = Sale.objects.filter(id__in=sales_approved).annotate(quantity=Sum(Case(When(sale_of_product__product__sku=1, then=Value(0, output_field=IntegerField())), default=F('sale_of_product__quantity'), output_field=IntegerField())), sub_status=F('shipping_type_sub_status'), description=F('comments'))
# header = Paragraph("Listado Ordenes de Compras (" + str(ventas.count()) + ")", styles['Heading4'])
# clientes.append(header)
# index = 0
# headings = ("N°","Canal",'OC', 'Productos', 'N° de Seguimiento', "Descripción", "             ", "             ")
# allclientes = []
# for p in ventas:
# 	index += 1
# 	allclientes.append((index,p.channel.name, p.order_channel, p.quantity, p.tracking_number, Paragraph(p.description, styleDescription), "             ", "             "))
# t = Table([headings] + allclientes, colWidths=["4%","15%","15%","10%","20%","16%","10%","10%"])
# t.setStyle(TableStyle(
# 	[
# 		('GRID', (0, 0), (7, -1), 1, colors.dodgerblue),
# 		('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
# 		('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue),
# 	]
# ))
# clientes.append(t)
# d = Drawing(300, 100)
# d.add(Line(15, 0, 110, 0))
# e = Drawing(600, 0)
# e.add(Line(145, 0, 245, 0))
# f = Drawing(600, 0)
# f.add(Line(280, 0, 375, 0))
# g = Drawing(600, 0)
# g.add(Line(410, 0, 505, 0))
# clientes.append(d)
# clientes.append(e)
# clientes.append(f)
# clientes.append(g)
# pageTextStyleCenter = ParagraphStyle(name="left", alignment=TA_CENTER, fontSize=13, leading=10)
# tbl_data = [
#     [Paragraph("Bodega", pageTextStyleCenter), Paragraph("Supervisor", pageTextStyleCenter), Paragraph("Empaque", pageTextStyleCenter), Paragraph("Despacho", pageTextStyleCenter)],
    
# ]
# tbl = Table(tbl_data)
# clientes.append(tbl)

# doc.build(clientes)
# files_dir = os.getcwd()
# file_manifiesto = "Manifiesto " + str(today)
# report_encoded = base64.b64encode(buff.getvalue())
# print(report_encoded)
# buff.close()

''' Archivo de Inventario'''

# import requests
# import xlsxwriter
# from sales.models import Product_SKU_Ripley, Product_Code, Product_Sale, Pack, Pack_Products
# from django.db.models.functions.text import Substr, Length
# from django.db.models import F
# from openpyxl import load_workbook
# from notifications.models import Token
# import pandas as pd
# import datetime

# wb =  xlsxwriter.Workbook("Inventario Variantes.xlsx")
# ws = wb.add_worksheet("Stock Variantes")
# ws.write(0, 0, str("SKU 5"))
# ws.write(0, 1, str("Descripción"))
# ws.write(0, 2, str("Inv Inicial"))
# ws.write(0, 3, str("Vendido"))
# ws.write(0, 4, str("Comprado"))
# ws.write(0, 5, str("Reacondicionado"))
# ws.write(0, 6, str("Quedan"))
# ws.write(0, 7, str("Variante"))
# ws.write(0, 8, str("Código 7"))
# row_num = 0
# url = "https://www.api.asiamerica.cl/product_sale_stock/?page=1&search="
# rows = requests.get(url).json()['results']

# for i in rows:
# 	row_num += 1
# 	ws.write(row_num, 0, str(i['sku']))
# 	ws.write(row_num, 1, str(i['description']))
# 	ws.write(row_num, 2, str(i['initial_stock']))
# 	ws.write(row_num, 3, str(i['sale_quantity']))
# 	ws.write(row_num, 4, str(i['purchase_quantity']))
# 	ws.write(row_num, 5, str(i['returns_reaconditionated']))
# 	ws.write(row_num, 6, str(i['initial_stock'] - i['sale_quantity'] + i['purchase_quantity'] + i['returns_reaconditionated']))
# 	ws.write(row_num, 7, str("-"))
# 	ws.write(row_num, 8, str("-"))
# 	for each in i['product_code']:
# 		if int(each['code_seven_digits'][-2:]) > 49:
# 			row_num += 1
# 			ws.write(row_num, 0, str(i['sku']))
# 			ws.write(row_num, 1, str(i['description']))
# 			ws.write(row_num, 2, str(each['initial_stock']))
# 			ws.write(row_num, 3, str(each['sale_quantity']))
# 			ws.write(row_num, 4, str(each['purchase_quantity']))
# 			ws.write(row_num, 5, str(each['returns_reaconditionated']))
# 			ws.write(row_num, 6, str(each['initial_stock'] - each['sale_quantity'] + each['purchase_quantity'] + each['returns_reaconditionated']))
# 			ws.write(row_num, 7, str(each['color_description']))
# 			ws.write(row_num, 8, str(each['code_seven_digits']))
# 		else:
# 			continue
# wb.close()
'''----------------------------------------'''




import requests
import xlsxwriter
from sales.models import Product_SKU_Ripley, Product_Code, Product_Sale, Pack, Pack_Products
from prices.models import Price_Product
from django.db.models.functions.text import Substr, Length
from django.db.models import F
from openpyxl import load_workbook
from notifications.models import Token
import pandas as pd
import datetime, time
import urllib
from hashlib import sha256
from hmac import HMAC
from OperationManagement.settings import API_KEY_LINIO, USER_ID, API_KEY_RP
from sales.models import Sale_Product, Sale
from django.db.models import Sum, Q
from OperationManagement.settings import USER_ID, API_KEY_LINIO
from hashlib import sha256
from django.db.models import F, Case, When, CharField, Value, IntegerField, DurationField, Q, Sum, Value, Count
from django.db.models.functions import Cast, Concat
from django.db.models.fields import DateField, FloatField
from hmac import HMAC
from django.db.models.functions.text import Substr
import math
from django.core.mail import send_mail
import urllib
from returns.models import Return, Return_Product
API_KEY_OF = "a3f8ebf39b63448f9eb7090c796adaed"
url_open_factura = "https://api.haulmer.com/v2/dte/document/76431547-2/"
headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
wb = xlsxwriter.Workbook("Base de Notas de crédito.xlsx")
ws = wb.add_worksheet("Notas de Credito")
cell_format_data1 = wb.add_format({'bg_color': "yellow", "font_size": 8})
cell_format_data2 = wb.add_format({"font_size": 8})
cell_format_data3 = wb.add_format({"font_size": 8, 'num_format': '#,###'})
# columns = [
# 			{"name": 'Fecha de Emsisión', "width": 11},
# 			{"name": 'Folio', "width": 25},
# 			{"name": 'Tipo de Documento', "width": 9},
# 			{"name": 'Indicador de Servicio', "width": 9},
# 			{"name": 'Dirección de Origen', "width": 9},
# 			{"name": 'Rut Emisor', "width": 7},
# 			{"name": 'Comuna Emisor', "width": 7},
# 			{"name": 'Giro Emisor', "width": 13},
# 			{"name": 'Razon social', "width": 13},
# 			{"name": 'Rut Receptor', "width": 13},
# 			{"name": 'Razon social Receptor', "width": 13},
# 			{"name": 'SKU', "width": 25},
# 			{"name": 'Nombre Producto', "width": 16},
# 			{"name": 'Precio Producto', "width": 16},
# 			{"name": 'Cantidad Producto', "width": 16},
# 			{"name": 'Monto Producto', "width": 16},
# 			{"name": 'N° Producto', "width": 16},
# 			{"name": 'Detalle Producto', "width": 16},
# 			{"name": 'Monto Total', "width": 16},
# 			{"name": 'Valor a Pagar', "width": 16},
# 			{"name": 'Total Periodo', "width": 16},
# 		]

columns = [
			{"name": 'Folio', "width": 25},
			{"name": 'Fecha de Emsisión', "width": 11},
			{"name": 'Tipo de Documento', "width": 9},
			# {"name": 'Indicador de Traslado', "width": 9},
			# {"name": 'Tipo de Despacho', "width": 9},
			# {"name": 'Tipo de transporte venta', "width": 9},
			# {"name": 'Indicador de Servicio', "width": 9},
			{"name": 'Razon social emisor', "width": 13},
			{"name": 'Giro Emisor', "width": 13},
			{"name": 'Dirección de Origen', "width": 9},
			{"name": 'Rut Emisor', "width": 7},
			{"name": 'Comuna Emisor', "width": 7},
			{"name": 'Código Sucursal', "width": 7},
			{"name": 'Ciudad Origen', "width": 7},
			# {"name": 'Actividad económica', "width": 13},
			# {"name": 'Teléfono emisor', "width": 13},
			# {"name": 'Correo Emisor', "width": 13},

			# {"name": 'Contacto', "width": 13},
			{"name": 'Dirección Receptor', "width": 13},
			{"name": 'Rut Receptor', "width": 13},
			{"name": 'Comuna Receptor', "width": 13},
			{"name": 'Giro Receptor', "width": 13},
			{"name": 'Ciudad Receptor', "width": 13},
			{"name": 'Razon social Receptor', "width": 13},


			# {"name": 'Dirección destino', "width": 25},
			# {"name": 'Comuna destino', "width": 16},
			# {"name": 'Ciudad destino', "width": 16},


			# {"name": 'SKU', "width": 16},
			# {"name": 'Descripcion Producto', "width": 16},
			{"name": 'Nombre producto', "width": 16},
			{"name": 'Precio producto', "width": 16},
			{"name": 'Cantidad producto', "width": 16},
			{"name": 'Monto producto', "width": 16},
			{"name": 'N° Lista', "width": 16},


			{"name": 'Iva', "width": 16},
			{"name": 'Monto Neto', "width": 16},
			{"name": 'Tasa Iva', "width": 16},
			{"name": 'Monto total', "width": 16},
			# {"name": 'Valor a pagar', "width": 16},
			# {"name": 'Monto Periodo', "width": 16},

			{"name": 'Código Referencia', "width": 16},
			{"name": 'Fecha Referencia', "width": 16},
			{"name": 'Folio Referencia', "width": 16},
			{"name": 'Razon Referencia', "width": 16},
			{"name": 'Numero Lin Referencia', "width": 16},
			{"name": 'Tipo de documento Referencia', "width": 16},
		]
for col_num in range(len(columns)):
	ws.write(0, col_num, str(columns[col_num]['name']), cell_format_data1)
	ws.set_column(0, col_num, columns[col_num]['width'])
row_num = 1
count = 0
for row in range(3, 652):
	url_requests = url_open_factura + "61/"+ str(row) + "/json"
	data = requests.get(url_requests, headers=headers).json()
	if data.get('json', None) is not None:
		print(row)
		for each in data['json']['Detalle']:
			ws.write(row_num, 0, data['json']['Encabezado']['IdDoc'].get('Folio', "-"), cell_format_data2)
			ws.write(row_num, 1, data['json']['Encabezado']['IdDoc'].get('FchEmis', "-"), cell_format_data2)
			ws.write(row_num, 2, data['json']['Encabezado']['IdDoc'].get('TipoDTE', "-"), cell_format_data2)
			# ws.write(row_num, 3, data['json']['Encabezado']['IdDoc'].get('IndTraslado', "-"), cell_format_data2)
			# ws.write(row_num, 4, data['json']['Encabezado']['IdDoc'].get('TipoDespacho', "-"), cell_format_data2)
			# ws.write(row_num, 5, data['json']['Encabezado']['IdDoc'].get('TpoTranVenta', "-"), cell_format_data2)
			# ws.write(row_num, 3, data['json']['Encabezado']['IdDoc'].get('IndServicio', "-"), cell_format_data2)
			# ws.write(row_num, 3, data['json']['Encabezado']['Emisor'].get('Acteco', "-"), cell_format_data2)
			ws.write(row_num, 3, data['json']['Encabezado']['Emisor'].get('RznSoc', "-"), cell_format_data2)
			ws.write(row_num, 4, data['json']['Encabezado']['Emisor'].get('GiroEmis', "-"), cell_format_data2)
			ws.write(row_num, 5, data['json']['Encabezado']['Emisor'].get('DirOrigen', "-"), cell_format_data2)
			ws.write(row_num, 6, data['json']['Encabezado']['Emisor'].get('RUTEmisor', "-"), cell_format_data2)
			ws.write(row_num, 7, data['json']['Encabezado']['Emisor'].get('CmnaOrigen', "-"), cell_format_data2)
			ws.write(row_num, 8, data['json']['Encabezado']['Emisor'].get('CdgSIISucur', "-"), cell_format_data2)
			ws.write(row_num, 9, data['json']['Encabezado']['Emisor'].get('CiudadOrigen', "-"), cell_format_data2)

			# ws.write(row_num, 11, data['json']['Encabezado']['Receptor'].get('Contacto', "-"), cell_format_data2)
			ws.write(row_num, 10, data['json']['Encabezado']['Receptor'].get('DirRecep', "-"), cell_format_data2)
			ws.write(row_num, 11, data['json']['Encabezado']['Receptor'].get('RUTRecep', "-"), cell_format_data2)
			ws.write(row_num, 12, data['json']['Encabezado']['Receptor'].get('CmnaRecep', "-"), cell_format_data2)
			ws.write(row_num, 13, data['json']['Encabezado']['Receptor'].get('GiroRecep', "-"), cell_format_data2)
			ws.write(row_num, 14, data['json']['Encabezado']['Receptor'].get('CiudadRecep', "-"), cell_format_data2)
			ws.write(row_num, 15, data['json']['Encabezado']['Receptor'].get('RznSocRecep', "-"), cell_format_data2)
			# if each.get('CdgItem', None) is not None:
			# 	ws.write(row_num, 17,each['CdgItem']['VlrCodigo'], cell_format_data2)
			# else:
			# 	ws.write(row_num, 17,"-", cell_format_data2)
			# ws.write(row_num, 18, each.get('DscItem', "-"), cell_format_data2)
			ws.write(row_num, 16, each['NmbItem'], cell_format_data3)
			ws.write(row_num, 17, each['PrcItem'], cell_format_data3)
			ws.write(row_num, 18, each['QtyItem'], cell_format_data3)
			ws.write(row_num, 19, each['MontoItem'], cell_format_data2)
			ws.write(row_num, 20, each.get('NroLinDet', "-"), cell_format_data2)


			ws.write(row_num, 21, data['json']['Encabezado']['Totales'].get('IVA', "-"), cell_format_data3)
			ws.write(row_num, 22, data['json']['Encabezado']['Totales'].get('MntNeto', "-"), cell_format_data3)
			ws.write(row_num, 23, data['json']['Encabezado']['Totales'].get('TasaIVA', "-"), cell_format_data3)
			ws.write(row_num, 24, data['json']['Encabezado']['Totales'].get('MntTotal', "-"), cell_format_data3)
			# ws.write(row_num, 28, data['json']['Encabezado']['Totales'].get('VlrPagar', "-"), cell_format_data3)
			# ws.write(row_num, 29, data['json']['Encabezado']['Totales'].get('MontoPeriodo', "-"), cell_format_data3)

			ws.write(row_num, 25, data['json']['Referencia'][0].get('CodRef', "-"), cell_format_data2)
			ws.write(row_num, 26, data['json']['Referencia'][0].get('FchRef', "-"), cell_format_data2)
			ws.write(row_num, 27, data['json']['Referencia'][0].get('FolioRef', "-"), cell_format_data2)
			ws.write(row_num, 28, data['json']['Referencia'][0].get('RazonRef', "-"), cell_format_data2)
			ws.write(row_num, 29, data['json']['Referencia'][0].get('NroLinRef', "-"), cell_format_data2)
			ws.write(row_num, 30, data['json']['Referencia'][0].get('TpoDocRef', "-"), cell_format_data2)

			row_num += 1
	# count += 1
	if row % 100 == 0:
		time.sleep(61)
wb.close()
















# returns_ = Return.objects.filter(sale__isnull=False)
# for i in returns_:
# 	a = []
# 	sales_products = i.sale.sale_of_product.exclude(product__sku=1).values_list('product_id', flat=True)
# 	products_return = i.product_return.all().values_list('product_sale_return_id', flat=True)
# 	for e in products_return:
# 		if e in sales_products:
# 			a.append(e)
# 	if len(sales_products) == len(a):
# 		id_sale_used = []
# 		id_return_used = []
# 		# repeated = i.sale.sale_of_product.all().distinct('product_id')
# 		all_products = i.product_return.all()
# 		# if repeated.count() != all_products.count():
# 		# 	continue
# 		for r in i.sale.sale_of_product.exclude(product__sku=1):
# 			for t in all_products:
# 				if r.product_id == t.product_sale_return_id and r.id not in id_sale_used and t.id not in id_return_used:
# 					print(str(r.product.sku) + " " + str(t.product_sale_return.sku))
# 					product_return = Return_Product.objects.filter(id=t.id).update(sale_product_asociation_id=r.id)
# 					id_sale_used.append(r.id)
# 					id_return_used.append(t.id)


# 		print("Devolucion Bien " + str(i.id))
# 	else:
# 		print("Revisar " +str(i.id))




# url1 = "https://sellercenter-api.linio.cl?Action=GetOrders&CreatedAfter=2018-11-29T00%3A00%3A00&Format=JSON&Timestamp=2019-05-29T19%3A36%3A02-04%3A00&UserID=manuel.ureta%40asiamerica.cl&Version=1.0&Signature=70183e3471e68fa28c34dd9cc613facf15099ed3aaf04cc58d912df010d12f01"

# # header = {'Accept': 'application/json', 'Authorization': API_KEY_RP}
# request1 = requests.get(url1).json()['SuccessResponse']['Body']['Orders']['Order']
# rows = request1
# # print(len(rows))
# url_linio = "https://sellercenter-api.linio.cl?"
# each_order = []
# for row in rows:
# 	print("hola")
# 	parameters = {
# 		'UserID': USER_ID,
# 		'Version': '1.0',
# 		'Action': 'GetOrderItems',
# 		'OrderId': row['OrderId'],
# 		'Format':'JSON',
# 		'Timestamp': datetime.datetime.now().isoformat(),
# 	}
# 	concatenated = urllib.parse.urlencode(sorted(parameters.items()))
# 	parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
# 	concatenated = urllib.parse.urlencode(sorted(parameters.items()))

# 	url_request = url_linio + concatenated
# 	# url_items = "https://sellercenter-api.linio.cl?Action=GetOrderItems&Format=JSON&OrderId={}&Timestamp=2019-05-29T19%3A37%3A28-04%3A00&UserID=manuel.ureta%40asiamerica.cl&Version=1.0&Signature=d998b96e38765abde45f1c0ec7687fc7f140dbc45a9126bb67be0589bc7553da"
# 	order_items = requests.get(url_request).json()["SuccessResponse"]["Body"]["OrderItems"]["OrderItem"]
# 	# print(order_items)
# 	if type(order_items) != list:
# 		order_items = [order_items]
# 	for i in order_items:
# 		if i['ShopSku'] == "GE657SP1GKLFMLACL-4950972":
# 			print("encontrada")
# 			each_order.append(row['OrderNumber'])

#### Algoritmo para etiquetas de starken


# pdf_file = open('etiquetas_ripley.pdf', 'rb')
# read_pdf = PyPDF2.PdfFileReader(pdf_file)
# number_of_pages = read_pdf.getPage(0)
# # print(number_of_pages)
# # orders = []
# # for j in range(number_of_pages):
# page = read_pdf.getPage(2)
# page_content = page.extractText()
# text_pdf = page_content.encode('utf-8').decode('utf-8')
# print(text_pdf[text_pdf.find("STGO.INTERNET")+14:text_pdf.find("STGO.INTERNET")+22].split("-")[0])

#### Algoritmo etiquetas de starken


	# count_sales = text_pdf.count('PEDIDO:')
	# if count_sales == 1:
	# 	order_channel = text_pdf[text_pdf.find("PEDIDO:")+7:text_pdf.find("PEDIDO:")+16].split("-")[0]
	# 	orders.append(order_channel)
	# page = read_pdf.getPage(j)
	# page_content = page.extractText()
	# text_pdf = page_content.encode('utf-8').decode('utf-8')
	# order_channel = text_pdf[text_pdf.find("REFERENCIA: ")+12:text_pdf.find("REFERENCIA: ")+22].split("-")[0]

# print(set(orders))
# count_sales = text_pdf.count('Venta:')
# for i in range(count_sales):
# 	a = text_pdf[text_pdf.find("Venta:")+6:text_pdf.find("Venta")+16].split("-")[0]
# 	print(a)
# 	text_pdf = text_pdf.replace("Venta:" + a, "")

# b = new_text[new_text.find("Venta:")+6:new_text.find("Venta")+16].split("-")[0]
# print(b)
# sales = Sale_Product.objects.filter(sale__order_channel__in=ventas_linio, sale__channel_id=5, product__sku=10023)

	# sku_ripley = row['ShopSku']
	# base_sku = Product_SKU_Ripley.objects.filter(sku_ripley=row['ShopSku'])
	# if base_sku:
	# 	# print(row['ShopSku'] + " existe")
	# 	pass
	# else:
	# 	if "pack" in row['Name'].lower() and row['ShopSku'] != "" and row['Status'] == 'active':
	# 		# pass
	# 		print("data api: " + row['SellerSku'] + " " + row['ShopSku'] + " " + row['Name'])
			# pack_data = Pack.objects.filter(sku_pack=row['SellerSku'])
			# if pack_data:
			# 	if pack_data[0].pack_product.all().count() == 1:
			# 		word_list = row['Name'].split()
			# 		data_products = pack_data[0].pack_product.all()
			# 		product_code = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6, 6)).filter(color__description=word_list[-1].upper(), code_seven__gte=50, product=data_products[0].product_pack)
					# if product_code:
					# 	print("data base: " + str(pack_data[0].sku_pack) + " " + pack_data[0].description + " código 7 " + str(product_code[0].code_seven_digits))


		# 			data_products = pack_data[0].pack_product.all()
		# 			color_filter = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6, 6)).filter(product=data_products[0].product_pack, code_seven__gte=50)
		# 			if color_filter.count() == 1:
		# 				variation = color_filter[0].color.description
		# 				pack_data2 = Pack.objects.filter(sku_pack=str(row['ShopSku']))
		# 				if pack_data2:
		# 					print("sku ripley usado")
		# 				else:
		# 					pack = Pack.objects.create(
		# 						sku_pack=row['ShopSku'],
		# 						description=row['Name'],
		# 						channel_id=5,
		# 					)
		# 					sku_ripley = Product_SKU_Ripley.objects.create(
		# 						description=row['Name'],
		# 						sku_ripley=str(row['ShopSku']),
		# 						pack_ripley=pack,
		# 						color=color_filter[0].color
		# 					)
		# 					pack_product = Pack_Products.objects.create(
		# 						pack=pack,
		# 						product_pack=data_products[0].product_pack,
		# 						quantity=data_products[0].quantity,
		# 						percentage_price=data_products[0].percentage_price,
		# 						color=color_filter[0].color,
		# 					)
		# 					print(row['ShopSku'] + " creado")
		# 			else:
		# 				variation = " Muchos colores"
		# 	print(row['ShopSku'] + " " + row['Name']) 
		# else:
		# 	print(row['SellerSku'] + " " + row['ShopSku'] + " " + row['Name'])
		# 	pass
		# 	if row['SellerSku'].startswith("5"):
		# 		pass
		# 		print(row['shop_sku'] + " " + row['product_title'])
		# 	else:
		# 		print("data base: " + product_code[0].code_seven_digits + " " + product_code[0].product.description + " variante " + product_code[0].color.description)
		# 			product_code = Product_Code.objects.filter(code_seven_digits=row['SellerSku'])
		# 		if product_code:
		# 			pack_data = Pack.objects.filter(sku_pack=str(row['ShopSku']))
		# 			if pack_data:
		# 				print("sku ripley usado")
		# 			else:
		# 				pack = Pack.objects.create(
		# 					sku_pack=row['ShopSku'],
		# 					description=row['Name'],
		# 					channel_id=5,
		# 				)
		# 				sku_ripley = Product_SKU_Ripley.objects.create(
		# 					description=row['Name'],
		# 					sku_ripley=str(row['ShopSku']),
		# 					pack_ripley=pack,
		# 					color=product_code[0].color
		# 				)
		# 				pack_product = Pack_Products.objects.create(
		# 					pack=pack,
		# 					product_pack=product_code[0].product,
		# 					quantity=1,
		# 					percentage_price=100,
		# 					color=product_code[0].color,
		# 				)
		# 				print(row['ShopSku'] + " creado")


################################## Descarga de Archivo de Ml publicaciones ####################################
# from sales.models import SubCategoriesChannel, Category_Product_Sale, SKUCategoryChannel
from categories.models import Channel
import requests
from notifications.models import Token
# wb = xlsxwriter.Workbook("Publicaciones de Mercadolibre 9999.xlsx")
# ws = wb.add_worksheet("Hoja1")
# token = Token.objects.get(id=1).token_ml
# url_mshop = "https://api.mercadolibre.com/orders/search?seller=216244489&access_token="+token+"&order.date_created.from=2019-03-01T16:00:00.000-00:00&order.date_created.to=2019-07-04T23:00:00.000-00:00&sort=date_asc"
# data = requests.get(url_mshop).json()
# if data['paging']['total'] <= 50:
# 	rows = data["results"]
# else:
# 	rows = []
# 	count = 0
# 	iteration_end = int(data['paging']['total']/50)
# 	module = int(data['paging']['total']%50)
# 	for i in range(iteration_end + 1):
# 		new_rows = requests.get(url_mshop + "&offset=" + str(i*50)).json()['results']
# 		rows = rows + new_rows
# 	rows = rows + requests.get(url_mshop + "&offset="+str(((iteration_end*50) + module))).json()['results']
# print(len(rows))
# for row in rows:
# 	if row['shipping']['id'] == 27996216459:
# 		print(row['id'])
# sale = Sale.objects.filter(id=5722)
# for i in sale[0].sale_of_product.exclude(product__sku=1):
# 	if i.product_code is not None:
# 		print(str(i.id) + " " + str(i.product.sku) + " " + str(i.product_code.code_seven_digits) + " " + str(i.quantity))
# 	else:
# 		print(str(i.id) + " " + str(i.product.sku) + " " + str(i.quantity))

# print(len(rows))
# row_num = 0
# for row in rows:
# 	print("hola")
# 	data = requests.get("https://api.mercadolibre.com/items/" + str(row['id']) + "?access_token="+token).json()
# 	if data['variations']:
# 		for i in data['variations']:
# 			ws.write(row_num, 0, row['id'])
# 			ws.write(row_num, 1, row['title'])
# 			ws.write(row_num, 2, i['attribute_combinations'][0]['value_name'])
# 			if i['seller_custom_field'] is not None:
# 				sku_data = i['seller_custom_field']
# 			else:
# 				if data['seller_custom_field'] is not None:
# 					sku_data = data['seller_custom_field']
# 				else:
# 					sku_data = "-"
# 			ws.write(row_num, 3, sku_data)
# 			if sku_data.startswith('1'):
# 				ws.write(row_num, 4, "Caso 1")
# 			elif sku_data.startswith('5') and "(" in  i['attribute_combinations'][0]['value_name']:
# 				ws.write(row_num, 4, "Caso 3")
# 			elif sku_data == "-":
# 				ws.write(row_num, 4, "-")
# 			else:
# 				ws.write(row_num, 4, "Caso 2")
# 			row_num += 1
# 	else:
# 		ws.write(row_num, 0, data['id'])
# 		ws.write(row_num, 1, data['title'])
# 		ws.write(row_num, 2, "-")
# 		if data['attributes']:
# 			sku_data = None
# 			for e in data['attributes']:
# 				if e['id'] == "SELLER_SKU":
# 					sku_data = e['value_name']
# 					break
# 			if sku_data is not None:
# 				sku_data = sku_data
# 			else:
# 				sku_data = "-"
# 		else:
# 			if data['seller_custom_field'] is not None:
# 				sku_data = data['seller_custom_field']
# 			else:
# 				sku_data = "-"
# 		ws.write(row_num, 3, sku_data)
# 		if sku_data.startswith('1'):
# 			ws.write(row_num, 4, "Caso 1")
# 		elif sku_data.startswith('5') and "(" in  i['attribute_combinations'][0]['value_name']:
# 			ws.write(row_num, 4, "Caso 3")
# 		elif sku_data == "-":
# 			ws.write(row_num, 4, "-")
# 		else:
# 			ws.write(row_num, 4, "Caso 2")
# 		row_num += 1

# wb.close()
# from sales.models import Product_Sale_Historic_Cost
# import datetime
# from products.models import ProductVariation
# from django.db.models import Sum
# from django.db.models.functions import Coalesce
# from datetime import timedelta
# import xlsxwriter
# from openpyxl import load_workbook



### Actualizar alarmas ###

# wb = load_workbook('inventario.xlsx')
# ws = wb['2019-07-05']
# count = 0
# for row in ws.rows:
# 	if count > 1:
# 		product_code = Product_Code.objects.filter(code_seven_digits=row[0].value).update(quantity=int(row[15].value))
# 		print("actualizada")
# 	count += 1

### ------------- ###
# d1 = datetime.date(2018, 11, 19)  # start date
# d2 = datetime.date(2019, 7, 2)
# delta = d2 - d1  # timedelta
# dates_ = []

# for i in range(delta.days + 1):
#     dates_.append(d1 + timedelta(days=i))
# wb2 = xlsxwriter.Workbook("Ventas Categoria Deportes.xlsx")
# ws2 = wb2.add_worksheet("Hoja1")
# row_num = 0
# for row in ws.rows:
# 	col = 1
# 	ws2.write(row_num, 0, row[0].value)
# 	for date in dates_:
# 		sales = ProductVariation.objects.filter(product__last_category=row[0].value, date_search=date).aggregate(ventas=Sum(Coalesce('diference_day_before', 0)))
# 		print(sales)
# 		if sales['ventas'] is not None:
# 			ws2.write(row_num, col, sales['ventas'])
# 		else:
# 			ws2.write(row_num, col, 0)
# 		col += 1
# 	row_num += 1
# wb2.close()
# wb.close()


# ws2 = wb['NACIONAL']
# count = 0
# products = []
# for row in ws2.rows:
# 	if count > 0:
# 		product_sale = Product_Sale.objects.filter(sku=row[3].value)
# 		if product_sale:
# 			date_ = datetime.datetime.strptime(row[1].value, '%d-%m-%Y')
# 			p_s_c = Product_Sale_Historic_Cost.objects.create(
# 				product_sale=product_sale[0],
# 				unit_cost=round(row[4].value),
# 				credit_iva=round(row[5].value),
# 				minimun_price_bill=round(row[6].value),
# 				purchase_type="LOCAL",
# 				date_cost=date_.date(),
# 				purchase_code=row[0].value,
# 			)
# 			print("bien")
			
# 		else:
# 			pass
# 			# print("no hola")
# 	count += 1
# count = 0
# for row in ws2.rows:
# 	if count > 1:
# 		product_sale = Product_Sale.objects.filter(sku=row[3].value)
# 		if product_sale:
# 			# print("hola")
# 			products.append(row[3].value)
# 		else:
# 			# print("no hola")
# 		count += 1

# print(len(list(set(products))))



################### Carga de Sku y categorías #######################
# wb = load_workbook('purchases/Maestro Comision 04-06-2019 19_38.xlsx')
# ws = wb['Base categorias SKU Pack']
# count = 0
# for row in ws.rows:
# 	count += 1
# 	if count > 1:
# 		print(str(row[0].value)[:5])
# 		sub_category_ripley = SubCategoriesChannel.objects.get(name=row[5].value.lower().capitalize(), channel_id=3, category_asiamerica__name=row[11].value)
# 		sub_category_falabella = SubCategoriesChannel.objects.get(name=row[6].value.lower().capitalize(), channel_id=8, category_asiamerica__name=row[11].value)
# 		sub_category_linio = SubCategoriesChannel.objects.get(name=row[7].value.lower().capitalize(), channel_id=5, category_asiamerica__name=row[11].value)
# 		pack = Pack.objects.get(sku_pack=int(str(row[0].value)[:5]))

# 		sku_category = SKUCategoryChannel.objects.create(
# 			product_sale=None,
# 			pack=pack,
# 			sub_category_channel=sub_category_ripley,
# 		)

# 		sku_category = SKUCategoryChannel.objects.create(
# 			product_sale=None,
# 			pack=pack,
# 			sub_category_channel=sub_category_falabella,
# 		)

# 		sku_category = SKUCategoryChannel.objects.create(
# 			product_sale=None,
# 			pack=pack,
# 			sub_category_channel=sub_category_linio,
# 		)




		# channel = Channel.objects.get(name=row[2].value)
		# category_asiamerica = Category_Product_Sale.objects.get(name=row[3].value)
		# name = row[0].value.lower().capitalize()
		# commission = int(row[1].value * 100)
		# sub_category = SubCategoriesChannel.objects.create(
		# 	commission=commission,
		# 	name=name,
		# 	category_asiamerica=category_asiamerica,
		# 	channel=channel,
		# )

		# print(str(row[0].value) + " medidas: " + str(row[2].value) + " " + str(row[3].value) + " " + str(row[4].value) + " " + str(row[5].value))
# 		sku_data = Product_Code.objects.filter(code_seven_digits=str(row[0].value))
# 		if sku_data:
# 			sku_data.update(quantity=int(row[14].value))
# 			print("actualizado")



# 	print("hi")
# 	if row['listing_type_id'] == 'gold_pro':
# 		premium = "Si"
# 	else:
# 		premium = "No"
# 	ws.write(row_num, 0, str(row['id']))
# 	ws.write(row_num, 1, str(row['title']))
# 	ws.write(row_num, 2, str(premium))
# 	row_num += 1
# wb.close()
	# count += 1
	# if count > 1:
	# 	product_code = Product_Code.objects.filter(code_seven_digits=str(row[5].value))
	# 	if product_code:
	# 		print(str(product_code[0].color.description))
	# 		continue
	# 	else:
	# 		print("no existe " +str(row[1].value))
			# product = Product_Code.objects.get(code_seven_digits=str(row[1].value))
			# pack = Pack.objects.create(
			# 	sku_pack="S" + str(row[0].value),
			# 	description=str(row[2].value),
			# 	channel_id=3,
			# )
			# sku_ripley = Product_SKU_Ripley.objects.create(
			# 	description=row[2].value,
			# 	sku_ripley=str(row[0].value),
			# 	pack_ripley=pack,
			# 	color=product.color
			# )
			# pack_product = Pack_Products.objects.create(
			# 	pack=pack,
			# 	product_pack=product.product,
			# 	quantity=1,
			# 	percentage_price=100,
			# 	color=product.color,
			# )

			# print("Creado " + str(row[0].value))


# print(len(rows))



# ------------------- Obtención de precios de página de Falabella
# import re
# from mechanize import Browser
# from bs4 import BeautifulSoup as BS
# import json
# from sales.models import Product_SKU_Ripley
# from prices.models import Price_Product
# import datetime

# br = Browser()
# br.set_handle_robots(False)
# br.addheaders = [('User-agent', 'Firefox')]
# br.open( "https://www.falabella.com/falabella-cl/" )
# for form in br.forms():
# 	if form.attrs.get('id', None) == 'searchForm':
# 		br.form = form
# 		break

# br.form[ 'Ntt' ] = '7242804'
# br.submit()
# soup = BS(br.response().read())
# data = soup.find_all('script')[8]

# # print(data)
# try:
# 	json_text = re.search(r'^\s*var fbra_browseMainProductConfig\s*=\s*({.*?})\s*;\s*$', data.string, flags=re.DOTALL | re.MULTILINE).group(1)
# 	data = json.loads(json_text)["state"]["product"]["prices"]
# 	offer_price = data[0]['originalPrice']


# 	page_price = int(offer_price.replace(".",""))
# except Exception as e:
# 	data = soup.find_all('script')[9]
# 	json_text = re.search(r'^\s*var fbra_browseMainProductConfig\s*=\s*({.*?})\s*;\s*$', data.string, flags=re.DOTALL | re.MULTILINE).group(1)
# 	data = json.loads(json_text)["state"]["product"]["prices"]
# 	offer_price = data[0]['originalPrice']

# print(data)
# print(page_price)
# sku_falabella = Product_SKU_Ripley.objects.filter(sku_ripley="7102932")
# date_compare = datetime.datetime.now().date()
# if sku_falabella:
# 	if sku_falabella[0].pack_ripley is not None:
# 		unit_price = Price_Product.objects.filter(pack=sku_falabella[0].pack_ripley)
# 	else:
# 		unit_price = Price_Product.objects.filter(product_sale=sku_falabella[0].product_ripley)
# 	if unit_price:
# 		if unit_price[0].falabella_offer_price is not None:
# 			if ((date_compare > unit_price[0].date_start) and (date_compare < unit_price[0].date_end)):
# 				unit_price_base = unit_price[0].falabella_offer_price
# 			else:
# 				unit_price_base = unit_price[0].falabella_price
# 	if unit_price_base != page_price:
# 		price_product = page_price
# 	else:
# 		price_product = unit_price_base
# print(str(page_price) + " precio pagina")
# print(str(unit_price_base) + " precio base")
# print(str(price_product) + " precio elegido")
