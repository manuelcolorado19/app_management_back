from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import PurchaseViewSet

router = DefaultRouter()
router.register(r'purchase', PurchaseViewSet, base_name='purchase')

urlpatterns = [
	url(r'^', include(router.urls)),
	
]
