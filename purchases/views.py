from django.shortcuts import render
from .models import Purchase_Product, Provider, Purchase
from sales.models import  Product_Sale, Sale, Sale_Product, Product_Channel, Product_Code, Color, Category_Product_Sale
from rest_framework.views import APIView
from rest_framework import status
from datetime import date, timedelta
import datetime
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.response import Response
import itertools
from operator import itemgetter
from django.db import transaction
from rest_framework.viewsets import ModelViewSet
from .serializers import PurchaseSerializer
import time
import xlwt
import xlsxwriter
import io
from django.http import HttpResponse
import os

class PurchaseXLSXExampleViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		file_path = os.path.join(os.path.dirname(os.path.realpath(__name__)), 'Archivo de ejemplo3.xlsx')
		response = HttpResponse(open(file_path, 'rb').read())
		response['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		response['Content-Disposition'] = 'attachment; filename=Archivo de ejemplo.xlsx'
		return response

class MassivePurchaseData(APIView):

	permission_classes = (IsAdminUser,)
	@transaction.atomic
	def post(self, request):

		for row in request.data:
			document_type =row['document_type']
			if document_type != 'sdt':
				if Purchase.objects.filter(number_document=row['number_document'], document_type=document_type, provider__rut=row['rut_provider']):
					return Response(data={"number_document": ["Revisar el número o tipo de documento " +str(row["number_document"]) +" no se puede repetir para el proveedor"]}, status=status.HTTP_400_BAD_REQUEST)
			if Product_Sale.objects.filter(sku=str(row['sku'])[:5]).count() == 0:
				return Response(data={"sku": ["El número de sku " + str(row['sku']) + " no existe"]}, status=status.HTTP_400_BAD_REQUEST)
			# if Product_Code.objects.filter(code_seven_digits=str(row['code_seven_digits'])).count() == 0:
			# 	return Response(data={"code_seven_digits": ["El código de 7 dígitos " + str(row['code_seven_digits']) + " no existe"]}, status=status.HTTP_400_BAD_REQUEST)


		sorted_order = sorted(request.data, key=itemgetter('number_document'))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['number_document']):
		    group_order.append(list(group))
		for each in group_order:
			# Creo la compra aqui
			purchase = self.create_purchase(request, each[0])
			total = 0
			for data in each:
				#Guardo cada producto de la venta
				
				product_sale = Product_Sale.objects.get(sku=str(data['sku'])[:5])
				#color = Color.objects.filter(description=data['model'])
				#if color:
					# Existe modelo
				#	color = Color.objects.get(description=data['model'])
				#else:
					# No existe modelo
				#	color = Color.objects.create(
				#		description=data['model'] 
				#	)
				# category = Category_Product_Sale.objects.get(name=data['category'])
				#code_seven_digits = Product_Code.objects.filter(product__sku=str(data['sku'])).order_by('-code_seven_digits')[0].code_seven_digits

				product_code = Product_Code.objects.filter(product__sku=str(data['sku'])[:5])
				if product_code:
					if product_code.count() == 1:
						product_code = product_code[0]
					else:
						product_code = Product_Code.objects.filter(code_seven_digits=str(data['code_seven_digits']))
						if product_code:
							product_code = product_code[0]
						else:
							product_code = None
					#print(data['code_seven_digits'])
					#product_code = Product_Code.objects.get(code_seven_digits=str(data['code_seven_digits']))
				else:
					product_code = None

				# Se guarda variación del producto
				#product_code = Product_Code.objects.get(
				#	code_seven_digits=str(data['code_seven_digits']),
				#)
				#print(type(data['pmb']))
				#print(type(data['credit_iva']))
				#print(type(data['unit_price']))
				#print(type(data['quantity']))
				#print(type(data['average_price']))
				total_unit = data['quantity'] * data['unit_price']

				product_purchase = Purchase_Product.objects.create(
					purchase=purchase,
					product_purchase=product_sale,
					product_purchase_code=product_code,
					quantity=int(data['quantity']),
					seller_sku=data['seller_sku'],
					# seller_sku=None,
					unit_price=int(data['unit_price']),
					average_price=float(data['average_price']),
					product_purchase_total=total_unit,
					credit_iva=float(data['credit_iva']),
					minimun_price_bill=int(data['pmb']),
					# minimun_price_bill=0,
				)
				product_sale.stock = product_sale.stock + int(data['quantity'])
				product_sale.save()
				if product_code is not None:
					Product_Code.objects.filter(id=product_code.id).update(quantity=(product_code.quantity + int(data['quantity'])))
				total += product_purchase.product_purchase_total
		purchase.total_purchase = total
		purchase.save()
			
		return Response(data={"purchase": ['Datos cargados exitósamente']}, status=status.HTTP_200_OK)

	def create_purchase(self, request, data):
		if data['rut_provider'] == '-':
			provider = Provider.objects.get(rut='9999999')
		else:
			if Provider.objects.filter(rut=data['rut_provider']):
				provider = Provider.objects.get(rut=data['rut_provider'])
			else:
				provider = Provider.objects.create(
					rut=data['rut_provider'],
					name=data['name_provider'],
					address="",
					phone="",
					email="",
				)
		if data['warehouse'] != '':
			warehouse = int(data['warehouse'])
		else:
			warehouse = None

		purchase = Purchase.objects.create(
			date_purchase=datetime.datetime.strptime(data['date_purchase'], "%d-%m-%Y"),
			user=request.user,
			number_document=data['number_document'],
			provider=provider,
			total_purchase=0,
			document_type=data['document_type'],
			purchase_type=data['purchase_type'],
			warehouse_id=warehouse,
		)
		return purchase

	
class PurchaseViewSet(ModelViewSet):
	queryset = Purchase.objects.all().order_by('-date_purchase')
	serializer_class = PurchaseSerializer
	permission_classes = (IsAuthenticated,)
	# filter_backends = (search_filter.SearchFilter,)
	http_method_names  = ['get', ]
	# search_fields  = ('=sku', '=product_code__code_seven_digits')

class PurchasesXLSViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, pk=None):
		# start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Informe de compras ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		# print(pk)
		if pk == '999999' or pk ==999999:
			rows = rows = Purchase_Product.objects.all().values(
				'purchase__id', 
				'purchase__date_purchase', 
				'product_purchase__sku', 
				'product_purchase_code__color__description', 
				'quantity', 
				'unit_price', 
				'product_purchase_total', 
				'credit_iva', 
				'minimun_price_bill',  
				'seller_sku', 
				'purchase__provider__rut', 
				'purchase__provider__name', 
				'purchase__document_type', 
				'purchase__number_document', 
				'purchase__purchase_type', 
			).order_by('-purchase__id')
			ws = wb.add_worksheet("Compras")
		else:
			ws = wb.add_worksheet("Compra número " + str(pk))
			rows = Purchase_Product.objects.filter(purchase__id=pk).values(
				'purchase__id', 
				'purchase__date_purchase', 
				'product_purchase__sku', 
				'product_purchase_code__color__description', 
				'quantity', 
				'unit_price', 
				'product_purchase_total', 
				'credit_iva', 
				'minimun_price_bill',  
				'seller_sku', 
				'purchase__provider__rut', 
				'purchase__provider__name', 
				'purchase__document_type', 
				'purchase__number_document', 
				'purchase__purchase_type', 
			).order_by('-purchase__id')


		columns = [{"name": 'Fecha Compra', "width":12, "rotate": True}, {"name": 'N° de Compra', "width":6, "rotate": True},{"name": 'SKU', "width": 9, "rotate": True}, {"name": 'Modelo', "width": 13, "rotate": True}, {"name": 'Cantidad', "width": 6, "rotate": True}, {"name": 'Costo Unit.', "width": 6, "rotate": True}, {"name": 'Total', "width": 7, "rotate": True}, {"name": 'Iva Credito', "width": 7, "rotate": True}, {"name": 'PVD', "width": 7, "rotate": True},{"name": 'Codigo producto Proveedor', "width": 11, "rotate": True}, {"name": 'Rut Proveedor', "width": 10, "rotate": True}, {"name": 'Nombre Proveedor', "width": 14, "rotate": True}, {"name": 'Tipo de Documento', "width": 9, "rotate": True}, {"name": 'N° de Documento', "width": 10, "rotate": True}, {"name": 'Tipo de Compra', "width": 10, "rotate": True}]

		number_columns = { "purchase__date_purchase": 0, "purchase__id": 1, "product_purchase__sku": 2, "product_purchase_code__color__description": 3, "quantity": 4, "unit_price": 5, "product_purchase_total": 6, "credit_iva": 7, "minimun_price_bill": 8,"seller_sku": 9, "purchase__provider__rut": 10, "purchase__provider__name": 11, "purchase__document_type": 12, "purchase__number_document": 13, "purchase__purchase_type": 14 }
		row_num = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()

		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])

		for row in rows:
			row_num += 1
			row["purchase__date_purchase"] = str(row['purchase__date_purchase'])
			row["purchase__purchase_type"] = row['purchase__purchase_type'].capitalize()
			row["purchase__document_type"] = row['purchase__document_type'].capitalize()
			for col_num in number_columns:
				if row[col_num] is not None:
					ws.write(row_num, number_columns[col_num], row[col_num], cell_format_data)
				else:
					ws.write(row_num, number_columns[col_num], "-", cell_format_data)
		
		ws.autofilter(0, 0, 0, len(columns) - 1)
		wb.close()
		output.seek(0)
		filename = 'Informe de Compras Asiamerica.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		# print("--- %s seconds ---" % (time.time() - start_time))
		return response

