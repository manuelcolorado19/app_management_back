from django.apps import AppConfig


class RetailpaymentConfig(AppConfig):
    name = 'retailPayment'
