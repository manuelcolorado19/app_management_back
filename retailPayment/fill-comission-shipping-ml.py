from sales.models import Sale, Sale_Product, Commission, CommissionChannel
from notifications.models import Token
import requests
import datetime
token = Token.objects.get(id=1).token_ml
date_from = datetime.date(2018,12,1)
date_to = datetime.date(2018,12,2)
sales = Sale.objects.filter(channel__id=1, date_sale__range=[date_from, date_to])

# Obtengo todas las ventas desde el 01-12-2018 hasta el 17-05-2019
url_ml = "https://api.mercadolibre.com/orders/search?seller=216244489&access_token="+ token +"&order.date_created.from=2018-11-30T16:00:00.000-00:00&order.date_created.to=2018-12-03T23:00:00.000-00:00"
# print(url_ml)
data = requests.get(url_ml).json()
# print(data['paging'])
if data['paging']['total'] <= 50:
	rows = data["results"]
else:
	rows = []
	count = 0
	iteration_end = int(data['paging']['total']/50)
	module = int(data['paging']['total']%50)
	for i in range(iteration_end + 1):
		print(i)
		new_rows = requests.get(url_ml + "&offset=" + str(i*50)).json()['results']
		rows = rows + new_rows
	rows = rows + requests.get(url_ml + "&offset="+str(((iteration_end*50) + module))).json()['results']
# print(len(rows))
# print(sales.count())
for sale in sales:
	for i in rows:
		if i['id'] == sale.order_channel:
			print(str(i['id']) + " numero de venta")
			commission_api = i['order_items'][0]['sale_fee'] * i['order_items'][0]['quantity']
			# print(commission_api)
			for sale_product in sale.sale_of_product.exclude(product__sku=1):
				percentage_commission = commission_api * (((sale_product.total * 100) / (i['order_items'][0]['quantity'] * i['order_items'][0]['unit_price']))/100)

				if i['shipping']['status'] != 'to_be_agreed':
					shipping_complete_price = (i['shipping']['shipping_option']['cost'] - i['shipping']['shipping_option'].get('list_cost', 0))
					unit_shipping_price = shipping_complete_price * (((sale_product.total * 100) / (i['order_items'][0]['quantity'] * i['order_items'][0]['unit_price']))/100)
				else:
					unit_shipping_price = 0

				print(str(percentage_commission) + " comision prorrateada")
				print(str(unit_shipping_price) + " envio prorrateado")
			print("\n")
				# commission = Commission.objects.create(
				# 	sale_product=sale_product,
				# 	api_commission=percentage_product_commission,
				# 	# calculate_commission=percentage_product_commission_calculate,
				# 	shipping_price_assigned=unit_shipping_price,
				# )
