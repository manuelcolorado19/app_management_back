from .models import BankReport
import django_filters

class BankReportFilter(django_filters.FilterSet):
    bank_retail_payment = django_filters.BooleanFilter(field_name='bank_retail_payment', lookup_expr='isnull', exclude=True)

    class Meta:

        model = BankReport
        fields = '__all__'