from openpyxl import load_workbook
import itertools
from operator import itemgetter
import xlsxwriter
import datetime
from os import listdir
from os.path import isfile, join
# wb = load_workbook('Liquidacion Linio 20180820.xlsx')
path_labels = "Archivos Linio/"
onlyfiles = [f for f in listdir(path_labels) if isfile(join(path_labels, f))]
# print(onlyfiles)
for file in onlyfiles:
	wb = load_workbook("Archivos Linio/" + file)
	ws = wb['Reporte de Ventas']
	count = 1
	data_xlsx = []
	for row in ws.rows:
		if count > 1:
			if "Sponsored Products" in row[1].value:
				pass
			else:
				data_xlsx.append({
					"date_payment": row[0].value,
					"transaction_type": row[1].value,
					"mount": row[6].value,
					"order_channel": row[12].value,
					"order_item_status": row[15].value,
				})
		count += 1

	sorted_order = sorted(data_xlsx, key=itemgetter('order_channel'))
	group_order = []
	for key, group in itertools.groupby(sorted_order, key=lambda x:x['order_channel']):
	    group_order.append(list(group))
	wb =  xlsxwriter.Workbook("Archivos Linio/" + file.replace(".xlsx", "-2.xlsx"))
	ws = wb.add_worksheet("Pagos")
	cell_format_data1 = wb.add_format({'bg_color': "yellow", "font_size": 8})
	cell_format_data2 = wb.add_format({"font_size": 8})
	cell_format_data3 = wb.add_format({"font_size": 8, 'num_format': '#,###'})

	row_num = 0
	ws.write(row_num, 0, "Id Liquidación Asiamerica", cell_format_data1)
	ws.write(row_num, 1, "Id Liquidación Retail", cell_format_data1)
	ws.write(row_num, 2, "Fecha Liquidación", cell_format_data1)
	ws.write(row_num, 3, "Canal", cell_format_data1)
	ws.write(row_num, 4, "Tipo Movimiento", cell_format_data1)
	ws.write(row_num, 5, "Id Venta", cell_format_data1)
	ws.write(row_num, 6, "Total Venta", cell_format_data1)
	ws.write(row_num, 7, "Descuento Comision", cell_format_data1)
	ws.write(row_num, 8, "Descuento envío", cell_format_data1)
	ws.write(row_num, 9, "Descuento Multas", cell_format_data1)
	ws.write(row_num, 10, "Descuento Reenvíos", cell_format_data1)
	ws.write(row_num, 11, "Descuento varios", cell_format_data1)
	ws.write(row_num, 12, "Total a pagar", cell_format_data1)
	ws.write(row_num, 13, "Rut empresa", cell_format_data1)
	ws.write(row_num, 14, "N° Factura envío", cell_format_data1)
	ws.write(row_num, 15, "N° Factura comision", cell_format_data1)
	ws.write(row_num, 16, "N° Factura productos", cell_format_data1)
	ws.write(row_num, 17, "Id Ingresos", cell_format_data1)

	# print(group_order)
	row_num = 1
	for each in group_order:
		commission = 0
		total_payment = 0
		total_sale = 0
		shipping = 0
		for row in each:
			if 'Comisi' in row['transaction_type'] or "dito de comis" in row['transaction_type']:
				commission += row['mount']
			if "Ingreso del producto" in row['transaction_type'] or "Precio del " in row['transaction_type']:
				total_sale += row['mount']
			if "Shipping Fee" in row['transaction_type']:
				shipping += row['mount']
			total_payment += row['mount']
		date_payment_format = datetime.datetime.strptime(each[0]['date_payment'], '%d %b %Y').date()
		retail_liquidation = file.replace("Liquidacion Linio ","").replace(".xlsx", "")
		ws.write(row_num, 0, "L000001", cell_format_data2)
		ws.write(row_num, 1, retail_liquidation, cell_format_data2)
		ws.write(row_num, 2, date_payment_format.strftime("%d-%m-%Y"), cell_format_data2)
		ws.write(row_num, 3, "Linio", cell_format_data2)
		if total_payment < 0:
			movement_type = "egreso"
		else:
			movement_type = 'ingreso'
		ws.write(row_num, 4, movement_type, cell_format_data2)
		ws.write(row_num, 5, each[0]['order_channel'], cell_format_data2)
		ws.write(row_num, 6, total_sale, cell_format_data3)
		ws.write(row_num, 7, commission, cell_format_data3)
		if shipping == 0:
			ws.write(row_num, 8, shipping, cell_format_data2)
		else:
			ws.write(row_num, 8, shipping, cell_format_data3)
		ws.write(row_num, 9, 0, cell_format_data2)
		ws.write(row_num, 10, 0, cell_format_data2)
		ws.write(row_num, 11, 0, cell_format_data2)
		ws.write(row_num, 12, total_payment, cell_format_data3)
		ws.write(row_num, 13, "76.212.492-0", cell_format_data2)
		row_num += 1
	wb.close()


