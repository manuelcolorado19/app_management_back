from openpyxl import load_workbook
import itertools
import datetime
from operator import itemgetter
import xlsxwriter
import datetime
from os import listdir
from os.path import isfile, join
path_labels = "Transbank/"
onlyfiles = [f for f in listdir(path_labels) if isfile(join(path_labels, f))]
for file in onlyfiles:
    wb = load_workbook("Transbank/"+file)
    ws = wb["sheet1"]
    count = 0 
    grouped_data = []
    data_new = []
    for row in ws.rows:
        if count > 0:
            data_new.append({
                "monto afecto": row[8].value,
                "tipo de tarjeta": row[5].value,
                "primer abono": row[13].value,
                "fecha de venta":row[1].value
            })
        count+=1 
    if row[13].value != "":
        data_new = sorted(data_new, key= lambda x : datetime.datetime.strptime( x["primer abono"].replace("/","-"), '%d-%m-%Y'))
    group_order = []
    row_count = 0
    row_check_count = 0
    narrow_data = [] 
    #for key, group in itertools.groupby(data_new, key=lambda x:(x['primer abono'],x['tipo de tarjeta'])):
    #    group_order.append(list(group))
    #adding headers
    wb =  xlsxwriter.Workbook("transbank/informe_general/"+file+"-2.xlsx")
    ws = wb.add_worksheet("Pagos")
    cell_format_data1 = wb.add_format({'bg_color': "yellow", "font_size": 8})
    cell_format_data2 = wb.add_format({"font_size": 8})
    cell_format_data3 = wb.add_format({"font_size": 8, 'num_format': '#,###'})

    row_num = 0
    ws.write(row_num, 0, "Id Liquidación Asiamerica", cell_format_data1)
    ws.write(row_num, 1, "Id Liquidación Retail", cell_format_data1)
    ws.write(row_num, 2, "Fecha Liquidación", cell_format_data1)
    ws.write(row_num, 3, "Canal", cell_format_data1)
    ws.write(row_num, 4, "Tipo Movimiento", cell_format_data1)
    ws.write(row_num, 5, "Id Venta", cell_format_data1)
    ws.write(row_num, 6, "Total Venta", cell_format_data1)
    ws.write(row_num, 7, "Descuento Comision", cell_format_data1)
    ws.write(row_num, 8, "Descuento envío", cell_format_data1)
    ws.write(row_num, 9, "Descuento Multas", cell_format_data1)
    ws.write(row_num, 10, "Descuento Reenvíos", cell_format_data1)
    ws.write(row_num, 11, "Descuento varios", cell_format_data1)
    ws.write(row_num, 12, "Total a pagar", cell_format_data1)
    ws.write(row_num, 13, "Rut empresa", cell_format_data1)
    ws.write(row_num, 14, "N° Factura envío", cell_format_data1)
    ws.write(row_num, 15, "N° Factura comision", cell_format_data1)
    ws.write(row_num, 16, "N° Factura productos", cell_format_data1)
    ws.write(row_num, 17, "Id Ingresos", cell_format_data1)

    #  writing sheet

    row_num = 1
    for row in data_new:
        ws.write(row_num, 0, "L000001", cell_format_data2)
        ws.write(row_num, 1, datetime.datetime.strptime(row["fecha de venta"].replace("*",""), '%d/%m/%Y %H:%M').strftime('%d-%m-%Y'), cell_format_data2)
        ws.write(row_num, 2, row['primer abono'].split("-")[0].replace("/","-") , cell_format_data2)
        ws.write(row_num, 3, "Presencial", cell_format_data2)
        if row["tipo de tarjeta"] == "DB":
            movement_type = "DEBITO"
        else:
            movement_type = "CREDITO"
        ws.write(row_num, 4, movement_type, cell_format_data2)
        ws.write(row_num, 5, 0, cell_format_data2)
        ws.write(row_num, 6, row["monto afecto"], cell_format_data3)
        if row["tipo de tarjeta"] == "DB":
            comission = (row["monto afecto"]*1.7)/100
        else:
            comission = (row["monto afecto"]*3.51)/100
        ws.write(row_num, 7, comission, cell_format_data3)
        ws.write(row_num, 8, 0, cell_format_data2)
        ws.write(row_num, 9, 0, cell_format_data2)
        ws.write(row_num, 10, 0, cell_format_data2)
        ws.write(row_num, 11, 0, cell_format_data2)
        ws.write(row_num, 12, row["monto afecto"]-comission, cell_format_data3)
        ws.write(row_num, 13, "96.689.310-9", cell_format_data2)
        row_num += 1	
    wb.close()











    #check = 0 
    #for d in narrow_data:
    #    check += d["total abono"]
    #    print(d)

    #print(check)