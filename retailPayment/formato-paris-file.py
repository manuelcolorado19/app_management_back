from openpyxl import load_workbook
import itertools
import datetime
from operator import itemgetter
import xlsxwriter
import datetime
from os import listdir
from os.path import isfile, join
path_labels = "Paris/"
onlyfiles = [f for f in listdir(path_labels) if isfile(join(path_labels, f))]
# print(onlyfiles)
for file in onlyfiles:
    wb = load_workbook("Paris/" + file)
    ws = wb["Hoja1"]
    count = 1
    grouped_data = []
    data_new = []
    for row in ws.rows:
        if count > 1:
            data_new.append({
                "nro documento": row[0].value,
                "monto": row[4].value,
            })
        count+=1 
    #data_new = sorted(data_new, key= lambda x : x["nro documento"])
    group_order = []
    narrow_data = [] 
    #for key, group in itertools.groupby(data_new, key=lambda x:(x['primer abono'],x['tipo de tarjeta'])):
    #    group_order.append(list(group))
    #adding headers
    wb =  xlsxwriter.Workbook("paris/reportes/"+file+"-2.xlsx")
    ws = wb.add_worksheet("Pagos")
    cell_format_data1 = wb.add_format({'bg_color': "yellow", "font_size": 8})
    cell_format_data2 = wb.add_format({"font_size": 8})
    cell_format_data3 = wb.add_format({"font_size": 8, 'num_format': '#,###'})

    row_num = 0
    ws.write(row_num, 0, "Id Liquidación Asiamerica", cell_format_data1)
    ws.write(row_num, 1, "Id Liquidación Retail", cell_format_data1)
    ws.write(row_num, 2, "Fecha Liquidación", cell_format_data1)
    ws.write(row_num, 3, "Canal", cell_format_data1)
    ws.write(row_num, 4, "Tipo Movimiento", cell_format_data1)
    ws.write(row_num, 5, "Id Venta", cell_format_data1)
    ws.write(row_num, 6, "Total Venta", cell_format_data1)
    ws.write(row_num, 7, "Descuento Comision", cell_format_data1)
    ws.write(row_num, 8, "Descuento envío", cell_format_data1)
    ws.write(row_num, 9, "Descuento Multas", cell_format_data1)
    ws.write(row_num, 10, "Descuento Reenvíos", cell_format_data1)
    ws.write(row_num, 11, "Descuento varios", cell_format_data1)
    ws.write(row_num, 12, "Total a pagar", cell_format_data1)
    ws.write(row_num, 13, "Rut empresa", cell_format_data1)
    ws.write(row_num, 14, "*N° Factura envío", cell_format_data1)
    ws.write(row_num, 15, "*N° Factura comision", cell_format_data1)
    ws.write(row_num, 16, "*N° Factura productos", cell_format_data1)
    ws.write(row_num, 17, "*Id Ingresos", cell_format_data1)

    #  writing sheet
    row_num = 1
    for row in data_new:
        ws.write(row_num, 0, "L000001", cell_format_data2)
        ws.write(row_num, 1, file.split(" ")[2] , cell_format_data2)
        ws.write(row_num, 2, datetime.datetime.strptime( file.split(" ")[3].replace(".xlsx",""), '%Y%m%d').strftime("%d-%m-%Y"), cell_format_data2)
        ws.write(row_num, 3, "Paris", cell_format_data2)
        ws.write(row_num, 4, "ingreso", cell_format_data2)
        ws.write(row_num, 5, row["nro documento"], cell_format_data2)
        ws.write(row_num, 6, row["monto"], cell_format_data3)
        ws.write(row_num, 7,  0, cell_format_data2)
        ws.write(row_num, 8,  0, cell_format_data2)
        ws.write(row_num, 9,  0, cell_format_data2)
        ws.write(row_num, 10, 0, cell_format_data2)
        ws.write(row_num, 11, 0, cell_format_data2)
        ws.write(row_num, 12, row["monto"], cell_format_data3)
        ws.write(row_num, 13, "81.201.000-K", cell_format_data2)
        ws.write(row_num, 14, 0, cell_format_data2)
        ws.write(row_num, 15, 0, cell_format_data2)
        ws.write(row_num, 16, 0, cell_format_data2)
        ws.write(row_num, 17, 0, cell_format_data2)
        row_num += 1	
    wb.close()






#check = 0 
#for d in narrow_data:
#    check += d["total abono"]
#    print(d)

#print(check)