from openpyxl import load_workbook
import itertools
from datetime import datetime, timedelta, date
import datetime
from retailPayment.models import BankReport
from retailPayment.models import RetailPayment
from operator import itemgetter
import xlsxwriter
from os import listdir
from os.path import isfile, join

reference_date  = datetime.date.today() - timedelta(days = 5000) 
retail_payment = RetailPayment.objects.filter(date_payment__gte =  reference_date.strftime("%Y-%m-%d"))
new_dict = []
for h in retail_payment:
    new_dict.append({
        "channel_id": h.channel_id,
        "total":h.total_payment_retail
    })
group_order = []
sorted_order = sorted(new_dict, key=itemgetter('channel_id'))
for key, group in itertools.groupby(sorted_order, key=lambda x:x['channel_id']):
	    group_order.append(list(group))
wb =  xlsxwriter.Workbook("proyeccion_pagos/payment_forecast.xlsx")
ws = wb.add_worksheet("datos")
ws.set_column('A:G',15)
cell_format_data1 = wb.add_format({'bg_color': "yellow", "font_size": 8, 'border': 1, 'align': 'left'})
cell_format_data2 = wb.add_format({"font_size": 8, 'border': 1, 'align': 'left'})
cell_format_data3 = wb.add_format({"font_size": 8, 'num_format': '#,###','border': 1, 'align': 'left'})

row_num = 0
ws.write(row_num, 0, "Mercadolibre", cell_format_data1)
ws.write(row_num, 1, "Ripley", cell_format_data1)
ws.write(row_num, 2, "Linio", cell_format_data1)
ws.write(row_num, 3, "Paris", cell_format_data1)
ws.write(row_num, 4, "Groupon", cell_format_data1)
ws.write(row_num, 5, "Falabella", cell_format_data1)
ws.write(row_num, 6, "Total", cell_format_data1)

days = [0,0,0,0,0,0]
row_num = 1
for row in group_order:
    for step in row:
        if row_num == 1:
            days[0] += step['total']
        if row_num == 2:
            days[1] += step['total']
        if row_num == 3:
            days[2] += step['total']
        if row_num == 4:
            days[3] += step['total']    
        if row_num == 5:
            days[4] += step['total']        
        if row_num == 6:
            days[5] += step['total']            
        if step['channel_id'] == 1 :
            ws.write(row_num, 0, step['total'], cell_format_data2)
            row_num += 1
        if step['channel_id'] == 3:     
            ws.write(row_num, 2, step['total'], cell_format_data2)
            row_num += 1
        if step['channel_id'] == 5:     
            ws.write(row_num, 3, step['total'], cell_format_data2)
            row_num += 1  
        if step['channel_id'] == 4:     
            ws.write(row_num, 4, step['total'], cell_format_data2)
            row_num += 1  
        if step['channel_id'] == 6:     
            ws.write(row_num, 5, step['total'], cell_format_data2)
            row_num += 1
        if step['channel_id'] == 8:     
            ws.write(row_num, 6, step['total'], cell_format_data2)
            row_num += 1            
    row_num = 1

for y in days: 
    ws.write(row_num, 6, y, cell_format_data2)
    row_num += 1 

wb.close()
