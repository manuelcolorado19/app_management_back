# Generated by Django 2.1.2 on 2019-08-05 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('retailPayment', '0013_auto_20190730_1223'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paymentchecks',
            name='order_channel',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
