from django.db import models
from categories.models import Channel
from sales.models import Sale

class BankReport(models.Model):
	MOVEMENT_TYPE = (
		('ingreso','ingreso'),
		('devolución','devolución'),
	)
	bank_register = models.CharField(max_length=10)
	movement_type = models.CharField(choices=MOVEMENT_TYPE, max_length=10, null=True)
	total_amount = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	date_report = models.DateField(null=True, blank=True)
	payment_type = models.CharField(max_length=60, null=True)
	channel = models.ForeignKey(Channel, on_delete=models.CASCADE, related_name="channel_bank_report", null=True, blank=True)
	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	comments = models.CharField(max_length=300, null=True)

	class Meta:
		db_table = 'bank-report'

class RetailPayment(models.Model):
	STATUS_PAYMENT = (
		('pendiente','pendiente'),
		('exacto','exacto'),
		('con observación','con observación'),
	)
	PAYMENT_TYPE = (
		('efectivo', 'efectivo'),
		('tarjeta de debito', 'tarjeta de debito'),
		('tarjeta de credito', 'tarjeta de credito'),
		('transferencia', 'transferencia'),
		('cheque', 'cheque'),
		('por pagar', 'por pagar'),
		('sin pago', 'sin pago'),
		('transferencia retail', 'transferencia retail'),
		('mixto', 'mixto')
	)
	channel = models.ForeignKey(Channel, on_delete=models.CASCADE, related_name="channel_payment")
	bill_register = models.CharField(max_length=30)
	retail_id_number = models.BigIntegerField(null=True)
	shipping_bill_number = models.CharField(max_length=15)
	commission_bill_number = models.CharField(max_length=15)
	product_bill_number = models.CharField(max_length=15, null=True, blank=True)
	enterprise_rut = models.CharField(max_length=13, null=True, blank=True)
	total_payment_retail = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	date_payment = models.DateField(null=True, blank=True)
	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	bank_register = models.ForeignKey(BankReport, on_delete=models.CASCADE, related_name='bank_retail_payment', null=True)
	status_payment = models.CharField(choices=STATUS_PAYMENT, max_length=30, null=True)
	payment_type=models.CharField(choices=PAYMENT_TYPE, max_length=40, default='transferencia retail')

	class Meta:
		db_table = "retail-payment"

class RetailPaymentDetail(models.Model):
	MOVEMENT_TYPE = (
		('ingreso','ingreso'),
		('egreso','egreso'),
	)
	retail_payment = models.ForeignKey(RetailPayment, on_delete=models.CASCADE, related_name='retail_payment_detail', null=True)
	sale = models.ForeignKey(Sale, on_delete=models.CASCADE, related_name='sale_retail_payment', null=True)

	total_sale = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	discount_commission = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	discount_shipping = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	discount_penalty = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	discount_resend = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	several_discount = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	retail_id_number = models.BigIntegerField(null=True)
	total_pay = models.DecimalField(decimal_places=1, max_digits=10, null=True)
	movement_type = models.CharField(choices=MOVEMENT_TYPE, max_length=10)
	class Meta:
		db_table = 'retail-payment-detail'

class PaymentChecks(models.Model):
	order_channel = models.CharField(max_length=100, blank=True, null=True)
	channel = models.CharField(max_length=60, blank=True, null=True)
	movement_type = models.CharField(max_length=30, blank=True)

	class Meta:
		db_table = 'retail-payment-checks'

class CheckStatusPayment(models.Model):
	STATUS_PAYMENT = (
		("pendiente","pendiente"),
		("revisado (con liquidación)", "revisado (con liquidación)"),
		("revisado (sin liquidación)", "revisado (sin liquidación)"),
	)
	status = models.CharField(choices=STATUS_PAYMENT, max_length=30)
	checked_date = models.DateField(null=True, blank=True)
	updated_date = models.DateTimeField(auto_now=True, null=True)
	created_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	channel = models.ForeignKey(Channel, on_delete=models.CASCADE,null=True, blank=False, related_name="channel_check_status")

	class Meta:
		db_table = "check_status_payment"
