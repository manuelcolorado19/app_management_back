from .models import BankReport, RetailPayment, RetailPaymentDetail
from rest_framework import serializers
from categories.models import Channel
from sales.models import Sale
# from sales.serializers import SaleSerializer

class SaleDataSerializer(serializers.ModelSerializer):
	class Meta:
		model = Sale
		fields = '__all__'
	
class ChannelName(serializers.CharField):
	def to_representation(self, value):
		if value.channel is not None:
			return value.channel.name
		return '-'

class RetailPaymentDetailSerializer(serializers.ModelSerializer):
	sale = SaleDataSerializer(read_only=True)
	class Meta:
		model = RetailPaymentDetail
		fields = '__all__'

class BillNumberSerializer(serializers.CharField):
	def to_representation(self, value):
		# if value.bank_retail_payment is not None:
		# 	return value.bank_retail_payment.bill_register
		return '-'

class RelatedPayments(serializers.ListField):
	def to_representation(self,value):
		return RetailPayment.objects.filter(bank_register_id = value.id).values("id","bill_register","channel__name","total_payment_retail","date_payment").order_by("channel_id","-date_payment")

class BankReportSerializer(serializers.ModelSerializer):
	channel_name = ChannelName(read_only=True, source='*')
	bill_number = BillNumberSerializer(read_only=True,source="*")
	related_payments  = RelatedPayments(read_only= True, source='*')
	class Meta:
		model = BankReport
		fields = '__all__'

class RetailPaymentSerializer(serializers.ModelSerializer):
	retail_payment_detail = RetailPaymentDetailSerializer(read_only=True, many=True)
	channel_name = ChannelName(read_only=True, source='*')
	bank_register = BankReportSerializer(read_only=True)
	class Meta:
		model = RetailPayment
		fields = '__all__'

