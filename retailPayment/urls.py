from .views import RetailPaymentViewSet, BankReportViewSet, RetailPaymentXLSX
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'retail-payment', RetailPaymentViewSet, base_name='retail_payment')
router.register(r'bank-report', BankReportViewSet, base_name='bank_report')

urlpatterns = [
	url(r'^', include(router.urls)),
]