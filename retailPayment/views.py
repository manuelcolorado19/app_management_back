from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
import xlwt
import xlsxwriter
from django.db import transaction
from django.db.models import F, Sum, IntegerField, Value, CharField, Case, When
from .models import BankReport, RetailPayment, RetailPaymentDetail, PaymentChecks, CheckStatusPayment
from .serializers import RetailPaymentSerializer, BankReportSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated
import itertools, datetime
from operator import itemgetter
import io, decimal
from categories.models import Channel
from sales.models import Sale, Sale_Product
from django.db.models import Sum, Count, F, IntegerField, Value, Q, Min
from django.http import HttpResponse
from django_filters import rest_framework as filters
from .filters import BankReportFilter
from django.db.models.functions import TruncDay, ExtractWeek
from datetime import timedelta
class RetailPaymentViewSet(ModelViewSet):
	serializer_class = RetailPaymentSerializer
	queryset = RetailPayment.objects.all().order_by('channel_id', '-date_payment')
	permission_classes = (AllowAny,)
	http_method_names = ['get', 'post','put', 'patch']
	filter_backends = (filters.DjangoFilterBackend,)
	filter_fields = ('status_payment',)


	def paginate_queryset(self, queryset):
		if self.paginator and self.request.query_params.get(self.paginator.page_query_param, None) is None:
			return None
		return super().paginate_queryset(queryset)

	def charge_groupon(self, data, bill_register, bank_register, channel, date_payment_file):
		if channel.id == 5:
			retail_id_number = data[0].get('retail_id_number', None)
			# date_payment = date_payment_file
		if channel.id == 6:
			retail_id_number = None
			# date_payment = datetime.datetime.strptime(data[0]['date_payment'], '%d-%m-%Y')
		if channel.id == 3:
			retail_id_number = None
			# date_payment = datetime.datetime.strptime(data[0]['date_payment'], '%d-%m-%Y')
		if channel.id == 4:
			retail_id_number = data[0].get('retail_id_number', None)
		date_payment = date_payment_file

		retail_payment = RetailPayment.objects.create(
			channel=channel,
			bill_register=bill_register,
			retail_id_number=retail_id_number,
			shipping_bill_number=data[0].get('shipping_bill_number', "XXXXXX"),
			commission_bill_number=data[0].get('commission_bill_number', "XXXXXX"),
			product_bill_number=data[0].get('product_bill_number', None),
			enterprise_rut=data[0].get('enterprise_rut', None),
			date_payment=date_payment,
			bank_register=bank_register,
			total_payment_retail=0,
			status_payment="pendiente",
		)
		total_payment = 0
		for each in data:
			# Se crea registro de Pago Retailchannel
			
			order_data = each.get('order_channel', None)
			if order_data is not None and str(order_data).isdigit():
				if channel.id == 4:
					sale = Sale.objects.filter(channel_id=4, number_document=each['order_channel'], document_type="factura")
					if sale:
						sale = sale[0]
					else:
						sale = None
				else:
					sale = Sale.objects.filter(order_channel=each['order_channel'], channel=retail_payment.channel)
					if sale:
						sale = sale[0]
					else:
						sale = None
			else:
				sale = None
		
			retail_payment_detail = RetailPaymentDetail.objects.create(
				retail_payment=retail_payment,
				sale=sale,
				total_sale=each['total_sale'],
				discount_commission=each['discount_commission'],
				discount_shipping=each['discount_shipping'],
				discount_penalty=each['discount_penalty'],
				discount_resend=each['discount_resend'],
				several_discount=each['several_discount'],
				total_pay=each['total_pay'],
				movement_type=each['movement_type'].lower(),
			)
			total_payment += float(each['total_pay'])
		retail_payment.total_payment_retail = total_payment
		retail_payment.save()

	def charge_other_channels(self, data, bill_register, date_payment_file):
		sorted_order = sorted(data, key=itemgetter('retail_id_number'))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['retail_id_number']):
		    group_order.append(list(group))

		for each in group_order:
			# Se crea registro de Pago Retailchannel
			bank_register = BankReport.objects.filter(bank_register=each[0]['bank_register'])
			if bank_register:
				bank_register = bank_register[0]
			else:
				bank_register = None
			# bill_register = RetailPayment.objects.all().count()
			# bill_register = "L" + "".join(["0" for i in range(5 - len(str(bill_register)))]) + str(bill_register + 1)

			retail_payment = RetailPayment.objects.create(
				channel=Channel.objects.get(name=each[0]['channel'].lower().capitalize()),
				bill_register=bill_register,
				retail_id_number=each[0]['retail_id_number'],
				shipping_bill_number=each[0].get('shipping_bill_number', "XXXXXX"),
				commission_bill_number=each[0].get('commission_bill_number', "XXXXXX"),
				product_bill_number=each[0].get('product_bill_number', None),
				enterprise_rut=each[0].get('enterprise_rut', None),
				date_payment=date_payment_file,
				bank_register=bank_register,
				total_payment_retail=0,
				status_payment="pendiente",
			)
			total_payment = 0
			for row in each:
				# Se crea cada detalle de cada pago de ventas dado por el retail.
				order_data = row.get('order_channel', None)
				if order_data is not None and str(order_data).isdigit():
					sale = Sale.objects.filter(order_channel=row['order_channel'], channel=retail_payment.channel)
					if sale:
						sale = sale[0]
					else:
						sale = None
				else:
					sale = None
			
				retail_payment_detail = RetailPaymentDetail.objects.create(
					retail_payment=retail_payment,
					sale=sale,
					total_sale=row['total_sale'],
					discount_commission=row['discount_commission'],
					discount_shipping=row['discount_shipping'],
					discount_penalty=row['discount_penalty'],
					discount_resend=row['discount_resend'],
					several_discount=row['several_discount'],
					total_pay=row['total_pay'],
					movement_type=row['movement_type'].lower(),
				)
				total_payment += float(row['total_pay'])
			retail_payment.total_payment_retail = total_payment
			retail_payment.save()
	def charge_presential(self, data, bill_register, bank_register, date_payment_file):
		bank_register = BankReport.objects.filter(bank_register=bank_register)
		if bank_register:
			bank_register = bank_register[0]
		else:
			bank_register = None
		retail_payment = RetailPayment.objects.create(
			channel=Channel.objects.get(name="Presencial"),
			bill_register=bill_register,
			retail_id_number=None,
			shipping_bill_number=data[0].get('shipping_bill_number', "XXXXXX"),
			commission_bill_number=data[0].get('commission_bill_number', "XXXXXX"),
			product_bill_number=data[0].get('product_bill_number', None),
			enterprise_rut=data[0].get('enterprise_rut', None),
			date_payment=date_payment_file,
			bank_register=bank_register,
			total_payment_retail=0,
			status_payment="pendiente",
			payment_type=data[0]['retail_id_number'],
		)
		total_payment = 0
		for row in data:
			order_data = row.get('order_channel', None)
			if order_data is not None and str(order_data).isdigit():
				sale = Sale.objects.filter(id=row['order_channel'])
				if sale:
					sale = sale[0]
				else:
					sale = None
			else:
				sale = None
		
			retail_payment_detail = RetailPaymentDetail.objects.create(
				retail_payment=retail_payment,
				sale=sale,
				total_sale=row['total_sale'],
				discount_commission=row['discount_commission'],
				discount_shipping=row['discount_shipping'],
				discount_penalty=row['discount_penalty'],
				discount_resend=row['discount_resend'],
				several_discount=row['several_discount'],
				total_pay=row['total_pay'],
				movement_type=row['movement_type'].lower(),
			)
			total_payment += float(row['total_pay'])
		retail_payment.total_payment_retail = total_payment
		retail_payment.save()

	def update(request,self,pk):
		result = RetailPayment.objects.filter(bank_register_id=pk).update(bank_register=None, status_payment="pendiente")
		# for item in result: 
			# item.bank_register = None
			# item.status_payment = "pendiente"
			# item.save()		
		return Response(data={"retail-payment": ["Datos cargados de forma exitosa"]}, status=status.HTTP_200_OK)

	@transaction.atomic
	def create(self, request):
		# Añadir validación de venta y canal.
		data = request.data['rows']
		date_payment_file = datetime.datetime.strptime(request.data['date_payment'], '%Y%m%d.xlsx')
		for i in data:
			#print(i)
			channel = Channel.objects.filter(name=i['channel'].lower().capitalize())
			if channel:
				order_data = i.get('order_channel', None)
				if channel[0].id in [1,8,5,6,3,4]:
					retail_ = RetailPaymentDetail.objects.filter(retail_id_number=i['retail_id_number'], retail_payment__channel_id=channel[0].id)
					if retail_:
						return Response(data={"retail": ['Disculpe el número de pago ' + str(i['retail_id_number']) + " del retail " + channel[0].name + " ya se encuentra en los registros"]}, status=status.HTTP_400_BAD_REQUEST)
					if order_data is not None and str(order_data).isdigit():
						if not Sale.objects.filter(order_channel=i['order_channel'], channel_id=channel[0].id):
							payment_check = PaymentChecks.objects.create(
								order_channel=i['order_channel'],
								channel=i['channel'].lower().capitalize(),
								movement_type=("egreso" if i['total_pay'] < 0 else "ingreso"),
							)
					else:
						payment_check = PaymentChecks.objects.create(
							order_channel=order_data,
							channel=i['channel'].lower().capitalize(),
							movement_type=("egreso" if i['total_pay'] < 0 else "ingreso"),
						)
					# return Response(data={"sale": ['Disculpe no se encuentra la orden de venta ' + str(i['order_channel'])+" para el canal indicado."]}, status=status.HTTP_400_BAD_REQUEST)
			else:
				return Response(data={"channel": ['Disculpe el canal ' + str(i['channel'])+" no se encuentra en los registros."]}, status=status.HTTP_400_BAD_REQUEST)
		bank_register = BankReport.objects.filter(bank_register=data[0].get('bank_register', None))
		if bank_register:
			bank_register = bank_register[0]
		else:
			bank_register = None
		bill_register_number = int(RetailPayment.objects.latest('date_created').bill_register.replace("L",""))
		bill_register = "L" + "".join(["0" for i in range(5 - len(str(bill_register_number)))]) + str(bill_register_number + 1)

		if channel[0].id in  [1,8]:
			# Mercadolibre, Falabella y Ripley?
			self.charge_other_channels(data, bill_register, date_payment_file)
		elif channel[0].id in [5,6,3,4]:
			# Groupon
			self.charge_groupon(data, bill_register, bank_register, channel[0], date_payment_file)
		else:
			# Canales Presenciales
			self.charge_presential(data, bill_register, bank_register, date_payment_file)
		# retail_payment = RetailPayment.objects.create(
		# 	channel=Channel.objects.get(name=data[0]['channel'].lower().capitalize()),
		# 	bill_register=bill_register,
		# 	retail_id_number=request.data[0].get('retail_id_number', None),
		# 	shipping_bill_number=request.data[0].get('shipping_bill_number', "XXXXXX"),
		# 	commission_bill_number=request.data[0].get('commission_bill_number', "XXXXXX"),
		# 	product_bill_number=request.data[0].get('product_bill_number', None),
		# 	enterprise_rut=request.data[0].get('enterprise_rut', None),
		# 	date_payment=datetime.datetime.strptime(request.data[0]['retail_id_number'], '%Y%m%d'),
		# 	bank_register=bank_register,
		# 	total_payment_retail=0,
		# 	status_payment="pendiente",
		# )
		# total_payment = 0
		# for each in request.data:
		# 	# Se crea registro de Pago Retailchannel
			
		# 	order_data = each.get('order_channel', None)
		# 	if order_data is not None and str(order_data).isdigit():
		# 		sale = Sale.objects.filter(order_channel=each['order_channel'], channel=retail_payment.channel)
		# 		if sale:
		# 			sale = sale[0]
		# 		else:
		# 			sale = None
		# 	else:
		# 		sale = None
		
		# 	retail_payment_detail = RetailPaymentDetail.objects.create(
		# 		retail_payment=retail_payment,
		# 		sale=sale,
		# 		total_sale=each['total_sale'],
		# 		discount_commission=each['discount_commission'],
		# 		discount_shipping=each['discount_shipping'],
		# 		discount_penalty=each['discount_penalty'],
		# 		discount_resend=each['discount_resend'],
		# 		several_discount=each['several_discount'],
		# 		total_pay=each['total_pay'],
		# 		movement_type=each['movement_type'].lower(),
		# 	)
		# 	total_payment += float(each['total_pay'])
		# retail_payment.total_payment_retail = total_payment
		# retail_payment.save()
		return Response(data={"retail-payment": ["Datos cargados de forma exitosa"]}, status=status.HTTP_200_OK)

class BankReportViewSet(ModelViewSet):
	serializer_class = BankReportSerializer
	queryset = BankReport.objects.all().order_by('channel_id', '-date_report')
	permission_classes = (AllowAny,)
	http_method_names = ['get', 'post', 'put', 'patch']
	filter_backends = (filters.DjangoFilterBackend,)
	filter_class = BankReportFilter
	filter_fields = ('bank_retail_payment',)
	

	def paginate_queryset(self, queryset):
		if self.paginator and self.request.query_params.get(self.paginator.page_query_param, None) is None:
			return None
		return super().paginate_queryset(queryset)

	@transaction.atomic
	def update(self, request,pk=None):
		# print(request.data)
		for each in request.data:
			if each.get('bill_register', None) is not None and each['bill_register'].startswith("L"):
				bill_register = each.get('bill_register', None)
			if each.get('bill_register', None) is not None and each['bill_register'].startswith("CB"):
				bank_register = each.get('bill_register', None)
		retail_payment = RetailPayment.objects.get(bill_register=bill_register)
		bank_report = BankReport.objects.get(bank_register=bank_register)
		if retail_payment.channel != bank_report.channel:
			return Response(data={"channel": ['Los canales no coinciden en los datos enviados']}, status=status.HTTP_400_BAD_REQUEST)
		if retail_payment.date_payment > bank_report.date_report:
			return Response(data={"channel": ['La fecha de cartola no puede ser menor que la del pago del retail']}, status=status.HTTP_400_BAD_REQUEST)
		retail_payment.bank_register = bank_report
		if retail_payment.total_payment_retail != bank_report.total_amount:
			status_pay = "con observación"
		else:
			status_pay = "exacto"
		retail_payment.status_payment = status_pay
		retail_payment.save()
		return Response(data={"retail_payment": ['Pago asociado de forma exitosa']}, status=status.HTTP_200_OK)


	@transaction.atomic
	def create(self, request):
		# Añadir validación de datos
		for each in request.data:
			if BankReport.objects.filter(bank_register=each['register_bank']):
				continue
			else:

				bank_report = BankReport.objects.create(
					bank_register=each['register_bank'],
					movement_type=each['movement_type'],
					total_amount=each['total_amount'],
					date_report=datetime.datetime.strptime(each['date_report'], '%d-%M-%Y'),
					payment_type=each['payment_type'],
					channel=Channel.objects.get(name=each['channel'].lower().capitalize()),
					comments=each['additional_description'] + " -- " +str(each['order_channel']) + " -- " + str(each['document_type']) + " -- " + str(each['number_document']),
				)
				if each.get('bill_register', None) is not None:
					retail_payment = RetailPayment.objects.filter(bill_register=each['bill_register'])
					if retail_payment:
						if bank_report.total_amount == retail_payment[0].total_payment_retail:
							status_payment = "exacto"
						else:
							status_payment = 'con observación'
						retail_payment.update(bank_register=bank_report, status_payment=status_payment)

		return Response(data={"retail-payment": ["Datos cargados de forma exitosa"]}, status=status.HTTP_200_OK)

class RetailPaymentSalesReport(APIView):
	permission_classes = (AllowAny,)
	def get(self, request):
		# today = date.today()
		retail_payment = self.request.query_params.get('retail_payment', None)
		rows = RetailPayment.objects.filter(id=int(retail_payment))
		output = io.BytesIO()
		file_name = 'Informe de liquidacion retail '
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(retail_payment))
		columns = [
			{"name": 'Id liquidacion Asiamerica', "width":12, "rotate": True}, 
			{"name": 'Id liquidacion Retail', "width":12, "rotate": True}, 
			{"name": 'Fecha liquidacion', "width":12, "rotate": True}, 
			{"name": 'Canal', "width":12, "rotate": True}, 
			{"name": 'Tipo movimiento', "width":12, "rotate": True}, 
			{"name": 'Id venta', "width":12, "rotate": True}, 
			{"name": 'Total venta', "width":12, "rotate": True},
			{"name": 'Descuento comision', "width":12, "rotate": True},
			{"name": 'Descuento envio', "width":12, "rotate": True},
			{"name": 'Descuento multas', "width":12, "rotate": True},
			{"name": 'Descuento reenvios', "width":12, "rotate": True}, 
			{"name": 'Descuento varios', "width":12, "rotate": True}, 
			{"name": 'Total a pagar', "width":12, "rotate": True}, 
			{"name": 'Rut empresa', "width":12, "rotate": True}, 
			{"name": 'N° factura envio', "width":12, "rotate": True},
			{"name": 'N° factura comision', "width":12, "rotate": True},
			{"name": 'N° factura productos', "width":12, "rotate": True},
			# {"name": 'Id ingresos', "width":12, "rotate": True},
			
		]
		

		row_num = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])

		row_num += 1
		for row in rows:
			for i in row.retail_payment_detail.all():
				ws.write(row_num, 0, row.bill_register)
				ws.write(row_num, 1, row.retail_id_number)
				ws.write(row_num, 2, str(row.date_payment))
				ws.write(row_num, 3, row.channel.name)
				ws.write(row_num, 4, i.movement_type)
				ws.write(row_num, 5, i.sale.order_channel)
				ws.write(row_num, 6, i.total_sale)
				ws.write(row_num, 7, i.discount_commission)
				ws.write(row_num, 8, i.discount_shipping)
				ws.write(row_num, 9, i.discount_penalty)
				ws.write(row_num, 10, i.discount_resend)
				ws.write(row_num, 11, i.several_discount)
				ws.write(row_num, 12, i.total_pay)
				ws.write(row_num, 13, row.enterprise_rut)
				ws.write(row_num, 14, row.shipping_bill_number)
				ws.write(row_num, 15, row.commission_bill_number)
				ws.write(row_num, 16, row.product_bill_number)
				row_num += 1
		ws.autofilter(0, 0, 0, len(columns) - 1)
		wb.close()
		output.seek(0)
		filename = 'Informe de Liquidacion.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response

class SalesPaymentXLSX(APIView):
	permission_classes = (AllowAny, )
	def get(self, request):
		output = io.BytesIO()
		kind = self.request.query_params.get("payment_type", None)
		get_date = self.request.query_params.get("date_payment", None)
		date = get_date.split("-")
		selected_date = datetime.date(int(date[0]), int(date[1]), int(date[2]))
		today_sales = Sale.objects.filter(date_sale=selected_date, payment_type=kind, status="entregado")
		if not today_sales:
			return Response(data={"error": ['No existen ventas presenciales para el día de hoy con este método de pago']}, status=status.HTTP_400_BAD_REQUEST)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("Pagos")
		cell_format_data1 = wb.add_format({'bg_color': "yellow", "font_size": 8})
		cell_format_data2 = wb.add_format({"font_size": 8})

		row_num = 0
		ws.write(row_num, 0, "Id Liquidación Asiamerica", cell_format_data1)
		ws.write(row_num, 1, "Id Liquidación Retail", cell_format_data1)
		ws.write(row_num, 2, "Fecha Liquidación", cell_format_data1)
		ws.write(row_num, 3, "Canal", cell_format_data1)
		ws.write(row_num, 4, "Tipo Movimiento", cell_format_data1)
		ws.write(row_num, 5, "Id Venta", cell_format_data1)
		ws.write(row_num, 6, "Total Venta", cell_format_data1)
		ws.write(row_num, 7, "Descuento Comision", cell_format_data1)
		ws.write(row_num, 8, "Descuento envío", cell_format_data1)
		ws.write(row_num, 9, "Descuento Multas", cell_format_data1)
		ws.write(row_num, 10, "Descuento Reenvíos", cell_format_data1)
		ws.write(row_num, 11, "Descuento varios", cell_format_data1)
		ws.write(row_num, 12, "Total a pagar", cell_format_data1)
		ws.write(row_num, 13, "Rut empresa", cell_format_data1)
		ws.write(row_num, 14, "N° Factura envío", cell_format_data1)
		ws.write(row_num, 15, "N° Factura comision", cell_format_data1)
		ws.write(row_num, 16, "N° Factura productos", cell_format_data1)
		ws.write(row_num, 17, "Id Ingresos", cell_format_data1)

		row_num = 1
		for row in today_sales:
			ws.write(row_num, 0, "", cell_format_data2)
			ws.write(row_num, 1, kind , cell_format_data2)
			ws.write(row_num, 2, row.date_sale.strftime("%d-%m-%Y"), cell_format_data2)
			ws.write(row_num, 3, "Presencial", cell_format_data2)
			ws.write(row_num, 4, "Ingreso", cell_format_data2)
			ws.write(row_num, 5, row.id, cell_format_data2)
			ws.write(row_num, 6, row.total_sale, cell_format_data2)
			ws.write(row_num, 7, 0, cell_format_data2)
			ws.write(row_num, 8, 0, cell_format_data2)
			ws.write(row_num, 9, 0, cell_format_data2)
			ws.write(row_num, 10, 0, cell_format_data2)
			ws.write(row_num, 11, 0, cell_format_data2)
			ws.write(row_num, 12, row.total_sale, cell_format_data2)
			ws.write(row_num, 13, "76.431.547-2", cell_format_data2)
			row_num += 1	
		wb.close()
		output.seek(0)
		filename  = "Reporte de ventas presenciales"
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response

class PaymentForecastReportXLSX(APIView):
	permission_classes = (AllowAny,)
	def get(self,request):
		reference_date  = datetime.date.today() - timedelta(days = 7) 
		retail_payment = RetailPayment.objects.filter(date_payment__gte =  reference_date.strftime("%Y-%m-%d"), bank_register__isnull = True)
		new_dict = []
		for h in retail_payment:
			new_dict.append({
				"channel_id": h.channel_id,
				"total":h.total_payment_retail
			})
		group_order = []
		sorted_order = sorted(new_dict, key=itemgetter('channel_id'))
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['channel_id']):
				group_order.append(list(group))
		output = io.BytesIO()
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("datos")
		ws.set_column('A:G',15)
		cell_format_data1 = wb.add_format({'bg_color': "yellow", "font_size": 8, 'border': 1, 'align': 'left'})
		cell_format_data2 = wb.add_format({"font_size": 8, 'border': 1, 'align': 'left'})
		cell_format_data3 = wb.add_format({"font_size": 8, 'num_format': '#,###','border': 1, 'align': 'left'})

		row_num = 0
		ws.write(row_num, 0, "Mercadolibre", cell_format_data1)
		ws.write(row_num, 1, "Ripley", cell_format_data1)
		ws.write(row_num, 2, "Linio", cell_format_data1)
		ws.write(row_num, 3, "Paris", cell_format_data1)
		ws.write(row_num, 4, "Groupon", cell_format_data1)
		ws.write(row_num, 5, "Falabella", cell_format_data1)
		ws.write(row_num, 6, "Total", cell_format_data1)

		days = [0,0,0,0,0,0]
		row_num = 1
		for row in group_order:
			for step in row:
				if row_num == 1:
					days[0] += step['total']
				if row_num == 2:
					days[1] += step['total']
				if row_num == 3:
					days[2] += step['total']
				if row_num == 4:
					days[3] += step['total']    
				if row_num == 5:
					days[4] += step['total']        
				if row_num == 6:
					days[5] += step['total']            
				if step['channel_id'] == 1:
					ws.write(row_num, 0, step['total'], cell_format_data2)
					row_num += 1
				if step['channel_id'] == 3:     
					ws.write(row_num, 2, step['total'], cell_format_data2)
					row_num += 1
				if step['channel_id'] == 5:     
					ws.write(row_num, 3, step['total'], cell_format_data2)
					row_num += 1
				if step['channel_id'] == 4:     
					ws.write(row_num, 4, step['total'], cell_format_data2)
					row_num += 1  
				if step['channel_id'] == 8:     
					ws.write(row_num, 5, step['total'], cell_format_data2)
					row_num += 1                
			row_num = 1
		for y in days: 
			ws.write(row_num, 6, y, cell_format_data2)
			row_num += 1 
		wb.close()
		output.seek(0)
		filename  = "Proyeccion de pagos"
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response

class RetailPaymentXLSX(APIView):
	permission_classes = (AllowAny,)
	def get(self,request):
		output = io.BytesIO()
		bank_report = BankReport.objects.all()
		retail_payment = RetailPayment.objects.all()
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("Liquidaciones")
		ws.set_column('A:I',15)
		cell_format_data1 = wb.add_format({'bg_color': "yellow", "font_size": 8, 'border': 1, 'align': 'left'})
		cell_format_data2 = wb.add_format({"font_size": 8, 'border': 1, 'align': 'left'})
		cell_format_data3 = wb.add_format({"font_size": 8, 'num_format': '#,##', 'border': 1, 'align': 'left'})

		row_num = 0
		ws.write(row_num, 0, "Registro de facturacion", cell_format_data1)
		ws.write(row_num, 1, "Nro de liquidacion", cell_format_data1)
		ws.write(row_num, 2, "Fecha de pago", cell_format_data1)
		ws.write(row_num, 3, "Canal", cell_format_data1)
		ws.write(row_num, 4, "Cartola asociada", cell_format_data1)
		ws.write(row_num, 5, "Total", cell_format_data1)
		ws.write(row_num, 6, "Tipo de pago", cell_format_data1)
		ws.write(row_num, 7, "Estatus de pago", cell_format_data1)
		ws.write(row_num, 8, "Rut", cell_format_data1)
        
		row_num = 1
		for row in retail_payment:
			ws.write(row_num, 0, row.bill_register , cell_format_data2)
			ws.write(row_num, 1, row.retail_id_number , cell_format_data2)
			ws.write(row_num, 2, row.date_payment.strftime("%d-%m-%Y"), cell_format_data2)
			ws.write(row_num, 3, row.channel.name, cell_format_data2)
			if  row.bank_register_id or row.bank_register_id == 0:
				cb =  BankReport.objects.get(id = row.bank_register_id)
				ws.write(row_num, 4, cb.bank_register, cell_format_data2)
			else: 
				cb = "-"
				ws.write(row_num, 4, cb, cell_format_data2)		
			ws.write(row_num, 5, row.total_payment_retail, cell_format_data3)
			ws.write(row_num, 6, row.payment_type, cell_format_data2)
			ws.write(row_num, 7, row.status_payment, cell_format_data2)
			ws.write(row_num, 8, row.enterprise_rut, cell_format_data2)
			row_num += 1	

		wx = wb.add_worksheet("Cartolas")
		wx.set_column('A:E',15)  
		row_num = 0
		wx.write(row_num, 0, "Nro. Cartola", cell_format_data1)
		wx.write(row_num, 1, "Fecha", cell_format_data1)
		wx.write(row_num, 2, "Monto", cell_format_data1)
		wx.write(row_num, 3, "Tipo de movimiento", cell_format_data1)
		wx.write(row_num, 4, "Tipo de pago", cell_format_data1)
		
		row_num = 1
		for row in bank_report:
			wx.write(row_num, 0, row.bank_register, cell_format_data2)
			wx.write(row_num, 1, row.date_report.strftime("%d-%m-%Y"), cell_format_data2)
			wx.write(row_num, 2, row.total_amount, cell_format_data3)
			wx.write(row_num, 3, row.movement_type, cell_format_data2)
			wx.write(row_num, 4, row.payment_type, cell_format_data2)
			row_num += 1	
		wb.close()
		output.seek(0)
		filename  = "Reporte de liquidaciones"
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response

class BankReportXLSX(APIView):
	permission_classes = (AllowAny,)
	def get(self, request):
		# today = date.today()
		retail_payment = self.request.query_params.get('retail_payment', None)
		rows = BankReport.objects.filter(id=int(retail_payment))
		output = io.BytesIO()
		file_name = 'Informe de liquidación retail '
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(retail_payment))
		columns = [
			{"name": 'Item cartola bancaria', "width":12, "rotate": True}, 
			{"name": 'Fecha', "width":12, "rotate": True}, 
			{"name": 'Depósitos y abonos', "width":12, "rotate": True}, 
			{"name": 'Tipo de movimiento', "width":12, "rotate": True}, 
			{"name": 'Tipo pago', "width":12, "rotate": True}, 
			{"name": 'Canal', "width":12, "rotate": True}, 
			{"name": 'Descripción adicional', "width":12, "rotate": True},
			{"name": 'Id Venta', "width":12, "rotate": True},
			{"name": 'Tipo de documento', "width":12, "rotate": True},
			{"name": 'Id liquidación', "width":12, "rotate": True},
			{"name": 'N° de documento tributario', "width":12, "rotate": True}, 
			# {"name": 'Id ingresos', "width":12, "rotate": True},
		]

		row_num = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
		row_num += 1
		for row in rows:
			# for i in row.retail_payment_detail.all():
			ws.write(row_num, 0, row.bank_register)
			ws.write(row_num, 1, str(row.date_report))
			ws.write(row_num, 2, row.total_amount)
			ws.write(row_num, 3, row.movement_type)
			ws.write(row_num, 4, row.payment_type)
			ws.write(row_num, 5, "")
			ws.write(row_num, 6, "")
			ws.write(row_num, 7, "")
			ws.write(row_num, 8, "")
			if row.bank_retail_payment is not None:
				ws.write(row_num, 9, row.bank_retail_payment.bill_register)
			else:
				ws.write(row_num, 9, "")
			ws.write(row_num, 10, "")
			row_num += 1
		ws.autofilter(0, 0, 0, len(columns) - 1)
		wb.close()
		output.seek(0)
		filename = 'Informe_Liquidación.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response

class RetailPaymentsChecked(APIView):
	permission_classes = (IsAuthenticated,)
	def get(self, request):
		week_days = CheckStatusPayment.objects.annotate(monday_date=TruncDay('created_date'), week_number=ExtractWeek('created_date')).values('monday_date', 'week_number').annotate(id=Min('id')).order_by('monday_date')[:4]
		# print(week_days)
		weeks_header = week_days
		channels = Channel.objects.all()
		data_retail_checked = []
		for channel in channels:
			data_checked_channel = []
			for each in week_days:
				start = each['monday_date'] - timedelta(days=each['monday_date'].weekday())
				end = start + timedelta(days=6)
				status_ = CheckStatusPayment.objects.filter(created_date__range=[start,end], channel=channel)
				if status_:
					payment = RetailPayment.objects.filter(date_payment__range=[start,end], channel=channel)
					if payment:
						payment_retail = True
					else:
						payment_retail = False
					status_checked = status_[0].status
					data_checked_channel.append({
						"payment":status_checked,
						"upload":payment_retail,
						"week":each['week_number'],
						"id_checked":status_[0].id
					})
			if data_checked_channel:
				# print(channel)
				data_retail_checked.append({
					"name": channel.name,
					"checked": data_checked_channel,
				})
			else:
				pass
		# print(data_retail_checked)


		data_checked = data_retail_checked
		return Response(data={"weeks_header":weeks_header, "data_checked":data_checked}, status=status.HTTP_200_OK)

	def put(self, request, pk=None):
		# print(request.data)
		r = CheckStatusPayment.objects.filter(id=request.data['id']).update(status=request.data['status'], checked_date=datetime.datetime.now().date())
		return Response(data={"status": ['Estatus cambiado exitosamente']}, status=status.HTTP_200_OK)
