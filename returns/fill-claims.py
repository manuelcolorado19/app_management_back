import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sales.models import Product_Sale, Color, Product_Code, Pack, Pack_Products, Product_SKU_Ripley
from django.db.models import Q
import requests
from returns.models import Claims
import xlsxwriter
wb = load_workbook('returns/files/Mensajeria Ripley.xlsx')
ws = wb['Hoja1']

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		claim = Claims.objects.create(
			order_channel=row[0].value,
			reference=row[1].value,
			subject=row[2].value,
		)
print("Listo")
