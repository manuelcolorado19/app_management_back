from django.db import models
from sales.models import Product_Sale, Sale, Product_Code, Sale_Product
from django.contrib.auth import get_user_model
from django.utils.timezone import now
from django.contrib.postgres.fields import ArrayField

User = get_user_model()

class Claims(models.Model):
	order_channel = models.CharField(max_length=20, null=True, blank=True)
	reference = models.CharField(max_length=80, null=True, blank=True)
	subject = models.CharField(max_length=50, null=True, blank=True)
	response = models.CharField(max_length=250, null=True, blank=True)
	status = models.CharField(max_length=50, null=True, blank=True)

	class Meta:
		db_table = 'claims'

class Client(models.Model):
	name = models.CharField(max_length=50, null=True, blank=True)
	email = models.EmailField(null=True, blank=True)
	phone = models.CharField(null=True, blank=True, max_length=20)

	class Meta:
		db_table = 'clients_returns'

class Courier(models.Model):
	name = models.CharField(max_length=80, blank=True, null=True)
	logo = models.ImageField(null=True, blank=True, upload_to='images/')

	class Meta:
		db_table = 'courier'

class Return(models.Model):
	STATUS_BOX = (
		('intacta', 'intacta'),
		('manipulada', 'manipulada'),
	)
	STATUS_PACKAGE = (
		('intacto', 'intacto'),
		('manipulado', 'manipulado'),
		('deterioro en transporte', 'deterioro en transporte'),
	)
	STATUS_PRODUCT = (
		('intacto', 'intacto'),
		('manipulado', 'manipulado'),
		('con uso sin observación', 'con uso sin observación'),
		('con uso maltratado', 'con uso maltratado'),
	)

	CLIENT_REQUEST = (
		('devolución del dinero', 'devolución del dinero'),
		('cambio del producto', 'cambio del producto'),
		('reparación gratuita', 'reparación gratuita'),
		('retracto', 'retracto'),
		('ninguno', 'ninguno'),
	)

	admission_date = models.DateField(default=now, blank=True)
	user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='return_user')
	courier = models.ForeignKey(Courier, on_delete=models.SET_NULL, blank=True, null=True, related_name='return_courier')
	status_box = models.CharField(choices=STATUS_BOX, default='manipulada', max_length=25, blank=True, null=True)
	status_package = models.CharField(choices=STATUS_PACKAGE, default='intacto', max_length=25, blank=True, null=True)
	status_product = models.CharField(choices=STATUS_PRODUCT, default='intacto', max_length=25, blank=True, null=True)
	client = models.ForeignKey(Client, on_delete=models.SET_NULL, blank=True, null=True, related_name='return_client')
	comment = models.TextField(blank=True, null=True)
	client_request = models.CharField(choices=CLIENT_REQUEST, max_length=40, default='', null=True, blank=True)
	client_comment = models.TextField(blank=True, null=True)
	sale = models.ForeignKey(Sale, on_delete=models.SET_NULL, null=True, blank=True, related_name='return_sale')
	total_products = models.IntegerField(null=True)
	bill_receive = models.BooleanField(default=False)
	missing = models.BooleanField(default=False)
	limit_date = models.DateField(blank=True, null=True)
	technical_report_generate = models.BooleanField(default=False)
	date_created= models.DateTimeField(auto_now_add=True, null=True, blank=True)
	class Meta:
		db_table = 'return'

class Return_Product(models.Model):
	return_sale = models.ForeignKey(Return, on_delete=models.CASCADE, blank=True, null=True, related_name='product_return')
	product_sale_return = models.ForeignKey(Product_Sale, on_delete=models.SET_NULL, blank=True, null=True, related_name='product_sale_return')
	product_sale_code_return = models.ForeignKey(Product_Code, on_delete=models.SET_NULL, blank=True, null=True, related_name='product_sale_code_return')
	sale_product_asociation = models.ForeignKey(Sale_Product, on_delete=models.SET_NULL, blank=True, null=True, related_name='sale_of_product_return')
	list_accesories = models.TextField(blank=True, null=True)
	quantity = models.IntegerField(null=True)
	reintegrate = models.BooleanField(default=False)
	reintegrate_date = models.DateTimeField(auto_now=True)
	class Meta:
		db_table = 'return_product'

class TechnicalReport(models.Model):
	RESOLUTION = (
		('cambio','cambio'),
		('reparación','reparación'),
		('reembolso','reembolso'),
		('devolución','devolución'),
		('sin gestión','sin gestión'),
	)

	# STATUS_PRODUCT_CHANGE = (
	# 	('entra reacondicionado','entra reacondicionado')
	# 	('entra como nuevo','entra como nuevo')
	# 	('entra como perdida','entra como perdida')
	# )
	RETIRED = (
		('si','si'),
		('no','no'),
		('no aplica','no aplica'),
	)
	CLIENT_CONTACT = (
		('si','si'),
		('no','no'),
	)
	FINAL_STATUS = (
		('sin revisión','sin revisión'),
		('pendiente','pendiente'),
		('finalizado','finalizado'),
	)
	return_sale = models.OneToOneField(Return, on_delete=models.CASCADE, blank=True, null=True, related_name='technical_report_return')
	technical_report = models.TextField(blank=True, null=True)
	technical_solution = models.TextField(blank=True, null=True)
	resolution = models.CharField(choices=RESOLUTION, default='', max_length=40, null=True)
	client_contact = models.CharField(choices=CLIENT_CONTACT, default='', max_length=15,null=True)
	retired = models.CharField(choices=RETIRED, default='', max_length=15,null=True)
	final_status = models.CharField(choices=FINAL_STATUS, default='sin revisión', max_length=15,null=True)
	user_created = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='technical_report_user')
	date_created= models.DateTimeField(auto_now_add=True, null=True, blank=True)
	# status_product_change = models.CharField(choices=STATUS_PRODUCT_CHANGE, default='', max_length=80, null=True)

	class Meta:
		db_table = 'technical_report'