from rest_framework import serializers
from .models import Claims, Client, Courier, Return, Return_Product, TechnicalReport
# from sales.serializers import SaleSerializer
from django.db import transaction
from datetime import timedelta  
from sales.models import Sale, Product_Sale, Product_Code, Sale_Product

class ClaimsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Claims
		fields = '__all__'

class SaleReturnSerializer(serializers.ModelSerializer):
	class Meta:
		model = Sale
		fields = '__all__'

class ClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = Client
		fields = '__all__'

class CourierSerializer(serializers.ModelSerializer):
	class Meta:
		model = Courier
		fields = '__all__'

class ProductDescriptionSerializer(serializers.CharField):
	def to_representation(self, value):
		if value.product_sale_return is not None:
			return value.product_sale_return.description
		return '-'

class ProductSKUSerializer(serializers.CharField):
	def to_representation(self, value):
		if value.product_sale_return is not None:
			return value.product_sale_return.sku
		return '-'

class VariationProductReturn(serializers.CharField):
	def to_representation(self, value):
		if value.product_sale_code_return is not None:
			return value.product_sale_code_return.color.description
		return '-'

class VariationCodeSevenReturn(serializers.CharField):
	def to_representation(self, value):
		if value.product_sale_code_return is not None:
			return value.product_sale_code_return.code_seven_digits
		return '-'

class Return_ProductSerializer(serializers.ModelSerializer):
	description = ProductDescriptionSerializer(read_only=True, source='*')
	sku = ProductSKUSerializer(read_only=True,  source='*')
	variation = VariationProductReturn(read_only=True, source='*')
	code_seven_digits = VariationCodeSevenReturn(read_only=True, source='*')
	class Meta:
		model = Return_Product
		fields = '__all__'

class TechnicalReportSerializer(serializers.ModelSerializer):
	class Meta:
		model = TechnicalReport
		fields = '__all__'
		extra_kwargs = {
			"final_status": {
				"read_only": True
			},
			"user_created": {
				"read_only": True
			}
		}

	@transaction.atomic
	def create(self, validated_data):
		if validated_data['resolution'] is not None or validated_data['client_contact'] is not None or validated_data['retired'] is not None:
			validated_data['final_status'] = "pendiente"
		# else:
		# 	validated_data['final_status'] = "sin revisión"
		sale_return = Return.objects.get(id=validated_data['return_sale'].id)
		sale_return.technical_report_generate = True
		sale_return.save()
		validated_data['user_created'] = self.context['request'].user
		return super().create(validated_data)

class ReturnSerializer(serializers.ModelSerializer):
	courier = CourierSerializer(read_only=True)
	courier_id = serializers.IntegerField(write_only=True, allow_null=True)
	order_channel = serializers.IntegerField(write_only=True, allow_null=True)
	sale = SaleReturnSerializer(read_only=True)
	products = serializers.ListField(write_only=True)
	sale_id = serializers.CharField(write_only=True)
	# client_data = serializers.DictField(child=serializers.CharField())
	client = ClientSerializer(allow_null=True)
	product_return = Return_ProductSerializer(read_only=True, many=True)
	technical_report_return = TechnicalReportSerializer(read_only=True)
	class Meta:
		model = Return
		fields = '__all__'


	@transaction.atomic
	def create(self, validated_data):
		if validated_data['courier_id'] is not None:
			validated_data['courier'] = Courier.objects.get(id=validated_data['courier_id'])
		else:
			validated_data['courier'] = None
		validated_data['user'] = self.context['request'].user
		# print(validated_data)
		if validated_data['sale_id'] == "999999X":
			validated_data['sale'] = None
		else:
			validated_data['sale'] = Sale.objects.get(id=validated_data['sale_id'])
		validated_data['limit_date'] = validated_data['admission_date'] + timedelta(days=9)
		products = validated_data.pop('products')
		del validated_data['sale_id']
		del validated_data['courier_id']
		del validated_data['order_channel']
		# print(validated_data)
		# Se crea la devolución
		if validated_data['client'] is not None:
			if validated_data['client'].get('email', None) is not None:
				client_get = Client.objects.filter(email=validated_data['client']['email'])
				if client_get:
					validated_data['client'] = client_get[0]
				else:
					validated_data['client'] = Client.objects.create(
						name=validated_data['client'].get('name',None),
						email=validated_data['client']['email'],
						phone=validated_data['client'].get('phone',None),
					)
			else:
				validated_data['client'] = Client.objects.create(name=validated_data['client']['name'], phone=validated_data['client'].get('phone',None))
			# pass
		return_created = Return.objects.create(**validated_data)
		total_products = 0
		for product in products:
			if product.get('code_seven_digits', None) is not None:
				product_code = Product_Code.objects.get(code_seven_digits=product['code_seven_digits'])
			else:
				product_code = None

			if product.get('id_sale', None) is not None:
				sale_product = Sale_Product.objects.get(id=product['id_sale'])
			else:
				sale_product = None
			return_product = Return_Product.objects.create(
				return_sale=return_created,
				product_sale_return=Product_Sale.objects.get(sku=product['sku']),			
				product_sale_code_return=product_code,			
				list_accesories=product['accesories'],
				quantity=product['quantity'],
				sale_product_asociation=sale_product,
			)
			total_products += int(product['quantity'])
		return_created.total_products = total_products
		return_created.save()
		return return_created
		# return Return.objects.get(id=1)

class TechnicalReportSerializer(serializers.ModelSerializer):
	class Meta:
		model = TechnicalReport
		fields = '__all__'
		extra_kwargs = {
			"final_status": {
				"read_only": True
			},
			"user_created": {
				"read_only": True
			}
		}

	@transaction.atomic
	def create(self, validated_data):
		if validated_data['resolution'] is not None or validated_data['client_contact'] is not None or validated_data['retired'] is not None:
			validated_data['final_status'] = "pendiente"
		# else:
		# 	validated_data['final_status'] = "sin revisión"
		sale_return = Return.objects.get(id=validated_data['return_sale'].id)
		sale_return.technical_report_generate = True
		sale_return.save()
		validated_data['user_created'] = self.context['request'].user
		return super().create(validated_data)

