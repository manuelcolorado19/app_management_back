from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import ClaimsViewSet, ClientViewSet, CourierViewSet, ReturnViewSet, TechnicalReportViewSet

router = DefaultRouter()
router.register(r'claims', ClaimsViewSet, base_name='claims')
router.register(r'clients', ClientViewSet, base_name='clients')
router.register(r'courier', CourierViewSet, base_name='courier')
router.register(r'returns', ReturnViewSet, base_name='returns')
router.register(r'technical-report', TechnicalReportViewSet, base_name='technical-report')

urlpatterns = [
	url(r'^', include(router.urls)),
	
]
