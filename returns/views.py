from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Claims, Client, Courier, Return, Return_Product, TechnicalReport
from .serializers import ClaimsSerializer, ClientSerializer, CourierSerializer, ReturnSerializer, TechnicalReportSerializer

from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle, Spacer, Indenter
from reportlab.graphics.shapes import Line, Drawing
from reportlab.lib.units import inch, mm
from django.http import HttpResponse
from reportlab.pdfgen import canvas
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT
from reportlab.platypus import ListFlowable, ListItem, Image
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table
import io
import os, base64
from rest_framework.response import Response
from rest_framework import status
from io import BytesIO
import locale
from django.http import HttpResponse, FileResponse
from django.db import transaction
from rest_framework.permissions import AllowAny, IsAuthenticated
import datetime
from rest_framework import filters as search_filter


class ClaimsViewSet(ModelViewSet):
	queryset = Claims.objects.all().order_by('id')
	serializer_class = ClaimsSerializer
	pagination_class = None

class ClientViewSet(ModelViewSet):
	queryset = Client.objects.all()
	serializer_class = ClientSerializer
	http_method_names = ['get',]

class CourierViewSet(ModelViewSet):
	queryset = Courier.objects.all()
	serializer_class = CourierSerializer
	# http_method_names = ['get',]

class ReturnViewSet(ModelViewSet):
	queryset = Return.objects.all().order_by('-id')
	serializer_class = ReturnSerializer
	filter_backends = (search_filter.SearchFilter,)
	search_fields  = ('=id', '^sale__order_channel', '^sale__number_document','=product_return__product_sale_return__sku', '=product_return__product_sale_code_return__code_seven_digits')
	# http_method_names = ['get', 'post',]

	def update(self, request, pk=None):
		# print(request.data)
		Return_Product.objects.filter(id=request.data['id']).update(reintegrate=True)
		return Response(data={"return": ['Producto reintegrado de forma exitosa']},status=status.HTTP_200_OK)

	@transaction.atomic
	def create(self, request):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		return_object = Return.objects.get(id=serializer.data['id'])
		base_b4_report = self.generate_reception_report(return_object)
		return Response(data={"pdf_file": base_b4_report, "id": serializer.data['id']}, status=status.HTTP_201_CREATED, headers=headers)

	def generate_reception_report(self, object_data):
	#def destroy(self, request, pk=None):
		return_object = object_data
		#return_object = Return.objects.get(id=18)
		styles = getSampleStyleSheet()
		styles2 = getSampleStyleSheet()
		styleN = styles["Normal"]
		styleN.fontSize = 20
		styleN.leading = 25
		styleN.alignment = TA_CENTER
		styleN1 = styles2["Normal"]
		styleN1.fontSize = 11
		styleN.leading = 45
		styleN1.alignment = TA_CENTER
		im = Image("Asiamerica.png", 2.188*inch, 0.5*inch)
		im.hAlign = 'LEFT'
		
		buff = BytesIO()
		doc = SimpleDocTemplate(buff, pagesize=letter, rightMargin=30, leftMargin=30, topMargin=30, bottomMargin=18,)
		clientes = []
		clientes.append(im)
		top_header = Paragraph("<strong><u>INFORME DE RECEPCIÓN</u></strong>", styleN)
		header = Paragraph("<strong>Cliente debe presentar la boleta, factura u otro documento que acredite la compra.</strong>", styleN1)
		clientes.append(top_header)
		clientes.append(header)
		# clientes.append(header)
		name = return_object.client.name if return_object.client is not None else '-'
		channel_name = return_object.sale.channel.name if return_object.sale is not None else '-'
		order_channel_data = return_object.sale.order_channel if return_object.sale is not None else '-'
		date_sale_data = return_object.sale.date_sale if return_object.sale is not None else '-'
		courier_name = return_object.courier.name if return_object.courier is not None else '-'
		email = return_object.client.email if (return_object.client is not None and return_object.client.email is not None) else '-'
		phone = return_object.client.phone if (return_object.client is not None and return_object.client.phone is not None) else '-'
		document_number = return_object.sale.number_document if (return_object.sale is not None and return_object.sale.number_document is not None) else '-'
		headings1 = ("DATOS DEL CLIENTE", '',)
		data = [('Canal: ' + channel_name , "Courier: " + str(courier_name)), ('Nombre: ' + name , "Fecha de Recepción: " + str(return_object.admission_date)),('N° de Ordén: ' + str(order_channel_data) , "Fecha de Compra: " + str(date_sale_data)),('N° de Documento: ' +str(document_number), "Teléfono: " + phone), ('Correo: ' + email, "N° de Reclamo: " + str(return_object.id))]
		t = Table([headings1,] + data, hAlign='LEFT', colWidths=["50%","50%"], rowHeights=(7*mm,7*mm,7*mm, 7*mm,7*mm,7*mm,), spaceBefore=5*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				('SPAN', (0, 0), (1, 0)),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
				('ALIGN', (0, 0), (0, 0), "CENTER"),
			]
		))
		clientes.append(t)
		products_data = Return_Product.objects.filter(return_sale=return_object).values('product_sale_return__description', 'product_sale_return__sku', 'return_sale__status_box', 'list_accesories', 'return_sale__client_comment', 'quantity')
		list_products = []

		style = ParagraphStyle(
	        name='Normal',
	        spaceBefore = 5,
	        fontSize=9,
	    )
		style.leading = 5
		styleX = getSampleStyleSheet()
		styleX1= styleX["BodyText"]
		styleX1.fontSize = 9
		styleX1.leading = 9
		accesories = []
		j = 1
		text_complete = ''
		for product in products_data:
			product_text = product['product_sale_return__description'] + ", Cantidad: " +str(product['quantity'])
			list_products.append(Paragraph(product_text, style=style,))
			if product['list_accesories'].splitlines():
				product_text = "<strong> Producto " + str(j) + ": </strong>"
				s = len(product['list_accesories'].splitlines()) - 1
				for i, item in enumerate(product['list_accesories'].splitlines()):
					if i == s:
						product_text += item +"."
					else:
						product_text += item + ", "
				text_complete += product_text
			# accesories.append()
			# accesories += product['list_accesories'].splitlines()
			j += 1
		list_accesories = []
		# text_accesories = ''
		# for word in accesories:
		# 	text_accesories += word + ", "
			# list_accesories.append(Paragraph(word, style=styleX1,))
		list_flowable = ListFlowable(list_products)
		list_flowable2 = Paragraph(text_complete, style=styleX1,)
		styles4 = getSampleStyleSheet()
		styleN3 = styles4["BodyText"]
		text_box_package = "Caja " + return_object.status_box.title() + ", Paquete " + return_object.status_package.title()

		headings1 = ("Nombre Producto o SKU", '',)
		data = [(list_flowable, ''),('Estado de la Caja y Paquete: ' + text_box_package, '',),('Producto y accesorios que trael el paquete (hacer lista) (Tomar foto junto a esta ficha)', '',),(list_flowable2, ''),('Sintoma indicado por cliente (Tiempo de Uso, Origen Falla, periodo que si funcionó, otros)', '',), (Paragraph(return_object.client_comment, style=styleN3,), '')]
		t = Table([headings1, data[0], data[1], data[2], data[3], data[4], data[5],] , hAlign='LEFT', colWidths=["100%"], rowHeights=(8*mm,12*mm,9*mm, 8*mm, 25*mm,7*mm,25*mm), spaceBefore=4*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				('SPAN', (0, 0), (-1, 0)),
				('SPAN', (0, 1), (-1, 1)),
				('SPAN', (0, 2), (-1, 2)),
				('SPAN', (0, 3), (-1, 3)),
				('SPAN', (0, 4), (-1, 4)),
				('SPAN', (0, 5), (-1, 5)),
				('SPAN', (0, 6), (-1, 6)),
				('VALIGN',(0,0),(-1,-1),'TOP'),
				('LINEABOVE', (0, 1), (-1, 1), 0, colors.white),
				('LINEABOVE', (0, 4), (-1, 4), 0, colors.white),
				('LINEABOVE', (0, 6), (-1, 6), 0, colors.white),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
			]
		))
		clientes.append(t)
		styles3 = getSampleStyleSheet()
		styleN2 = styles3["BodyText"]
		styleN2.fontSize = 9
		styleN2.leading = 9
		note = Paragraph('El derecho de garantía legal solo corresponde cuando el producto es defectuoso, le faltan piezas o partes, no es apto para el uso que fue destinado, que fue anteriormente arreglado pero sus deficiencias persisten o presenta nuevas fallas.', styleN2)
		note2 = Paragraph('Una vez recepcionado el producto, la empresa tiene un plazo máximo de 10 días, para dar una respuesta al cliente.', styleN2)
		headings1 = ("Cliente solicita", '',)
		return_money = '__X__' if return_object.client_request == 'devolución del dinero' else '______'
		change_product = '__X__' if return_object.client_request == 'cambio del producto' else '______'
		free_repair = '__X__' if return_object.client_request == 'reparación gratuita' else '______'
		retract = '__X__' if return_object.client_request == 'retracto' else '______'
		none_client = '__X__' if return_object.client_request == 'ninguno' else '______'
		data = [('Devolución del dinero ' + return_money , 'Cambio del producto ' +change_product,),('Reparación gratuita '+free_repair, 'Retracto (Solo 10 días Hábiles) '+retract,) ,('Ninguno ' +none_client, '',),(note, '',), (note2, '',)]
		t = Table([headings1, data[0], data[1], data[2], data[3], data[4]] , hAlign='LEFT', colWidths=["100%"], rowHeights=(6*mm,7*mm, 7*mm, 7*mm, 9*mm, 9*mm ), spaceBefore=3*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				('LINEABOVE', (0, 1), (-1, -1), 0, colors.white),
				('LINEBEFORE', (1, 1), (-1, -1), 0, colors.white),
				('SPAN', (0, 0), (-1, 0)),
				('SPAN', (0, 3), (-1, 3)),
				('SPAN', (0, 4), (-1, 4)),
				# ('SPAN', (0, 2), (-1, 2)),
				('VALIGN',(0,0),(-1,-1),'TOP'),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
			]
		))
		clientes.append(t)
		d = Drawing(300, 70)
		d.add(Line(85, 0, 190, 0))
		e = Drawing(800, 0)
		e.add(Line(355, 0, 460, 0))
		
		clientes.append(d)
		clientes.append(e)
		pageTextStyleCenter = ParagraphStyle(name="left", alignment=TA_CENTER, fontSize=8, leading=4)
		tbl_data = [
		    [Paragraph("<strong><u>Nombre y Firma</u></strong>", pageTextStyleCenter), Paragraph("<strong><u>Nombre y Firma</u></strong>", pageTextStyleCenter),],
		    [Paragraph("<strong><u>(Cliente o de quien Entrega)</u></strong>", pageTextStyleCenter), Paragraph("<strong><u>(Proveedor o de quien recibe)</u></strong>", pageTextStyleCenter),],
		]
		tbl = Table(tbl_data)
		clientes.append(tbl)
		doc.build(clientes)
		#response = HttpResponse(buff.getvalue(), content_type='application/pdf')
		#response['Content-Disposition'] = 'attachment; filename="' + "hola.pdf" + '"'
		#return response
		report_encoded = base64.b64encode(buff.getvalue())
		return report_encoded

class TechnicalReportViewSet(ModelViewSet):
	queryset = TechnicalReport.objects.all().order_by('-id')
	serializer_class = TechnicalReportSerializer
	permission_classes = (IsAuthenticated,)
	http_method_names = ['get', 'post', 'put', 'delete']

	@transaction.atomic
	def create(self, request):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		return_object = TechnicalReport.objects.get(id=serializer.data['id'])
		base_b4_report = self.generate_reception_report(return_object)
		return Response(data={"pdf_file": base_b4_report, "id": serializer.data['id']}, status=status.HTTP_201_CREATED, headers=headers)

	def generate_reception_report(self, return_object):
		return_object = return_object
		styles = getSampleStyleSheet()
		styles2 = getSampleStyleSheet()
		styleN = styles["Normal"]
		styleN.fontSize = 20
		styleN.leading = 25
		styleN.alignment = TA_CENTER
		styleN1 = styles2["Normal"]
		styleN1.fontSize = 11
		styleN.leading = 45
		styleN1.alignment = TA_CENTER
		im = Image("Asiamerica.png", 2.188*inch, 0.5*inch)
		im.hAlign = 'LEFT'
		
		buff = BytesIO()
		doc = SimpleDocTemplate(buff, pagesize=letter, rightMargin=30, leftMargin=30, topMargin=30, bottomMargin=18,)
		clientes = []
		clientes.append(im)
		top_header = Paragraph("<strong><u>INFORME TECNICO</u></strong>", styleN)
		
		clientes.append(top_header)
		name = return_object.return_sale.client.name if return_object.return_sale.client is not None else '-'
		order_channel = return_object.return_sale.sale.order_channel if return_object.return_sale.sale.order_channel is not None else '-'
		
		headings1 = ("DATOS DEL CLIENTE", '',)
		data = [('Nombre: ' + name , "N° de Ordén: " + str(order_channel)), ('N° Caso: ' + str(return_object.id) , "Fecha de Compra: " + str(return_object.return_sale.sale.date_sale)),]
		t = Table([headings1,] + data, hAlign='LEFT', colWidths=["50%","50%"], rowHeights=(7*mm,7*mm,7*mm,), spaceBefore=5*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				('SPAN', (0, 0), (1, 0)),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
				('ALIGN', (0, 0), (0, 0), "CENTER"),
			]
		))
		clientes.append(t)
		user_data = return_object.user_created.first_name + " " + return_object.user_created.last_name
		headings1 = ("DATOS DE LA TIENDA", '',)
		data = [('Nombre: ' + "ASIAMERICA LTDA" , "Responsable: " + user_data), ('Teléfono: ' + "+56 9 3214 6174" , "Fecha de Recepción: " + str(return_object.return_sale.admission_date)),]
		t = Table([headings1,] + data, hAlign='LEFT', colWidths=["50%","50%"], rowHeights=(7*mm,7*mm,7*mm,), spaceBefore=5*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				('SPAN', (0, 0), (1, 0)),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
				('ALIGN', (0, 0), (0, 0), "CENTER"),
			]
		))
		clientes.append(t)


		products_data = return_object.return_sale.product_return.all().values('product_sale_return__description','quantity')
		# print(products_data)
		list_products = []

		style = ParagraphStyle(
	        name='Normal',
	        spaceBefore = 5,
	        fontSize=9,
	    )
		style.leading = 5
		styleX = getSampleStyleSheet()
		styleX1= styleX["BodyText"]
		styleX1.fontSize = 9
		styleX1.leading = 9
		accesories = []
		j = 1
		text_complete = ''
		for product in products_data:
			product_text = product['product_sale_return__description'] + ", Cantidad: " +str(product['quantity'])
			list_products.append(Paragraph(product_text, style=style,))
			j += 1
		list_flowable = ListFlowable(list_products)
		

		headings1 = ("Productos:", '',)
		data = [(list_flowable, ''),]
		t = Table([headings1, data[0], ] , hAlign='LEFT', colWidths=["100%"], rowHeights=(6*mm,17*mm,), spaceBefore=4*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				('SPAN', (0, 0), (-1, 0)),
				('SPAN', (0, 1), (-1, 1)),
				# ('SPAN', (0, 2), (-1, 2)),
				# ('SPAN', (0, 3), (-1, 3)),
				# ('SPAN', (0, 4), (-1, 4)),
				# ('SPAN', (0, 5), (-1, 5)),
				# ('SPAN', (0, 6), (-1, 6)),
				('VALIGN',(0,0),(-1,-1),'TOP'),
				('LINEABOVE', (0, 1), (-1, 1), 0, colors.white),
				('LINEABOVE', (0, 4), (-1, 4), 0, colors.white),
				('LINEABOVE', (0, 6), (-1, 6), 0, colors.white),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
			]
		))
		clientes.append(t)
		styles4 = getSampleStyleSheet()
		styleN3 = styles4["BodyText"]
		headings1 = ("Estado del Producto:", '',)
		data = [(Paragraph(return_object.return_sale.status_product.title(), style=styleN3), ''),]
		t = Table([headings1, data[0], ] , hAlign='LEFT', colWidths=["100%"], rowHeights=(6*mm,12*mm,), spaceBefore=4*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				('SPAN', (0, 0), (-1, 0)),
				('SPAN', (0, 1), (-1, 1)),
				# ('SPAN', (0, 2), (-1, 2)),
				# ('SPAN', (0, 3), (-1, 3)),
				# ('SPAN', (0, 4), (-1, 4)),
				# ('SPAN', (0, 5), (-1, 5)),
				# ('SPAN', (0, 6), (-1, 6)),
				('VALIGN',(0,0),(-1,-1),'TOP'),
				('LINEABOVE', (0, 1), (-1, 1), 0, colors.white),
				('LINEABOVE', (0, 4), (-1, 4), 0, colors.white),
				('LINEABOVE', (0, 6), (-1, 6), 0, colors.white),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
			]
		))
		clientes.append(t)
		headings1 = ("Sintoma indicado por cliente:", 'Informe del Técnico',)
		data = [(Paragraph(return_object.return_sale.client_comment, style=styleN3), Paragraph(return_object.technical_report, style=styleN3)),]
		t = Table([headings1, data[0], ] , hAlign='LEFT', colWidths=["35%", "65%"], rowHeights=(6*mm,35*mm,), spaceBefore=4*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				# ('SPAN', (0, 0), (-1, 0)),
				# ('SPAN', (0, 1), (-1, 1)),
				# ('SPAN', (0, 2), (-1, 2)),
				# ('SPAN', (0, 3), (-1, 3)),
				# ('SPAN', (0, 4), (-1, 4)),
				# ('SPAN', (0, 5), (-1, 5)),
				# ('SPAN', (0, 6), (-1, 6)),
				('VALIGN',(0,0),(-1,-1),'TOP'),
				('LINEABOVE', (0, 1), (-1, 1), 0, colors.white),
				('LINEABOVE', (0, 4), (-1, 4), 0, colors.white),
				('LINEABOVE', (0, 6), (-1, 6), 0, colors.white),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
			]
		))
		clientes.append(t)

		headings1 = ("Solución Técnica:", '',)
		data = [(Paragraph(return_object.technical_solution, style=styleN3,),),]
		t = Table([headings1, data[0], ] , hAlign='LEFT', colWidths=["100%"], rowHeights=(6*mm,29*mm,), spaceBefore=4*mm)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (6, -1), 0.7, colors.black),
				('SPAN', (0, 0), (-1, 0)),
				('SPAN', (0, 1), (-1, 1)),
				# ('SPAN', (0, 2), (-1, 2)),
				# ('SPAN', (0, 3), (-1, 3)),
				# ('SPAN', (0, 4), (-1, 4)),
				# ('SPAN', (0, 5), (-1, 5)),
				# ('SPAN', (0, 6), (-1, 6)),
				('VALIGN',(0,0),(-1,-1),'TOP'),
				('LINEABOVE', (0, 1), (-1, 1), 0, colors.white),
				('LINEABOVE', (0, 4), (-1, 4), 0, colors.white),
				('LINEABOVE', (0, 6), (-1, 6), 0, colors.white),
				('BACKGROUND', (0, 0), (-1, 0), colors.white),
				('FONTSIZE', (0, 0), (-1, -1), 12), 
			]
		))
		clientes.append(t)

		
		# d = Drawing(300, 70)
		# d.add(Line(85, 0, 190, 0))
		e = Drawing(800, 70)
		e.add(Line(430, 0, 535, 0))
		
		# clientes.append(d)
		clientes.append(e)
		pageTextStyleCenter = ParagraphStyle(name="left", alignment=TA_LEFT, fontSize=8, leading=4)
		tbl_data = [
		    [Paragraph("<strong>Fecha Informe: " +str(return_object.date_created.date())+ "</strong>", pageTextStyleCenter), Paragraph("<strong> Firma</strong>", pageTextStyleCenter),],
		]
		tbl = Table(tbl_data, colWidths=["80%", "20%"],)
		clientes.append(tbl)
		doc.build(clientes)
		# response = HttpResponse(buff.getvalue(), content_type='application/pdf')
		# response['Content-Disposition'] = 'attachment; filename="' + "hola.pdf" + '"'
		# return response

		report_encoded = base64.b64encode(buff.getvalue())
		return report_encoded

