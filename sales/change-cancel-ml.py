import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sales.models import Product_Sale, Color, Product_Code, Pack, Pack_Products, Product_SKU_Ripley, Sale
from django.db.models import Q
from notifications.models import Token
import xlsxwriter
import pandas as pd
from multiprocessing.dummy import Pool as ThreadPool
import requests

token = Token.objects.get(id=1).token_ml
url = "https://api.mercadolibre.com/orders/search?seller=216244489&access_token="
url = url + str(token) + "&order.date_created.from=2018-12-01T00:00:00.000-00:00&order.date_created.to=2019-02-15T23:00:00.000-00:00&order.status=cancelled"
data = requests.get(url).json()
if data['paging']['total'] <= 50:
	rows = data["results"]
else:
	rows = []
	count = 0
	iteration_end = int(data['paging']['total']/50)
	module = int(data['paging']['total']%50)
	for i in range(iteration_end + 1):
		new_rows = requests.get(url + "&offset=" + str(i*50)).json()['results']
		rows = rows + new_rows
	rows = rows + requests.get(url + "&offset="+str(((iteration_end*50) + module))).json()['results']

for row in rows:
	sale = Sale.objects.filter(order_channel=row["id"], channel__name="Mercadolibre")
	if sale:
		sale = Sale.objects.get(order_channel=row["id"])
	else:
		continue
	print("Cancelada " + str(row["id"]))
	sale.status = "cancelado"
	sale.shipping_status = "Cancelado"
	sale.save()
