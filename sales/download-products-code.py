import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sales.models import Product_Sale, Color, Product_Code, Pack, Pack_Products, Product_SKU_Ripley
from django.db.models import Q
from notifications.models import Token
import xlsxwriter
import pandas as pd
from multiprocessing.dummy import Pool as ThreadPool
list_branch_offices_bulk = {"sku":[], "description":[],"category":[], "code_seven_digits":[],"color":[]}

products = Product_Sale.objects.all()
for product in products:
	if product.product_code.all():
		for code in product.product_code.all():
			list_branch_offices_bulk['sku'].append(code.product.sku)
			list_branch_offices_bulk['description'].append(code.product.description)
			list_branch_offices_bulk['category'].append(code.product.category_product_sale.name)
			list_branch_offices_bulk['code_seven_digits'].append(code.code_seven_digits)
			list_branch_offices_bulk['color'].append(code.color.description)
	else:
		list_branch_offices_bulk['sku'].append(product.sku)
		list_branch_offices_bulk['description'].append(product.description)
		if product.category_product_sale is not None:
			categoria = product.category_product_sale.name
		else:
			categoria = '-'
		list_branch_offices_bulk['category'].append(categoria)
		list_branch_offices_bulk['code_seven_digits'].append('-')
		list_branch_offices_bulk['color'].append('-')
	print("Done")

new_dataframe = pd.DataFrame.from_dict(list_branch_offices_bulk, orient='index').transpose()
new_dataframe.to_excel("Listado de SKU y colores.xlsx")
