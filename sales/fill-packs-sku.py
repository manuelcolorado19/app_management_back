import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
# from sellers.models import Seller
from sales.models import Product_Sale, Color, Product_Code, Category_Product_Sale, Pack, Pack_Products
from django.db.models import Q
# wb = load_workbook('sales/files/Compra 2.xlsx')
wb = load_workbook('sales/files/Packs.xlsx')
ws = wb['SKU Mercado Libre']
count = 0
#for row in ws.rows:
#	count += 1
#	if count > 1:
#		print(str(row[4].value))
#		if Product_Sale.objects.filter(sku=int(row[4].value)).count() == 0:
#			print("No existe el SKU " + str(row[4].value))
#fill_packs()

#def fill_packs():
count = 0
for row in ws.rows:
	count += 1
	if count > 1:

		# if Product_Sale.objects.filter(Q(sku=int(row[0].value)) | Q(description__startswith=row[1].value[0:9])):
		# 	continue
		# else:
		# 	print("No existe sku :" + str(int(row[0].value)))
		if Pack.objects.filter(sku_pack=int(row[0].value)):
			pack = Pack.objects.get(sku_pack=int(row[0].value))

			pack_products = Pack_Products.objects.create(
				pack=pack,
				product_pack=Product_Sale.objects.get(sku=int(row[4].value)),
				quantity=int(row[3].value),
				percentage_price=int(row[5].value * 100),
			)
		else:
			pack = Pack.objects.create(
				sku_pack=int(row[0].value),
				description=str(row[1].value).split(": ",1)[1]
			)
			pack_products = Pack_Products.objects.create(
					pack=pack,
					product_pack=Product_Sale.objects.get(sku=int(row[4].value)),
					quantity=int(row[3].value),
					percentage_price=int(row[5].value * 100),
			)

print("Packs created")
