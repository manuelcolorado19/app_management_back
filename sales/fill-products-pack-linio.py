import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sales.models import Product_Sale, Color, Product_Code, Pack, Pack_Products, Product_SKU_Ripley
from django.db.models import Q
from categories.models import Channel
wb = load_workbook('sales/files/Listado de precios y ofertas Linio.xlsx')
ws = wb['Hoja1']
# count = 0
# for row in ws.rows:
# 	count += 1
# 	if count > 1:
# 		if Pack.objects.filter(sku_pack=str(row[1].value)).count() == 0 and Product_Sale.objects.filter(sku=str(row[1].value)).count() == 0:
# 			print("No existe el SKU " + str(row[1].value))

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		product_ripley = Product_SKU_Ripley.objects.filter(sku_ripley=str(row[0].value))
		if product_ripley:
			continue
		else:
			pack = Pack.objects.filter(
				sku_pack=str(row[1].value),
			)
			if pack:
				pack = pack[0]
				product = None
			else:
				product = Product_Sale.objects.filter(sku=int(str(row[1].value)))
				if product:
					product = product[0]
					pack = None

			sku_ripley = Product_SKU_Ripley.objects.create(
				pack_ripley=pack,
				product_ripley=product,
				description=str(row[2].value),
				code_seven_digits=None,
				sku_ripley=str(row[0].value),
				channel=Channel.objects.get(name="Linio"),
			)

# count = 0
# for row in ws.rows:
# 	count += 1
# 	if count > 1:
# 		product_ripley = Product_SKU_Ripley.objects.filter(sku_ripley=row[0].value)
# 		if product_ripley:
# 			pack = product_ripley[0].pack_ripley
# 			color = Color.objects.filter(description=row[9].value)
# 			if color:
# 				color = Color.objects.get(description=row[9].value)
# 			else:
# 				if row[9].value is not None:
# 					color = Color.objects.create(
# 						description=row[9].value
# 					)
# 				else:
# 					color = None
# 			pack_products = Pack_Products.objects.create(
# 				pack=pack,
# 				product_pack=Product_Sale.objects.get(sku=int(row[6].value)),
# 				quantity=int(row[7].value),
# 				percentage_price=int(row[8].value * 100),
# 				color=color
# 			)
# 		else:
# 			pack = Pack.objects.create(
# 				sku_pack=row[0].value,
# 				description=str(row[1].value)
# 			)
# 			color = Color.objects.filter(description=row[9].value)
# 			if color:
# 				color = Color.objects.get(description=row[9].value)
# 			else:
# 				# print(row[2].value)
# 				if row[9].value is not None:
# 					color = Color.objects.create(
# 						description=row[9].value
# 					)
# 				else:
# 					color = None
# 			sku_ripley = Product_SKU_Ripley.objects.create(
# 				pack_ripley=pack,
# 				color=color,
# 				description=str(row[1].value),
# 				sku_ripley=str(row[0].value),
# 			)
# 			pack_products = Pack_Products.objects.create(
# 				pack=pack,
# 				product_pack=Product_Sale.objects.get(sku=int(row[6].value)),
# 				quantity=int(row[7].value),
# 				percentage_price=int(row[8].value * 100),
# 				color=color
# 			)
print("Packs linio created")