import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sales.models import Product_Sale, Color, Product_Code, Pack, Pack_Products, Product_SKU_Ripley
from django.db.models import Q
from categories.models import Channel
wb = load_workbook('sales/files/Listado de precios y ofertas Mercadolibre.xlsx')
ws = wb['Mercadolibre']

# count = 0
# for row in ws.rows:
# 	count += 1
# 	if count > 1:
# 		if Pack.objects.filter(sku_pack=str(row[0].value)).count() == 0 and Product_Sale.objects.filter(sku=str(row[0].value)).count() == 0:
# 			print("No existe el SKU " + str(row[0].value))

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		product_ripley = Product_SKU_Ripley.objects.filter(code=str(row[2].value))
		if product_ripley:
			continue
		else:
			pack = Pack.objects.filter(
				sku_pack=str(row[0].value),
			)
			if pack:
				pack = pack[0]
				product = None
			else:
				product = Product_Sale.objects.filter(sku=int(str(row[0].value)))
				if product:
					product = product[0]
					pack = None

			sku_ripley = Product_SKU_Ripley.objects.create(
				pack_ripley=pack,
				product_ripley=product,
				description=str(row[1].value),
				code_seven_digits=None,
				code=str(row[2].value),
				sku_ripley=str(row[0].value),
				channel=Channel.objects.get(name="Mercadolibre"),
			)
print("Packs mercadolibre created")