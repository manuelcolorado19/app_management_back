# from products.models import Product
# import requests
# from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
# from sellers.models import Seller
from sales.models import Product_Sale, Color, Product_Code, Category_Product_Sale
wb = load_workbook('sales/files/Compra 2.xlsx')
wb2 = load_workbook('sales/files/Categorias.xlsx')
# ws = wb['SKU']
ws = wb['CODIGOS ETIQUETAS']
ws2 = wb2['SKU']
colors = []
count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		if Color.objects.filter(description=row[2].value):
			continue
		color = Color.objects.create(
			description=row[2].value,			
		)
		# colors.append(row[2].value)
print("Colores creados")

count2 = 0
for row2 in ws2.rows:
	count2 += 1
	if count2 > 1:
		if Category_Product_Sale.objects.filter(name=row2[2].value):
			continue
		category = Category_Product_Sale.objects.create(
			name=row2[2].value,
			sku_start=str(row2[0].value)[0] + str(row2[0].value)[1]
		)
		# colors.append(row[2].value)

print("Categorias creados")

# colors = Color.objects.all()

count = 0
for row in ws.rows:
	count +=1
	if count > 1:
		if Product_Sale.objects.filter(sku=row[1].value):
			product_code = Product_Code.objects.create(
				product=Product_Sale.objects.get(sku=row[1].value),
				color=Color.objects.get(description=row[2].value),
				quantity=row[5].value,
				code_seven_digits=row[3].value
			)
		else:
			category_product_sale = Category_Product_Sale.objects.get(sku_start=(str(row[1].value)[0] + str(row[1].value)[1]))
			product_sale = Product_Sale.objects.create(
				sku=row[1].value,
				category_product_sale=category_product_sale,
				description=row[4].value,
				stock=9999,
				price=row[6].value,
			)
			product_code = Product_Code.objects.create(
				product=product_sale,
				color=Color.objects.get(description=row[2].value),
				# category_product_sale=None,
				quantity=row[5].value,
				code_seven_digits=row[3].value
			)
		
print("Products created")

