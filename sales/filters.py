# import rest_framework_filters as filters
# from categories.models import Channel
from .models import Sale, Sale_Product
import django_filters

# from django_filters import rest_framework as filters


class SalesFilter(django_filters.FilterSet):
    sale_of_product__product_code = django_filters.BooleanFilter(field_name='sale_of_product__product_code', lookup_expr='isnull', exclude=True)
    number_document = django_filters.BooleanFilter(field_name='number_document', lookup_expr='isnull', exclude=True)
    sale_of_product = django_filters.BooleanFilter(field_name='sale_of_product', lookup_expr='isnull', exclude=True)

    class Meta:

        model = Sale
        fields = '__all__'

class SalesFilterByDate(django_filters.FilterSet):
	date_update_date = django_filters.DateTimeFilter(field_name='date_update', lookup_expr='date')
	class Meta:
		model = Sale
		fields = '__all__'