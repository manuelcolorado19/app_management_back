import requests
import datetime
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail, EmailMessage
import base64
import os
from io import BytesIO

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		subject = 'Ventas revisadas'
		from_email = 'mejiasabelito@gmail.com'
		to = ["juan.ureta@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl"]
		# to = ["mejiasabelito@gmail.com",]
		message = "Buen día ya he revisado las ventas del día de Hoy. Buen día a todos."
		send_mail(
			subject,
			message,
			'mejiasabelito@gmail.com',
			to,
			fail_silently=False,
		)
