import requests
import datetime
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail, EmailMessage
import base64
import os
from io import BytesIO
from notifications.models import Token
from sales.models import Sale
from categories.models import Channel
from datetime import timedelta
from django.contrib.auth import get_user_model
from django.db import transaction
User = get_user_model()
token = Token.objects.get(id=1).token_ml

def nth_repl(s, sub, repl, nth):
    find = s.find(sub)
    # if find is not p1 we have found at least one match for the substring
    i = find != -1
    # loop util we find the nth or we find no match
    while find != -1 and i != nth:
        # find + 1 means we start at the last match start index + 1
        find = s.find(sub, find + 1)
        i += 1
    # if i  is equal to nth we found nth matches so replace
    if i == nth:
        return s[:find]+repl+s[find + len(sub):]
    return s
class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def status(self, x):
		return {
			"paid":"Pagado",
			"cancelled":"Cancelado",
			"confirmed":"Confirmado",
			"payment_in_process":"Pago en proceso",
			"payment_required":"Pago requerido",
			"invalid":"Invalido",
		}[x]
	def status_shipping(self, x):
		return {
			'delivered':"Entregado",
			'shipped':"Enviado",
			'pending':"Pendiente",
			'ready_to_ship':"Listo para enviar",
			'handling':"En Manejo",
			'not_delivered':"No entregado",
			'not_verified':"No verificado",
			'cancelled':"Cancelado",
			'closed':"Cerrado",
			'to_be_agreed':None,
			'error':"Error",
			'active':"Activo",
			'not_specified':"No especificado",
			'stale_ready_to_ship':"Estancado en listo para enviar",
			'stale_shipped':"Estancado en enviado",
			'to_be_agreed':"Por acordar",
		}[x]

	def add_arguments(self, parser):
		parser.add_argument('order_channel', nargs='+', type=int)

	@transaction.atomic
	def handle(self, *args, **options):
		today = datetime.datetime.today().date()
		order_channel = options['order_channel'][0]
		if Sale.objects.filter(order_channel=order_channel).count() == 0:
			url_ml = "https://api.mercadolibre.com/orders/" + str(order_channel) + "?access_token=" + token
			mercadolibre_data = requests.get(url_ml).json()
			# print(mercadolibre_data['total_amount'])
			if mercadolibre_data['shipping']['status'] != 'to_be_agreed':
				shipping_tracking = requests.get("https://api.mercadolibre.com/shipments/" + str(mercadolibre_data['shipping']['id']) + "?access_token=" +token).json()['tracking_number']
				if shipping_tracking is not None:
					shipping_tracking = int(shipping_tracking)
				shipping_type = 'con etiqueta'
				shipping_status = self.status_shipping(mercadolibre_data['shipping']['status'])
				shipping_type_sub_status=''
			else:
				shipping_type = 'por acordar'
				shipping_type_sub_status = 'pendiente'
				shipping_tracking = None
				shipping_status = None
			date_sale = datetime.datetime.strptime(nth_repl(mercadolibre_data['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.%f%z") + timedelta(hours=1)
			comment = mercadolibre_data['order_items'][0]['item']['title']
			if mercadolibre_data['order_items'][0]['item']['seller_custom_field'].endswith("99"):
				comment += " REACONDICIONADA"
			sale = Sale.objects.create(
				order_channel=mercadolibre_data['id'],
				total_sale=0,
				user=User.objects.get(username='api'),
				channel=Channel.objects.get(name='Mercadolibre'),
				document_type="sdt",
				payment_type="transferencia retail",
				sender_responsable="comprador" if mercadolibre_data['shipping']['status'] == 'to_be_agreed' else "vendedor",
				status='pendiente',
				channel_status=self.status(mercadolibre_data['status']),
				comments=comment,
				date_sale=date_sale,
				tracking_number=shipping_tracking,
				shipping_type=shipping_type,
				shipping_status=shipping_status,
				shipping_type_sub_status=shipping_type_sub_status,
				last_user_update=User.objects.get(username='api'),
				hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >=10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >=10 else ('0' + str(date_sale.minute)))),
			)
			print("Venta creada sin problemas")
