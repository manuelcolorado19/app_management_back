import requests
import datetime
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail, EmailMessage
import base64
import os
from io import BytesIO
from OperationManagement.settings import USERNAME_TEST, PASSWORD_TEST
import json

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		data_access = {"username":USERNAME_TEST, "password":PASSWORD_TEST}
		get_token_access = requests.post("http://127.0.0.1:8000/login/", data=data_access).json()['token']
		# print(get_token_access)
		headers = {"Authorization": "Bearer " + get_token_access}
		dls = "http://localhost:8000/export/sales/xlsx?all_columns=hola"
		resp = requests.get(dls, headers=headers)
		file_name = "Reporte de Ventas " + datetime.datetime.now().strftime("%Y%m%d%H%M") + ".xlsx"
		path_save = os.path.expanduser('~/Documents/Reports/' + file_name)
		# file_name_path = 'products/files/Ordenes ' + str(today) + '.xlsx'
		output = open(path_save, 'wb')
		output.write(resp.content)
		output.close()
		# print("File downloaded and send")


