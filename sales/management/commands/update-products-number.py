from sales.models import Sale
from django.db.models import Sum
import datetime
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		sales = Sale.objects.filter(date_created__gte=datetime.date(2019,7,2))
		for sale in sales:
			products_count = sale.sale_of_product.exclude(product__sku=1).aggregate(item_number=Sum('quantity'))['item_number']
			if products_count is not None:
				new_sale = Sale.objects.filter(id=sale.id).update(total_number_of_product=products_count)
			else:
				new_sale = Sale.objects.filter(id=sale.id).update(total_number_of_product=0)
		# print("end")