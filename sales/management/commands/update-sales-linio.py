import requests
from sales.models import Sale, Sale_Product, Product_SKU_Ripley, Product_Sale, Product_Code, Pack, Pack_Products, CommissionChannel, Commission
import datetime
from datetime import timedelta
from OperationManagement.settings import API_KEY_RP, API_KEY_LINIO, USER_ID
from categories.models import Channel
from django.contrib.auth import get_user_model
from django.db import transaction
from django.core.management.base import BaseCommand, CommandError
import urllib
from hashlib import sha256
from django.db.models import F, Case, When, CharField, Value, IntegerField, DurationField, Q, Sum, Value, Count
from django.db.models.functions import Cast, Concat
from django.db.models.fields import DateField, FloatField
from hmac import HMAC
from django.db.models.functions.text import Substr
import math
from django.core.mail import send_mail

User = get_user_model()

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'
	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl", "abel.mejias@asiamerica.cl","mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code = Product_Code.objects.filter(code_seven_digits=each)[0]
				message = product_code.product.description + " Variante: " + product_code.color.description + "(" + str(each) + ")"
			else:
				product_sale = Product_Sale.objects.filter(sku=each)[0]
				message = product_sale.description + "(" + str(each) + ")"
			message_append.append(message)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)
	def roundup(self,x):
		return int(math.ceil(x / 100.0)) * 100

	def f(self, x):
		if type(x) == list:
			status = x[0]
		else:
			status = x
		return {
	        'pending': "Pendiente",
	        'canceled': "Cancelado",
	        'ready_to_ship': "Listo para Enviar",
	        'delivered': "Entregado",
	        'returned': "Devuelto",
	        'shipped': "Enviado",
	        'failed': "Fallido",
			'return_shipped_by_customer': "Devolución enviada por comprador",
			'return_waiting_for_approval': "Devolución esperando por aprobación",
	    }[status]

	# @transaction.atomic
	def handle(self, *args, **options):
		url_linio = "https://sellercenter-api.linio.cl?"
		#date_from = datetime.datetime.strptime("09-04-2019", '%d-%m-%Y')
		#date_to = datetime.datetime.strptime("11-04-2019", '%d-%m-%Y')

		parameters = {
		  'UserID': USER_ID,
		  'Version': '1.0',
		  'Action': 'GetOrders',
		  'Format':'JSON',
		  'Timestamp':datetime.datetime.now().isoformat(),
		  'CreatedAfter': (datetime.datetime.now() - timedelta(days=30)).isoformat(),
		  #'CreatedAfter': date_from.isoformat(),
		  'CreatedBefore': (datetime.datetime.now()).isoformat()
		  #'CreatedBefore': date_to.isoformat()
		}
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))
		parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))

		url_request = url_linio + concatenated
		#print(url_request)
		data = requests.get(url_request).json()
		#print()
		rows = data["SuccessResponse"]["Body"]["Orders"]["Order"]
		if type(rows) != list:
			rows = [rows]
		# print(rows)
		products_stock = []
		for row in rows:
			# print(type(row))
			if Sale.objects.filter(order_channel=int(row['OrderNumber'])):
				sale = Sale.objects.get(order_channel=int(row['OrderNumber']))
				status_sale = self.f(row['Statuses']['Status'])
				if status_sale != sale.channel_status:
					sale.channel_status = status_sale
					sale.save()
				if sale.tracking_number == None:
					parameters = {
			  			'UserID': USER_ID,
			  			'Version': '1.0',
			  			'Action': 'GetOrderItems',
			  			'OrderId': row['OrderId'],
			  			'Format':'JSON',
			  			'Timestamp': datetime.datetime.now().isoformat(),
					}
					concatenated = urllib.parse.urlencode(sorted(parameters.items()))
					parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
					concatenated = urllib.parse.urlencode(sorted(parameters.items()))

					url_request = url_linio + concatenated
					print(url_request)
					data_order = requests.get(url_request).json()["SuccessResponse"]["Body"]["OrderItems"]["OrderItem"]
					if type(data_order) == list:
						tracking_number = data_order[0]['TrackingCode']
						if tracking_number != '':
							sale.tracking_number = tracking_number
							sale.save()
					else:
						tracking_number = data_order['TrackingCode']
						if tracking_number != '':
							sale.tracking_number = tracking_number
							sale.save()
				continue
			parameters = {
			  'UserID': USER_ID,
			  'Version': '1.0',
			  'Action': 'GetOrderItems',
			  'OrderId': row['OrderId'],
			  'Format':'JSON',
			  'Timestamp': datetime.datetime.now().isoformat(),
			}
			concatenated = urllib.parse.urlencode(sorted(parameters.items()))
			parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
			concatenated = urllib.parse.urlencode(sorted(parameters.items()))

			url_request = url_linio + concatenated
			#print(url_request)

			data_order = requests.get(url_request).json()["SuccessResponse"]["Body"]["OrderItems"]["OrderItem"]

			date_sale = datetime.datetime.strptime(row['CreatedAt'], "%Y-%m-%d %H:%M:%S")

			hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
			if type(data_order) == list:
				tracking_number = data_order[0]['TrackingCode']
				if tracking_number == '':
					tracking_number = None
			else:
				tracking_number = data_order['TrackingCode']
				if tracking_number == '':
					tracking_number = None
			sale = Sale.objects.create(
				order_channel=int(row['OrderNumber']),
				total_sale=0,
				declared_total=0,
				revenue=0,
				user=User.objects.get(username='api-linio'),
				channel=Channel.objects.get(name="Linio"),
				channel_status=self.f(row['Statuses']['Status']),
				document_type="sdt",
				payment_type='transferencia retail',
				sender_responsable="vendedor",
				status="pendiente",
				tracking_number=tracking_number,
				last_user_update=User.objects.get(username='api-linio'),
				date_sale=date_sale,
				#cut_inventory="I1",
				hour_sale_channel=hour_sale_channel,
			)
			# unit_price = int(float(row['Price']) / int(row['ItemsCount']))
			total = 0
			comments = ""
			total_products = 0
			if type(data_order) == list:
				# Si posee mas productos en el carrito se itera cada uno para luego iterar nuevamente cada producto (si es pack) o no
				shipping = 0
				for product in data_order:
					shipping += float(product['ShippingAmount'])
				total_sale_no_shipping = int(float(row['Price'])) - shipping
				for product in data_order:
					#commission = None
					#percentage_commission = SKUCategoryChannel.objects.filter(sub_category_channel__channel_id=5).filter(Q(product_sale__sku=product['Sku'][:5]) | Q(pack__sku_pack=product['Sku'][:5]))
					#if percentage_commission:
					#	commission = percentage_commission[0].sub_category_channel.commission
					#comments += product['Name'] + "/"
					#if Product_Sale.objects.filter(sku=product['Sku'][:5]):
					#	products =  Product_Sale.objects.filter(sku=product['Sku'][:5]).annotate(pack_ripley__pack_product__product_pack__sku=F('sku'), pack_ripley__pack_product__quantity=Value(1, output_field=IntegerField()), pack_ripley__pack_product__percentage_price=Value(100, output_field=IntegerField())).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price')
					#else:
					products = Product_SKU_Ripley.objects.filter(sku_ripley=product['ShopSku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price', 'color__description', 'pack_ripley__id')
					print(products)
					#print(product['ShopSku'])
					for each in products:
						if each.get('color__description', None) is not None:
							product_code = Product_Code.objects.filter(product__sku=each['pack_ripley__pack_product__product_pack__sku'])
							if product_code.count() == 1:
								product_code = product_code[0]
							else:
								code_seven = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6,6)).filter(code_seven__gte=50, product__sku=each['pack_ripley__pack_product__product_pack__sku'], color__description=each['color__description'])
								if code_seven:
									product_code = code_seven[0]
								else:
									product_code = None
						else:
							product_code = None
						quantity = ((int(row['ItemsCount'])/len(data_order)) * each['pack_ripley__pack_product__quantity'])
						unit_price = (float(product['ItemPrice']) * (each['pack_ripley__pack_product__percentage_price']/100))/(quantity)
						if product['Variation'] is not None:
							comment_unit= product['Name'] + " " + product['Variation']
							comments += product['Name'] + " " +product['Variation'] + "/"
						else:
							comment_unit = product['Name']
							comments += product['Name'] + "/"
						if ((unit_price * quantity) > total_sale_no_shipping):
							unit_price = total_sale_no_shipping/quantity
						else:
							if not unit_price.is_integer():
								unit_price = self.roundup(unit_price)
								total_sale_no_shipping -= (unit_price * quantity)

						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=each['pack_ripley__pack_product__product_pack__sku']),
							product_code=product_code,
							sale=sale,
							quantity=quantity,
							unit_price=unit_price,
							total=(quantity * unit_price),
							#from_pack=Pack.objects.get(id=each['pack_ripley__id']),
							comment_unit=comment_unit,
						)
						#if commission is not None:
						#	calculate = (commission * sale_product.total) / 100
						#else:
						#	calculate = 0
						#shipping_assigned = shipping * ((sale_product.total * 100)/(float(product['ItemPrice'])) / 100)
						#commission_ = Commission.objects.create(
						#	sale_product=sale_product,
						#	api_commission=0,
						#	calculate_commission=calculate,
						#	discount_commission=0,
						#	shipping_price_assigned=shipping_assigned,
						#)
						quantity_stock = Product_Sale.objects.get(sku=each['pack_ripley__pack_product__product_pack__sku']).stock - (quantity)
						Product_Sale.objects.filter(sku=each['pack_ripley__pack_product__product_pack__sku']).update(stock=quantity_stock)
						if product_code is not None:
							quantity_stock_color = product_code.quantity - (quantity)
							Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
							if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not (product_code.code_seven_digits in products_stock)):
								products_stock.append(product_code.code_seven_digits)
						else:
							if quantity_stock <= 5 and quantity_stock > 0 and (not (each['pack_ripley__pack_product__product_pack__sku'] in products_stock)):
								products_stock.append(each['pack_ripley__pack_product__product_pack__sku'])
						total += (quantity * unit_price)
						total_products += quantity

					
				shipping = int(shipping)
			else:
				total_sale_no_shipping = int(float(row['Price'])) - int(float(data_order['ShippingAmount']))
				commission = None
				#percentage_commission = CommissionChannel.objects.filter(channel_id=5).filter(Q(product_sale__sku=data_order['Sku'][:5]) | Q(pack__sku_pack=data_order['Sku'][:5]))
				#print(percentage_commission)
				#if percentage_commission:
				#	commission = percentage_commission[0].percentage_commission
				#print(commission)
				#comments += data_order['Name'] + "/"
				#if Product_Sale.objects.filter(sku=data_order['Sku'][:5]):
				#	products =  Product_Sale.objects.filter(sku=data_order['Sku'][:5]).annotate(pack_ripley__pack_product__product_pack__sku=F('sku'), pack_ripley__pack_product__quantity=Value(1, output_field=IntegerField()), pack_ripley__pack_product__percentage_price=Value(100, output_field=IntegerField())).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price')
				#else:
				products = Product_SKU_Ripley.objects.filter(sku_ripley=data_order['ShopSku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price', 'color__description', 'pack_ripley__id')
				print(products)
				if data_order['Variation'] is not None:
					comments += data_order['Name'] + " " +data_order["Variation"] + "/"
				else:
					comments += data_order['Name'] + "/"
				for each in products:
					print(each)
					if each.get('color__description', None) is not None:
						product_code = Product_Code.objects.filter(product__sku=each['pack_ripley__pack_product__product_pack__sku'])
						if product_code.count() == 1:
							product_code = product_code[0]
						else:
							code_seven = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6, 6)).filter(code_seven__gte=50, product__sku=each['pack_ripley__pack_product__product_pack__sku'], color__description=each['color__description'])
							if code_seven:
								product_code = code_seven[0]
							else:
								product_code = None
					else:
						product_code = None
					quantity = int(row['ItemsCount']) * each['pack_ripley__pack_product__quantity']
					unit_price = (float(data_order['ItemPrice'])* (each['pack_ripley__pack_product__percentage_price']/100))/(quantity)
					if ((unit_price * quantity) > total_sale_no_shipping):
						unit_price = total_sale_no_shipping/quantity
					else:
						if not unit_price.is_integer():
							unit_price = self.roundup(unit_price)
							total_sale_no_shipping -= (unit_price * quantity)
					if data_order['Variation'] is not None:
						comment_unit = data_order['Name'] + " " + data_order['Variation']
						#comments += data_order['Name'] + " " + data_order['Variation'] + "/"
					else:
						comment_unit = data_order['Name']
						#comments += data_order['Name'] + "/"
					sale_product = Sale_Product.objects.create(
						product=Product_Sale.objects.get(sku=each['pack_ripley__pack_product__product_pack__sku']),
						product_code=product_code,
						sale=sale,
						quantity=quantity,
						unit_price=unit_price,
						total=(quantity * unit_price),
						#from_pack=Pack.objects.get(id=each['pack_ripley__id']),
						comment_unit=comment_unit,
					)
					#if commission is not None:
					#	calculate = (commission * sale_product.total) / 100
					#else:
					#	calculate = 0
					#shipping_assigned = int(float(data_order['ShippingAmount'])) * ((sale_product.total * 100)/(float(data_order['ItemPrice'])) / 100)
					#commission_ = Commission.objects.create(
					#	sale_product=sale_product,
					#	api_commission=0,
					#	calculate_commission=calculate,
					#	discount_commission=0,
					#	shipping_price_assigned=shipping_assigned,
					#)
					quantity_stock = Product_Sale.objects.get(sku=each['pack_ripley__pack_product__product_pack__sku']).stock - (quantity)
					Product_Sale.objects.filter(sku=each['pack_ripley__pack_product__product_pack__sku']).update(stock=quantity_stock)
					if product_code is not None:
						quantity_stock_color = product_code.quantity - (quantity)
						Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
						if quantity_stock_color <= 5 and quantity_stock_color >= 0 and (not (product_code.code_seven_digits in products_stock)):
							products_stock.append(product_code.code_seven_digits)
					else:
						if quantity_stock <= 5 and quantity_stock >= 0 and (not (each['pack_ripley__pack_product__product_pack__sku'] in products_stock)):
							products_stock.append(each['pack_ripley__pack_product__product_pack__sku'])
					total += (quantity * unit_price)
					total_products += quantity
				if comments == '':
					comments += data_order['Name'] + "/"
				shipping = int(float(data_order['ShippingAmount']))
			sale_product = Sale_Product.objects.create(
				product=Product_Sale.objects.get(sku=1),
				sale=sale,
				quantity=1,
				unit_price=shipping,
				total=shipping,
			)
			total += shipping
			sale.total_sale = total
			sale.total_number_of_product = total_products
			sale.declared_total = total - shipping
			sale.comments = comments
			sale.save()
		if products_stock:
			self.alert_stock(products_stock)
		print("Ventas actualizadas")

