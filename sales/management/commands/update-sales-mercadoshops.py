from django.core.management.base import BaseCommand, CommandError
from sales.models import Sale, Sale_Product, Product_SKU_Ripley, Product_Sale, Product_Code, Pack, Pack_Products
import datetime
from datetime import timedelta
from notifications.models import Token
from django.db import transaction
import requests
from django.contrib.auth import get_user_model
from categories.models import Channel
from django.db.models import F, Value, IntegerField
import math
from django.core.mail import send_mail
from django.db.models.functions.text import Substr


User = get_user_model()
class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl", "abel.mejias@asiamerica.cl","mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(products_sku) + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def roundup(self, x):
		return int(math.ceil(x / 100.0)) * 100

	
	# @transaction.atomic
	def handle(self, *args, **options):
		now = datetime.datetime.now()
		date_from = (now + timedelta(days=-5,hours=-6))
		#date_from = datetime.date(2018,8,1)
		#date_to = datetime.date(2018,11,30)
		date_to = now
		# print(date_from)
		# print(date_to)
		token = Token.objects.get(id=1).token_ml
		url_mshop = "https://api.mercadoshops.com/v1/shops/216244489/orders/search?access_token="+ token +"&channel=mshops"
		data = requests.get(url_mshop).json()
		if data['paging']['total'] <= 50:
			rows = data["results"]
		else:
			rows = []
			count = 0
			iteration_end = int(data['paging']['total']/50)
			module = int(data['paging']['total']%50)
			for i in range(iteration_end + 1):
				new_rows = requests.get(url_mshop + "&offset=" + str(i*50)).json()['results']
				rows = rows + new_rows
			rows = rows + requests.get(url_mshop + "&offset="+str(((iteration_end*50) + module))).json()['results']
		# print(len(rows))
		for row in rows:
			if Sale.objects.filter(order_channel=row['id'], channel__name="Mercadoshop"):
				continue
			else:
				date_creation =  datetime.datetime.strptime(row['creation_date'], "%Y-%m-%dT%H:%M:%S.000-04:00")
				if date_creation > date_to or date_creation < date_from:
					continue
				# print(row['products'][0]['sku'])
				date_sale = datetime.datetime.strptime(row['creation_date'], "%Y-%m-%dT%H:%M:%S.000-04:00")
				hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
				sale = Sale.objects.create(
					order_channel=row['id'],
					total_sale=0,
					declared_total=0,
					revenue=0,
					user=User.objects.get(username='api-mercadoshop'),
					channel=Channel.objects.get(name="Mercadoshop"),
					document_type="sdt",
					payment_type='transferencia retail',
					sender_responsable="vendedor",
					status="pendiente",
					tracking_number=None,
					date_sale=date_sale,
					hour_sale_channel=hour_sale_channel,
					last_user_update=User.objects.get(username='api-mercadoshop'),
					shipping_type="por acordar",
					shipping_type_sub_status='pendiente',
				)
				comments = ""
				total = 0
				revenue = 0
				total_sale_no_shipping = row['grand_total']
				for row2 in row['products']:
					# row2['sku'] = "52039" if not row2['sku'].isdigit() else row2['sku']


					sku_filter = Product_SKU_Ripley.objects.filter(sku_ripley=row2['sku'])
					if sku_filter:
						if sku_filter[0].pack_ripley is not None:
							products = Pack_Products.objects.filter(pack=sku_filter[0].pack_ripley).values("product_pack__sku", "quantity", "percentage_price")
						else:
							products = Product_Sale.objects.filter(id=sku_filter[0].product_ripley.id).annotate(product_pack__sku=F('sku'), quantity=Value(1, output_field=IntegerField()), percentage_price=Value(100, output_field=IntegerField())).values('product_pack__sku', 'quantity', 'percentage_price')
					else:
						products = []



					comments += row2['title'] + " - "
					for product in products:
						# if len(products) == 1:
						# 	unit_price = (row2['price'] * (product['percentage_price']/100))/(row2['quantity'] * product['quantity'])
						# else:
						unit_price = ((row2['unit_price'] * (product['percentage_price']/100))/(row2['quantity'] * product['quantity'])) * (row2['quantity'])
						if (unit_price * row2['quantity'] * product['quantity']) > total_sale_no_shipping:
							unit_price = total_sale_no_shipping/(row2['quantity'] * product['quantity'])
						else:
							if not unit_price.is_integer():
								unit_price = self.roundup(unit_price)
								total_sale_no_shipping -= (unit_price * row2['quantity'] * product['quantity'])


						product_code = None
						if Product_Code.objects.filter(product__sku=product['product_pack__sku']).count() == 1:
							product_code = Product_Code.objects.get(product__sku=product['product_pack__sku'])
						else:
							if sku_filter[0].product_ripley is not None:
								if sku_filter[0].code_seven_digits is not None:
									product_code = Product_Code.objects.filter(code_seven_digits=sku_filter[0].code_seven_digits)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								else:
									product_code = None
							else:
								if sku_filter[0].color is not None:
									product_code=Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6,6)).filter(code_seven__gte=50, product__sku=product['product_pack__sku'],color__description=sku_filter[0].color.description)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								else:
									product_code = None
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=product['product_pack__sku']),
							product_code=product_code,
							sale=sale,
							quantity=row2['quantity'] * product['quantity'],
							unit_price=unit_price,
							total=(row2['quantity'] * product['quantity'] * unit_price),
							comment_unit=row2['title'],
							# from_pack=pack,
						)
						quantity_stock = Product_Sale.objects.get(sku=product['product_pack__sku']).stock - 1
						Product_Sale.objects.filter(sku=product['product_pack__sku']).update(stock=quantity_stock)
						if product_code is not None:
							quantity_stock_color = product_code.quantity - 1
							Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
							if quantity_stock_color <= 5 and quantity_stock_color > 0:
								products_stock.append(product_code.code_seven_digits)
						else:
							if quantity_stock <= 5 and quantity_stock > 0:
								products_stock.append(product['product_pack__sku'])
						total += (row2['quantity'] * product['quantity'] * unit_price)
				sale.total_sale = total
				sale.declared_total = total
				sale.comments = comments
				sale.save()
		print("Ventas mercadoshop actualizadas")
