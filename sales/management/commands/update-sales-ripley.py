import requests
from sales.models import Sale, Sale_Product, Product_SKU_Ripley, Product_Sale, Product_Code, Pack, Pack_Products, CommissionChannel, Commission
import datetime
from datetime import timedelta
from django.db.models import F, Case, When, CharField, Value, IntegerField, DurationField, Q, Sum, Value, Count
from OperationManagement.settings import API_KEY_RP, ACCOUNT_SID, AUTH_TOKEN 
#from OperationManagement.settings import API_KEY_RP 
from categories.models import Channel
from django.contrib.auth import get_user_model
from django.db import transaction
from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string
from django.core.mail import send_mail
import math
from django.db.models.functions.text import Substr

User = get_user_model()


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'
	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl", "abel.mejias@asiamerica.cl","mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code = Product_Code.objects.filter(code_seven_digits=each)[0]
				message = product_code.product.description + " Variante: " + product_code.color.description + "(" +str(each) + ")"
			else:
				product_sale = Product_Sale.objects.filter(sku=each)[0]
				message = product_sale.description + "(" + str(each) + ")"
			message_append.append(message)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def status(self, x):
		return {
			'RECEIVED': "Recibido",
			'CLOSED': "Cerrado",
			'SHIPPED': "Enviado",
			'SHIPPING': "Enviando",
			'WAITING_DEBIT_PAYMENT': "Pago de débito en espera",
			'WAITING_ACCEPTANCE': "Esperando aceptación",
			'STAGING': "En Andamiaje",
			'CANCELED': "Cancelada",
		}[x]
	def send_email_notification(self, order_channel, total_products):
		if total_products == None:
			subject = 'hello'
			from_email = 'info@asiamerica.cl'
			to = ["info@asiamerica.cl", "juan.ureta@asiamerica.cl", "pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl"]
			msg_html = render_to_string('ripley-express-sale.html',
				{
					"order_channel": str(order_channel),
				}
			)
			send_mail(
				'Venta Express Ripley',
				subject,
				from_email,
				to,
				html_message=msg_html,
			)
		else:
			subject = 'hello'
			from_email = 'info@asiamerica.cl'
			to = ["juan.ureta@asiamerica.cl",]
			msg_html = render_to_string('ripley-label-request.html',
				{
					"order_channel": str(order_channel),
					"total_products": str(total_products),
				}
			)
			send_mail(
				'Solicitud de etiqueta',
				subject,
				from_email,
				to,
				html_message=msg_html,
			)
	def roundup(self, x):
		return int(math.ceil(x / 100.0)) * 100

	# @transaction.atomic
	def handle(self, *args, **options):
		url_rp = "https://ripley-prod.mirakl.net"
		headers = {'Accept': 'application/json', 'Authorization': API_KEY_RP}
#<<<<<<< Updated upstream
#		now = datetime.datetime.now()
#		date_from = "2019-02-17T12:00:00Z"
#		date_to = "2019-02-17T20:30:00Z"
		# date_from = (now + timedelta(hours=-10)).strftime("%Y-%m-%dT00:%M:%SZ")
		# date_to = now.strftime("%Y-%m-%dT23:00:%SZ")
#=======
		now = datetime.datetime.now() + timedelta(days=1)
		#date_from = "2018-08-01T12:00:00Z"
		#date_to = "2018-11-30T23:00:00Z"
		date_from = (now + timedelta(days=-2)).strftime("%Y-%m-%dT00:%M:%SZ")
		date_to = now.strftime("%Y-%m-%dT23:58:%SZ")
		#date_from = datetime.datetime.strptime("17-12-2018", '%d-%m-%Y').strftime("%Y-%m-%dT00:%M:%SZ")
		#date_to = datetime.datetime.strptime("19-12-2018", '%d-%m-%Y').strftime("%Y-%m-%dT23:30:%SZ")
		print(date_from)
		#print(date_to)
#>>>>>>> Stashed changes
		url_request = url_rp + "/api/orders?start_date="+ date_from +"&end_date="+ date_to
		max_count = requests.get(url_request, headers=headers).json()['total_count']
		url_request = url_rp + "/api/orders?start_date="+ date_from +"&end_date="+ date_to + "&paginate=false" 
		data = requests.get(url_request, headers=headers).json()
		rows = data['orders']
		products_stock = []
		for row in rows:
			# print(row['commercial_id'])
			if Sale.objects.filter(order_channel=row['commercial_id'].split("-")[0]):
				sale = Sale.objects.get(order_channel=row['commercial_id'].split("-")[0])
				status_sale = self.status(row['order_state'])
				if status_sale != sale.channel_status:
					sale.channel_status = status_sale
					sale.save()
					#if "Despacho Express" in sale.comments and sale.channel_status == 'Enviando':
						#self.send_email_notification(sale.order_channel, None)
					#if "Multibulto" in sale.comments and sale.channel_status == 'Enviando':
					#	self.send_email_notification(row['order_id'], sale.total_number_of_product)
				if (str(sale.tracking_number) != row['shipping_tracking']) and (not ("Despacho Express" in sale.comments)):
					#print(row['order_id'])
					sale.tracking_number = row['shipping_tracking']
					sale.save()
				continue
			else:
				date_sale = datetime.datetime.strptime(row['created_date'], "%Y-%m-%dT%H:%M:%SZ") + timedelta(hours=-4)
				hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
				if row['shipping_tracking'] is not None:
					row['shipping_tracking'] = None if not row['shipping_tracking'].isdigit() else row['shipping_tracking']
				sale = Sale.objects.create(
					order_channel=row['commercial_id'].split("-")[0],
					total_sale=0,
					declared_total=0,
					revenue=0,
					user=User.objects.get(username='api-ripley'),
					channel=Channel.objects.get(name="Ripley"),
					channel_status=self.status(row['order_state']),
					document_type="sdt",
					payment_type='transferencia retail',
					sender_responsable="vendedor",
					status="pendiente",
					tracking_number=row['shipping_tracking'],
					last_user_update=User.objects.get(username='api-ripley'),
					date_sale=date_sale,
					hour_sale_channel=hour_sale_channel,
				)
				total = 0
				revenue = 0
				total_products = 0
				comments = ''
				total_sale_no_shipping = row['price']
				for row2 in row['order_lines']:

					#sku_filter = Product_SKU_Ripley.objects.filter(sku_ripley=row2['offer_sku'])
					#if sku_filter:
					#	if sku_filter[0].pack_ripley is not None:
					#		products = Pack_Products.objects.filter(pack=sku_filter[0].pack_ripley).values("product_pack__sku", "quantity", "percentage_price")
						#else:
						#	products = Product_Sale.objects.filter(id=sku_filter[0].product_ripley.id).annotate(product_pack__sku=F('sku'), quantity=Value(1, output_field=IntegerField()), percentage_price=Value(100, output_field=IntegerField())).values('product_pack__sku', 'quantity', 'percentage_price')
					#else:
					#	products = []
					products = Product_SKU_Ripley.objects.filter(sku_ripley=row2['offer_sku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price','color__description', 'pack_ripley__sku_pack')
					#if products:
					#	products = products
					#else:
					#	pack = Pack.objects.filter(sku_pack=row2['offer_sku'])
					#	if pack:
					#		products = Pack_Products.objects.filter(pack__sku_pack=row2['offer_sku']).annotate(pack_ripley__pack_product__product_pack__sku=F('product_pack__sku'), pack_ripley__pack_product__quantity=F('quantity'), pack_ripley__pack_product__percentage_price=F('percentage_price'))
					#	else:
					#		products = Product_Sale.objects.filter(sku=row2['offer_sku'][:5]).annotate(pack_ripley__pack_product__product_pack__sku=F('sku'), pack_ripley__pack_product__quantity=Value(1, output_field=IntegerField()), pack_ripley__pack_product__percentage_price=Value(100, output_field=IntegerField()))
					print(products)
					#print(row2['offer_sku'])
					for product in products:
						if len(products) == 1:
							unit_price = (row2['price'] * (product['pack_ripley__pack_product__percentage_price']/100))/(row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						else:
							unit_price = (row2['price_unit'] * (product['pack_ripley__pack_product__percentage_price']/100))/(row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						if (unit_price * row2['quantity'] * product['pack_ripley__pack_product__quantity']) > total_sale_no_shipping:
							unit_price = total_sale_no_shipping/(row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						else:
							if not unit_price.is_integer():
								unit_price = self.roundup(unit_price)
								total_sale_no_shipping -= (unit_price * row2['quantity'] * product['pack_ripley__pack_product__quantity'])

						if product['color__description'] is not None:
							comments += row2['product_title'] + " (Modelo: " +product['color__description']+ ") -"
						else:
							comments += row2['product_title'] + " -"
						product_code = Product_Code.objects.filter(product__sku=product['pack_ripley__pack_product__product_pack__sku'])
						if product_code.count() == 1:
							product_code = Product_Code.objects.get(id=product_code[0].id)
						else:
							#if row2['offer_sku'].isdigits() and len(row2['offer_sku']) == 7 and products.count() == 1):
							#	product_code = Product_Code.objects.filter(code_seven_digits=row2['offer_sku'])
							#	if product_code:
							#		product_code = product_code[0]
							#else:
							product_code_50 = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits',6,6)).filter(product__sku=product['pack_ripley__pack_product__product_pack__sku'], code_seven__gte=50, color__description=product['color__description'])
							if product_code_50:
								product_code = product_code_50[0]
							else:
								product_code = None
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=product['pack_ripley__pack_product__product_pack__sku']),
							product_code=product_code,
							sale=sale,
							quantity=(row2['quantity'] * product['pack_ripley__pack_product__quantity']),
							unit_price=unit_price,
							total=(row2['quantity'] * product['pack_ripley__pack_product__quantity'] * unit_price),
							# from_pack=Pack.objects.get(id=product['pack_ripley__id']),
							comment_unit=row2['product_title'],
						)




						#comissions_api = row2['total_commission']
						#unit_price_shipping = (row2['total_price'] * (product['pack_ripley__pack_product__percentage_price']/100))/(row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						#commission = comissions_api * (((unit_price_shipping * 100)/(row2['total_price'])) / 100)
						#shipping_assigned = row['shipping_price'] * (((sale_product.total * 100)/(row['price'])) / 100)
						#percentage_commission_channel = SKUCategoryChannel.objects.filter(product_sale=sale_product.product, sub_category_channel__channel__id=3)
						#if percentage_commission_channel:
						#	commission_calculate = unit_price_shipping * (percentage_commission_channel[0].sub_category_channel.commission / 100)
						#else:
						#	commission_calculate = 0
						#commission_ = Commission.objects.create(
						#	sale_product=sale_product,
						#	api_commission=commission,
						#	calculate_commission=0,
						#	shipping_price_assigned=shipping_assigned,
						#)
						# percentage_product_commission_calculate = commision_calculate * (((sale_product.total * 100) / (order_data['order_items'][0]['quantity'] * order_data['order_items'][0]['unit_price']))/100)




						quantity_stock = Product_Sale.objects.get(sku=product['pack_ripley__pack_product__quantity']).stock - (row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						Product_Sale.objects.filter(sku=product['pack_ripley__pack_product__product_pack__sku']).update(stock=quantity_stock)
						if product_code is not None:
							quantity_stock_color = product_code.quantity - (row2['quantity'] * product['pack_ripley__pack_product__quantity'])
							Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
							if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not (product_code.code_seven_digits in products_stock)):
								products_stock.append(product_code.code_seven_digits)
						else:
							if quantity_stock <= 5 and quantity_stock >= 0 and (not (product['pack_ripley__pack_product__product_pack__sku'] in products_stock)):
								products_stock.append(product['pack_ripley__pack_product__product_pack__sku'])
						total += (row2['quantity'] * product['pack_ripley__pack_product__quantity'] * unit_price)
						revenue += (row2['quantity'] * product['pack_ripley__pack_product__quantity'] * unit_price) - (row['total_commission'] * (product['pack_ripley__pack_product__percentage_price']/100))/len(row['order_lines'])
						total_products += (row2['quantity'] * product['pack_ripley__pack_product__quantity'])
					if row2['shipping_price'] == 0:
						pass
					else:
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=1),
							sale=sale,
							quantity=1,
							unit_price=row2['shipping_price'],
							total=row2['shipping_price'],
						)
						total += row2['shipping_price']
				sale.total_sale = total
				sale.declared_total = total
				sale.total_number_of_product = total_products
				sale.revenue = revenue + row['shipping_price']
				#if "Despacho 24 Horas Hábiles" in row['shipping_type_label'] or "Despacho Domicilio - Despacho 3 Hrs Express" in row['shipping_type_label']:
				#	comments += " Despacho Express "
					#client = Client(ACCOUNT_SID, AUTH_TOKEN)
					#body_message = "Te notificamos que se ha generado una venta express del canal Ripley, con número de ordén " + str(sale.order_channel)
					# receiver = ['whatsapp:+56976846957', 'whatsapp:+56956301999', 'whatsapp:+56995398995', 'whatsapp:+56944253964']
				if sale.total_number_of_product > 1 or len(row['order_lines']) > 1:
					comments += " Multibulto "
					#self.send_email_notification(row['order_id'], total_products)
				sale.comments = comments
				sale.save()
		if products_stock:
			self.alert_stock(products_stock)

		print("Ventas actualizadas.")
