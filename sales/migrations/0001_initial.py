# Generated by Django 2.1.2 on 2018-11-19 19:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('categories', '0003_auto_20181024_1739'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Product_Sale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sku', models.IntegerField()),
                ('description', models.CharField(max_length=140)),
                ('stock', models.IntegerField()),
                ('price', models.DecimalField(decimal_places=1, max_digits=10)),
                ('code', models.CharField(max_length=10)),
            ],
            options={
                'db_table': 'product_sale',
            },
        ),
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_channel', models.IntegerField()),
                ('total', models.DecimalField(decimal_places=2, max_digits=10)),
                ('document_type', models.CharField(blank=True, choices=[('boleta', 'boleta'), ('factura', 'factura'), ('sin documento tributario', 'sin documento tributario')], max_length=40, null=True)),
                ('number_document', models.IntegerField()),
                ('payment_type', models.CharField(choices=[('efectivo', 'efectivo'), ('tarjeta de debito', 'tarjeta de debito'), ('tarjeta de credito', 'tarjeta de credito'), ('transferencia', 'transferencia'), ('cheque', 'cheque'), ('por pagar', 'por pagar'), ('sin pago', 'sin pago'), ('transferencia retail', 'transferencia retail')], max_length=40)),
                ('comments', models.TextField(max_length=200)),
                ('sender_responsable', models.CharField(choices=[('comprador', 'comprador'), ('vendedor', 'vendedor'), ('retira personalmente', 'retira personalmente')], max_length=40)),
                ('status', models.CharField(choices=[('pendiente', 'pendiente'), ('enviado', 'enviado'), ('cancelado', 'cancelado'), ('cambiado', 'cambiado'), ('devuelto', 'devuelto')], max_length=40)),
                ('status_product', models.CharField(choices=[('vendido', 'vendido'), ('muestra', 'muestra'), ('merma', 'merma')], max_length=40)),
                ('date_sale', models.DateTimeField(auto_now_add=True)),
                ('channel', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='categories.Channel')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'sales',
            },
        ),
        migrations.CreateModel(
            name='Sale_Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('total', models.DecimalField(decimal_places=2, max_digits=10)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sales.Product_Sale')),
                ('sale', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sales.Sale')),
            ],
            options={
                'db_table': 'sale_product_pivote',
            },
        ),
    ]
