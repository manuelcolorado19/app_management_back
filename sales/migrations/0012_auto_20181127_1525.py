# Generated by Django 2.1.2 on 2018-11-27 18:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0011_auto_20181127_1345'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale',
            name='date_created',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='sale',
            name='date_sale',
            field=models.DateField(auto_now_add=True),
        ),
    ]
