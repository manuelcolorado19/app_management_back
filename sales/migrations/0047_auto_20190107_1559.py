# Generated by Django 2.1.2 on 2019-01-07 15:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0046_sale_shipping_type_sub_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sale',
            name='shipping_type_sub_status',
            field=models.CharField(blank=True, choices=[('retiro presencial', 'retiro presencial'), ('pendiente', 'pendiente'), ('envío por pagar', 'envío por pagar')], default='', max_length=30, null=True),
        ),
    ]
