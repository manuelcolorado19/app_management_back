# Generated by Django 2.1.2 on 2019-01-14 08:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0049_sales_report'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='sales_report',
            table='sales_report',
        ),
    ]
