# Generated by Django 2.1.2 on 2019-01-21 17:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0053_pack_channel'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale_product',
            name='comment_unit',
            field=models.CharField(blank=True, max_length=350, null=True),
        ),
    ]
