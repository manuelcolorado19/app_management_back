# Generated by Django 2.1.2 on 2019-02-22 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0058_product_sku_ripley_code_seven_digits'),
    ]

    operations = [
        migrations.AddField(
            model_name='product_sale',
            name='cut_inventory',
            field=models.CharField(blank=True, default='', max_length=10, null=True),
        ),
    ]
