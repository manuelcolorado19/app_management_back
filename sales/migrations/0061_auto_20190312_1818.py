# Generated by Django 2.1.2 on 2019-03-12 18:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0060_auto_20190222_1538'),
    ]

    operations = [
        migrations.AddField(
            model_name='pack',
            name='height',
            field=models.DecimalField(decimal_places=1, max_digits=10, null=True),
        ),
        migrations.AddField(
            model_name='pack',
            name='length',
            field=models.DecimalField(decimal_places=1, max_digits=10, null=True),
        ),
        migrations.AddField(
            model_name='pack',
            name='weight',
            field=models.DecimalField(decimal_places=1, max_digits=10, null=True),
        ),
        migrations.AddField(
            model_name='pack',
            name='width',
            field=models.DecimalField(decimal_places=1, max_digits=10, null=True),
        ),
    ]
