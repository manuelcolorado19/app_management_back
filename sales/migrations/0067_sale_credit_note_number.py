# Generated by Django 2.1.2 on 2019-04-30 17:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0066_product_code_quantity_reset'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale',
            name='credit_note_number',
            field=models.IntegerField(null=True),
        ),
    ]
