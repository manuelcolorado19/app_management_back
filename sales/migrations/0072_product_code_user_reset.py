# Generated by Django 2.1.2 on 2019-05-20 10:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sales', '0071_commission_shipping_price_assigned'),
    ]

    operations = [
        migrations.AddField(
            model_name='product_code',
            name='user_reset',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user_reset_stock', to=settings.AUTH_USER_MODEL),
        ),
    ]
