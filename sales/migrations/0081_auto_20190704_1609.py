# Generated by Django 2.1.2 on 2019-07-04 16:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0080_product_sale_historic_cost_purchase_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='skucategorychannel',
            name='date_end',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='skucategorychannel',
            name='date_start',
            field=models.DateField(blank=True, null=True),
        ),
    ]
