# Generated by Django 2.1.2 on 2019-07-15 14:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0081_auto_20190704_1609'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale',
            name='product_quantity_api',
            field=models.IntegerField(default=0),
        ),
    ]
