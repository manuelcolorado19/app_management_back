# Generated by Django 2.1.2 on 2019-07-24 14:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0084_product_sale_category_retail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categorytree',
            name='parent_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='parent_category_retail', to='sales.RetailCategory'),
        ),
    ]
