from django.db import models
from categories.models import Channel
from django.contrib.auth import get_user_model
import datetime
from django.utils.timezone import now
User = get_user_model()

class Category_Product_Sale(models.Model):
	name = models.CharField(max_length=50)
	sku_start=models.CharField(max_length=5)
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)


	class Meta:
		db_table = 'category_product_sale'

class Product_Sale(models.Model):
	sku=models.IntegerField()
	description=models.CharField(max_length=140)
	category_product_sale = models.ForeignKey(Category_Product_Sale, on_delete=models.SET_NULL, related_name="category_of_product_sale", blank=True, null=True)
	stock=models.IntegerField()
	price=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	height=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	width=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	length=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	weight=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	box_quantity=models.IntegerField(null=True)
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)


	class Meta:
		db_table = 'product_sale'

class Product_Sale_Historic_Cost(models.Model):
	product_sale = models.ForeignKey(Product_Sale, on_delete=models.CASCADE, related_name='product_sale_cost', blank=True, null=True)
	unit_cost = models.DecimalField(decimal_places=2, max_digits=10, null=True)
	credit_iva = models.DecimalField(decimal_places=2, max_digits=10, null=True)
	minimun_price_bill = models.DecimalField(decimal_places=2, max_digits=10, null=True)
	purchase_type = models.CharField(max_length=20, blank=True, null=True)
	purchase_code = models.CharField(max_length=30, blank=True, null=True)
	date_cost = models.DateField(null=True, blank=True)
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)

	class Meta:
		db_table = 'product_historic_cost'

class Color(models.Model):
	description = models.CharField(max_length=100, blank=True)

	class Meta:
		db_table = 'color'

class Pack(models.Model):
	sku_pack = models.CharField(null=True, blank=True, max_length=35)
	category_pack = models.ForeignKey(Category_Product_Sale, on_delete=models.SET_NULL, related_name="category_of_pack", blank=True, null=True)
	description = models.CharField(max_length=200)
	price = models.IntegerField(null=True)
	height=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	width=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	length=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	weight=models.DecimalField(decimal_places=1, max_digits=10, null=True)
	channel = models.ForeignKey(Channel, on_delete=models.SET_NULL, blank=True, null=True, related_name='channel_pack')
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)

	class Meta:
		db_table = 'packs'

class Pack_Products(models.Model):
	pack = models.ForeignKey(Pack, on_delete=models.SET_NULL, blank=True, null=True, related_name='pack_product')
	product_pack = models.ForeignKey(Product_Sale, on_delete=models.SET_NULL, blank=True, null=True, related_name='products_of_pack')
	quantity = models.IntegerField()
	percentage_price = models.IntegerField(null=True)
	color = models.ForeignKey(Color, on_delete=models.SET_NULL, blank=True, null=True, related_name='pack_product_color')
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)

	class Meta:
		db_table = 'packs_products'


class Product_SKU_Ripley(models.Model):
	code = models.CharField(max_length=30, null=True, blank=True)
	pack_ripley = models.ForeignKey(Pack, on_delete=models.SET_NULL, blank=True, null=True, related_name='pack_ripley')
	product_ripley = models.ForeignKey(Product_Sale, on_delete=models.SET_NULL, blank=True, null=True, related_name='product_ripley')
	color = models.ForeignKey(Color, on_delete=models.SET_NULL, blank=True, null=True, related_name='color_ripley')
	code_seven_digits = models.CharField(max_length=7, null=True, blank=True)
	description = models.CharField(max_length=200, null=True, blank=True)
	sku_ripley = models.CharField(max_length=35, null=True, blank=True)
	channel = models.ForeignKey(Channel, on_delete=models.SET_NULL, blank=True, null=True, related_name='channel_sku')

	class Meta:
		db_table = 'sku_ripley'

class Color_Product(models.Model):

	color = models.ForeignKey(Color, on_delete=models.DO_NOTHING, related_name='color_product')
	product = models.ForeignKey(Product_Sale, on_delete=models.DO_NOTHING, related_name='product_color')
	quantity = models.IntegerField()

	class Meta:
		db_table = 'color_product'

class Product_Code(models.Model):
	code_seven_digits=models.CharField(max_length=7, null=True, blank=True)
	product = models.ForeignKey(Product_Sale, on_delete=models.DO_NOTHING, related_name='product_code', blank=True, null=True)
	color = models.ForeignKey(Color, on_delete=models.DO_NOTHING, related_name='product_code_color', blank=True, null=True)
	# category_product_sale = models.ForeignKey(Category_Product_Sale, on_delete=models.SET_NULL, related_name="category_of_product_sale", blank=True, null=True)
	quantity = models.IntegerField(null=True, blank=True)
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)
	reset=models.BooleanField(default=False)
	date_reset=models.DateTimeField(null=True)
	quantity_reset = models.IntegerField(null=True, blank=True)
	user_reset=models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='user_reset_stock')
	merma=models.IntegerField(null=True)
	quantity_reset=models.IntegerField(null=True)

class Sales_Report(models.Model):
	user_created=models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='user_sale_report')
	report_file_name=models.CharField(max_length=60, blank=True, null=True)
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)

	class Meta:
		db_table = 'sales_report'

class ClientSale(models.Model):
	receiver_rut = models.CharField(max_length=10)
	social_reason = models.CharField(max_length=150)
	sale_description = models.CharField(max_length=150)
	address = models.CharField(max_length=100)
	comune_receiver = models.CharField(max_length=50)
	contact = models.CharField(max_length=50, null=True)
	email = models.CharField(max_length=50, null=True)

	class Meta:
		db_table = 'client_sale_data'

class Sale(models.Model):
	DOCUMENT_TYPE = (
		('boleta electrónica', "boleta electrónica"),
		('boleta manual', "boleta manual"),
		('factura', "factura"),
		('sdt', "sdt"),
		('boleta/transbank', "boleta/transbank"),
	)
	PAYMENT_TYPE = (
		('efectivo', 'efectivo'),
		('tarjeta de debito', 'tarjeta de debito'),
		('tarjeta de credito', 'tarjeta de credito'),
		('transferencia', 'transferencia'),
		('cheque', 'cheque'),
		('por pagar', 'por pagar'),
		('sin pago', 'sin pago'),
		('transferencia retail', 'transferencia retail'),
		('mixto', 'mixto')
	)
	SENDER_RESPONSABLE = (
		('comprador', 'comprador'),
		('vendedor', 'vendedor'),
		('retira personalmente', 'retira personalmente'),
	)
	STATUS = (
		('pendiente', 'pendiente'),
		('entregado', 'entregado'),
		('cancelado', 'cancelado'),
		('cambiado', 'cambiado'),

	)
	STATUS_PRODUCT = (
		('vendido', 'vendido'),
		('muestra', 'muestra'),
		('merma', 'merma'),
	)
	STATUS_SHIPPING = (
		('por acordar', 'por acordar'),
		('con etiqueta', 'con etiqueta'),
	)
	SUB_STATUS_SHIPPING = (
		('retiro presencial', 'retiro presencial'),
		('pendiente', 'pendiente'),
		('envío por pagar', 'envío por pagar'),
	)
	order_channel=models.BigIntegerField(null=True)
	total_sale=models.IntegerField(null=True, blank=True)
	declared_total=models.IntegerField(null=True, blank=True)
	revenue=models.IntegerField(null=True, blank=True)
	user=models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='user_created')
	channel=models.ForeignKey(Channel, on_delete=models.SET_NULL, blank=True, null=True, related_name='channel_sale')
	channel_status=models.CharField(null=True, blank=True, max_length=40)
	shipping_type=models.CharField(choices=STATUS_SHIPPING, null=True, blank=True, default="", max_length=15)
	shipping_type_sub_status=models.CharField(choices=SUB_STATUS_SHIPPING, null=True, blank=True, default="", max_length=30)
	shipping_status=models.CharField(null=True, blank=True, default="", max_length=25)
	document_type=models.CharField(choices=DOCUMENT_TYPE, max_length=40, default='boleta', blank=True, null=True)
	number_document=models.IntegerField(null=True)
	payment_type=models.CharField(choices=PAYMENT_TYPE, max_length=40, default='')
	comments=models.TextField(max_length=500, blank=True, null=True, default='')
	sender_responsable=models.CharField(choices=SENDER_RESPONSABLE, max_length=40, default='')
	tracking_number=models.BigIntegerField(null=True)
	status=models.CharField(choices=STATUS, max_length=40, default='entregado')
	status_product=models.CharField(choices=STATUS_PRODUCT, max_length=40, default='vendido')
	bill=models.BooleanField(default=False)
	token_bill=models.CharField(null=True, blank=True, max_length=70)
	date_sale=models.DateField(default=now, blank=True)
	hour_sale_channel=models.CharField(max_length=6, null=True, blank=True)
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)
	date_update=models.DateTimeField(auto_now=True)
	date_document_emision=models.DateField(null=True, blank=True)
	shipping_guide_number=models.IntegerField(null=True)
	last_user_update=models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='last_update_user')
	total_number_of_product=models.IntegerField(null=True)
	mlc = models.CharField(blank=True, null=True, max_length=14)
	user_id_ml = models.BigIntegerField(null=True)
	cut_inventory=models.CharField(null=True, blank=True, default='', max_length=10)
	shipping_date= models.DateField(blank=True, null=True)
	credit_note_number=models.IntegerField(null=True)
	product_quantity_api=models.IntegerField(default=0)
	sales_report = models.ForeignKey(Sales_Report, on_delete=models.SET_NULL, related_name='sales_report_manifiesto', blank=True, null=True)
	bill_fact=models.BooleanField(default=False)
	client_data=models.ForeignKey(ClientSale, on_delete=models.SET_NULL, related_name='sale_client_data', blank=True, null=True)

	class Meta:
		db_table = 'sales'

class Sale_Product(models.Model):
	product=models.ForeignKey(Product_Sale, on_delete=models.DO_NOTHING, related_name='product_sale', null=True, blank=True)
	product_code=models.ForeignKey(Product_Code, on_delete=models.DO_NOTHING, related_name='product_code_sale', null=True, blank=True)
	sale=models.ForeignKey(Sale, on_delete=models.CASCADE, related_name='sale_of_product', null=True, blank=True)
	quantity=models.IntegerField()
	unit_price=models.DecimalField(max_digits=10,decimal_places=2)
	total=models.DecimalField(max_digits=10 ,decimal_places=2)
	from_pack=models.ForeignKey(Pack, on_delete=models.SET_NULL, related_name='sale_pack', null=True, blank=True, default=None)
	comment_unit = models.CharField(max_length=350, null=True, blank=True)
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)


	class Meta:
		db_table = 'sale_product_pivote'

class Product_Channel(models.Model):
	product = models.ForeignKey(Product_Sale, on_delete=models.DO_NOTHING, related_name='product_channel', null=True, blank=True)
	channel = models.ForeignKey(Channel, on_delete=models.DO_NOTHING, related_name='channel_product', null=True, blank=True )
	assigned_amount = models.IntegerField(default=0)
	assignment_date=models.DateTimeField(auto_now_add=True, null=True, blank=True)
	date_update=models.DateTimeField(auto_now=True)
	date_created=models.DateTimeField(auto_now_add=True, null=True, blank=True)
	

	class Meta:
		db_table = 'product_channel_pivote'



class Commission(models.Model):
	sale_product = models.OneToOneField(Sale_Product, on_delete=models.CASCADE, related_name='commission_sale_product')
	api_commission = models.DecimalField(null=True, decimal_places=3, max_digits=10)
	calculate_commission = models.DecimalField(null=True, decimal_places=3, max_digits=10)
	discount_commission = models.DecimalField(null=True, decimal_places=3, max_digits=10)
	shipping_price_assigned = models.DecimalField(null=True, decimal_places=3, max_digits=10)
	date_created=models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'sale_product_commission'

class CommissionChannel(models.Model):
	CATEGORIES_COMMISSION = (
		('belleza', 'belleza'),
		('bicicletas', 'bicicletas'),
		('cubre sofa', 'cubre sofa'),
		('decohogar', 'decohogar'),
		('decoracion', 'decoracion'),
		('deporte', 'deporte'),
		('deportes', 'deportes'),
		('drone', 'drone'),
		('drones', 'drones'),
		('generica', 'generica'),
		('hogar', 'hogar'),
		('infantil', 'infantil'),
		('juguetes', 'juguetes'),
		('luces', 'luces'),
		('muebles', 'muebles'),
		('rodillo', 'rodillo'),
		('tecno', 'tecno'),
		('tecnologia', 'tecnologia'),
	)
	product_sale = models.ForeignKey(Product_Sale, on_delete=models.SET_NULL, blank=True, null=True, related_name='commission_product_sale')
	pack = models.ForeignKey(Pack, on_delete=models.SET_NULL, blank=True, null=True, related_name='commission_pack')
	channel = models.ForeignKey(Channel, on_delete=models.CASCADE, related_name='commission_channel')
	percentage_commission = models.IntegerField(null=True)
	category_sku = models.CharField(max_length=25, choices=CATEGORIES_COMMISSION, blank=True, null=True)

	class Meta:
		db_table = 'channel_commission'

class DimensionsSale(models.Model):
	sale = models.OneToOneField(Sale, on_delete=models.CASCADE, related_name='dimensions_sale', null=True, blank=True)
	height = models.DecimalField(null=True, decimal_places=3, max_digits=10)
	weight = models.DecimalField(null=True, decimal_places=3, max_digits=10)
	length = models.DecimalField(null=True, decimal_places=3, max_digits=10)
	width = models.DecimalField(null=True, decimal_places=3, max_digits=10)

	class Meta:
		db_table = 'dimensions_product_sale'


class SubCategoriesChannel(models.Model):
	commission = models.IntegerField(null=True)
	name = models.CharField(max_length=200)
	category_asiamerica = models.ForeignKey(Category_Product_Sale, on_delete=models.CASCADE, related_name='category_asiamerica')
	channel = models.ForeignKey(Channel, on_delete=models.SET_NULL, related_name='channel_sub_category', null=True, blank=True)
	date_created=models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'sub_category_channel'


class SKUCategoryChannel(models.Model):
	product_sale = models.ForeignKey(Product_Sale, on_delete=models.CASCADE, null=True, blank=True, related_name='product_sale_sub_category')
	pack = models.ForeignKey(Pack, on_delete=models.CASCADE, null=True, blank=True, related_name='pack_sub_category')
	sub_category_channel = models.ForeignKey(SubCategoriesChannel, on_delete=models.CASCADE, null=True, blank=True, related_name='sub_category_sku')
	date_start=models.DateField(null=True, blank=True)
	date_end=models.DateField(null=True, blank=True)
	date_created=models.DateTimeField(auto_now_add=True)
	
	class Meta:
		db_table = 'sku_category_channel'


class RetailCategory(models.Model):
	category_name = models.CharField(max_length=150)
	code_category = models.CharField(max_length=50)
	channel = models.ForeignKey(Channel, on_delete=models.CASCADE, related_name='retail_category_channel')
	category_asiamerica = models.ForeignKey(Category_Product_Sale, on_delete=models.SET_NULL, null=True, blank=True, related_name='retail_asiamerica_category')
	date_start = models.DateField(null=True)
	date_end = models.DateField(null=True)
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'retail-categories'

class CategoryTree(models.Model):
	parent_category = models.ForeignKey(RetailCategory, on_delete=models.CASCADE, related_name='parent_category_retail', null=True, blank=True)
	child_category = models.ForeignKey(RetailCategory, on_delete=models.CASCADE, related_name='child_category_retail')
	commission = models.IntegerField()
	level = models.IntegerField(null=True)
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'category-tree'

class Product_Sale_Category_Retail(models.Model):
	product_sale = models.ForeignKey(Product_Sale, on_delete=models.CASCADE, related_name='product_sale_category_retail_asociated')
	retail_category = models.ForeignKey(RetailCategory, on_delete=models.CASCADE, related_name='retail_category_product_sale')
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'product_sale_category_retail'



class OperationSpending(models.Model):
	spend_month = models.DateField()
	spend_amount = models.BigIntegerField()
	iva_sii_amount = models.BigIntegerField(null=True)
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'month-operation-spend'

class RetailCreditIvaSpend(models.Model):
	spend_month = models.DateField()
	channel = models.ForeignKey(Channel, on_delete=models.CASCADE, related_name='credit_iva_channel_spend')
	total_amount = models.BigIntegerField()
	date_created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'credit-iva-retail'




