from rest_framework import permissions

class SalesPermissions(permissions.IsAuthenticated):
    
    def has_permission(self, request, view):

        if not request.user.is_anonymous:
            if request.user.is_superuser:
                return True
            else:
                if view.action == 'destroy':
                    return False
                else:
                    return True
        else:
            return False
        return super(
            OperationsPermissions,
            self
        ).has_permission(
            request,
            view
        )

class Product_SalePermissions(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        if not request.user.is_anonymous:
            if request.user.is_superuser:
                return True
            else:
                if view.action == 'create':
                    return False
                else:
                    return True
        else:
            return False
        return super(
            OperationsPermissions,
            self
        ).has_permission(
            request,
            view
        )
