from sales.models import SubCategoriesChannel, Category_Product_Sale, SKUCategoryChannel
from categories.models import Channel
import requests
from notifications.models import Token
import itertools
from operator import itemgetter
from openpyxl import load_workbook
import json
token = Token.objects.get(id=1).token_ml
wb = load_workbook('purchases/faltantes.xlsx')
ws = wb['Hoja1']
url_mshop = "https://api.mercadolibre.com/orders/search?seller=216244489&access_token=" + token +"&order.date_created.from=2019-01-01T16:00:00.000-00:00&order.date_created.to=2019-06-21T23:00:00.000-00:00"
data = requests.get(url_mshop).json()
if data['paging']['total'] <= 50:
	rows = data["results"]
else:
	rows = []
	count = 0
	iteration_end = int(data['paging']['total']/50)
	module = int(data['paging']['total']%50)
	for i in range(iteration_end + 1):
		new_rows = requests.get(url_mshop + "&offset=" + str(i*50)).json()['results']
		rows = rows + new_rows
	rows = rows + requests.get(url_mshop + "&offset="+str(((iteration_end*50) + module))).json()['results']
mlc_data = []
for row in ws.rows:
	mlc_data.append(row[0].value)
mlc_data = list(set(mlc_data))

print(len(rows))
for i in mlc_data:
	for each in rows:
		esta = False
		if i == each['order_items'][0]['item']['id']:
			esta = True
			if each['order_items'][0]['item']['seller_custom_field'] is not None:
				if len(each['order_items'][0]['item']['seller_custom_field']) == 7:
					print(i + "{" + each['order_items'][0]['item']['seller_custom_field'])
				else:
					print(i + "{" + each['order_items'][0]['item']['seller_custom_field'][:5])
			elif each['order_items'][0]['item']['seller_sku']:
				if len(each['order_items'][0]['item']['seller_sku']) == 7:
					print(i + "{" + each['order_items'][0]['item']['seller_sku'])
				else:
					print(i + "{" + each['order_items'][0]['item']['seller_sku'][:5])
			else:
				print(i + "{ -" )
			break
	if esta:
		pass
	else:
		print(i + "{ -")

# print(len(mlc_data))