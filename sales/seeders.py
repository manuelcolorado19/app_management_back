from sales.models import Product_Sale, Sale, Sale_Product, Product_Channel, Product_Code
from django.contrib.auth import get_user_model
from categories.models import Channel
from faker import Faker
from random import randint
from datetime import datetime
import random
from random_words import RandomWords

rw = RandomWords()
fake = Faker()

User = get_user_model()
products = Product_Sale.objects.all()
channels = Channel.objects.exclude(id__in=[2,9,10,11,12,13,7])
product_code = Product_Code.objects.all()
users = User.objects.all()

for i in range(700):
	total = 0
	sale = Sale.objects.create(
		order_channel=random.choice([randint(10000,99999), None]),
		total_sale=0,
		user=users[randint(0, users.count() - 1)],
		channel=channels[randint(0, channels.count() - 1)],
		document_type=random.choice(['boleta electrónica','boleta manual', 'factura', 'sdt','boleta/transbank']),
		number_document=random.choice([randint(10000,99999), None]),
		payment_type=random.choice(['efectivo', 'tarjeta de debito', 'tarjeta de credito','transferencia', 'cheque', 'por pagar', 'sin pago', 'transferencia retail']),
		comments=rw.random_word(),
		sender_responsable=random.choice(['comprador', 'vendedor', 'retira personalmente']),
		status=random.choice(['pendiente', 'entregado', 'cancelado','cambiado']),
		status_product=random.choice(['vendido', 'muestra', 'merma']),
		date_sale=datetime(2017,randint(1, 12), randint(1, 28)),
	)
	for j in range(1):
		quantity = randint(1,5)
		unit_price=randint(10000,99999)
		product = products[randint(0, products.count() - 1)]
		product_code_unit = product_code.filter(product__id=product.id)
		sale_product = Sale_Product.objects.create(
			product=product,
			# product_code=random.choice([product_code_unit[randint(0, product_code_unit.count() - 1)], None]),
			sale=sale,
			quantity=quantity,
			unit_price=unit_price,
			total=quantity*unit_price,
		)
		total += sale_product.total;
	sale.total_sale = total
	sale.save()
print("Sales created")

# for y in products:
# 	for j in channels:
# 		p_channel = Product_Channel.objects.create(
# 			product=y,
# 			channel=j,
# 			assigned_amount=randint(1,20),
# 		)
print("Product assignation created")

