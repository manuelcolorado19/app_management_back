from .models import Product_Sale, Sale, Sale_Product, Color_Product, Product_Channel, Product_Code, Pack, Pack_Products, Category_Product_Sale, Sales_Report, Color, Product_SKU_Ripley, Commission, DimensionsSale
from rest_framework import serializers
from purchases.models import Purchase, Purchase_Product
from django.db.models import F, Sum, IntegerField, Value, CharField, Case, When
import datetime
from django.db import transaction
import datetime
from datetime import timedelta
import requests
import json
from OperationManagement.settings import API_KEY_OF, OPEN_FACTURA_URL, DATA_EMITTER
from django.db.models.functions import Cast, Concat
from notifications.models import Token
import math
from returns.serializers import ReturnSerializer
from django.core.mail import send_mail
from stock.serializers import Product_ColorSerializer

class LastProductSKU(serializers.IntegerField):
	def to_representation(self, value):
		all_sku = Product_Sale.objects.filter(category_product_sale=value).order_by('sku')
		if all_sku:
			return all_sku[all_sku.count()-1].sku
		else:
			return int(str(value.sku_start) + "000")

class LastPackSKU(serializers.IntegerField):
	def to_representation(self, value):
		all_sku = Pack.objects.filter(category_pack=value).exclude(channel__name__in=['Ripley', 'Linio']).order_by('sku_pack')
		if all_sku:
			return all_sku[all_sku.count()-1].sku_pack
		else:
			return int(str(value.sku_start) + "000")

class Category_Product_SaleSerializer(serializers.ModelSerializer):
	last_sku_product = LastProductSKU(read_only=True, source='*')
	last_sku_pack = LastPackSKU(read_only=True, source='*')
	class Meta:
		model = Category_Product_Sale
		fields = '__all__'

class SkuNewSerializer(serializers.ModelSerializer):
	type_product = serializers.CharField()
	products = serializers.ListField()
	description = serializers.CharField(allow_blank=True)
	height = serializers.DecimalField(allow_null=True, required=False, max_digits=10, decimal_places=1)
	width = serializers.DecimalField(allow_null=True, required=False, max_digits=10, decimal_places=1)
	length = serializers.DecimalField(allow_null=True, required=False, max_digits=10, decimal_places=1)
	weight = serializers.DecimalField(allow_null=True, required=False, max_digits=10, decimal_places=1)

	def validate_sku(self, value):
		if Product_Sale.objects.filter(sku=value).count() > 0 or Pack.objects.filter(sku_pack=value).count() > 0:
			raise serializers.ValidationError("El SKU " + str(value) +" ya se encuentra registrado.")
		return value

	def validate_products(self, value):
		if len(value) == 0:
			return value
		percent_total = 0
		for product in value:
			if product['quantity'] <= 0:
				raise serializers.ValidationError("No se puede añadir producto con cantidad 0")
			percent_total += product['percentage_price']
		if percent_total > 100:
			raise serializers.ValidationError("La suma de los porcentajes de precio debe ser igual a 100%")
		return value

	def validate_description(self, value):
		if len(value) == 0:
			raise serializers.ValidationError("La descripción no puede estar vacía.")
		return value

	def validate(self, data):
		if data['type_product'] != 'Pack':
			if str(data['sku'])[:2] != data['category_product_sale'].sku_start:
				raise serializers.ValidationError({"sku": ['El número de SKU debe ser correlativo con la categoría seleccionada']})
		return data
	class Meta:
		model = Product_Sale
		fields = (
			'sku',
			'description',
			'category_product_sale',
			'type_product',
			'products',
			'height',
			'width',
			'length',
			'weight',
		)
class SkuUpdateSerializer(serializers.ModelSerializer):
	type_product = serializers.CharField()
	products = serializers.ListField(required=False)
	description = serializers.CharField(allow_blank=True)
	height = serializers.DecimalField(allow_null=True, required=False, max_digits=10, decimal_places=1)
	width = serializers.DecimalField(allow_null=True, required=False, max_digits=10, decimal_places=1)
	length = serializers.DecimalField(allow_null=True, required=False, max_digits=10, decimal_places=1)
	weight = serializers.DecimalField(allow_null=True, required=False, max_digits=10, decimal_places=1)

	def validate_products(self, value):
		if len(value) == 0:
			return value
		percent_total = 0
		for product in value:
			if product['quantity'] <= 0:
				raise serializers.ValidationError("No se puede añadir producto con cantidad 0")
			percent_total += product['percentage_price']
		if percent_total > 100:
			raise serializers.ValidationError("La suma de los porcentajes de precio debe ser igual a 100%")
		return value

	def validate_description(self, value):
		if len(value) == 0:
			raise serializers.ValidationError("La descripción no puede estar vacía.")
		return value

	def validate(self, data):
		if data['type_product'] != 'Pack':
			# print(data['sku'])
			if str(data['sku'])[:2] != data['category_product_sale'].sku_start:
				raise serializers.ValidationError({"sku": ['El número de SKU debe ser correlativo con la categoría seleccionada']})
		return data
	class Meta:
		model = Product_Sale
		fields = (
			'sku',
			'description',
			'category_product_sale',
			'type_product',
			'products',
			'height',
			'width',
			'length',
			'weight',
		)

class CategoryName(serializers.CharField):
	def to_representation(self, value):
		if isinstance(value, Product_Sale):
			if value.category_product_sale is not None:
				return value.category_product_sale.name
			else:
				return '-'
		else:
			if value.category_pack is not None:
				return value.category_pack.name
			else:
				return '-'

class TypeSKU(serializers.CharField):
	def to_representation(self, value):
		if isinstance(value, Product_Sale):
			return "Normal"
		return "Pack"

class Product_CodeSerializer2(serializers.ListField):
	def to_representation(self, value):
		return Product_Code.objects.filter(product=value).values('id', 'code_seven_digits', 'color__description').order_by('code_seven_digits')

class Product_SaleSerializer2(serializers.ModelSerializer):
	product_code = Product_CodeSerializer2(read_only=True, source='*')
	class Meta:
		fields = '__all__'
		model = Product_Sale

		
class Product_SaleSerializer(serializers.ModelSerializer):

	class Meta:
		fields = '__all__'
		model = Product_Sale

	def to_representation(self, instance):
		code_count = Product_Code.objects.filter(product=instance)
		if code_count.count() == 1:
			code_seven_digits = code_count[0].code_seven_digits
			color = code_count[0].color.description
			# max_items = 0
		else:
			if len(self.context['request'].query_params['search']) > 5 and self.context['request'].query_params['search'].isdigit():
				code_seven_digits = self.context['request'].query_params['search']
				color = Product_Code.objects.get(code_seven_digits=code_seven_digits)
				color = color.color.description
				# max_items = color.quantity
			else:
				code_seven_digits = ""
				color = "----"
				# max_items = instance.stock
		return {
			"id":instance.id,
			"id_sale":None,
			"id_of_product":instance.id,
			"description":instance.description,
			"price":instance.price,
			"sku":instance.sku,
			"stock":instance.stock,
			"code_seven_digits":code_seven_digits,
			"box_quantity":instance.box_quantity,
			# "max_items":max_items,
			"color":color
		}
class ProductsPackSerializer(serializers.ListField):
	def to_representation(self, value):
		return Pack_Products.objects.filter(pack=value).annotate(sku=F('product_pack__sku'), description=F('product_pack__description')).values('id','sku','description', 'quantity', 'percentage_price')

class Category_Pack(serializers.IntegerField):
	def to_representation(self, value):
		if value.category_pack is not None:
			return value.category_pack.id
		return 0

class ChannelName(serializers.CharField):
	def to_representation(self, value):
		if value.channel is not None:
			return value.channel.name
		return '-'

class PackSerializer(serializers.ModelSerializer):
	sku = serializers.CharField(read_only=True, source='sku_pack')
	type_sku = TypeSKU(read_only=True, source='*')
	category_name = CategoryName(read_only=True, source='*')
	products = ProductsPackSerializer(read_only=True, source='*')
	category_product_sale = Category_Pack(read_only=True, source='*')
	channel_name = ChannelName(read_only=True, source='*')
	class Meta:
		model = Pack
		fields = '__all__'
		
class Sale_ProductSerializer(serializers.ModelSerializer):
	product = Product_SaleSerializer(read_only=True)
	class Meta:
		fields = '__all__'
		model = Sale_Product

class ProductOfSale(serializers.ListField):
	def to_representation(self, value):
		data = []
		for i in Sale_Product.objects.filter(sale=value).annotate(sku=F('product__sku'), description=F('product__description'), color=F('product_code__color__description'), code_seven_digits=F('product_code__code_seven_digits'), id_of_product=F('product__id'), id_sale=F('id')).values('id_sale','sku','description', 'quantity', 'unit_price', 'total', 'color', 'code_seven_digits', 'id_of_product'):
			data.append(i)
		return data


class FirstName(serializers.CharField):
	def to_representation(self, value):
		if value.last_user_update is not None:
			return value.last_user_update.first_name + " " + value.last_user_update.last_name

# class LastName(serializers.CharField):
# 	def to_representation(self, value):
# 		return value.user.last_name

class TodayDate(serializers.CharField):
	def to_representation(self, value):
		return str(datetime.datetime.now())

class HourSale(serializers.CharField):
	def to_representation(self, value):
		return '%s:%s' % (value.date_created.hour if value.date_created.hour >= 10 else ('0' + str(value.date_created.hour)), value.date_created.minute if value.date_created.minute >= 10 else ('0' + str(value.date_created.minute)))

class DateWithSlashed(serializers.CharField):
	def to_representation(self, value):
		return str(value.date_sale.strftime('%Y/%m/%d'))

		# return str(value.date_created.time())

# class SevenDigits(serializers.CharField):
# 	def to_representation(self, value):
# 		return Sale_Product.objects.filter()

class TotalDeclared(serializers.IntegerField):
	def to_representation(self, value):
		if value.declared_total is not None and value.declared_total != 0:
			return value.declared_total
		else:
			if value.channel.name == 'Ripley':
				return Sale_Product.objects.filter(sale=value).aggregate(total_declared=Sum('total'))['total_declared']
			else:
				return Sale_Product.objects.filter(sale=value).exclude(product__sku=1).aggregate(total_declared=Sum('total'))['total_declared']

class DimesionSaleSerializer(serializers.ModelSerializer):
	class Meta:
		model = DimensionsSale
		fields = '__all__'

class SaleSerializer(serializers.ModelSerializer):
	products = serializers.ListField(
	   child=serializers.JSONField(), write_only=True
	)
	number_document = serializers.IntegerField(required=True)
	sender_responsable = serializers.CharField(allow_blank=True, allow_null=False)
	payment_type = serializers.CharField(allow_blank=True, allow_null=False)
	number_document = serializers.IntegerField(required=False, allow_null=True)
	order_channel = serializers.IntegerField(required=False, allow_null=True)
	sale_of_product = ProductOfSale(read_only=True, source='*')
	channel_name = ChannelName(read_only=True, source='*')
	user_full_name = FirstName(read_only=True, source='*')
	# last_name = LastName(read_only=True, source='*')
	date_sale = serializers.CharField()
	date_with_slashed = DateWithSlashed(read_only=True, source='*')
	hour_sale = HourSale(read_only=True, source='*')
	bill = serializers.BooleanField()
	total_declared = TotalDeclared(read_only=True, source='*')
	credit_note_number = serializers.IntegerField(read_only=True)
	dimensions_sale = DimesionSaleSerializer(read_only=True)
	# return_sale = serializers.SerializerMethodField(read_only=True)
	# today = TodayDate(read_only=True, source='*')
	# code_seven_digits = SevenDigits(read_only=True, source='*')
	class Meta:
		fields = (
			'id',
			'sender_responsable',
			'payment_type',
			'number_document',
			'order_channel',
			'total_sale',
			'document_type',
			'comments',
			'status',
			'status_product',
			# 'date_sale',
			'user',
			'channel',
			'products',
			'sale_of_product',
			'channel_name',
			'user_full_name',
			'date_sale',
			'hour_sale',
			'date_with_slashed',
			'bill',
			'total_declared',
			'tracking_number',
			'credit_note_number',
			'dimensions_sale',
			'sales_report',
			# 'return_sale',
			# 'code_seven_digits'
			# 'today'
		)
		model = Sale

	# def get_return_sale(self, instance):
	# 	return_sales = instance.return_sale.all().order_by('-id')
	# 	return ReturnSerializer(return_sales, many=True).data

	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl","abel.mejias@asiamerica.cl", "mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code = Product_Code.objects.filter(code_seven_digits=each)[0]
				message = product_code.product.description + " Variante: " + product_code.color.description + " (" + str(each) + ")"
			else:
				product_sale = Product_Sale.objects.filter(sku=each)[0]
				message = product_sale.description + " (" + str(each) + ")"
			message_append.append(message)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def validate_sender_responsable(self, value):
		if value == '':
			raise serializers.ValidationError('Debe indicar un responsable de envío.')
		return value

	def validate_payment_type(self, value):
		if value == '':
			raise serializers.ValidationError('Debe indicar una forma de pago.')
		return value


	def validate_products(self, value):
		if value:
			return value
		raise serializers.ValidationError('Debe haber al menos un producto agregado')

	# def validate_date_sale(self, value):
	# 	return value
	def roundup(self, x):
		return int(math.ceil(x / 100.0)) * 100

	def calculate_commission(self, sale_product):
		# if sale_product.sale.channel_id in [7,10,11,12,14]:
		if sale_product.sale.channel_id == 1:
			token = Token.objects.get(id=1).token_ml
			sale_date_data = sale_product.sale.date_sale
			sale_date_plus = sale_product.sale.date_sale + timedelta(days=1)
			url_data = "https://api.mercadolibre.com/orders/search?seller=216244489&access_token="+ token +"&order.date_created.from="+sale_date_data.strftime("%Y-%m-%dT00:00:00.000-00:00")+"&order.date_created.to="+sale_date_plus.strftime("%Y-%m-%dT23:59:00.000-00:00")
			# print(url_data)
			data_orders = requests.get(url_data).json()
			if data_orders.get('results', None) is not None:
				commission_api = None
				data_orders = data_orders['results']
				for i in data_orders:
					if i['id'] == sale_product.sale.order_channel:
						# print("hi")
						commission_api = i['order_items'][0]['sale_fee']
						unit_price_api = i['order_items'][0]['unit_price']
						quantity_api = i['order_items'][0]['quantity']
						break
			else:
				raise serializers.ValidationError({"date_sale": ["Disculpe. La fecha de venta no es correcta"]})

			if commission_api == None:
				raise serializers.ValidationError({"date_sale": ["Disculpe. La fecha de venta no es correcta"]})
			if unit_price_api <= 19990:
				additional_cost = 100 * quantity_api
			else:
				additional_cost = 0
			# commission_total = commission_api
			percentage_product_commission = commission_api * (((sale_product.total * 100) / (unit_price_api * quantity_api))/100)
			print(percentage_product_commission)
			return [(quantity_api * percentage_product_commission), (sale_product.total * 0.13 + additional_cost)]
		if sale_product.sale.channel_id == 6:
			return [0, sale_product.total * 0.35]
		# if sale_product.sale.channel_id == 3:
		# 	sale_date_data = sale_product.sale.date_sale
		# 	url_data = "https://ripley-prod.mirakl.net/api/orders?start_date="+sale_date_plus.strftime("%Y-%m-%dT00:00:00Z") + "&end_date="+ sale_date_plus.strftime("%Y-%m-%dT23:59:00Z") +"&paginate=false"
		# 	headers = {'Accept': 'application/json', 'Authorization': API_KEY_RP}
		# 	data = requests.get(url_data, headers=headers).json()
		# 	if data.get('orders', None) is not None:
		# 		commission_api = None
		# 		data_orders = data_orders['orders']
		# 		for i in data:
		# 			if i['commercial_id'] == str(sale_product.sale.order_channel):
		# 				# print("hi")
		# 				commission_api = i['total_commission']
		# 				unit_price_api = i['order_items'][0]['unit_price']
		# 				quantity_api = i['order_items'][0]['quantity']
		# 				break
		# 	else:
		# 		raise serializers.ValidationError({"date_sale": ["Disculpe. La fecha de venta no es correcta"]})
			return [0,0]
		return [0, 0]


	@transaction.atomic
	def create(self, validated_data):
		products = validated_data.pop('products')
		special_register = any(i['sku'] == 2 for i in products)
		# print(validated_data['document_type'])
		if validated_data.get('channel', None) == None:
			raise serializers.ValidationError({"channel": ["Debe indicar un canal de venta"]})
		if special_register and (validated_data.get('comments', None) == '' or validated_data.get('comments', None) == None):
			raise serializers.ValidationError({'comments': ["Debe añadir un comentario para el registro de ingresos especiales"]})

		if ((validated_data['document_type'] != 'sdt') and (validated_data['document_type'] != 'boleta electrónica')) and validated_data.get('number_document', None) == None:
			raise serializers.ValidationError({'number_document': ['El número de documento es requerido para este documento tributario']})

		if (validated_data.get('order_channel', None) is None) and (validated_data['channel'].name == 'Mercadolibre' or validated_data['channel'].name == 'Ripley' or validated_data['channel'].name == 'Linio' or validated_data['channel'].name == 'Groupon' or validated_data['channel'].name == 'Paris' or validated_data['channel'].name == 'Mercadoshop' or validated_data['channel'].name == 'Cluboferta' or validated_data['channel'].name == 'Falabella'):
			raise serializers.ValidationError({"id_venta": ["Se debe enviar id de venta para el canal seleccionado"]})

		if validated_data.get('order_channel', None) is not None and Sale.objects.filter(order_channel=validated_data['order_channel']):
			raise serializers.ValidationError({"order_channel": ['El id de venta debe ser único en los registros.']})

		if validated_data.get('number_document', None) is not None and Sale.objects.filter(number_document=validated_data['number_document'], document_type=validated_data['document_type']):
			raise serializers.ValidationError({"number_document": ['El número de documento debe ser único para el tipo de documento']})
		if validated_data['payment_type'] == "mixto" and validated_data['comments'] == "":
			raise serializers.ValidationError({"comments": ["Debe indicar comentario para ventas con método de pago mixto"]})
		#if validated_data['channel'].name == 'Mercadolibre':
		#	token = Token.objects.get(id=1).token_ml
		#	url_request = "https://api.mercadolibre.com/orders/" + str(validated_data['order_channel']) + "?access_token=" + token
		#	feedback_channel = requests.get(url_request).json()['feedback']['sale']
		#	if feedback_channel is not None:
		#		validated_data['status'] = 'entregado'
		#	else:
		#		validated_data['status'] = 'pendiente'
		

		validated_data['user'] = self.context['request'].user
		try:
			validated_data['date_sale'] = datetime.datetime.strptime(validated_data['date_sale'], "%Y-%m-%dT%H:%M:%S.%fZ")
		except ValueError as e:
			validated_data['date_sale'] = datetime.datetime.strptime(validated_data['date_sale'], "%Y-%m-%dT%H:%M:%S.%fZ")

		sale = Sale.objects.create(**validated_data)
		sale.date_document_emision = datetime.datetime.now().date()
		total_sale = 0
		total_sale_no_shipping = 0
		total_products = 0
		#print(products)
		products_stock = []
		for product in products:
			if not Product_Sale.objects.get(id=product['id_of_product']).sku == 1:
				total_sale_no_shipping += (product['unit_price'] * product['quantity'])
		for product in products:
			product_model = Product_Sale.objects.get(id=product['id_of_product'])
			product_code = product.get('code_seven_digits', None)
			if product_code is not None and product_code != "":
				product_code = Product_Code.objects.get(code_seven_digits=int(product['code_seven_digits'])) 
			else:
				product_code = None

			# if product['code_seven_digits'] == "" and (validated_data.get('comments', None) == '' or validated_data.get('comments', None) == None):
			# 	raise serializers.ValidationError({"comments": ["Debe añadir comentario al ingresar productos con sku de 5 digitos"]})
			# total_product = product['quantity'] * product['unit_price']
			if (product['unit_price'] * product['quantity']) > total_sale_no_shipping:
				product['unit_price'] = total_sale_no_shipping/(product['quantity'])
			else:
				if not float(product['unit_price']).is_integer():
					product['unit_price'] = self.roundup(product['unit_price'])
					total_sale_no_shipping -= (product['unit_price'] * product['quantity'])
			sale_product = Sale_Product.objects.create(
				product=product_model,
				sale=sale,
				quantity=product['quantity'],
				unit_price=product['unit_price'],
				total=(product['quantity'] * product['unit_price']),
				product_code=product_code
			)
			# comssions_calculate = self.calculate_commission(sale_product)
			# commission = Commission.objects.create(
			# 	sale_product=sale_product,
			# 	api_commission=comssions_calculate[0],
			# 	calculate_commission=comssions_calculate[1],
			# )
			if product_code is not None:
				product_code.quantity = product_code.quantity - product['quantity']
				product_code.save()
			
			count_sku7 = Product_Code.objects.filter(product=sale_product.product)
			if count_sku7.count() == 1:
				sale_product.product_code = count_sku7[0]
				sale_product.save()
			total_sale += (product['quantity'] * product['unit_price'])
			if product_model.sku != 1:
				total_products += product['quantity']

			product_model.stock = product_model.stock - product['quantity']
			product_model.save()
			quantity_stock = Product_Sale.objects.get(sku=product_model.sku).stock - (product['quantity'])
			Product_Sale.objects.filter(sku=product_model.sku).update(stock=quantity_stock)
			if product_code is not None:
				quantity_stock_color = product_code.quantity - (product['quantity'])
				Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
				if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not (product_code.code_seven_digits in products_stock)):
					products_stock.append(product_code.code_seven_digits)
			else:
				if quantity_stock <= 5 and quantity_stock >= 0 and (not (product_model.sku in products_stock)):
					products_stock.append(product_model.sku)
		sale.total_sale = total_sale
		sale.total_number_of_product = total_products
		sale.save()
		# print(validated_data)
		if Sale_Product.objects.filter(sale=sale).exclude(product__sku=1):
			if sale.total_sale != validated_data['total_sale'] and validated_data['total_sale'] is not None:
				product_to_split = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1)[0]
				product_split = Sale_Product.objects.get(id=product_to_split.id)
				diference_totals = validated_data['total_sale'] - sale.total_sale
				if product_split.quantity == 1:
					product_split.unit_price = float(product_split.unit_price) + diference_totals
					product_split.save()
					product_split.total = float(product_split.unit_price) * float(product_split.quantity)
					# product_split.total = product_split.unit_price * product_split.quantity
					# product_split.commission_sale_product.calculate_commission = self.calculate_commission(product_split)[1]
					# product_split.commission_sale_product.save()
					product_split.save()
				else:
					product_split.quantity = product_split.quantity - 1
					product_split.total = product_split.total - product_split.unit_price
					product_split.save()
					# product_split.commission_sale_product.calculate_commission = self.calculate_commission(product_split)[1]
					# product_split.commission_sale_product.save()
					sale_product = Sale_Product.objects.create(
						product=Product_Sale.objects.get(sku=product_split.product),
						product_code=product_split.product_code,
						sale=sale,
						quantity=1,
						unit_price=product_split.unit_price + diference_totals,
						total=(product_split.unit_price + diference_totals),
					)
					# commission = Commission.objects.create(
					# 	sale_product=sale_product,
					# 	api_commission=0,
					# 	calculate_commission=self.calculate_commission(sale_product)[1],
					# )
				total_sale_split = validated_data['total_sale']
				sale.total_sale = total_sale_split
				sale.save()
		if sale.bill and sale.channel.name != 'Paris' and sale.channel.name != 'Falabella':
			if sale.channel.name == 'Ripley':
				products_data = Sale_Product.objects.filter(sale__id=sale.id).annotate(NmbItem=Case(When(product_code=None,then=Concat(Value('# ', output_field=CharField()), F('product__description'))),default=F('product__description'), output_field=CharField()), DscItem=Concat('product__sku', Value(' - ', output_field=CharField()), Value(' (', output_field=CharField()), 'sale__comments',Value(')', output_field=CharField()), output_field=CharField()),QtyItem=F('quantity'),PrcItem=Cast(F('unit_price'), IntegerField()), MontoItem=Cast(F('total'), IntegerField())).values(
					"NmbItem",
					"DscItem",
					"QtyItem",
					"PrcItem",
					"MontoItem",
				)
				total_data = Sale_Product.objects.filter(sale=sale).aggregate(MntTotal=Cast(Sum('total'), IntegerField()), TotalPeriodo=Cast(Sum('total'), IntegerField()), VlrPagar=Cast(Sum('total'), IntegerField()))
			else:
				products_data = Sale_Product.objects.filter(sale__id=sale.id).exclude(product__sku=1).annotate(NmbItem=Case(When(product_code=None,then=Concat(Value('# ', output_field=CharField()), F('product__description'))),default=F('product__description'), output_field=CharField()), DscItem=Concat('product__sku', Value('  - ', output_field=CharField()), Value(' (', output_field=CharField()), 'sale__comments',Value(')', output_field=CharField()), output_field=CharField()),QtyItem=F('quantity'),PrcItem=Cast(F('unit_price'), IntegerField()), MontoItem=Cast(F('total'), IntegerField())).values(
					"NmbItem",
					"DscItem",
					"QtyItem",
					"PrcItem",
					"MontoItem",
				)
				total_data = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1).aggregate(MntTotal=Cast(Sum('total'), IntegerField()), TotalPeriodo=Cast(Sum('total'), IntegerField()), VlrPagar=Cast(Sum('total'), IntegerField()))
			# print(products_data)
			index = 0
			for i in products_data:
				i['NroLinDet'] = index + 1
				if "Envío" in i['NmbItem']:
					i.pop('DscItem')
				index += 1
			order_channel = sale.order_channel
			products_data = [x for x in products_data if x['PrcItem'] != 0]

			if order_channel == None:
				order_channel = "No Aplica"
			data_bill = {  
			   "response":[  
			      "JSON",
			      "PDF",
			   ],
			   "dte":{  
			      "Encabezado":{  
			         "IdDoc":{  
			            "TipoDTE":39,
			            "Folio":0,
			            "FchEmis":sale.date_created.strftime("%Y-%m-%d"),
			            "IndServicio":3
			         },
			         "Emisor":DATA_EMITTER,
			         "Receptor":{  
			            "RUTRecep":"66666666-6",
			            "RznSocRecep":sale.channel.name + " " +str(order_channel),
			         },
			         "Totales":total_data
			      },
			      "Detalle":list(products_data)
			   }
			}
			# print(json.dumps(data_bill))
			url_of = OPEN_FACTURA_URL + "/v2/dte/document"
			headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
			response = requests.post(url_of, data=json.dumps(data_bill), headers=headers)
			#print(response.json())
			if response.status_code == 200:
				token = response.json()['TOKEN']
				response = requests.get(OPEN_FACTURA_URL + "/v2/dte/document/" + response.json()['TOKEN'] + "/json", headers=headers).json()
				sale.number_document = response['json']['Encabezado']['IdDoc']['Folio']
				sale.document_type = 'boleta electrónica'
				sale.bill = True
				sale.token_bill = token
				sale.status = "entregado"
				sale.date_document_emision = datetime.datetime.now().date()
				sale.save()
		if products_stock:
			self.alert_stock(products_stock)
		if sale.channel_id == 3:
			total_declared = sale.sale_of_product.aggregate(sum_totals=Sum('total'))
		else:
			total_declared = sale.sale_of_product.exclude(product__sku=1).aggregate(sum_totals=Sum('total'))
		if total_declared['sum_totals'] is not None:
			sale.declared_total = total_declared['sum_totals']
			sale.save()

		return sale

	@transaction.atomic
	def update(self, instance, validated_data):
		validated_data['date_sale'] = datetime.datetime.strptime(validated_data['date_sale'], "%Y-%m-%dT%H:%M:%S.%fZ")
		products = validated_data.pop('products')
		special_register = any(i['sku'] == 2 for i in products)
		validated_data.pop('bill')

		if special_register and (validated_data.get('comments', None) == '' or validated_data.get('comments', None) == None):
			raise serializers.ValidationError({'comments': ["Debe añadir un comentario para el registro de ingresos especiales"]})

		if validated_data['document_type'] is not 'sdt' and validated_data.get('number_document', None) == None:
			raise serializers.ValidationError({'number_document': ['El número de documento es requerido para este documento tributario']})

		if (validated_data.get('order_channel', None) is None) and (validated_data['channel'].name == 'Mercadolibre' or validated_data['channel'].name == 'Ripley' or validated_data['channel'].name == 'Linio' or validated_data['channel'].name == 'Groupon' or validated_data['channel'].name == 'Paris' or validated_data['channel'].name == 'Mercadoshop' or validated_data['channel'].name == 'Cluboferta' or validated_data['channel'].name == 'Falabella'):
			raise serializers.ValidationError({"id_venta": ["Se debe enviar id de venta para el canal seleccionado"]})

		sales_products_id = []
		total_sale = 0
		products_stock = []
		for product in products:
			if Sale_Product.objects.filter(id=product['id_sale']):
				product.pop('sku')
				product.pop('description')
				product.pop('color')
				# product.pop('max_items')
				# print(product)
				sales_products_id.append(product['id_sale'])
				id_sale = product.pop('id_sale')
				id_of_product = product.pop('id_of_product')
				code_seven_digits = product.pop('code_seven_digits')


				sale_product_quantity = Sale_Product.objects.filter(id=id_sale)[0].quantity

				product_sale = Product_Sale.objects.get(id=id_of_product)
				if code_seven_digits is not None and code_seven_digits != '':
					product_code = Product_Code.objects.get(code_seven_digits=code_seven_digits)
					new_sale_product = Sale_Product.objects.filter(id=id_sale).update(**product)

					product_sale.stock = product_sale.stock + sale_product_quantity - product['quantity']

					product_code.quantity = product_code.quantity + sale_product_quantity - product['quantity']
					product_code.save()
				else:
					new_sale_product = Sale_Product.objects.filter(id=id_sale).update(**product)

					product_sale.stock = product_sale.stock + sale_product_quantity - product['quantity']

				product_sale.save()

			else:
				if product['sku'] == 2 or product['sku'] == 1:
					product_sale = Product_Sale.objects.get(id=product['id_of_product'])
					product_code = None
				else:
					product_sale = Product_Sale.objects.get(id=product['id_of_product'])
					product_code = product.get('code_seven_digits', None)
					print(product_code)
					if product_code is not None and product_code != '':
						product_code = Product_Code.objects.get(code_seven_digits=product['code_seven_digits'])
					else:
						product_code = None

				sale_product = Sale_Product.objects.create(
					product=product_sale,
					sale=instance,
					quantity=product['quantity'],
					unit_price=product['unit_price'],
					total=product['total'],
					product_code=product_code
				)
				product_sale.stock = product_sale.stock - sale_product.quantity
				if product_code is not None:
					product_code.quantity = product_code.quantity - sale_product.quantity
					product_code.save()
				product_sale.save()
				#product_code.save()

				sales_products_id.append(sale_product.id)
			total_sale += product['total']
		# Sale_Product.objects.filter(sale=instance).exclude(id__in=sales_products_id).delete()
		for i in Sale_Product.objects.filter(sale=instance):
			if not (i.id in sales_products_id):
				# print(i.id)
				product_sale = Product_Sale.objects.get(id=i.product.id)
				if i.product_code is not None:
					product_code = Product_Code.objects.get(id=i.product_code.id)
					product_code.quantity = product_code.quantity + i.quantity
					product_code.save()
				product_sale.stock = product_sale.stock + i.quantity
				#product_code.quantity = product_code.quantity + i.quantity
				product_sale.save()
				#product_code.save()
				Sale_Product.objects.get(id=i.id).delete()

		for j in Sale_Product.objects.filter(sale=instance):
			if j.product_code is not None:
				quantity_stock_color = j.product_code.quantity
				#Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
				if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not (j.product_code.code_seven_digits in products_stock)):
					products_stock.append(j.product_code.code_seven_digits)
			else:
				if j.product.stock <= 5 and j.product.stock >= 0 and (not (j.product.sku in products_stock)):
					products_stock.append(j.product.sku)
		
		sale = super().update(instance, validated_data)
		if total_sale == validated_data['total_sale']:
			sale.total_sale = total_sale
			sale.save()
		if products_stock:
			self.alert_stock(products_stock)
		return sale


class Product_SaleSKUSerializer(serializers.ModelSerializer):
	category_name = CategoryName(read_only=True, source='*')
	type_sku = TypeSKU(read_only=True, source='*')
	product_code = Product_ColorSerializer(read_only=True, many=True)
	class Meta:
		fields = '__all__'
		model = Product_Sale

class Asigned(serializers.IntegerField):
	def to_representation(self, value):
		a = Product_Channel.objects.filter(product=value).aggregate(asigned=Sum('assigned_amount'))['asigned']
		if a:
			return a
		return 0

class NotAsigned(serializers.IntegerField):
	def to_representation(self, value):
		a = Product_Channel.objects.filter(product=value).aggregate(asigned=Sum('assigned_amount'))['asigned']
		if a:
			return value.stock - a
		return 0

class MercadolibreAsigned(serializers.IntegerField):
	def to_representation(self, value):
		a = Product_Channel.objects.filter(product=value, channel__id=1).aggregate(asigned=Sum('assigned_amount'))['asigned']
		if a:
			return a
		return 0

class RipleyAsigned(serializers.IntegerField):
	def to_representation(self, value):
		a = Product_Channel.objects.filter(product=value, channel__id=3).aggregate(asigned=Sum('assigned_amount'))['asigned']
		if a:
			return a
		return 0

class ParisAsigned(serializers.IntegerField):
	def to_representation(self, value):
		a = Product_Channel.objects.filter(product=value, channel__id=4).aggregate(asigned=Sum('assigned_amount'))['asigned']
		if a:
			return a
		return 0

class FalabellaAsigned(serializers.IntegerField):
	def to_representation(self, value):
		a = Product_Channel.objects.filter(product=value, channel__id=8).aggregate(asigned=Sum('assigned_amount'))['asigned']
		if a:
			return a
		return 0

class LinioAsigned(serializers.IntegerField):
	def to_representation(self, value):
		a = Product_Channel.objects.filter(product=value, channel__id=5).aggregate(asigned=Sum('assigned_amount'))['asigned']
		if a:
			return a
		return 0
class GrouponAsigned(serializers.IntegerField):
	def to_representation(self, value):
		a = Product_Channel.objects.filter(product=value, channel__id=6).aggregate(asigned=Sum('assigned_amount'))['asigned']
		if a:
			return a
		return 0

class Product_ChannelSerializer(serializers.ModelSerializer):
	asigned = Asigned(read_only=True, source='*')
	not_asigned = NotAsigned(read_only=True, source='*')
	mercadolibre_asigned = MercadolibreAsigned(read_only=True, source='*')
	ripley_asigned = RipleyAsigned(read_only=True, source='*')
	paris_asigned = ParisAsigned(read_only=True, source='*')
	falabella_asigned = FalabellaAsigned(read_only=True, source='*')
	linio_asigned = LinioAsigned(read_only=True, source='*')
	groupon_asigned = GrouponAsigned(read_only=True, source='*')
	product_color = Product_ColorSerializer(read_only=True, many=True)

	class Meta:
		model = Product_Sale
		fields = (
			'id',
			'sku',
			'asigned',
			'mercadolibre_asigned',
			'ripley_asigned',
			'paris_asigned',
			'falabella_asigned',
			'linio_asigned',
			'groupon_asigned',
			'not_asigned',
			'stock',
			'product_color'
		)

class ProductsRetailSale(serializers.ListField):
	def to_representation(self, value):
		products = []
		for product in Sale_Product.objects.filter(sale=value).values('id','product__sku', 'product__description', 'unit_price', 'quantity', 'total', 'product_code__code_seven_digits'):
			products.append(product)
		return products

class WarningLabel(serializers.BooleanField):
	def to_representation(self, value):
		shipping_data = Sale_Product.objects.filter(sale=value, product__sku=1)
		if shipping_data:
			if shipping_data[0].total > 7001 and value.channel.name == 'Mercadolibre':
				return True
			else:
				return False
		return False

class RetailSalesSerializer(serializers.ModelSerializer):
	products = ProductsRetailSale(read_only=True, source='*')
	channel = ChannelName(read_only=True, source='*')
	total_declared = TotalDeclared(read_only=True, source='*')
	warning_label = WarningLabel(read_only=True, source='*')
	dimensions_sale = DimesionSaleSerializer(read_only=True)
	class Meta:
		model = Sale
		fields = (
			'id', 
			'channel',
			'date_sale',
			'order_channel',
			'total_sale',
			# 'sku',
			# 'description',
			'document_type',
			'number_document',
			'payment_type',
			'products',
			'total_declared',
			'bill',
			'warning_label',
			'sender_responsable',
			'comments',
			'channel_status',
			'hour_sale_channel',
			'shipping_status',
			'shipping_type_sub_status',
			'shipping_date',
			'dimensions_sale',
			'bill_fact',
		)

class UserCreatedName(serializers.CharField):
	def to_representation(self, value):
		if value.user_created == None:
			return "-"
		return value.user_created.first_name + " " + value.user_created.last_name

class DateCreatedFormat(serializers.CharField):
	def to_representation(self, value):
		if value.report_file_name is not None:
			return str(value.report_file_name.replace("Manifiesto ", ""))
		return ""

class Sales_ReportSerializer(serializers.ModelSerializer):
	user_created_name = UserCreatedName(read_only=True, source='*')
	date_created_format = DateCreatedFormat(read_only=True, source='*')
	class Meta:
		model = Sales_Report
		fields = '__all__'


class Product_Code_CreateSerializer(serializers.ModelSerializer):
	color_description = serializers.CharField(write_only=True)

	class Meta:
		fields = (
			'code_seven_digits',
			'color_description',
			'product',
			)
		model = Product_Code
	def create(self, validated_data):
		if Product_Code.objects.filter(code_seven_digits=validated_data['code_seven_digits']).count() > 0:
			raise serializers.ValidationError("El Código de 7 dígitos " + str(validated_data['code_seven_digits']) + " ya se encuentra registrado.")
		color = Color.objects.filter(description=validated_data['color_description'])
		if color:
			color = color[0]
		else:
			color = Color.objects.create(description=validated_data['color_description'])
		product_code = Product_Code.objects.create(
			code_seven_digits=validated_data['code_seven_digits'],
			product=validated_data['product'],
			color=color,
		)
		return product_code

	def update(self, instance, validated_data):
		if Product_Code.objects.filter(code_seven_digits=validated_data['code_seven_digits']).exclude(code_seven_digits=instance.code_seven_digits).count() > 0:
			raise serializers.ValidationError("El Código de 7 dígitos " + str(validated_data['code_seven_digits']) + " ya se encuentra asignado a otro producto.")
		instance.code_seven_digits = validated_data.get('code_seven_digits', instance.code_seven_digits)
		color = Color.objects.filter(description=validated_data['color_description'])
		if color:
			color = color[0]
		else:
			color = Color.objects.create(description=validated_data['color_description'])
		instance.color = color
		instance.save()
		return instance
class SKUAsiamerica(serializers.CharField):
	def to_representation(self, value):
		if value.product_ripley is not None:
			return value.product_ripley.sku
		if value.pack_ripley is not None:
			return value.pack_ripley.sku_pack
		return '-'

class Product_SKU_RipleySerializer(serializers.ModelSerializer):
	sku_asiamerica = SKUAsiamerica(read_only=True, source='*')
	channel_name = ChannelName(read_only=True, source='*')
	class Meta:
		model = Product_SKU_Ripley
		fields = '__all__'

class Product_Code_Search(serializers.ListField):
	def to_representation(self, value):
		return Product_Code.objects.filter(product=value).values('code_seven_digits', 'color__description')
class Product_SaleSerializer2(serializers.ModelSerializer):
	product_code = Product_Code_Search(read_only=True, source='*')
	class Meta:
		fields = '__all__'
		model = Product_Sale
