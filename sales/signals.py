from django.db.models.signals import pre_save          
from django.dispatch import receiver
from .models import Sale
# from django.contrib.auth import get_user_model
import inspect
# User = get_user_model()

@receiver(pre_save, sender=Sale)
def my_handler(sender, instance, **kwargs):
	# print(**kwargs)
    for frame_record in inspect.stack():
        if frame_record[3]=='get_response':
            request = frame_record[0].f_locals['request']
            break
    else:
        request = None
    if request is not None:
        if (not request.user.is_anonymous):
            instance.last_user_update = request.user
            # instance.save()