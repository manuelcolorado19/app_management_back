import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sales.models import Product_Sale, Color, Product_Code, Pack, Pack_Products, Product_SKU_Ripley
from django.db.models import Q
from categories.models import Channel
wb = load_workbook('sales/files/I3 - 22-02-2019 (05-03-2019).xlsx')
ws = wb['BASE INVENTARIO']

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		color = Color.objects.filter(description=row[3].value)
		if color:
			continue
		else:
			color = Color.objects.create(
				description=row[3].value
			)