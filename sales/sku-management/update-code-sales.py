import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sales.models import Product_Sale, Color, Product_Code, Pack, Pack_Products, Product_SKU_Ripley, Sale_Product
from django.db.models import Q
from categories.models import Channel
wb = load_workbook('sales/files/Asignacion codigo 7 digitos.xlsx')
ws = wb['Hoja1 (2)']

count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		product_in_sale = Sale_Product.objects.filter(sale__id=row[1].value, product__sku=row[2].value)
		if product_in_sale:
			product_code = Product_Code.objects.filter(code_seven_digits=str(row[11].value))
			if product_code:
				print(str(row[1].value) + " actualizada")
				product_in_sale.update(product_code=product_code[0])
				continue
			else:
				continue
				#print(str(row[11].value) + " no existe")
			#product_in_sale.update(product_code=product_code)
		else:
			continue
