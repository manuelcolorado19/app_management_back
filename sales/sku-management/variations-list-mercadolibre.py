import requests
import xlsxwriter
from notifications.models import Token
wb =  xlsxwriter.Workbook("Lista de Variantes por publicación Mercadolibre.xlsx")
ws = wb.add_worksheet("Variantes")
ws.write(0, 0, "MLC")
ws.write(0, 1, "Descripción")
ws.write(0, 2, "Variantes")
ws.write(0, 3, "SKU")
token = Token.objects.get(id=1).token_ml
url = "https://api.mercadolibre.com/sites/MLC/search?seller_id=216244489&access_token=" + token
url2 = "https://api.mercadolibre.com/items/{}"
# headers = {'Accept': 'application/json', 'Authorization': "f2f69342-5490-40c5-a635-b6e1b0c4df6e"}
# rows = requests.get(url, headers=headers).json()['offers']
data = requests.get(url).json()
if data['paging']['total'] <= 50:
	rows = data["results"]
else:
	rows = []
	count = 0
	iteration_end = int(data['paging']['total']/50)
	module = int(data['paging']['total']%50)
	for i in range(iteration_end + 1):
		new_rows = requests.get(url + "&offset=" + str(i*50)).json()['results']
		rows = rows + new_rows
	rows = rows + requests.get(url + "&offset="+str(((iteration_end*50) + module))).json()['results']
# rows2 = requests.get(url2, headers=headers).json()['offers']
# rows = rows + rows2
row_num = 1
for i in rows:
	# if not i['active']:
	print(i['id'])
	# print(sku_seller)
	# sku_7 = 
	variations = requests.get(url2.format(i['id']) + "?access_token=" + token).json()
	sku_seller = variations.get('attributes', None)
	if sku_seller is not None and len(sku_seller) > 1:
		sku_7 = sku_seller[1]["value_name"]
	else:
		sku_7 = variations['seller_custom_field']
	data_var = ''
	for var in variations['variations']:
		data_var += var['attribute_combinations'][0]['value_name'] + ", "

	ws.write(row_num, 0, str(i['id']))
	ws.write(row_num, 1, str(i['title']))
	ws.write(row_num, 2, str(data_var))
	ws.write(row_num, 3, sku_7)
	row_num +=1
wb.close()

