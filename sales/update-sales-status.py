from sales.models import Sale 
import requests
from notifications.models import Token

sales_ml = Sale.objects.filter(channel__name="Mercadolibre")
token = Token.objects.get(id=1).token_ml

for sale in sales_ml:
	url = "https://api.mercadolibre.com/orders/" + str(sale.order_channel) + "?access_token=" + token
	sale = Sale.objects.get(id=sale.id)
	shipping_type = requests.get(url)
	if shipping_type.status_code != 200:
		continue
	else:
		shipping_type = shipping_type.json()['shipping']['status']
	shipping_status = {
	    'delivered': "Entregado",
	    'shipped': "Enviado",
	    'to_be_agreed': None,
	    'pending': "Pendiente",
	    'ready_to_ship': "Listo para enviar",
	    'handling': "En Manejo",
	    'not_delivered': "No entregado",
	    'not_verified': "No verificado",
	    'cancelled': "Cancelado",
	    'closed': "Cerrado",
	    'error': "Error",
	    'active': "Activo",
	    'not_specified': "No especificado",
	    'stale_ready_to_ship': "Estancado en listo para enviar",
	    'stale_shipped': "Estancado en enviado",
    }[shipping_type]
	sale.shipping_status = shipping_status
	#if shipping_type.status_code != 200:
	#	continue
	sale.save()
	print("Sale update")
