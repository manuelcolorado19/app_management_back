from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import Product_SaleViewSet, SaleViewSet, Sale_ProductViewSet, Product_ChannelViewSet, RetailSales, PackViewSet, Category_Product_SaleViewSet, Product_SaleSKUViewSet, Sales_ReportViewSet, Product_SaleViewSet2, Product_Code_CreateViewSet, Product_SKU_RipleyViewSet

router = DefaultRouter()
router.register(r'product_sale', Product_SaleViewSet, base_name='product_sale')
router.register(r'product_sale2', Product_SaleViewSet2, base_name='product_sale2')
router.register(r'product_sale_sku', Product_SaleSKUViewSet, base_name='product_sale_sku')
router.register(r'sale', SaleViewSet, base_name='sale')
router.register(r'sale_product', Sale_ProductViewSet, base_name='sale_product')
router.register(r'product_sale_channels', Product_ChannelViewSet, base_name='product_sale_channels')
router.register(r'retail-sales', RetailSales, base_name='retail-sales')
router.register(r'pack', PackViewSet, base_name='pack')
router.register(r'category_product', Category_Product_SaleViewSet, base_name='category_product')
router.register(r'sales_report', Sales_ReportViewSet, base_name='sales_report')
router.register(r'product_code', Product_Code_CreateViewSet, base_name='product_code')
router.register(r'sku_channel', Product_SKU_RipleyViewSet, base_name='sku_channel')

urlpatterns = [
	url(r'^', include(router.urls)),
	
]
