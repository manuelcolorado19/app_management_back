from django.shortcuts import render
from .serializers import Product_SaleSerializer, SaleSerializer, Sale_ProductSerializer, Product_ChannelSerializer, RetailSalesSerializer, PackSerializer, Category_Product_SaleSerializer, SkuNewSerializer, Product_SaleSKUSerializer, SkuUpdateSerializer, Sales_ReportSerializer, Product_Code_CreateSerializer, Product_SKU_RipleySerializer, Product_SaleSerializer2, Product_SaleSerializer2
#from .serializers import Product_SaleSerializer, SaleSerializer, Sale_ProductSerializer, Product_SaleStockSerializer, Product_ChannelSerializer, RetailSalesSerializer, PackSerializer, Category_Product_SaleSerializer, SkuNewSerializer, Product_SaleSKUSerializer, SkuUpdateSerializer, Sales_ReportSerializer, Product_Code_CreateSerializer, Product_SKU_RipleySerializer
from rest_framework.viewsets import ModelViewSet
from .models import Product_Sale, Sale, Sale_Product, Product_Channel, Product_Code, Pack, Pack_Products, Category_Product_Sale, Product_SKU_Ripley, Sales_Report, Color, Commission, CommissionChannel, DimensionsSale, SKUCategoryChannel, Product_Sale_Historic_Cost, CategoryTree, OperationSpending, RetailCreditIvaSpend, Product_Sale_Category_Retail, ClientSale
from categories.models import Channel
from rest_framework.permissions import AllowAny, IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as filters
import xlwt
import xlsxwriter
from rest_framework.views import APIView
from django.contrib.postgres.aggregates.general import ArrayAgg
import time, decimal
from datetime import date, timedelta
import io
from django.http import HttpResponse
from django.db.models import F, Case, When, CharField, Value, IntegerField, DurationField, Q, Sum, Value, Count
from django.apps import apps
from django.db.models.functions import Cast, Concat, Length
from django.db.models.fields import DateField, FloatField
from rest_framework.pagination import PageNumberPagination
import datetime
from rest_framework.response import Response
from rest_framework import filters as search_filter
from .permissions import SalesPermissions, Product_SalePermissions
from rest_framework import status
import os, base64
from django.db import transaction
import itertools
from operator import itemgetter
from notifications.models import Token, CustomMessageProduct
import requests
import json
from OperationManagement.settings import API_KEY_OF, OPEN_FACTURA_URL, DATA_EMITTER, API_KEY_RP, API_KEY_LINIO, USER_ID, DATA_EMITTER_SHIPPING_GUIDE, DATA_RECEIVER, DATA_EMITTER_BILL, DATA_RECEIVER_BILL
from wsgiref.util import FileWrapper
from .filters import SalesFilter, SalesFilterByDate
import urllib
from hashlib import sha256
from io import BytesIO
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle, Spacer, Indenter
from reportlab.graphics.shapes import Line, Drawing
from reportlab.lib.units import inch, mm
from django.http import HttpResponse
from reportlab.pdfgen import canvas
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER, TA_RIGHT
from reportlab.platypus import ListFlowable, ListItem
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table
from hmac import HMAC
from django.contrib.auth import get_user_model
from PyPDF2 import PdfFileReader, PdfFileMerger
import urllib.request
import math
import locale, calendar
import pandas as pd
from django.db.models.functions import TruncMonth, TruncYear, TruncDay, TruncWeek, ExtractWeek, ExtractYear, ExtractMonth, ExtractWeekDay
from django.db.models.functions.text import Substr
import dateutil.relativedelta
from prices.models import Price_Product
from django.core.mail import send_mail
# from datetime import datetime
from django.template.loader import render_to_string
from django.core.mail import send_mail
from mechanize import Browser
from bs4 import BeautifulSoup as BS
import re
from os.path import isfile, join
from os import listdir
User = get_user_model()
def nth_repl(s, sub, repl, nth):
	find = s.find(sub)
	# if find is not p1 we have found at least one match for the substring
	i = find != -1
	# loop util we find the nth or we find no match
	while find != -1 and i != nth:
		# find + 1 means we start at the last match start index + 1
		find = s.find(sub, find + 1)
		i += 1
	# if i  is equal to nth we found nth matches so replace
	if i == nth:
		return s[:find]+repl+s[find + len(sub):]
	return s

class CustomPagination(PageNumberPagination):
	page_size = 50
	page_size_query_param = 'page_size'
	max_page_size = 50
	def get_paginated_response(self, data):
		today_date = datetime.datetime.now()
		return Response({
		   'next': self.get_next_link(),
		   'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'results': data,
			'today_date': today_date
		})

class PackViewSet(ModelViewSet):
	queryset = Pack.objects.all().order_by('sku_pack')
	serializer_class = PackSerializer
	permission_classes = (IsAuthenticated,)
	filter_backends = (search_filter.SearchFilter,)
	pagination_class = CustomPagination
	http_method_names = ['get',]
	lookup_field = 'sku_pack'
	search_fields  = ('^sku_pack', '^description')

class Category_Product_SaleViewSet(ModelViewSet):
	queryset = Category_Product_Sale.objects.all()
	serializer_class = Category_Product_SaleSerializer
	permission_classes = (IsAuthenticated,)
	http_method_names = ['get',]

class NoPaginationSearch(PageNumberPagination):
	page_size = 50
	page_size_query_param = 'page_size'
	max_page_size = 50
	def get_paginated_response(self, data):
		if self.request.query_params.get('search', None) is not None:
			self.page_size = 50
			self.max_page_size = 50
		else:
			self.page_size = 10
			self.max_page_size = 10
		return Response({
		   'next': self.get_next_link(),
		   'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'results': data,
		})

class Product_SaleViewSet2(ModelViewSet):
	queryset = Product_Sale.objects.all().order_by('sku')
	serializer_class = Product_SaleSerializer2
	permission_classes = (Product_SalePermissions,)
	pagination_class = NoPaginationSearch
	filter_backends = (search_filter.SearchFilter,)
	http_method_names  = ['get',]
	search_fields  = ('description', 'sku',)

class Product_SaleViewSet(ModelViewSet):
	queryset = Product_Sale.objects.all()
	serializer_class = Product_SaleSerializer
	permission_classes = (Product_SalePermissions,)
	pagination_class = NoPaginationSearch
	filter_backends = (search_filter.SearchFilter,)
	http_method_names  = ['get', 'post', 'put']
	search_fields  = ('=sku', '=product_code__code_seven_digits',)

	@transaction.atomic
	def create(self, request):
		# print(request.data)
		serializer = SkuNewSerializer(data=request.data)
		if serializer.is_valid():
			if serializer.data['type_product'] == "Pack":
				# create pack
				if len(serializer.data['products']) == 0:
					return Response(data={"products": ["Debe añadir por lo menos un producto para crear sku de pack"]}, status=status.HTTP_400_BAD_REQUEST)
				else:
					# print(serializer.data)
					pack = Pack.objects.create(
						sku_pack=str(serializer.data['sku']),
						category_pack=Category_Product_Sale.objects.get(id=int(serializer.data['category_product_sale'])),
						description=serializer.data['description'],
					)
					message = CustomMessageProduct.objects.create(
						message='',
						pack=pack,
					)
					for product in serializer.data['products']:
						product_sale = Product_Sale.objects.filter(sku=product['sku'])
						if not product_sale:
							return Response(data={"product": ["No existe el producto con sku " + product['sku']]}, status=status.HTTP_400_BAD_REQUEST)

						product_pack = Pack_Products.objects.create(
							pack=pack,
							product_pack=Product_Sale.objects.get(sku=int(product['sku'])),
							quantity=product['quantity'],
							percentage_price=product['percentage_price'],
						)
					return Response(data={"product_sale": ["SKU creado exitosamente"]}, status=status.HTTP_200_OK)
			request.data.pop('type_product')
			#print(serializer.data)
			product_new = Product_Sale.objects.create(
				sku=serializer.data['sku'],
				description=serializer.data['description'],
				category_product_sale=Category_Product_Sale.objects.get(id=int(serializer.data['category_product_sale'])),
				stock=99999,
				price=99999,
				height=serializer.data['height'],
				width=serializer.data['width'],
				length=serializer.data['length'],
				weight=serializer.data['weight'],
			)
			message = CustomMessageProduct.objects.create(
				message='',
				product=product_new,
			)
			return Response(data={"product_sale": ["SKU creado exitosamente"]}, status=status.HTTP_200_OK)
		else:
			return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	@transaction.atomic
	def update(self, request, pk=None):
		# print(request.data)
		# print(pk)
		serializer = SkuUpdateSerializer(data=request.data)
		if serializer.is_valid():
			if serializer.data['type_product'] == 'Normal':
				sku_old = Product_Sale.objects.get(id=pk)
				if Product_Sale.objects.filter(sku=serializer.data['sku']).exclude(sku=sku_old.sku).count() > 0:
					return Response(data={"sku": ['El SKU ' + str(serializer.data['sku'])+ " ya se encuentra registrado."]}, status=status.HTTP_400_BAD_REQUEST)
				# print(serializer.data)
				sku_old.sku = serializer.data.get('sku', sku_old.sku)
				sku_old.description = serializer.data.get('description', sku_old.description)
				category = serializer.data.get('category_product_sale')
				sku_old.category_product_sale = Category_Product_Sale.objects.get(id=category)
				sku_old.height = serializer.data.get('height')
				sku_old.length = serializer.data.get('length')
				sku_old.width = serializer.data.get('width')
				sku_old.weight = serializer.data.get('weight')

			else:
				sku_old = Pack.objects.get(id=pk)
				if Pack.objects.filter(sku_pack=serializer.data['sku']).exclude(sku_pack=sku_old.sku_pack).count() > 0:
					return Response(data={"sku": ['El SKU ' + str(serializer.data['sku'])+ " ya se encuentra registrado."]}, status=status.HTTP_400_BAD_REQUEST)
				sku_old.sku_pack = serializer.data.get('sku', sku_old.sku_pack)
				sku_old.description = serializer.data.get('description', sku_old.description)
				category = serializer.data.get('category_product_sale')
				sku_old.category_pack = Category_Product_Sale.objects.get(id=category)
				pack_products_id = []
				for product in serializer.data['products']:
					id_product = product.get('id', None)
					if id_product is not None:
						if Pack_Products.objects.filter(id=id_product):
							pack_products_id.append(id_product)
							product_pack = Pack_Products.objects.get(id=id_product)
							product_pack.quantity = product['quantity']
							product_pack.percentage_price = product['percentage_price']
							product_pack.save()
					else:
						product_pack = Pack_Products.objects.create(
							pack=sku_old,
							product_pack=Product_Sale.objects.get(sku=product['sku']),
							quantity=product['quantity'],
							percentage_price=product['percentage_price'],
						)
						pack_products_id.append(product_pack.id)
				Pack_Products.objects.filter(pack=sku_old).exclude(id__in=pack_products_id).delete()
				# for i in Pack_Products.objects.filter(pack=sku_old):
				# 	if not (i.id in pack_products_id):
				# 		Pack_Products.objects.get(id=i.id).delete()
			sku_old.save()
			# print(serializer.data)
			return Response(data={"sku": ['SKU actualizado exitosamente']}, status=status.HTTP_200_OK)
		else:
			return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Product_SaleSKUViewSet(ModelViewSet):
	queryset = Product_Sale.objects.all().order_by('sku')
	serializer_class = Product_SaleSKUSerializer
	permission_classes = (Product_SalePermissions,)
	filter_backends = (search_filter.SearchFilter,)
	pagination_class = CustomPagination
	http_method_names  = ['get',]
	lookup_field = 'sku'
	search_fields  = ('^sku', 'description')


class DeleteSKU(APIView):
	permission_classes = (Product_SalePermissions,)

	def delete(self, pk):
		sku_id = self.request.query_params.get('sku_id', None)
		if sku_id is not None:
			sales = Sale.objects.filter(Q(sale_of_product__product__sku=sku_id) | Q(sale_of_product__product_code__code_seven_digits=sku_id))
			if sales:
				return Response(data={"sku": ['Disculpe el sku posee '+sku_id+' ventas histórica, contacte administrador para reasignación']}, status=status.HTTP_400_BAD_REQUEST)
			else:
				product_sale = Product_Sale.objects.filter(sku=sku_id)
				if product_sale:
					Product_Code.objects.filter(product__sku=sku_id).delete()
					product_sale.delete()
				else:
					product_code = Product_Code.objects.filter(code_seven_digits=sku_id)
					if product_code:
						product_code.delete()

				return Response(status=status.HTTP_204_NO_CONTENT)


class CustomPaginationStock(PageNumberPagination):
	page_size = 400
	page_size_query_param = 'page_size'
	max_page_size = 400
	def get_paginated_response(self, data):
		today_date = datetime.datetime.now()
		return Response({
		   'next': self.get_next_link(),
		   'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'results': data,
		})


class SaleViewSet(ModelViewSet):
	queryset = Sale.objects.all().order_by('-id')
	serializer_class = SaleSerializer
	permission_classes = (SalesPermissions,)
	filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter,)
	filter_class = SalesFilterByDate
	filter_fields = ('date_update__date', 'channel' )
	pagination_class = CustomPagination
	search_fields  = ('^channel__name', '^order_channel', '^user__first_name', '^user__last_name', '=id','=sale_of_product__product__sku', '=number_document', '=sale_of_product__product_code__code_seven_digits', '=tracking_number')
#	search_fields  = ('^channel__name', '^order_channel', '^user__first_name', '^user__last_name', '=id','^sale_of_product__product__sku','^number_document')

	def get_queryset(self):
		if self.request.query_params.get('return', None) == 'yes':
			return self.queryset.filter()
		if self.request.query_params.get('date_update__date', None):
			query_set = self.queryset.filter(user=self.request.user)
		else:
			if self.request.user.is_superuser:
				query_set = self.queryset.filter()
			else:
				if self.action == 'list':
					query_set = self.queryset.filter(Q(last_user_update=self.request.user) | Q(user=self.request.user))
				else:
					query_set = self.queryset.filter()

		if self.request.query_params.get('no_code', None) is not None:
			if no_code == 'true':
				query_set = self.queryset.filter(sale_of_product__product_code__isnull=True)
			else:
				query_set = self.queryset.filter(sale_of_product__product_code__isnull=False)
		return query_set

	def partial_update(self, request, pk=None):

		if self.request.query_params.get('flag', None) is not None:
			# print(request.data)
			rut_data = request.data.get("rut", None)
			if rut_data is not None and rut_data != "":
				client = ClientSale.objects.filter(receiver_rut=rut_data)
				if client:
					client_data = client[0]
				else:
					headers = {"apikey": API_KEY_OF}
					open_factura_rut_data = requests.get(OPEN_FACTURA_URL + "/v2/dte/taxpayer/" + rut_data, headers=headers)
					if open_factura_rut_data.status_code == 200:
						data_json_rut = open_factura_rut_data.json()
						client_data = ClientSale.objects.create(
							receiver_rut=data_json_rut['rut'],
							social_reason=data_json_rut['razonSocial'],
							sale_description=data_json_rut['actividades'][0]['giro'],
							address=data_json_rut.get('direccion', None),
							comune_receiver=data_json_rut.get('comuna', None),
							contact=data_json_rut.get('telefono', None),
							email=request.data.get("email", None),
						)
					else:
						return Response(data={"rut": ['Disculpe los datos del R.U.T no coinciden con ninguno de los registros en la Base de Datos']}, status=status.HTTP_400_BAD_REQUEST)
			else:
				return Response(data={"rut": ['El campo R.U.T es obligatorio']}, status=status.HTTP_400_BAD_REQUEST)
			sale = Sale.objects.filter(id=pk).update(bill_fact=True, client_data=client_data)
			return Response(data={"sale": ['Factura programada para la venta de forma exitosa']}, status=status.HTTP_200_OK)


		sale = Sale.objects.get(id=pk)
		sale.declared_total = int(request.data['total_declared'])
		sale.save()
		return Response(data={"sale": ['Venta actualizada exitosamente']}, status=status.HTTP_200_OK)

	@transaction.atomic
	def destroy(self, request, pk=None):
		try:
			sale = Sale.objects.get(id=pk)
			products = Sale_Product.objects.filter(sale=sale)
			for product in products:
				product_sale = Product_Sale.objects.get(id=product.product.id)
				product_sale.stock = product_sale.stock + product.quantity
				if product.product_code is not None:
					product_code = Product_Code.objects.get(id=product.product_code.id)
					product_code.quantity = product_code.quantity + product.quantity
					product_code.save()
				product_sale.save()
			sale.delete()
			return Response(data={"sale": ["Venta eliminada exitosamente"]}, status=status.HTTP_200_OK)
		except Exception as e:
			return Response(data={"sale": ["No existe la venta indicada"]}, status=status.HTTP_400_BAD_REQUEST)

	@transaction.atomic
	def create(self, request):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		# print(serializer.data)
		if serializer.data['bill'] and request.data.get('print_bill', False):
			token_bill = Sale.objects.get(id=serializer.data['id']).token_bill
			url_of = OPEN_FACTURA_URL + "/v2/dte/document/" + token_bill + "/pdf"
			header_open_factura = {'content-type': 'application/json', 'apikey': API_KEY_OF}
			pdf_data = requests.get(url_of, headers=header_open_factura).json()
			newdict={'pdf':pdf_data["pdf"]}
			newdict.update(serializer.data)
			return Response(newdict, status=status.HTTP_201_CREATED, headers=headers)
		else:
			return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
class Sale_ProductViewSet(ModelViewSet):
	queryset = Sale_Product.objects.all()
	serializer_class = Sale_ProductSerializer
	permission_classes = (IsAuthenticated,)

class Product_ChannelViewSet(ModelViewSet):
	queryset = Product_Sale.objects.all().order_by('-sku')
	serializer_class = Product_ChannelSerializer
	permission_classes = (IsAuthenticated,)

	def update(self, request, pk=None, *args, **kwargs):
		serializer = self.serializer_class(data=request.data)
		if serializer.is_valid():
			# print(serializer.data)
			product = Product_Channel.objects.filter(product__id=pk, channel__name='Mercadolibre').update(assigned_amount=request.data['mercadolibre_asigned'])
			product = Product_Channel.objects.filter(product__id=pk, channel__name='Ripley').update(assigned_amount=request.data['ripley_asigned'])
			product = Product_Channel.objects.filter(product__id=pk, channel__name='Paris').update(assigned_amount=request.data['paris_asigned'])
			product = Product_Channel.objects.filter(product__id=pk, channel__name='Linio').update(assigned_amount=request.data['linio_asigned'])
			product = Product_Channel.objects.filter(product__id=pk, channel__name='Groupon').update(assigned_amount=request.data['groupon_asigned'])
			product = Product_Channel.objects.filter(product__id=pk, channel__name='Falabella').update(assigned_amount=request.data['falabella_asigned'])
			return Response(data=request.data, status=status.HTTP_200_OK)
		else:
			return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class RetailSales(ModelViewSet):
	queryset = Sale.objects.all().order_by('-id')
	serializer_class = RetailSalesSerializer
	permission_classes = (IsAuthenticated,)
	http_method_names = ['get', 'put', 'post']
	filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter,)
	filter_class = SalesFilter
	filter_fields = ('status', 'channel__name', 'date_sale', 'sale_of_product__product_code', 'channel_status', 'shipping_type_sub_status')
	# pagination_class = None
	search_fields  = ('^order_channel', '^sale_of_product__product__sku')

	@transaction.atomic
	def update(self, request, pk=None):
		sale = Sale.objects.get(id=pk)
		sale.document_type = request.data.get('document_type', sale.document_type)
		sale.number_document = request.data.get('number_document', sale.number_document)
		sale.payment_type = request.data.get('payment_type', sale.payment_type)
		if request.user.username == 'juan.ureta' and request.data.get('total_declared', None) is not None:
			sale.declared_total = request.data.get('total_declared', None)

		total = 0
		for product in request.data['products']:
			try:
				sale_product = Sale_Product.objects.get(id=product['id'])
				if sale_product.product.sku == 1 or sale_product.product.sku == 2:
					sale_product.product_code = None
				else:
					code_seven_digits = product.get('product_code__code_seven_digits', sale_product.product_code)
					if code_seven_digits is not None:
						sale_product.product_code = Product_Code.objects.get(code_seven_digits=product.get('product_code__code_seven_digits', sale_product.product_code))
				sale_product.unit_price = product.get('unit_price', sale_product.unit_price)
				sale_product.total = int(sale_product.unit_price) * sale_product.quantity
				total += sale_product.total
				sale_product.save()
			except Exception as e:
				return Response(data={"sale_product": ["Debe indicar un código de producto válido"]}, status=status.HTTP_400_BAD_REQUEST)
		if (sale.bill or sale.document_type != 'boleta electrónica') and sale.number_document is not None and Sale_Product.objects.filter(sale=sale, product_code__isnull=True).exclude(product__sku=1).count() == 0:
			sale.status = "entregado"
		sale.total_sale = total
		sale.save()
		return Response(data={"sale": ["Venta actualizada con éxito"]}, status=status.HTTP_200_OK)

	def roundup(self, x):
		return int(math.ceil(x / 100.0)) * 100

	@transaction.atomic
	def create(self, request):
		manifiesto_upload = self.request.query_params.get('manifiesto_upload', None)
		if manifiesto_upload is not None:
			channel_manifiesto = self.request.query_params.get('channel', None)
			if channel_manifiesto is not None:
				if channel_manifiesto == 'Ripley':
					pdf_file = request.data['file_upload']
					read_pdf = PdfFileReader(pdf_file)
					number_of_pages = read_pdf.getNumPages()
					orders = []
					for j in range(number_of_pages):
						page = read_pdf.getPage(j)
						page_content = page.extractText()
						text_pdf = page_content.encode('utf-8').decode('utf-8')
						order_channel = text_pdf[text_pdf.find("REFERENCIA: ")+12:text_pdf.find("REFERENCIA: ")+22].split("-")[0]
						orders.append(order_channel)
					orders_filter = list(set(orders))
					all_sales = list(Sale.objects.filter(order_channel__in=orders_filter, channel_id=3, status="pendiente").values_list('id', flat=True))
				if channel_manifiesto == 'Mercadolibre':
					# pass
					pdf_file = request.data['file_upload']
					read_pdf = PdfFileReader(pdf_file)
					page = read_pdf.getPage(0)
					page_content = page.extractText()
					text_pdf = page_content.encode('utf-8').decode('utf-8')
					count_sales = text_pdf.count('Venta:')
					orders = []
					for i in range(count_sales):
						order_channel = text_pdf[text_pdf.find("Venta:")+6:text_pdf.find("Venta")+16].split("-")[0]
						orders.append(order_channel)
						text_pdf = text_pdf.replace("Venta:" + order_channel, "")
					orders_filter = list(set(orders))
					all_sales = list(Sale.objects.filter(order_channel__in=orders_filter, channel_id=1, status="pendiente").distinct('order_channel').values_list('id', flat=True))
				if channel_manifiesto == 'Linio':
					pdf_file = request.data['file_upload']
					read_pdf = PdfFileReader(pdf_file)
					number_of_pages = read_pdf.getNumPages()
					orders = []
					for j in range(number_of_pages):
						page = read_pdf.getPage(j)
						page_content = page.extractText()
						text_pdf = page_content.encode('utf-8').decode('utf-8')
						count_sales = text_pdf.count('PEDIDO:')
						if count_sales == 1:
							order_channel = text_pdf[text_pdf.find("PEDIDO:")+7:text_pdf.find("PEDIDO:")+16].split("-")[0]
							orders.append(order_channel)
					orders_filter = list(set(orders))
					all_sales = list(Sale.objects.filter(order_channel__in=orders_filter, channel_id=5, status="pendiente").values_list('id', flat=True))
					# print(orders_filter)
					# return Response(data={"sales": ['Hola']})

		else:
			all_sales = [i['id'] for i in request.data]

		merger = PdfFileMerger()
		sales_approved = []
		sales_unapproved = []
		buff = BytesIO()
		today = datetime.datetime.today().strftime("%Y-%m-%d")
		for j in range(len(all_sales)):
			validate_sale = Sale.objects.get(id=all_sales[j])
			if validate_sale.channel.name == 'Falabella':
				return Response(data={"sales":["Disculpe no puede generar boletas para falabella o paris"]}, status=status.HTTP_400_BAD_REQUEST)
		for sale_id in range(len(all_sales)):
			sale = Sale.objects.get(id=all_sales[sale_id])
			if sale.channel.name == 'Ripley':
				products_data = Sale_Product.objects.filter(sale__id=sale.id).annotate(NmbItem=Case(When(Q(product_code=None) & ~Q(product__sku=1),then=Concat(Value('# ', output_field=CharField()), F('product__description'))),default=F('product__description'), output_field=CharField()), DscItem=Concat('product__sku', Value(' - ', output_field=CharField()), Value(' (', output_field=CharField()), Case(When(comment_unit=None, then=F('sale__comments')), default=F('comment_unit'), output_field=CharField()),Value(')', output_field=CharField()), output_field=CharField()),QtyItem=F('quantity'),PrcItem=Cast(F('unit_price'), FloatField()), MontoItem=Cast(F('total'), FloatField())).values(
					"NmbItem",
					"DscItem",
					"QtyItem",
					"PrcItem",
					"MontoItem",
					"id"
				)
				total_data = Sale_Product.objects.filter(sale=sale).aggregate(MntTotal=Cast(Sum('total'), IntegerField()), TotalPeriodo=Cast(Sum('total'), IntegerField()), VlrPagar=Cast(Sum('total'), IntegerField()))
			else:
				products_data = Sale_Product.objects.filter(sale__id=sale.id).exclude(product__sku=1).annotate(NmbItem=Case(When(product_code=None,then=Concat(Value('# ', output_field=CharField()), F('product__description'))),default=F('product__description'), output_field=CharField()), DscItem=Concat('product__sku', Value('  - ', output_field=CharField()), Value(' (', output_field=CharField()), 'sale__comments',Value(')', output_field=CharField()), output_field=CharField()),QtyItem=F('quantity'),PrcItem=Cast(F('unit_price'), FloatField()), MontoItem=Cast(F('total'), FloatField())).values(
					"NmbItem",
					"DscItem",
					"QtyItem",
					"PrcItem",
					"MontoItem",
					"id"
				)
				total_data = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1).aggregate(MntTotal=Cast(Sum('total'), IntegerField()), TotalPeriodo=Cast(Sum('total'), IntegerField()), VlrPagar=Cast(Sum('total'), IntegerField()))
			index = 0
			total_products_no_shipping = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1).aggregate(MntTotal=Cast(Sum('total'), IntegerField()))['MntTotal']

			if sale.bill_fact:
				# Crea la factura y la añade al manifiesto


				if total_products_no_shipping == None:
					continue
				id_sale_association = {}
				for i in products_data:
					index += 1
					i['NroLinDet'] = index
					id_sale_association[str(index)] = i['id']
					if i['MontoItem'] > total_products_no_shipping and not "Envío" in i['NmbItem']:
						i['PrcItem'] = total_products_no_shipping/i['QtyItem']
						i['MontoItem'] = total_products_no_shipping
					else:
						if not i['MontoItem'].is_integer() and not "Envío" in i['NmbItem']:
							i['MontoItem'] = self.roundup(i['MontoItem'])
							i['PrcItem'] = i['MontoItem']/i['QtyItem']
							total_products_no_shipping -= i['MontoItem']
							continue
						else:
							if not "Envío" in i['NmbItem']:
								total_products_no_shipping -= i['MontoItem']
					if "Envío" in i['NmbItem']:
						i.pop('DscItem')
				for i in products_data:
					i.pop('id')
				products_data = [x for x in products_data if x['PrcItem'] != 0]
				new_products_data = []
				new_total_bill_fact = 0
				for u in products_data:
					# Iterar para arreglar los totales y añadir el IVA
					data_new_generate = u
					data_new_generate['PrcItem'] = round((float(data_new_generate['PrcItem']) / 1.19), 3)
					data_new_generate['MontoItem'] = round(data_new_generate['PrcItem'] * data_new_generate['QtyItem'])
					new_products_data.append(data_new_generate)
					
					new_total_bill_fact += u['MontoItem']
				iva = round(new_total_bill_fact * 0.19)
				if sale.client_data is not None:
					data_bill_receiver = {
						"RUTRecep":sale.client_data.receiver_rut,
			            "RznSocRecep":sale.client_data.social_reason,
			            "GiroRecep":sale.client_data.sale_description[:40],
			            "DirRecep":sale.client_data.address,
			            "CmnaRecep":sale.client_data.comune_receiver,
			            "Contacto": sale.client_data.contact,
			            "CorreoRecep": sale.client_data.email,
					}
				else:
					sales_unapproved.append(all_sales[sale_id])
					continue
				# print(data_bill_receiver)
				data_bill = {
				  "response":[
					  "PDF",
					  "FOLIO"
				   ],
				   "dte":{
				  "Encabezado":{
						 "IdDoc":{
							"TipoDTE":33,
							"Folio":0,
							"FchEmis":today,
							"TpoTranCompra":1,
							"TpoTranVenta":1,
							"FmaPago":2
						 },
						 "Emisor":DATA_EMITTER_BILL,
						 "Receptor":data_bill_receiver,
						 "Totales":{
							"MntNeto":new_total_bill_fact,
							"TasaIVA":"19",
							"IVA":iva,
							"MntTotal":new_total_bill_fact + iva,
						 }
					  },
					  "Detalle": new_products_data
					}
				}


				url_of = OPEN_FACTURA_URL + "/v2/dte/document"
				headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
				response = requests.post(url_of, data=json.dumps(data_bill), headers=headers)
				# print(response.json())
				if response.status_code == 200:
					# products_bill_round = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1)
					# for key, value in id_sale_association.items():
					# 	for i in products_data:
					# 		if int(key) == i['NroLinDet']:
					# 			product_data_bill = Sale_Product.objects.get(id=value)
					# 			product_data_bill.quantity = i['QtyItem']
					# 			product_data_bill.unit_price = i['PrcItem']
					# 			product_data_bill.total = i['MontoItem']
					# 			product_data_bill.save()
					sales_approved.append(all_sales[sale_id])
					token = response.json()['TOKEN']
					base_64_pdf = response.json()['PDF']
					sale.number_document = response.json()['FOLIO']
					folio = response.json()['FOLIO']
					sale.document_type = "factura"
					sale.date_document_emision = datetime.datetime.now().date()
					sale.status = "entregado"
					sale.token_bill = token
					sale.save()
					file_name = "Factura " + str(folio) + ".pdf"
					with open(os.path.expanduser('~/Documents/Facturas/' + file_name), 'wb') as fout:
						fout.write(base64.decodestring(base_64_pdf.encode('ascii')))
					file_data = open(os.path.expanduser('~/Documents/Facturas/' + file_name), 'rb')
					merger.append(PdfFileReader(file_data))
				else:
					sales_unapproved.append(all_sales[sale_id])
					continue
			else:
				# Proceso de creación de Boletas normal
				if total_products_no_shipping == None:
					continue
				id_sale_association = {}
				for i in products_data:
					index += 1
					i['NroLinDet'] = index
					id_sale_association[str(index)] = i['id']
					if i['MontoItem'] > total_products_no_shipping and not "Envío" in i['NmbItem']:
						i['PrcItem'] = total_products_no_shipping/i['QtyItem']
						i['MontoItem'] = total_products_no_shipping
					else:
						if not i['MontoItem'].is_integer() and not "Envío" in i['NmbItem']:
							i['MontoItem'] = self.roundup(i['MontoItem'])
							i['PrcItem'] = i['MontoItem']/i['QtyItem']
							total_products_no_shipping -= i['MontoItem']
							continue
						else:
							if not "Envío" in i['NmbItem']:
								total_products_no_shipping -= i['MontoItem']
					if "Envío" in i['NmbItem']:
						i.pop('DscItem')
				for i in products_data:
					i.pop('id')
				products_data = [x for x in products_data if x['PrcItem'] != 0]

				order_channel = sale.order_channel
				if order_channel == None:
					order_channel = "No Aplica"
					social_reason = sale.channel.name + order_channel
				else:
					if sale.tracking_number is not None:
						order_channel = "-OC " +str(order_channel) + "-T " + str(sale.tracking_number)
						social_reason = sale.channel.name + order_channel
					else:
						order_channel = "-OC " +str(order_channel)
						social_reason = sale.channel.name + order_channel
				data_bill = {
				   "response":[
					  "JSON",
					  "PDF"
				   ],
				   "dte":{
					  "Encabezado":{
						 "IdDoc":{
							"TipoDTE":39,
							"Folio":0,
							"FchEmis":today,
							"IndServicio":3
						 },
						 "Emisor":DATA_EMITTER,
						 "Receptor":{
							"RUTRecep":"66666666-6",
							"RznSocRecep":social_reason,
						 },
						 "Totales":total_data
					  },
					  "Detalle":list(products_data)
				   }
				}

				url_of = OPEN_FACTURA_URL + "/v2/dte/document"
				headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
				response = requests.post(url_of, data=json.dumps(data_bill), headers=headers)
				if response.status_code == 200:
					products_bill_round = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1)
					for key, value in id_sale_association.items():
						for i in products_data:
							if int(key) == i['NroLinDet']:
								product_data_bill = Sale_Product.objects.get(id=value)
								product_data_bill.quantity = i['QtyItem']
								product_data_bill.unit_price = i['PrcItem']
								product_data_bill.total = i['MontoItem']
								product_data_bill.save()
					sales_approved.append(all_sales[sale_id])
					token = response.json()['TOKEN']
					base_64_pdf = response.json()['PDF']
					response = requests.get(OPEN_FACTURA_URL + "/v2/dte/document/" + response.json()['TOKEN'] + "/json", headers=headers).json()
					sale.number_document = response['json']['Encabezado']['IdDoc']['Folio']
					folio = response['json']['Encabezado']['IdDoc']['Folio']
					sale.document_type = "boleta electrónica"
					sale.bill = True
					sale.date_document_emision = datetime.datetime.now().date()
					# if sale.bill and sale.number_document is not None and Sale_Product.objects.filter(sale=sale, product_code__isnull=True).exclude(product__sku=1).count() == 0:
					sale.status = "entregado"
					sale.token_bill = token
					sale.save()
					file_name = "Boleta " + str(folio) + ".pdf"
					with open(os.path.expanduser('~/Documents/Boletas/' + file_name), 'wb') as fout:
						fout.write(base64.decodestring(base_64_pdf.encode('ascii')))
					file_data = open(os.path.expanduser('~/Documents/Boletas/' + file_name), 'rb')
					merger.append(PdfFileReader(file_data))
				else:
					sales_unapproved.append(all_sales[sale_id])
					continue



		if not sales_approved:
			print(sales_approved)
			return Response(data={"sales": ["No se pudo generar ninguna de las boletas seleccionadas."]}, status=status.HTTP_400_BAD_REQUEST)
		report = Sales_Report.objects.create(report_file_name=None, user_created=request.user)
		sales_update = Sale.objects.filter(id__in=sales_approved).update(sales_report=report)

		doc = SimpleDocTemplate(buff, pagesize=letter, rightMargin=40, leftMargin=40, topMargin=60, bottomMargin=18,)
		clientes = []
		styles = getSampleStyleSheet()
		products_count_manifiesto = Sale.objects.filter(id__in=sales_approved).aggregate(product_quantity=Sum(Case(When(sale_of_product__product__sku=1, then=Value(0, output_field=IntegerField())), default=F('sale_of_product__quantity'), output_field=IntegerField())))
		header = Paragraph("Listado Productos a Buscar en Bodega (" + str(products_count_manifiesto['product_quantity']) +")", styles['Heading4'])
		style_products = getSampleStyleSheet()
		style_products2 = getSampleStyleSheet()
		styleDescription2 = style_products2["BodyText"]
		styleDescription2.fontSize = 10
		styleDescription2.leading = 10
		normal = style_products['Heading4']
		normal.alignment = TA_RIGHT
		normal.bottomMargin = 5
		products_receive = Paragraph("Productos Recibidos: _______________", normal)
		header_data = [
			[header, products_receive],
		]
		table_header = Table(header_data)
		last_manifiesto = Sales_Report.objects.latest('date_created')
		today = datetime.datetime.today().strftime("%Y-%m-%d %H:%M")
		number_manifiesto = Paragraph("Manifiesto #" + str(report.id), styles['Heading2'])
		date_generate = Paragraph("Fecha: " +str(today), styles['Heading2'])
		clientes.append(number_manifiesto)
		clientes.append(date_generate)
		clientes.append(table_header)
		# clientes.append(products_receive)
		headings = ('SKU', 'Producto','Modelo', 'SKU7','Cant.',"             ")
		a = Sale_Product.objects.filter(sale__id__in=sales_approved).exclude(product__sku=1).annotate(model=Case(When(product_code__isnull=False, then=F('product_code__color__description')), default=Value('por definir'), output_field=CharField()), code_seven=Case(When(product_code__isnull=False, then=F('product_code__code_seven_digits')), default=Value('0000000'), output_field=CharField())).values('product__description', 'quantity', 'product_code__color__description', 'product__sku', 'model', 'code_seven')
		sorted_order = sorted(a, key=itemgetter('product__sku', 'code_seven'))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:(x['product__sku'], x['code_seven'])):
			group_order.append(list(group))
		# print(group_order)
		data = []
		for row in group_order:
			quantity = 0
			for row2 in row:
				quantity += row2['quantity']
			data.append((row[0]['product__sku'], Paragraph(row[0]['product__description'], styleDescription2), Paragraph(row[0]['model'], styleDescription2),row[0]['code_seven'],quantity,"        "))
		allclientes = data
		t = Table([headings] + allclientes, colWidths=["15%","45%","15%",'10%',"5%","10%"])
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (5, -1), 1, colors.dodgerblue),
				('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
				('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
			]
		))
		clientes.append(t)
		styleDescription = style_products["BodyText"]
		styleDescription.fontSize = 5
		styleDescription.leading = 5
		ventas = Sale.objects.filter(id__in=sales_approved).annotate(quantity=Sum(Case(When(sale_of_product__product__sku=1, then=Value(0, output_field=IntegerField())), default=F('sale_of_product__quantity'), output_field=IntegerField())), sub_status=F('shipping_type_sub_status'), description=F('comments'))
		header = Paragraph("Listado Ordenes de Compras (" + str(ventas.count()) + ")", styles['Heading4'])
		clientes.append(header)
		index = 0
		headings = ("N°","Canal",'OC', 'Productos', 'N° de Seguimiento', "Descripción", "             ", "             ")
		allclientes = []
		for p in ventas:
			index += 1
			allclientes.append((index,p.channel.name, p.order_channel, p.quantity, p.tracking_number, Paragraph(p.description, styleDescription), "             ", "             "))
		t = Table([headings] + allclientes, colWidths=["4%","15%","15%","10%","20%","16%","10%","10%"])
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (7, -1), 1, colors.dodgerblue),
				('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
				('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue),
				# ('FONTSIZE', (5, 1), (5, -1), 8),
			]
		))
		clientes.append(t)
		d = Drawing(300, 100)
		d.add(Line(15, 0, 110, 0))
		e = Drawing(600, 0)
		e.add(Line(145, 0, 245, 0))
		f = Drawing(600, 0)
		f.add(Line(280, 0, 375, 0))
		g = Drawing(600, 0)
		g.add(Line(410, 0, 505, 0))
		clientes.append(d)
		clientes.append(e)
		clientes.append(f)
		clientes.append(g)
		pageTextStyleCenter = ParagraphStyle(name="left", alignment=TA_CENTER, fontSize=13, leading=10)
		tbl_data = [
			[Paragraph("Bodega", pageTextStyleCenter), Paragraph("Supervisor", pageTextStyleCenter), Paragraph("Empaque", pageTextStyleCenter), Paragraph("Despacho", pageTextStyleCenter)],

		]
		tbl = Table(tbl_data)
		today = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
		clientes.append(tbl)
		#if sales_unapproved:
		#	text_unapproved = ""
		#	for sale_un in sales_unapproved:
		#		text_unapproved += str(Sale.objects.get(id=sales_unapproved).order_channel) + ", "
		#	note_unapproved=Paragraph("Nota: No se pudieron generar las boletas con número de ordén: " +text_unapproved, styles['Heading4'])
		#	clientes.append(note_unapproved)


		doc.build(clientes)
		files_dir = os.getcwd()
		merger.append(PdfFileReader(buff))
		buff.close()
		# file_manifiesto = "Manifiesto " 
		file_manifiesto = "Manifiesto " + str(today)
		path_file = os.path.expanduser('~/Documents/Manifiestos/' + file_manifiesto + '.pdf')
		merger.write(path_file)
		merger.close()
		short_report = open(path_file, 'rb')
		report_encoded = base64.b64encode(short_report.read())
		report.report_file_name = file_manifiesto
		report.save()
		return Response(data={"pdf_file": report_encoded})

class MassiveRetailDataViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	@transaction.atomic
	def post(self, request):
		sales = []
		sales_products = []
		repeat_order_channel = {}

		for row in request.data:
			if Sale.objects.filter(order_channel=row['order_channel']):
				return Response(data={"order_channel": ["Revisar el id de venta " +str(row["order_channel"]) +" no se puede repetir el id de venta"]}, status=status.HTTP_400_BAD_REQUEST)
			if Product_Sale.objects.filter(sku=row['sku']).count() == 0:
				return Response(data={"sku": ["Revisar el sku " +str(row["sku"]) +" no se encuentra en los registros de productos"]}, status=status.HTTP_400_BAD_REQUEST)

		sorted_order = sorted(request.data, key=itemgetter('order_channel'))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['order_channel']):
			group_order.append(list(group))

		for each in group_order:
			# print(each)
			if each[0]['channel'] == 'Ripley':
				sender_responsable = "comprador"
			else:
				if each[0]['label'] == 'SI':
					sender_responsable = "vendedor"
				elif each[0]['label'] == 'NO':
					sender_responsable = 'comprador'
				else:
					sender_responsable = ""
			date_sale = datetime.datetime.strptime(each[0]['date_register'], "%d-%m-%Y")
			sale = Sale.objects.create(
					order_channel=each[0]['order_channel'],
					channel=Channel.objects.get(name=each[0]['channel']),
					comments=each[0]['comment'],
					date_sale= date_sale,
					total_sale=each[0]['total'],
					user=request.user,
					status='pendiente',
					document_type="",
					payment_type="transferencia retail",
					sender_responsable=sender_responsable,
					hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
			)
			total = 0
			for each_sale in each:
				# print("hola")
				sale_product = Sale_Product.objects.create(
					product=Product_Sale.objects.get(sku=each_sale['sku']),
					sale=sale,
					quantity=each_sale['quantity'],
					unit_price=each_sale['unit_price'],
					total=each_sale['total']
				)
				count_sku7 = Product_Code.objects.filter(product=sale_product.product)
				if count_sku7.count() == 1:
					sale_product.product_code = count_sku7[0]
					sale_product.save()
				total += each_sale['total']

			sale.total_sale = total
			sale.save()

		return Response(data={"retail-data": ["Data cargada con éxito"]}, status=status.HTTP_200_OK)

class RetailXLSExampleViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		file_path = os.path.join(os.path.dirname(os.path.realpath(__name__)), 'Archivo de ejemplo2.xlsx')
		response = HttpResponse(open(file_path, 'rb').read())
		response['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		response['Content-Disposition'] = 'attachment; filename=Archivo de ejemplo.xlsx'
		return response

class SalesXLSViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	def return_commission(self, channel_id):
		if not channel_id in [1,6,13]:
			return 0
		return {
			1: 13,
			6: 30,
			13: 13,
		}[channel_id]

	def payment_day_expected(self, date_sale, channel, payment_type):
		if channel in [8, 4]:
			cut_date = calendar.monthrange(date_sale.year,date_sale.month)[1]
			date_cut = '-'.join(str(x) for x in [date_sale.year, date_sale.month, cut_date])
			datetime_cut = datetime.datetime.strptime(date_cut, '%Y-%m-%d')
			date_payment = datetime_cut + timedelta(days=15)
			return date_payment.date()
		elif channel in [1, 13, 6]:
			if date_sale.weekday() == 6:
				return date_sale + timedelta(days=5)
			idx = (date_sale.weekday() + 1) % 7
			sun = date_sale - timedelta(7 + idx)
			return sun + datetime.timedelta(days=(sun.weekday() + 6), weeks=1)
		elif channel == 5:
			if date_sale.weekday() == 4:
				return date_sale + timedelta(days=10)
			idx = (date_sale.weekday() + 1) % 7
			fri = date_sale - timedelta(2 + idx)
			return fri + timedelta(weeks=1) + timedelta(days=10)
		elif channel == 3:
			day_date_sale = date_sale.day
			if day_date_sale < 12:
				return datetime.date(date_sale.year, date_sale.month, 13)
			elif (day_date_sale < 27):
				return datetime.date(date_sale.year, date_sale.month, 28)
			else:
				if date_sale.month == 12:
					return datetime.date(date_sale.year, 1, 13)
				return datetime.date(date_sale.year, date_sale.month + 1, 13)
		else:
			if payment_type in ['Efectivo', 'Transferencia']:
				if date_sale.weekday() == 4:
					return date_sale + timedelta(days=1)
				idx = (date_sale.weekday() + 1) % 7
				fri = date_sale - timedelta(2 + idx)
				return fri + timedelta(weeks=1) + timedelta(days=1)
			if payment_type in ['Tarjeta de credito', 'Tarjeta de debito', 'Cheque', 'Mixto']:
				return date_sale + timedelta(days=4)
			if payment_type == 'Por pagar':
				cut_date = calendar.monthrange(date_sale.year,date_sale.month)[1]
				date_cut = '-'.join(str(x) for x in [date_sale.year, date_sale.month, cut_date])
				return datetime.datetime.strptime(date_cut, '%Y-%m-%d')
		return ''

	def write_return_row(self, return_data, row_num, ws, cell_format_data, data_re_write, number_columns, columns_no_number, wb, retail_payment_return):
		cell_format_data2 = wb.add_format({'font_size': 8, 'color': "red"})
		cell_format_data2.set_text_wrap()
		cell_format_data3 = wb.add_format({'font_size': 8, 'num_format': '0%', 'color': "red"})
		row = {}
		other_fields = ['sale__order_channel', 'sale__channel__name', 'sale__document_type', 'sale__number_document', 'sale__payment_type', 'sale__comments', 'sale__sender_responsable', 'sale__status', 'sale__status_product', 'sale__date_created', 'sale__hour_sale_channel', 'sale__date_update', 'sale__last_user_update__first_name', 'sale__shipping_type', 'sale__bill', 'sale__shipping_status', 'sale__shipping_type_sub_status', 'sale__cut_inventory', 'sale__shipping_date', 'sale__date_document_emision', 'sale__credit_note_number', 'sale__sales_report__id', 'sale__id', 'sale__total_sale', 'total_sale', 'total_sale2', 'unit_price_int', 'total_sale_declared', 'debit_iva', 'product_cost', 'total_cost_sale', 'credit_iva', 'commission_calculate', 'commission_percentage', 'to_receive', 'utility', 'rentability', 'total_shipping', 'prorated_shipping', 'total_shipping2', 'prorated_shipping2', 'declared_total', 'month_format', 'date_payment', 'total_shipping_prorrated', 'retail_liquidatio_id', "total_sale_retail" ,"id", "commission_percentage_retail", 'contribution_retail', 'total_shipping_discount_ml', 'pay_by_retail', 'status_ryt', 'payment_ryt', 'contribution_ryt','contribution_with_iva', 'status_load_liquidation', 'status_payment_liquidation', 'final_contribution', 'year_format', 'payment_bank_report_date', 'bill_register_date', 'optimal_utility', 'sale__channel_status', 'commission_credit_iva', 'commission_ryt', 'shipping_credit_iva', 'contribution_ryt2', 'final_contribution_percentage', 'final_contribution_percentage_with_ivas', 'result_final_contribution_with_iva', 'result_final_contribution']
				

		for retur in return_data:
			for ret in retur.product_return.filter(reintegrate=True):
				# row['sale__id'] = ret.return_sale.admission_date
				row['sale__date_sale'] = retur.admission_date
				row['product__sku'] = ret.product_sale_return.sku
				row['product__description'] = ret.product_sale_return.description
				row['product__category_product_sale__name'] = ret.product_sale_return.category_product_sale.name
				row['quantity'] = ret.quantity
				if ret.product_sale_code_return is not None:
					row['product_code__code_seven_digits'] = int(ret.product_sale_code_return.code_seven_digits)
					row['product_code__color__description'] = ret.product_sale_code_return.color.description
				else:
					row['product_code__code_seven_digits'] = None
					row['product_code__color__description'] = None
				row['movement_type'] = "Devolución"
				row['sale__user__first_name'] = retur.user.first_name
				row['status_sale_return'] = "Con observación"
				row['iva_sii'] = None
				row['credit_iva_retail'] = None
				row['prorrated_operation_spending'] = None
				row['status_sale_return'] = "Con observación (Devolución)"
				if ret.sale_product_asociation is not None:
					for y in data_re_write:
						if y['id'] == ret.sale_product_asociation_id:
							# Seleccionar y para reescritura de datos
							for u in other_fields:
								row[u] = y[u]
							# retail_payment_return = RetailPaymentDetail.objects.filter(sale_id=row['sale__id'], movement_type='egreso')
							if retail_payment_return:
								row['bill_register'] = retail_payment_return[0].retail_payment.bill_register
								if (row['sale__total_sale'] - row['total_shipping']) != 0:
									row['discount_commission'] = (row['total_sale2'] * retail_payment_return[0].discount_commission) / (row['sale__total_sale'] - row['total_shipping'])
								else:
									row['discount_commission'] = 0
								if row['sale__total_sale'] - row['total_shipping'] != 0:
									row['discount_shipping'] = (row['total_sale2'] * retail_payment_return[0].discount_shipping) / (row['sale__total_sale'] - row['total_shipping'])
								else:
									row['discount_shipping'] = 0

								row['discount_product'] = None
								row['status_commission'] = "Iguales" if int(row['commission_calculate']) == (int(row['discount_commission']) * (-1)) else "Distintos"
								row['status_shipping'] = "Iguales" if int(row['prorated_shipping']) == (int(row['discount_shipping']) * (-1)) else "Distintos"
								row['status_product_bill'] = None
								row['shipping_bill_number'] = retail_payment_return[0].retail_payment.shipping_bill_number
								row['commission_bill_number'] = retail_payment_return[0].retail_payment.commission_bill_number
								if retail_payment_return[0].retail_payment.bank_register is not None:
									row['bank_register'] = retail_payment_return[0].retail_payment.bank_register.bank_register
								else:
									row['bank_register'] = None
							else:
								row['bill_register'] = None
								row['discount_commission'] = None
								row['discount_shipping'] = None
								row['discount_product'] = None
								row['status_commission'] = None
								row['status_shipping'] = None
								row['status_product_bill'] = None
								row['shipping_bill_number'] = None
								row['commission_bill_number'] = None
								row['bank_register'] = None

				for col_num in number_columns:
					if row[col_num] is not None:
						if isinstance(row[col_num], int) or isinstance(row[col_num], float) or isinstance(row[col_num], decimal.Decimal):
							if (row[col_num] == 0 and col_num != 'rentability' and col_num != 'commission_percentage') or (col_num in columns_no_number):
								ws.write_number(row_num, number_columns[col_num], row[col_num], cell_format_data2)
							elif col_num == 'rentability' or col_num == 'commission_percentage' or col_num == 'commission_percentage_retail' or col_num == 'final_contribution_percentage':
								ws.write_number(row_num, number_columns[col_num], row[col_num], cell_format_data3)
							else:
								ws.write_number(row_num, number_columns[col_num], row[col_num], cell_format_data)
						else:
							ws.write(row_num, number_columns[col_num], str(row[col_num]), cell_format_data)
					else:
						ws.write(row_num, number_columns[col_num], "-", cell_format_data)
				row_num += 1
		return row_num



	def get(self, request):
		date_filter = self.request.query_params.get('date_filter', None)
		all_columns = self.request.query_params.get('all_columns', None)
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Informe de registro de ventas ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		Return = apps.get_model('returns', 'Return')
		RetailPaymentDetail = apps.get_model('retailPayment', 'RetailPaymentDetail')
		initial_args = [
			'sale__date_sale',
			'sale__id',
			'product__sku',
			'product__description',
			'quantity',
			'unit_price_int',
			'total_sale',
			'total_sale_declared',
			'sale__user__first_name',
			'product_code__code_seven_digits',
			'sale__order_channel',
			'sale__channel__name',
			'sale__document_type',
			'sale__number_document',
			'sale__payment_type',
			'sale__comments',
			'sale__sender_responsable',
			'sale__status',
			'sale__status_product',
			'sale__date_created',
			'sale__hour_sale_channel',
			'sale__date_update',
			'sale__last_user_update__first_name',
			'sale__shipping_type',
			'sale__bill',
			'sale__channel_status',
			'sale__shipping_status',
			'sale__shipping_type_sub_status',
			'sale__cut_inventory',
			'sale__shipping_date',
			'product_code__color__description',
			'product__category_product_sale__name',
			'month_format',
			'year_format',
			'sale__date_document_emision',
			'sale__credit_note_number',
			'sale__sales_report__id',
			'movement_type',
			'id',
		]
		if all_columns is not None:
			values_args = [
				'sale__date_document_emision',
				'product_cost',
				'total_cost_sale',
				'credit_iva',
				'sale__channel__id',
				'commission_calculate',
				'commission_percentage',
				'to_receive',
				'utility',
				'rentability',
				'sale__sales_report__id',
				'sale__credit_note_number',
				'total_shipping',
				'prorated_shipping',
				'total_shipping2',
				'prorated_shipping2',
				'sale__total_sale',
				'declared_total',
				'debit_iva',
				'total_sale2',
				'date_payment',
				'sale__product_quantity_api',
				'total_shipping_prorrated',
				'retail_liquidatio_id',
				'total_sale_retail',
				'commission_percentage_retail',
				'contribution_retail',
				'total_shipping_discount_ml',
				'pay_by_retail',
				'prorrated_operation_spending',
				'credit_iva_retail',
				'status_ryt',
				'payment_ryt',
				'contribution_ryt',
				'contribution_with_iva',
				'status_load_liquidation',
				'status_payment_liquidation',
				'status_sale_return',
				'iva_sii',
				'final_contribution',
				'payment_bank_report_date',
				'bill_register_date',
				'optimal_utility',
				'commission_credit_iva',

				'commission_ryt',
				'shipping_credit_iva',
				'contribution_ryt2',
				'final_contribution_percentage',
				'final_contribution_percentage_with_ivas',
				'result_final_contribution_with_iva',
				'result_final_contribution',
			]

		else:
			values_args = []

		if date_filter == None and (request.user.is_superuser or request.user.username == 'abel.mejias'):
			rows = Sale_Product.objects.annotate(sale__date_sale=Cast('sale__date_sale', DateField()), total_sale=Cast('total', IntegerField()),total_sale2=Cast('total', IntegerField()), unit_price_int=Cast('unit_price', IntegerField()),total_sale_declared=Cast('total', IntegerField()), debit_iva=Value(0, output_field=IntegerField()), product_cost=Value(0, output_field=IntegerField()), total_cost_sale=Value(0, output_field=IntegerField()), credit_iva=Value(0, output_field=IntegerField()), commission_calculate=Value(0, output_field=IntegerField()), commission_percentage=Value(0, output_field=IntegerField()), to_receive=Value(0, output_field=IntegerField()), utility=Value(0, output_field=IntegerField()), rentability=Value(0, output_field=IntegerField()), total_shipping=Value(0, output_field=IntegerField()), prorated_shipping=Value(0, output_field=IntegerField()), total_shipping2=Value(0, output_field=IntegerField()), prorated_shipping2=Value(0, output_field=IntegerField()), declared_total=Value(0, output_field=IntegerField()), month_format=Value("", output_field=CharField()), date_payment=Value("", output_field=CharField()), product_code__code_seven_digits=Cast('product_code__code_seven_digits', IntegerField()), movement_type=Value("Venta",output_field=CharField()), total_shipping_prorrated=Value(0, output_field=IntegerField()), retail_liquidatio_id=Value(0, output_field=IntegerField()), total_sale_retail=Value(0, output_field=IntegerField()), commission_percentage_retail=Value(0, output_field=IntegerField()), contribution_retail=Value(0, output_field=IntegerField()), total_shipping_discount_ml=Value(0, output_field=IntegerField()), pay_by_retail=Value(0, output_field=IntegerField()), prorrated_operation_spending=Value(0, output_field=IntegerField()), credit_iva_retail=Value(0, output_field=IntegerField()), status_ryt=Value("", output_field=CharField()), payment_ryt=Value(0, output_field=IntegerField()), contribution_ryt=Value(0, output_field=IntegerField()), contribution_with_iva=Value(0, output_field=IntegerField()), status_load_liquidation=Value("", output_field=CharField()), status_payment_liquidation=Value("", output_field=CharField()), status_sale_return=Case(When(sale__status='pendiente', then=Value('Pendiente')), When(sale__status='cancelado', then=Value('Cancelada')), default=Value("Normal"), output_field=CharField()), iva_sii=Value(0, output_field=IntegerField()),final_contribution=Value(0, output_field=IntegerField()), year_format=Value("", output_field=CharField()), payment_bank_report_date=Value("", output_field=CharField()), bill_register_date=Value("", output_field=CharField()), optimal_utility=Value(0, output_field=IntegerField()), commission_credit_iva=Value(0, output_field=IntegerField()), commission_ryt=Value(0, output_field=IntegerField()), shipping_credit_iva=Value(0, output_field=IntegerField()), contribution_ryt2=Value(0, output_field=IntegerField()), final_contribution_percentage=Value(0, output_field=IntegerField()), final_contribution_percentage_with_ivas=Value(0, output_field=IntegerField()), result_final_contribution_with_iva=Value("", output_field=CharField()), result_final_contribution=Value("", output_field=CharField())).values(
				*initial_args,
				*values_args,
				).order_by('-sale__id')
		else:
			if date_filter is not None:
				rows = Sale_Product.objects.filter(sale__user=request.user, sale__date_update=date_filter,).annotate(sale__date_sale=Cast('sale__date_sale', DateField()), total_sale=Cast('total', IntegerField()), total_sale_declared=Cast('total', IntegerField()), unit_price_int=Cast('unit_price', IntegerField()), debit_iva=Value(0, output_field=IntegerField()), product_cost=Value(0, output_field=IntegerField()), total_cost_sale=Value(0, output_field=IntegerField()), credit_iva=Value(0, output_field=IntegerField()), commission_calculate=Value(0, output_field=IntegerField()), commission_percentage=Value(0, output_field=IntegerField()), to_receive=Value(0, output_field=IntegerField()), utility=Value(0, output_field=IntegerField()), rentability=Value(0, output_field=IntegerField()), total_shipping=Value(0, output_field=IntegerField()), prorated_shipping=Value(0, output_field=IntegerField()), declared_total=Value(0, output_field=IntegerField()),total_sale2=Cast('total', IntegerField()), total_shipping2=Value(0, output_field=IntegerField()), prorated_shipping2=Value(0, output_field=IntegerField()), month_format=Value("", output_field=CharField()), date_payment=Value("", output_field=CharField()), product_code__code_seven_digits=Cast('product_code__code_seven_digits', IntegerField()), movement_type=Value("Venta",output_field=CharField()), total_shipping_prorrated=Value(0, output_field=IntegerField()), retail_liquidatio_id=Value(0, output_field=IntegerField()), total_sale_retail=Value(0, output_field=IntegerField()), commission_percentage_retail=Value(0, output_field=IntegerField()), contribution_retail=Value(0, output_field=IntegerField()), total_shipping_discount_ml=Value(0, output_field=IntegerField()), pay_by_retail=Value(0, output_field=IntegerField()), prorrated_operation_spending=Value(0, output_field=IntegerField()), credit_iva_retail=Value(0, output_field=IntegerField()), status_ryt=Value("", output_field=CharField()), payment_ryt=Value(0, output_field=IntegerField()), contribution_ryt=Value(0, output_field=IntegerField()), contribution_with_iva=Value(0, output_field=IntegerField()), status_load_liquidation=Value("", output_field=CharField()), status_payment_liquidation=Value("", output_field=CharField()), status_sale_return=Case(When(sale__status='pendiente', then=Value('Pendiente')), When(sale__status='cancelado', then=Value('Cancelada')), default=Value("Normal"), output_field=CharField()), iva_sii=Value(0, output_field=IntegerField()), final_contribution=Value(0, output_field=IntegerField()), year_format=Value("", output_field=CharField()), payment_bank_report_date=Value("", output_field=CharField()), bill_register_date=Value("", output_field=CharField()), optimal_utility=Value(0, output_field=IntegerField()), commission_credit_iva=Value(0, output_field=IntegerField()), commission_ryt=Value(0, output_field=IntegerField()), shipping_credit_iva=Value(0, output_field=IntegerField()), contribution_ryt2=Value(0, output_field=IntegerField()), final_contribution_percentage=Value(0, output_field=IntegerField()), final_contribution_percentage_with_ivas=Value(0, output_field=IntegerField()), result_final_contribution_with_iva=Value("", output_field=CharField()), result_final_contribution=Value("", output_field=CharField())).values(
					*initial_args,
					*values_args,
				).order_by('-sale__id')
			else:
				rows = Sale_Product.objects.filter(sale__user=request.user,).annotate(sale__date_sale=Cast('sale__date_sale', DateField()), total_sale=Cast('total', IntegerField()),total_sale_declared=Cast('total', IntegerField()), unit_price_int=Cast('unit_price', IntegerField()), product_cost=Value(0, output_field=IntegerField()), total_cost_sale=Value(0, output_field=IntegerField()), credit_iva=Value(0, output_field=IntegerField()), commission_calculate=Value(0, output_field=IntegerField()), commission_percentage=Value(0, output_field=IntegerField()), to_receive=Value(0, output_field=IntegerField()), utility=Value(0, output_field=IntegerField()), rentability=Value(0, output_field=IntegerField()), total_shipping=Value(0, output_field=IntegerField()), prorated_shipping=Value(0, output_field=IntegerField()), declared_total=Value(0, output_field=IntegerField()),debit_iva=Value(0, output_field=IntegerField()), total_sale2=Cast('total', IntegerField()), total_shipping2=Value(0, output_field=IntegerField()), prorated_shipping2=Value(0, output_field=IntegerField()), month_format=Value("", output_field=CharField()), date_payment=Value("", output_field=CharField()), product_code__code_seven_digits=Cast('product_code__code_seven_digits', IntegerField()), movement_type=Value("Venta",output_field=CharField()), total_shipping_prorrated=Value(0, output_field=IntegerField()), retail_liquidatio_id=Value(0, output_field=IntegerField()), total_sale_retail=Value(0, output_field=IntegerField()), commission_percentage_retail=Value(0, output_field=IntegerField()), contribution_retail=Value(0, output_field=IntegerField()), total_shipping_discount_ml=Value(0, output_field=IntegerField()), pay_by_retail=Value(0, output_field=IntegerField()), prorrated_operation_spending=Value(0, output_field=IntegerField()), credit_iva_retail=Value(0, output_field=IntegerField()), status_ryt=Value("", output_field=CharField()), payment_ryt=Value(0, output_field=IntegerField()), contribution_ryt=Value(0, output_field=IntegerField()), contribution_with_iva=Value(0, output_field=IntegerField()), status_load_liquidation=Value("", output_field=CharField()), status_payment_liquidation=Value("", output_field=CharField()), status_sale_return=Case(When(sale__status='pendiente', then=Value('Pendiente')), When(sale__status='cancelado', then=Value('Cancelada')), default=Value("Normal"), output_field=CharField()), iva_sii=Value(0, output_field=IntegerField()), final_contribution=Value(0, output_field=IntegerField()), year_format=Value("", output_field=CharField()), payment_bank_report_date=Value("", output_field=CharField()), bill_register_date=Value("", output_field=CharField()), optimal_utility=Value(0, output_field=IntegerField()), commission_credit_iva=Value(0, output_field=IntegerField()), commission_ryt=Value(0, output_field=IntegerField()), shipping_credit_iva=Value(0, output_field=IntegerField()), contribution_ryt2=Value(0, output_field=IntegerField()), final_contribution_percentage=Value(0, output_field=IntegerField()), final_contribution_percentage_with_ivas=Value(0, output_field=IntegerField()), result_final_contribution_with_iva=Value("", output_field=CharField()), result_final_contribution=Value("", output_field=CharField())).values(
					*initial_args,
					*values_args,
				).order_by('-sale__id')
		cell_format_fields_90 = wb.add_format({'bg_color': 'yellow', 'border': 1, 'font_size': 8})
		cell_format_data901 = wb.add_format({'bg_color': "#0070c0", 'color': "white", 'border': 1, 'font_size': 8})
		cell_format_data902 = wb.add_format({'bg_color': "#ffc000", 'color': "white", 'border': 1, 'font_size': 8})
		cell_format_data903 = wb.add_format({'bg_color': "#92d050", 'color': "white", 'border': 1, 'font_size': 8})
		cell_format_data904 = wb.add_format({'bg_color': "#632523", 'color': "white", 'border': 1, 'font_size': 8})
		cell_format_data905 = wb.add_format({'bg_color': "#60497a", 'color': "white", 'border': 1, 'font_size': 8})
		cell_format_fields_90.set_center_across()
		cell_format_fields_90.set_text_wrap()
		cell_format_fields_90.set_align('vcenter')
		cell_format_data901.set_center_across()
		cell_format_data901.set_text_wrap()
		cell_format_data901.set_align('vcenter')
		cell_format_data902.set_center_across()
		cell_format_data902.set_text_wrap()
		cell_format_data902.set_align('vcenter')
		cell_format_data903.set_center_across()
		cell_format_data903.set_text_wrap()
		cell_format_data903.set_align('vcenter')
		cell_format_data904.set_center_across()
		cell_format_data904.set_text_wrap()
		cell_format_data904.set_align('vcenter')
		cell_format_data905.set_center_across()
		cell_format_data905.set_text_wrap()
		cell_format_data905.set_align('vcenter')
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		# cell_format_data1 = wb.add_format({'bold': True, 'bg_color': '"#0070c0"', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		# cell_format_data1.set_center_across()
		cell_format_data = wb.add_format({'font_size': 8, 'num_format': '#,###'})
		cell_format_data_return = wb.add_format({'font_size': 8, 'color': "red", 'num_format': '#,###'})
		cell_format_data_return.set_text_wrap()
		cell_format_data2 = wb.add_format({'font_size': 8})
		cell_format_data3 = wb.add_format({'font_size': 8, 'num_format': '0%'})
		cell_format_data.set_text_wrap()
		cell_format_data2.set_text_wrap()
		new_politics_date = datetime.date(2019,4,29)
		total_sales_by_month = Sale.objects.filter(status='entregado').annotate(truncated_month_year=TruncMonth('date_sale')).values('truncated_month_year').annotate(total_sales_month=Sum(Case(When(sale_of_product__product_id=549, then=0), When(sale_of_product__sale_of_product_return__reintegrate=True, then=0), default=F('sale_of_product__total'), output_field=IntegerField()))).values('truncated_month_year', 'total_sales_month')

		total_sales_by_month_and_channel = Channel.objects.annotate(truncated_month_year=TruncMonth('channel_sale__date_sale')).values('truncated_month_year').annotate(total_sales_month=Sum(Case(When(channel_sale__sale_of_product__product_id=549, then=0), When(channel_sale__sale_of_product__sale_of_product_return__reintegrate=True, then=0),default=F('channel_sale__sale_of_product__total'), output_field=IntegerField()))).values('truncated_month_year', 'total_sales_month').annotate(channel_name=F('name')).values('truncated_month_year', 'total_sales_month', 'channel_name')
		with open('result.json') as f:
			data_json = json.load(f)
		# total_sales_by_month_and_channel = Sale.objects.exclude(sale_of_product__product_id=549).annotate(truncated_month_year=TruncMonth('date_sale')).values('truncated_month_year').annotate(total_sales_month=Sum('sale_of_product__total')).values('truncated_month_year', 'total_sales_month')
		# print(total_sales_by_month)

		columns = [
			{"name": 'Fecha venta', "width":12, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Pedido', "width": 9, "rotate": False, "format_data":cell_format_fields_90},
			{"name": 'Sku', "width": 9, "rotate": False, "format_data":cell_format_fields_90},
			{"name": 'Descripción', "width": 30, "rotate": False, "format_data":cell_format_fields_90},
			{"name": "Categoría", "width":15, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Cantidad', "width": 6, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Precio unit', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Total', "width": 15, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Usuario creador', "width": 9, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Código producto', "width": 8, "rotate": True, "format_data":cell_format_fields_90},
			{"name": "Variante", "width":15,"rotate":True, "format_data":cell_format_fields_90},
			{"name": 'Id venta', "width": 10, "rotate": False, "format_data":cell_format_fields_90},
			{"name": 'Canal', "width": 15, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Tipo doc', "width": 7, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'N° documento', "width": 10, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Forma pago', "width": 16, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Comentarios', "width": 25, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Responsable envío', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Estado', "width": 9, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Estado producto', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Fecha creación', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Hora venta', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Ultima modificación', "width": 13, "rotate": True, "format_data":cell_format_fields_90} ,
			{"name": 'Último usuario modificó', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Tipo de Envío', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Boleta electrónica', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Estado Canal (API)', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Estado de envío', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name": 'Sub estado de Envío', "width": 13, "rotate": True, "format_data":cell_format_fields_90},
			{"name":"Corte de inventario", "width":5, "rotate":True, "format_data":cell_format_fields_90},
			{"name": "Fecha de envio", "width":10, "rotate":True, "format_data":cell_format_fields_90},
			{"name": "Fecha emision boleta", "width":10, "rotate":True, "format_data":cell_format_fields_90},
			{"name": "Manifiesto", "width":10, "rotate":True, "format_data":cell_format_fields_90},
			{"name": "Nota de crédito", "width":10, "rotate":True, "format_data":cell_format_fields_90},
			{"name": "Mes", "width":10, "rotate":True, "format_data":cell_format_fields_90},
			{"name": "Año", "width":10, "rotate":True, "format_data":cell_format_fields_90},

		]
		number_columns = {
			"sale__date_sale": 0,
			"sale__id": 1,
			"product__sku": 2,
			"product__description": 3,
			"product__category_product_sale__name": 4,
			"quantity": 5,
			"unit_price_int": 6,
			"total_sale": 7,
			"sale__user__first_name": 8,
			"product_code__code_seven_digits": 9,
			"product_code__color__description":10,
			"sale__order_channel": 11,
			"sale__channel__name": 12,
			"sale__document_type": 13,
			"sale__number_document": 14,
			"sale__payment_type": 15,
			"sale__comments": 16,
			"sale__sender_responsable": 17,
			"sale__status": 18,
			"sale__status_product": 19,
			"sale__date_created": 20,
			"sale__hour_sale_channel":21,
			"sale__date_update": 22,
			'sale__last_user_update__first_name': 23,
			'sale__shipping_type': 24,
			'sale__bill':25,
			'sale__channel_status':26,
			'sale__shipping_status':27,
			'sale__shipping_type_sub_status':28,
			'sale__cut_inventory':29,
			'sale__shipping_date':30,
			'sale__date_document_emision':31,
			'sale__sales_report__id':32,
			'sale__credit_note_number':33,
			'month_format':34,
			'year_format':35,

		}

		if all_columns is not None:
			new_columns = [
				"(A)\nTotal venta producto ", 
				"(B) \nGastos envío 1", 
				"(C)\nGasto envío prorrateado 1", 
				"(D)\nGastos adicional", 
				"(E)\nGasto adicional prorrateado 2", 
				"(F)\nValor declarado", 
				"(G)\nIva débito (7) \n (F - F / 1.19)", 
				"(H)\nCosto unitario", 
				"(I)\nCosto total (9) \n (H * Cantidad)", 
				"(J)\nIva crédito (10)",
				"(K)\nComisión pactada ($) \n ((L * A))", 
				"(L)\nComisión pactada (%)", 
				"(M)\nTotal descuentos prorrateados \n(K + E)",
				"(N)\nTotal envíos descontados",
				"(O)\nA recibir \n(A - M)", 
				"(P)\nContribución", 
				"(Q)\nRentabilidad (%)", 
				"(R)\nFecha de pago estimada", 
				'(S)\nTipo de movimiento',

				"(a)\nRegistro facturación", 
				"(b)\nFecha de liquidación",
				"(c)\nMonto vendido Retail",
				"(d)\nMonto facturado comisión ($)", 
				"(e)\nMonto facturado comisión (%)" ,
				"(f)\nMonto facturado envío", 
				"(g)\nMonto facturado producto", 
				"(h)\nPagado por Retail",
				"(i)\nContribución Retail \n(h - H)", 
				"(j)\nEstado comisión", 
				"(k)\nEstado envío", 
				"(l)\nEstado producto", 
				"(m)\nN° factura envío", 
				"(n)\nN° factura comisión", 
				"(o)\nRegistro bancario", 
				"(p)\nFecha de pago", 
				"(q)\nId Liquidación Retail", 

				"(AA)\nEstado Venta", 
				"(AB)\nEstado carga liquidación", 
				"(AC)\nEstado pago liquidación", 
				"(AD)\nEstado (RyT)", 
				"(AE)\nPagado (RyT)", 
				"(AF)\nComisión (RyT)", 
				"(AG)\nContribución (RyT)", 
				"(AH)\nGasto Operacional prorrateado MENSUAL", 
				"(AI)\nIva SII prorrateado MENSUAL",
				"(AJ)\nIva Crédito Retail prorrateado MENSUAL",
				"(AK)\nIva Crédito Comision Retail",
				"(AL)\nIva Crédito envío Retail",
				"(AM)\nContribución Real (Sin ivas)",
				"(AN)\nContribución (Con todos los ivas)" ,
				"(AO)\n% Contribución final sobre costo y (AN)" ,
				"(AP)\nResultado sobre (AN)" ,
				"(AQ)\nContribución (Considera iva y G. Operacional)",
				"(AR)\n% Contribución final sobre costo y (AQ)",
				"(AS)\nResultado sobre (AQ)" ,
				# "(AQ)\nUtilidad Optima \n"
			]
			new_columns2 = [
				'total_sale2', 
				'total_shipping', 
				'prorated_shipping',
				'total_shipping2', 
				'prorated_shipping2', 
				'declared_total', 
				'debit_iva', 
				'product_cost', 
				'total_cost_sale', 
				'credit_iva', 
				'commission_calculate', 
				'commission_percentage', 
				'total_shipping_prorrated',
				'total_shipping_discount_ml',
				'to_receive', 
				'utility', 
				'rentability', 
				'date_payment', 
				'movement_type', 
				"bill_register",
				"bill_register_date" ,
				'total_sale_retail',
				"discount_commission",
				"commission_percentage_retail", 
				"discount_shipping",
				"discount_product",
				'pay_by_retail',
				"contribution_retail",
				"status_commission", 
				"status_shipping", 
				"status_product_bill",
				"shipping_bill_number", 
				"commission_bill_number", 
				"bank_register",
				"payment_bank_report_date", 
				"retail_liquidatio_id", 

				'status_sale_return', 
				'status_load_liquidation', 
				'status_payment_liquidation', 
				'status_ryt', 
				'payment_ryt',
				'commission_ryt',
				'contribution_ryt', 
				'prorrated_operation_spending', 
				'iva_sii', 
				'credit_iva_retail',
				'commission_credit_iva', 
				'shipping_credit_iva', 
				'contribution_ryt2',
				'contribution_with_iva', 
				'final_contribution_percentage_with_ivas',
				'result_final_contribution_with_iva',
				'final_contribution', 
				'final_contribution_percentage', 
				'result_final_contribution',
				# 'optimal_utility'
			]
			t = 36
			orange_bg = ['(O)\nA recibir \n(A - M)', '(P)\nContribución', '(Q)\nRentabilidad (%)']
			green_bg = [
				"(a)\nRegistro facturación", 
				"(b)\nFecha de liquidación",
				"(c)\nMonto vendido Retail",
				"(d)\nMonto facturado comisión ($)", 
				"(e)\nMonto facturado comisión (%)" ,
				"(f)\nMonto facturado envío", 
				"(g)\nMonto facturado producto", 
				"(h)\nPagado por Retail",
				"(i)\nContribución Retail \n(h - H)", 
				"(j)\nEstado comisión", 
				"(k)\nEstado envío", 
				"(l)\nEstado producto", 
				"(m)\nN° factura envío", 
				"(n)\nN° factura comisión", 
				"(o)\nRegistro bancario",
				"(p)\nFecha de pago", 
				"(q)\nId Liquidación Retail",
			]
			brown_bg = [
				"(AA)\nEstado Venta", 
				"(AB)\nEstado carga liquidación", 
				"(AC)\nEstado pago liquidación", 
				"(AD)\nEstado (RyT)", 
				"(AE)\nPagado (RyT)", 
				"(AF)\nComisión (RyT)", 
				"(AG)\nContribución (RyT)", 
				"(AH)\nGasto Operacional prorrateado MENSUAL", 
				"(AI)\nIva SII prorrateado MENSUAL",
				"(AJ)\nIva Crédito Retail prorrateado MENSUAL",
				"(AK)\nIva Crédito Comision Retail",
				"(AL)\nIva Crédito envío Retail",
			]
			purple_bg = [
				"(AM)\nContribución Real (Sin ivas)",
				"(AN)\nContribución (Con todos los ivas)" ,
				"(AO)\n% Contribución final sobre costo y (AN)" ,
				"(AP)\nResultado sobre (AN)" ,
				"(AQ)\nContribución (Considera iva y G. Operacional)",
				"(AR)\n% Contribución final sobre costo y (AQ)",
				"(AS)\nResultado sobre (AQ)" ,
			]
			
			for i in new_columns:
				if i in orange_bg:
					columns.append({"name": i, "width":10, "format_data":cell_format_data902})
				elif i in green_bg:
					columns.append({"name": i, "width":10, "format_data":cell_format_data903})
				elif i in brown_bg:
					columns.append({"name": i, "width":10, "format_data":cell_format_data904})
				elif i in purple_bg:
					columns.append({"name": i, "width":10, "format_data":cell_format_data905})
				else:
					columns.append({"name": i, "width":10, "format_data":cell_format_data901})
			for j in new_columns2:
				number_columns[j] = t
				t += 1
		else:
			pass
		row_num = 0

		for col_num in range(len(columns)):
			ws.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
			ws.set_column(col_num, col_num, columns[col_num]['width'])

		row_num = 1
		first_sale_id = 0
		first_sale_id2 = rows[0]['sale__id']
		data_re_write = []

		for row in rows:
			# row["sale__hour"] = ('%s:%s' % (row['sale__date_created'].hour if row['sale__date_created'].hour >= 10 else ('0' + str(row['sale__date_created'].hour)), row['sale__date_created'].minute if row['sale__date_created'].minute >= 10 else ('0' + str(row['sale__date_created'].minute))))
			row["sale__document_type"] = row['sale__document_type'].capitalize()
			row["sale__payment_type"] = row['sale__payment_type'].capitalize()
			row["sale__sender_responsable"] = row['sale__sender_responsable'].capitalize()
			row["sale__status"] = row['sale__status'].capitalize()
			row["sale__bill"] = "Si" if row['sale__bill'] else "No"
			row["sale__status_product"] = row['sale__status_product'].capitalize()
			row["sale__status_product"] = row['sale__status_product'].capitalize()
			row["sale__shipping_type"] = row['sale__shipping_type'].capitalize()
			row["sale__date_update"] = row['sale__date_update'].strftime("%Y-%m-%d %H:%M")
			row["sale__date_created"] = row["sale__date_created"].strftime("%Y-%m-%d %H:%M")
			row['month_format'] = int(row['sale__date_sale'].strftime("%Y%m"))
			row['year_format'] = int(row['sale__date_sale'].strftime("%Y"))
			if row["sale__shipping_date"] is not None:
				row["sale__shipping_date"] = row["sale__shipping_date"].strftime("%Y-%m-%d")

			if all_columns is not None:

				total_cost_product = Product_Sale_Historic_Cost.objects.filter(product_sale__sku=row['product__sku']).order_by('-date_cost')
				if total_cost_product:
					row['total_cost_sale'] = (total_cost_product[0].unit_cost * row['quantity'])
					row['product_cost'] = (total_cost_product[0].unit_cost)
					row['credit_iva'] = (total_cost_product[0].credit_iva * row['quantity'])
				else:
					row['total_cost_sale'] = 0
					row['credit_iva'] = 0
				commission_json = data_json.get(str(row['sale__id']), None)
				if commission_json is not None:
					calculate_commission = (commission_json / 100) * row['total_sale']
					percentage_comission = commission_json
				else:
					commission_new_format = CategoryTree.objects.filter(child_category__retail_category_product_sale__product_sale__sku=row['product__sku'], child_category__channel_id=row['sale__channel__id'])
					if commission_new_format:
						calculate_commission = (commission_new_format[0].commission / 100) * row['total_sale']
						percentage_comission = commission_new_format[0].commission
					else:
						comission = SKUCategoryChannel.objects.filter(product_sale__sku=row['product__sku'], sub_category_channel__channel_id=row['sale__channel__id'])
						if comission:
							calculate_commission = (comission[0].sub_category_channel.commission / 100) * row['total_sale']
							percentage_comission = comission[0].sub_category_channel.commission
						elif row['product__sku'] != 1:
							percentage_comission = self.return_commission(row['sale__channel__id'])
							calculate_commission = (percentage_comission / 100) * row['total_sale']
						else:
							calculate_commission = 0
							percentage_comission = 0

				row["commission_calculate"] = calculate_commission
				row["commission_percentage"] = percentage_comission / 100

				if row['sale__date_document_emision'] is not None:
					row['sale__date_document_emision'] = row['sale__date_document_emision'].strftime("%Y-%m-%d")

				if row['product__sku'] == 1:
					continue
				else:
					if row['sale__channel__id'] == 8:
						row['total_shipping'] = 2380
						row['prorated_shipping'] = (2380 * float(row['total_sale'])) / (float(row['sale__total_sale']))
					else:
						total_shipping = Sale_Product.objects.filter(sale_id=row['sale__id'], product__sku=1).aggregate(suma_envios=Sum('total'))
						if total_shipping['suma_envios'] is not None:
							row['total_shipping'] = total_shipping['suma_envios']
							row['prorated_shipping'] = (float(total_shipping['suma_envios']) * float(row['total_sale'])) / (float(row['sale__total_sale']) - float(total_shipping['suma_envios']))
							
							if row['sale__channel__id'] == 1:
								row['total_shipping_discount_ml'] = row['prorated_shipping']

								if row['sale__product_quantity_api'] != 0 and row['sale__date_sale'] > new_politics_date and ((float(row['sale__total_sale']) - float(row['total_shipping'])) / (row['sale__product_quantity_api'])) < 19990:
									row['total_shipping2'] = 100 * row['sale__product_quantity_api']
								else:
									row['total_shipping2'] = 0
								row['prorated_shipping2'] = (row['total_shipping2'] * float(row['total_sale'])) / (float(row['sale__total_sale']) - float(row['total_shipping']))

							elif row['sale__channel__id'] == 3:
								row['total_shipping2'] = float(row['total_shipping']) * (row["commission_percentage"])
								row['prorated_shipping2'] = (row['total_shipping2'] * float((row['total_sale'] + row['prorated_shipping']))) / (float(row['sale__total_sale']))
								row['total_shipping_discount_ml'] = 0
							else:
								row['total_shipping_discount_ml'] = 0
						else:
							row['total_shipping'] = 0
							row['prorated_shipping'] = 0
							row['total_shipping_discount_ml'] = 0
				if row['sale__channel__id'] == 3:
					row['declared_total'] = float(row['total_sale']) + float(row['prorated_shipping'])
				else:
					row['declared_total'] = row['total_sale']
				row['debit_iva'] = row['declared_total'] - (row['declared_total']/ 1.19)
				if row['sale__channel__id'] in [1, 8]:
					row["to_receive"] = float(row['total_sale2']) - float(row['prorated_shipping']) - float(row['prorated_shipping2']) - float(row["commission_calculate"])

					row["utility"] = row["to_receive"] - float(row['total_cost_sale'])
				else:
					row["to_receive"] = float(row['total_sale2']) + float(row['prorated_shipping']) - float(row['prorated_shipping']) - float(row['prorated_shipping2']) - float(row["commission_calculate"])

					row["utility"] = row["to_receive"] - float(row['total_cost_sale'])

				if row['total_cost_sale'] != 0:
					row['rentability'] = row["utility"] / float(row['total_cost_sale'])
					row['rentability'] = float("{0:.2f}".format(row['rentability']))
				else:
					row['rentability'] = 0
				row['date_payment'] = self.payment_day_expected(row['sale__date_sale'], row['sale__channel__id'], row['sale__payment_type'])
				row['total_shipping_prorrated'] = row['prorated_shipping2'] + row['commission_calculate']
				
				retail_payment_data = RetailPaymentDetail.objects.filter(sale_id=row['sale__id'], movement_type='ingreso')
				if retail_payment_data:
					row['bill_register'] = retail_payment_data[0].retail_payment.bill_register
					if (row['sale__total_sale'] - row['total_shipping']) != 0:
						if row['sale__channel__id'] == 8:
							row['discount_commission'] = (row['total_sale2'] * retail_payment_data.aggregate(sum_discount=Sum('discount_commission'))['sum_discount']) / (row['sale__total_sale'])
							
						else:
							row['discount_commission'] = (row['total_sale2'] * retail_payment_data.aggregate(sum_discount=Sum('discount_commission'))['sum_discount']) / (row['sale__total_sale'] - row['total_shipping'])
							
					else:
						row['discount_commission'] = 0
					if row['sale__total_sale'] - row['total_shipping'] != 0:
						if row['sale__channel__id'] == 8:
							row['discount_shipping'] = (row['total_sale2'] * retail_payment_data.aggregate(sum_discount=Sum('discount_shipping'))['sum_discount']) / (row['sale__total_sale'])
						else:
							row['discount_shipping'] = (row['total_sale2'] * retail_payment_data.aggregate(sum_discount=Sum('discount_shipping'))['sum_discount']) / (row['sale__total_sale'] - row['total_shipping'])
					else:
						row['discount_shipping'] = 0
					row['discount_product'] = None
					sum_total_shipping = abs(int(row['total_shipping_prorrated']) - (int(row['discount_commission']) * (-1)))
					sum_total_commission_prorrated = abs(int(row['commission_calculate']) - (int(row['discount_commission']) * (-1)))
					sum_prorated_shipping = abs(int(row['prorated_shipping']) - (int(row['discount_shipping']) * (-1)))
					row['status_commission'] = "Iguales" if (sum_total_shipping < 4 or sum_total_commission_prorrated < 4) else "Distintos"
					if row['sale__channel__id'] == 8:
						row['status_shipping'] = "Iguales"
					else:
						row['status_shipping'] = "Iguales" if (sum_prorated_shipping < 4) else "Distintos"
					row['status_product_bill'] = None
					row['shipping_bill_number'] = retail_payment_data[0].retail_payment.shipping_bill_number
					row['commission_bill_number'] = retail_payment_data[0].retail_payment.commission_bill_number
					row['retail_liquidatio_id'] = retail_payment_data[0].retail_payment.retail_id_number
					if row['sale__total_sale'] != 0:
						if row['sale__channel__id'] == 8:
							row['total_sale_retail'] = (row['total_sale2'] * retail_payment_data.aggregate(sum_discount=Sum('total_sale'))['sum_discount']) / (row['sale__total_sale'])
						else:
							row['total_sale_retail'] = (row['total_sale2'] * retail_payment_data.aggregate(sum_discount=Sum('total_sale'))['sum_discount']) / (row['sale__total_sale'] - row['total_shipping'])
					else:
						row['total_sale_retail'] = 0

					if row['total_sale_retail'] != 0:
						row['commission_percentage_retail'] = int((row['discount_commission'] * -1)) / row['total_sale_retail']
					else:
						row['commission_percentage_retail'] = 0
					
					if retail_payment_data[0].retail_payment.bank_register is not None:
						row['bank_register'] = retail_payment_data[0].retail_payment.bank_register.bank_register
						row['payment_bank_report_date'] = str(retail_payment_data[0].retail_payment.bank_register.date_report)
					else:
						row['bank_register'] = None
						row['payment_bank_report_date'] = None

					if row['sale__channel__id'] == 1:
						row['pay_by_retail'] = row['total_sale_retail'] + row['discount_commission'] + row['discount_shipping']
						row['contribution_retail'] = float(row['pay_by_retail']) - float(row['total_cost_sale'])
					elif row['sale__channel__id'] == 6:
						row['pay_by_retail'] = (row['total_sale2'] * retail_payment_data[0].total_pay) / (row['sale__total_sale'])
						row['discount_commission'] = (row['total_sale2'] - row['pay_by_retail']) * (-1)
						row['contribution_retail'] = float(row['pay_by_retail']) - float(row['total_cost_sale'])
					else:
						row['pay_by_retail'] = row['total_sale_retail'] + row['discount_commission']
						row['contribution_retail'] = float(row['pay_by_retail']) - float(row['total_cost_sale'])
					row['status_ryt'] = "Real"
					row['payment_ryt'] = row['pay_by_retail']
					row['contribution_ryt'] = row['contribution_retail']
					row['status_load_liquidation'] = "Cargado"
					row['bill_register_date'] = str(retail_payment_data[0].retail_payment.date_payment)
					row['commission_ryt'] = float(row['discount_commission']) * (-1)
					if row['bank_register'] is not None:
						row['status_payment_liquidation'] = "Cruzado"
					else:
						row['status_payment_liquidation'] = "No cruzado"
				else:
					row['bill_register'] = None
					row['discount_commission'] = None
					row['discount_shipping'] = None
					row['discount_product'] = None
					row['status_commission'] = None
					row['status_shipping'] = None
					row['status_product_bill'] = None
					row['shipping_bill_number'] = None
					row['commission_bill_number'] = None
					row['bank_register'] = None
					row['retail_liquidatio_id'] = None
					row['commission_percentage_retail'] = None
					row['total_sale_retail'] = None
					row['contribution_retail'] = None
					row['pay_by_retail'] = None
					row['status_ryt'] = "Teórico"
					row['payment_ryt'] = row["to_receive"]
					row['contribution_ryt'] = row["utility"]
					row['status_load_liquidation'] = "No cargado"
					row['status_payment_liquidation'] = "No cruzado"
					row['bill_register_date'] = None
					row['payment_bank_report_date'] = None
					row['commission_ryt'] = row['commission_calculate']

				operation_spend_month = OperationSpending.objects.filter(spend_month__month=row['sale__date_sale'].month, spend_month__year=row['sale__date_sale'].year)
				if operation_spend_month and row['status_sale_return'] == 'Normal':
					sales_of_month = total_sales_by_month.filter(truncated_month_year__month=row['sale__date_sale'].month, truncated_month_year__year=row['sale__date_sale'].year)
					row['prorrated_operation_spending'] = round(operation_spend_month[0].spend_amount / sales_of_month[0]['total_sales_month'], 5) * (row['total_sale2'])
					prorrated_operation_spending = row['prorrated_operation_spending']
					row['iva_sii'] = round(operation_spend_month[0].iva_sii_amount / sales_of_month[0]['total_sales_month'], 5) * (row['total_sale2'])
					# row['final_contribution'] = row['contribution_ryt'] - row['iva_sii'] - row['prorrated_operation_spending']
				else:
					row['prorrated_operation_spending'] = None
					prorrated_operation_spending = 0
					row['iva_sii'] = None
					# row['final_contribution'] = None
				
				credit_iva_retail = RetailCreditIvaSpend.objects.filter(spend_month__month=row['sale__date_sale'].month, channel_id=row['sale__channel__id'], spend_month__year=row['sale__date_sale'].year)
				if credit_iva_retail:
					sales_of_month_channel = total_sales_by_month_and_channel.filter(truncated_month_year__month=row['sale__date_sale'].month, truncated_month_year__year=row['sale__date_sale'].year, channel_name=row['sale__channel__name'])
					row['credit_iva_retail'] = round(credit_iva_retail[0].total_amount / sales_of_month_channel[0]['total_sales_month'], 5) * (row['total_sale2'])
					credit_iva_retail = row['credit_iva_retail']
				else:
					row['credit_iva_retail'] = None
					credit_iva_retail = 0
				row['optimal_utility'] = row['contribution_with_iva'] + credit_iva_retail + prorrated_operation_spending
				row['commission_credit_iva'] = float(row['commission_ryt']) - (float(row['commission_ryt']) / 1.19)
				if row['sale__channel__id'] == 3:
					row['shipping_credit_iva'] = row['prorated_shipping'] - (row['prorated_shipping'] / 1.19)
				else:
					row['shipping_credit_iva'] = 0
				row['contribution_ryt2'] = row['contribution_ryt']
				# row['contribution_ryt2'] = row['contribution_ryt']
				row['contribution_with_iva'] = row['contribution_ryt'] - float(row['debit_iva']) + float(row['credit_iva']) + float(row['commission_credit_iva'])
				row['final_contribution'] = row['contribution_with_iva'] - (row['prorrated_operation_spending'] if row['prorrated_operation_spending'] is not None else 0)
				if row['total_cost_sale'] != 0:
					row['final_contribution_percentage'] = (row['final_contribution'] / float(row['total_cost_sale']))
					row['final_contribution_percentage_with_ivas'] = (row['contribution_with_iva'] / float(row['total_cost_sale']))
				else:
					row['final_contribution_percentage'] = 0
					row['final_contribution_percentage_with_ivas'] = 0

				row['result_final_contribution_with_iva'] = "Perdida" if row['contribution_with_iva'] < 0 else "Ganancia"
				row['result_final_contribution'] = "Perdida" if row['final_contribution'] < 0 else "Ganancia"

			columns_no_number = ['sale__order_channel', 'product__sku', 'sale__number_document', 'sale__sales_report__id', 'sale__credit_note_number', 'sale__id', 'product_code__code_seven_digits', 'month_format', 'year_format']

			if all_columns is not None:
				if row['sale__id'] == first_sale_id2:
					# if row['status_commission'] == "Distintos":
					# 	commision_acum_payment += row['discount_commission']
					# 	commission_acum_calculate += row['commission_calculate']
					# else:
					# 	commision_acum_payment = 0
					# 	commission_acum_calculate = 0
					data_re_write.append(row)
				else:
					# Revisar devolución
					# diference_commissions = abs(commision_acum_payment - commission_acum_calculate)
					# if diference_commissions < 4:
					# 	for t in range(len(data_re_write)):
						# Iterar filas anteriores y modificar los valores de 

					return_data = Return.objects.filter(sale_id=first_sale_id2, )
					retail_payment_return = RetailPaymentDetail.objects.filter(sale_id=first_sale_id2, movement_type='egreso')
					if return_data:
						row_pivote = row_num
						
						row_num = self.write_return_row(return_data, row_num, ws, cell_format_data_return, data_re_write, number_columns, columns_no_number, wb, retail_payment_return)
						if row_pivote != row_num:
							for row_before in data_re_write:
								row_pivote -= 1
								ws.write(row_pivote, 72, "Con observación (Devolución)", cell_format_data)
								ws.write(row_pivote, 79, "-", cell_format_data)
								ws.write(row_pivote, 80, "-", cell_format_data)
								ws.write(row_pivote, 81, "-", cell_format_data)
						# print(row_num)



					data_re_write = []
					data_re_write.append(row)
					first_sale_id2 = row['sale__id']
			for col_num in number_columns:
				if row[col_num] is not None:
					if isinstance(row[col_num], int) or isinstance(row[col_num], float) or isinstance(row[col_num], decimal.Decimal):
						if (row[col_num] == 0 and col_num != 'rentability' and col_num != 'commission_percentage') or (col_num in columns_no_number):
							ws.write_number(row_num, number_columns[col_num], row[col_num], cell_format_data2)
						elif col_num == 'rentability' or col_num == 'commission_percentage' or col_num == 'commission_percentage_retail' or col_num == 'final_contribution_percentage' or col_num == 'final_contribution_percentage_with_ivas':
							ws.write_number(row_num, number_columns[col_num], row[col_num], cell_format_data3)
						else:
							ws.write_number(row_num, number_columns[col_num], row[col_num], cell_format_data)
					else:
						ws.write(row_num, number_columns[col_num], str(row[col_num]), cell_format_data)
				else:
					ws.write(row_num, number_columns[col_num], "-", cell_format_data)
			row_num += 1

		ws.autofilter(0, 0, 0, len(columns) - 1)
		ws.freeze_panes(1, 0)

		# Hoja 2 con base de costos
		if all_columns is not None:
			ws2 = wb.add_worksheet("Base de Costos")
			rows2 = Product_Sale_Historic_Cost.objects.all().values('purchase_code', 'date_cost', 'product_sale__description', 'product_sale__sku', 'unit_cost', 'credit_iva', 'minimun_price_bill', 'purchase_type')
			columns = [
				{"name": 'Código', "width":12, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Fecha', "width": 9, "rotate": False, "format_data":cell_format_fields_90},
				{"name": 'Descripción', "width": 30, "rotate": False, "format_data":cell_format_fields_90},
				{"name": 'Sku', "width": 12, "rotate": False, "format_data":cell_format_fields_90},
				{"name": "Costo unitario", "width":10, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Iva crédito', "width": 10, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Pmvd', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Tipo de compra', "width": 11, "rotate": True, "format_data":cell_format_fields_90},

			]

			row_num = 0

			for col_num in range(len(columns)):
				ws2.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
				ws2.set_column(col_num, col_num, columns[col_num]['width'])
			row_num = 1

			for row in rows2:
				ws2.write(row_num, 0, row['purchase_code'], cell_format_data)
				ws2.write(row_num, 1, str(row['date_cost']), cell_format_data)
				ws2.write(row_num, 2, row['product_sale__description'], cell_format_data)
				ws2.write_number(row_num, 3, row['product_sale__sku'], cell_format_data2)
				ws2.write_number(row_num, 4, row['unit_cost'], cell_format_data)
				ws2.write_number(row_num, 5, row['credit_iva'], cell_format_data)
				if row['minimun_price_bill'] == 0:
					ws2.write_number(row_num, 6, 0, cell_format_data2)
				else:
					ws2.write_number(row_num, 6, row['minimun_price_bill'], cell_format_data)
				ws2.write(row_num, 7, row['purchase_type'], cell_format_data)
				row_num += 1
			ws3= wb.add_worksheet("Base de Sku")
			rows3 = Product_Sale.objects.all()
			columns = [
				{"name": 'Sku', "width":12, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Descripción', "width": 30, "rotate": False, "format_data":cell_format_fields_90},
				{"name": 'Categoría', "width": 12, "rotate": False, "format_data":cell_format_fields_90},
				{"name": "Alto", "width":10, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Largo', "width": 10, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Ancho', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Peso', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Variante', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Código 7', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Fecha de creación', "width": 11, "rotate": True, "format_data":cell_format_fields_90},

			]

			row_num = 0

			for col_num in range(len(columns)):
				ws3.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
				ws3.set_column(col_num, col_num, columns[col_num]['width'])
			row_num = 1

			for row in rows3:
				codes_seven = row.product_code.all()
				if codes_seven:
					for e in codes_seven:
						ws3.write(row_num, 0, str(e.product.sku), cell_format_data)
						ws3.write(row_num, 1, e.product.description, cell_format_data)
						if e.product.category_product_sale is not None:
							ws3.write(row_num, 2, e.product.category_product_sale.name, cell_format_data)
						else:
							ws3.write(row_num, 2, "-", cell_format_data)
						ws3.write(row_num, 3, e.product.height, cell_format_data2)
						ws3.write(row_num, 4, e.product.length, cell_format_data)
						ws3.write(row_num, 5, e.product.width, cell_format_data)
						ws3.write(row_num, 6, e.product.weight, cell_format_data)
						ws3.write(row_num, 7, e.color.description, cell_format_data)
						ws3.write(row_num, 8, e.code_seven_digits, cell_format_data)
						ws3.write(row_num, 9, str(e.product.date_created.date()), cell_format_data)
						row_num += 1

				else:
					ws3.write(row_num, 0, str(row.sku), cell_format_data)
					ws3.write(row_num, 1, row.description, cell_format_data)
					if row.category_product_sale is not None:
						ws3.write(row_num, 2, row.category_product_sale.name, cell_format_data)
					else:
						ws3.write(row_num, 2, "-", cell_format_data)
					ws3.write(row_num, 3, row.height, cell_format_data2)
					ws3.write(row_num, 4, row.length, cell_format_data)
					ws3.write(row_num, 5, row.width, cell_format_data)
					ws3.write(row_num, 6, row.weight, cell_format_data)
					ws3.write(row_num, 7, "-", cell_format_data)
					ws3.write(row_num, 8, "-", cell_format_data)
					ws3.write(row_num, 9, str(row.date_created.date()), cell_format_data)
					row_num += 1
			ws4= wb.add_worksheet("Base de comisiones")
			rows4 = SKUCategoryChannel.objects.filter(product_sale__isnull=False).values('product_sale__sku', 'product_sale__description', 'sub_category_channel__category_asiamerica__name', 'sub_category_channel__channel__name', 'sub_category_channel__name', 'sub_category_channel__commission', 'date_start').order_by('product_sale_id')
			columns = [
				{"name": 'Sku', "width":12, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Descripción', "width": 30, "rotate": False, "format_data":cell_format_fields_90},
				{"name": 'Categoría Asiamerica', "width": 12, "rotate": False, "format_data":cell_format_fields_90},
				{"name": "Canal", "width":10, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Categoría canal', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Comisión (%)', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Día de inicio', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Día de fin', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
			]

			row_num = 0


			for col_num in range(len(columns)):
				ws4.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
				ws4.set_column(col_num, col_num, columns[col_num]['width'])
			row_num = 1
			for row in rows4:
				commission = row['sub_category_channel__commission'] / 100
				ws4.write(row_num, 0, row['product_sale__sku'], cell_format_data2)
				ws4.write(row_num, 1, row['product_sale__description'], cell_format_data)
				ws4.write(row_num, 2, row['sub_category_channel__category_asiamerica__name'], cell_format_data)
				ws4.write(row_num, 3, row['sub_category_channel__channel__name'], cell_format_data)
				ws4.write(row_num, 4, row['sub_category_channel__name'], cell_format_data)
				ws4.write(row_num, 5, commission, cell_format_data3)
				ws4.write(row_num, 6, str(row['date_start']), cell_format_data)
				ws4.write(row_num, 7, str(today), cell_format_data)
				row_num += 1

			ws4.autofilter(0, 0, 0, len(columns) - 1)
			ws4.freeze_panes(1, 0)

			ws5= wb.add_worksheet("Base de comisiones Web")
			rows5 = Product_Sale_Category_Retail.objects.all().values('product_sale__sku', 'product_sale__description', 'retail_category__channel__name', 'retail_category__category_name', 'retail_category__code_category', 'retail_category__child_category_retail__commission', 'retail_category__date_start').order_by('product_sale_id')
			columns = [
				{"name": 'Sku', "width":12, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Descripción', "width": 30, "rotate": False, "format_data":cell_format_fields_90},
				{"name": "Canal", "width":10, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Categoría canal', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Código Categoría Canal', "width": 12, "rotate": False, "format_data":cell_format_fields_90},
				{"name": 'Comisión (%)', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Día de inicio', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
				{"name": 'Día de fin', "width": 11, "rotate": True, "format_data":cell_format_fields_90},
			]

			row_num = 0


			for col_num in range(len(columns)):
				ws5.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
				ws5.set_column(col_num, col_num, columns[col_num]['width'])
			row_num = 1
			for row in rows5:
				commission = row['retail_category__child_category_retail__commission'] / 100
				ws5.write(row_num, 0, row['product_sale__sku'], cell_format_data2)
				ws5.write(row_num, 1, row['product_sale__description'], cell_format_data)
				ws5.write(row_num, 2, row['retail_category__channel__name'], cell_format_data)
				ws5.write(row_num, 3, row['retail_category__category_name'], cell_format_data)
				ws5.write(row_num, 4, row['retail_category__code_category'], cell_format_data)
				ws5.write(row_num, 5, commission, cell_format_data3)
				ws5.write(row_num, 6, str(row['retail_category__date_start']), cell_format_data)
				ws5.write(row_num, 7, str(today), cell_format_data)
				row_num += 1

			ws5.autofilter(0, 0, 0, len(columns) - 1)
			ws5.freeze_panes(1, 0)

		wb.close()
		output.seek(0)

		if all_columns is not None:
			file_name = "Reporte de Ventas " + datetime.datetime.now().strftime("%Y%m%d%H%M") + ".xlsx"
			path_folder = os.path.expanduser('~/Documents/Reports/')
			
			file_generate = open(path_folder + file_name, 'wb')
			file_generate.write(output.read())
			file_generate.close()

			response = HttpResponse(open(path_folder + file_name, 'rb').read())
			response['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			response['Content-Disposition'] = 'attachment; filename=' + file_name
			print("--- %s seconds ---" % (time.time() - start_time))
			
			return response

		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % 'Registro de ventas Asiamerica.xlsx'
		# print("--- %s seconds ---" % (time.time() - start_time))
		return response


class AutomaticRetailSalesXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def f(self, x):
		return {
			'paid': "Pagado",
			'cancelled': "Cancelado",
			'confirmed': "Confirmado",
			'payment_in_process': "Pago en proceso",
			'payment_required': "Pago requerido",
			'invalid': "Invalido",
		}[x]

	def write_file(self, ws, row, data, cell_format_data):
		ws.write(row, 0, str(data['date_created']), cell_format_data)
		ws.write(row, 1, data['sku'], cell_format_data)
		ws.write(row, 2, data['quantity'], cell_format_data)
		ws.write(row, 3, data['unit_price'], cell_format_data)
		ws.write(row, 4, data['total_amount'], cell_format_data)
		ws.write(row, 5, int(data['id']), cell_format_data)
		ws.write(row, 6, "Mercadolibre", cell_format_data)
		ws.write(row, 7, data['comments'], cell_format_data)
		ws.write(row, 8, self.f(data['status']), cell_format_data)
		ws.write(row, 9, data['url'], cell_format_data)
		ws.write(row, 10, data['label'], cell_format_data)

	def get(self, request):
		date_from = self.request.query_params.get('date_from', None)
		date_to = self.request.query_params.get('date_to', None)
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Informe de registro de ventas retail Canal Mercadolibre ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		url_ml = "https://api.mercadolibre.com/orders/"
		token = Token.objects.get(id=1).token_ml
		date_from = datetime.datetime.strptime(date_from, '%d-%m-%Y').strftime("%Y-%m-%dT00:%M:%S.000%z-00:00")
		date_to = datetime.datetime.strptime(date_to, '%d-%m-%Y').strftime("%Y-%m-%dT23:%M:%S.000%z-00:00")
		# print(date_from)
		# print(date_to)
		url_request = url_ml + "search?seller=216244489"+ "&access_token="+ token +"&order.date_created.from="+ date_from +"&order.date_created.to="+ date_to

		columns = [{"name": 'Fecha Registro', "width":12, "rotate": True},{"name": 'SKU', "width": 9, "rotate": False}, {"name": 'Cantidad', "width": 6, "rotate": True}, {"name": 'Precio Unit', "width": 11, "rotate": True}, {"name": 'Total', "width": 15, "rotate": True},{"name": 'Id venta', "width": 10, "rotate": False}, {"name": 'Canal', "width": 15, "rotate": True},  {"name": 'Comentarios', "width": 25, "rotate": True}, {"name": 'Estado', "width": 13, "rotate": True}, {"name": 'Url', "width": 50, "rotate": True}, {"name": '¿Con Etiqueta?', "width": 10, "rotate": True}]
		row_num = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()

		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
		data = requests.get(url_request).json()
		# print(json.dumps(data))
		if data['paging']['total'] <= 50:
			rows = data['results']
			row_num += 1
			for row in rows:
				format_date = datetime.datetime.strptime(nth_repl(row['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z")
				date_created=format_date.strftime('%d-%m-%Y')
				if row['shipping']['id'] is not None:
					shipping_cost = requests.get("https://api.mercadolibre.com/shipments/" + str(row['shipping']['id']) + "?access_token=" +token).json()['shipping_option']
					shipping_cost = ((shipping_cost['cost'] - shipping_cost['list_cost']) * (-1))
				else:
					shipping_cost = 0

				if row['order_items'][0]['item']['seller_custom_field'] is not None:
					url_permalink = ""
				else:
					url_permalink = requests.get("https://api.mercadolibre.com/items/" + row['order_items'][0]['item']['id']).json().get('permalink', "")
				if row['shipping']['status'] == 'to_be_agreed':
					label = 'NO'
				else:
					label = 'SI'


				if row['order_items'][0]['item']['seller_custom_field'] is not None:
					pack = Pack.objects.filter(sku_pack=int(row['order_items'][0]['item']['seller_custom_field']))
				else:
					pack = []
				if pack:
					products_count = Pack_Products.objects.filter(pack__sku_pack=int(row['order_items'][0]['item']['seller_custom_field']))
					if products_count.count() > 1:
						for product in products_count:
							quantity = row['order_items'][0]['quantity'] * product.quantity
							unit_price = row['order_items'][0]['unit_price'] * (product.percentage_price/100)
							data_write = {
								"date_created": date_created,
								"sku":product.product_pack.sku,
								"quantity":quantity,
								"unit_price":unit_price,
								"total_amount":unit_price * quantity,
								"id":row['id'],
								"status":row['status'],
								"url":url_permalink,
								"comments":row['order_items'][0]['item']['title'],
								"label":label
							}
							self.write_file(ws, row_num, data_write, cell_format_data)
							row_num += 1
						data_write = {
							"date_created": date_created,
							"sku":1,
							"quantity":1,
							"unit_price":shipping_cost,
							"total_amount":shipping_cost,
							"id":row['id'],
							"status":row['status'],
							"url":url_permalink,
							"comments":row['order_items'][0]['item']['title'],
							"label":label

						}
						self.write_file(ws, row_num, data_write, cell_format_data)
						row_num += 1
						# print(row_num)
						continue
						# For para añadir cada producto con distinto sku en el pack
					else:
						data_write = {
								"date_created": date_created,
								"sku":products_count[0].product_pack.sku,
								"quantity":row['order_items'][0]['quantity'] * products_count[0].quantity,
								"unit_price":(row['order_items'][0]['unit_price'] * row['order_items'][0]['quantity'])/(row['order_items'][0]['quantity'] * products_count[0].quantity),
								"total_amount":row['total_amount'],
								"id":row['id'],
								"status":row['status'],
								"url":url_permalink,
								"comments":row['order_items'][0]['item']['title'],
								"label":label
							}
						self.write_file(ws, row_num, data_write, cell_format_data)
						row_num += 1
						data_write = {
								"date_created": date_created,
								"sku":1,
								"quantity":1,
								"unit_price":shipping_cost,
								"total_amount":shipping_cost,
								"id":row['id'],
								"status":row['status'],
								"url":url_permalink,
								"comments":row['order_items'][0]['item']['title'],
								"label":label
							}
						self.write_file(ws, row_num, data_write, cell_format_data)
						row_num += 1
						# print(row_num)

						continue
						# Añadir un producto únicamente
					# se añaden filas con packs
				data_write = {
					"date_created": date_created,
					"sku":row['order_items'][0]['item']['seller_custom_field'],
					"quantity":row['order_items'][0]['quantity'],
					"unit_price":row['order_items'][0]['unit_price'],
					"total_amount":row['total_amount'],
					"id":row['id'],
					"status":row['status'],
					"url":url_permalink,
					"comments":row['order_items'][0]['item']['title'],
					"label":label
				}
				self.write_file(ws, row_num, data_write, cell_format_data)
				row_num += 1
				data_write = {
					"date_created": date_created,
					"sku":1,
					"quantity":1,
					"unit_price":shipping_cost,
					"total_amount":shipping_cost,
					"id":row['id'],
					"status":row['status'],
					"url":url_permalink,
					"comments":row['order_items'][0]['item']['title'],
					"label":label
				}
				self.write_file(ws, row_num, data_write, cell_format_data)
				row_num += 1
				# print(row_num)


		else:
			row_num += 1
			count = 0
			id_sales = []
			iteration_end = int(data['paging']['total']/50)
			module = int(data['paging']['total']%50)
			for i in range(iteration_end + 1):
				rows = requests.get(url_request + "&offset=" + str(i*50)).json()['results']
				for row in rows:
					# row_num += 1
					count += 1
					id_sales.append(row['id'])

					format_date = datetime.datetime.strptime(nth_repl(row['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z")
					date_created=format_date.strftime('%d-%m-%Y')

					if row['shipping']['id'] is not None:
						shipping_cost = requests.get("https://api.mercadolibre.com/shipments/" + str(row['shipping']['id']) + "?access_token=" +token).json()['shipping_option']
						shipping_cost = ((shipping_cost['cost'] - shipping_cost['list_cost']) * (-1))
					else:
						shipping_cost = 0
					if row['order_items'][0]['item']['seller_custom_field'] is not None:
						url_permalink = ""
					else:
						url_permalink = requests.get("https://api.mercadolibre.com/items/" + row['order_items'][0]['item']['id']).json().get('permalink', "")

					if row['shipping']['status'] == 'to_be_agreed':
						label = 'NO'
					else:
						label = 'SI'


					if row['order_items'][0]['item']['seller_custom_field'] is not None:
						pack = Pack.objects.filter(sku_pack=int(row['order_items'][0]['item']['seller_custom_field']))
					else:
						pack = []
					if pack:
						products_count = Pack_Products.objects.filter(pack__sku_pack=int(row['order_items'][0]['item']['seller_custom_field']))
						if products_count.count() > 1:
							for product in products_count:
								unit_price = row['order_items'][0]['unit_price'] * (product.percentage_price/100)
								quantity = row['order_items'][0]['quantity'] * product.quantity

								data_write = {
									"date_created": date_created,
									"sku":product.product_pack.sku,
									"quantity":quantity,
									"unit_price":unit_price,
									"total_amount":unit_price * quantity,
									"id":row['id'],
									"status":row['status'],
									"url":url_permalink,
									"comments":row['order_items'][0]['item']['title'],
									"label":label
								}
								self.write_file(ws, row_num, data_write, cell_format_data)
								row_num += 1
							data_write = {
								"date_created": date_created,
								"sku":1,
								"quantity":1,
								"unit_price":shipping_cost,
								"total_amount":shipping_cost,
								"id":row['id'],
								"status":row['status'],
								"url":url_permalink,
								"comments":row['order_items'][0]['item']['title'],
								"label":label
							}
							self.write_file(ws, row_num, data_write, cell_format_data)
							row_num += 1
							# print(row_num)


							continue
							# For para añadir cada producto con distinto sku en el pack
						else:
							data_write = {
								"date_created": date_created,
								"sku":products_count[0].product_pack.sku,
								"quantity":row['order_items'][0]['quantity'] * products_count[0].quantity,
								"unit_price":(row['order_items'][0]['unit_price'] * row['order_items'][0]['quantity'])/(row['order_items'][0]['quantity'] * products_count[0].quantity),
								"total_amount":row['total_amount'],
								"id":row['id'],
								"status":row['status'],
								"url":url_permalink,
								"comments":row['order_items'][0]['item']['title'],
								"label":label
							}
							self.write_file(ws, row_num, data_write, cell_format_data)
							row_num += 1
							data_write = {
								"date_created": date_created,
								"sku":1,
								"quantity":1,
								"unit_price":shipping_cost,
								"total_amount":shipping_cost,
								"id":row['id'],
								"status":row['status'],
								"url":url_permalink,
								"comments":row['order_items'][0]['item']['title'],
								"label":label
							}
							self.write_file(ws, row_num, data_write, cell_format_data)
							row_num += 1
							# print(row_num)

							continue
					data_write = {
						"date_created": date_created,
						"sku":row['order_items'][0]['item']['seller_custom_field'],
						"quantity":row['order_items'][0]['quantity'],
						"unit_price":row['order_items'][0]['unit_price'],
						"total_amount":row['total_amount'],
						"id":row['id'],
						"status":row['status'],
						"url":url_permalink,
						"comments":row['order_items'][0]['item']['title'],
						"label":label
					}
					self.write_file(ws, row_num, data_write, cell_format_data)
					row_num += 1
					data_write = {
						"date_created": date_created,
						"sku":1,
						"quantity":1,
						"unit_price":shipping_cost,
						"total_amount":shipping_cost,
						"id":row['id'],
						"status":row['status'],
						"url":url_permalink,
						"comments":row['order_items'][0]['item']['title'],
						"label":label
					}
					self.write_file(ws, row_num, data_write, cell_format_data)
					row_num += 1
					# print(row_num)

			rows = requests.get(url_request + "&offset="+str(((iteration_end*50) + module))).json()['results']
			for row in rows:
				# row_num += 1
				count += 1
				id_sales.append(row['id'])
				format_date = datetime.datetime.strptime(nth_repl(row['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z")
				date_created=format_date.strftime('%d-%m-%Y')
				if row['shipping']['id'] is not None:
					shipping_cost = requests.get("https://api.mercadolibre.com/shipments/" + str(row['shipping']['id']) + "?access_token=" +token).json()['shipping_option']
					shipping_cost = ((shipping_cost['cost'] - shipping_cost['list_cost']) * (-1))
				else:
					shipping_cost = 0

				if row['order_items'][0]['item']['seller_custom_field'] is not None:
					url_permalink = ""
				else:
					url_permalink = requests.get("https://api.mercadolibre.com/items/" + row['order_items'][0]['item']['id']).json().get('permalink', "")
				if row['shipping']['status'] == 'to_be_agreed':
					label = 'NO'
				else:
					label = 'SI'

				if row['order_items'][0]['item']['seller_custom_field'] is not None:
					pack = Pack.objects.filter(sku_pack=int(row['order_items'][0]['item']['seller_custom_field']))
				else:
					pack = []

				if pack:
					products_count = Pack_Products.objects.filter(pack__sku_pack=int(row['order_items'][0]['item']['seller_custom_field']))
					if products_count.count() > 1:
						for product in products_count:
							unit_price = row['order_items'][0]['unit_price'] * (product.percentage_price/100)
							quantity = row['order_items'][0]['quantity'] * product.quantity
							data_write = {
								"date_created": date_created,
								"sku":product.product_pack.sku,
								"quantity":quantity,
								"unit_price":unit_price,
								"total_amount":unit_price * quantity,
								"id":row['id'],
								"status":row['status'],
								"url":url_permalink,
								"comments":row['order_items'][0]['item']['title'],
								"label":label
							}
							self.write_file(ws, row_num, data_write, cell_format_data)
							row_num += 1
							# print(row_num)

						data_write = {
							"date_created": date_created,
							"sku":1,
							"quantity":1,
							"unit_price":shipping_cost,
							"total_amount":shipping_cost,
							"id":row['id'],
							"status":row['status'],
							"url":url_permalink,
							"comments":row['order_items'][0]['item']['title'],
							"label":label
						}
						self.write_file(ws, row_num, data_write, cell_format_data)
						row_num += 1
						# print(row_num)


						continue
						# For para añadir cada producto con distinto sku en el pack
					else:
						data_write = {
							"date_created": date_created,
							"sku":products_count[0].product_pack.sku,
							"quantity":row['order_items'][0]['quantity'] * products_count[0].quantity,
							"unit_price":(row['order_items'][0]['unit_price'])/(row['order_items'][0]['quantity'] * products_count[0].quantity),
							"total_amount":row['total_amount'],
							"id":row['id'],
							"status":row['status'],
							"url":url_permalink,
							"comments":row['order_items'][0]['item']['title'],
							"label":label
						}
						self.write_file(ws, row_num, data_write, cell_format_data)
						row_num += 1
						data_write = {
							"date_created": date_created,
							"sku":1,
							"quantity":1,
							"unit_price":shipping_cost,
							"total_amount":shipping_cost,
							"id":row['id'],
							"status":row['status'],
							"url":url_permalink,
							"comments":row['order_items'][0]['item']['title'],
							"label":label
						}
						self.write_file(ws, row_num, data_write, cell_format_data)
						row_num += 1
						# print(row_num)

						continue
				data_write = {
					"date_created": date_created,
					"sku":row['order_items'][0]['item']['seller_custom_field'],
					"quantity":row['order_items'][0]['quantity'],
					"unit_price":row['order_items'][0]['unit_price'],
					"total_amount":row['total_amount'],
					"id":row['id'],
					"status":row['status'],
					"url":url_permalink,
					"comments":row['order_items'][0]['item']['title'],
					"label":label
				}
				self.write_file(ws, row_num, data_write, cell_format_data)
				row_num += 1
				data_write = {
					"date_created": date_created,
					"sku":1,
					"quantity":1,
					"unit_price":shipping_cost,
					"total_amount":shipping_cost,
					"id":row['id'],
					"status":row['status'],
					"url":url_permalink,
					"comments":row['order_items'][0]['item']['title'],
					"label":label
				}
				self.write_file(ws, row_num, data_write, cell_format_data)
				row_num += 1
				# print(row_num)


		# print(id_sales)
		wb.close()
		output.seek(0)
		filename = 'Informe de registro de ventas retail Canal Mercadolibre.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		print("--- %s seconds ---" % (time.time() - start_time))
		return response

	def status(self, x):
		return {
			'paid': "Pagado",
			'cancelled': "Cancelado",
			'confirmed': "Confirmado",
			'payment_in_process': "Pago en proceso",
			'payment_required': "Pago requerido",
			'invalid': "Invalido",
		}[x]

	def status_shipping(self, x):
		return {
			'delivered': "Entregado",
			'shipped': "Enviado",
			'pending': "Pendiente",
			'ready_to_ship': "Listo para enviar",
			'handling': "En Manejo",
			'not_delivered': "No entregado",
			'not_verified': "No verificado",
			'cancelled': "Cancelado",
			'closed': "Cerrado",
			'to_be_agreed':None,
			'error': "Error",
			'active': "Activo",
			'not_specified': "No especificado",
			'stale_ready_to_ship': "Estancado en listo para enviar",
			'stale_shipped': "Estancado en enviado",
			'to_be_agreed': "Por acordar",
		#'cost_exceeded': "Costo Excesivo",
		#'printed': "Impresa"
		}[x]

	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl","abel.mejias@asiamerica.cl", "mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code_data  = Product_Code.objects.filter(code_seven_digits=each)[0]
				message_text = product_code_data.product.description + " Variante: " + product_code_data.color.description + "(" + str(each) + ")"
			else:
				product_sale_data = Product_Sale.objects.filter(sku=each)[0]
				message_text = product_sale_data.description + "(" + str(each) + ")"
			message_append.append(message_text)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def post(self, request):
		date_from = request.data.get('date_from', None)
		date_to = request.data.get('date_to', None)
		url_ml = "https://api.mercadolibre.com/orders/"
		token = Token.objects.get(id=1).token_ml
		date_from = datetime.datetime.strptime(date_from, '%d-%m-%Y').strftime("%Y-%m-%dT00:%M:%S.000%z-00:00")
		date_to = datetime.datetime.strptime(date_to, '%d-%m-%Y').strftime("%Y-%m-%dT23:%M:%S.000%z-00:00")
		url_request = url_ml + "search?seller=216244489"+ "&access_token="+ token +"&order.date_created.from="+ date_from +"&order.date_created.to="+ date_to
		data = requests.get(url_request).json()
		if data['paging']['total'] <= 50:
			rows = data["results"]
		else:
			rows = []
			count = 0
			iteration_end = int(data['paging']['total']/50)
			module = int(data['paging']['total']%50)
			for i in range(iteration_end + 1):
				new_rows = requests.get(url_request + "&offset=" + str(i*50)).json()['results']
				rows = rows + new_rows
			rows = rows + requests.get(url_request + "&offset="+str(((iteration_end*50) + module))).json()['results']
		products_stock = []
		for row in rows:
			sale_data = Sale.objects.filter(order_channel=row['id'])
			if sale_data.count() > 0:
				continue
			else:
				order_data = requests.get(url_ml + str(row['id']) + "?access_token=" + token, timeout=20).json()
				if order_data['order_items'][0]['item']['seller_custom_field'] == None and order_data['order_items'][0]['item']['seller_sku'] == None:
					if order_data['shipping']['status'] != 'to_be_agreed':
						shipping_tracking = requests.get("https://api.mercadolibre.com/shipments/" + str(order_data['shipping']['id']) + "?access_token=" +token).json()['tracking_number']
						if shipping_tracking is not None:
							shipping_tracking = int(shipping_tracking)
						shipping_type="con etiqueta"
						shipping_status=self.status_shipping(order_data['shipping']['status'])
						shipping_type_sub_status=''
					else:
						shipping_type="por acordar"
						shipping_type_sub_status='pendiente'
						shipping_tracking=None
						shipping_status=None
					date_sale = datetime.datetime.strptime(nth_repl(order_data['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.%f%z") + timedelta(hours=1)
					sale = Sale.objects.create(
						order_channel=order_data['id'],
						total_sale=0,
						user=User.objects.get(username='api'),
						channel=Channel.objects.get(name='Mercadolibre'),
						document_type='sdt',
						payment_type='transferencia retail',
						sender_responsable="comprador" if order_data['shipping']['status'] == "to_be_agreed" else "vendedor",
						status='pendiente',
						channel_status=self.status(order_data['status']),
						comments=order_data['order_items'][0]['item']['title'] +" (Venta total de "+ str(order_data['total_amount_with_shipping']) + ")",
						date_sale=date_sale,
						tracking_number=shipping_tracking,
						shipping_type=shipping_type,
						shipping_status=shipping_status,
						shipping_type_sub_status=shipping_type_sub_status,
						mlc=order_data['order_items'][0]['item']['id'],
						user_id_ml=order_data['buyer']['id'],
						last_user_update=User.objects.get(username='api'),
						hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute)))),
					)
					if shipping_type == 'con etiqueta':
						unit_price = (order_data['shipping']['shipping_option']['cost'] - order_data['shipping']['shipping_option']['list_cost']) * (-1)
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=1),
							sale=sale,
							quantity=1,
							unit_price=unit_price,
							total=(unit_price*1),
						)
						sale.total_sale = sale_product.total
						sale.save()
				else:
					if order_data['order_items'][0]['item']['seller_sku'] is not None:
						sku_mercadolibre = order_data['order_items'][0]['item']['seller_sku']
					else:
						sku_mercadolibre = order_data['order_items'][0]['item']['seller_custom_field']
					is_pack = Pack.objects.filter(sku_pack=sku_mercadolibre[:5])
					if is_pack:
						pack = is_pack[0]
						products = Pack_Products.objects.filter(pack=is_pack[0]).annotate(
							sku_product=F('product_pack__sku'),
							quantity_product=F('quantity') * Value(order_data['order_items'][0]['quantity'], output_field=IntegerField()),
							unit_price_product=(Value(order_data['order_items'][0]['unit_price'], output_field=IntegerField()) * F('percentage_price') / Value(100, output_field=IntegerField())) / (F('quantity'))).values('sku_product', 'quantity_product', 'unit_price_product')
						if products.count() == 1 and len(order_data['order_items'][0]['item']['variation_attributes']) > 0:
							sku_7_pack = str(products[0]['sku_product']) + order_data['order_items'][0]['item']['variation_attributes'][0]['value_name'][:2]
						else:
							sku_7_pack = None
					else:
						pack = None
						products = Product_Sale.objects.filter(sku=sku_mercadolibre[:5]).annotate(sku_product=F('sku'), quantity_product=Value(order_data['order_items'][0]['quantity'], output_field=IntegerField()), unit_price_product=Value(order_data['order_items'][0]['unit_price'], output_field=IntegerField())).values('sku_product', 'quantity_product', 'unit_price_product')
					# print(products)
					date_sale = datetime.datetime.strptime(nth_repl(order_data['date_created'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.%f%z") + timedelta(hours=1)
					if order_data['shipping']['status'] != 'to_be_agreed':
						shipping_tracking = requests.get("https://api.mercadolibre.com/shipments/" + str(order_data['shipping']['id']) + "?access_token=" +token).json()['tracking_number']
						shipping_type="con etiqueta"
						shipping_status=self.status_shipping(order_data['shipping']['status'])
						shipping_type_sub_status=''
						if shipping_tracking is not None:
							shipping_tracking = int(shipping_tracking)
					else:
						shipping_type="por acordar"
						shipping_tracking=None
						shipping_status=None
						shipping_type_sub_status='pendiente'
					comment = order_data['order_items'][0]['item']['title']
					if sku_mercadolibre.endswith("99"):
						comment += " REACONDICIONADA"
					if order_data['order_items'][0]['item']['variation_attributes']:
						comment += (" " + order_data['order_items'][0]['item']['variation_attributes'][0]['value_name'])
					sale = Sale.objects.create(
						order_channel=order_data['id'],
						total_sale=0,
						user=User.objects.get(username='api'),
						channel=Channel.objects.get(name='Mercadolibre'),
						document_type='sdt',
						payment_type='transferencia retail',
						sender_responsable="comprador" if order_data['shipping']['status'] == "to_be_agreed" else "vendedor",
						status='pendiente',
						channel_status=self.status(order_data['status']),
						comments=comment,
						date_sale=date_sale,
						tracking_number=shipping_tracking,
						shipping_type=shipping_type,
						shipping_status=shipping_status,
						mlc=order_data['order_items'][0]['item']['id'],
						user_id_ml=order_data['buyer']['id'],
						last_user_update=User.objects.get(username='api'),
						shipping_type_sub_status=shipping_type_sub_status,
						hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute)))),
					)
					total = 0
					total_sale_no_shipping = order_data['total_amount']
					total_products = 0
					for product in products:
						if (len(sku_mercadolibre[:7]) == 7) and (not sku_mercadolibre.endswith("99")):
							product_code = Product_Code.objects.filter(code_seven_digits=sku_mercadolibre[:7])
							if product_code:
								product_code = product_code[0]
							else:
								product_code = None
						else:
							codes = Product_Code.objects.filter(product__sku=product['sku_product']).values('code_seven_digits')
							if codes.count() == 1:
								product_code = Product_Code.objects.get(code_seven_digits=codes[0]['code_seven_digits'])
							else:
								codes_quantity = []
								for code in codes:
									if int(code['code_seven_digits'][5]) > 4:
										codes_quantity.append(code['code_seven_digits'])
								if len(codes_quantity) == 1:
									product_code = Product_Code.objects.get(code_seven_digits=codes_quantity[0])
								else:
									if is_pack and (sku_7_pack is not None):
										product_code = Product_Code.objects.filter(code_seven_digits=sku_7_pack)
										if product_code:
											product_code = product_code[0]
										else:
											product_code = None
									else:
										# tratamiento para packs con mas de 1 producto y mas de 1 variante en ambos productos o en al menos 1 de ellos
										product_code = None

						if (product['unit_price_product'] * product['quantity_product']) > total_sale_no_shipping:
							unit_price = total_sale_no_shipping/(product['quantity_product'])
						else:
							if not (product['unit_price_product']/1.0).is_integer():
								unit_price = self.roundup(product['unit_price_product'])
								total_sale_no_shipping -= (unit_price * product['quantity_product'])
							else:
								unit_price = product['unit_price_product']
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=product['sku_product']),
							product_code=product_code,
							sale=sale,
							quantity=product['quantity_product'],
							unit_price=unit_price,
							total=product['quantity_product'] * unit_price,
							comment_unit=order_data['order_items'][0]['item']['title'],
						)
						quantity_stock = Product_Sale.objects.get(sku=product['sku_product']).stock - (product['quantity_product'])
						Product_Sale.objects.filter(sku=product['sku_product']).update(stock=quantity_stock)
						if product_code is not None:
							quantity_stock_color = product_code.quantity - (product['quantity_product'])
							Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
							if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not (product_code.code_seven_digits in products_stock)):
								products_stock.append(product_code.code_seven_digits)
						else:
							if quantity_stock <= 5 and quantity_stock >= 0 and (not (product['sku_product'] in products_stock)):
								products_stock.append(product['sku_product'])
						total += product['quantity_product'] * unit_price
						total_products += product['quantity_product']
					if Sale_Product.objects.filter(sale=sale).exclude(product__sku=1):
						if total != order_data['total_amount']:
							product_to_split = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1)[0]
							product_split = Sale_Product.objects.get(id=product_to_split.id)
							diference_totals = order_data['total_amount'] - total
							if product_split.quantity == 1:
								product_split.unit_price = product_split.unit_price + diference_totals
								product_split.save()
								product_split.total = product_split.unit_price * product_split.quantity
								product_split.save()
							else:
								product_split.quantity = product_split.quantity - 1
								product_split.total = product_split.total - product_split.unit_price
								product_split.save()
								sale_product = Sale_Product.objects.create(
									product=Product_Sale.objects.get(sku=product_split.product.sku),
									product_code=product_split.product_code,
									sale=sale,
									quantity=1,
									unit_price=product_split.unit_price + diference_totals,
									total=(product_split.unit_price + diference_totals),
								)
							total = order_data['total_amount']

					if order_data['shipping']['status'] == 'to_be_agreed':
						shipping_price = 0
					else:
						shipping_price = (order_data['shipping']['shipping_option']['cost'] - order_data['shipping']['shipping_option']['list_cost']) * (-1)
					sale_product = Sale_Product.objects.create(
						product=Product_Sale.objects.get(sku=1),
						sale=sale,
						quantity=1,
						unit_price=shipping_price,
						total=shipping_price,
					)
					total += shipping_price
					sale.total_sale = total
					sale.total_number_of_product = total_products
					sale.save()
		if products_stock:
			self.alert_stock(products_stock)
		return Response(data={"sales": ["Ventas de mercadolibre cargadas exitosamente"]}, status=status.HTTP_200_OK)


class GenerateBillOpenFactura(APIView):
	permission_classes = (IsAuthenticated,)

	def roundup(self, x):
		return int(math.ceil(x / 100.0)) * 100

	def post(self, request):
		id_sale = request.data['id']
		try:
			sale = Sale.objects.get(id=id_sale)
		except Sale.DoesNotExists as e:
			return Response(data={"sale": ['La venta indicada no existe o fue eliminada']}, status=status.HTTP_404_NOT_FOUND)

		if sale.bill:
			return Response(data={"sale": ['La venta ya posee boleta electrónica generada.']}, status=status.HTTP_400_BAD_REQUEST)

		today = datetime.datetime.today().strftime("%Y-%m-%d")

		if (sale.channel.name == 'Mercadolibre' or sale.channel.name == 'Ripley') and sale.order_channel == None:
			return Response(data={"order_channel": ["La venta del canal debe tener Id de Venta para poder generar boleta."]}, status=status.HTTP_400_BAD_REQUEST)
		if sale.channel.name == 'Falabella' or sale.channel.name == 'Paris':
			return Response(data={"sale": ['Disculpe no se puede generar boleta para Paris o Falabella']}, status=status.HTTP_400_BAD_REQUEST)

		if sale.channel.name == 'Ripley':
			products_data = Sale_Product.objects.filter(sale__id=sale.id).annotate(NmbItem=Case(When(Q(product_code=None) & ~Q(product__sku=1),then=Concat(Value('# ', output_field=CharField()), F('product__description'))),default=F('product__description'), output_field=CharField()), DscItem=Concat('product__sku', Value(' - ', output_field=CharField()), Value(' (', output_field=CharField()), Case(When(comment_unit=None, then=F('sale__comments')), default=F('comment_unit'), output_field=CharField()), Value(')', output_field=CharField()), output_field=CharField()),QtyItem=F('quantity'),PrcItem=Cast(F('unit_price'), FloatField()), MontoItem=Cast(F('total'), FloatField())).values(
				"NmbItem",
				"DscItem",
				"QtyItem",
				"PrcItem",
				"MontoItem",
				"id",
			)
			total_data = Sale_Product.objects.filter(sale=sale).aggregate(MntTotal=Cast(Sum('total'), IntegerField()), TotalPeriodo=Cast(Sum('total'), IntegerField()), VlrPagar=Cast(Sum('total'), IntegerField()))
		else:
			products_data = Sale_Product.objects.filter(sale__id=sale.id).exclude(product__sku=1).annotate(NmbItem=Case(When(product_code=None,then=Concat(Value('# ', output_field=CharField()), F('product__description'))),default=F('product__description'), output_field=CharField()), DscItem=Concat('product__sku', Value('  - ', output_field=CharField()), Value(' (', output_field=CharField()), 'sale__comments',Value(')', output_field=CharField()), output_field=CharField()),QtyItem=F('quantity'),PrcItem=Cast(F('unit_price'), FloatField()), MontoItem=Cast(F('total'), FloatField())).values(
				"NmbItem",
				"DscItem",
				"QtyItem",
				"PrcItem",
				"MontoItem",
				"id",
			)
			total_data = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1).aggregate(MntTotal=Cast(Sum('total'), IntegerField()), TotalPeriodo=Cast(Sum('total'), IntegerField()), VlrPagar=Cast(Sum('total'), IntegerField()))
		index = 0
		total_products_no_shipping = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1).aggregate(MntTotal=Cast(Sum('total'), IntegerField()))['MntTotal']
		if total_products_no_shipping == None:
			return Response(data={"sale": ['Hubo un error al generar la boleta']}, status=status.HTTP_400_BAD_REQUEST)
		id_sale_association = {}
		for i in products_data:
			index += 1
			i['NroLinDet'] = index
			id_sale_association[str(index)] = i['id']
			if i['MontoItem'] > total_products_no_shipping and not "Envío" in i['NmbItem']:
				i['PrcItem'] = total_products_no_shipping/i['QtyItem']
				i['MontoItem'] = total_products_no_shipping
			else:
				if not i['MontoItem'].is_integer() and not "Envío" in i['NmbItem']:
					i['MontoItem'] = self.roundup(i['MontoItem'])
					i['PrcItem'] = i['MontoItem']/i['QtyItem']
					total_products_no_shipping -= i['MontoItem']
					continue
				else:
					if not "Envío" in i['NmbItem']:
						total_products_no_shipping -= i['MontoItem']
			if "Envío" in i['NmbItem']:
				i.pop('DscItem')
		for i in products_data:
			i.pop('id')
		products_data = [x for x in products_data if x['PrcItem'] != 0]

		order_channel = sale.order_channel
		if order_channel == None:
			order_channel = "No Aplica"
			social_reason = sale.channel.name + order_channel
		else:
			if sale.tracking_number is not None:
				order_channel = "-OC " +str(order_channel) + " -T " + str(sale.tracking_number)
				social_reason = sale.channel.name + order_channel
			else:
				order_channel = "-OC " +str(order_channel)
				social_reason = sale.channel.name + order_channel
		data_bill = {
		   "response":[
			  "JSON",
			  "PDF"
		   ],
		   "dte":{
			  "Encabezado":{
				 "IdDoc":{
					"TipoDTE":39,
					"Folio":0,
					"FchEmis":today,
					"IndServicio":3
				 },
				 "Emisor":DATA_EMITTER,
				 "Receptor":{
					"RUTRecep":"66666666-6",
					"RznSocRecep":social_reason,
				 },
				 "Totales":total_data
			  },
			  "Detalle":list(products_data)
		   }
		}

		url_of = OPEN_FACTURA_URL + "/v2/dte/document"
		headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
		# print(json.dumps(data_bill))
		response = requests.post(url_of, data=json.dumps(data_bill), headers=headers)
		# print(response.json())
		if response.status_code == 200:
			products_bill_round = Sale_Product.objects.filter(sale=sale).exclude(product__sku=1)
			for key, value in id_sale_association.items():
				for i in products_data:
					if int(key) == i['NroLinDet']:
						product_data_bill = Sale_Product.objects.get(id=value)
						product_data_bill.quantity = i['QtyItem']
						product_data_bill.unit_price = i['PrcItem']
						product_data_bill.total = i['MontoItem']
						product_data_bill.save()
			token = response.json()['TOKEN']
			base_64_pdf = response.json()['PDF']
			response = requests.get(OPEN_FACTURA_URL + "/v2/dte/document/" + response.json()['TOKEN'] + "/json", headers=headers).json()
			sale.number_document = response['json']['Encabezado']['IdDoc']['Folio']
			sale.document_type = "boleta electrónica"
			sale.bill = True
			if sale.bill and sale.number_document is not None and Sale_Product.objects.filter(sale=sale, product_code__isnull=True).exclude(product__sku=1).count() == 0:
				sale.status = "entregado"
			sale.token_bill = token
			sale.save()
			if request.data['print_bill']:
				return Response(data={"sale": ['Documento generado exitosamente'], "pdf": base_64_pdf, "folio":response['json']['Encabezado']['IdDoc']['Folio']}, status=status.HTTP_200_OK)

			return Response(data={"sale": ['Documento generado exitosamente']}, status=status.HTTP_200_OK)
		else:
			return Response(data={"sale": ['Hubo un error al generar la boleta eletrónica']}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data={"sale": ['Hubo un error al generar la boleta eletrónica']}, status=status.HTTP_400_BAD_REQUEST)

class AutomaticRetailSalesRipleyXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def f(self, x):
	   return {
			'RECEIVED': "Recibido",
			'CLOSED': "Cerrado",
			'SHIPPED': "Enviado",
			'SHIPPING': "Enviando",
			'WAITING_DEBIT_PAYMENT': "Pago de débito en espera",
			'WAITING_ACCEPTANCE': "Esperando aceptación",
			'STAGING': "En Andamiaje",
			'CANCELED': "Cancelada",
		}[x]

	def write_file(self, ws, row, data, cell_format_data):
		ws.write(row, 0, str(data['date_created']), cell_format_data)
		ws.write(row, 1, str(data['hour_created']), cell_format_data)
		ws.write(row, 2, data['sku'], cell_format_data)
		ws.write(row, 3, data['quantity'], cell_format_data)
		ws.write(row, 4, data['unit_price'], cell_format_data)
		ws.write(row, 5, data['total_amount'], cell_format_data)
		ws.write(row, 6, data['revenue'], cell_format_data)
		ws.write(row, 7, int(data['id']), cell_format_data)
		ws.write(row, 8, "Ripley", cell_format_data)
		ws.write(row, 9, data['comments'], cell_format_data)
		ws.write(row, 10, self.f(data['status']), cell_format_data)
		ws.write(row, 11, data['label'], cell_format_data)

	def get(self, request):
		date_from = self.request.query_params.get('date_from', None)
		date_to = self.request.query_params.get('date_to', None)
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Informe de registro de ventas retail Canal Ripley ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		url_rp = "https://ripley-prod.mirakl.net"
		date_from = datetime.datetime.strptime(date_from, '%d-%m-%Y').strftime("%Y-%m-%dT00:%M:%S.000%z")
		date_to = datetime.datetime.strptime(date_to, '%d-%m-%Y').strftime("%Y-%m-%dT23:%M:%S.000%z")
		# print(date_from)
		# print(date_to)
		headers = {'Accept': 'application/json', 'Authorization': API_KEY_RP}
		url_request = url_rp + "/api/orders?start_date="+ date_from +"&end_date="+ date_to
		max_count = requests.get(url_request, headers=headers).json()['total_count']
		url_request = url_rp + "/api/orders?start_date="+ date_from +"&end_date="+ date_to + "&max=" + str(max_count)
		# print(url_request)

		columns = [{"name": 'Fecha Registro', "width":12, "rotate": True},{"name": 'Hora Registro', "width":12, "rotate": True}, {"name": 'SKU', "width": 9, "rotate": False}, {"name": 'Cantidad', "width": 6, "rotate": True}, {"name": 'Precio Unit', "width": 11, "rotate": True}, {"name": 'Total', "width": 15, "rotate": True}, {"name": 'Ganancia', "width": 10, "rotate": False}, {"name": 'Id venta', "width": 10, "rotate": False}, {"name": 'Canal', "width": 15, "rotate": True},  {"name": 'Comentarios', "width": 25, "rotate": True}, {"name": 'Estado', "width": 13, "rotate": True}, {"name": '¿Con Etiqueta?', "width": 10, "rotate": True}]
		row_num = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
		data = requests.get(url_request, headers=headers).json()
		rows = data['orders']
		row_num += 1
		for row in rows:
			format_date = datetime.datetime.strptime(row['created_date'], "%Y-%m-%dT%H:%M:%S%z")
			date_created=format_date.strftime('%d-%m-%Y')
			hour_created=format_date.strftime('%H:%M')
			products = Product_SKU_Ripley.objects.filter(sku_ripley=row['order_lines'][0]['offer_sku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price', 'color__description')
			for row2 in row['order_lines']:
				for product in products:
					unit_price = (row2['price_unit'] * (product['pack_ripley__pack_product__percentage_price']/100))/(row2['quantity'] * product['pack_ripley__pack_product__quantity'])
					if product['color__description'] is not None:
						comments = row2['product_title'] + " (Modelo: " +product['color__description']+ ")"
					else:
						comments = row2['product_title']
					data_write = {
							"date_created": date_created,
							"hour_created": hour_created,
							"sku":product['pack_ripley__pack_product__product_pack__sku'],
							"quantity":(row2['quantity'] * product['pack_ripley__pack_product__quantity']),
							"unit_price":unit_price,
							"total_amount": (row2['quantity'] * product['pack_ripley__pack_product__quantity'] * unit_price),
							"revenue": (row2['quantity'] * product['pack_ripley__pack_product__quantity'] * unit_price) - (row['total_commission'] * (product['pack_ripley__pack_product__percentage_price']/100))/len(row['order_lines']),
							"id":row['commercial_id'],
							"status":row['order_state'],
							"comments":comments,
							"label":"SI"
					}
					self.write_file(ws, row_num, data_write, cell_format_data)
					row_num += 1
				data_write = {
					"date_created": date_created,
					"hour_created": hour_created,
					"sku":1,
					"quantity":1,
					"unit_price":row['shipping_price'],
					"total_amount": row['shipping_price'],
					"revenue": 0,
					"id":row['commercial_id'],
					"status":row['order_state'],
					"comments":"",
					"label":"SI"
				}
				self.write_file(ws, row_num, data_write, cell_format_data)
				row_num += 1

		# print(data["total_count"])
		wb.close()
		output.seek(0)
		filename = 'Informe de registro de ventas retail Canal Ripley.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		print("--- %s seconds ---" % (time.time() - start_time))
		return response

	def send_email_notification(self, order_channel, total_products):
		if total_products is not None:
			subject = 'hello'
			from_email = 'info@asiamerica.cl'
			to = ["juan.ureta@asiamerica.cl",]
			msg_html = render_to_string('ripley-label-request.html',
				{
					"order_channel": str(order_channel),
					"total_products": str(total_products),
				}
			)
			send_mail(
				'Solicitud de etiqueta',
				subject,
				from_email,
				to,
				html_message=msg_html,
			)
		else:
			subject = 'hello'
			from_email = 'info@asiamerica.cl'
			to = ["info@asiamerica.cl", "juan.ureta@asiamerica.cl", "pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl"]
			msg_html = render_to_string('ripley-express-sale.html',
				{
					"order_channel": str(order_channel),
				}
			)
			send_mail(
				'Venta Express Ripley',
				subject,
				from_email,
				to,
				html_message=msg_html,
			)
	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl","abel.mejias@asiamerica.cl", "mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code = Product_Code.objects.filter(code_seven_digits=each)[0]
				message = product_code.product.description + " Variante: " + product_code.color.description + "(" +str(each) + ")"
			else:
				product_sale = Product_Sale.objects.filter(sku=each)[0]
				message = product_sale.description + "(" + str(each) + ")"
			message_append.append(message)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def post(self, request):
		url_rp = "https://ripley-prod.mirakl.net"
		date_from = datetime.datetime.strptime(request.data['date_from'], '%d-%m-%Y').strftime("%Y-%m-%dT00:%M:%SZ")
		date_to = datetime.datetime.strptime(request.data['date_to'], '%d-%m-%Y').strftime("%Y-%m-%dT23:%M:%SZ")
		headers = {'Accept': 'application/json', 'Authorization': API_KEY_RP}
		url_request = url_rp + "/api/orders?start_date="+ date_from +"&end_date="+ date_to + "&paginate=false"
		data = requests.get(url_request, headers=headers).json()
		rows = data['orders']
		for row in rows:
			if Sale.objects.filter(order_channel=row['commercial_id'].split("-")[0]):
				sale = Sale.objects.get(order_channel=row['commercial_id'].split("-")[0])
				status_sale = self.f(row['order_state'])
				if status_sale != sale.channel_status:
					sale.channel_status = status_sale
					sale.save()
					#if "Despacho Express" in sale.comments and sale.channel_status == 'Enviando':
						#self.send_email_notification(sale.order_channel, None)
					#if "Multibulto" in sale.comments and sale.channel_status == 'Enviando':
					#	self.send_email_notification(row['order_id'], sale.total_number_of_product)
				if (str(sale.tracking_number) != row['shipping_tracking']) and (not ("Despacho Express" in sale.comments)):
					#print(row['order_id'])
					sale.tracking_number = row['shipping_tracking']
					sale.save()
				continue
			else:
				date_sale = datetime.datetime.strptime(row['created_date'], "%Y-%m-%dT%H:%M:%SZ") + timedelta(hours=-3)
				hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
				if row['shipping_tracking'] is not None:
					row['shipping_tracking'] = None if not row['shipping_tracking'].isdigit() else row['shipping_tracking']
				sale = Sale.objects.create(
					order_channel=row['commercial_id'].split("-")[0],
					total_sale=0,
					declared_total=0,
					revenue=0,
					user=User.objects.get(username='api-ripley'),
					channel=Channel.objects.get(name="Ripley"),
					channel_status=self.f(row['order_state']),
					document_type="sdt",
					payment_type='transferencia retail',
					sender_responsable="vendedor",
					status="pendiente",
					tracking_number=row['shipping_tracking'],
					last_user_update=User.objects.get(username='api-ripley'),
					date_sale=date_sale,
					hour_sale_channel=hour_sale_channel,
				)
				total = 0
				revenue = 0
				total_products = 0
				comments = ''
				total_sale_no_shipping = row['price']
				for row2 in row['order_lines']:
					comments = ''
					products = Product_SKU_Ripley.objects.filter(sku_ripley=row2['offer_sku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price','color__description')
					for product in products:
						if len(products) == 1:
							unit_price = (row2['price'] * (product['pack_ripley__pack_product__percentage_price']/100))/(row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						else:
							unit_price = (row2['price_unit'] * (product['pack_ripley__pack_product__percentage_price']/100))/(row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						if (unit_price * row2['quantity'] * product['pack_ripley__pack_product__quantity']) > total_sale_no_shipping:
							unit_price = total_sale_no_shipping/(row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						else:
							if not unit_price.is_integer():
								unit_price = self.roundup(unit_price)
								total_sale_no_shipping -= (unit_price * row2['quantity'] * product['pack_ripley__pack_product__quantity'])

						if product['color__description'] is not None:
							comments += row2['product_title'] + " (Modelo: " +product['color__description']+ ") -"
						else:
							comments += row2['product_title'] + " -"
						product_code = Product_Code.objects.filter(product__sku=product['pack_ripley__pack_product__product_pack__sku'])
						if product_code.count() == 1:
							product_code = Product_Code.objects.get(id=product_code[0].id)
						else:
							#if row2['offer_sku'].isdigits() and len(row2['offer_sku']) == 7 and products.count() == 1):
							#	product_code = Product_Code.objects.filter(code_seven_digits=row2['offer_sku'])
							#	if product_code:
							#		product_code = product_code[0]
							#else:
							product_code_50 = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits',6,6)).filter(product__sku=product['pack_ripley__pack_product__product_pack__sku'], code_seven__gte=50, color__description=product['color__description'])
							if product_code_50:
								product_code = product_code_50[0]
							else:
								product_code = None
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=product['pack_ripley__pack_product__product_pack__sku']),
							product_code=product_code,
							sale=sale,
							quantity=(row2['quantity'] * product['pack_ripley__pack_product__quantity']),
							unit_price=unit_price,
							total=(row2['quantity'] * product['pack_ripley__pack_product__quantity'] * unit_price),
							# from_pack=Pack.objects.get(id=product['pack_ripley__id']),
							comment_unit=row2['product_title'],
						)
						quantity_stock = Product_Sale.objects.get(sku=product['pack_ripley__pack_product__quantity']).stock - (row2['quantity'] * product['pack_ripley__pack_product__quantity'])
						Product_Sale.objects.filter(sku=product['pack_ripley__pack_product__product_pack__sku']).update(stock=quantity_stock)
						if product_code is not None:
							quantity_stock_color = product_code.quantity - (row2['quantity'] * product['pack_ripley__pack_product__quantity'])
							Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
							if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not (product_code.code_seven_digits in products_stock)):
								products_stock.append(product_code.code_seven_digits)
						else:
							if quantity_stock <= 5 and quantity_stock >= 0 and (not (product['pack_ripley__pack_product__product_pack__sku'] in products_stock)):
								products_stock.append(product['pack_ripley__pack_product__product_pack__sku'])
						total += (row2['quantity'] * product['pack_ripley__pack_product__quantity'] * unit_price)
						revenue += (row2['quantity'] * product['pack_ripley__pack_product__quantity'] * unit_price) - (row['total_commission'] * (product['pack_ripley__pack_product__percentage_price']/100))/len(row['order_lines'])
						total_products += (row2['quantity'] * product['pack_ripley__pack_product__quantity'])
					if row2['shipping_price'] == 0:
						pass
					else:
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=1),
							sale=sale,
							quantity=1,
							unit_price=row2['shipping_price'],
							total=row2['shipping_price'],
						)
						total += row2['shipping_price']
				sale.total_sale = total
				sale.declared_total = total
				sale.total_number_of_product = total_products
				sale.revenue = revenue + row['shipping_price']
				if "Despacho 24 Horas Hábiles" in row['shipping_type_label'] or "Despacho Domicilio - Despacho 3 Hrs Express" in row['shipping_type_label']:
					comments += " Despacho Express "
					#client = Client(ACCOUNT_SID, AUTH_TOKEN)
					#body_message = "Te notificamos que se ha generado una venta express del canal Ripley, con número de ordén " + str(sale.order_channel)
					# receiver = ['whatsapp:+56976846957', 'whatsapp:+56956301999', 'whatsapp:+56995398995', 'whatsapp:+56944253964']
				if sale.total_number_of_product > 1 or len(row['order_lines']) > 1:
					comments += " Multibulto "
					#self.send_email_notification(row['order_id'], total_products)
				sale.comments = comments
				sale.save()
		if products_stock:
			self.alert_stock(products_stock)
		return Response(data={"sales": ['Ventas de Ripley actualizadas exitosamente']}, status=status.HTTP_200_OK)



class StockXLSXViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	def write_file(self, ws, row, data, cell_format_data):
		ws.write(row, 0, str(data['sku']), cell_format_data)
		ws.write(row, 1, str(data['description']), cell_format_data)
		ws.write(row, 2, data['sales_quantity'], cell_format_data)
		ws.write(row, 3, data['merma_quantity'], cell_format_data)
		ws.write(row, 4, data['purchases_quantity'], cell_format_data)
		ws.write(row, 5, data['left'], cell_format_data)
		ws.write(row, 6, data['week_sales'], cell_format_data)
		ws.write(row, 7, data['stock_weeks'], cell_format_data)
		ws.write(row, 8, data['last_day_stock'], cell_format_data)

	def get(self, request):
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Inventario Asiamerica ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))

		columns = [{"name": 'SKU 5', "width":12, "rotate": True},{"name": 'Descripción', "width":12, "rotate": True}, {"name": 'Vendido', "width": 9, "rotate": False}, {"name": 'Merma', "width": 6, "rotate": True}, {"name": 'Comprado', "width": 11, "rotate": True}, {"name": 'Quedan', "width": 15, "rotate": True}, {"name": 'Ventas semanales', "width": 10, "rotate": True}, {"name": 'Semanas con stock', "width": 10, "rotate": True}, {"name": 'Fecha sin stock', "width": 15, "rotate": True}]
		row_num = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
		rows = Product_Sale.objects.annotate(sales_quantity=Sum('product_sale__quantity'), purchases_quantity=Sum('product_of_purchase__quantity')).values('id','sku', 'description', 'sales_quantity', 'purchases_quantity').order_by('sku')
		row_num += 1
		for row in rows:
			# print("SKU es de " + str(row['sku']))
			# print(row['purchases_quantity'])
			# print(row['sales_quantity'])
			row['purchases_quantity'] = 0 if row['purchases_quantity'] == None else row['purchases_quantity']
			row['sales_quantity'] = 0 if row['sales_quantity'] == None else row['sales_quantity']
			week_sales = Sale_Product.objects.filter(product__id=row['id'], sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))['sale_quantity']
			week_sales = 0 if week_sales == None else week_sales
			stock_weeks = 0 if week_sales == 0 else int(round((row['purchases_quantity'] - row['sales_quantity']) / week_sales))
			# print(week_sales)
			# print(row['purchases_quantity'] - row['sales_quantity'])
			if (row['purchases_quantity'] - row['sales_quantity']) <= 0 or week_sales <= 0:
				last_day_stock = '9999-01-01'
			else:
				last_day_stock = str((datetime.datetime.today() + timedelta(days=round((row['purchases_quantity'] - row['sales_quantity']) / (week_sales / 7)))).date())

			data_write = {
					"sku":row['sku'],
					"description":row['description'],
					"sales_quantity":row['sales_quantity'],
					"merma_quantity": 0,
					"purchases_quantity": row['purchases_quantity'],
					"left": row['purchases_quantity'] - row['sales_quantity'],
					"week_sales":week_sales,
					"stock_weeks":stock_weeks,
					"last_day_stock":last_day_stock
			}
			self.write_file(ws, row_num, data_write, cell_format_data)
			row_num += 1
		wb.close()
		output.seek(0)
		filename = 'Inventario de Productos Asiamerica.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		print("--- %s seconds ---" % (time.time() - start_time))
		return response

class AutomaticRetailSalesLinioXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def f(self, x):
	   return {
			'pending': "Pendiente",
			'canceled': "Cancelado",
			'ready_to_ship': "Listo para Enviar",
			'delivered': "Entregado",
			'returned': "Devuelto",
			'shipped': "Enviado",
			'failed': "Fallido",
			'return_shipped_by_customer': "Devolución enviada por comprador",
			'return_waiting_for_approval': "Devolución esperando por aprobación",
		}[x]

	def write_file(self, ws, row, data, cell_format_data):
		ws.write(row, 0, str(data['date_created']), cell_format_data)
		ws.write(row, 1, str(data['hour_created']), cell_format_data)
		ws.write(row, 2, data['sku'], cell_format_data)
		ws.write(row, 3, data['quantity'], cell_format_data)
		ws.write(row, 4, data['unit_price'], cell_format_data)
		ws.write(row, 5, data['total_amount'], cell_format_data)
		ws.write(row, 6, data['revenue'], cell_format_data)
		ws.write(row, 7, int(data['id']), cell_format_data)
		ws.write(row, 8, "Linio", cell_format_data)
		ws.write(row, 9, data['comments'], cell_format_data)
		ws.write(row, 10, self.f(data['status']), cell_format_data)
		ws.write(row, 11, data['label'], cell_format_data)

	def get(self, request):
		date_from = self.request.query_params.get('date_from', None)
		date_to = self.request.query_params.get('date_to', None)
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Informe de registro de ventas retail Canal Linio ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		url_linio = "https://sellercenter-api.linio.cl?"
		date_from = datetime.datetime.strptime(date_from, '%d-%m-%Y')
		date_to = datetime.datetime.strptime(date_to, '%d-%m-%Y')
		parameters = {
		  'UserID': USER_ID,
		  'Version': '1.0',
		  'Action': 'GetOrders',
		  'Format':'JSON',
		  'Timestamp':datetime.datetime.now().isoformat(),
		  # 'CreatedAfter': (datetime.datetime.now() - timedelta(days=1)).isoformat(),
		  'CreatedAfter': date_from.isoformat(),
		  # 'CreatedBefore': datetime.datetime.now().isoformat()
		  'CreatedBefore': date_to.isoformat()
		}
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))
		parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))

		url_request = url_linio + concatenated
		data = requests.get(url_request).json()
		# print(url_request)
		# return Response(data={"data":["Mostrado con exito"]})
		columns = [{"name": 'Fecha Registro', "width":12, "rotate": True},{"name": 'Hora Registro', "width":12, "rotate": True}, {"name": 'SKU', "width": 9, "rotate": False}, {"name": 'Cantidad', "width": 6, "rotate": True}, {"name": 'Precio Unit', "width": 11, "rotate": True}, {"name": 'Total', "width": 15, "rotate": True}, {"name": 'Ganancia', "width": 10, "rotate": False}, {"name": 'Id venta', "width": 10, "rotate": False}, {"name": 'Canal', "width": 15, "rotate": True},  {"name": 'Comentarios', "width": 25, "rotate": True}, {"name": 'Estado', "width": 13, "rotate": True}, {"name": '¿Con Etiqueta?', "width": 10, "rotate": True}]
		row_num = 0
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
		rows = data["SuccessResponse"]["Body"]["Orders"]["Order"]
		row_num += 1
		for row in rows:
			parameters = {
			  'UserID': USER_ID,
			  'Version': '1.0',
			  'Action': 'GetOrderItems',
			  'OrderId': row['OrderId'],
			  'Format':'JSON',
			  'Timestamp': datetime.datetime.now().isoformat(),
			}
			concatenated = urllib.parse.urlencode(sorted(parameters.items()))
			parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
			concatenated = urllib.parse.urlencode(sorted(parameters.items()))
			url_request = url_linio + concatenated
			data_order = requests.get(url_request).json()["SuccessResponse"]["Body"]["OrderItems"]["OrderItem"]
			# print(data_order)
			format_date = datetime.datetime.strptime(row['CreatedAt'], "%Y-%m-%d %H:%M:%S")
			date_created=format_date.strftime('%d-%m-%Y')
			hour_created=format_date.strftime('%H:%M')
			unit_price = int(float(row['Price']) / int(row['ItemsCount']))
			if type(data_order) == list:
				shipping = 0
				for product in data_order:
					products = Product_SKU_Ripley.objects.filter(sku_ripley=product['ShopSku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price', 'color__description')
					for each in products:
						quantity = ((int(row['ItemsCount'])/len(data_order)) * each['pack_ripley__pack_product__quantity'])
						unit_price = (float(product['ItemPrice']) * (each['pack_ripley__pack_product__percentage_price']/100))/(quantity)
						data_write = {
							"date_created": date_created,
							"hour_created": hour_created,
							"sku":each['pack_ripley__pack_product__product_pack__sku'],
							"quantity":quantity,
							"unit_price":unit_price,
							"total_amount": quantity * unit_price,
							"revenue": 0,
							"id":row['OrderNumber'],
							"status":row['Statuses']['Status'],
							"comments":product['Name'],
							"label":"SI"
						}
						self.write_file(ws, row_num, data_write, cell_format_data)
						row_num += 1
					shipping += float(product['ShippingAmount'])
				shipping = int(shipping)

			else:
				products = Product_SKU_Ripley.objects.filter(sku_ripley=data_order['ShopSku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price', 'color__description')
				for each in products:
					quantity = int(row['ItemsCount']) * each['pack_ripley__pack_product__quantity']
					unit_price = (float(row['Price'])* (each['pack_ripley__pack_product__percentage_price']/100))/(quantity)
					data_write = {
							"date_created": date_created,
							"hour_created": hour_created,
							"sku":each['pack_ripley__pack_product__product_pack__sku'],
							"quantity":quantity,
							"unit_price":unit_price,
							"total_amount": quantity * unit_price,
							"revenue": 0,
							"id":row['OrderNumber'],
							"status":row['Statuses']['Status'],
							"comments":data_order['Name'],
							"label":"SI"
					}
					self.write_file(ws, row_num, data_write, cell_format_data)
					row_num += 1
				shipping = int(float(data_order['ShippingAmount']))
			data_write = {
				"date_created": date_created,
				"hour_created": hour_created,
				"sku":1,
				"quantity":1,
				"unit_price":shipping,
				"total_amount": shipping,
				"revenue": 0,
				"id":row['OrderNumber'],
				"status":row['Statuses']['Status'],
				"comments":"",
				"label":"SI"
			}
			self.write_file(ws, row_num, data_write, cell_format_data)
			row_num += 1

		# print(data["total_count"])
		wb.close()
		output.seek(0)
		filename = 'Informe de registro de ventas retail Canal Linio.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		print("--- %s seconds ---" % (time.time() - start_time))
		return response

	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl","abel.mejias@asiamerica.cl", "mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code = Product_Code.objects.filter(code_seven_digits=each)[0]
				message = product_code.product.description + " Variante: " + product_code.color.description + "("+str(each) + ")"
			else:
				product_sale = Product_Sale.objects.filter(sku=each)[0]
				message = product_sale.description + "("+str(each) + ")"
			message_append.append(message)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(products_sku).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def post(self, request):
		url_linio = "https://sellercenter-api.linio.cl?"

		date_from = datetime.datetime.strptime(request.data['date_from'], '%d-%m-%Y').replace(hour=1, minute=00).isoformat()
		date_to = datetime.datetime.strptime(request.data['date_to'], '%d-%m-%Y').replace(hour=23, minute=00).isoformat()
		parameters = {
		  'UserID': USER_ID,
		  'Version': '1.0',
		  'Action': 'GetOrders',
		  'Format':'JSON',
		  'Timestamp':datetime.datetime.now().isoformat(),
		  'CreatedAfter': date_from,
		  # 'CreatedAfter': date_from.isoformat(),
		  'CreatedBefore': date_to
		  # 'CreatedBefore': date_to.isoformat()
		}
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))
		parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))

		url_request = url_linio + concatenated
		data = requests.get(url_request).json()
		#print(data)
		rows = data["SuccessResponse"]["Body"]["Orders"]["Order"]
		if type(rows) != list:
			rows = [rows]
		# print(rows)
		products_stock = []
		for row in rows:
			if Sale.objects.filter(order_channel=int(row['OrderNumber'])):
				sale = Sale.objects.get(order_channel=int(row['OrderNumber']))
				status_sale = self.f(row['Statuses']['Status'])
				if status_sale != sale.channel_status:
					sale.channel_status = status_sale
					sale.save()
				if sale.tracking_number == None:
					parameters = {
						'UserID': USER_ID,
						'Version': '1.0',
						'Action': 'GetOrderItems',
						'OrderId': row['OrderId'],
						'Format':'JSON',
						'Timestamp': datetime.datetime.now().isoformat(),
					}
					concatenated = urllib.parse.urlencode(sorted(parameters.items()))
					parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
					concatenated = urllib.parse.urlencode(sorted(parameters.items()))

					url_request = url_linio + concatenated
					print(url_request)
					data_order = requests.get(url_request).json()["SuccessResponse"]["Body"]["OrderItems"]["OrderItem"]
					if type(data_order) == list:
						tracking_number = data_order[0]['TrackingCode']
						if tracking_number != '':
							sale.tracking_number = tracking_number
							sale.save()
					else:
						tracking_number = data_order['TrackingCode']
						if tracking_number != '':
							sale.tracking_number = tracking_number
							sale.save()
				continue
			parameters = {
			  'UserID': USER_ID,
			  'Version': '1.0',
			  'Action': 'GetOrderItems',
			  'OrderId': row['OrderId'],
			  'Format':'JSON',
			  'Timestamp': datetime.datetime.now().isoformat(),
			}
			concatenated = urllib.parse.urlencode(sorted(parameters.items()))
			parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
			concatenated = urllib.parse.urlencode(sorted(parameters.items()))

			url_request = url_linio + concatenated
			#print(url_request)

			data_order = requests.get(url_request).json()["SuccessResponse"]["Body"]["OrderItems"]["OrderItem"]

			date_sale = datetime.datetime.strptime(row['CreatedAt'], "%Y-%m-%d %H:%M:%S")

			hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
			if type(data_order) == list:
				tracking_number = data_order[0]['TrackingCode']
				if tracking_number == '':
					tracking_number = None
			else:
				tracking_number = data_order['TrackingCode']
				if tracking_number == '':
					tracking_number = None
			sale = Sale.objects.create(
				order_channel=int(row['OrderNumber']),
				total_sale=0,
				declared_total=0,
				revenue=0,
				user=User.objects.get(username='api-linio'),
				channel=Channel.objects.get(name="Linio"),
				channel_status=self.f(row['Statuses']['Status']),
				document_type="sdt",
				payment_type='transferencia retail',
				sender_responsable="vendedor",
				status="pendiente",
				tracking_number=tracking_number,
				last_user_update=User.objects.get(username='api-linio'),
				date_sale=date_sale,
				#cut_inventory="I1",
				hour_sale_channel=hour_sale_channel,
			)
			# unit_price = int(float(row['Price']) / int(row['ItemsCount']))
			total = 0
			comments = ""
			total_products = 0
			if type(data_order) == list:
				# Si posee mas productos en el carrito se itera cada uno para luego iterar nuevamente cada producto (si es pack) o no
				shipping = 0
				for product in data_order:
					shipping += float(product['ShippingAmount'])
				total_sale_no_shipping = int(float(row['Price'])) - shipping
				for product in data_order:
					#comments += product['Name'] + "/"
					#if Product_Sale.objects.filter(sku=product['Sku'][:5]):
					#	products =  Product_Sale.objects.filter(sku=product['Sku'][:5]).annotate(pack_ripley__pack_product__product_pack__sku=F('sku'), pack_ripley__pack_product__quantity=Value(1, output_field=IntegerField()), pack_ripley__pack_product__percentage_price=Value(100, output_field=IntegerField())).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price')
					#else:
					products = Product_SKU_Ripley.objects.filter(sku_ripley=product['ShopSku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price', 'color__description', 'pack_ripley__id')
					print(products)
					#print(product['ShopSku'])
					for each in products:
						if each.get('color__description', None) is not None:
							product_code = Product_Code.objects.filter(product__sku=each['pack_ripley__pack_product__product_pack__sku'])
							if product_code.count() == 1:
								product_code = product_code[0]
							else:
								code_seven = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6,6)).filter(code_seven__gte=50, product__sku=each['pack_ripley__pack_product__product_pack__sku'], color__description=each['color__description'])
								if code_seven:
									product_code = code_seven[0]
								else:
									product_code = None
						else:
							product_code = None
						quantity = ((int(row['ItemsCount'])/len(data_order)) * each['pack_ripley__pack_product__quantity'])
						unit_price = (float(product['ItemPrice']) * (each['pack_ripley__pack_product__percentage_price']/100))/(quantity)
						if product['Variation'] is not None:
							comment_unit= product['Name'] + " " + product['Variation']
							comments += product['Name'] + " " +product['Variation'] + "/"
						else:
							comment_unit = product['Name']
							comments += product['Name'] + "/"
						if ((unit_price * quantity) > total_sale_no_shipping):
							unit_price = total_sale_no_shipping/quantity
						else:
							if not unit_price.is_integer():
								unit_price = self.roundup(unit_price)
								total_sale_no_shipping -= (unit_price * quantity)

						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=each['pack_ripley__pack_product__product_pack__sku']),
							product_code=product_code,
							sale=sale,
							quantity=quantity,
							unit_price=unit_price,
							total=(quantity * unit_price),
							#from_pack=Pack.objects.get(id=each['pack_ripley__id']),
							comment_unit=comment_unit,
						)
						quantity_stock = Product_Sale.objects.get(sku=each['pack_ripley__pack_product__product_pack__sku']).stock - (quantity)
						Product_Sale.objects.filter(sku=each['pack_ripley__pack_product__product_pack__sku']).update(stock=quantity_stock)
						if product_code is not None:
							quantity_stock_color = product_code.quantity - (quantity)
							Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
							if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not (product_code.code_seven_digits in products_stock)):
								products_stock.append(product_code.code_seven_digits)
						else:
							if quantity_stock <= 5 and quantity_stock > 0 and (not (each['pack_ripley__pack_product__product_pack__sku'] in products_stock)):
								products_stock.append(each['pack_ripley__pack_product__product_pack__sku'])
						total += (quantity * unit_price)
						total_products += quantity
				shipping = int(shipping)
			else:
				total_sale_no_shipping = int(float(row['Price'])) - int(float(data_order['ShippingAmount']))
				#comments += data_order['Name'] + "/"
				#if Product_Sale.objects.filter(sku=data_order['Sku'][:5]):
				#	products =  Product_Sale.objects.filter(sku=data_order['Sku'][:5]).annotate(pack_ripley__pack_product__product_pack__sku=F('sku'), pack_ripley__pack_product__quantity=Value(1, output_field=IntegerField()), pack_ripley__pack_product__percentage_price=Value(100, output_field=IntegerField())).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price')
				#else:
				products = Product_SKU_Ripley.objects.filter(sku_ripley=data_order['ShopSku']).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price', 'color__description', 'pack_ripley__id')
				print(products)
				if data_order['Variation'] is not None:
					comments += data_order['Name'] + " " +data_order["Variation"] + "/"
				else:
					comments += data_order['Name'] + "/"
				for each in products:
					print(each)
					if each.get('color__description', None) is not None:
						product_code = Product_Code.objects.filter(product__sku=each['pack_ripley__pack_product__product_pack__sku'])
						if product_code.count() == 1:
							product_code = product_code[0]
						else:
							code_seven = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6, 6)).filter(code_seven__gte=50, product__sku=each['pack_ripley__pack_product__product_pack__sku'], color__description=each['color__description'])
							if code_seven:
								product_code = code_seven[0]
							else:
								product_code = None
					else:
						product_code = None
					quantity = int(row['ItemsCount']) * each['pack_ripley__pack_product__quantity']
					unit_price = (float(data_order['ItemPrice'])* (each['pack_ripley__pack_product__percentage_price']/100))/(quantity)
					if ((unit_price * quantity) > total_sale_no_shipping):
						unit_price = total_sale_no_shipping/quantity
					else:
						if not unit_price.is_integer():
							unit_price = self.roundup(unit_price)
							total_sale_no_shipping -= (unit_price * quantity)
					if data_order['Variation'] is not None:
						comment_unit = data_order['Name'] + " " + data_order['Variation']
						#comments += data_order['Name'] + " " + data_order['Variation'] + "/"
					else:
						comment_unit = data_order['Name']
						#comments += data_order['Name'] + "/"
					sale_product = Sale_Product.objects.create(
						product=Product_Sale.objects.get(sku=each['pack_ripley__pack_product__product_pack__sku']),
						product_code=product_code,
						sale=sale,
						quantity=quantity,
						unit_price=unit_price,
						total=(quantity * unit_price),
						#from_pack=Pack.objects.get(id=each['pack_ripley__id']),
						comment_unit=comment_unit,
					)
					quantity_stock = Product_Sale.objects.get(sku=each['pack_ripley__pack_product__product_pack__sku']).stock - (quantity)
					Product_Sale.objects.filter(sku=each['pack_ripley__pack_product__product_pack__sku']).update(stock=quantity_stock)
					if product_code is not None:
						quantity_stock_color = product_code.quantity - (quantity)
						Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
						if quantity_stock_color <= 5 and quantity_stock_color >= 0 and (not (product_code.code_seven_digits in products_stock)):
							products_stock.append(product_code.code_seven_digits)
					else:
						if quantity_stock <= 5 and quantity_stock >= 0 and (not (each['pack_ripley__pack_product__product_pack__sku'] in products_stock)):
							products_stock.append(each['pack_ripley__pack_product__product_pack__sku'])
					total += (quantity * unit_price)
					total_products += quantity
				if comments == '':
					comments += data_order['Name'] + "/"
				shipping = int(float(data_order['ShippingAmount']))
			sale_product = Sale_Product.objects.create(
				product=Product_Sale.objects.get(sku=1),
				sale=sale,
				quantity=1,
				unit_price=shipping,
				total=shipping,
			)
			total += shipping
			sale.total_sale = total
			sale.total_number_of_product = total_products
			sale.declared_total = total - shipping
			sale.comments = comments
			sale.save()
		if products_stock:
			self.alert_stock(products_stock)
		return Response(data={"sales": ['Ventas de Linio actualizadas exitosamente']}, status=status.HTTP_200_OK)

class ExportXLSSKUViewSet(APIView):
	permission_classes = (AllowAny,)


	def write_file(self, ws, row, data, cell_format_data):
		ws.write(row, 0, data['sku'], cell_format_data)
		ws.write(row, 1, data['description'], cell_format_data)
		if data['category'] is not None:
			ws.write(row, 2, data['category'].name, cell_format_data)
		else:
			ws.write(row, 2, "-", cell_format_data)
		# ws.write(row, 2, data['category'], cell_format_data)
		# ws.write(row, 3, data['type_sku'], cell_format_data)
		ws.write(row, 3, data['height'], cell_format_data)
		ws.write(row, 4, data['length'], cell_format_data)
		ws.write(row, 5, data['width'], cell_format_data)
		ws.write(row, 6, data['weight'], cell_format_data)
		ws.write(row, 7, data['variation'], cell_format_data)
		ws.write(row, 8, data['code'], cell_format_data)
		if data['date_created'] is not None:
			ws.write(row, 9, str(data['date_created']), cell_format_data)


	def get(self, request):
		type_sku = self.request.query_params.get('type_sku', "normal")
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Listado de SKU ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		columns = [{"name": 'SKU', "width":12, "rotate": False},{"name": 'Descripción', "width":40, "rotate": False},{"name": "Categoría", "width": 10, "rotate": False}, {"name": 'Alto', "width": 6, "rotate": False}, {"name": 'Largo', "width": 6, "rotate": False}, {"name": 'Ancho', "width": 6, "rotate": False}, {"name": 'Peso', "width": 6, "rotate": False}, {"name": 'Variante', "width": 20, "rotate": False}, {"name": 'Códigos 7', "width": 20, "rotate": False}, {"name":"Fecha de Creación", "width":20, "rotate": False}]
		row_num = 0

		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
		if type_sku == 'normal':
			rows = Product_Sale.objects.all()
			pack = False
		else:
			rows = Pack.objects.annotate(sku_len=Length('sku_pack')).filter(sku_pack__startswith="5", sku_len=5)
			pack = True
		# row_num += 1
		for row in rows:
			if not pack:
				if row.product_code.all():
					for e in row.product_code.all():
						data_write = {
							"sku":row.sku,
							"description": row.description,
							"category": row.category_product_sale,
							"height":row.height,
							"width":row.width,
							"length":row.length,
							"weight":row.weight,
							"variation":e.color.description,
							"code":e.code_seven_digits,
							"date_created": e.date_created.date(),
						}
						row_num += 1
						self.write_file(ws, row_num, data_write, cell_format_data)
				else:
					data_write = {
							"sku":row.sku,
							"description": row.description,
							"category": row.category_product_sale,
							"height":row.height,
							"width":row.width,
							"length":row.length,
							"weight":row.weight,
							"variation":'-',
							"code":'-',
							"date_created": row.date_created.date()
						}
					row_num += 1
					self.write_file(ws, row_num, data_write, cell_format_data)
			else:
				data_write = {
							"sku":row.sku_pack,
							"description": row.description,
							"category": row.category_pack,
							"height":row.height,
							"width":row.width,
							"length":row.length,
							"weight":row.weight,
							"variation":'-',
							"code":'-',
							"date_created":row.date_created.date()
						}
				row_num += 1
				self.write_file(ws, row_num, data_write, cell_format_data)
		wb.close()
		output.seek(0)
		filename = 'Listado de productos.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		print("--- %s seconds ---" % (time.time() - start_time))
		return response

class ShippingDocument(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request):
		for each_sale in request.data:
			sale = Sale.objects.get(id=each_sale['id'])
			note_format = False
			token = Token.objects.get(id=1).token_ml
			if sale.channel.name == 'Mercadolibre':
				notes = requests.get("https://api.mercadolibre.com/orders/" + str(sale.order_channel) + "/notes?access_token=" +token).json()[0]['results'][::-1]
			else:
				notes = requests.get("https://api.mercadoshops.com/v1/shops/216244489/orders/" + str(sale.order_channel) + "/notes?access_token=" +token).json()[::-1]
			# notes = [{"note": "Datos: Abel Eduardo De la castañeda Mejias Dominguez, 26.499.409-8, +56 9 7684 6957, Eloy Rosales 3381 Conchali Region Metropolitana"}]
			# print(notes)
			for note in notes:
				if "Datos:" in note["note"]:
					note_format = True
			if not note_format:
				return Response(data={"note": "La venta " +str(sale.order_channel)+" no posee nota, o el formato de la misma no es correcto (El formato de notas es: Datos: Nombre, R.U.T, Télefono, Dirección)"}, status=status.HTTP_400_BAD_REQUEST)

		merger = PdfFileMerger()
		token = Token.objects.get(id=1).token_ml
		labels_id = []
		for each_sale in request.data:
			sale = Sale.objects.get(id=each_sale['id'])
			if sale.channel.name == 'Mercadolibre':
				notes = requests.get("https://api.mercadolibre.com/orders/" + str(sale.order_channel) + "/notes?access_token=" +token).json()[0]['results'][::-1]
			else:
				notes = requests.get("https://api.mercadoshops.com/v1/shops/216244489/orders/" + str(sale.order_channel) + "/notes?access_token=" +token).json()[::-1]
			# notes = [{"note": "Datos: Abel Eduardo De la castañeda Mejias Dominguez, 26.499.409-8, +56 9 7684 6957, Eloy Rosales 3381 Conchali Region Metropolitana"}]
			word_split = ''
			for note in notes:
				if "Datos:" in note["note"]:
					word_split = note['note']
			# if word_split == '':
			# 	return Response(data={"note": "Por favor agregar nota en la plataforma de mercadolibre para poder crear Documento de envío (El formato es: Datos: Nombre, R.U.T, Télefono, Dirección)"}, status=status.HTTP_400_BAD_REQUEST)
			# print(word_split)
			styles = getSampleStyleSheet()
			styleN = styles["BodyText"]
			styleN.fontSize = 22
			styleN.leading = 22
			note_split = word_split.replace('Datos:','').lstrip().split(",")
			name =Paragraph(note_split[0].upper(), styleN)
			rut =Paragraph(note_split[1].lstrip(), styleN)
			phone =Paragraph(note_split[2].lstrip(), styleN)
			address =Paragraph(note_split[3].lstrip().upper(), styleN)
			buff = BytesIO()
			doc = SimpleDocTemplate(buff, pagesize=letter, rightMargin=10, leftMargin=10, topMargin=10, bottomMargin=18,)
			clientes = []
			locale.setlocale(locale.LC_TIME, 'es_CL.utf8')
			top_heading = "Venta #" + str(sale.order_channel) + " - " +sale.date_sale.strftime('%d de %B') + " " + sale.hour_sale_channel
			style = ParagraphStyle(
				name='Normal',
				spaceBefore = 10,
				fontSize=14,
			)
			style.leading = 16
			header = Paragraph("DESTINATARIO", styles['Heading1'])
			clientes.append(header)
			header = Paragraph(top_heading.upper(), styleN)
			# clientes.append(header)
			headings1 = (header, '',)
			headings2 = ('DESTINATARIO', name,)
			data = [('RUT', rut),('TELEFONO', phone),('DIRECCION', address)]
			t = Table([headings1, headings2] + data, hAlign='LEFT', colWidths=["28%","72%"], rowHeights=(17*mm,32*mm, 16*mm,16*mm,35*mm,))
			t.setStyle(TableStyle(
				[
					('GRID', (0, 0), (5, -1), 1, colors.black),
					('SPAN', (0, 0), (1, 0)),
					('VALIGN',(0,0),(-1,-1),'MIDDLE'),
					('BACKGROUND', (0, 0), (-1, 0), colors.white),
					('FONTSIZE', (0, 0), (-1, -1), 20),
				]
			))
			clientes.append(t)
			header = Paragraph("REMITENTE", styles['Heading1'])
			clientes.append(header)
			headings = ('REMITENTE', "ASIAMERICA LTDA")
			list_products = []
			for product in Sale_Product.objects.filter(sale=sale).exclude(product__sku=1):
				if product.product_code == None:
					color = ''
				else:
					color = product.product_code.color.description
				product_text = product.product.description +" "+ color + " (" + str(product.quantity) + ") VALOR: " + str(product.total)
				list_products.append(Paragraph(product_text, style=style,))
			list_flowable = ListFlowable(list_products)
			data = [("RUT", "76.431.547-2"), ("DIRECCION","ANTONIO VARAS 2156 - ÑUÑOA",), ("TELEFONO","+56 9 3214 6174",), ("PRODUCTOS", list_flowable)]
			t = Table([headings] + data, colWidths=["28%","72%"], rowHeights=(20*mm, 20*mm,20*mm,20*mm,45*mm))
			t.setStyle(TableStyle(
				[
					('GRID', (0, 0), (5, -1), 1, colors.black),
					('VALIGN',(0,0),(-1,-1),'MIDDLE'),
					('BACKGROUND', (0, 0), (-1, 0), colors.white),
					('FONTSIZE', (0, 0), (-1, -1), 22),
				]
			))
			clientes.append(t)
			doc.build(clientes)
			labels_id.append(each_sale['id'])
			if len(request.data) == 1:
				report_encoded = base64.b64encode(buff.getvalue())
				return Response(data={"pdf":report_encoded, "id_pedidos": labels_id}, status=status.HTTP_200_OK)
			merger.append(PdfFileReader(buff))
			buff.close()
		today = datetime.datetime.today().strftime("%Y-%m-%d %H %M %S")
		file_manifiesto = "Etiquetas " + str(today)
		merger.write(os.path.expanduser('~/Documents/Etiquetas/' + file_manifiesto + '.pdf'))
		merger.close()
		short_report = open(os.path.expanduser('~/Documents/Etiquetas/' + file_manifiesto + '.pdf'), 'rb')
		report_encoded = base64.b64encode(short_report.read())
		return Response(data={"pdf": report_encoded, "id_pedidos": labels_id}, status=status.HTTP_200_OK)

class UpdateShippingSubStatus(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		sales = Sale.objects.filter(shipping_type='por acordar', status='pendiente', channel__name='Mercadolibre', number_document__isnull=True)
		token = Token.objects.get(id=1).token_ml
		for sale in sales:
			label_generation = False
			sub_status = ''
			notes = requests.get("https://api.mercadolibre.com/orders/" + str(sale.order_channel) + "/notes?access_token=" +token).json()[0]['results'][::-1]
			for note in notes:
				if "Datos:" in note["note"]:
					label_generation = True
					sub_status = 'envío por pagar'
					break
				if "Presencial" in note["note"]:
					label_generation = True
					sub_status = 'retiro presencial'
					break
			if label_generation:
				sale_data = Sale.objects.get(id=sale.id)
				sale_data.shipping_type_sub_status = sub_status
				sale_data.save()
		return Response(data={"sales": ['Ventas por acordar actualizadas']}, status=status.HTTP_200_OK)

class Sales_ReportViewSet(ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = Sales_Report.objects.all().order_by('-id')
	serializer_class = Sales_ReportSerializer
	pagination_class = CustomPagination
	http_method_names = ['get',]
	filter_backends = (search_filter.SearchFilter,)
	search_fields  = ('user_created__first_name','user_created__last_name', 'id',)


	def retrieve(self, request, *args, **kwargs):
		instance = self.get_object()
		serializer = self.get_serializer(instance)
		path_to_file = os.path.expanduser('~/Documents/Manifiestos/' + instance.report_file_name + '.pdf')
		short_report = open(path_to_file, 'rb')
		report_encoded = base64.b64encode(short_report.read())
		newdict={'pdf':report_encoded}
		newdict.update(serializer.data)
		return Response(newdict)

class Sales_ReportCashViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		date_from = self.request.query_params.get('date_from', None)
		date_to = self.request.query_params.get('date_to', None)
		if date_to is not None:
			string_date_annotate = date_from + " al " + date_to
		else:
			string_date_annotate = date_from
			date_to = date_from
		sales = User.objects.filter(user_created__date_sale__range=[date_from, date_to], user_created__status='entregado').annotate(sales_number=Count('user_created__id'),products_number=Sum('user_created__total_number_of_product'), total_sales=Sum('user_created__total_sale'), date=Value(string_date_annotate, output_field=CharField()), user=Concat(F('first_name'),Value(' ', output_field=CharField()), F('last_name')), cash=Sum(Case(When(user_created__payment_type='efectivo', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), check=Sum(Case(When(user_created__payment_type='cheque', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), debit_card=Sum(Case(When(user_created__payment_type='tarjeta de debito', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), credit_card=Sum(Case(When(user_created__payment_type='tarjeta de credito', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), transfer=Sum(Case(When(user_created__payment_type='transferencia', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), retail_transfer=Sum(Case(When(user_created__payment_type='transferencia retail', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), to_pay=Sum(Case(When(user_created__payment_type='por pagar', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), no_pay=Sum(Case(When(user_created__payment_type='sin pago', then=F('user_created__total_sale')), default=0, output_field=IntegerField()))).values('user', 'sales_number', 'products_number', 'total_sales', 'date', 'cash', 'debit_card', 'credit_card', 'transfer', 'to_pay', 'no_pay', 'check', 'retail_transfer').order_by('-total_sales')
		df = pd.DataFrame(list(sales))
		if sales:
			g = df.groupby('user', as_index=False).sum()
		else:
			return Response(data={"sales": sales}, status=status.HTTP_200_OK)
		# print(g.to_dict('r'))

		return Response(data={"sales": g.to_dict('r'), "date_search": string_date_annotate}, status=status.HTTP_200_OK)

	def post(self, request):
		date_from = request.data.get('date_from', None)
		date_to = request.data.get('date_to', None)
		if date_to is not None:
			string_date_annotate = date_from + " al " + date_to
		else:
			string_date_annotate = date_from
			date_to = date_from
		buff = BytesIO()
		doc = SimpleDocTemplate(buff, pagesize=letter, rightMargin=40, leftMargin=40, topMargin=60, bottomMargin=18,)
		clientes = []
		styles = getSampleStyleSheet()
		today = datetime.datetime.today().strftime("%Y-%m-%d %H:%M")
		header = Paragraph("Informe de Cuadratura de Caja " +string_date_annotate, styles['Heading2'])
		date_generate = Paragraph("Fecha: " +str(today), styles['Heading2'])
		clientes.append(header)
		clientes.append(date_generate)
		headings = ('Usuario', 'N° Ventas','N° Productos', 'Efectivo', 'TDD','TDC','Transf.','Cheque','Por pagar','Sin pago','Mixto', 'T. Retail','Total',)

		rows = User.objects.filter(user_created__date_sale__range=[date_from, date_to], user_created__status='entregado').annotate(sales_number=Count('user_created__id'),products_number=Sum('user_created__total_number_of_product'), total_sales=Sum('user_created__total_sale'), date=Value(string_date_annotate, output_field=CharField()), user=Concat(F('first_name'),Value(' ', output_field=CharField()), F('last_name')), cash=Sum(Case(When(user_created__payment_type='efectivo', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), check=Sum(Case(When(user_created__payment_type='cheque', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), debit_card=Sum(Case(When(user_created__payment_type='tarjeta de debito', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), credit_card=Sum(Case(When(user_created__payment_type='tarjeta de credito', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), transfer=Sum(Case(When(user_created__payment_type='transferencia', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), retail_transfer=Sum(Case(When(user_created__payment_type='transferencia retail', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), to_pay=Sum(Case(When(user_created__payment_type='por pagar', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), no_pay=Sum(Case(When(user_created__payment_type='sin pago', then=F('user_created__total_sale')), default=0, output_field=IntegerField())), mixt_pay=Sum(Case(When(user_created__payment_type='mixto', then=F('user_created__total_sale')), default=0, output_field=IntegerField()))).values('user', 'sales_number', 'products_number', 'total_sales', 'date', 'cash', 'debit_card', 'credit_card', 'transfer', 'to_pay', 'no_pay', 'check', 'retail_transfer', 'mixt_pay').order_by('-total_sales')
		df = pd.DataFrame(list(rows))
		g = df.groupby('user', as_index=False).sum()
		# print(g.to_dict('r'))

		data = [(row['user'], row['sales_number'], row['products_number'], row['cash'],row['debit_card'],row['credit_card'],row['transfer'],row['check'],row['to_pay'],row['no_pay'],row['mixt_pay'],row['retail_transfer'], row['total_sales'],) for row in g.to_dict('r')]
		t = Table([headings] + data,)
		t.setStyle(TableStyle(
			[
				('GRID', (0, 0), (12, -1), 1, colors.black),
				('LINEBELOW', (0, 0), (-1, 0), 2, colors.black),
				('BACKGROUND', (0, 0), (-1, 0), colors.orange),
				('FONTSIZE', (0, 0), (-1, -1), 8),
			]
		))
		clientes.append(t)
		d = Drawing(500, 100)
		d.add(Line(65, 0, 205, 0))
		e = Drawing(800, 0)
		e.add(Line(320, 0, 450, 0))
		clientes.append(d)
		clientes.append(e)
		pageTextStyleCenter = ParagraphStyle(name="left", alignment=TA_CENTER, fontSize=13, leading=10)
		tbl_data = [
			[Paragraph("Entrega", pageTextStyleCenter), Paragraph("Recibe", pageTextStyleCenter)],

		]
		tbl = Table(tbl_data)
		clientes.append(tbl)
		doc.build(clientes)
		# response = HttpResponse(content_type='application/pdf')
		# response.write(buff.getvalue())
		# buff.close()
		# return response
		report_encoded = base64.b64encode(buff.getvalue())
		buff.close()
		return Response(data={"pdf_file":report_encoded}, status=status.HTTP_200_OK)

class Product_Code_CreateViewSet(ModelViewSet):
	http_method_names = ['post', 'put']
	permission_classes = (IsAuthenticated,)
	serializer_class = Product_Code_CreateSerializer
	queryset = Product_Code.objects.all()

class GrouponUpload(APIView):
	permission_classes = (IsAuthenticated,)

	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl","abel.mejias@asiamerica.cl", "mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code = Product_Code.objects.filter(code_seven_digits=each)[0]
				message = product_code.product.description + " Variante: " + product_code.color.description + "(" + str(each) + ")"
			else:
				product_sale = Product_Sale.objects.filter(sku=each)[0]
				message = product_sale.description + "(" + str(each) + ")"
			message_append.append(message)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def post(self, request):
		# print(request.data)
		products_stock = []
		# print(request.data)
		for i in request.data:
			if Product_Sale.objects.filter(sku=i['description'][i['description'].find("(")+1:i['description'].find(")")].split("-")[0][:5]).count() == 0:
				return Response(data={"sku":["El SKU " + str(i['description'][i['description'].find("(")+1:i['description'].find(")")].split("-")[0][:5]) + " no se encuentra en los registros"]}, status=status.HTTP_400_BAD_REQUEST)
		for row in request.data:
			coupon = row['coupon'].replace("CL", "").replace('XX','')
			if Sale.objects.filter(order_channel=coupon):
				continue
			else:
				sku_description = row['description'][row['description'].find("(")+1:row['description'].find(")")].split("-")[0]
				product = Product_Sale.objects.filter(sku=sku_description[:5])
				if len(sku_description) == 7:
					product_code = Product_Code.objects.filter(code_seven_digits=sku_description)
					if product_code:
						product_code = product_code[0]
					else:
						product_code = None
				else:
					product_code = None
				total_sale = row['total_sale']
				#product_code = None
				# Se crea la venta con la información dada.
				sale = Sale.objects.create(
					order_channel=coupon,
					total_sale=total_sale,
					user=User.objects.get(username='api-groupon'),
					channel=Channel.objects.get(name='Groupon'),
					document_type='sdt',
					payment_type='transferencia retail',
					comments=row['description'],
					sender_responsable='comprador',
					status='pendiente',
					declared_total=total_sale,
					#cut_inventory="I1",
				)
				sale_product = Sale_Product.objects.create(
					product=product[0],
					product_code=product_code,
					sale=sale,
					quantity=1,
					unit_price=total_sale,
					total=total_sale,
				)
				# commission = Commission.objects.create(
				# 	sale_product=sale_product,
				# 	api_commission=0,
				# 	calculate_commission=total_sale*0.35,
				# )
				quantity_stock = Product_Sale.objects.get(sku=product[0].sku).stock - 1
				Product_Sale.objects.filter(sku=product[0].sku).update(stock=quantity_stock)
				if product_code is not None:
					quantity_stock_color = product_code.quantity - 1
					Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
					if quantity_stock_color <= 5 and quantity_stock_color >= 0 and (not (product_code.code_seven_digits) in products_stock):
						products_stock.append(product_code.code_seven_digits)
				else:
					if quantity_stock <= 5 and quantity_stock >= 0 and (not (product[0].sku in products_stock)):
						products_stock.append(product[0].sku)
		if products_stock:
			self.alert_stock(products_stock)
		return Response(data={"sales": ["Ventas cargadas exitosamente"]}, status=status.HTTP_200_OK)

class FalabellaUpload(APIView):
	permission_classes = (IsAuthenticated,)

	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl","abel.mejias@asiamerica.cl", "mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code = Product_Code.objects.filter(code_seven_digits=each)[0]
				message = product_code.product.description + " Variante: " + product_code.color.description + "(" + str(each) + ")"
			else:
				product_sale = Product_Sale.objects.filter(sku=each)[0]
				message = product_sale.description + "(" + str(each) + ")"
			message_append.append(message)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def calculate_commission(self, sale_product):
		percentage_commission = CommissionChannel.objects.filter(product_sale__sku=sale_product.product.sku)
		if percentage_commission:
			return ((percentage_commission[0].percentage_commission * sale_product.total) / 100)
		else:
			return 0

	@transaction.atomic
	def post(self, request):
		today = datetime.datetime.now().date()
		sorted_order = sorted(request.data, key=itemgetter('order_channel'))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['order_channel']):
			group_order.append(list(group))
		products_stock = []
		for row in group_order:
			order_channel = row[0]['order_channel']
			#print(row[0]['order_channel'])
			if Sale.objects.filter(order_channel=order_channel):
				sale = Sale.objects.get(order_channel=order_channel)
				if row[0].get('status', None) is not None:
					if sale.channel_status != row[0]['status']:
						sale.channel_status = row[0]['status']
						if row[0]['status'] == 'Cancelado':
							sale.status = "cancelado"
						sale.save()
				continue
			else:
				# Crear venta
				print(row[0]['order_channel'])
				try:
					date_sale = datetime.datetime.strptime(row[0]['date_sale'], "%d-%m-%Y %H:%M")
				except ValueError as e:
					date_sale = datetime.datetime.strptime(row[0]['date_sale'], "%d/%m/%Y %H:%M")
				date_compare = date_sale.date()
				hour_sale = ('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
				sale = Sale.objects.create(
					order_channel=row[0]['order_channel'],
					total_sale=0,
					user=User.objects.get(username='api-falabella'),
					channel=Channel.objects.get(name='Falabella'),
					document_type='sdt',
					payment_type='transferencia retail',
					comments="",
					sender_responsable='vendedor',
					status='pendiente',
					date_sale=date_sale,
					hour_sale_channel=hour_sale,
					channel_status=row[0].get('status', None),
					shipping_date=datetime.datetime.strptime(row[0]['shipping_date'], "%d-%m-%Y").date(),
				)
				total = 0
				comments = ''
				for each in row:

					br = Browser()
					br.set_handle_robots(False)
					br.addheaders = [('User-agent', 'Firefox')]
					br.open( "https://www.falabella.com/falabella-cl/" )
					for form in br.forms():
						if form.attrs.get('id', None) == 'searchForm':
							br.form = form
							break
					br.form[ 'Ntt' ] = str(each['sku'].replace(" ", ""))
					br.submit()
					soup = BS(br.response().read(), 'lxml')
					data = soup.find_all('script')
					script_selected = ""
					for i in data:
					    if "var fbra_browseMainProductConfig" in i.text:
					        script_selected = i
					        break

					try:
						json_text = re.search(r'^\s*var fbra_browseMainProductConfig\s*=\s*({.*?})\s*;\s*$', script_selected.string, flags=re.DOTALL | re.MULTILINE).group(1)
						data = json.loads(json_text)["state"]["product"]["prices"]
						offer_price = data[0]['originalPrice']
						page_price = int(offer_price.replace(".",""))
					except Exception as e:
						try:
							data = soup.find_all('script')[9]
							json_text = re.search(r'^\s*var fbra_browseMainProductConfig\s*=\s*({.*?})\s*;\s*$', data.string, flags=re.DOTALL | re.MULTILINE).group(1)
							data = json.loads(json_text)["state"]["product"]["prices"]
							offer_price = data[0]['originalPrice']
							page_price = int(offer_price.replace(".",""))
						except Exception as e:
							page_price = None


					comments += each['description'] + " - "
					sku_filter = Product_SKU_Ripley.objects.filter(sku_ripley=str(each['sku'].replace(" ", "")))
					if sku_filter:
						if sku_filter[0].pack_ripley is not None:
							products = Pack_Products.objects.filter(pack=sku_filter[0].pack_ripley)
							# products_dimensions = products.aggregate(height_selected=Max('product_pack__height'), weight_selected=Sum('product_pack__weight'), length_selected=Max('product_pack__length'), width_selected=Sum('product_pack__width'))
							products = products.annotate(weight=F('product_pack__weight'), height=F('product_pack__height'), width=F('product_pack__width'), length=F('product_pack__length')).values("product_pack__sku", "quantity", "percentage_price",'weight','length','height','width')
						else:
							products = Product_Sale.objects.filter(id=sku_filter[0].product_ripley.id)
							# products_dimensions = products.aggregate(height_selected=Max('height'), weight_selected=Sum('weight'), length_selected=Max('length'), width_selected=Sum('width'))
							products = products.annotate(product_pack__sku=F('sku'), quantity=Value(1, output_field=IntegerField()), percentage_price=Value(100, output_field=IntegerField())).values('product_pack__sku', 'quantity', 'percentage_price','weight','length','height','width')
					else:
						print(str(each['sku'].replace(" ", '')) + " no esta cargada")
						products = []
					# products = Product_SKU_Ripley.objects.filter(sku_ripley=str(each['sku'].replace(" ", ""))).values('pack_ripley__pack_product__product_pack__sku', 'pack_ripley__pack_product__quantity', 'pack_ripley__pack_product__percentage_price', 'color__description', 'pack_ripley__id')
					# print(products)
					height = 0
					length = 0
					weight = 0
					width = 0
					for product in products:
						pack_multiple_quantity = False
						if len(products) > 1:
							unit_price = Price_Product.objects.get(pack__id=sku_filter[0].pack_ripley.id)
						else:
							try:
								unit_price = Price_Product.objects.get(product_sale__sku=product['product_pack__sku'])
							except Price_Product.DoesNotExist as e:
								pack_multiple_quantity = True
								unit_price = Price_Product.objects.get(pack__id=sku_filter[0].pack_ripley.id)
						if unit_price.falabella_offer_price is not None:
							if ((date_compare > unit_price.date_start) and (date_compare < unit_price.date_end)):
								if pack_multiple_quantity:
									if unit_price.falabella_offer_price != page_price and page_price is not None:
										price_product = page_price
									else:
										price_product = unit_price.falabella_offer_price

									unit_price = (price_product / product['quantity'])
								else:
									if unit_price.falabella_offer_price != page_price and page_price is not None:
										price_product = page_price
									else:
										price_product = unit_price.falabella_offer_price
									unit_price = price_product
							else:
								if pack_multiple_quantity:
									if unit_price.falabella_price != page_price and page_price is not None:
										price_product = page_price
									else:
										price_product = unit_price.falabella_price

									unit_price = (price_product / product['quantity'])
								else:
									if unit_price.falabella_price != page_price and page_price is not None:
										price_product = page_price
									else:
										price_product = unit_price.falabella_price

									unit_price = price_product
						else:
							# unit_price = unit_price.falabella_price
							if pack_multiple_quantity:
								if unit_price.falabella_price != page_price and page_price is not None:
									price_product = page_price
								else:
									price_product = unit_price.falabella_price

								unit_price = (price_product / product['quantity'])
							else:
								if unit_price.falabella_price != page_price and page_price is not None:
									price_product = page_price
								else:
									price_product = unit_price.falabella_price
								unit_price = price_product

						price_percentage = (unit_price * (product['percentage_price']/100))
						quantity = (int(each['quantity'].replace(" ", "")) * product['quantity'])
						product_code = None
						if Product_Code.objects.filter(product__sku=product['product_pack__sku']).count() == 1:
							product_code = Product_Code.objects.get(product__sku=product['product_pack__sku'])
						else:
							if sku_filter[0].product_ripley is not None:
								if sku_filter[0].code_seven_digits is not None:
									product_code = Product_Code.objects.filter(code_seven_digits=sku_filter[0].code_seven_digits)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								else:
									product_code = None
							else:
								if sku_filter[0].color is not None:
									product_code=Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6,6)).filter(code_seven__gte=50, product__sku=product['product_pack__sku'],color__description=sku_filter[0].color.description)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								else:
									product_code = None
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=product['product_pack__sku']),
							product_code=product_code,
							sale=sale,
							quantity=quantity,
							unit_price=price_percentage,
							total=(quantity * price_percentage),
							comment_unit="[" + str(each['sku'].replace(" ", "")) + "] " + sku_filter[0].description,
						)
						for l in range(quantity):
							if product.get('height', None) is not None:
								if product['height'] > height:
									height = product['height']
							if product.get('length', None) is not None:
								if product['length'] > length:
									length = product['length']
							if product.get('width', None) is not None:
								width += product['width']
							if product.get('weight', None) is not None:
								weight += product['weight']
						# shipping_assigned = 2380 * (sale_product.total * 100/)
						# commission = Commission.objects.create(
						# 	sale_product=sale_product,
						# 	api_commission=0,
						# 	calculate_commission=self.calculate_commission(sale_product),
						# 	discount_commission=0,
						# )

						quantity_stock = Product_Sale.objects.get(sku=product['product_pack__sku']).stock - (quantity)
						Product_Sale.objects.filter(sku=product['product_pack__sku']).update(stock=quantity_stock)
						if product_code is not None:
							quantity_stock_color = product_code.quantity - (quantity)
							Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
							if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not product_code.code_seven_digits in products_stock):
								products_stock.append(product_code.code_seven_digits)
						else:
							if quantity_stock <= 5 and quantity_stock >= 0 and (not (product['product_pack__sku'] in products_stock)):
								products_stock.append(product['product_pack__sku'])

						total += (quantity * price_percentage)
				sale.total_sale = total
				sale.declared_total = total
				sale.comments = comments + " Fecha de Envío: " + row[0]['shipping_date']
				sale.save()
				# Dimensiones paquete
				dimensions_sale = DimensionsSale.objects.create(
					sale=sale,
					height=height,
					weight=weight,
					length=length,
					width=width,
				)

				# Comision Prorrateada
				# for i in sale.sale_of_product.all():
				# 	sale_producto = Sale_Product.objects.get(id=i.id)
				# 	sale_producto.commission_sale_product.shipping_price_assigned = 2380 * ((float(sale_producto.total * 100)/ total) / 100)
				# 	sale_producto.commission_sale_product.save()
		if products_stock:
			self.alert_stock(products_stock)

		return Response(data={"sales": ["Ventas cargadas exitosamente"]}, status=status.HTTP_200_OK)


class ShippingGuide(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request):
		merger = PdfFileMerger()
		buff = BytesIO()
		today = datetime.datetime.today().strftime("%Y-%m-%d")
		address_falabella = self.request.query_params.get('address', None)
		for sale in request.data['sales']:
			if sale['channel'] != 'Falabella':
				return Response(data={"sale": ['Solo se pueden generar guías de despacho para Falabella']}, status=status.HTTP_400_BAD_REQUEST)
		details_guide = []
		index = 1
		total = 0
		iva_acum = 0
		no_round_acum = 0
		no_round_iva = 0
		for sale in request.data['sales']:
			sale = Sale.objects.get(id=sale['id'])
			products_data = Sale_Product.objects.filter(sale__id=sale.id).annotate(NmbItem=F('product__description'), DscItem=Concat(Value("F12/ ", output_field=CharField()), F('sale__order_channel'), output_field=CharField()), QtyItem=F('quantity'), PrcItem=F('unit_price')).values('NmbItem', 'DscItem', 'QtyItem', 'PrcItem', 'product__sku', 'comment_unit')
			for product in products_data:
				data_append = {}
				data_append['NroLinDet'] = index
				data_append['NmbItem'] = product['comment_unit'].split("] ")[1]
				data_append['DscItem'] = product['DscItem'] + " - SKU Falabella: " + product['comment_unit'][product['comment_unit'].find("[")+1:product['comment_unit'].find("]")].split("-")[0]
				data_append['QtyItem'] = product['QtyItem']
				data_append['PrcItem'] = round((float(product['PrcItem']) / 1.19), 3)
				data_append['MontoItem'] = round(data_append['PrcItem'] * product['QtyItem'])
				data_append["CdgItem"] = [
					{
						"TpoCodigo": "INT1",
						"VlrCodigo": str(product['product__sku'])
					}
				]
				details_guide.append(data_append)
				index += 1
				total += data_append['MontoItem']
		iva = round(total * 0.19)
		date_emition = datetime.datetime.strptime(request.data['date_guide'], "%d-%m-%Y").strftime("%Y-%m-%d")
		if address_falabella is not None:
			if address_falabella == 'lo espejo':
				transportation_data = {
					"DirDest":"AV. LO ESPEJO 3200",
					"CmnaDest":"SANTIAGO",
					"CiudadDest":"SANTIAGO"
				}
			else:
				transportation_data = {
					"DirDest":"Camino Interior 5524, Parcelación Santa Filomena, Sectores 1-2-3-4",
					"CmnaDest":"SAN BERNARDO",
					"CiudadDest":"SANTIAGO"
				}
		else:
			transportation_data = {
				"DirDest":"AV. LO ESPEJO 3200",
				"CmnaDest":"SANTIAGO",
				"CiudadDest":"SANTIAGO"
			}
		data_bill = {
				  "response":[
				  "TIMBRE",
				  "PDF"
			   ],
			   "dte":{
			  "Encabezado":{
				 "IdDoc":{
					"TipoDTE":52,
					"Folio":0,
					"FchEmis":date_emition,
					"TipoDespacho":"1",
					"IndTraslado":"3",
					"TpoTranVenta":"1"
				 },
				 "Emisor":DATA_EMITTER_SHIPPING_GUIDE,
				 "Receptor":DATA_RECEIVER,
				  "Transporte":transportation_data,
				 "Totales":{
					"MntNeto":total,
					"TasaIVA":"19",
					"IVA":iva,
					"MntTotal":total + iva,
					"MontoPeriodo":total + iva,
					"VlrPagar":total + iva
				 }
			  },
			  "Detalle": details_guide
			}
		}
		# print(json.dumps(data_bill))
		url_of = OPEN_FACTURA_URL + "/v2/dte/document"
		headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
		response = requests.post(url_of, data=json.dumps(data_bill), headers=headers)
		if response.status_code == 200:
			pdf_file = response.json()["PDF"]
			response_folio = requests.get(OPEN_FACTURA_URL + "/v2/dte/document/" + response.json()['TOKEN'] + "/json", headers=headers).json()
			folio = response_folio['json']['Encabezado']['IdDoc']['Folio']
			for sale in request.data['sales']:
				sale = Sale.objects.get(id=sale['id'])
				sale.comments = sale.comments + " Guía de Despacho: "+ str(folio)
				sale.date_document_emision = datetime.datetime.now().date()
				sale.shipping_guide_number = int(folio)
				sale.status = "entregado"
				sale.save()
			file_name = "Guia de Despacho " + str(folio) + ".pdf"
			with open(os.path.expanduser('~/Documents/Guias Despacho/' + file_name), 'wb') as fout:
				fout.write(base64.decodestring(pdf_file.encode('ascii')))
			file_data = open(os.path.expanduser('~/Documents/Guias Despacho/' + file_name), 'rb')
			merger.append(PdfFileReader(file_data))
			sales_approved = [i['id'] for i in request.data['sales']]
			report = Sales_Report.objects.create(report_file_name=None, user_created=request.user)
			sales_update = Sale.objects.filter(id__in=sales_approved).update(sales_report=report)

			doc = SimpleDocTemplate(buff, pagesize=letter, rightMargin=40, leftMargin=40, topMargin=60, bottomMargin=18,)
			clientes = []
			styles = getSampleStyleSheet()
			products_count_manifiesto = Sale.objects.filter(id__in=sales_approved).aggregate(product_quantity=Sum(Case(When(sale_of_product__product__sku=1, then=Value(0, output_field=IntegerField())), default=F('sale_of_product__quantity'), output_field=IntegerField())))

			header = Paragraph("Listado Productos a Buscar en Bodega (" + str(products_count_manifiesto['product_quantity']) +")", styles['Heading4'])

			style_products = getSampleStyleSheet()
			style_products2 = getSampleStyleSheet()
			# style_products3 = getSampleStyleSheet()
			styleDescription2 = style_products2["BodyText"]
			styleDescription2.fontSize = 10
			styleDescription2.leading = 10
			normal = style_products['Heading4']
			normal.alignment = TA_RIGHT
			normal.bottomMargin = 5
			products_receive = Paragraph("Productos Recibidos: _______________", normal)
			header_data = [
				[header, products_receive],
			]
			table_header = Table(header_data)
			last_manifiesto = Sales_Report.objects.latest('date_created')
			today = datetime.datetime.today().strftime("%Y-%m-%d %H:%M")
			number_manifiesto = Paragraph("Manifiesto #" + str(report.id), styles['Heading2'])
			date_generate = Paragraph("Fecha: " +str(today), styles['Heading2'])
			clientes.append(number_manifiesto)
			clientes.append(date_generate)
			clientes.append(table_header)
			# clientes.append(products_receive)
			headings = ('SKU', 'Producto','Modelo', 'SKU7','Cant.',"             ")
			a = Sale_Product.objects.filter(sale__id__in=sales_approved).exclude(product__sku=1).annotate(model=Case(When(product_code__isnull=False, then=F('product_code__color__description')), default=Value('por definir'), output_field=CharField()), code_seven=Case(When(product_code__isnull=False, then=F('product_code__code_seven_digits')), default=Value('0000000'), output_field=CharField())).values('product__description', 'quantity', 'product_code__color__description', 'product__sku', 'model', 'code_seven')
			sorted_order = sorted(a, key=itemgetter('product__sku','code_seven'))
			group_order = []
			for key, group in itertools.groupby(sorted_order, key=lambda x:(x['product__sku'], x['code_seven'])):
				group_order.append(list(group))
			# print(group_order)
			data = []
			for row in group_order:
				quantity = 0
				for row2 in row:
					quantity += row2['quantity']
				data.append((row[0]['product__sku'], Paragraph(row[0]['product__description'], styleDescription2), Paragraph(row[0]['model'], styleDescription2), row[0]['code_seven'],quantity,"        "))
			allclientes = data
			t = Table([headings] + allclientes, colWidths=["15%","45%","15%",'10%',"5%","10%"])
			t.setStyle(TableStyle(
				[
					('GRID', (0, 0), (5, -1), 1, colors.dodgerblue),
					('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
					('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
				]
			))
			clientes.append(t)
			styleDescription = style_products["BodyText"]
			styleDescription.fontSize = 5
			styleDescription.leading = 5
			ventas = Sale.objects.filter(id__in=sales_approved).annotate(quantity=Sum(Case(When(sale_of_product__product__sku=1, then=Value(0, output_field=IntegerField())), default=F('sale_of_product__quantity'), output_field=IntegerField())), sub_status=F('shipping_type_sub_status'), description=F('comments'))
			header = Paragraph("Listado Ordenes de Compras (" + str(ventas.count()) + ")", styles['Heading4'])
			clientes.append(header)
			index = 0
			headings = ("N°","Canal",'OC', 'Productos', 'SKU productos', "Descripción", "             ", "             ")
			allclientes = []
			for p in ventas:
				index += 1
				products_sku = ""
				for i in p.sale_of_product.all():
					products_sku += str(i.product.sku) + ", "
				allclientes.append((index,p.channel.name, p.order_channel, p.quantity, Paragraph(products_sku, styleDescription2), Paragraph(p.description, styleDescription), "             ", "             "))
			t = Table([headings] + allclientes, colWidths=["4%","15%","15%","10%","20%","16%","10%","10%"])
			t.setStyle(TableStyle(
				[
					('GRID', (0, 0), (7, -1), 1, colors.dodgerblue),
					('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
					('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue),
					# ('FONTSIZE', (5, 1), (5, -1), 8),
				]
			))
			clientes.append(t)
			d = Drawing(300, 100)
			d.add(Line(15, 0, 110, 0))
			e = Drawing(600, 0)
			e.add(Line(145, 0, 245, 0))
			f = Drawing(600, 0)
			f.add(Line(280, 0, 375, 0))
			g = Drawing(600, 0)
			g.add(Line(410, 0, 505, 0))
			clientes.append(d)
			clientes.append(e)
			clientes.append(f)
			clientes.append(g)
			pageTextStyleCenter = ParagraphStyle(name="left", alignment=TA_CENTER, fontSize=13, leading=10)
			tbl_data = [
				[Paragraph("Bodega", pageTextStyleCenter), Paragraph("Supervisor", pageTextStyleCenter), Paragraph("Empaque", pageTextStyleCenter), Paragraph("Despacho", pageTextStyleCenter)],

			]
			tbl = Table(tbl_data)
			clientes.append(tbl)
			doc.build(clientes)
			files_dir = os.getcwd()
			merger.append(PdfFileReader(buff))
			buff.close()
			today = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
			file_manifiesto = "Manifiesto " + str(today)
			path_file = os.path.expanduser('~/Documents/Manifiestos/' + file_manifiesto + '.pdf')
			merger.write(path_file)
			merger.close()
			short_report = open(path_file, 'rb')
			report_encoded = base64.b64encode(short_report.read())
			# report = Sales_Report.objects.create(report_file_name=file_manifiesto, user_created=request.user)
			report.report_file_name = file_manifiesto
			report.save()

			return Response(data={"pdf_file": report_encoded, "folio": str(folio)}, status=status.HTTP_200_OK)
		else:
			print(response.json())
			return Response(data={"sales": ["Hubo un problema al generar la guía de despacho"]}, status=status.HTTP_400_BAD_REQUEST)

class ParisUpload(APIView):
	permission_classes = (IsAuthenticated,)

	def alert_stock(self, products_sku):
		subject = 'Stock minimo'
		from_email = 'info@asiamerica.cl'
		to = ["pablo.guerrero@asiamerica.cl", "manuel.ureta@asiamerica.cl", "cristian.arcos@asiamerica.cl", "girbet.garcia@asiamerica.cl", "jeffer.mujica@asiamerica.cl","abel.mejias@asiamerica.cl", "mariela.rodriguez@asiamerica.cl",]
		# to = ["info@asiamerica.cl",]
		message_append = []
		for each in products_sku:
			if len(str(each)) == 7:
				product_code = Product_Code.objects.filter(code_seven_digits=each)[0]
				message = product_code.product.description + " Variante: " + product_code.color.description + "(" +str(each) + ")"
			else:
				product_sale = Product_Sale.objects.filter(sku=each)[0]
				message = product_sale.description + "(" +str(each) + ")"
			message_append.append(message)
		message = "Buen día atentos por stock menor a 5 de los productos: " + str(message_append).replace("[","").replace("]","") + ". Buen día a todos."
		send_mail(
			subject,
			message,
			'info@asiamerica.cl',
			to,
			fail_silently=False,
		)

	def calculate_commission(self, sale_product):
		percentage_commission = CommissionChannel.objects.filter(product_sale__sku=sale_product.product.sku, channel_id=8)
		if percentage_commission:
			return ((percentage_commission[0].percentage_commission * sale_product.total) / 100)
		else:
			return 0

	@transaction.atomic
	def post(self, request):
		today = datetime.datetime.now().date()
		sorted_order = sorted(request.data, key=itemgetter('order_channel'))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['order_channel']):
			group_order.append(list(group))

		# print(group_order)
		# return Response(data="Todo bien")
		products_stock = []
		for row in group_order:
			order_channel = row[0]['order_channel']
			if Sale.objects.filter(order_channel=order_channel):
				continue
			else:
				# Crear venta
				# print(row[0]['order_channel'])
				try:
					date_sale = datetime.datetime.strptime(row[0]['date_sale'], "%d-%m-%Y")
				except ValueError as e:
					date_sale = datetime.datetime.strptime(row[0]['date_sale'], "%d/%m/%Y")
				# hour_sale = ('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
				sale = Sale.objects.create(
					order_channel=row[0]['order_channel'],
					total_sale=0,
					user=User.objects.get(username='api-paris'),
					channel=Channel.objects.get(name='Paris'),
					document_type='factura',
					payment_type='transferencia retail',
					comments="",
					sender_responsable='vendedor',
					status='pendiente',
					date_sale=date_sale,
					tracking_number=row[0].get('tracking_number', None),
					shipping_date= datetime.datetime.strptime(row[0]['shipping_date'], "%d-%m-%Y"),
					hour_sale_channel=None,
					#cut_inventory="I1",
				)
				total = 0
				comments = ''

				for each in row:
					comments += each['description'] + " - "
					sku_filter = Product_SKU_Ripley.objects.filter(sku_ripley=str(each['sku']))
					if sku_filter:
						if sku_filter[0].pack_ripley is not None:
							products = Pack_Products.objects.filter(pack=sku_filter[0].pack_ripley).values("product_pack__sku", "quantity", "percentage_price")
							# commission_percentage = CommissionChannel.objects.filter(channel_id=8, pack=sku_filter[0].pack_ripley)

						else:
							products = Product_Sale.objects.filter(id=sku_filter[0].product_ripley.id).annotate(product_pack__sku=F('sku'), quantity=Value(1, output_field=IntegerField()), percentage_price=Value(100, output_field=IntegerField())).values('product_pack__sku', 'quantity', 'percentage_price')
							# commission_percentage = CommissionChannel.objects.filter(channel_id=8, product_sale__id=sku_filter[0].product_ripley.id)
					else:
						print(each['sku'])
					# print(products)
					# Busqueda de comisión
					for product in products:
						unit_price = (each['unit_price'] * 1.19) / product['quantity']
						price_percentage = (unit_price * (product['percentage_price']/100))
						quantity = (int(each['quantity']) * product['quantity'])
						product_code = None
						if Product_Code.objects.filter(product__sku=product['product_pack__sku']).count() == 1:
							product_code = Product_Code.objects.get(product__sku=product['product_pack__sku'])
						else:
							if sku_filter[0].product_ripley is not None:
								if sku_filter[0].code_seven_digits is not None:
									product_code = Product_Code.objects.filter(code_seven_digits=sku_filter[0].code_seven_digits)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								else:
									product_code = None
							else:
								if sku_filter[0].color is not None:
									product_code=Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6,6)).filter(code_seven__gte=50, product__sku=product['product_pack__sku'],color__description=sku_filter[0].color.description)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								else:
									product_code = None
						# if sku_filter[0].code_seven_digits == None:
						# 	product_code = None
						# else:
						# 	product_code=Product_Code.objects.get(code_seven_digits=sku_filter[0].code_seven_digits)
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=product['product_pack__sku']),
							product_code=product_code,
							sale=sale,
							quantity=quantity,
							unit_price=price_percentage,
							total=(quantity * price_percentage),
							comment_unit="N° Despacho " + str(row[0]['request_number']) +" - SKU "+ str(each['sku']),
						)
						# commission = Commission.objects.create(
						# 	sale_product=sale_product,
						# 	api_commission=0,
						# 	calculate_commission=self.calculate_commission(sale_product),
						# 	discount_commission=0,
						# )
						quantity_stock = Product_Sale.objects.get(sku=product['product_pack__sku']).stock - quantity
						Product_Sale.objects.filter(sku=product['product_pack__sku']).update(stock=quantity_stock)
						# print(products_stock)
						if product_code is not None:
							quantity_stock_color = product_code.quantity - quantity
							Product_Code.objects.filter(id=product_code.id).update(quantity=quantity_stock_color)
							if quantity_stock_color <= 5 and quantity_stock_color > 0 and (not product_code.code_seven_digits in products_stock):
								products_stock.append(product_code.code_seven_digits)
						else:
							# print(product['product_pack__sku'])
							if quantity_stock <= 5 and quantity_stock >= 0 and (not (product['product_pack__sku'] in products_stock)):
								products_stock.append(product['product_pack__sku'])
						total += (quantity * price_percentage)
				sale.total_sale = total
				sale.declared_total = total
				sale.comments = comments
				sale.save()
		print(products_stock)
		# print(jajaj)
		if products_stock:
			self.alert_stock(products_stock)

		return Response(data={"sales": ["Ventas cargadas exitosamente"]}, status=status.HTTP_200_OK)

class BillParisGenerate(APIView):

	permission_classes = (IsAuthenticated,)
	def post(self, request):
		if not request.user.is_superuser:
			return Response(data={"sales": ["Ud no tiene permisos para realizar esta acción"]}, status=status.HTTP_400_BAD_REQUEST)
		merger = PdfFileMerger()
		buff = BytesIO()
		today_hour = datetime.datetime.today()
		today = datetime.datetime.today().strftime("%Y-%m-%d")
		date_emition = datetime.datetime.strptime(request.data['date_emititon'], "%d-%m-%Y").strftime("%Y-%m-%d")
		for sale in request.data['sales']:
			if sale['channel'] != 'Paris':
				return Response(data={"sale": ['Solo se pueden hacer facturas electrónicas para Paris']}, status=status.HTTP_400_BAD_REQUEST)

		reference_index = 0
		for sale in request.data['sales']:
			details_guide = []
			index = 1
			total = 0
			sale = Sale.objects.get(id=sale['id'])
			references = [{
				"NroLinRef": 1,
	            "TpoDocRef": "801",
	            "FolioRef": str(sale.order_channel)[:10],
	            "FchRef": sale.date_sale.strftime("%Y-%m-%d")
			}]
			products_data = Sale_Product.objects.filter(sale__id=sale.id).annotate(NmbItem=F('product__description'), DscItem=Concat(F('product__description'),Value(' - OC/', output_field=CharField()), F('sale__order_channel'),Value(' - ', output_field=CharField()), F('comment_unit'), Value(' - ', output_field=CharField()), F('product__description'), output_field=CharField()), QtyItem=F('quantity'), PrcItem=F('unit_price')).values('NmbItem', 'DscItem', 'QtyItem', 'PrcItem', 'product__sku')
			for product in products_data:
				data_append = {}
				data_append['NroLinDet'] = index
				data_append['NmbItem'] = product['NmbItem']
				data_append['DscItem'] = product['DscItem']
				data_append['QtyItem'] = product['QtyItem']
				data_append['PrcItem'] = round((float(product['PrcItem']) / 1.19), 3)
				data_append['MontoItem'] = round(data_append['PrcItem'] * product['QtyItem'])
				data_append["CdgItem"] = [
					{
						"TpoCodigo": "INT1",
						"VlrCodigo": str(product['product__sku'])
					}
				]
				details_guide.append(data_append)
				index += 1
				total += data_append['MontoItem']
			iva = round(total * 0.19)
			data_bill = {
					  "response":[
					  "TIMBRE",
					  "PDF"
				   ],
				   "dte":{
				  "Encabezado":{
					 "IdDoc":{
						"TipoDTE":33,
						"Folio":0,
						"FchEmis":date_emition,
						"TpoTranCompra":1,
						"TpoTranVenta":1,
						"FmaPago":2
					 },
					 "Emisor":DATA_EMITTER_BILL,
					 "Receptor":DATA_RECEIVER_BILL,
					 "Totales":{
						"MntNeto":total,
						"TasaIVA":"19",
						"IVA":iva,
						"MntTotal":total + iva,
					 }
				  },
				  "Detalle": details_guide,
				  "Referencia":references
				}
			}
			# print(json.dumps(data_bill))
			url_of = OPEN_FACTURA_URL + "/v2/dte/document"
			headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
			response = requests.post(url_of, data=json.dumps(data_bill), headers=headers)
			if response.status_code == 200:
				# print("Todo bien")
				token = response.json()['TOKEN']
				base_64_pdf = response.json()['PDF']
				response = requests.get(OPEN_FACTURA_URL + "/v2/dte/document/" + response.json()['TOKEN'] + "/json", headers=headers).json()
				sale.number_document = response['json']['Encabezado']['IdDoc']['Folio']
				folio = response['json']['Encabezado']['IdDoc']['Folio']
				sale.token_bill = token
				sale.date_document_emision = datetime.datetime.now().date()
				sale.save()
				file_name = "Factura " + str(folio) + ".pdf"
				with open(os.path.expanduser('~/Documents/Facturas/' + file_name), 'wb') as fout:
					fout.write(base64.decodestring(base_64_pdf.encode('ascii')))
				file_data = open(os.path.expanduser('~/Documents/Facturas/' + file_name), 'rb')
				merger.append(PdfFileReader(file_data))

			else:
				print(response.json())
		buff.close()
		today = datetime.datetime.today().strftime("%Y-%m-%d %H:%M")
		file_manifiesto = "Facturas Paris " + str(today)
		path_file = os.path.expanduser('~/Documents/Facturas Paris/' + file_manifiesto + '.pdf')
		merger.write(path_file)
		merger.close()
		short_report = open(path_file, 'rb')
		report_encoded = base64.b64encode(short_report.read())
		return Response(data={"pdf_file": report_encoded}, status=status.HTTP_200_OK)


class ChannelSaleReport(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		filter_type = self.request.query_params.get('filter_type', 'today')
		# date_for_filter = (datetime.datetime.today() - timedelta(days=15)).date()
		if filter_type == 'today':
			truncate_date = TruncDay('date_sale')
			week_number = ExtractWeek('date_sale')
			month_format = Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField())
			date_format = truncate_date
			year_format = ExtractYear('date_sale')
			date_for_filter = (datetime.datetime.today() - timedelta(days=14)).date()
			sales_by_channel = Sale.objects.filter(date_sale__gte=date_for_filter, status__in=["pendiente", "entregado"]).annotate(date_truncated=TruncDay('date_sale')).values('date_truncated').annotate(sales_total=Count('id'), mercadolibre_total=Sum(Case(When(channel__name='Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), ripley_total=Sum(Case(When(channel__name='Ripley', then=F('total_sale')), default=0, output_field=IntegerField())), falabella_total=Sum(Case(When(channel__name='Falabella', then=F('total_sale')), default=0, output_field=IntegerField())), paris_total=Sum(Case(When(channel__name='Paris', then=F('total_sale')), default=0, output_field=IntegerField())), groupon_total=Sum(Case(When(channel__name='Groupon', then=F('total_sale')), default=0, output_field=IntegerField())), mercadoshops_total=Sum(Case(When(channel__name='Mercadoshop', then=F('total_sale')), default=0, output_field=IntegerField())), yapo_total=Sum(Case(When(channel__name='Yapo', then=F('total_sale')), default=0, output_field=IntegerField())), presencial_total=Sum(Case(When(channel__name='Presencial', then=F('total_sale')), default=0, output_field=IntegerField())), linio_total=Sum(Case(When(channel__name='Linio', then=F('total_sale')), default=0, output_field=IntegerField())), extra_ml_total=Sum(Case(When(channel__name='Extra Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), facebook_total=Sum(Case(When(channel__name='Facebook', then=F('total_sale')), default=0, output_field=IntegerField())),instagram_total=Sum(Case(When(channel__name='Instagram', then=F('total_sale')), default=0, output_field=IntegerField())), total_sales=Sum('total_sale'), week_number=ExtractWeek('date_sale'), month_format=Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField(), ), date_format=TruncDay('date_sale'), year_format=ExtractYear('date_sale'), weekday=ExtractWeekDay('date_sale')).order_by('date_truncated')
			field_name = "date_format"


		if filter_type == 'week':
			truncate_date = TruncWeek('date_sale')
			week_number = ExtractWeek('date_sale')
			month_format = Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField())
			date_format = Value(' - ', output_field=CharField())
			year_format = ExtractYear('date_sale')
			date_for_filter = (datetime.datetime.today() - timedelta(weeks=14)).date()
			sales_by_channel = Sale.objects.filter(date_sale__gte=date_for_filter, status__in=["pendiente", "entregado"]).annotate(date_truncated=TruncWeek('date_sale')).values('date_truncated').annotate(sales_total=Count('id'), mercadolibre_total=Sum(Case(When(channel__name='Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), ripley_total=Sum(Case(When(channel__name='Ripley', then=F('total_sale')), default=0, output_field=IntegerField())), falabella_total=Sum(Case(When(channel__name='Falabella', then=F('total_sale')), default=0, output_field=IntegerField())), paris_total=Sum(Case(When(channel__name='Paris', then=F('total_sale')), default=0, output_field=IntegerField())), groupon_total=Sum(Case(When(channel__name='Groupon', then=F('total_sale')), default=0, output_field=IntegerField())), mercadoshops_total=Sum(Case(When(channel__name='Mercadoshop', then=F('total_sale')), default=0, output_field=IntegerField())), yapo_total=Sum(Case(When(channel__name='Yapo', then=F('total_sale')), default=0, output_field=IntegerField())), presencial_total=Sum(Case(When(channel__name='Presencial', then=F('total_sale')), default=0, output_field=IntegerField())), linio_total=Sum(Case(When(channel__name='Linio', then=F('total_sale')), default=0, output_field=IntegerField())),extra_ml_total=Sum(Case(When(channel__name='Extra Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), facebook_total=Sum(Case(When(channel__name='Facebook', then=F('total_sale')), default=0, output_field=IntegerField())), instagram_total=Sum(Case(When(channel__name='Instagram', then=F('total_sale')), default=0, output_field=IntegerField())),total_sales=Sum('total_sale'), week_number=ExtractWeek('date_sale'), month_format=Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField()), date_format=Value(' - ', output_field=CharField()), week_format=Concat(ExtractWeek('date_sale'),Value(' - '),ExtractYear('date_sale'), output_field=CharField()), year_format=ExtractYear('date_sale'), weekday=Value(' - ', output_field=CharField())).order_by('date_truncated')
			sorted_order = sorted(sales_by_channel, key=itemgetter('week_number'))
			group_order = []
			for key, group in itertools.groupby(sorted_order, key=lambda x:x['week_number']):
				group_order.append(list(group))
			data_sales = []
			for each in group_order:
				if len(each) == 1:
					data_sales.append(each[0])
				else:
					date_truncated_ = each[0]['date_truncated']
					week_number_ = each[0]['week_number']
					date_format_ = each[0]['date_format']
					week_format_ = each[0]['week_format']
					year_format_ = each[0]['year_format']
					weekday_ = each[0]['weekday']
					for d in each:
						del d['date_truncated']
						del d['week_number']
						del d['month_format']
						del d['date_format']
						del d['week_format']
						del d['year_format']
						del d['weekday']
					sum_data = { k: each[0].get(k, 0) + each[1].get(k, 0) for k in set(each[0]) & set(each[1]) }
					sum_data['date_truncated'] = date_truncated_
					sum_data['week_number'] = week_number_
					sum_data['month_format'] = '-'
					sum_data['date_format'] = date_format_
					sum_data['week_format'] = week_format_
					sum_data['year_format'] = year_format_
					sum_data['weekday'] = weekday_
					data_sales.append(sum_data)
			sales_by_channel = data_sales
			# print(data_sales)
			field_name = "week_format"

		if filter_type == 'month':
			truncate_date = TruncMonth('date_sale')
			week_number = Value(' - ', output_field=CharField())
			month_format = Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField())
			date_format = Value(' - ', output_field=CharField())
			year_format = ExtractYear('date_sale')
			date_for_filter = (datetime.datetime.today() - dateutil.relativedelta.relativedelta(months=14)).date()
			sales_by_channel = Sale.objects.filter(date_sale__gte=date_for_filter, status__in=["entregado", "pendiente"]).annotate(date_truncated=TruncMonth('date_sale')).values('date_truncated').annotate(sales_total=Count('id'), mercadolibre_total=Sum(Case(When(channel__name='Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), ripley_total=Sum(Case(When(channel__name='Ripley', then=F('total_sale')), default=0, output_field=IntegerField())), falabella_total=Sum(Case(When(channel__name='Falabella', then=F('total_sale')), default=0, output_field=IntegerField())), paris_total=Sum(Case(When(channel__name='Paris', then=F('total_sale')), default=0, output_field=IntegerField())), groupon_total=Sum(Case(When(channel__name='Groupon', then=F('total_sale')), default=0, output_field=IntegerField())), mercadoshops_total=Sum(Case(When(channel__name='Mercadoshop', then=F('total_sale')), default=0, output_field=IntegerField())), yapo_total=Sum(Case(When(channel__name='Yapo', then=F('total_sale')), default=0, output_field=IntegerField())), presencial_total=Sum(Case(When(channel__name='Presencial', then=F('total_sale')), default=0, output_field=IntegerField())), linio_total=Sum(Case(When(channel__name='Linio', then=F('total_sale')), default=0, output_field=IntegerField())),extra_ml_total=Sum(Case(When(channel__name='Extra Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), facebook_total=Sum(Case(When(channel__name='Facebook', then=F('total_sale')), default=0, output_field=IntegerField())), instagram_total=Sum(Case(When(channel__name='Instagram', then=F('total_sale')), default=0, output_field=IntegerField())),total_sales=Sum('total_sale'), week_number=Value(' - ', output_field=CharField()), month_format=Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField()), date_format=Value(' - ', output_field=CharField()), year_format=ExtractYear('date_sale'), weekday=Value(' - ', output_field=CharField())).order_by('-date_truncated__year', '-date_truncated__month')
			field_name = "month_format"
		if filter_type == 'year':
			truncate_date = TruncYear('date_sale')
			week_number = Value(' - ', output_field=CharField())
			month_format = Value('-', output_field=CharField())
			date_format = Value(' - ', output_field=CharField())
			year_format = ExtractYear('date_sale')
			date_for_filter = (datetime.datetime.today() - dateutil.relativedelta.relativedelta(years=15)).date()
			sales_by_channel = Sale.objects.filter(date_sale__gte=date_for_filter, status__in=["entregado", "pendiente"]).annotate(date_truncated=TruncYear('date_sale')).values('date_truncated').annotate(sales_total=Count('id'), mercadolibre_total=Sum(Case(When(channel__name='Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), ripley_total=Sum(Case(When(channel__name='Ripley', then=F('total_sale')), default=0, output_field=IntegerField())), falabella_total=Sum(Case(When(channel__name='Falabella', then=F('total_sale')), default=0, output_field=IntegerField())), paris_total=Sum(Case(When(channel__name='Paris', then=F('total_sale')), default=0, output_field=IntegerField())), groupon_total=Sum(Case(When(channel__name='Groupon', then=F('total_sale')), default=0, output_field=IntegerField())), mercadoshops_total=Sum(Case(When(channel__name='Mercadoshop', then=F('total_sale')), default=0, output_field=IntegerField())), yapo_total=Sum(Case(When(channel__name='Yapo', then=F('total_sale')), default=0, output_field=IntegerField())), presencial_total=Sum(Case(When(channel__name='Presencial', then=F('total_sale')), default=0, output_field=IntegerField())), linio_total=Sum(Case(When(channel__name='Linio', then=F('total_sale')), default=0, output_field=IntegerField())),extra_ml_total=Sum(Case(When(channel__name='Extra Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), facebook_total=Sum(Case(When(channel__name='Facebook', then=F('total_sale')), default=0, output_field=IntegerField())), instagram_total=Sum(Case(When(channel__name='Instagram', then=F('total_sale')), default=0, output_field=IntegerField())),total_sales=Sum('total_sale'), week_number=Value(' - ', output_field=CharField()), month_format=Value('-', output_field=CharField()), year_format=ExtractYear('date_sale'), date_format=Value(' - ', output_field=CharField()), weekday=Value(' - ', output_field=CharField())).order_by('date_truncated')
			field_name = "year_format"

		channels = [{"name": "Mercadolibre", "key_dict": "mercadolibre_total"}, {"name": "Ripley", "key_dict": "ripley_total"}, {"name": "Falabella", "key_dict": "falabella_total"}, {"name": "Linio", "key_dict": "linio_total"}, {"name": "Paris", "key_dict": "paris_total"}, {"name": "Groupon", "key_dict": "groupon_total"}, {"name": "Mercadoshops", "key_dict": "mercadoshops_total"}, {"name": "Yapo", "key_dict": "yapo_total"}, {"name": "Presencial", "key_dict": "presencial_total"}, {"name": "Extra Mercadolibre", "key_dict": "extra_ml_total"}, {"name": "Facebook", "key_dict": "facebook_total"},{"name": "Instagram", "key_dict": "instagram_total"}]
		data_chart = []
		data_channel = {"series" : []}
		for channel in channels:
			data_channel = {"series" : []}
			data_channel['name'] = channel['name']
			for sale in sales_by_channel:
				if isinstance(sale[field_name], datetime.date):
					data_channel['series'].append({"name": sale[field_name].strftime('%d-%m-%Y'), "value": sale[channel['key_dict']]})
				else:
					# print(type(sale[field_name]))
					data_channel['series'].append({"name": sale[field_name], "value": sale[channel['key_dict']]})
			data_chart.append(data_channel)
		if filter_type == 'month':
			for each in data_chart:
				each['series'] = each['series'][::-1]

		return Response(data={"sales": sales_by_channel, "data_chart":data_chart}, status=status.HTTP_200_OK)


	def write_file(self, ws, row, data, cell_format_data):
		ws.write(row, 0, str(data['day']), cell_format_data)
		ws.write(row, 1, data['week'], cell_format_data)
		ws.write(row, 2, data['month'], cell_format_data)
		ws.write(row, 3, data['year'], cell_format_data)
		ws.write(row, 4, data['Mercadolibre'], cell_format_data)
		ws.write(row, 5, data['Ripley'], cell_format_data)
		ws.write(row, 6, data['Falabella'], cell_format_data)
		ws.write(row, 7, data['Paris'], cell_format_data)
		ws.write(row, 8, data['Groupon'], cell_format_data)
		ws.write(row, 9, data['Mercadoshops'], cell_format_data)
		ws.write(row, 10, data['Yapo'], cell_format_data)
		ws.write(row, 11, data['Presencial'], cell_format_data)
		ws.write(row, 12, data['Linio'], cell_format_data)
		ws.write(row, 13, data['Extra Mercadolibre'], cell_format_data)
		ws.write(row, 14, data['Facebook'], cell_format_data)
		ws.write(row, 15, data['total'], cell_format_data)

	def post(self, request):
		output = io.BytesIO()
		file_name = 'Informe de Ventas Asiamerica por canales'
		wb =  xlsxwriter.Workbook(output)
		date_filter = Sale.objects.annotate(date_truncated=TruncDay('date_sale')).values('date_truncated').annotate(sales_total=Count('id'), mercadolibre_total=Sum(Case(When(channel__name='Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), ripley_total=Sum(Case(When(channel__name='Ripley', then=F('total_sale')), default=0, output_field=IntegerField())), falabella_total=Sum(Case(When(channel__name='Falabella', then=F('total_sale')), default=0, output_field=IntegerField())), paris_total=Sum(Case(When(channel__name='Paris', then=F('total_sale')), default=0, output_field=IntegerField())), groupon_total=Sum(Case(When(channel__name='Groupon', then=F('total_sale')), default=0, output_field=IntegerField())), mercadoshops_total=Sum(Case(When(channel__name='Mercadoshops', then=F('total_sale')), default=0, output_field=IntegerField())), yapo_total=Sum(Case(When(channel__name='Yapo', then=F('total_sale')), default=0, output_field=IntegerField())), presencial_total=Sum(Case(When(channel__name='Presencial', then=F('total_sale')), default=0, output_field=IntegerField())), linio_total=Sum(Case(When(channel__name='Linio', then=F('total_sale')), default=0, output_field=IntegerField())), extra_ml_total=Sum(Case(When(channel__name='Extra Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), facebook_total=Sum(Case(When(channel__name='Facebook', then=F('total_sale')), default=0, output_field=IntegerField())), total_sales=Sum('total_sale'), week_number=ExtractWeek('date_sale'), month_format=Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField()), date_format=TruncDay('date_sale'), year_format=ExtractYear('date_sale')).order_by('date_truncated')
		week_filter = Sale.objects.annotate(date_truncated=TruncWeek('date_sale')).values('date_truncated').annotate(sales_total=Count('id'), mercadolibre_total=Sum(Case(When(channel__name='Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), ripley_total=Sum(Case(When(channel__name='Ripley', then=F('total_sale')), default=0, output_field=IntegerField())), falabella_total=Sum(Case(When(channel__name='Falabella', then=F('total_sale')), default=0, output_field=IntegerField())), paris_total=Sum(Case(When(channel__name='Paris', then=F('total_sale')), default=0, output_field=IntegerField())), groupon_total=Sum(Case(When(channel__name='Groupon', then=F('total_sale')), default=0, output_field=IntegerField())), mercadoshops_total=Sum(Case(When(channel__name='Mercadoshops', then=F('total_sale')), default=0, output_field=IntegerField())), yapo_total=Sum(Case(When(channel__name='Yapo', then=F('total_sale')), default=0, output_field=IntegerField())), presencial_total=Sum(Case(When(channel__name='Presencial', then=F('total_sale')), default=0, output_field=IntegerField())), linio_total=Sum(Case(When(channel__name='Linio', then=F('total_sale')), default=0, output_field=IntegerField())),extra_ml_total=Sum(Case(When(channel__name='Extra Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())),facebook_total=Sum(Case(When(channel__name='Facebook', then=F('total_sale')), default=0, output_field=IntegerField())), total_sales=Sum('total_sale'), week_number=ExtractWeek('date_sale'), month_format=Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField()), date_format=Value(' - ', output_field=CharField()), week_format=Concat(ExtractWeek('date_sale'),Value(' - '),ExtractYear('date_sale'), output_field=CharField()), year_format=ExtractYear('date_sale')).order_by('date_truncated')
		month_filter = Sale.objects.annotate(date_truncated=TruncMonth('date_sale')).values('date_truncated').annotate(sales_total=Count('id'), mercadolibre_total=Sum(Case(When(channel__name='Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), ripley_total=Sum(Case(When(channel__name='Ripley', then=F('total_sale')), default=0, output_field=IntegerField())), falabella_total=Sum(Case(When(channel__name='Falabella', then=F('total_sale')), default=0, output_field=IntegerField())), paris_total=Sum(Case(When(channel__name='Paris', then=F('total_sale')), default=0, output_field=IntegerField())), groupon_total=Sum(Case(When(channel__name='Groupon', then=F('total_sale')), default=0, output_field=IntegerField())), mercadoshops_total=Sum(Case(When(channel__name='Mercadoshops', then=F('total_sale')), default=0, output_field=IntegerField())), yapo_total=Sum(Case(When(channel__name='Yapo', then=F('total_sale')), default=0, output_field=IntegerField())), presencial_total=Sum(Case(When(channel__name='Presencial', then=F('total_sale')), default=0, output_field=IntegerField())), linio_total=Sum(Case(When(channel__name='Linio', then=F('total_sale')), default=0, output_field=IntegerField())),extra_ml_total=Sum(Case(When(channel__name='Extra Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), facebook_total=Sum(Case(When(channel__name='Facebook', then=F('total_sale')), default=0, output_field=IntegerField())), total_sales=Sum('total_sale'), week_number=Value(' - ', output_field=CharField()), month_format=Concat(ExtractYear('date_sale'),Value(' - '),ExtractMonth('date_sale'), output_field=CharField()), date_format=Value(' - ', output_field=CharField()), year_format=ExtractYear('date_sale')).order_by('date_truncated')
		year_filter = Sale.objects.annotate(date_truncated=TruncYear('date_sale')).values('date_truncated').annotate(sales_total=Count('id'), mercadolibre_total=Sum(Case(When(channel__name='Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), ripley_total=Sum(Case(When(channel__name='Ripley', then=F('total_sale')), default=0, output_field=IntegerField())), falabella_total=Sum(Case(When(channel__name='Falabella', then=F('total_sale')), default=0, output_field=IntegerField())), paris_total=Sum(Case(When(channel__name='Paris', then=F('total_sale')), default=0, output_field=IntegerField())), groupon_total=Sum(Case(When(channel__name='Groupon', then=F('total_sale')), default=0, output_field=IntegerField())), mercadoshops_total=Sum(Case(When(channel__name='Mercadoshops', then=F('total_sale')), default=0, output_field=IntegerField())), yapo_total=Sum(Case(When(channel__name='Yapo', then=F('total_sale')), default=0, output_field=IntegerField())), presencial_total=Sum(Case(When(channel__name='Presencial', then=F('total_sale')), default=0, output_field=IntegerField())), linio_total=Sum(Case(When(channel__name='Linio', then=F('total_sale')), default=0, output_field=IntegerField())),extra_ml_total=Sum(Case(When(channel__name='Extra Mercadolibre', then=F('total_sale')), default=0, output_field=IntegerField())), facebook_total=Sum(Case(When(channel__name='Facebook', then=F('total_sale')), default=0, output_field=IntegerField())), total_sales=Sum('total_sale'), week_number=Value(' - ', output_field=CharField()), month_format=Value('-', output_field=CharField()), year_format=ExtractYear('date_sale'), date_format=Value(' - ', output_field=CharField())).order_by('date_truncated')
		work_sheets = [{"name":'Por día', "rows": date_filter},{"name":'Por semana', "rows": week_filter}, {"name":'Por mes', "rows": month_filter}, {"name": "Por año", "rows": year_filter}]
		columns = [{"name": 'Día', "width":12, "rotate": True},{"name": 'Semana', "width":12, "rotate": True}, {"name": 'Mes', "width": 9, "rotate": False},{"name": 'Año', "width": 9, "rotate": False}, {"name": 'Mercadolibre', "width": 15, "rotate": True}, {"name": 'Ripley', "width": 11, "rotate": True}, {"name": 'Falabella', "width": 15, "rotate": True}, {"name": 'Paris', "width": 10, "rotate": True}, {"name": 'Groupon', "width": 10, "rotate": True}, {"name": 'Mercadoshops', "width": 15, "rotate": True}, {"name": 'Yapo', "width": 15, "rotate": True}, {"name": 'Presencial', "width": 15, "rotate": True}, {"name": 'Linio', "width": 15, "rotate": True}, {"name": 'Extra Mercadolibre', "width": 15, "rotate": True}, {"name": 'Facebook', "width": 15, "rotate": True}, {"name": 'Total', "width": 15, "rotate": True}]
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields_90.set_center_across()
		cell_format_fields = wb.add_format({'bold': True, 'bg_color': 'yellow', 'border': 1, 'font_size': 10})
		cell_format_fields.set_center_across()
		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for sheet in work_sheets:
			ws = wb.add_worksheet(sheet['name'])
			row_num = 0
			for col_num in range(len(columns)):
				if columns[col_num]['rotate']:
					ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
					ws.set_column(col_num, col_num, columns[col_num]['width'])
				else:
					ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
					ws.set_column(col_num, col_num, columns[col_num]['width'])
			rows = sheet['rows']
			row_num += 1
			for row in rows:
				data_write = {
					"day":row['date_format'],
					"week":row['week_number'],
					"month":row['month_format'],
					"year":row['year_format'],
					"Mercadolibre": row['mercadolibre_total'],
					"Ripley": row['ripley_total'],
					"Falabella":row['falabella_total'],
					"Paris":row['paris_total'],
					"Groupon":row['groupon_total'],
					"Mercadoshops":row['mercadoshops_total'],
					"Yapo":row['yapo_total'],
					"Presencial":row['presencial_total'],
					"Linio":row['linio_total'],
					"Extra Mercadolibre":row['extra_ml_total'],
					"Facebook":row['facebook_total'],
					"total":row['total_sales'],
				}
				self.write_file(ws, row_num, data_write, cell_format_data)
				row_num += 1
			ws.autofilter(0, 0, 0, len(columns) - 1)

		wb.close()
		output.seek(0)
		filename = 'Reporte de Ventas por Canales.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		# print("--- %s seconds ---" % (time.time() - start_time))
		return response

class ExportParisEmmitedOrdersXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def post(self, request):
		start_time = time.time()
		today = date.today()
		output = io.BytesIO()
		file_name = 'Listado de Emitidas' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		ws.freeze_panes(1, 0)

		columns = [{"name": 'N° Orden', "width":10, "rotate": False}, {"name": 'N° Solicitud', "width": 12, "rotate": False}, {"name": 'SKU paris', "width": 10, "rotate": False}, {"name": 'Descripción', "width": 40, "rotate": False}, {"name": 'Solicitado', "width": 10, "rotate": False}, {"name": 'Precio Costo', "width": 13, "rotate": False}, {"name": 'Factura', "width": 10, "rotate": False}]

		row_num = 0
		cell_format_fields = wb.add_format({'bold': True, 'font_size': 10, 'bg_color': 'gray', 'border': 1})
		cell_format_fields.set_center_across()

		cell_format_data = wb.add_format({'font_size': 8, 'border': 1})
		cell_format_data.set_text_wrap()
		cell_format_data.set_align('vcenter')
		cell_format_fields_90 = wb.add_format({'bold': True, 'rotation': 90, 'font_size': 10, 'bg_color': 'gray', 'border': 1})
		cell_format_fields_90.set_center_across()

		cell_format_text_wrap = wb.add_format({'font_size': 8, 'border': 1})
		cell_format_text_wrap.set_text_wrap()
		cell_format_text_wrap.set_align('top')

		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields_90)
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
			ws.set_column(col_num, col_num, columns[col_num]['width'])
		row_num += 1
		for row in request.data['sales']:
			sale = Sale.objects.filter(order_channel=row['order_channel'])
			if sale:
				ws.write(row_num, 0, row['order_channel'], cell_format_data)
				ws.write(row_num, 1, row['tracking_number'], cell_format_data)
				ws.write(row_num, 2, row['sku'], cell_format_data)
				ws.write(row_num, 3, row['description'], cell_format_data)
				ws.write(row_num, 4, row['quantity'], cell_format_data)
				ws.write(row_num, 5, row['unit_price'], cell_format_data)
				ws.write(row_num, 6, sale[0].number_document, cell_format_data)
			else:
				ws.write(row_num, 0, row['order_channel'], cell_format_data)
				ws.write(row_num, 1, row['tracking_number'], cell_format_data)
				ws.write(row_num, 2, row['sku'], cell_format_data)
				ws.write(row_num, 3, row['description'], cell_format_data)
				ws.write(row_num, 4, row['quantity'], cell_format_data)
				ws.write(row_num, 5, row['unit_price'], cell_format_data)
				ws.write(row_num, 6, "1233456", cell_format_data)
			row_num += 1
		wb.close()
		output.seek(0)
		filename = 'Listado de Emitidas.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		# print("--- %s seconds ---" % (time.time() - start_time))
		return response

class AutomaticRetailSalesMercadoshopXLSViewSet(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request):
		# print(request.data)
		date_from = datetime.datetime.strptime(request.data['date_from'], '%d-%m-%Y')
		date_to = datetime.datetime.strptime(request.data['date_to'], '%d-%m-%Y')
		token = Token.objects.get(id=1).token_ml
		url_mshop = "https://api.mercadoshops.com/v1/shops/216244489/orders/search?access_token="+ token +"&channel=mshops"
		data = requests.get(url_mshop).json()
		if data['paging']['total'] <= 50:
			rows = data["results"]
		else:
			rows = []
			count = 0
			iteration_end = int(data['paging']['total']/50)
			module = int(data['paging']['total']%50)
			for i in range(iteration_end + 1):
				new_rows = requests.get(url_mshop + "&offset=" + str(i*50)).json()['results']
				rows = rows + new_rows
			rows = rows + requests.get(url_mshop + "&offset="+str(((iteration_end*50) + module))).json()['results']
		# print(len(rows))
		for row in rows:
			if Sale.objects.filter(order_channel=row['id'], channel__name="Mercadoshop"):
				continue
			else:
				date_creation =  datetime.datetime.strptime(row['creation_date'], "%Y-%m-%dT%H:%M:%S.000-04:00")
				if date_creation.date() > date_to.date() or date_creation.date() < date_from.date():
					continue
				# print(row['products'][0]['sku'])
				date_sale = datetime.datetime.strptime(row['creation_date'], "%Y-%m-%dT%H:%M:%S.000-04:00")
				hour_sale_channel=('%s:%s' % (date_sale.hour if date_sale.hour >= 10 else ('0' + str(date_sale.hour)), date_sale.minute if date_sale.minute >= 10 else ('0' + str(date_sale.minute))))
				sale = Sale.objects.create(
					order_channel=row['id'],
					total_sale=0,
					declared_total=0,
					revenue=0,
					user=User.objects.get(username='api-mercadoshop'),
					channel=Channel.objects.get(name="Mercadoshop"),
					document_type="sdt",
					payment_type='transferencia retail',
					sender_responsable="vendedor",
					status="pendiente",
					tracking_number=None,
					date_sale=date_sale,
					hour_sale_channel=hour_sale_channel,
					last_user_update=User.objects.get(username='api-mercadoshop'),
					shipping_type="por acordar",
					shipping_type_sub_status='pendiente',
				)
				comments = ""
				total = 0
				revenue = 0
				total_sale_no_shipping = row['grand_total']
				for row2 in row['products']:
					sku_filter = Product_SKU_Ripley.objects.filter(sku_ripley=row2['sku'])
					if sku_filter:
						if sku_filter[0].pack_ripley is not None:
							products = Pack_Products.objects.filter(pack=sku_filter[0].pack_ripley).values("product_pack__sku", "quantity", "percentage_price")
						else:
							products = Product_Sale.objects.filter(id=sku_filter[0].product_ripley.id).annotate(product_pack__sku=F('sku'), quantity=Value(1, output_field=IntegerField()), percentage_price=Value(100, output_field=IntegerField())).values('product_pack__sku', 'quantity', 'percentage_price')
					else:
						products = []
					comments += row2['title'] + " - "
					for product in products:
						# product_code = None
						unit_price = ((row2['unit_price'] * (product['percentage_price']/100))/(row2['quantity'] * product['quantity'])) * (row2['quantity'])
						if (unit_price * row2['quantity'] * product['quantity']) > total_sale_no_shipping:
							unit_price = total_sale_no_shipping/(row2['quantity'] * product['quantity'])
						else:
							if not unit_price.is_integer():
								unit_price = self.roundup(unit_price)
								total_sale_no_shipping -= (unit_price * row2['quantity'] * product['quantity'])
						if Product_Code.objects.filter(product__sku=product['product_pack__sku']).count() == 1:
							product_code = Product_Code.objects.get(product__sku=product['product_pack__sku'])
						else:
							if sku_filter[0].product_ripley is not None:
								if sku_filter[0].code_seven_digits is not None:
									product_code = Product_Code.objects.filter(code_seven_digits=sku_filter[0].code_seven_digits)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								else:
									product_code = None
							else:
								if sku_filter[0].color is not None:
									product_code=Product_Code.objects.annotate(code_seven=Substr('code_seven_digits', 6,6)).filter(code_seven__gte=50, product__sku=product['product_pack__sku'],color__description=sku_filter[0].color.description)
									if product_code:
										product_code = product_code[0]
									else:
										product_code = None
								else:
									product_code = None
						sale_product = Sale_Product.objects.create(
							product=Product_Sale.objects.get(sku=product['product_pack__sku']),
							product_code=product_code,
							sale=sale,
							quantity=row2['quantity'] * product['quantity'],
							unit_price=unit_price,
							total=(row2['quantity'] * product['quantity'] * unit_price),
							comment_unit=row2['title'],
						)
						total += (row2['quantity'] * product['quantity'] * unit_price)
				sale.total_sale = total
				sale.declared_total = total
				sale.comments = comments
				sale.save()
		return Response(data={"sales": ['Ventas de Mercadoshop actualizadas exitosamente']}, status=status.HTTP_200_OK)

class Product_SKU_RipleyViewSet(ModelViewSet):
	queryset = Product_SKU_Ripley.objects.all().order_by('channel__id')
	permission_classes = (IsAuthenticated,)
	serializer_class = Product_SKU_RipleySerializer
	http_method_names = ['get', 'post',]
	pagination_class = CustomPagination
	filter_backends = (filters.DjangoFilterBackend, search_filter.SearchFilter,)
	filter_fields = ('channel', )
	search_fields  = ('sku_ripley', 'product_ripley__sku', 'pack_ripley__sku_pack', 'description')


	@transaction.atomic
	def create(self, request):
		token = Token.objects.get(id=1).token_ml
		url_mercadolibre = "https://api.mercadolibre.com/users/216244489/items/search?status=active&access_token="+ token
		rows_mercadolibre = requests.get(url_mercadolibre).json()
		if rows_mercadolibre['paging']['total'] <= 50:
			rows = rows_mercadolibre["results"]
		else:
			rows = []
			count = 0
			iteration_end = int(rows_mercadolibre['paging']['total']/50)
			module = int(rows_mercadolibre['paging']['total']%50)
			for i in range(iteration_end + 1):
				new_rows = requests.get(url_mercadolibre + "&offset=" + str(i*50)).json()['results']
				rows = rows + new_rows
			rows = rows + requests.get(url_mercadolibre + "&offset="+str(((iteration_end*50) + module))).json()['results']

		for row in rows:
			if Product_SKU_Ripley.objects.filter(code=row):
				continue
			else:
				#Crear nuevo sku y asignarle todos sus atributos
				product_data = requests.get("https://api.mercadolibre.com/items/" + row + "?access_token=" + token).json()
				# print(product_data['seller_custom_field'])
				if product_data['seller_custom_field'] is not None:
					pack = Pack.objects.filter(
						sku_pack=str(product_data['seller_custom_field']),
					)
					if pack:
						pack = pack[0]
						product = None
					else:
						product = Product_Sale.objects.filter(sku=int(product_data['seller_custom_field']))
						if product:
							product = product[0]
							pack = None
					if pack or product:
						sku_new = Product_SKU_Ripley.objects.create(
							pack_ripley=pack,
							product_ripley=product,
							description=product_data['title'],
							code_seven_digits=None,
							sku_ripley=str(product_data['seller_custom_field']),
							code=row,
							channel=Channel.objects.get(name="Mercadolibre"),
						)
					else:
						pass
						# print("El sku no se encuentra registrado " + str(product_data['seller_custom_field']))
				else:
					print("Esta publicación no se posee SKU " + str(row))
		return Response(data={"sku": ["SKU actualizados satisfactoriamente"]}, status=status.HTTP_200_OK)

class ChargeSKUList(APIView):
	permission_classes = (IsAuthenticated,)
	@transaction.atomic
	def post(self, request):
		# print(request.data)
		sorted_order = sorted(request.data, key=itemgetter('sku'))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['sku']):
			group_order.append(list(group))
		# print(group_order)
		for row in group_order:
			# print(row[0])
			product_sale = Product_Sale.objects.filter(sku=row[0]['sku'])
			if product_sale:
				product_sale_description = product_sale[0].description
				product_sale.update(
					description=row[0].get('description', product_sale_description),
					height=row[0].get('height', None),
					width=row[0].get('width', None),
					length=row[0].get('length', None),
					weight=row[0].get('weight', None),
				)
				product_sale = product_sale[0]
				product_sale2 = Product_Sale.objects.filter(id=product_sale.id).update(height=int(row[0]['height']),width=int(row[0]['width']),length=int(row[0]['length']),weight=int(row[0]['weight']))
			else:
				category_product_sale = Category_Product_Sale.objects.filter(sku_start=str(row[0]['sku'])[:2])
				if category_product_sale:
					category = category_product_sale[0]
				else:
					category = None
				product_sale = Product_Sale.objects.create(
					sku=row[0]['sku'],
					description=row[0]['description'],
					stock=99999,
					height=row[0].get('height', None),
					width=row[0].get('width', None),
					length=row[0].get('length', None),
					weight=row[0].get('weight', None),
					category_product_sale=category,
				)
			for each in row:
			# print(each)
				product_code = Product_Code.objects.filter(code_seven_digits=each['code_seven_digits'])
				if product_code:
					continue
				else:
					color = Color.objects.filter(description=each['variation'])
					if color:
						color = color[0]
					else:
						color = Color.objects.create(
							description=each['variation']
						)
					product_code = Product_Code.objects.create(
						code_seven_digits=str(each['code_seven_digits']),
						product=product_sale,
						color=color,
					)
		return Response(data={"sku": ['Lista cargada exitosamente']}, status=status.HTTP_200_OK)

class ChargeSKUPack(APIView):
	permission_classes = (IsAuthenticated,)
	@transaction.atomic
	def post(self, request):
		# print(request.data)
		#Validar que sku de productos existan
		for product in request.data:
			if Product_Sale.objects.filter(sku=product['sku_product']).count() == 0:
				return Response(data={"sku": ['El sku '+ str(product['sku_product'])+" no se encuentra registrado, por favor registrelo previamente de forma individual o masiva"]}, status=status.HTTP_400_BAD_REQUEST)
		sorted_order = sorted(request.data, key=itemgetter('sku'))
		group_order = []

		for key, group in itertools.groupby(sorted_order, key=lambda x:x['sku']):
			group_order.append(list(group))

		for row in group_order:
			pack = Pack.objects.filter(sku_pack=row[0]['sku'])
			if pack:
				pack.update(
					height=row[0].get('height', None),
					width=row[0].get('width', None),
					length=row[0].get('length', None),
					weight=row[0].get('weight', None),
				)
				# Actualizar productos?
			else:
				pack = Pack.objects.create(
					sku_pack=row[0]['sku'],
					description=row[0]['description'],
					height=row[0].get('height', None),
					width=row[0].get('width', None),
					length=row[0].get('length', None),
					weight=row[0].get('weight', None),
				)
				for each in row:
					product_pack = Pack_Products.objects.create(
						pack=pack,
						product_pack=Product_Sale.objects.get(sku=each['sku_product']),
						quantity=each['quantity'],
						percentage_price=each['percentage_price'],
					)
		return Response(data={"sku": ['Lista cargada exitosamente']}, status=status.HTTP_200_OK)


class CancelBillOpenFactura(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request):
		if not request.user.is_superuser:
			return Response(data={"sales": ["Ud no tiene permisos para realizar esta acción"]}, status=status.HTTP_400_BAD_REQUEST)
		# print(request.data)
		sale = Sale.objects.get(id=request.data['sale'])
		if sale.number_document == None and sale.status in ["pendiente", "entregado"]:
			sale.status = "cancelado"
			sale.save()
			return Response(data={"sales": ["Venta cancelada exitosamente"]}, status=status.HTTP_200_OK)
		date_emition = datetime.datetime.now().strftime("%Y-%m-%d")
		if request.data['observation'] == 'reprint':
			observation = "Reimpresión"
		else:
			observation = "Cancelación"
		headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
		data_json = requests.get(OPEN_FACTURA_URL + "/v2/dte/document/" + sale.token_bill + "/json", headers=headers).json()['json']
		# print(data_json)
		iva = round(int(data_json['Encabezado']['Totales']['MntTotal']) - (int(data_json['Encabezado']['Totales']['MntTotal']) / 1.19))
		neto = round(int(data_json['Encabezado']['Totales']['MntTotal']) / 1.19)
		# print(iva)
		# print(neto)
		data_bill = {
			  "response":[
				  "TIMBRE",
				  "PDF"
		   ],
		   "dte":{
		   	"Encabezado":{
				 "IdDoc":{
					"TipoDTE":61,
					"Folio":data_json['Encabezado']['IdDoc']['Folio'],
					"FchEmis":date_emition,
					"IndServicio": data_json['Encabezado']['IdDoc']['IndServicio'],
					"MntBruto": 1
				 },
				 "Emisor":{
				 	"Acteco": 479100,
					"CmnaOrigen": data_json['Encabezado']['Emisor']['CmnaOrigen'],
					"DirOrigen": data_json['Encabezado']['Emisor']['DirOrigen'],
					"GiroEmis": data_json['Encabezado']['Emisor']['GiroEmisor'],
					"RUTEmisor": data_json['Encabezado']['Emisor']['RUTEmisor'],
					"RznSoc": data_json['Encabezado']['Emisor']['RznSocEmisor']
				 },
				 "Receptor":data_json['Encabezado']['Receptor'],
				 "Totales":{
				 	"IVA": str(iva),
					"MntExe": "0",
					"MntNeto": str(neto),
					"MntTotal": str(neto + iva),
					"MontoNF": "0",
					"MontoPeriodo": str(neto + iva),
					"TasaIVA": "19",
					"VlrPagar": str(neto + iva),
				 }
			  },
			  "Detalle": data_json['Detalle'],
			  "Referencia": [{
	                "NroLinRef": 1,
	                "TpoDocRef": "39",
	                "FolioRef": str(sale.number_document),
	                "FchRef": date_emition,
	                "RazonRef": observation,
	                "CodRef": "1"
	            }]
			}
		}
		# print(json.dumps(data_bill))
		url_of = OPEN_FACTURA_URL + "/v2/dte/document"
		response = requests.post(url_of, data=json.dumps(data_bill), headers=headers)
		if response.status_code == 200:
			response = requests.get(OPEN_FACTURA_URL + "/v2/dte/document/" + response.json()['TOKEN'] + "/json", headers=headers).json()
			sale.credit_note_number = response['json']['Encabezado']['IdDoc']['Folio']
			if observation == "Reimpresión":
				sale.status = "pendiente"
				sale.document_type = "sdt"
				sale.number_document = None
				sale.bill = False
			else:
				sale.status = "cancelado"
			sale.save()
		else:
			print(response.json())
			return Response(data={"sale": ["Hubo un error al intentar anular el documento"]}, status=status.HTTP_400_BAD_REQUEST)

		return Response(data={"sale": ["Documento Anulado exitosamente"]}, status=status.HTTP_200_OK)


class ExportPublicationsXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def get(self, request):
		output = io.BytesIO()
		file_name = 'Publicaciones'
		token = Token.objects.get(id=1).token_ml
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("Hoja1")
		cell_format_data = wb.add_format({'bg_color': "red", "font_size": 8})
		cell_format_data1 = wb.add_format({'bg_color': "#0070c0", 'color': "white", "font_size": 8})
		cell_format_data2 = wb.add_format({'bg_color': "yellow", "font_size": 8})
		cell_format_data3 = wb.add_format({'color': "red", "font_size": 8})
		cell_format_data4 = wb.add_format({'bg_color': "#d9d9d9", "font_size": 8})
		cell_format_data5 = wb.add_format({"font_size": 8})
		cell_format_data6 = wb.add_format({"font_size": 8, 'num_format': '#,###', 'color': "red"})
		columns = [
			{"name": 'MLC', "width": 11, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Título', "width": 25, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Id Categoría', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Precio', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Precio Base', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Precio Original', "width": 7, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Cantidad Inicial', "width": 7, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Cantidad Disponible', "width": 12, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Cantidad Vendida', "width": 10, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Tipo de Publicación', "width": 10, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Permalink', "width": 14, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Video', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": '¿Acepta MercadoPago?', "width": 10, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Estado', "width": 8, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Tags', "width": 17, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Garantía', "width": 13, "rotate": True, "format_data":cell_format_data1},
			{"name": '¿Envío Gratis?', "width": 13, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Costo de Envío', "width": 10, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Variante', "width": 17, "rotate": True, "format_data":cell_format_data1},


			{"name": 'SKU', "width": 13, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Caso', "width": 13, "rotate": True, "format_data":cell_format_data2},
			{"name": 'SKU 7 Ml', "width": 13, "rotate": True, "format_data":cell_format_data2},
			{"name": 'SKU 5 Ml', "width": 13, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Variante Ml', "width": 13, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Título Base', "width": 25, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Variante Base', "width": 16, "rotate": True, "format_data":cell_format_data2},
		]

		url_ml = "https://api.mercadolibre.com/users/216244489/items/search?search_type=scan&limit=100&access_token=" + token
		data = requests.get(url_ml).json()
		if data['paging']['total'] <= 100:
			rows = data["results"]
		else:
			rows = []
			count = 0
			iteration_end = int(data['paging']['total']/100)
			module = int(data['paging']['total']%100)
			for i in range(iteration_end + 1):
				if i > 0:
					new_rows = requests.get(url_ml + "&scroll_id=" + scrol_id).json()
					scrol_id = new_rows['scroll_id']
					new_rows = new_rows['results']
				else:
					new_rows = requests.get(url_ml).json()
					scrol_id = new_rows['scroll_id']
					new_rows = new_rows['results']
				rows = rows + new_rows
			rows = rows + requests.get(url_ml + "&scroll_id=" + scrol_id).json()['results']
		row_num = 0
		for col_num in range(len(columns)):
			ws.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
			ws.set_column(col_num, col_num, columns[col_num]['width'])
		row_num += 1

		sales_base = []
		url_ml = "https://api.mercadolibre.com/orders/search?seller=216244489&access_token=" + token + "&order.date_created.from=2019-01-01T16:00:00.000-00:00&order.date_created.to=2019-06-25T23:00:00.000-00:00&sort=date_asc"
		data = requests.get(url_ml).json()
		if data['paging']['total'] <= 50:
			sales_base = data["results"]
		else:
			sales_base = []
			count = 0
			iteration_end = int(data['paging']['total']/50)
			module = int(data['paging']['total']%50)
			for i in range(iteration_end + 1):
				sales_base = requests.get(url_ml + "&offset=" + str(i*50)).json()['results']
				sales_base = sales_base + sales_base
			sales_base = sales_base + requests.get(url_ml + "&offset="+str(((iteration_end*50) + module))).json()['results']

		sales_base.reverse()
		for row in rows:
			# print(row)
			data_complete = requests.get("https://api.mercadolibre.com/items/" + row + "?access_token=" + token + "").json()
			if data_complete['variations']:
				if data_complete['shipping']['free_shipping']:
					list_cost = requests.get("https://api.mercadolibre.com/items/" + row + "/shipping_options/free").json().get('coverage', None)
					if list_cost is not None:
						list_cost = list_cost['all_country']['list_cost']
					else:
						list_cost = 0
				else:
					list_cost = "-"
				for e in data_complete['variations']:
					ws.write(row_num, 0, data_complete['id'],cell_format_data5)
					ws.write(row_num, 1, data_complete['title'],cell_format_data5)
					ws.write(row_num, 2, data_complete['category_id'],cell_format_data5)
					ws.write(row_num, 3, data_complete['price'],cell_format_data6)
					ws.write(row_num, 4, data_complete['base_price'],cell_format_data6)
					ws.write(row_num, 5, data_complete['original_price'],cell_format_data6)
					ws.write(row_num, 6, data_complete['initial_quantity'],cell_format_data5)
					ws.write(row_num, 7, data_complete['available_quantity'],cell_format_data5)
					ws.write(row_num, 8, data_complete['sold_quantity'],cell_format_data5)
					ws.write(row_num, 9, data_complete['listing_type_id'],cell_format_data5)
					ws.write(row_num, 10, data_complete['permalink'],cell_format_data5)
					ws.write(row_num, 11, data_complete['video_id'],cell_format_data5)
					ws.write(row_num, 12, data_complete['accepts_mercadopago'],cell_format_data5)
					ws.write(row_num, 13, data_complete['status'],cell_format_data5)
					ws.write(row_num, 14, str(data_complete['tags']),cell_format_data5)
					ws.write(row_num, 15, data_complete['warranty'],cell_format_data5)
					ws.write(row_num, 16, data_complete['shipping']['free_shipping'],cell_format_data5)
					ws.write(row_num, 18, e['attribute_combinations'][0]['value_name'],cell_format_data5)
					ws.write(row_num, 17, list_cost,cell_format_data5)
					variation_split = e['attribute_combinations'][0]['value_name'].split("-")
					# Busqueda por value_name
					if len(variation_split[0].replace(" ", "")) == 7 and (not "(" in variation_split[0]) and len(variation_split) > 1 and variation_split[0].isdigit():
						sku_7 = variation_split[0].replace(" ", "")
						sku_5 = variation_split[0][:5]
						variation = variation_split[1].lstrip()
						product_sale = Product_Sale.objects.filter(sku=sku_5)
						if product_sale:
							title_base = product_sale[0].description
							product_code = Product_Code.objects.filter(code_seven_digits=sku_7)
							if product_code:
								variation_base = product_code[0].color.description
							else:
								variation_base = "-"
						else:
							pack = Pack.objects.filter(sku_pack=sku_5)
							if pack:
								title_base = pack[0].description
								if pack[0].pack_product.all().count() == 1:
									sku_7_pack = str(pack[0].pack_product.all()[0].product_pack.sku) + sku_7[5:7]
									variation_base_code = Product_Code.objects.filter(code_seven_digits=sku_7_pack)
									if variation_base_code:
										variation_base = variation_base_code[0].color.description
									else:
										variation_base = "-"
								else:
									variation_base = "-"
							else:
								title_base = "-"
								variation_base = "-"
						# ws.write(row_num, 21, sku_7)
						# ws.write(row_num, 22, sku_5)
						# ws.write(row_num, 23, variation)
						# ws.write(row_num, 24, title_base)
						# ws.write(row_num, 25, variation_base)
					# Busqueda por seller_custom_field
					elif data_complete['seller_custom_field'] is not None and (not "(" in e['attribute_combinations'][0]['value_name']):
						sku_7 = data_complete['seller_custom_field'] + e['attribute_combinations'][0]['value_name'][:2]
						sku_5 = sku_7[:5]
						# print(e['attribute_combinations'][0]['value_name'])
						try:
							variation = e['attribute_combinations'][0]['value_name'].split("-")[1].lstrip()
						except IndexError as e:
							variation = "-"
						product_sale = Product_Sale.objects.filter(sku=sku_5)
						if product_sale:
							title_base = product_sale[0].description
							product_code = Product_Code.objects.filter(code_seven_digits=sku_7)
							if product_code:
								variation_base = product_code[0].color.description
							else:
								variation_base = "-"
						else:
							pack = Pack.objects.filter(sku_pack=sku_5)
							if pack:
								title_base = pack[0].description
								if pack[0].pack_product.all().count() == 1 and variation != "-":
									sku_7_pack = str(pack[0].pack_product.all()[0].product_pack.sku) + e['attribute_combinations'][0]['value_name'][:2]
									variation_base_code = Product_Code.objects.filter(code_seven_digits=sku_7_pack)
									if variation_base_code:
										variation_base = variation_base_code[0].color.description
									else:
										variation_base = "-"
								else:
									variation_base = "-"
							else:
								title_base = "-"
								variation_base = "-"
						# ws.write(row_num, 21, sku_7)
						# ws.write(row_num, 22, sku_5)
						# ws.write(row_num, 23, variation)
						# ws.write(row_num, 24, title_base)
						# ws.write(row_num, 25, variation_base)
					elif "(" in e['attribute_combinations'][0]['value_name'] and data_complete['seller_custom_field'] is not None:
						sku_7 = "Sku No Obtenido"
						variation = e['attribute_combinations'][0]['value_name'].split(" (")[0]
						sku_5 = data_complete['seller_custom_field']
						pack = Pack.objects.filter(sku_pack=sku_5)
						if pack:
							title_base = pack[0].description
							variation_base = "-"
						else:
							title_base = "-"
							variation_base = "-"
						# ws.write(row_num, 21, sku_7)
						# ws.write(row_num, 22, sku_5)
						# ws.write(row_num, 23, variation)
						# ws.write(row_num, 24, title_base)
						# ws.write(row_num, 25, variation_base)
					else:
						# Busqueda por variantes previas
						sku_5 = ""
						for r in data_complete['variations']:
							sku_split = r['attribute_combinations'][0]['value_name'].split(" - ")
							if len(sku_split[0]) > 4:
								sku_5 = sku_split[0][:5]
								break
						# for t in sales_base:
						# 	if t['order_items'][0]['item']['id'] == row['id']:
						# 		sku_5 = t['order_items'][0]['item']['seller_custom_field'][:5] if t['order_items'][0]['item']['seller_custom_field'] is not None else t['order_items'][0]['item']['seller_sku'][:5]
						# 		break
						if sku_5 != "" and (not "(" in e['attribute_combinations'][0]['value_name']) and sku_5.isdigit():
							sku_7 = sku_5 + e['attribute_combinations'][0]['value_name'][:2]
							try:
								variation = e['attribute_combinations'][0]['value_name'].split("-")[1].lstrip()
							except IndexError as e:
								variation = "-"
							product_sale = Product_Sale.objects.filter(sku=sku_5)
							if product_sale:
								title_base = product_sale[0].description
								product_code = Product_Code.objects.filter(code_seven_digits=sku_7)
								if product_code:
									variation_base = product_code[0].color.description
								else:
									variation_base = "-"
							else:
								pack = Pack.objects.filter(sku_pack=sku_5)
								if pack:
									title_base = pack[0].description
									if pack[0].pack_product.all().count() == 1 and variation != "-":
										sku_7_pack = str(pack[0].pack_product.all()[0].product_pack.sku) + e['attribute_combinations'][0]['value_name'][:2]
										variation_base_code = Product_Code.objects.filter(code_seven_digits=sku_7_pack)
										if variation_base_code:
											variation_base = variation_base_code[0].color.description
										else:
											variation_base = "-"
									else:
										variation_base = "-"
								else:
									title_base = "-"
									variation_base = "-"
						elif (not "(" in e['attribute_combinations'][0]['value_name']):
							# Busqueda por ventas
							sku_5 = ""
							for t in sales_base:
								if t['order_items'][0]['item']['id'] == row:
									sku_5 = t['order_items'][0]['item']['seller_custom_field'][:5] if t['order_items'][0]['item']['seller_custom_field'] is not None else t['order_items'][0]['item']['seller_sku'][:5]
									break
							if sku_5 != "" and sku_5.isdigit():
								sku_7 = sku_5 + e['attribute_combinations'][0]['value_name'][:2]
								try:
									variation = e['attribute_combinations'][0]['value_name'].split("-")[1].lstrip()
								except IndexError as e:
									variation = "-"
								product_sale = Product_Sale.objects.filter(sku=sku_5)
								if product_sale:
									title_base = product_sale[0].description
									product_code = Product_Code.objects.filter(code_seven_digits=sku_7)
									if product_code:
										variation_base = product_code[0].color.description
									else:
										variation_base = "-"
								else:
									pack = Pack.objects.filter(sku_pack=sku_5)
									if pack:
										title_base = pack[0].description
										if pack[0].pack_product.all().count() == 1 and variation != "-":
											sku_7_pack = str(pack[0].pack_product.all()[0].product_pack.sku) + e['attribute_combinations'][0]['value_name'][:2]
											variation_base_code = Product_Code.objects.filter(code_seven_digits=sku_7_pack)
											if variation_base_code:
												variation_base = variation_base_code[0].color.description
											else:
												variation_base = "-"
										else:
											variation_base = "-"
									else:
										title_base = "-"
										variation_base = "-"
							else:
								sku_7 = "Sku No Obtenido"
								variation = "-"
								title_base = "-"
								variation_base = "-"
						else:
							sku_7 = "Sku No Obtenido"
							variation = "-"
							title_base = "-"
							variation_base = "-"
					if sku_7 == "Sku No Obtenido":
						ws.write(row_num, 21, sku_7, cell_format_data)
					else:
						ws.write(row_num, 21, sku_7, cell_format_data4)
					ws.write(row_num, 22, sku_5, cell_format_data4)
					ws.write(row_num, 23, variation, cell_format_data4)
					ws.write(row_num, 24, title_base, cell_format_data4)
					ws.write(row_num, 25, variation_base, cell_format_data4)

					row_num += 1
			else:
				ws.write(row_num, 0, data_complete['id'],cell_format_data5)
				ws.write(row_num, 1, data_complete['title'],cell_format_data5)
				ws.write(row_num, 2, data_complete['category_id'],cell_format_data5)
				ws.write(row_num, 3, data_complete['price'], cell_format_data6)
				ws.write(row_num, 4, data_complete['base_price'], cell_format_data6)
				ws.write(row_num, 5, data_complete['original_price'], cell_format_data6)
				ws.write(row_num, 6, data_complete['initial_quantity'],cell_format_data5)
				ws.write(row_num, 7, data_complete['available_quantity'],cell_format_data5)
				ws.write(row_num, 8, data_complete['sold_quantity'],cell_format_data5)
				ws.write(row_num, 9, data_complete['listing_type_id'],cell_format_data5)
				ws.write(row_num, 10, data_complete['permalink'],cell_format_data5)
				ws.write(row_num, 11, data_complete['video_id'],cell_format_data5)
				ws.write(row_num, 12, data_complete['accepts_mercadopago'],cell_format_data5)
				ws.write(row_num, 13, data_complete['status'],cell_format_data5)
				ws.write(row_num, 14, str(data_complete['tags']),cell_format_data5)
				ws.write(row_num, 15, data_complete['warranty'],cell_format_data5)
				ws.write(row_num, 16, data_complete['shipping']['free_shipping'],cell_format_data5)
				ws.write(row_num, 18, "-")
				if data_complete['shipping']['free_shipping']:
					list_cost = requests.get("https://api.mercadolibre.com/items/" + row + "/shipping_options/free").json().get('coverage', None)
					if list_cost is not None:
						list_cost = list_cost['all_country']['list_cost']
					else:
						list_cost = 0
					ws.write(row_num, 17, list_cost)
				else:
					ws.write(row_num, 17, "-")
				if data_complete['attributes']:
					sku_data = None
					for e in data_complete['attributes']:
						if e['id'] == "SELLER_SKU":
							sku_data = e['value_name']
							break
					if sku_data is not None:
						sku_data = sku_data
					else:
						sku_data = "-"
				else:
					if data_complete['seller_custom_field'] is not None:
						sku_data = data_complete['seller_custom_field']
					else:
						sku_data = "-"
				ws.write(row_num, 19, sku_data)
				if sku_data != '-' and len(sku_data) > 4:
					sku_7 = sku_data
					sku_5 = sku_7[:5]
					try:
						variation = e['attribute_combinations'][0]['value_name'].split("-")[1].lstrip()
					except (IndexError, KeyError) as e:
						variation = "-"
					product_sale = Product_Sale.objects.filter(sku=sku_5)
					if product_sale:
						title_base = product_sale[0].description
						product_code = Product_Code.objects.filter(code_seven_digits=sku_7)
						if product_code:
							variation_base = product_code[0].color.description
						else:
							variation_base = "-"
					else:
						pack = Pack.objects.filter(sku_pack=sku_5)
						if pack:
							title_base = pack[0].description
							if pack[0].pack_product.all().count() == 1 and variation != "-":
								sku_7_pack = str(pack[0].pack_product.all()[0].product_pack.sku) + e['attribute_combinations'][0]['value_name'][:2]
								variation_base_code = Product_Code.objects.filter(code_seven_digits=sku_7_pack)
								if variation_base_code:
									variation_base = variation_base_code[0].color.description
								else:
									variation_base = "-"
							else:
								variation_base = "-"
						else:
							title_base = "-"
							variation_base = "-"
					# ws.write(row_num, 21, sku_7)
					# ws.write(row_num, 22, sku_5)
					# ws.write(row_num, 23, variation)
					# ws.write(row_num, 24, title_base)
					# ws.write(row_num, 25, variation_base)
				else:
					sku_7 = "Sku No Obtenido"
					sku_5 = "-"
					variation = "-"
					title_base = "-"
					variation_base = "-"
				if sku_7 == "Sku No Obtenido":
					ws.write(row_num, 21, sku_7, cell_format_data)
				else:
					ws.write(row_num, 21, sku_7, cell_format_data4)
				ws.write(row_num, 22, sku_5, cell_format_data4)
				ws.write(row_num, 23, variation, cell_format_data4)
				ws.write(row_num, 24, title_base, cell_format_data4)
				ws.write(row_num, 25, variation_base, cell_format_data4)

				# if sku_data.startswith('1'):
				# 	ws.write(row_num, 20, "Caso 1")
				# elif sku_data.startswith('5'):
				# 	ws.write(row_num, 20, "Caso 3")
				# elif sku_data == "-":
				# 	ws.write(row_num, 20, "-")
				# else:
				# 	ws.write(row_num, 20, "Caso 2")
				row_num += 1
		wb.close()
		output.seek(0)
		filename = 'Publicaciones Mercadolibre.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response


class ExportPublicationsRipleyXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def get(self, request):
		output = io.BytesIO()
		file_name = 'Publicaciones Ripley'
		token = Token.objects.get(id=1).token_ml
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("Hoja1")
		cell_format_data = wb.add_format({'bg_color': "red", "font_size": 8})
		cell_format_data1 = wb.add_format({'bg_color': "#0070c0", 'color': "white", "font_size": 8})
		cell_format_data2 = wb.add_format({'bg_color': "yellow", "font_size": 8})
		cell_format_data3 = wb.add_format({'color': "red", "font_size": 8})
		cell_format_data4 = wb.add_format({'bg_color': "#d9d9d9", "font_size": 8})
		cell_format_data5 = wb.add_format({"font_size": 8})
		cell_format_data6 = wb.add_format({"font_size": 8, 'num_format': '#,###', 'color': "red"})
		columns = [
			{"name": 'Id de Oferta', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": 'SKU Ripley', "width": 13, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Título', "width": 25, "rotate": True, "format_data":cell_format_data1},
			{"name": 'SKU Seller', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Categoría', "width": 15, "rotate": True, "format_data":cell_format_data1},
			{"name": '¿Activa?', "width": 13, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Precio', "width": 7, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Precio de Origen', "width": 12, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Stock', "width": 8, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Mínimo precio envío', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Precio Total', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Clase Logistica', "width": 13, "rotate": True, "format_data":cell_format_data1},


			{"name": 'SKU5 Base Asiamerica', "width": 10, "rotate": True, "format_data":cell_format_data2},
			{"name": 'SKU7 Base Asiamerica', "width": 13, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Título Base', "width": 25, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Variante Base', "width": 12, "rotate": True, "format_data":cell_format_data2},
			{"name": '¿En base?', "width": 9, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Código Categoría', "width": 9, "rotate": True, "format_data":cell_format_data2},
		]
		url_ml = "https://ripley-prod.mirakl.net/api/offers?max=100"
		headers = {'Accept': 'application/json', 'Authorization': API_KEY_RP}
		data = requests.get(url_ml, headers=headers).json()
		if data['total_count'] <= 100:
			rows = data["offers"]
		else:
			rows = []
			count = 0
			iteration_end = int(data['total_count']/100)
			module = int(data['total_count']%100)
			for i in range(iteration_end + 1):
				new_rows = requests.get(url_ml + "&offset=" + str(i*100), headers=headers).json()['offers']
				rows = rows + new_rows
			rows = rows + requests.get(url_ml + "&offset="+str(((iteration_end*100) + module)), headers=headers).json()['offers']
		row_num = 0
		for col_num in range(len(columns)):
			ws.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
			ws.set_column(col_num, col_num, columns[col_num]['width'])
		row_num += 1

		for row in rows:
			ws.write(row_num, 0, row['offer_id'], cell_format_data5)
			ws.write(row_num, 1, row['product_sku'], cell_format_data5)
			ws.write(row_num, 2, row['product_title'], cell_format_data3)
			ws.write(row_num, 3, row['shop_sku'], cell_format_data3)
			ws.write(row_num, 4, row['category_label'], cell_format_data5)
			ws.write(row_num, 5, row['active'], cell_format_data5)
			ws.write(row_num, 6, row['price'], cell_format_data6)
			ws.write(row_num, 7, row['all_prices'][0]['unit_origin_price'], cell_format_data6)
			ws.write(row_num, 8, row['quantity'], cell_format_data3)
			ws.write(row_num, 9, row['min_shipping_price'], cell_format_data6)
			ws.write(row_num, 10, row['total_price'], cell_format_data5)
			ws.write(row_num, 11, row['logistic_class']['label'], cell_format_data5)
			ws.write(row_num, 17, row['category_code'], cell_format_data5)
			sku_ripley = Product_SKU_Ripley.objects.filter(sku_ripley=row['shop_sku'])
			if sku_ripley:
				# Se hace la busqueda por Pack y se asocian los datos
				if sku_ripley[0].pack_ripley.pack_product.all().count() == 1:
					pack_product_data = sku_ripley[0].pack_ripley.pack_product.all()[0]
					product_sale = pack_product_data.product_pack
					sku_5 = product_sale.sku
					title_base = product_sale.description
					if pack_product_data.color is not None:
						product_code = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits',6,6)).filter(product=product_sale, color=pack_product_data.color, code_seven__gt=49)
						if product_code:
							sku_7 = product_code[0].code_seven_digits
							variation_base = product_code[0].color.description
						else:
							sku_7 = "Sku No Obtenido"
							variation_base = '-'
					elif sku_ripley[0].color is not None:
						product_code = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits',6,6)).filter(product=product_sale, color=sku_ripley[0].color, code_seven__gt=49)
						if product_code:
							sku_7 = product_code[0].code_seven_digits
							variation_base = product_code[0].color.description
						else:
							sku_7 = "Sku No Obtenido"
							variation_base = '-'
					else:
						sku_7 = "Sku No Obtenido"
						variation_base = '-'
				else:
					sku_5 = "-"
					sku_7 = "Sku No Obtenido"
					title_base = "-"
					variation_base = '-'

				base_ = "Si"
			else:
				if row['shop_sku'].isdigit():
					product_sale = Product_Sale.objects.filter(sku=row['shop_sku'][:5])
					if product_sale:
						sku_5 = row['shop_sku'][:5]
						title_base = product_sale[0].description
						if len(row['shop_sku']) >= 7:
							product_code = Product_Code.objects.filter(code_seven_digits=row['shop_sku'][:7])
							if product_code:
								sku_7 = product_code[0].code_seven_digits
								variation_base = product_code[0].color.description
							else:
								sku_7 = "Sku No Obtenido"
								variation_base = '-'
						else:
							sku_7 = "Sku No Obtenido"
							variation_base = '-'
					else:
						sku_7 = "Sku No Obtenido"
						sku_5 = "-"
						title_base = "-"
						variation_base = "-"
				else:
					sku_7 = "Sku No Obtenido"
					sku_5 = "-"
					title_base = "-"
					variation_base = "-"
				base_ = "No"
			if sku_7 == "Sku No Obtenido":
				ws.write(row_num, 13, sku_7, cell_format_data)
			else:
				ws.write(row_num, 13, sku_7, cell_format_data4)

			ws.write(row_num, 12, sku_5, cell_format_data4)
			ws.write(row_num, 14, title_base, cell_format_data4)
			ws.write(row_num, 15, variation_base, cell_format_data4)
			ws.write(row_num, 16, base_, cell_format_data4)
			row_num += 1

		ws.autofilter(0, 0, 0, len(columns) - 1)
		ws.freeze_panes(1, 0)
		wb.close()
		output.seek(0)
		filename = 'Publicaciones Mercadolibre.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response

class ExportPublicationsLinioXLSViewSet(APIView):
	permission_classes = (AllowAny,)

	def get(self, request):
		output = io.BytesIO()
		file_name = 'Publicaciones Linio'
		token = Token.objects.get(id=1).token_ml
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("Hoja1")
		cell_format_data = wb.add_format({'bg_color': "red", "font_size": 8})
		cell_format_data1 = wb.add_format({'bg_color': "#0070c0", 'color': "white", "font_size": 8})
		cell_format_data2 = wb.add_format({'bg_color': "yellow", "font_size": 8})
		cell_format_data3 = wb.add_format({'color': "red", "font_size": 8})
		cell_format_data4 = wb.add_format({'bg_color': "#d9d9d9", "font_size": 8})
		cell_format_data5 = wb.add_format({"font_size": 8})
		cell_format_data6 = wb.add_format({"font_size": 8, 'num_format': '#,###'})
		columns = [
			{"name": 'Id Producto', "width": 11, "rotate": True, "format_data":cell_format_data1},
			{"name": 'SKU Linio', "width": 19, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Título', "width": 30, "rotate": True, "format_data":cell_format_data1},
			{"name": 'SKU Seller', "width": 9, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Categoría Primaria', "width": 13, "rotate": True, "format_data":cell_format_data1},
			{"name": '¿Activa?', "width": 7, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Precio', "width": 8, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Precio de Venta', "width": 8, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Stock', "width": 8, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Variante', "width": 12, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Marca', "width": 11, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Medidas del Producto', "width": 13, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Garantía del Producto', "width": 14, "rotate": True, "format_data":cell_format_data1},
			{"name": 'Pais de Producción', "width": 11, "rotate": True, "format_data":cell_format_data1},


			{"name": 'SKU5 Base Asiamerica', "width": 10, "rotate": True, "format_data":cell_format_data2},
			{"name": 'SKU7 Base Asiamerica', "width": 10, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Título Base', "width": 28, "rotate": True, "format_data":cell_format_data2},
			{"name": 'Variante Base', "width": 12, "rotate": True, "format_data":cell_format_data2},
			{"name": '¿En base?', "width": 9, "rotate": True, "format_data":cell_format_data2},
		]
		row_num = 0
		for col_num in range(len(columns)):
			ws.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
			ws.set_column(col_num, col_num, columns[col_num]['width'])
		row_num += 1

		url_linio = "https://sellercenter-api.linio.cl?"

		parameters = {
		  'UserID': USER_ID,
		  'Version': '1.0',
		  'Action': 'GetProducts',
		  'Filter': 'all',
		  'Format':'JSON',
		  'Timestamp':datetime.datetime.now().isoformat(),
		}
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))
		parameters['Signature'] = HMAC(API_KEY_LINIO.encode('ascii'), concatenated.encode('utf-8'), sha256).hexdigest()
		concatenated = urllib.parse.urlencode(sorted(parameters.items()))

		url_request = url_linio + concatenated
		data = requests.get(url_request).json()
		rows = data["SuccessResponse"]["Body"]["Products"]["Product"]
		# print(len(rows))
		for row in rows:
			ws.write(row_num, 0, row['ProductId'], cell_format_data5)
			ws.write(row_num, 1, row['ShopSku'], cell_format_data5)
			ws.write(row_num, 2, row['Name'], cell_format_data3)
			ws.write(row_num, 3, row['SellerSku'], cell_format_data3)
			ws.write(row_num, 4, row['PrimaryCategory'], cell_format_data5)
			ws.write(row_num, 5, row['Status'], cell_format_data5)
			ws.write(row_num, 6, int(row['Price'].replace(".00", "")), cell_format_data6)
			if row['SalePrice'] == "":
				ws.write(row_num, 7, row['SalePrice'], cell_format_data3)
			else:
				ws.write(row_num, 7, int(row['SalePrice'].replace(".00", "")), cell_format_data6)
			ws.write(row_num, 8, row['Quantity'], cell_format_data3)
			ws.write(row_num, 9, row['Variation'], cell_format_data5)
			ws.write(row_num, 10, row['Brand'], cell_format_data5)
			ws.write(row_num, 11, row['ProductData'].get('ProductMeasures', ""), cell_format_data5)
			ws.write(row_num, 12, row['ProductData'].get('ProductWarranty', ""), cell_format_data5)
			ws.write(row_num, 13, row['ProductData'].get('ProductionCountry', ""), cell_format_data5)
			sku_ripley = Product_SKU_Ripley.objects.filter(sku_ripley=row['ShopSku'])
			if sku_ripley:
				# Se hace la busqueda por Pack y se asocian los datos
				if sku_ripley[0].pack_ripley.pack_product.all().count() == 1:
					pack_product_data = sku_ripley[0].pack_ripley.pack_product.all()[0]
					product_sale = pack_product_data.product_pack
					sku_5 = product_sale.sku
					title_base = product_sale.description
					if pack_product_data.color is not None:
						product_code = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits',6,6)).filter(product=product_sale, color=pack_product_data.color, code_seven__gt=49)
						if product_code:
							sku_7 = product_code[0].code_seven_digits
							variation_base = product_code[0].color.description
						else:
							sku_7 = "Sku No Obtenido"
							variation_base = '-'
					elif sku_ripley[0].color is not None:
						product_code = Product_Code.objects.annotate(code_seven=Substr('code_seven_digits',6,6)).filter(product=product_sale, color=sku_ripley[0].color, code_seven__gt=49)
						if product_code:
							sku_7 = product_code[0].code_seven_digits
							variation_base = product_code[0].color.description
						else:
							sku_7 = "Sku No Obtenido"
							variation_base = '-'
					else:
						sku_7 = "Sku No Obtenido"
						variation_base = '-'
				else:
					sku_5 = "-"
					sku_7 = "Sku No Obtenido"
					title_base = "-"
					variation_base = '-'

				base_ = "Si"
			else:
				if row['SellerSku'].isdigit():
					product_sale = Product_Sale.objects.filter(sku=row['SellerSku'][:5])
					if product_sale:
						sku_5 = row['SellerSku'][:5]
						title_base = product_sale[0].description
						if len(row['SellerSku']) >= 7:
							product_code = Product_Code.objects.filter(code_seven_digits=row['SellerSku'][:7])
							if product_code:
								sku_7 = product_code[0].code_seven_digits
								variation_base = product_code[0].color.description
							else:
								sku_7 = "Sku No Obtenido"
								variation_base = '-'
						else:
							sku_7 = "Sku No Obtenido"
							variation_base = '-'
					else:
						sku_7 = "Sku No Obtenido"
						sku_5 = "-"
						title_base = "-"
						variation_base = "-"
				else:
					sku_7 = "Sku No Obtenido"
					sku_5 = "-"
					title_base = "-"
					variation_base = "-"
				base_ = "No"
			if sku_7 == "Sku No Obtenido":
				ws.write(row_num, 15, sku_7, cell_format_data)
			else:
				ws.write(row_num, 15, sku_7, cell_format_data4)
			ws.write(row_num, 14, sku_5, cell_format_data4)
			ws.write(row_num, 16, title_base, cell_format_data4)
			ws.write(row_num, 17, variation_base, cell_format_data4)
			ws.write(row_num, 18, base_, cell_format_data4)
			row_num += 1


		ws.autofilter(0, 0, 0, len(columns) - 1)
		ws.freeze_panes(1, 0)
		wb.close()
		output.seek(0)
		filename = 'Publicaciones Mercadolibre.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response

class LastReportSales(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		path_folder = os.path.expanduser('~/Documents/Reports/')
		onlyfiles = [f for f in listdir(path_folder) if isfile(join(path_folder, f))]
		latest = datetime.datetime(1,1,1,0,0)
		lastest_file = ""
		for each in onlyfiles:
			string_replace = each.replace("Reporte de Ventas ", "").replace(".xlsx", "")
			date_file = datetime.datetime.strptime(string_replace, "%Y%m%d%H%M")
			if date_file > latest:
				latest = date_file
				lastest_file = each
		if self.request.query_params.get('file_name', None) is not None:
			return Response(data={"filename": lastest_file.replace(".xlsx", "")}, status=status.HTTP_200_OK)

		response = HttpResponse(open(path_folder + lastest_file, 'rb').read())
		response['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		response['Content-Disposition'] = 'attachment; filename=' + lastest_file
		return response
