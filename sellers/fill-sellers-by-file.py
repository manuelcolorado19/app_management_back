import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from categories.models import Channel
import asyncio
import sys
# import resource
from aiohttp import ClientSession
import json
from sellers.models import Seller
wb = load_workbook('sellers/Sellers.xlsm')
ws = wb['Hoja1']
all_sellers = []
sellers_ids = []
count = 0
for row in ws.rows:
	count += 1
	if count > 1:
		sellers_ids.append(str(row[0].value))

def nth_repl(s, sub, repl, nth):
    find = s.find(sub)
    i = find != -1
    while find != -1 and i != nth:
        find = s.find(sub, find + 1)
        i += 1
    if i == nth:
        return s[:find]+repl+s[find + len(sub):]
    return s

async def run(session, number_of_requests, concurrent_limit):
    base_url = "https://api.mercadolibre.com/users/{}"
    second_childs = []
    tasks = []
    responses = []
    sem = asyncio.Semaphore(concurrent_limit)

    async def fetch(i):
        url = base_url.format(i)
        async with session.get(url) as response:
            response = await response.read()
            sem.release()
            responses.append(response)
            data_json = json.loads(response)
            print(data_json['nickname'])
            seller = Seller(
            	id_ml=i,
            	nickname=data_json['nickname'],
				registration_date=dt.strptime(nth_repl(data_json['registration_date'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z") ,
				city=data_json['address']['city'],
				user_type=data_json['user_type'],
				points=data_json['points'],
				seller_url=data_json['permalink'],
				level_ml=data_json['seller_reputation']['level_id'],
				power_seller_status=data_json['seller_reputation']['power_seller_status'],
				complete_sales=data_json['seller_reputation']['transactions']['completed'],
				canceled_sales=data_json['seller_reputation']['transactions']['canceled'],
				total_sales=data_json['seller_reputation']['transactions']['total'],
				negative_percent=data_json['seller_reputation']['transactions']['ratings']['negative'],
				neutral_percent=data_json['seller_reputation']['transactions']['ratings']['neutral'],
				positive_percent=data_json['seller_reputation']['transactions']['ratings']['positive'],
				user_status=data_json['status']['site_status'],
            )
            all_sellers.append(seller)
            return response

    for i in sellers_ids:
        await sem.acquire()
        task = asyncio.ensure_future(fetch(i))
        task.add_done_callback(tasks.remove)
        tasks.append(task)

    await asyncio.wait(tasks)
    print("total_responses: {}".format(len(responses)))
    print("total_sellers_add: {}".format(len(all_sellers)))
    Seller.objects.bulk_create(all_sellers)


async def main(number_of_requests, concurrent_limit):
    async with ClientSession() as session:
        responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
        return

loop = asyncio.get_event_loop()
loop.run_until_complete(main(10000, 10000))
loop.close()