from django.core.management.base import BaseCommand, CommandError
from products.models import Product
import os
from time import time
import argparse
import asyncio
import sys
from aiohttp import ClientSession
import json
from datetime import datetime as dt
from sellers.models import Seller
from products.models import Product
from categories.models import Channel
from datetime import timedelta
import time as ti_me
from notifications.models import Token
all_products = Product.objects.filter(channel__id=1, seller__isnull=True).values_list('seller_id_ml', flat=True).distinct()
token = Token.objects.get(id=1).token_ml
start_time = time()
sellers_bulk = []

def nth_repl(s, sub, repl, nth):
    find = s.find(sub)
    # if find is not p1 we have found at least one match for the substring
    i = find != -1
    # loop util we find the nth or we find no match
    while find != -1 and i != nth:
        # find + 1 means we start at the last match start index + 1
        find = s.find(sub, find + 1)
        i += 1
    # if i  is equal to nth we found nth matches so replace
    if i == nth:
        return s[:find]+repl+s[find + len(sub):]
    return s

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		print(len(all_products))
		if all_products:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()
		# print(len(repeated_remove))
async def run(session, number_of_requests, concurrent_limit):
	base_url = "https://api.mercadolibre.com/users/{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		url = base_url.format(i)
		async with session.get(url) as response:
			response = await response.read()
			sem.release()
			responses.append(response)
			data_json = json.loads(response)
			# Se añade el seller a la base
			seller = Seller.objects.filter(id_ml=i)
			print(data_json['id'])
			if seller:
				pass
			else:
				try:
					seller = Seller(
						id_ml=data_json['id'],
						nickname=data_json['nickname'],
						registration_date=dt.strptime(nth_repl(data_json['registration_date'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z") ,
						city=data_json['address']['city'],
						user_type=data_json['user_type'],
						points=data_json['points'],
						seller_url=data_json['permalink'],
						level_ml=data_json['seller_reputation']['level_id'],
						power_seller_status=data_json['seller_reputation']['power_seller_status'],
						complete_sales=data_json['seller_reputation']['transactions']['completed'],
						canceled_sales=data_json['seller_reputation']['transactions']['canceled'],
						total_sales=data_json['seller_reputation']['transactions']['total'],
						negative_percent=data_json['seller_reputation']['transactions']['ratings']['negative'],
						neutral_percent=data_json['seller_reputation']['transactions']['ratings']['neutral'],
						positive_percent=data_json['seller_reputation']['transactions']['ratings']['positive'],
						user_status=data_json['status']['site_status'],
					)
					sellers_bulk.append(seller)

				except Exception as e:
					pass
					# print(e)
			
			return response

	for i in all_products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	print("total_sellers_append: {}".format(len(sellers_bulk)))
	Seller.objects.bulk_create(sellers_bulk)
	print("--- %s seconds ---" % (time() - start_time))

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return

