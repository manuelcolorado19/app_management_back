from django.core.management.base import BaseCommand, CommandError
from products.models import Product
import os
from time import time
import argparse
import asyncio
import sys
from aiohttp import ClientSession
import json
from datetime import datetime as dt
from sellers.models import Seller
from products.models import Product
from categories.models import Channel
from datetime import timedelta
import time as ti_me
from notifications.models import Token
all_products = Product.objects.filter(channel__id=1, seller__isnull=True)[:10000]
start_time = time()

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		print(len(all_products))
		if all_products:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()
		# print(len(repeated_remove))
async def run(session, number_of_requests, concurrent_limit):
	# base_url = "https://api.mercadolibre.com/users/{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		seller = Seller.objects.filter(id_ml=i.seller_id_ml)
		if seller:
			print(i.code)
			a = Product.objects.filter(id=i.id).update(seller_id=seller[0].id,)
		else:
			pass
		# url = base_url.format(i)
		

	for i in all_products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	# print("total_sellers_append: {}".format(len(sellers_bulk)))
	# Seller.objects.bulk_create(sellers_bulk)
	print("--- %s seconds ---" % (time() - start_time))

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return

