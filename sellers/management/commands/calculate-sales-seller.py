from products.models import Product
import os
from time import time
import argparse
import asyncio
import sys
# import resource
from aiohttp import ClientSession
import json
from datetime import datetime as dt
from sellers.models import Seller, SellerVariation
from products.models import Product, ProductVariation
from categories.models import Channel
from django.core.management.base import BaseCommand, CommandError
from datetime import timedelta
from django.db.models import Prefetch, Sum, F, Func, Aggregate, FloatField, Case, When, Avg, IntegerField, Count, Q
all_seller = Seller.objects.all().order_by('id')
start_time = time()

class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		# print(len(all_products))
		if all_seller:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()

async def run(session, number_of_requests, concurrent_limit):
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		# print(i.id)
		data_variations = i.seller_variation.all().values('sales_last_4_month','id').order_by('date_search')
		for each in range(1, data_variations.count()):
			# print(each)
			if data_variations[each - 1]:
				yesterday = data_variations[each - 1]['sales_last_4_month']
				today = data_variations[each]['sales_last_4_month']
				if today is not None and yesterday is not None:
					diference = today - yesterday
					if diference < 0:
						diference = 0
					else:
						diference = diference
				else:
					diference = None
				SellerVariation.objects.filter(id=data_variations[each]['id']).update(daily_sales=diference)
				# print(str(diference) + " venta calculada")
		print("Ventas calculada")

	for i in all_seller:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	print("total_products_append: {}".format(len(all_seller)))
	print("--- %s seconds ---" % (time() - start_time))

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return
