from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.core.management.base import BaseCommand, CommandError
from products.models import Product, ProductVariation
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from django.utils import timezone
from datetime import date, timedelta
from django.contrib.postgres.aggregates.general import ArrayAgg
from sellers.models import Seller
from notifications.models import Token
from time import time as time_start
from bs4 import SoupStrainer,  BeautifulSoup
import asyncio
from aiohttp import ClientSession, ClientTimeout
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import requests
sellers = Seller.objects.filter(number_of_items=0)
token = Token.objects.get(id=1).token_ml
url_ml = "https://api.mercadolibre.com/sites/MLC/search?seller_id="
start_time = time_start()
content_product = SoupStrainer(id='short-desc')

async def save_product_from_url_sellers(data, page_content):
	content = page_content.find('div', attrs={"class": "item-conditions"})
	quantity_content = page_content.find('span', attrs={"class": "dropdown-quantity-available"})
	sold_quantity = 0
	if content is not None:
		content_split = content.text.split()
		for t in content_split:
			try:
				sold_quantity = int(t.replace('.','').replace(',','.'))
			except ValueError:
				pass
	else:
		sold_quantity = 0

	if quantity_content is not None:
		quantity_split = quantity_content.text.replace('(', '').replace(')', '').split()
		for a in quantity_split:
			try:
				quantity = int(a.replace('.','').replace(',','.'))
			except ValueError:
				pass
	else:
		quantity = 0

	if data['reviews']:
		rating = data['reviews']['rating_average']
	else:
		rating = None
	if Product.objects.filter(code=data['id'], url_product=data['permalink']):
		print("Product already created")
	else:
		seller = Seller.objects.filter(id_ml=data['seller']['id'])
		sub_category = SubCategories.objects.filter(code=data['category_id'])
		if not seller:
			seller=None
		else:
			seller = seller[0]

		if not sub_category:
			category=None
		else:
			category = sub_category[0]
		product = Product.objects.create(
			code=data['id'],
			name=data['title'],
			price=data['price'],
			sold_quantity=sold_quantity,
			sub_category=category,
		  	available_quantity=quantity,
		  	url_product=data['permalink'],
		  	original_price=data['original_price'],
		  	stop_time=dt.strptime(data['stop_time'], "%Y-%m-%dT%H:%M:%S.%fZ") + date_time.timedelta(days=-7300),
		  	rating=rating,
		  	seller=seller,
		  	from_seller_list=True
			)
		# products_bulk.append(product)
		print("Product append")



async def fetch(url, session, product):
	try:
		async with session.get(url, timeout=20) as response:
			text = await response.read()
			page_content = BeautifulSoup(text.decode('utf-8'), "html.parser", parse_only=content_product)
			await save_product_from_url_sellers(product, page_content)
		return text
	except Exception as e:
		pass

async def bound_fetch(sem, url, session, product):
	async with sem:
		# print("hoahb")
		await fetch(url, session, product)


async def run(r):
    
    tasks = []
    # create instance of Semaphore
    sem = asyncio.Semaphore(1000)

    async with ClientSession() as session:
    	# products_bulk = []
    	for seller in sellers:
    		url_seller_list = url_ml + str(seller.id_ml) + "&access_token=" + token
    		for offset in range(0, 5000, 50):
    			tasks = []
    			try:
	    			request_made = requests.get(url_seller_list + "&offset=" + str(offset), timeout=15).json()
    				number_of_items = request_made['paging']['total']
	    			if request_made['paging']['offset'] >= number_of_items:
	    				for each_product in request_made['results']:
	    					task = asyncio.ensure_future(bound_fetch(sem, each_product['permalink'], session, each_product))
	    					tasks.append(task)
	    				break
	    			else:
	    				for each_product in request_made['results']:
	    					# print(each_product)
	    					task = asyncio.ensure_future(bound_fetch(sem, each_product['permalink'], session, each_product))
	    					tasks.append(task)
			    	responses = asyncio.gather(*tasks)
			    	await responses
	    		except requests.exceptions.Timeout:
			    	continue
    		# print(number_of_items)
    		update_seller = Seller.objects.filter(id=seller.id).update(number_of_items=number_of_items)

class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def handle(self, *args, **options):
    	start_time = time.time()
    	number = 10000
    	loop = asyncio.get_event_loop()
    	future = asyncio.ensure_future(run(number))
    	loop.run_until_complete(future)
    	print("--- %s seconds ---" % (time.time() - start_time))

