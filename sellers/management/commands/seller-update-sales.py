from django.core.management.base import BaseCommand, CommandError
import argparse
import asyncio
import sys
# import resource
from aiohttp import ClientSession
from functools import partial
from time import time
from bs4 import BeautifulSoup, SoupStrainer
import json
import requests
from multiprocessing.dummy import Pool as ThreadPool
from products.models import Product, ProductVariation
from sellers.models import SellerVariation, Seller
from django.utils import timezone
import time as ti_me
from datetime import datetime as dt

initial_time = ti_me.strftime("%H:%M:%S")
start_time = time()
content_product = SoupStrainer('div',{'class': 'seller-info'})
today_date = timezone.now()
variations = SellerVariation.objects.filter(date_search=today_date).values_list('seller__id', flat=True)
all_products = Seller.objects.filter(seller_url__isnull=False,).exclude(id__in=variations).values('seller_url', 'id')[:2000]
# all_products = Product.objects.filter(channel__id=1, url_product__isnull=False).values('url_product', 'id')[:4000]
list_variations = []
print(all_products.count())


async def run(session, number_of_requests, concurrent_limit):
	base_url = "{}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i, id_seller):
		url = base_url.format(i)
		try:
			async with session.get(url) as response:
				response = await response.read()
				sem.release()
				responses.append(response)
				page_content = BeautifulSoup(response.decode('utf-8'), 'lxml', parse_only=content_product)
				content = page_content.find('p', attrs={"class": "seller-info__subtitle-sales"})
				if content is not None:
					sales_seller = content.find('span').find('span').text
				else:
					sales_seller = None
				print(sales_seller)
				
				list_variations.append(
		            SellerVariation(
		            	seller_id=id_seller,
						sales_last_4_month=sales_seller,
		            )
		        )
			
				return response
		except Exception as e:
			print(e)
			pass

	for i in all_products:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i['seller_url'], i['id']))
		task.add_done_callback(tasks.remove)
		tasks.append(task)

	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	print("total_variations: {}".format(len(list_variations)))
	SellerVariation.objects.bulk_create(list_variations)
	# with open('LogProcess.txt', "a+") as f:
	# 	f.write(str(dt.now().date())+ "\t" +"Etapa 3 Recolección de datos" + "\t" + initial_time + "\t" + ti_me.strftime("%H:%M:%S") + "\t" + str(len(list_variations)) + "\t" + str(len(variations)) + "\n")
	# 	f.close()
	return responses


async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		if all_products:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()
		print("--- %s seconds ---" % (time() - start_time))