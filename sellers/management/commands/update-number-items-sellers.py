from django.core.management.base import BaseCommand, CommandError
from products.models import Product
import os
from time import time
import argparse
import asyncio
import sys
from aiohttp import ClientSession
import json
from datetime import datetime as dt
from sellers.models import Seller
from products.models import Product
from categories.models import Channel
from datetime import timedelta
import time as ti_me
from notifications.models import Token
all_sellers = Seller.objects.all().values_list('id_ml', flat=True).distinct()
token = Token.objects.get(id=1).token_ml
start_time = time()
# sellers_bulk = []


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		print(len(all_sellers))
		if all_sellers:
			loop = asyncio.get_event_loop()
			loop.run_until_complete(main(10000, 10000))
			loop.close()
		# print(len(repeated_remove))
async def run(session, number_of_requests, concurrent_limit):
	base_url = "https://api.mercadolibre.com/sites/MLC/search?seller_id={}"
	second_childs = []
	tasks = []
	responses = []
	sem = asyncio.Semaphore(concurrent_limit)

	async def fetch(i):
		url = base_url.format(i) + "&access_token=" + token
		async with session.get(url) as response:
			response = await response.read()
			sem.release()
			responses.append(response)
			data_json = json.loads(response)
			# Se añade el seller a la base
			# seller = Seller.objects.filter(id_ml=i)
			print(data_json['paging']['total'])
			Seller.objects.filter(id_ml=i).update(number_of_items=data_json['paging']['total'])
			
			return response

	for i in all_sellers:
		await sem.acquire()
		task = asyncio.ensure_future(fetch(i))
		task.add_done_callback(tasks.remove)
		tasks.append(task)
	await asyncio.wait(tasks)
	print("total_responses: {}".format(len(responses)))
	# print("total_sellers_append: {}".format(len(sellers_bulk)))
	# Seller.objects.bulk_create(sellers_bulk)
	print("--- %s seconds ---" % (time() - start_time))

async def main(number_of_requests, concurrent_limit):
	async with ClientSession() as session:
		responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
		return
