# Generated by Django 2.1.2 on 2018-11-09 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nickname', models.CharField(max_length=40)),
                ('registration_date', models.DateField()),
                ('city', models.CharField(blank=True, max_length=40, null=True)),
                ('user_type', models.CharField(choices=[('normal', 'normal'), ('brand', 'brand')], max_length=7)),
                ('tags', models.CharField(max_length=60)),
                ('points', models.IntegerField(default=0)),
                ('site_id', models.CharField(default='MLC', max_length=10)),
                ('seller_url', models.URLField(blank=True, null=True)),
                ('level_ml', models.CharField(blank=True, max_length=30, null=True)),
                ('power_seller_status', models.CharField(blank=True, max_length=30, null=True)),
                ('complete_sales', models.IntegerField(default=0)),
                ('canceled_sales', models.IntegerField(default=0)),
                ('total_sales', models.IntegerField(default=0)),
                ('negative_percent', models.DecimalField(decimal_places=1, max_digits=4)),
                ('neutral_percent', models.DecimalField(decimal_places=1, max_digits=4)),
                ('positive_percent', models.DecimalField(decimal_places=1, max_digits=4)),
                ('user_status', models.CharField(choices=[('active', 'active'), ('inactive', 'inactive')], max_length=15)),
            ],
        ),
    ]
