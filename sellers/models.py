from django.db import models
from django.utils import timezone
# Create your models here.

# class City(models.Model):
# 	name = models.CharField(max_length=60)

class Seller(models.Model):
	
	USER_STATUS = (
		('active', 'active'),
		('inactive', 'inactive'),
	)
	id_ml = models.IntegerField(null=True, default=0)
	nickname = models.CharField(max_length=40)
	registration_date = models.DateField()
	city = models.CharField(max_length=40,null=True, blank=True)
	user_type = models.CharField(max_length=20)
	tags = models.CharField(max_length=60)
	points = models.IntegerField(default=0)
	site_id = models.CharField(max_length=10, default='MLC')
	seller_url = models.URLField(null=True, blank=True)
	level_ml = models.CharField(max_length=30, null=True, blank=True)
	power_seller_status = models.CharField(max_length=30, null=True, blank=True)
	complete_sales = models.IntegerField(default=0)
	canceled_sales = models.IntegerField(default=0)
	total_sales = models.IntegerField(default=0)
	negative_percent = models.DecimalField(decimal_places=1, max_digits=4)
	neutral_percent = models.DecimalField(decimal_places=1, max_digits=4)
	positive_percent = models.DecimalField(decimal_places=1, max_digits=4)
	user_status = models.CharField(choices=USER_STATUS, max_length=15)
	number_of_items = models.IntegerField(default=0)

	class Meta:
		db_table = 'seller'


class SellerVariation(models.Model):
	seller = models.ForeignKey(Seller, on_delete=models.SET_NULL, null=True, blank=True, related_name='seller_variation')
	date_search = models.DateField(default=timezone.now)
	sales_last_4_month = models.IntegerField(null=True)
	daily_sales = models.IntegerField(default=0)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'seller_variation'
