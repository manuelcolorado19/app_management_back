from products.models import Product
import requests
from multiprocessing.dummy import Pool as ThreadPool
import time
from functools import partial
from datetime import datetime as dt
import datetime as date_time
from openpyxl import load_workbook
from sellers.models import Seller

def nth_repl(s, sub, repl, nth):
    find = s.find(sub)
    # if find is not p1 we have found at least one match for the substring
    i = find != -1
    # loop util we find the nth or we find no match
    while find != -1 and i != nth:
        # find + 1 means we start at the last match start index + 1
        find = s.find(sub, find + 1)
        i += 1
    # if i  is equal to nth we found nth matches so replace
    if i == nth:
        return s[:find]+repl+s[find + len(sub):]
    return s


url_ml = "https://api.mercadolibre.com/users/"
sellers_bulk = []
def get_seller_data_from_api(data):
	# print("id " + str(data))
	data_api = requests.get(url_ml + str(data), timeout=15).json()
	tags = ''
	for i in data_api['tags']:
		tags += i
	number_of_items = requests.get("https://api.mercadolibre.com/sites/MLC/search?seller_id=" + str(data)).json()['paging']['total']
	seller = Seller.objects.get(
		id_ml=data,
		# nickname=data_api['nickname'],
		# registration_date=dt.strptime(nth_repl(data_api['registration_date'], ":", "", 3), "%Y-%m-%dT%H:%M:%S.000%z") ,
		# city=data_api['address']['city'],
		# user_type=data_api['user_type'],
		# tags=tags,
		# points=data_api['points'],
		# seller_url=data_api['permalink'],
		# level_ml=data_api['seller_reputation']['level_id'],
		# power_seller_status=data_api['seller_reputation']['power_seller_status'],
		# complete_sales=data_api['seller_reputation']['transactions']['completed'],
		# canceled_sales=data_api['seller_reputation']['transactions']['canceled'],
		# total_sales=data_api['seller_reputation']['transactions']['total'],
		# negative_percent=data_api['seller_reputation']['transactions']['ratings']['negative'],
		# neutral_percent=data_api['seller_reputation']['transactions']['ratings']['neutral'],
		# positive_percent=data_api['seller_reputation']['transactions']['ratings']['positive'],
		# user_status=data_api['status']['site_status'],
		)
	seller.number_of_items = number_of_items
	seller.save()
	# sellers_bulk.append(seller)
	print("Seller append")

# open file
start_time = time.time()
wb = load_workbook('sellers/Sellers.xlsm')
ws = wb['Hoja1']
data = []
row_number = 1
for row in ws.rows:
	if row_number > 1:
		data.append(row[0].value)
		row_number += 1
	else:
		row_number +=1
pool = ThreadPool(20)
results = pool.map(get_seller_data_from_api, data)
pool.close()
pool.join()
# Seller.objects.bulk_create(sellers_bulk)
print("--- %s seconds ---" % (time.time() - start_time))
