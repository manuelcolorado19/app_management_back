import asyncio
from aiohttp import ClientSession, ClientTimeout
from sellers.models import Seller
from notifications.models import Token
from categories.models import SubCategories
from datetime import datetime as dt
import datetime as date_time
from time import time
import json
import requests
from django.contrib.postgres.aggregates.general import ArrayAgg
list_sellers = []
# id__in=[2601,2428,2374],
sellers = Seller.objects.filter(id=2316).annotate(products_seller=ArrayAgg('seller_product__code'))[:3]
token = Token.objects.get(id=1).token_ml
url_seller = "https://api.mercadolibre.com/sites/MLC/search?seller_id="
async def fetch(seller, session):
    try:
        async with session.get(url_seller + str(seller.id_ml) + "&access_token=" + token, timeout=20) as response:
            text = await response.json()
            total_api = text['paging']['total']
            print(total_api)
            print(seller.number_of_items)
            if not(total_api == seller.number_of_items):
            	for offset in range(0, total_api, 50):
            		print(offset)
    				# print(len(data['results']))

            		# api_response = requests.get(url_seller + str(seller.id_ml) + "&access_token=" + token +"&offset=" +offset, timeout=15).json()['results']
            		# print(api_response)
            		# # data = json.loads('results']
            		# for each_product in api_response:
            		# 	if each_product['id'] in seller.products_seller:
            		# 		continue
            		# 	else:
            		# 		print("Este producto es nuevo")
            else:
            	pass
            return text
    except Exception as e:
        pass

async def bound_fetch(sem, seller, session):
    async with sem:
        await fetch(seller, session)


async def run(r):
    
    tasks = []
    # create instance of Semaphore
    sem = asyncio.Semaphore(1000)

    async with ClientSession() as session:
        for seller in sellers:
        	task = asyncio.ensure_future(bound_fetch(sem, seller, session))
        	tasks.append(task)

        responses = asyncio.gather(*tasks)
        await responses

start_time = time()
number = 10000
loop = asyncio.get_event_loop()
future = asyncio.ensure_future(run(number))
loop.run_until_complete(future)
# Product.objects.bulk_create(products_bulk)
print("--- %s seconds ---" % (time() - start_time))