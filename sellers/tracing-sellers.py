from products.models import Product
from sellers.models import Seller
import requests
import asyncio
from aiohttp import ClientSession, ClientTimeout

products = Product.objects.all()

async def fetch(session, product):
    try:
        async with session.get("https://api.mercadolibre.com/items/"+product.code, timeout=20) as response:
            text = await response.json()
            # print(text.get('seller_id', 0))
            seller_id = text.get('seller_id', None)
            if Seller.objects.filter(id_ml=seller_id):
            	seller = Seller.objects.get(id_ml=seller_id)
            	seller_id = seller_id
            else:
            	seller = None
            p = Products.objects.get(id=product.id)
            p.seller = seller
            p.seller_id_ml = seller_id
            p.save()
            print("Products save")

        return text
    except Exception as e:
        pass

async def bound_fetch(sem, session, product):
    async with sem:
        await fetch(session, product)


async def run(r):
    
    tasks = []
    # create instance of Semaphore
    sem = asyncio.Semaphore(1000)

    async with ClientSession() as session:
        for product in products:
            task = asyncio.ensure_future(bound_fetch(sem, session, product))
            tasks.append(task)
        responses = asyncio.gather(*tasks)
        await responses

number = 10000
loop = asyncio.get_event_loop()
future = asyncio.ensure_future(run(number))
loop.run_until_complete(future)
