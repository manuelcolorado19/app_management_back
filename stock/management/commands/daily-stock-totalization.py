import datetime
from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail, EmailMessage
from stock.serializers import Product_SaleStockSerializer
from sales.models import Product_Sale
from stock.models import InventoryTotalization, Inventory


class Command(BaseCommand):
	args = ''
	help = 'Export data to remote server'

	def handle(self, *args, **options):
		queryset = Product_Sale.objects.exclude(sku__in=[1,2]).order_by('sku')
		serializer = Product_SaleStockSerializer(queryset, many=True)
		quantity = 0
		total_cost = 0
		last_inventory = Inventory.objects.latest('date_created')
		rows = serializer.data
		for row in rows:
			if row['product_code']:
				for i in row['product_code']:
					quantity_product = i['initial_stock'] + i['initial_warehouse_stock'] + i['purchases_warehouse5'] - i['sale_quantity'] - i['sale_pending']
					if quantity_product < 0:
						quantity_product = 0
					quantity += quantity_product
					total_cost += (row['product_cost'] * quantity)
			else:
				pass
		inventory_totalization = InventoryTotalization.objects.create(
			date_search=datetime.datetime.now().date(),
			quantity_stock=quantity,
			total_cost=total_cost,
			inventory=last_inventory,
		)

