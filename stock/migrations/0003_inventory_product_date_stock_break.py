# Generated by Django 2.1.2 on 2019-06-10 15:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0002_inventory_product_reaconditionated'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory_product',
            name='date_stock_break',
            field=models.DateTimeField(null=True),
        ),
    ]
