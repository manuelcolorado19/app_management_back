from django.db import models
from django.contrib.auth import get_user_model
from sales.models import Product_Sale, Product_Code
User = get_user_model()


class Inventory(models.Model):
	name = models.CharField(max_length=15, null=True, blank=True)
	description = models.TextField(null=True)
	user_charge = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='user_inventory')
	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	date_stock_break = models.DateTimeField(null=True, blank=True)
	class Meta:
		db_table = "inventory"

class Inventory_Product(models.Model):
	inventory = models.ForeignKey(Inventory, on_delete=models.CASCADE, related_name='stock_inventory')
	product = models.ForeignKey(Product_Sale, on_delete=models.SET_NULL, null=True, blank=True, related_name='product_stock')
	product_code = models.ForeignKey(Product_Code, on_delete=models.SET_NULL, null=True, blank=True, related_name='product_code_stock')
	stock_good = models.IntegerField(null=True)
	stock_bad = models.IntegerField(null=True)
	stock_detail = models.IntegerField(null=True)
	total_stock = models.IntegerField(null=True)
	reaconditionated = models.IntegerField(null=True, default=0)
	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	date_updated = models.DateTimeField(auto_now=True)

class InventoryTotalization(models.Model):
	inventory = models.ForeignKey(Inventory, on_delete=models.SET_NULL, related_name='stock_inventory_totalization', blank=True, null=True)
	date_search = models.DateField()
	quantity_stock = models.IntegerField()
	total_cost = models.BigIntegerField()
	date_created = models.DateTimeField(auto_now_add=True)
	date_updated = models.DateTimeField(auto_now=True)

	class Meta:
		db_table = 'inventory_totalization'