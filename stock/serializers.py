from .models import Inventory, Inventory_Product
from rest_framework import serializers
import datetime
from django.db import transaction
from datetime import timedelta
from django.db.models.functions import Cast, Concat
from django.db.models import F, Sum, IntegerField, Value, CharField, Case, When, Q
from sales.models import Product_Sale, Sale, Sale_Product, Color_Product, Product_Channel, Product_Code, Pack, Pack_Products, Category_Product_Sale, Sales_Report, Color, Product_SKU_Ripley, Product_Sale_Historic_Cost
from purchases.models import Purchase, Purchase_Product
import datetime
from returns.models import Return_Product, Return
from warehouses.serializers import IncomingWareHouse, IncomingWareHouseCode, StockWarehouse, StockWareHouseCode, RetirementProduct
from warehouses.models import StockWareHouseCode, RetirementProduct
date_last_inventory = Inventory.objects.latest('date_created').date_stock_break.date()
class UserDataSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.user_charge.first_name + " " + value.user_charge.last_name

class EndStockDate(serializers.CharField):
	def to_representation(self, value):
		last_inventory = Inventory.objects.filter(id=value.id + 1)
		if last_inventory:
			if last_inventory[0].date_stock_break is not None:
				return last_inventory[0].date_stock_break
		return "-"

class InventorySerializer(serializers.ModelSerializer):
	date_end = EndStockDate(read_only=True, source='*')
	user_data = UserDataSerializer(read_only=True, source='*')
	class Meta:
		model = Inventory
		fields = '__all__'

class SaleQuantity(serializers.IntegerField):
	def to_representation(self, value):
		sales = Sale_Product.objects.filter(Q(product=value), Q(sale__cut_inventory__in=["I4",'I5', ""]), Q(sale__status='entregado') | Q(sale__comments__icontains="Guía de Despacho")).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
		if sales is not None:
			return sales
		else:
			return 0

		# sales = Sale_Product.objects.filter(product=value, sale__cut_inventory="", sale__status="entregado").aggregate(sale_quantity=Sum(Case(When(product_code__reset=True, then=Case(When(sale__date_created__gt=F('product_code__date_reset'), then=F('quantity')), default=0, output_field=IntegerField())), When(product_code__isnull=True, then=Value(0, output_field=IntegerField())), default=F('quantity'), output_field=IntegerField())))['sale_quantity']
		# if sales is not None:
		# 	return sales
		return 0

class SaleQuantityPending(serializers.IntegerField):
        def to_representation(self, value):
                sales = Sale_Product.objects.filter(Q(product=value), Q(sale__cut_inventory__in=["I4",'I5',""]), Q(sale__status='pendiente')).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
                if sales is not None:
                    return sales
                else:
                    return 0

                # sales = Sale_Product.objects.filter(product=value, sale__cut_inventory="", sale__status="entregado").aggregate(sale_quantity=Sum(Case(When(product_code__reset=True, then=$
                # if sales is not None:
                #       return sales
                return 0


class WeeksSales(serializers.IntegerField):
	def to_representation(self, value):
		sales = Sale_Product.objects.filter(product=value, sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))['sale_quantity']
		if sales is not None:
			return sales
		return 0

class InitialStock(serializers.IntegerField):
	def to_representation(self, value):
		return 0
		# stock_reset = Product_Code.objects.filter(id__in=value.product_code.filter(reset=True)).values_list('id',flat=True).aggregate(sum_reset=Sum('quantity_reset'))

		# initial_stock = Inventory_Product.objects.filter(product=value).aggregate(quantity_stock=Sum(Case(When(product_code__reset=True, then=Value(0, output_field=IntegerField())), default=F('stock_good'), output_field=IntegerField())))
		# reset_stock = stock_reset['sum_reset'] if stock_reset['sum_reset'] is not None else 0
		# stock_initial = initial_stock['quantity_stock'] if initial_stock['quantity_stock'] is not None else 0
		# return reset_stock + stock_initial

		# print(Purchase_Product.objects.filter(product_purchase=value))
		# reset_filter = value.product_code.filter(reset=True)
		# if reset_filter:
		# 	initial_stock = Inventory_Product.objects.filter(product=value, product_code__reset=False).aggregate(quantity_stock=Sum('stock_good'))
		# 	if initial_stock['quantity_stock'] is not None:
		# 		count = initial_stock['quantity_stock']
		# 	else:
		# 		count = 0
		# 	for each in reset_filter:
		# 		count += each.quantity
		# 	return count
		# else:
		# initial_stock = Inventory_Product.objects.filter(product=value).aggregate(quantity_stock=Sum('stock_good'))
		# if initial_stock['quantity_stock'] is not None:
		# 	return initial_stock['quantity_stock']
		# else:
		# 	return 0

class MermaQuantityProduct(serializers.IntegerField):
	def to_representation(self, value):
		return 0

class LeftQuantityProduct(serializers.IntegerField):
	def to_representation(self, value):
		purchases= Purchase_Product.objects.filter(product_purchase=value).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			purchase_quantity = purchases['count_product']
		else:
			purchase_quantity = 0

		sales = Sale_Product.objects.filter(product=value).aggregate(sale_quantity=Sum('quantity'))
		if sales['sale_quantity'] is not None:
			sales = sales['sale_quantity']
		else:
			sales = 0

		return purchase_quantity - sales

class AvailableStockWeeks(serializers.CharField):
	def to_representation(self, value):
		purchases = Purchase_Product.objects.filter(product_purchase=value).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			purchases = purchases['count_product']
		else:
			purchases = 0

		sales_product = Sale_Product.objects.filter(product=value).aggregate(sale_quantity=Sum('quantity'))
		if sales_product['sale_quantity'] is not None:
			sales_product = sales_product['sale_quantity']
		else:
			sales_product = 0
		stock = purchases - sales_product

		sales = Sale_Product.objects.filter(product=value, sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))['sale_quantity']

		if sales is not None:
			return str(int(round(stock / sales)))
		return '-'


class EmptyStockDate(serializers.CharField):
	def to_representation(self, value):

		purchases = Purchase_Product.objects.filter(product_purchase=value).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			purchases = purchases['count_product']
		else:
			purchases = 0

		sales_product = Sale_Product.objects.filter(product=value, sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))
		if sales_product['sale_quantity'] is not None:
			sales_product = sales_product['sale_quantity']
		else:
			sales_product = 0
		stock = purchases - sales_product

		# sales = Sale_Product.objects.filter(product=value, sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))['sale_quantity']
		if stock > 0 and sales_product > 0:
			sale_last_week_days = sales_product / 7
			total_dates = round(stock / sale_last_week_days)
			return str((datetime.datetime.today() + timedelta(days=total_dates)).date())
		else:
			return '9999-01-01'

class PurchasesQuantity1(serializers.IntegerField):
	def to_representation(self, value):
		purchases = Purchase_Product.objects.filter(product_purchase=value, purchase__id=88).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
		 	return purchases['count_product']
		return 0
class PurchasesQuantity2(serializers.IntegerField):
        def to_representation(self, value):
                purchases = Purchase_Product.objects.filter(product_purchase=value, purchase__id=89).aggregate(count_product=Sum('quantity'))
                if purchases['count_product'] is not None:
                        return purchases['count_product']
                return 0
class PurchasesQuantity3(serializers.IntegerField):
        def to_representation(self, value):
                purchases = Purchase_Product.objects.filter(product_purchase=value, purchase__id=87).aggregate(count_product=Sum('quantity'))
                if purchases['count_product'] is not None:
                        return purchases['count_product']
                return 0
class PurchasesQuantity4(serializers.IntegerField):
        def to_representation(self, value):
                purchases = Purchase_Product.objects.filter(product_purchase=value, purchase__id=70).aggregate(count_product=Sum('quantity'))
                if purchases['count_product'] is not None:
                        return purchases['count_product']
                return 0


class ReturnsTotal(serializers.IntegerField):
	def to_representation(self, value):
		# returns = Return_Product.objects.filter(product_sale_return=value).aggregate(count_product=Sum('quantity'))
		# if returns['count_product'] is not None:
		# 	return returns['count_product']
		return 0

class ReturnsReaconditinated(serializers.IntegerField):
	def to_representation(self, value):
		# returns = Return_Product.objects.filter(product_sale_return=value, return_sale__client_request__in=['cambio del producto']).aggregate(count_product=Sum('quantity'))
		returns = Inventory_Product.objects.filter(product=value).aggregate(quantity_stock=Sum('reaconditionated'))
		if returns['quantity_stock'] is not None:
			return returns['quantity_stock']
		return 0

class ReturnsAsNew(serializers.IntegerField):
	def to_representation(self, value):
		returns = Return_Product.objects.filter(product_sale_return=value, return_sale__client_request__in=['retracto','devolución del dinero']).aggregate(count_product=Sum('quantity'))
		if returns['count_product'] is not None:
			return returns['count_product']
		return 0

class Color_Description(serializers.CharField):
	def to_representation(self, value):
		return value.color.description

class SaleQuantityCode(serializers.IntegerField):
	def to_representation(self, value):
		if value.reset:
			sales = Sale_Product.objects.filter(Q(product_code=value), Q(sale__cut_inventory=""), Q(date_created__gt=value.date_reset), Q(sale__status="entregado") | Q(sale__comments__icontains="Guía de Despacho")).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
			if sales is not None:
				return sales
			else:
				return 0
		else:
			sales = Sale_Product.objects.filter(Q(product_code=value), Q(sale__status='entregado'), Q(sale__date_document_emision__gt=date_last_inventory)).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
			# if sales2 is not None:
			# 	sales3 = sales2
			# else:
			# 	sales3 = 0
			if sales is not None:
				return sales
			else:
				return 0
class SaleQuantityCode2(serializers.IntegerField):
        def to_representation(self, value):
		#return 0
                if value.reset:
                        sales = Sale_Product.objects.filter(Q(product_code=value), Q(sale__cut_inventory=""), Q(date_created__gt=value.date_reset), Q(sale__status="entregado") | Q(sale__comments__icontains="Guía de Despacho")).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
                        if sales is not None:
                                return sales
                        else:
                                return 0
                else:
                        sales = Sale_Product.objects.filter(Q(product_code=value), Q(sale__cut_inventory__in=['']), Q(sale__status='entregado') | Q(sale__comments__icontains="Guía de Despacho")).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
                        if sales is not None:
                                return 0
                        else:
                                return 0


class SalePendingCode(serializers.IntegerField):
	def to_representation(self, value):
		today_lest_7 = datetime.datetime.now().date() + timedelta(days=-30)
		if value.reset:
			sales = Sale_Product.objects.filter(Q(product_code=value), Q(sale__cut_inventory=""), Q(date_created__gt=value.date_reset), Q(sale__status="pendiente")).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
			if sales is not None:
				return sales
			else:
				return 0
		else:
			sales = Sale_Product.objects.filter(Q(product_code=value), Q(sale__status='pendiente'), Q(sale__date_sale__gte=today_lest_7)).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
			if sales is not None:
				return sales
			else:
				return 0

class PurchasesQuantityCode(serializers.IntegerField):
	def to_representation(self, value):
		if value.reset:
			purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__date_created__gte=value.date_reset, purchase__warehouse__isnull=True).aggregate(count_product=Sum('quantity'))
			if purchases['count_product'] is not None:
				return purchases['count_product']
			return 0
		else:
			#purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__date_purchase__gte=datetime.date(2019,4,1), purchase__warehouse__isnull=True).aggregate(count_product=Sum('quantity'))
			purchases = StockWareHouseCode.objects.filter(product_code=value, stock_warehouse__id__gt=314).aggregate(count_product=Sum('initial_stock'))
			if purchases['count_product'] is not None:
				#retirement_b1 = RetirementProduct.objects.filter(product_code=value, retirement__date_retirement__gt=datetime.date(2019,6,6), retirement__warehouse__id=1).aggregate(sum_products=Sum('quantity')).get('sum_products', 0)
				#retirement_b2 = RetirementProduct.objects.filter(product_code=value, retirement__date_retirement__gt=datetime.date(2019,6,7), retirement__warehouse__id=2).aggregate(sum_products=Sum('quantity')).get('sum_products', 0)
				#if retirement_b1 is not None:
				#	reti1 = retirement_b1
				#else:
				#	reti1 = 0
				#if retirement_b2 is not None:
				#	reti2 = retirement_b2
				#else:
				#	reti2 = 0
				#if value.code_seven_digits == "1201350":
				#	print(purchases['count_product'])
				return purchases['count_product']
			return 0

class PurchasesOffice(serializers.IntegerField):
	def to_representation(self, value):
		return 0

class PurchasesWareHouse(serializers.IntegerField):
	def to_representation(self, value):
		purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__date_purchase__gte=datetime.date(2019,4,1), purchase__warehouse__isnull=False).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			return purchases['count_product']
		return 0

class ReturnsTotalCode(serializers.IntegerField):
	def to_representation(self, value):
		returns = Return_Product.objects.filter(product_sale_code_return=value).aggregate(count_product=Sum('quantity'))
		if returns['count_product'] is not None:
			return returns['count_product']
		return 0

class ReturnsReaconditinatedCode(serializers.IntegerField):
	def to_representation(self, value):
		# returns = Return_Product.objects.filter(product_sale_code_return=value, return_sale__client_request__in=['cambio del producto']).aggregate(count_product=Sum('quantity'))
		if value.reset:
			return 0
		returns = Inventory_Product.objects.filter(product_code=value).aggregate(quantity_stock=Sum('reaconditionated'))
		if returns['quantity_stock'] is not None:
			return returns['quantity_stock']
		return 0
		# if returns['count_product'] is not None:
		# 	return returns['count_product']
		# return 0

class ReturnsAsNewCode(serializers.IntegerField):
	def to_representation(self, value):
		returns = Return_Product.objects.filter(product_sale_code_return=value, return_sale__client_request__in=['retracto','devolución del dinero']).aggregate(count_product=Sum('quantity'))
		if returns['count_product'] is not None:
			return returns['count_product']
		return 0

class WeeksSalesProductCode(serializers.IntegerField):
	def to_representation(self, value):
		sales = Sale_Product.objects.filter(product_code=value, sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))['sale_quantity']
		if sales is not None:
			return sales
		return 0

class MermaQuantityProductCode(serializers.IntegerField):
	def to_representation(self, value):
		return 0

class InitialStockCode(serializers.IntegerField):
	def to_representation(self, value):
		if value.reset:
			if value.quantity_reset is not None:
				return value.quantity_reset
			else:
				return 0
		else:
			initial_stock = Inventory_Product.objects.filter(product_code=value).order_by('-inventory__date_created')[:1]
			if initial_stock:
				if initial_stock[0].stock_good is not None:
					return initial_stock[0].stock_good
				else:
					return 0
			else:
				return 0

class LeftProductCode(serializers.IntegerField):
	def to_representation(self, value):
		return 0

class AvailableStockWeeksCode(serializers.CharField):
	def to_representation(self, value):

		purchases = Purchase_Product.objects.filter(product_purchase_code=value).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			purchases = purchases['count_product']
		else:
			purchases = 0

		sales_product = Sale_Product.objects.filter(product_code=value).aggregate(sale_quantity=Sum('quantity'))
		if sales_product['sale_quantity'] is not None:
			sales_product = sales_product['sale_quantity']
		else:
			sales_product = 0
		# print(str(purchases) + " Comprados")
		# print(str(sales_product) + " Vendidos")
		stock = purchases - sales_product

		sales = Sale_Product.objects.filter(product_code=value, sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))['sale_quantity']
		if sales is not None:
			# print(sales)
			return str(int(round(stock / sales)))
			# return 9
		return '-'

class EmptyStockDateCode(serializers.CharField):
	def to_representation(self, value):

		purchases = Purchase_Product.objects.filter(product_purchase_code=value).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			purchases = purchases['count_product']
		else:
			purchases = 0

		sales_product = Sale_Product.objects.filter(product_code=value, sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))
		if sales_product['sale_quantity'] is not None:
			sales_product = sales_product['sale_quantity']
		else:
			sales_product = 0
		stock = purchases - sales_product

		# sales = Sale_Product.objects.filter(product_code=value, sale__date_sale__gte=(datetime.datetime.today() - timedelta(days=7)).date()).aggregate(sale_quantity=Sum('quantity'))['sale_quantity']
		if stock > 0 and sales_product > 0:
			sale_last_week_days = sales_product / 7
			total_dates = round(stock / sale_last_week_days)
			return str((datetime.datetime.today() + timedelta(days=total_dates)).date())
		else:
			return '-'



class LeftQuantityProductCode(serializers.IntegerField):
	def to_representation(self, value):
		purchases= Purchase_Product.objects.filter(product_purchase_code=value).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			purchase_quantity = purchases['count_product']
		else:
			purchase_quantity = 0

		sales = Sale_Product.objects.filter(product_code=value).aggregate(sale_quantity=Sum('quantity'))
		if sales['sale_quantity'] is not None:
			sales = sales['sale_quantity']
		else:
			sales = 0
			
		return purchase_quantity - sales
class ProductDescriptionSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.product.description

class WareHouseTotalProductCode(serializers.IntegerField):
	def to_representation(self, value):
		product_total = StockWareHouseCode.objects.filter(product_code=value, id__gt=358).aggregate(total_warehouses=Sum('initial_stock'))
		#retirements_total = RetirementProduct.objects.filter(product_code=value).aggregate(total_retirement=Sum('quantity'))
		purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__warehouse__isnull=False).aggregate(count_product=Sum('quantity'))
		# if value.code_seven_digits == "1200452":
		# 	print("hola")
		if product_total['total_warehouses'] is not None:
			initial_warehouse = product_total['total_warehouses'] 
			#if retirements_total['total_retirement'] is not None:
			#	retirement_warehouse = retirements_total['total_retirement'] 
			#else:
			#	retirement_warehouse = 0

			if purchases['count_product'] is not None:
				purchase_warehouse = purchases['count_product']
			else:
				purchase_warehouse = 0
			return initial_warehouse - 0 + purchase_warehouse
		else:
			return 0
		
class MermaTotalProduct(serializers.IntegerField):
	def to_representation(self, value):
		return 0
class PurchasesOffice(serializers.IntegerField):
	def to_representation(self, value):
		return 0

class PurchasesWareHouse1(serializers.IntegerField):
	def to_representation(self, value):
		purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__id=88)
		if purchases:
			return 0 #purchases[0].quantity
		return 0
class PurchasesWareHouse2(serializers.IntegerField):
        def to_representation(self, value):
                purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__id=89)
                if purchases:
                        return 0 #purchases[0].quantity
                return 0
class PurchasesWareHouse3(serializers.IntegerField):
        def to_representation(self, value):
                purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__id=87)
                if purchases:
                        return 0 #purchases[0].quantity
                return 0
class PurchasesWareHouse4(serializers.IntegerField):
        def to_representation(self, value):
                purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__id=70)
                if purchases:
                        return 0 #purchases[0].quantity
                return 0
class PurchasesWareHouse5(serializers.IntegerField):
        def to_representation(self, value):
            purchases = Purchase_Product.objects.filter(product_purchase_code=value, purchase__date_purchase__gt=date_last_inventory).aggregate(count_product=Sum('quantity'))
            if purchases['count_product'] is not None:
                    return purchases['count_product']
            return 0

class B1Stock(serializers.IntegerField):
	def to_representation(self, value):
		stock_warehouse = StockWareHouseCode.objects.filter(product_code=value, stock_warehouse__warehouse__id=1, id__gt=571).aggregate(sum_products=Sum('initial_stock'))
		
		if stock_warehouse['sum_products'] is not None:
			return stock_warehouse['sum_products']
		return 0

class B1Retirement(serializers.IntegerField):
	def to_representation(self, value):
		retirement_warehouse = RetirementProduct.objects.filter(product_code=value, retirement__warehouse__id=1, retirement__date_retirement__gt=date_last_inventory, retirement__status_retirement="aprobada").aggregate(sum_products=Sum('quantity'))
		if retirement_warehouse['sum_products'] is not None:
			return retirement_warehouse['sum_products']
		return 0

class B2Stock(serializers.IntegerField):
	def to_representation(self, value):
		stock_warehouse = StockWareHouseCode.objects.filter(product_code=value, stock_warehouse__warehouse__id=2, id__gt=571).aggregate(sum_products=Sum('initial_stock'))

		if stock_warehouse['sum_products'] is not None:
			return stock_warehouse['sum_products']
		return 0

class B2Retirement(serializers.IntegerField):
	def to_representation(self, value):
		retirement_warehouse = RetirementProduct.objects.filter(product_code=value, retirement__warehouse__id=2, retirement__date_retirement__gt=date_last_inventory, retirement__status_retirement="aprobada").aggregate(sum_products=Sum('quantity'))
		if retirement_warehouse['sum_products'] is not None:
			return retirement_warehouse['sum_products']
		return 0

class InitialStockWareHouse(serializers.IntegerField):
	def to_representation(self, value):
		stock = StockWareHouseCode.objects.filter(product_code=value, id__gt=571).aggregate(sum_products=Sum('initial_stock'))
		if stock['sum_products'] is not None:
			return stock['sum_products']
		else:
			return 0

class Product_ColorSerializer(serializers.ModelSerializer):
	initial_stock = InitialStockCode(read_only=True, source='*')# Si
	initial_warehouse_stock = InitialStockWareHouse(read_only=True, source='*')
	b1_retirement = B1Retirement(read_only=True, source='*')
	b2_retirement = B2Retirement(read_only=True, source='*')
	purchases_warehouse5 = PurchasesWareHouse5(read_only=True, source='*')
	sale_quantity = SaleQuantityCode(read_only=True, source='*') # Si
	sale_pending = SalePendingCode(read_only=True, source='*') # Si
	b1_stock = B1Stock(read_only=True, source='*')
	b2_stock = B2Stock(read_only=True, source='*')

	color_description = Color_Description(read_only=True, source='*')
	
	product_description = ProductDescriptionSerializer(read_only=True, source='*')
	class Meta:
		model = Product_Code
		
		fields = (
			'id',
			'initial_stock', 
			'initial_warehouse_stock', 
			'b1_retirement', 
			'b2_retirement',
			'purchases_warehouse5',
			'sale_quantity',
			'sale_pending', 
			'b1_stock', 
			'b2_stock', 
			'color_description', 
			'product_description', 
			'quantity_reset', 
			'code_seven_digits', 
			'reset', 
		)

class Product_CostSerializer(serializers.IntegerField):
	def to_representation(self, value):
		cost = Product_Sale_Historic_Cost.objects.filter(product_sale=value)
		if cost:
			return cost[0].unit_cost
		return 0



class Product_SaleStockSerializer(serializers.ModelSerializer):
	initial_stock = InitialStock(read_only=True, source='*')# Si
	product_code = Product_ColorSerializer(read_only=True, many=True)
	product_cost = Product_CostSerializer(read_only=True, source='*')

	class Meta:
		model = Product_Sale
		fields = (
			'id',
			'sku',
			'description',
			'stock',
			'initial_stock',
			'product_code',
			'product_cost',
		)
