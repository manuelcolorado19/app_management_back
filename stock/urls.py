from .views import InventoryViewSet, Product_SaleStockViewSet, Product_SaleStockViewSetFileXLSX
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'inventory', InventoryViewSet, base_name='inventory')
router.register(r'product_sale_stock', Product_SaleStockViewSet, base_name='product_sale_stock')
router.register(r'product_sale_stock_file', Product_SaleStockViewSetFileXLSX, base_name='product_sale_stock_file')

urlpatterns = [
	url(r'^', include(router.urls)),
]