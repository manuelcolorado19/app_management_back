from django.shortcuts import render
from .models import Inventory, Inventory_Product, InventoryTotalization
from sales.models import Product_Sale, Product_Code
from .serializers import InventorySerializer, Product_SaleStockSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
import datetime
from django.db import transaction
import itertools
from operator import itemgetter
from rest_framework.views import APIView
import io
import xlwt, requests
import xlsxwriter
from django.http import HttpResponse
from rest_framework.permissions import AllowAny, IsAuthenticated
from sales.views import CustomPaginationStock
from rest_framework import filters as search_filter
from django.db.models import F, Sum, IntegerField, Value, CharField, Case, When, Q
from purchases.models import Purchase, Purchase_Product
from rest_framework.pagination import PageNumberPagination

class PaginationStock(PageNumberPagination):
	page_size = 10
	page_size_query_param = 'page_size'
	max_page_size = 10
	def get_paginated_response(self, data):
		date_last_inventory = Inventory.objects.latest('date_created').date_stock_break.date().strftime("%d-%m-%Y")
		return Response({
		   'next': self.get_next_link(),
		   'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'results': data,
			'last_inventory': date_last_inventory
		})
class InventoryViewSet(ModelViewSet):
	serializer_class = InventorySerializer
	queryset = Inventory.objects.all().order_by('-id')
	permission_classes = (IsAuthenticated,)
	http_method_names = ['get', 'post']

	@transaction.atomic
	def create(self, request):
		# print(request.data)
		for row in request.data['data_rows']:
			if Product_Sale.objects.filter(sku=row['sku']).count() == 0:
				return Response(data={"sku": ['El sku ' + str(row['sku']) + " no se encuentra en los registros."]}, status=status.HTTP_400_BAD_REQUEST)

			if Product_Code.objects.filter(code_seven_digits=str(row['code_seven'])).count() == 0:
				return Response(data={"sku": ['El código ' + str(row['code_seven']) + " no se encuentra en los registros."]}, status=status.HTTP_400_BAD_REQUEST)
		sorted_order = sorted(request.data['data_rows'], key=itemgetter('sku',))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['sku']):
		    group_order.append(list(group))
		today = datetime.datetime.now().date()
		date_stock_break = datetime.datetime.strptime(request.data['date_stock_break'], "%d-%m-%Y %H:%M")

		all_inventory = Inventory.objects.all().order_by('id')
		name = int(all_inventory[all_inventory.count() - 1].name.replace("I", "")) + 1
		# print(name)
		name = "I" + str(name)

		inventory = Inventory.objects.create(
			name=name,
			description="Inventario de productos hasta el " + str(date_stock_break),
			user_charge=request.user,
			date_stock_break=date_stock_break
		)
		for product in group_order:
			total_stock_product = 0
			for code in product:
				stock_product = Inventory_Product.objects.create(
					inventory=inventory,
					product=Product_Sale.objects.get(sku=product[0]['sku']),
					product_code=Product_Code.objects.get(code_seven_digits=code['code_seven']),
					stock_good=code['total_stock'],
					stock_bad=code['stock_bad'],
					stock_detail=code['stock_detail'],
					total_stock=int(code['total_stock']) + int(code['stock_bad']) + int(code['stock_detail']),
				)
				#Product_Code.objects.filter(code_seven_digits=code['code_seven']).update(quantity=stock_product.stock_good)
				total_stock_product += stock_product.stock_good
			#Product_Sale.objects.filter(sku=product[0]['sku']).update(stock=total_stock_product)
		return Response(data={"inventory": ['Inventario cargado exitosamente']}, status=status.HTTP_200_OK)


class InventoryReport(APIView):
	def get(self, request):
		print(request.query_params)
		id_inventory = request.query_params.get('inventory', None)
		output = io.BytesIO()
		file_name = 'Inventario.xlsx'
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("Inventario")
		columns = [{"name": 'SKU', "width":12, "rotate": False}, {"name": 'Descripción', "width": 40, "rotate": False}, {"name": 'Variante', "width": 14, "rotate": False}, {"name": 'Código 7', "width": 14, "rotate": False}, {"name": 'Total', "width": 7.4, "rotate": True}, {"name": 'Malo', "width": 7.4, "rotate": True}, {"name": 'Con detalle', "width": 7.4, "rotate": True}]
		row_num = 0

		for col_num in range(len(columns)):
			if columns[col_num]['rotate']:
				ws.write(row_num, col_num, str(columns[col_num]['name']),)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
			else:
				ws.write(row_num, col_num, str(columns[col_num]['name']),)
				ws.set_column(col_num, col_num, columns[col_num]['width'])
		rows = Inventory_Product.objects.filter(inventory__id=int(id_inventory)).values('product__sku', 'product__description', 'product_code__color__description', 'product_code__code_seven_digits','stock_good', 'stock_bad', 'stock_detail').order_by('product__sku','product_code__code_seven_digits')
		# print(rows)
		for row in rows:
			row_num += 1
			ws.write(row_num, 0, row['product__sku'],)
			ws.write(row_num, 1, row['product__description'],)
			ws.write(row_num, 2, row['product_code__color__description'],)
			ws.write(row_num, 3, row['product_code__code_seven_digits'],)
			ws.write(row_num, 4, row['stock_good'],)
			ws.write(row_num, 5, row['stock_bad'],)
			ws.write(row_num, 6, row['stock_detail'],)
		wb.close()
		output.seek(0)
		filename = 'Inventario.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		# print("--- %s seconds ---" % (time.time() - start_time))
		return response
		return Response(data={"report": ['Reporte generado exitosamente']}, status=status.HTTP_200_OK)

class Product_SaleStockViewSet(ModelViewSet):
	queryset = Product_Sale.objects.exclude(sku__in=[1,2]).order_by('sku')
	serializer_class = Product_SaleStockSerializer
	permission_classes = (AllowAny,)
	pagination_class = PaginationStock
	filter_backends = (search_filter.SearchFilter,)
	http_method_names  = ['get', 'put']
	search_fields  = ('^sku', '^product_code__code_seven_digits', 'description')


	def update(self, request, pk=None):
		# print(request.data)
		if request.user.id != 6:
			return Response(data={"reset": ['Disculpe ud no tiene permiso para resetear este producto de inventario']}, status=status.HTTP_401_UNAUTHORIZED)
		product_code = Product_Code.objects.filter(id=pk)
		# product_sale = Product_Sale.objects.get(id=product_code[0].product.id)
		product_code.update(quantity_reset=request.data, reset=True, date_reset=datetime.datetime.now(), quantity=request.data, user_reset=request.user)

		return Response(data={"product_sale": ['Inventario de producto reiniciado exitosamente']}, status=status.HTTP_200_OK)

class Product_SaleStockViewSetFileXLSX(ModelViewSet):
	queryset = Product_Sale.objects.exclude(sku__in=[1,2]).order_by('sku')
	serializer_class = Product_SaleStockSerializer
	permission_classes = (AllowAny,)
	pagination_class = None
	filter_backends = (search_filter.SearchFilter,)
	http_method_names  = ['get',]
	search_fields  = ('^sku', '^product_code__code_seven_digits', 'description')

	def write_file(self, ws, row, data, cell_format_data):

		ws.write(row, 0, int(data['sku']), cell_format_data)
		ws.write(row, 1, str(data['description']), cell_format_data)
		ws.write(row, 2, str(data['variante']), cell_format_data)
		ws.write(row, 3, data['initial_stock'], cell_format_data)
		ws.write(row, 4, data['initial_stock_warehouses'], cell_format_data)
		ws.write(row, 5, data['b1_retirement'], cell_format_data)
		ws.write(row, 6, data['b2_retirement'], cell_format_data)
		ws.write(row, 7, data['office_retirement'], cell_format_data)
		ws.write(row, 8, data['purchase5'], cell_format_data)
		ws.write(row, 9, data['sales_quantity'], cell_format_data)
		ws.write(row, 10, data['sale_pending'], cell_format_data)
		b1_stock = int(data['b1_stock']) - int(data['b1_retirement'])
		b2_stock = int(data['b2_stock']) - int(data['b2_retirement'])
		office_stock = data['initial_stock'] + data['b1_retirement'] + data['b2_retirement'] - data['sales_quantity']
		ws.write(row, 11, b1_stock, cell_format_data)
		ws.write(row, 12, b2_stock, cell_format_data)
		ws.write(row, 13, (b1_stock + b2_stock + data['purchase5']), cell_format_data)
		ws.write(row, 14, office_stock, cell_format_data)

		total_stock = (b1_stock + b2_stock + data['purchase5']) + office_stock - data['sale_pending']
		ws.write(row, 15, total_stock, cell_format_data)

	def list(self, request):
		queryset = Product_Sale.objects.exclude(sku__in=[1,2]).order_by('sku')
		serializer = Product_SaleStockSerializer(queryset, many=True)
		today = datetime.datetime.now().date()
		output = io.BytesIO()
		file_name = 'Inventario Asiamerica ' + str(today)
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet(str(today))
		cell_format_fields = wb.add_format({'bg_color': 'yellow', 'border': 1, 'font_size': 9})
		cell_format_fields.set_center_across()
		cell_format_fields.set_align('vcenter')
		cell_format_fields.set_text_wrap()
		cell_format_fields2 = wb.add_format({'bg_color': '#76933c', 'border': 1, 'font_size': 9})
		cell_format_fields2.set_center_across()
		cell_format_fields2.set_align('vcenter')
		cell_format_fields2.set_text_wrap()
		cell_format_fields3 = wb.add_format({'bg_color': '#538dd5', 'border': 1, 'font_size': 9})
		cell_format_fields3.set_center_across()
		cell_format_fields3.set_align('vcenter')
		cell_format_fields3.set_text_wrap()
		cell_format_fields4 = wb.add_format({'bg_color': '#963634', 'border': 1, 'font_size': 9})
		cell_format_fields4.set_center_across()
		cell_format_fields4.set_align('vcenter')
		cell_format_fields4.set_text_wrap()
		columns = [
			{"name": 'Sku 7', "width":12, "rotate": False, "format_data":cell_format_fields},
			{"name": 'Descripción', "width":25, "rotate": False, "format_data":cell_format_fields},
			{"name": 'Variante', "width":12, "rotate": False, "format_data":cell_format_fields}, 
			{"name": '1. Inv inicial oficina', "width": 9, "rotate": False, "format_data":cell_format_fields2}, 
			{"name": '2. Inv inicial bodega', "width": 9, "rotate": False, "format_data":cell_format_fields2}, 
			{"name": '3. Añadido a oficina de B1', "width": 9, "rotate": False, "format_data":cell_format_fields3}, 
			{"name": '4. Añadido a oficina de B2', "width": 9, "rotate": False, "format_data":cell_format_fields3}, 
			{"name": '5. Añadido a B2 de oficina', "width": 9, "rotate": False, "format_data":cell_format_fields3}, 
			{"name": '6. Compras periodo', "width": 10, "rotate": False, "format_data":cell_format_fields}, 
			{"name": '7. Ventas entregadas', "width": 10, "rotate": False, "format_data":cell_format_fields}, 
			{"name": '8. Ventas por entregar (30 días)', "width": 10, "rotate": False, "format_data":cell_format_fields}, 
			{"name": '9. Queda B1', "width": 9, "rotate": False, "format_data":cell_format_fields3}, 
			{"name": '10. Queda B2', "width": 9, "rotate": False, "format_data":cell_format_fields3}, 
			{"name": '11. Queda bodegas real (6+9+10)', "width": 9, "rotate": False, "format_data":cell_format_fields3}, 
			{"name": '12. Queda oficina real (1+3+4-5-7)', "width": 9, "rotate": False, "format_data":cell_format_fields3}, 
			{"name": '13. Disponible venta (11+12-8)', "width": 10, "rotate": False, "format_data":cell_format_fields4}, 
		]
		row_num = 0

		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data.set_text_wrap()
		for col_num in range(len(columns)):
			# if columns[col_num]['rotate']:
			ws.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
			ws.set_column(col_num, col_num, columns[col_num]['width'])
			# else:
			# 	ws.write(row_num, col_num, str(columns[col_num]['name']), cell_format_fields)
			# 	ws.set_column(col_num, col_num, columns[col_num]['width'])
		rows = serializer.data
		row_num += 1
		for row in rows:
			if row['product_code']:
				for i in row['product_code']:
					if int(i['code_seven_digits'][5:7]) > 49:
						data_write = {
							"sku":i['code_seven_digits'],
							"description":row['description'],
							"variante":i['color_description'],
							"initial_stock":i['initial_stock'],
							"initial_stock_warehouses":i['initial_warehouse_stock'],
							"b1_retirement":i['b1_retirement'],
							"b2_retirement":i['b2_retirement'],
							"office_retirement":0,
							"purchase5": i['purchases_warehouse5'],
							"sales_quantity":i['sale_quantity'],
							"sale_pending":i['sale_pending'],
							"b1_stock":i['b1_stock'],
							"b2_stock":i['b2_stock'],
						}
						self.write_file(ws, row_num, data_write, cell_format_data)
						row_num += 1
					else:
						pass
			else:
				pass
		ws.autofilter(0, 0, 0, len(columns) - 1)
		ws.freeze_panes(1, 0)
		wb.close()
		output.seek(0)
		filename = 'Inventario de Productos Asiamerica.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		return response
		# return Response(serializer.data)

class TotalizationInventory(APIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		output = io.BytesIO()
		wb =  xlsxwriter.Workbook(output)
		ws = wb.add_worksheet("Hoja1")
		cell_format_fields = wb.add_format({'bg_color': 'yellow', 'border': 1, 'font_size': 9})
		cell_format_fields.set_center_across()
		cell_format_fields.set_align('vcenter')
		cell_format_fields.set_text_wrap()
		columns = [
			{"name": 'Día', "width":12, "rotate": False, "format_data":cell_format_fields},
			{"name": 'Unidades Disponibles', "width":20, "rotate": False, "format_data":cell_format_fields},
			{"name": 'Valor ($)', "width":20, "rotate": False, "format_data":cell_format_fields}, 
		]
		row_num = 0

		cell_format_data = wb.add_format({'font_size': 9.5})
		cell_format_data2 = wb.add_format({"font_size": 9.5, 'num_format': '#,###'})
		for col_num in range(len(columns)):
			ws.write(row_num, col_num, str(columns[col_num]['name']), columns[col_num]['format_data'])
			ws.set_column(col_num, col_num, columns[col_num]['width'])
		rows = InventoryTotalization.objects.values().order_by('-date_search')
		row_num += 1
		for row in rows:
			ws.write(row_num, 0, row['date_search'].strftime("%d-%m-%Y"), cell_format_data)
			ws.write(row_num, 1, row['quantity_stock'], cell_format_data2)
			ws.write(row_num, 2, row['total_cost'], cell_format_data2)
			row_num += 1
		wb.close()
		output.seek(0)
		filename = 'Totalización de Productos.xlsx'
		response = HttpResponse(output,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		response['Content-Disposition'] = 'attachment; filename=%s' % filename
		# print("--- %s seconds ---" % (time.time() - start_time))
		return response
