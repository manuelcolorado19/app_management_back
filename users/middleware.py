from django.contrib.auth.middleware import get_user
from django.utils.functional import SimpleLazyObject
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
import datetime
from django.contrib.auth import get_user_model

User = get_user_model()
now = datetime.datetime.now()

class JWTAuthenticationMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.user = SimpleLazyObject(lambda:self.__class__.get_jwt_user(request))
        # print(request.user)
        if request.user.is_anonymous:
            # print(request.user)
            return self.get_response(request)
        else:
            if request.user.is_superuser and request.user.superuser_until != None and request.user.superuser_until < now :
                # print("ñlkjhg")
                user = User.objects.get(id=request.user.id)
                user.is_superuser = False
                user.save()
                return self.get_response(request)
            else:
                return self.get_response(request)
        return self.get_response(request)

    @staticmethod
    def get_jwt_user(request):
        user = get_user(request)
        if user.is_authenticated:
            return user
        jwt_authentication = JSONWebTokenAuthentication()
        if jwt_authentication.get_jwt_value(request):
            user, jwt = jwt_authentication.authenticate(request)
        return user


# class UserUntilTimeMiddleware:
#     def __init__(self, get_response):
#         self.get_response = get_response

#     def __call__(self, request):

        if request.user.is_anonymous:
            print(request.user)
            return self.get_response(request)
        else:
            if request.user.is_superuser and request.user.superuser_until < now and request.user.superuser_until != None:
                user = User.objects.get(id=request.user.id)
                user.is_superuser = False
                user.save()
                return self.get_response(request)
            else:
                return self.get_response(request)
