from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model


class User(AbstractUser):
    USER_STATUS = (
        ('active', 'active'),
        ('disabled', 'disabled'),
        ('locked', 'locked'),
    )

    rut = models.CharField(max_length=15, unique=True)
    phone = models.CharField(max_length=20, null=True)
    web = models.CharField(max_length=255, null=True)
    address = models.CharField(max_length=255, null=True)
    user_status = models.CharField(max_length=10, choices=USER_STATUS)
    review = models.TextField(blank=True, default='')
    superuser_until=models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'auth_user'

class TokenUser(models.Model):
    token = models.CharField(max_length=300)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'token_user'


# Create your models here.
