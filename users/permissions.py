from rest_framework import permissions
import datetime
from datetime import timedelta

now = datetime.datetime.now()
class UserPermissions(permissions.IsAuthenticated):
    def has_permission(self, request, view):
    	if not request.user.is_anonymous:
	        if request.user.is_superuser:
	        	if request.user.superuser_until is not None:
	        		return now < request.user.superuser_until
	        	else:
	        		return True
	        else:
	        	if view.action in ['list', 'retrieve']:
	        		return True
	    else:
            return False
