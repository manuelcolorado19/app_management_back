from django.contrib.auth import get_user_model
from faker import Faker
from random import randint
from datetime import datetime
import random
from random_words import RandomWords

rw = RandomWords()
fake = Faker()
User = get_user_model()

for i in range(5):
	User.objects.create_user(
		username=rw.random_word(),
		first_name=fake.name(),
		last_name=fake.name(),
		password="1111",
		rut=str(randint(100000,999999)),
		phone=str(randint(100000,999999)),
		web=fake.url(),
		address=fake.address(),
		user_status=random.choice(['active', 'disabled', 'locked']),
		review=fake.text(),
	)
print("Users created")


