from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import TokenUser
User = get_user_model()
import datetime

class TodayDate(serializers.CharField):
    def to_representation(self, value):
        return str(datetime.datetime.now())

class UserSerializer(serializers.ModelSerializer):
    today_date = TodayDate(read_only=True, source='*')
    class Meta:
        model = User
        exclude = ('is_staff', 'is_active', 'user_status', 'groups', 'user_permissions', 'last_login',)
        extra_kwargs = {
            'password': {
                'write_only': True
            },
            'date_joined': {
                'read_only': True
            },
            'superuser_until': {
                'validators': [],
            },

        }
    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        # print(validated_data)
        # return User.objects.get(id=1)
        # if validated_data.get('superuser_until', instance.superuser_until) is not None:
        #     instance.superuser_until = validated_data.get('superuser_until', instance.superuser_until)
        # else:
        #     instance.superuser_until = None
        instance.is_superuser = True
        instance.superuser_until = validated_data.get('superuser_until', instance.superuser_until)
        instance.save()
        return instance

class ForgotSerializer(serializers.Serializer):
    email = serializers.EmailField()

class RecoverPasswordSerializer(serializers.Serializer):
    password = serializers.CharField()
    confirm_password = serializers.CharField()
    token = serializers.CharField()

    def validate(self, data):
        if data['password'] != data['confirm_password']:
            raise serializers.ValidationError({"password": ['Las contraseñas no coinciden.']})
        if not TokenUser.objects.filter(token=data['token']):
            raise serializers.ValidationError({"token": ['El token enviado no coincide con algun registro de recuperación de contraseña']})
        return data