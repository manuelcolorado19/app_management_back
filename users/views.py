from django.shortcuts import render
from .serializers import UserSerializer, ForgotSerializer, RecoverPasswordSerializer
from rest_framework.viewsets import ModelViewSet
from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from jwt import decode, encode
from OperationManagement.settings import JWT_AUTH, SECRET_KEY, FRONT_URL
from .models import TokenUser
from time import time
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.db import transaction
User = get_user_model()
class UserViewSet(ModelViewSet):
    queryset = User.objects.all().order_by('rut')
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
    	# if self.request.user.is_superuser:
    	# 	return self.queryset.filter()
    	return self.queryset.filter(id=self.request.user.id)

    # @transaction.atomic
    # def create(self, request):
    # 	# print(request.data)
    # 	# request.data['password'] = "asiamerica2018"
    # 	serializer = self.get_serializer(data=request.data)
    # 	serializer.is_valid(raise_exception=True)
    # 	self.perform_create(serializer)
    # 	headers = self.get_success_headers(serializer.data)
    # 	return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

class ForgotViewSet(APIView):
	permission_classes = (AllowAny,)

	def post(self, request):
		serializer = ForgotSerializer(data=request.data)
		if not serializer.is_valid():
			return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		else:
			user = User.objects.filter(email=serializer.data['email'])
			if user:
				token = encode(
                {
                    'user_email': user[0].email,
                    'exp': int(time()) + JWT_AUTH['JWT_EXPIRATION_DELTA'].total_seconds()
                },
                SECRET_KEY, algorithm=JWT_AUTH['JWT_ALGORITHM']).decode("utf-8")
				TokenUser.objects.create(token=token, user=user[0])
				subject = 'Recuperación de contraseña'
				from_email = 'info@asiamerica.cl'
				to = serializer.data['email']
				msg_html = render_to_string('recover-password.html',
					{
						"name": user[0].first_name + " " + user[0].last_name,
						"url": FRONT_URL + "/auth/change-password?token=" + token,
					}
				)
				send_mail(
				   'Recuperación de contraseña',
				   subject,
				   from_email,
				   [to, ],
				   html_message=msg_html,
				)
				return Response(data={"email": ['Por favor ingrese a su correo para poder confirmar y reestablecer su contraseña']}, status=status.HTTP_200_OK)
			else:
				return Response(data={"email": ['No existe registro del correo electrónico indicado']}, status=status.HTTP_400_BAD_REQUEST)

class RecoverPasswordViewSet(APIView):
	permission_classes = (AllowAny,)

	def post(self, request):
		serializer = RecoverPasswordSerializer(data=request.data)
		if not serializer.is_valid():
			return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		else:
			# print(serializer.data)
			user = TokenUser.objects.get(token=serializer.data['token'])
			user.user.set_password(serializer.data['password'])
			user.user.save()
			user.delete()
			return Response(data={"password": ['Contraseña cambiada exitosamente']}, status=status.HTTP_200_OK)

# Create your views here.
