# Generated by Django 2.1.2 on 2019-04-24 11:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0066_product_code_quantity_reset'),
        ('warehouses', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='retirementproduct',
            name='product_sale',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='product_sale_retirement', to='sales.Product_Sale'),
        ),
        migrations.AddField(
            model_name='stockwarehouse',
            name='product_sale',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='product_sale_warehouse', to='sales.Product_Sale'),
        ),
    ]
