from django.db import models
from sales.models import Product_Code, Product_Sale
from django.contrib.auth import get_user_model
from django.utils.timezone import now
User = get_user_model()

class WareHouse(models.Model):
	name = models.CharField(max_length=100)
	address = models.CharField(max_length=50)
	phone = models.CharField(max_length=15)
	email = models.CharField(max_length=50)
	square_meters = models.IntegerField(null=True)
	class Meta:
		db_table = 'warehouse'

class StockWarehouse(models.Model):
	product_sale = models.ForeignKey(Product_Sale, on_delete=models.SET_NULL, related_name='product_sale_warehouse', null=True)
	warehouse = models.ForeignKey(WareHouse, on_delete=models.CASCADE, related_name='warehouse_stock')
	initial_stock = models.IntegerField(null=True)
	date_count = models.DateTimeField(auto_now_add=True, null=True, blank=True)
	class Meta:
		db_table = 'stock_warehouse'

class StockWareHouseCode(models.Model):
	product_code = models.ForeignKey(Product_Code, on_delete=models.SET_NULL, related_name='product_code_stock_warehouse', null=True)
	stock_warehouse = models.ForeignKey(StockWarehouse, on_delete=models.CASCADE, related_name='stock_warehouse')
	initial_stock = models.IntegerField(null=True)
	date_count = models.DateTimeField(auto_now_add=True, null=True, blank=True)

class Retirement(models.Model):

	STATUS_RETIREMENT = (
		('aprobada', 'aprobada'),
		('sin aprobar', 'sin aprobar'),
	)
	date_retirement = models.DateField(default=now, blank=True)
	user_created = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='user_created_retirement')
	warehouse = models.ForeignKey(WareHouse, on_delete=models.CASCADE, null=True, related_name='warehouse_retirement')
	total_products = models.IntegerField(default=0)
	document_number = models.IntegerField(null=True)
	token_bill = models.CharField(null=True, blank=True, max_length=70)
	status_retirement = models.CharField(choices=STATUS_RETIREMENT, blank=True, null=True, max_length=30)
	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)

	class Meta:
		db_table = 'retirement'

class RetirementProduct(models.Model):
	product_sale = models.ForeignKey(Product_Sale, on_delete=models.SET_NULL, related_name='product_sale_retirement', null=True)
	product_code = models.ForeignKey(Product_Code, on_delete=models.SET_NULL, related_name='product_code_retirement', null=True)
	retirement = models.ForeignKey(Retirement, on_delete=models.CASCADE, related_name='retirement_product')
	quantity = models.IntegerField()
	unit_price = models.DecimalField(max_digits=10,decimal_places=2)
	total = models.DecimalField(max_digits=10 ,decimal_places=2)
	class Meta:
		db_table = 'retirement_product'


# class Movement_WareHouse(models.Model):
# 	warehouse_in = models.ForeignKey(WareHouse, on_delete=models.SET_NULL, related_name='movement_warehouse_in', null=True)
# 	warehouse_out = models.ForeignKey(WareHouse, on_delete=models.SET_NULL, related_name='movement_warehouse_out', null=True)
# 	date_movement = models.DateField(default=now, blank=True)
# 	user_movement = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
# 	total_products = models.IntegerField()
# 	date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
# 	from_office = models.BooleanField(default=False)
# 	class Meta:
# 		db_table = 'movement_warehouse'

# class Product_Movement_WareHouse(models.Model):
# 	product_code = models.ForeignKey(Product_Code, on_delete=models.SET_NULL, related_name='product_code_movement', null=True)
# 	quantity = models.IntegerField(null=True)
# 	movement_warehouse = models.ForeignKey(Movement_WareHouse, )
