from .models import WareHouse, StockWarehouse, StockWareHouseCode, RetirementProduct, Retirement
from rest_framework import serializers
from sales.models import Product_Sale, Product_Code, Sale_Product
from purchases.serializers import PurchasesWareHouse, PurchasesWareHouseCode
from django.db.models import Sum, Q, F, Case, When, Value, IntegerField
from OperationManagement.settings import DATA_RECEIVER_WAREHOUSE, DATA_EMITTER_SHIPPING_GUIDE, OPEN_FACTURA_URL, API_KEY_OF
import requests, json, datetime
from datetime import timedelta
from stock.models import Inventory_Product, Inventory
from purchases.models import Purchase_Product
date_last_inventory = Inventory.objects.latest('date_created').date_stock_break.date()
class RetirementWareHouse(serializers.IntegerField):
	def to_representation(self, value):
		# retirements = RetirementProduct.objects.filter(product_sale=value.product_sale, retirement__warehouse=value.warehouse, retirement__date_retirement__gte=value.date_count).aggregate(count_retirements=Sum('quantity'))
		# if retirements['count_retirements'] is not None:
		# 	return retirements['count_retirements']
		return 0

class IncomingWareHouse(serializers.IntegerField):
	def to_representation(self, value):
		# retirements = RetirementProduct.objects.filter(product_sale=value, retirement__date_retirement__gte=datetime.date(2019,4,29)).aggregate(count_retirements=Sum('quantity'))
		# if retirements['count_retirements'] is not None:
		# 	return retirements['count_retirements']
		return 0

class IncomingWareHouseCode(serializers.IntegerField):
	def to_representation(self, value):
		if value.reset:
			retirements = RetirementProduct.objects.filter(product_code=value, retirement__date_created__gte=value.date_reset).aggregate(count_retirements=Sum('quantity'))
			if retirements['count_retirements'] is not None:
				return retirements['count_retirements']
		else:
			retirements = RetirementProduct.objects.filter(product_code=value, retirement__date_retirement__gte=datetime.date(2019,4,29)).aggregate(count_retirements=Sum('quantity'))
			if retirements['count_retirements'] is not None:
				return retirements['count_retirements']
		return 0


class RetirementWareHouseCode(serializers.IntegerField):
	def to_representation(self, value):
		retirements = RetirementProduct.objects.filter(product_code=value.product_code, retirement__warehouse=value.stock_warehouse.warehouse, retirement__date_retirement__gte=value.date_count).aggregate(count_retirements=Sum('quantity'))
		if retirements['count_retirements'] is not None:
			return retirements['count_retirements']
		return 0

class VariationCode(serializers.CharField):
	def to_representation(self, value):
		return value.color.description

class Product_CodeWareHouse(serializers.ModelSerializer):
	variation = VariationCode(read_only=True, source='*')
	class Meta:
		model = Product_Code
		fields = ('code_seven_digits', 'variation')


class SalePendingCode(serializers.IntegerField):
	def to_representation(self, value):
		today_lest_7 = datetime.datetime.now().date() + timedelta(days=-7)
		sales = Sale_Product.objects.filter(Q(product_code=value.product_code), Q(sale__cut_inventory__in=["I4",""]), Q(sale__status='pendiente'), Q(sale__date_sale__gte=today_lest_7)).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
		if sales is not None:
			return sales
		else:
			return 0
class OfficeCount(serializers.IntegerField):
	def to_representation(self, value):
		initial_stock = Inventory_Product.objects.filter(product_code=value.product_code, inventory__name="I3")
		if initial_stock:
			initial_stock_good = initial_stock[0].stock_good
		else:
			initial_stock_good = 0
		sales = Sale_Product.objects.filter(Q(product_code=value.product_code), Q(sale__cut_inventory__in=['']), Q(sale__status='entregado') | Q(sale__comments__icontains="Guía de Despacho")).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
		if sales is not None:
			sale_count = sales
		else:
			sale_count = 0
		purchases = Purchase_Product.objects.filter(product_purchase_code=value.product_code, purchase__date_purchase__gte=datetime.date(2019,4,1), purchase__warehouse__isnull=True).aggregate(count_product=Sum('quantity'))
		if purchases['count_product'] is not None:
			purchase_count = purchases['count_product']
		else:
			purchase_count = 0
		return initial_stock_good - sale_count + purchase_count

class B1Stock(serializers.IntegerField):
	def to_representation(self, value):
		stock_warehouse = StockWareHouseCode.objects.filter(product_code=value.product_code, stock_warehouse__warehouse__id=1).aggregate(sum_products=Sum('initial_stock'))
		if stock_warehouse['sum_products'] is not None:
			return stock_warehouse['sum_products']
		return 0

class B1Retirement(serializers.IntegerField):
	def to_representation(self, value):
		retirement_warehouse = RetirementProduct.objects.filter(product_code=value.product_code, retirement__warehouse__id=1).aggregate(sum_products=Sum('quantity'))
		if retirement_warehouse['sum_products'] is not None:
			return retirement_warehouse['sum_products']
		return 0

class B2Stock(serializers.IntegerField):
	def to_representation(self, value):
		stock_warehouse = StockWareHouseCode.objects.filter(product_code=value.product_code, stock_warehouse__warehouse__id=2).aggregate(sum_products=Sum('initial_stock'))
		if stock_warehouse['sum_products'] is not None:
			return stock_warehouse['sum_products']
		return 0

class B2Retirement(serializers.IntegerField):
	def to_representation(self, value):
		retirement_warehouse = RetirementProduct.objects.filter(product_code=value.product_code, retirement__warehouse__id=2).aggregate(sum_products=Sum('quantity'))
		if retirement_warehouse['sum_products'] is not None:
			return retirement_warehouse['sum_products']
		return 0


# Devuelve conteo y retiros y demas para codigos de 7
class StockWareHouseCodeSerializer(serializers.ModelSerializer):
	product_code = Product_CodeWareHouse(read_only=True)
	purchases_warehouse_code = PurchasesWareHouseCode(read_only=True, source='*')
	retirement_warehouse_code = RetirementWareHouseCode(read_only=True, source='*')
	sale_pending = SalePendingCode(read_only=True, source='*')
	office_count = OfficeCount(read_only=True, source='*')
	b1_stock = B1Stock(read_only=True, source='*')
	b1_retirement = B1Retirement(read_only=True, source='*')
	b2_stock = B2Stock(read_only=True, source='*')
	b2_retirement = B2Retirement(read_only=True, source='*')
	class Meta:
		model = StockWareHouseCode
		fields = '__all__'

class Product_SaleWareHouse(serializers.ModelSerializer):
	class Meta:
		model = Product_Sale
		fields = ('sku', 'description',)


# Devuelve conteo y retiros y demas para codigos de 5
class StockWarehouseSerializer(serializers.ModelSerializer):
	product_sale = Product_SaleWareHouse(read_only=True)
	stock_warehouse = StockWareHouseCodeSerializer(read_only=True, many=True)
	purchases_warehouse = PurchasesWareHouse(read_only=True, source='*')
	retirement_warehouse = RetirementWareHouse(read_only=True, source='*')
	class Meta:
		model = StockWarehouse
		fields = '__all__'

class WareHouseSerializer(serializers.ModelSerializer):
	warehouse_stock = StockWarehouseSerializer(read_only=True,  many=True)
	class Meta:
		model = WareHouse
		fields = '__all__'

class WareHouseNameSerializer(serializers.CharField):
	def to_representation(self, value):
		return value.warehouse.name

class ProductsRetirement(serializers.ListField):
	def to_representation(self, value):
		return value.retirement_product.all().values('product_sale__sku', 'product_sale__description', 'product_code__code_seven_digits', 'quantity', 'unit_price', 'product_code__color__description', 'total', 'id')

class RetirementNewSerializer(serializers.ModelSerializer):
	products = serializers.ListField(
	   child=serializers.JSONField(), write_only=True
	)
	warehouse_name = WareHouseNameSerializer(read_only=True, source='*')
	products_retirement = ProductsRetirement(read_only=True, source='*')
	class Meta:
		model = Retirement
		fields = '__all__'

	def create(self, validated_data):
		details_guide = []
		total = 0
		index = 1
		for product in validated_data['products']:
			data_append = {}
			data_append['NroLinDet'] = index
			data_append['NmbItem'] = product['description'] + " - Variante: " + product['color']
			# data_append['DscItem'] = product['description'] 
			data_append['QtyItem'] = product['quantity']
			data_append['PrcItem'] = 1
			data_append['MontoItem'] = product['quantity']
			data_append["CdgItem"] = [
				{
					"TpoCodigo": "INT1",
					"VlrCodigo": str(product['code_seven_digits'])
				}
			]
			details_guide.append(data_append)
			index += 1
			total += data_append['MontoItem']
		iva = round(total * 0.19)
		number_dec = str((total * 0.19)-int(total * 0.19))[2:3]
		iva = (iva + 1) if number_dec == '5' else (iva)
		data_bill = {
				  "response":[  
				  "TIMBRE",
				  "PDF"
			   ],
			   "dte":{  
			  "Encabezado":{  
				 "IdDoc":{  
					"TipoDTE":52,
					"Folio":0,
					"FchEmis":validated_data['date_retirement'].strftime("%Y-%m-%d"),
					"TipoDespacho":"2",
					"IndTraslado":"3",
					"TpoTranVenta":"1"
				 },
				 "Emisor":DATA_EMITTER_SHIPPING_GUIDE,
				 "Receptor":DATA_RECEIVER_WAREHOUSE,
				  "Transporte":{  
					"DirDest":"ANTONIO VARAS 2156",
					"CmnaDest":"ÑUÑOA",
					"CiudadDest":"SANTIAGO"
				 },
				 "Totales":{  
					"MntNeto":0,
					"TasaIVA":"19",
					"IVA":0,
					"MntTotal":0,
					# "MontoPeriodo":total + iva,
					# "VlrPagar":total + iva
				 }
			  },
			  "Detalle": details_guide,
			  "DscRcgGlobal":[
				{
				   "NroLinDR":1,
				   "TpoMov":"D",
				   "TpoValor":"%",
				   "ValorDR":"100"
				}
			 ]
			}
		}
		url_of = OPEN_FACTURA_URL + "/v2/dte/document"
		headers = {'content-type': 'application/json', 'apikey': API_KEY_OF}
		response = requests.post(url_of, data=json.dumps(data_bill, ensure_ascii=False).encode("utf-8"), headers=headers)
		# print(response.json())
		if response.status_code == 200:
			# Se guardan todos los datos y se devuelve la guía de despacho
			response_folio = requests.get(OPEN_FACTURA_URL + "/v2/dte/document/" + response.json()['TOKEN'] + "/json", headers=headers).json()
			folio = response_folio['json']['Encabezado']['IdDoc']['Folio']
			retirement = Retirement.objects.create(
				date_retirement=validated_data['date_retirement'],
				user_created=self.context['request'].user,
				warehouse=validated_data['warehouse'],
				total_products=0,
				document_number=folio,
				token_bill=response.json()['TOKEN'],
			)
			total_products = 0
			for product in validated_data['products']:
				product_code = Product_Code.objects.get(code_seven_digits=str(product['code_seven_digits']))
				retirement_product = RetirementProduct.objects.create(
					product_sale=product_code.product,
					product_code=product_code,
					retirement=retirement,
					quantity=int(product['quantity']),
					unit_price=1,
					total=product['quantity'],
				)
				total_products += int(product['quantity'])
			retirement.total_products = total_products
			retirement.save()

			return retirement
		else:
			raise serializers.ValidationError({'retirement': ["Ocurrió un error al generar la guía de retiro"]})

		# print(response.json())
		# return Retirement.objects.get(id=1)

class ColorProductCode(serializers.CharField):
	def to_representation(self, value):
		return value.description

class SalePendingDistribution(serializers.IntegerField):
	def to_representation(self, value):
		today_lest_7 = datetime.datetime.now().date() + timedelta(days=-30)
		sales = Sale_Product.objects.filter(Q(product_code=value), Q(sale__status='pendiente'), Q(sale__date_sale__gte=today_lest_7)).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
		if sales is not None:
			return sales
		else:
			return 0

class WarehouseDistributionStock(serializers.ListField):
	def to_representation(self, value):
		warehouses = WareHouse.objects.all()
		warehouses_append = []
		for warehouse in warehouses:
			stock_warehouse = StockWareHouseCode.objects.filter(product_code_id=value, id__gt=571).values_list('initial_stock', flat=True)
			purchases = Purchase_Product.objects.filter(product_purchase_code_id=value, purchase__date_purchase__gt=date_last_inventory).aggregate(count_product=Sum('quantity'))
			if purchases['count_product'] is not None:
				purchases__ = purchases['count_product']
			else:
				purchases__ = 0
			if stock_warehouse:
				# if warehouse.id == 1:
				# 	date_start = datetime.date(2019,6,6)
				# else:
				# 	date_start = datetime.date(2019,6,7)
				retirements_w1 = RetirementProduct.objects.filter(product_code_id=value, retirement__warehouse__id=1, retirement__date_retirement__gte=date_last_inventory, retirement__status_retirement="aprobada").aggregate(sum_products=Sum('quantity'))
				retirements_w2 = RetirementProduct.objects.filter(product_code_id=value, retirement__warehouse__id=2, retirement__date_retirement__gte=date_last_inventory, retirement__status_retirement="aprobada").aggregate(sum_products=Sum('quantity'))
				if retirements_w1['sum_products'] is not None:
					retirements1 = retirements_w1['sum_products']
				else:
					retirements1 = 0
				if retirements_w2['sum_products'] is not None:
					retirements2 = retirements_w2['sum_products']
				else:
					retirements2 = 0
				if warehouse.id == 1:
					warehouses_append.append({
						"name":warehouse.name,
						"stock":0
					})
				else:
					warehouses_append.append({
						"name":warehouse.name,
						"stock":(stock_warehouse[0] - retirements1 - retirements2 + purchases__)
					})
				#warehouses_append.append({
				#	"name":warehouse.name,
				#	"stock":(stock_warehouse[0] - retirements)
				#})
			else:
				warehouses_append.append({
					"name":warehouse.name,
					"stock":0
				})

		return warehouses_append


class OfficeStockCode(serializers.IntegerField):
	def to_representation(self, value):
		initial_stock = Inventory_Product.objects.filter(product_code_id=value).order_by('-inventory__date_created')[:1]
		if initial_stock:
			initial_stock_good = initial_stock[0].stock_good
		else:
			initial_stock_good = 0
		sales = Sale_Product.objects.filter(Q(product_code_id=value), Q(sale__status='entregado'), Q(sale__date_document_emision__gt=date_last_inventory)).aggregate(sales_quantity=Sum('quantity'))['sales_quantity']
		if sales is not None:
			sale_count = sales
		else:
			sale_count = 0
		# purchases = Purchase_Product.objects.filter(product_purchase_code_id=value, purchase__date_purchase__gt=date_last_inventory).aggregate(count_product=Sum('quantity'))
		# if purchases['count_product'] is not None:
			# purchase_count = purchases['count_product']
		# else:
			# purchase_count = 0
		retirements_in_b1 = RetirementProduct.objects.filter(product_code_id=value, retirement__warehouse__id=1, retirement__date_retirement__gt=date_last_inventory, retirement__status_retirement="aprobada").aggregate(sum_products=Sum('quantity'))['sum_products']
		retirements_in_b2 = RetirementProduct.objects.filter(product_code_id=value, retirement__warehouse__id=2, retirement__date_retirement__gt=date_last_inventory, retirement__status_retirement="aprobada").aggregate(sum_products=Sum('quantity'))['sum_products']
		if retirements_in_b1 is not None:
			r1 = retirements_in_b1
		else:
			r1 = 0
		if retirements_in_b2 is not None:
			r2 = retirements_in_b2
		else:
			r2 = 0
		return initial_stock_good - sale_count + 0 + r1 + r2

class BreakStock(serializers.IntegerField):
	def to_representation(self, value):
		if int(value[5:7]) > 49:
			return 1
		return 0

class StockDistributionVariations(serializers.ModelSerializer):
	color_name = ColorProductCode(read_only=True, source='color')
	sales_pending = SalePendingDistribution(read_only=True, source='id')
	warehousedistribution = WarehouseDistributionStock(read_only=True, source='id')
	office_stock = OfficeStockCode(read_only=True, source='id')
	break_stock = BreakStock(read_only=True, source='code_seven_digits')
	class Meta:
		model = Product_Code
		fields = '__all__'

class StockDistributionSerializer(serializers.ModelSerializer):
	product_code = StockDistributionVariations(read_only=True, many=True)
	class Meta:
		model = Product_Sale
		# fields = '__all__'
		fields = ('id','sku', 'description', 'product_code')
