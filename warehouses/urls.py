from .views import WareHouseViewSet, RetirementViewSet, StockDistributionViewSet
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'warehouse', WareHouseViewSet, base_name='warehouse')
router.register(r'retirement', RetirementViewSet, base_name='retirement')
router.register(r'stock-distribution', StockDistributionViewSet, base_name='stock-distribution')

urlpatterns = [
	url(r'^', include(router.urls)),
]