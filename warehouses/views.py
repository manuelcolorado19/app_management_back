from django.shortcuts import render
from .models import WareHouse, StockWarehouse, Retirement, StockWarehouse, StockWareHouseCode, RetirementProduct
from .serializers import WareHouseSerializer, StockWarehouseSerializer, RetirementNewSerializer, StockDistributionSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from OperationManagement.settings import OPEN_FACTURA_URL, API_KEY_OF
from django.db import transaction
import requests
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
import itertools
from operator import itemgetter
import datetime
from sales.models import Product_Sale, Product_Code
from rest_framework import filters as search_filter
from rest_framework.pagination import PageNumberPagination
from django.db.models import IntegerField, Value


class WareHouseViewSet(ModelViewSet):
	serializer_class = WareHouseSerializer
	queryset = WareHouse.objects.all().order_by('-id')
	permission_classes = (AllowAny,)
	http_method_names = ['get',]

class RetirementViewSet(ModelViewSet):
	serializer_class = RetirementNewSerializer
	queryset = Retirement.objects.all().order_by('-id')
	permission_classes = (AllowAny,)
	http_method_names = ['get', 'post', 'put']
	filter_backends = (search_filter.SearchFilter,)
	search_fields  = ('document_number','retirement_product__product_sale__sku', 'retirement_product__product_code__code_seven_digits')


	@transaction.atomic
	def create(self, request):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		token_bill = Retirement.objects.get(id=serializer.data['id']).token_bill
		url_of = OPEN_FACTURA_URL + "/v2/dte/document/" + token_bill + "/pdf"
		header_open_factura = {'content-type': 'application/json', 'apikey': API_KEY_OF}
		pdf_data = requests.get(url_of, headers=header_open_factura).json()
		newdict={'pdf':pdf_data["pdf"]}
		newdict.update(serializer.data)
		return Response(newdict, status=status.HTTP_201_CREATED, headers=headers)
		# return Response(data={"pdf_file": base_b4_report, "id": serializer.data['id']}, status=status.HTTP_201_CREATED, headers=headers)
	@transaction.atomic
	def update(self, request, pk=None):
		# print(request.data)
		status_retirement = self.request.query_params.get('status', None)
		if status_retirement is not None:
			Retirement.objects.filter(id=request.data['id']).update(status_retirement="aprobada")
			return Response(data={"retirement": ["Retiro actualizado exitosamente"]}, status=status.HTTP_200_OK)
		product_count = 0
		sales_products_id = []
		for each in request.data['products_retirement']:
			id_retirement_product = each.get('id', None)
			if id_retirement_product is not None:
				product_retirement = RetirementProduct.objects.filter(id=id_retirement_product).update(
					quantity=each['quantity'],
					unit_price=each['unit_price'],
					total=each['quantity']
				)
				product_count += int(each['quantity'])
				sales_products_id.append(id_retirement_product)
			else:
				product_retirement = RetirementProduct.objects.create(
					product_sale=Product_Sale.objects.get(sku=each['product_sale__sku']),
					product_code=Product_Code.objects.get(code_seven_digits=each['product_code__code_seven_digits']),
					quantity=int(each['quantity']),
					unit_price=int(each['unit_price']),
					total=int(each['quantity']),
					retirement_id=request.data['id'],
				)
				product_count += int(each['quantity'])
				sales_products_id.append(product_retirement.id)
		for i in RetirementProduct.objects.filter(retirement_id=request.data['id']):
			if not (i.id in sales_products_id):
				RetirementProduct.objects.get(id=i.id).delete()
		Retirement.objects.filter(id=request.data['id']).update(total_products=product_count)
		return Response(data={"retirement": ["Retiro actualizado exitosamente"]}, status=status.HTTP_200_OK)

class InitialStockWarehouse(APIView):
	permission_classes = (IsAuthenticated,)

	@transaction.atomic
	def post(self, request):
		warehouse = request.data['warehouse']
		# print(request.data)
		for i in request.data['data']:
			if Product_Code.objects.filter(code_seven_digits=i['sku']).count() == 0:
				return Response(data={"sku": ["No existe el sku " + str(i['sku']) + " en los registros"]}, status=status.HTTP_400_BAD_REQUEST)
			i['sku_5'] = str(i['sku'])[:5]
		sorted_order = sorted(request.data['data'], key=itemgetter('sku_5',))
		group_order = []
		for key, group in itertools.groupby(sorted_order, key=lambda x:x['sku_5']):
			group_order.append(list(group))

		# print(group_order)
		for each in group_order:
			stock_warehouse = StockWarehouse.objects.create(
				product_sale=Product_Sale.objects.get(sku=int(each[0]['sku_5'])),
				warehouse_id=int(warehouse),
				initial_stock=0,
			)
			total_count = 0
			for product in each:
				stock_code = StockWareHouseCode.objects.create(
					product_code=Product_Code.objects.get(code_seven_digits=product['sku']),
					stock_warehouse=stock_warehouse,
					initial_stock=product['initial_stock'],
					date_count=datetime.datetime.strptime(product['date_count'], '%d-%m-%Y'),
				)
				total_count += product['initial_stock']
			stock_warehouse.initial_stock = total_count
			stock_warehouse.save()
		
		return Response(data={"data": ["Data cargada exitosamente"]}, status=status.HTTP_200_OK)


class StockDistributionPagination(PageNumberPagination):
	page_size = 20
	page_size_query_param = 'page_size'
	max_page_size = 20
	def get_paginated_response(self, data):
		warehouses = WareHouse.objects.annotate(stock=Value(0, output_field=IntegerField())).values('name', 'stock')
		return Response({
		   'next': self.get_next_link(),
		   'previous': self.get_previous_link(),
			'count': self.page.paginator.count,
			'results': data,
			'warehouses': warehouses
		})
class StockDistributionViewSet(ModelViewSet):
	queryset = Product_Sale.objects.all()
	serializer_class = StockDistributionSerializer
	permission_classes = (AllowAny,)
	pagination_class = StockDistributionPagination
	http_method_names = ['get']
	filter_backends = (search_filter.SearchFilter,)
	search_fields  = ('^sku', '^product_code__code_seven_digits', 'description')
